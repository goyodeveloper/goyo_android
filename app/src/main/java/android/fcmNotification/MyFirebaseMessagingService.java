package android.fcmNotification;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.model.NotificationBadgeCountModel;
import com.differenzsystem.goyog.userProfile.SplashActivity;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Notification_Data " + remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData());
                //setBadgecount(getApplicationContext());
                handleDataMessage(json);
                callNotificationBadgeCountApi(getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // get notification type from jsonobject and based on that display notification
    private void handleDataMessage(final JSONObject data) {
        try {
            if (data.has(Constant.NotificationType)) {
                if (data.getString(Constant.NotificationType).equalsIgnoreCase(Constant.NewChallange)) {
                    int count = UtilsPreferences.getInt(getApplicationContext(), "CahllangeBadge");
                    UtilsPreferences.setInt(getApplicationContext(), Constant.CahllangeBadge, count + 1);
                }

                if (data.getString(Constant.NotificationType).equalsIgnoreCase(Constant.manually_win_nitification)) {
                    displayPopup(data);
                    if (data.has(Constant.is_won)) {
                        String won = data.getString(Constant.is_won);
                        if (won.equalsIgnoreCase("1")) {
                            int profileadge = UtilsPreferences.getInt(getApplicationContext(), Constant.ProfileBadge);
                            UtilsPreferences.setInt(getApplicationContext(), Constant.ProfileBadge, profileadge + 1);
                        }
                    }
                } else if (data.getString(Constant.NotificationType).equalsIgnoreCase(Constant.sync_band_request_notification)) {
                    displayPopup(data);
                } else {
                    createNotification(data);
                }
            } else {
                createNotification(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", "not able to create notification");
        }
    }

    // create notification and if its new challenge type the display image with text
    public void createNotification(final JSONObject data) {
        try {
            Intent resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
            resultIntent.putExtra(Constant.fromNotificationFlag, true);
            Bundle bundle = new Bundle();
            bundle.putString(Constant.notification, data.toString());
            resultIntent.putExtras(bundle);
            Log.e("Notification Data =>", data.toString());

            if (data.has(Constant.NotificationType)) {
                if (data.getString(Constant.NotificationType).equalsIgnoreCase(Constant.NewChallange)) {
                    showChallengeNotificationMessage(getApplicationContext(), data, resultIntent);
                } else {
                    showNotificationMessage(getApplicationContext(), getString(R.string.app_name), data.getString(Constant.alert), resultIntent);
                }
            } else {
                if (data.has(Constant.alert)) {
                    showNotificationMessage(getApplicationContext(), getString(R.string.app_name), data.getString(Constant.alert), resultIntent);
                } else {
                    if (data.has(Constant.title) && data.has(Constant.body)) {
                        showNotificationMessage(getApplicationContext(), data.getString(Constant.title), data.getString(Constant.body), resultIntent);
                    }

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // generate notification and set all the data
    private void showNotificationMessage(Context context, String title, String message, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, intent);
        setBadgecount(context);
    }

    // generate challenge notification and set all the data
    private void showChallengeNotificationMessage(Context context, JSONObject data, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showChallengeNotificationMessage(data, intent);
        setBadgecount(context);
    }

    // set the badge count and popup
    public void setBadgecount(Context context) {
        if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            int val = UtilsPreferences.getInt(context, Constant.totalbadge) + 1;
            ShortcutBadger.applyCount(context, val);
            UtilsPreferences.setInt(context, Constant.totalbadge, val);
        }
    }

    // if app is in foreground the instead of notification , it will display popup
    public void displayPopup(final JSONObject data) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (data.getString(Constant.NotificationType).equalsIgnoreCase(Constant.sync_band_request_notification)) {
                            foregroundSyncPopup(getApplicationContext(), data);
                        } else {
                            InstantWinPopup(data.getString(Constant.alert));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            createNotification(data);
        }
    }

    // display popup for wining challenge
    public static void InstantWinPopup(String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder
                (DashboardActivity.getInstance(), R.style.MyAlertDialogStyle);
        alertDialog.setCancelable(false);
        HomeFragment.setChartforNotificationtype();
        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //HomeFragment.setChartforNotificationtype();
                dialog.cancel();
                // Write your code here to invoke YES event
            }
        });

        alertDialog.show();
    }

    // popup appear in foreground
    public void foregroundSyncPopup(final Context aContext, final JSONObject data) {

        try {
            final String message = data.getString(Constant.alert);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder
                    (DashboardActivity.getInstance(), R.style.MyAlertDialogStyle);
            alertDialog.setCancelable(false);
            HomeFragment.setChartforNotificationtype();
            // Setting Dialog Message
            alertDialog.setMessage(data.getString(Constant.alert));

            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //HomeFragment.setChartforNotificationtype();
                    dialog.dismiss();
                    // Write your code here to invoke YES event
                    if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                        // Using ACTIVITY_SERVICE with getSystemService(String)
                        // to retrieve a ActivityManager for interacting with the global system state.

                        ActivityManager am = (ActivityManager) aContext
                                .getSystemService(Context.ACTIVITY_SERVICE);

                        // Return a list of the tasks that are currently running,
                        // with the most recent being first and older ones after in order.
                        // Taken 1 inside getRunningTasks method means want to take only
                        // top activity from stack and forgot the olders.

                        List<ActivityManager.RunningTaskInfo> alltasks = am
                                .getRunningTasks(1);

                        //
                        for (ActivityManager.RunningTaskInfo aTask : alltasks) {

                            // Used to check for CALL screen
                            if (aTask.topActivity.getClassName().equals("com.differenzsystem.goyog.dashboard.DashboardActivity")) {
                                if (HomeFragment.getInstance() != null && HomeFragment.getInstance().isVisible()) {
                                    HomeFragment.getInstance().onRefresh();
                                } else {
                                    Bundle extra = new Bundle();
                                    extra.putBoolean(Constant.key_refresh, true);
                                    DashboardActivity.getInstance().setupHomeFragment(extra);
                                }
                            } else {
                                DashBoardIntentForegroundSync();
                            }
                        }
                    }
                }
            });

            alertDialog.show();

        } catch (Throwable t) {
            Log.i(TAG, "Throwable caught: "
                    + t.getMessage(), t);
        }
    }

    // when user click on notification the here it will redirect to dashboard
    private void DashBoardIntentForegroundSync() {
        Intent notificationIntent = new Intent(getApplicationContext(), DashboardActivity.class);
        notificationIntent.putExtra(Constant.key_refresh, true);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(notificationIntent);
    }

    // api call for NotificationBadgeCount
    static void callNotificationBadgeCountApi(Context context) {
        if (ConnectionDetector.internetCheck(context)) {

            String user_id = UtilsPreferences.getString(context, Constant.user_id);
            String accesstoken = UtilsPreferences.getString(context, Constant.accesstoken);
            if (user_id != null && !user_id.isEmpty() && accesstoken != null && !accesstoken.isEmpty()) {
                try {
                    JSONObject data = new JSONObject();
                    data.put(Constant.user_id, UtilsPreferences.getString(context, Constant.user_id));
                    data.put(Constant.accesstoken, UtilsPreferences.getString(context, Constant.accesstoken));

                    Application.NotificationBadgeCountCall task = new Application.NotificationBadgeCountCall(context, data, false);
                    task.execute();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static class NotificationBadgeCountCall extends AsyncTask<String, Integer, String> {
        private Context context;
        Gson gson;

        JSONObject postData;
        private boolean isDialog = false;

        public NotificationBadgeCountCall(Context context, JSONObject data, boolean isDialog) {
            this.context = context;
            this.isDialog = isDialog;
            postData = data;
            gson = new Gson();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isDialog) {
                UtilsCommon.showProgressDialog(context);
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String str_response = null;
            try {
                String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.notification_badge_count_url);
                str_response = JSONParse.postJSON(url, postData);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return str_response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                NotificationBadgeCountModel notificationBadgeCountModel = new Gson().fromJson(result, NotificationBadgeCountModel.class);
                if (notificationBadgeCountModel.flag) {
                    UtilsPreferences.setInt(context, Constant.NotificationBadge, notificationBadgeCountModel.data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (isDialog) {
                    UtilsCommon.destroyProgressBar();
                }
            }
        }
    }
}
