package android.fcmNotification;

import android.util.Log;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.api.UpdateDeviceToken;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONObject;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        if (!refreshedToken.trim().isEmpty()) {
            UtilsPreferences.setString(getBaseContext(), Constant.devicegcmid, refreshedToken);
            Log.e("Refreshed token => ", refreshedToken);
            // globals.setGCM_DeviceToken(refreshedToken);
            //Debugger.debugE("FCMDEviceToken", refreshedToken);
            updateDeviceToken();
        }
    }

    // api call for device token update
    public void updateDeviceToken() {

        if (UtilsPreferences.getString(Application.getContext(), Constant.user_id) != null && !(UtilsPreferences.getString(Application.getContext(), Constant.user_id).isEmpty())) {
            try {
                JSONObject data = new JSONObject();
                data.put(Constant.user_id, UtilsPreferences.getString(Application.getContext(), Constant.user_id));
                data.put(Constant.device_token, UtilsPreferences.getString(Application.getContext(), Constant.devicegcmid));
                data.put(Constant.device_type, Constant.device_type_value);
                if (ConnectionDetector.internetCheck(Application.getContext()))
                    new UpdateDeviceToken(Application.getContext(), null, data, false).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
