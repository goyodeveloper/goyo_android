package android.fcmNotification;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.UtilsCommon;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;

public class NotificationUtils {

    private Context mContext;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }

    // generate notification

    /**
     * @param title   title
     * @param message text message
     * @param intent  intent redirect after click on notification
     */
    public void showNotificationMessage(final String title, String message, Intent intent) {
        if (TextUtils.isEmpty(message))
            return;

        final int icon = getSmallIcon();

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent resultPendingIntent = PendingIntent.getActivity(
                mContext,
                0,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);

//        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                + "://" + mContext.getPackageName()
//                + "/raw/notification");

        showSmallNotification(mBuilder, icon, title, message, resultPendingIntent);
        //playNotificationSound();
    }

    // generate notification

    /**
     * @param data   data for title and text and image link
     * @param intent intent redirect after click on notification
     */
    public void showChallengeNotificationMessage(final JSONObject data, final Intent intent) {
        if (data == null)
            return;

        try {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            final PendingIntent resultPendingIntent = PendingIntent.getActivity(
                    mContext,
                    0,
                    intent,
                    PendingIntent.FLAG_CANCEL_CURRENT);

            NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();

            String big_url = "";

            big_url = mContext.getResources().getString(R.string.server_url)
                    + mContext.getResources().getString(R.string.challenge_notification_image)
                    + data.getString(Constant.reminder_push_img);

            bigPictureStyle.bigPicture(getBitmapFromURL(big_url));

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),
                            R.mipmap.ic_launcher))
                    .setSmallIcon(getSmallIcon())
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle(data.getString(Constant.reminder_push_text))
                    .setContentText(data.getString(Constant.alert))
                    .setAutoCancel(true)
                    .setStyle(bigPictureStyle)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentIntent(resultPendingIntent);

            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(new Random().nextInt(), notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showChallengeNotificationMessage1(final JSONObject data, final Intent intent) {
        if (data == null)
            return;

//        final int icon = getSmallIcon();
//
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        final PendingIntent resultPendingIntent = PendingIntent.getActivity(
//                mContext,
//                0,
//                intent,
//                PendingIntent.FLAG_CANCEL_CURRENT);
//
//        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
////        bigPictureStyle.bigPicture(BitmapFactory.decodeResource(mContext.getResources(),
////                R.mipmap.ic_launcher));
//
//        String big_url = "";
//
//        try {
//            big_url = mContext.getResources().getString(R.string.server_url)
//                    + mContext.getResources().getString(R.string.challenge_notification_image)
//                    + data.getString(Constant.reminder_push_img);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        bigPictureStyle.bigPicture(getBitmapFromURL(big_url));
//
//
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
//                .setSmallIcon(icon)
//                .setContent(getComplexNotificationView(data))
//                .setAutoCancel(true)
//                .setContentIntent(resultPendingIntent)
//                .setStyle(bigPictureStyle)
//                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);
//
//
//        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(new Random().nextInt(), mBuilder.build());

        // Using RemoteViews to bind custom layouts into Notification
        final RemoteViews contentView = new RemoteViews(
                mContext.getPackageName(),
                R.layout.custom_notification_new_challenge
        );

        try {
            final String url = mContext.getResources().getString(R.string.server_url)
                    + mContext.getResources().getString(R.string.challenge_notification_image)
                    + data.getString(Constant.reminder_push_thumb_img);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    Glide.with(mContext)
                            .load(url)
                            .asBitmap()
                            .centerCrop()
                            .placeholder(R.mipmap.ic_launcher)
                            .transform(new CircleTransform(mContext))
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(final Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                contentView.setImageViewBitmap(R.id.img_notification_profile, resource);
                                                contentView.setTextViewText(R.id.tv_notification_title, data.getString(Constant.reminder_push_text));
                                                contentView.setTextViewText(R.id.tv_notification_summary_text, data.getString(Constant.alert));

                                                contentView.setTextViewText(R.id.tv_notification_time, UtilsCommon.getCurrentDateMonth());
                                                final int icon = getSmallIcon();

                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                final PendingIntent resultPendingIntent = PendingIntent.getActivity(
                                                        mContext,
                                                        0,
                                                        intent,
                                                        PendingIntent.FLAG_CANCEL_CURRENT);

                                                NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();

                                                String big_url = "";

                                                big_url = mContext.getResources().getString(R.string.server_url)
                                                        + mContext.getResources().getString(R.string.challenge_notification_image)
                                                        + data.getString(Constant.reminder_push_img);

                                                bigPictureStyle.bigPicture(getBitmapFromURL(big_url));

                                                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                                                        .setSmallIcon(icon)
                                                        .setContent(contentView)
                                                        .setAutoCancel(true)
                                                        .setContentIntent(resultPendingIntent)
                                                        .setStyle(bigPictureStyle)
                                                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);

                                                NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

                                                notificationManager.notify(new Random().nextInt(), mBuilder.build());
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();

                                }
                            });
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, PendingIntent resultPendingIntent) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        inboxStyle.addLine(message);

        Notification notification;

        /*notification = mBuilder
                .setColor(ContextCompat.getColor(mContext, R.color.app_theme))
                .setSmallIcon(icon)
//                .setTicker(title)
                .setWhen(0)
                .setAutoCancel(true)
//                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                //.setSound(alarmSound)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setStyle(inboxStyle)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .build();*/

        mBuilder.setColor(ContextCompat.getColor(mContext, R.color.app_theme));
        mBuilder.setSmallIcon(icon);
        mBuilder.setWhen(0);
        mBuilder.setAutoCancel(true);
        if (title != null && !title.isEmpty()) {
            mBuilder.setTicker(title);
            mBuilder.setContentTitle(title);
        }

        mBuilder.setContentIntent(resultPendingIntent);

        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);
        mBuilder.setStyle(inboxStyle);
        mBuilder.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher));
        mBuilder.setContentText(message);
        /*mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(message));*/

        notification = mBuilder.build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(new Random().nextInt(), notification);

    }

    private RemoteViews getComplexNotificationView(JSONObject data) {
        // Using RemoteViews to bind custom layouts into Notification
        final RemoteViews contentView = new RemoteViews(
                mContext.getPackageName(),
                R.layout.custom_notification_new_challenge
        );

        try {
            final String url = mContext.getResources().getString(R.string.server_url)
                    + mContext.getResources().getString(R.string.challenge_notification_image)
                    + data.getString(Constant.reminder_push_thumb_img);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    Glide.with(mContext)
                            .load(url)
                            .asBitmap()
                            .centerCrop()
                            .placeholder(R.mipmap.ic_launcher)
                            .transform(new CircleTransform(mContext))
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    contentView.setImageViewBitmap(R.id.img_notification_profile, resource);
                                }
                            });
                }
            });

//            contentView.setImageViewResource(R.id.img_notification_profile, R.mipmap.ic_launcher);

            contentView.setTextViewText(R.id.tv_notification_title, data.getString(Constant.reminder_push_text));
            contentView.setTextViewText(R.id.tv_notification_summary_text, data.getString(Constant.alert));
            contentView.setTextViewText(R.id.tv_notification_time, UtilsCommon.getCurrentDateMonth());
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        contentView.setImageViewResource(R.id.big_image, R.drawable.appbg);
        return contentView;
    }

    public int getSmallIcon() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //return R.drawable.ic_notification_trans;
            return R.drawable.push_notification_transperent;
        } else {
            return R.mipmap.ic_launcher;
        }
    }

    // Playing notification sound
    public void playNotificationSound() {
        try {
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(mContext, sound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CircleTransform extends BitmapTransformation {
        public CircleTransform(Context context) {
            super(context);
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            return circleCrop(pool, toTransform);
        }

        private Bitmap circleCrop(BitmapPool pool, Bitmap source) {
            if (source == null) return null;

            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            // TODO this could be acquired from the pool too
            Bitmap squared = Bitmap.createBitmap(source, x, y, size, size);

            Bitmap result = pool.get(size, size, Bitmap.Config.ARGB_8888);
            if (result == null) {
                result = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setShader(new BitmapShader(squared, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            return result;
        }

        @Override
        public String getId() {
            return getClass().getName();
        }
    }
}

