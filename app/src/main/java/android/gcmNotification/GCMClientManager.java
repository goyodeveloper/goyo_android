package android.gcmNotification;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;

import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.Debugger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class GCMClientManager {

    static String TAG = "GCMClientManager";
    private GoogleCloudMessaging gcm;
    private String regid;

    public GCMClientManager(GoogleCloudMessaging gcm) {
        this.gcm = gcm;
    }

    private Activity activity;

    public static abstract class RegistrationCompletedHandler {
        public abstract void onsuccess(String registrationId, boolean isNewRegistration);

        public void onFailure(String ex) {
            Debugger.debugE(TAG, ex);
        }
    }

    public GCMClientManager(Activity activity, String projectNumber) {
        this.activity = activity;
        this.gcm = GoogleCloudMessaging.getInstance(activity);
    }

    public void registerIfNeeded(final RegistrationCompletedHandler handler) {
        if (checkPlayService()) {
            regid = getRegistrationId(getContext());
            if (regid.isEmpty()) {
                registerInBackground(handler);
            } else {
                Debugger.debugE(TAG, regid);
                handler.onsuccess(regid, false);
            }
        } else {
            Debugger.debugE(TAG, "No valid Google Play Services APK found");
        }
    }

    private void registerInBackground(final RegistrationCompletedHandler handler) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getContext());
                    }
                    InstanceID instanceID = InstanceID.getInstance(getContext());
                    regid = instanceID.getToken(Constant.SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Debugger.debugE(TAG, regid);
                    storeRegistrationId(getContext(), regid);
                } catch (IOException e) {
                    handler.onFailure("Error: " + e.getMessage());
                }
                return regid;
            }

            @Override
            protected void onPostExecute(String regId) {
                if (regId != null) {
                    handler.onsuccess(regId, true);
                }
            }

        }.execute(null, null, null);
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(Constant.PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Debugger.debugE(TAG, "Registration not found");
            return "";
        }
        int registeredVersion = prefs.getInt(Constant.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Debugger.debugE(TAG, "App version changed");
            return "";
        }
        return registrationId;
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constant.PROPERTY_REG_ID, regId);
        editor.putInt(Constant.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            throw new RuntimeException("Could not find package name: " + e);
        }
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getContext().getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    private boolean checkPlayService() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil
                        .getErrorDialog(resultCode, getActivity(), Constant.PLAY_SERVICE_RESOLUTION_REQUEST).show();
            } else {
                Debugger.debugE(TAG, "This device is not supported");
            }
            return false;
        }
        return true;
    }

    private Context getContext() {
        return activity;
    }

    private Activity getActivity() {
        return activity;
    }
}
