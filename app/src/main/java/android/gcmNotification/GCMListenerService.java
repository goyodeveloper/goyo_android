package android.gcmNotification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.userProfile.SplashActivity;
import com.differenzsystem.goyog.utility.Debugger;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONObject;


public class GCMListenerService extends GcmListenerService {

    Context context;
    Bundle bundle;
    PendingIntent pendingIntent = null;
    int NOTIFICATION_ID = (int) System.currentTimeMillis();


    public GCMListenerService() {
        super();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        //String message = data.getString("Message");
        Debugger.debugE("DAta", data.getString(Constant.notificationdata));
        Debugger.debugE("Bundle", data.toString());
        Debugger.debugE("Alert", data.getString(Constant.alert));
        this.bundle = data;
        createNotification(from, data);
    }

    @Override
    public void onDeletedMessages() {
        Debugger.debugE(getClass().getName(), "Received deleted messages notification :");

        super.onDeletedMessages();
    }

    @Override
    public void onSendError(String msgId, String error) {
        super.onSendError(msgId, error);
    }

    @Override
    public void onMessageSent(String msgId) {
        super.onMessageSent(msgId);
        Debugger.debugE(getClass().getName(), "Message sent : " + msgId);
    }

    private void createNotification(String from, Bundle bundle) {
        try {
            context = getBaseContext();
            String message = bundle.getString(Constant.alert);
            String title = "GOYOG";

            Intent notificationIntent = new Intent(context, SplashActivity.class);
            notificationIntent.putExtra(Constant.fromNotificationFlag, true);
            notificationIntent.putExtras(bundle);

            pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setWhen(System.currentTimeMillis())
                    .setLargeIcon(largeIcon)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setTicker(message)
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                    .setAutoCancel(true);

            setSmallIcon(mBuilder);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Notification notification = mBuilder.build();
            mNotificationManager.notify(NOTIFICATION_ID, notification);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", "not able to create notification");
        }
    }

    public static void setSmallIcon(Builder mBuilder) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        } else {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
    }

}
