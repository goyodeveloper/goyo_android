package android.classes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.SectionIndexer;

import com.differenzsystem.goyog.R;

/**
 * Created by Union Assurance PLC on 02/05/16.
 */
public class SideSelector extends View {
    private static String TAG = SideSelector.class.getCanonicalName();

    public static char[] ALPHABET = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    public static final int BOTTOM_PADDING = 10;

    private SectionIndexer selectionIndexer = null;
    private ListView list;
    private Paint paint;
    private String[] sections;
    Context context;

    public SideSelector(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public SideSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SideSelector(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setBackgroundColor(getResources().getColor(R.color.transparent));
        paint = new Paint();
        paint.setColor(getResources().getColor(R.color.yellow_text));
        paint.setTextSize(25);
        paint.setTextAlign(Paint.Align.CENTER);
        Typeface type = Typeface.createFromAsset(getResources().getAssets(), "TitilliumWeb-Regular.ttf");
        paint.setTypeface(type);
    }

    public void setListView(ListView _list) {
        list = _list;
        selectionIndexer = (SectionIndexer) _list.getAdapter();

        Object[] sectionsArr = selectionIndexer.getSections();
        sections = new String[sectionsArr.length];
        for (int i = 0; i < sectionsArr.length; i++) {
            sections[i] = sectionsArr[i].toString();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        int y = (int) event.getY();
        float selectedIndex = ((float) y / (float) getPaddedHeight()) * ALPHABET.length;

        if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
            if (selectionIndexer == null) {
                selectionIndexer = (SectionIndexer) list.getAdapter();
            }
            int position = selectionIndexer.getPositionForSection((int) selectedIndex);
            if (position == -1) {
                return true;
            }
            list.setSelection(position);
        }
        return true;
    }

    protected void onDraw(Canvas canvas) {
        try {

            int viewHeight = getPaddedHeight();
            float charHeight = ((float) viewHeight) / (float) sections.length;

            float widthCenter = getMeasuredWidth() / 2;
            for (int i = 0; i < sections.length; i++) {
                canvas.drawText(String.valueOf(sections[i]), widthCenter, charHeight + (i * charHeight), paint);
            }
            super.onDraw(canvas);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getPaddedHeight() {
        return getHeight() - BOTTOM_PADDING;
    }
}

