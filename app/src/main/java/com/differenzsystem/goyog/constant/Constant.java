package com.differenzsystem.goyog.constant;

public class Constant {
    public static int getTimeInterval = 10;
    // Leveles
    public static final int level_steps = 1;
    public static final int level_calories = 2;
    public static final int level_distance = 3;
    public static final int level_sleep = 5;
    public static final int level_heart = 4;

    public static String AppLink = "https://play.google.com/store/apps/details?id=com.developer.goyo";
    public static final int MM_SplashTime = 1500;
    public static final String PREFERENCE_NAME = "PREFERENCE_NAME";
    public static final String access_token_value = "eeff4680106d8f9ce55a67b86649454ac34c4b74ea08639f1a0ba3b103161ffc";
    public static final String organization_id_value = "57addd93ff9d930009000025";
    public static final String TEMP_PHOTO_FILE_NAME = "TEMP_PHOTO_FILE_NAME";
    public static final String profile_image_url = "http://54.211.158.108/application/profile_image/";

    /*public static final String badge_image_url = "http://54.211.158.108/application/badges_image/";
    public static final String challanges_image_url = "http://54.211.158.108/application/challenge_images/";*/

    //public static final String badge_image_url = "http://54.211.158.108/v1/badges_image/";
    //public static final String challanges_image_url = "http://54.211.158.108/v1/challenge_images/";
    //public static final String offer_image_url = "http://54.211.158.108/v1/offer_image/";

    public static final String badge_image_url = "http://54.211.158.108/v2/badges_image/";
    public static final String challanges_image_url = "http://54.211.158.108/v2/challenge_images/";
    public static final String offer_image_url = "http://54.211.158.108/v2/offer_image/";


    public static final String GCM_server_api_key = "AIzaSyBqCh32xy3B3LIL0H3kErX7vzFo7nLeNAY";
    public static final String device_type_value = "2";

    public static final String hidemeon = "Your status is currently visible";
    public static final String hidemeoff = "Your status is currently hidden";

    public static String SENDER_ID = "142714561424";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    public static final int PLAY_SERVICE_RESOLUTION_REQUEST = 9000;
    public static String devicegcmid = "DeviceGcmId";
    public static String isdevicegcmidstore = "IsDeviceIdGcmStore";
    public static String alert = "alert";
    public static String title = "title";

    public static final String not_connected = "0";
    public static final String connected_with_validic = "1";
    public static final String connected_with_device = "2";
    public static final String connected_with_tracker = "3";
    public static final String connect_flag = "connect_flag";
    public static final String tutorial_flag = "tutorial_flag";

    public static final String name = "name";
    public static final String image = "image";
    public static final String description = "description";
    public static final String skill = "skill";
    public static final String level = "level";
    public static final String reward_hyperlink_text = "reward_hyperlink_text";
    public static final String reward_hyperlink = "reward_hyperlink";

    public static final String home = "home";
    public static final String challenges = "challenges";
    public static final String leaderboard = "leaderboard";
    public static final String profile = "profile";
    public static final String settings = "settings";
    public static final String Notification = "Notification";

    public static final String email = "email";
    public static final String password = "password";
    public static final String device_token = "device_token";
    public static final String device_type = "device_type";


    public static final String full_name = "full_name";
    public static final String profile_image = "profile_image";
    public static final String registration_date = "registration_date";
    public static final String last_update_date = "last_update_date";
    public static final String validic_id = "validic_id";
    public static final String validic_access_token = "validic_access_token";
    public static final String user_id = "user_id";
    public static final String accesstoken = "accesstoken";
    public static final String display_status = "display_status";
    public static final String uid = "uid";
    public static final String user = "user";
    public static final String access_token = "access_token";
    public static final String code = "code";
    public static final String challenge_id = "challenge_id";
    public static final String level_id = "level_id";
    public static final String challenge_status = "challenge_status";
    public static final String timezone = "timezone";
    public static final String date = "date";
    public static final String regdate = "regdate";
    public static final String complete_date = "complete_date";

    public static final String results = "results";
    public static final String summary = "summary";
    public static final String status = "status";
    public static final String fitness = "fitness";
    public static final String routine = "routine";
    public static final String calories_burned = "calories_burned";
    public static final String distance = "distance";
    public static final String step = "step";
    public static final String heart_rate = "heart_rate";
    public static final String sleep = "sleep";


    public static final String flag = "flag";
    public static final String message = "message";
    public static final String body = "body";
    public static final String data = "data";
    public static final String month = "month";
    public static final String time = "time";

    public static final String day = "day";
    public static final String hour = "hour";
    public static final String week = "week";
    public static final String dashboard = "dashboard";

    public static final String completed_challenges = "completed_challenges";
    public static final String completed_badges = "completed_badges";
    public static final String time_line_result = "time_line_result";

    public static final String process = "process";
    public static final String thisweek = "thisweek";
    public static final String lastweek = "lastweek";
    public static final String nextweek = "nextweek";

    public static final String ChallengeType_Inprogress = "In-Progress";
    public static final String ChallengeType_thisweek = "This Week";
    public static final String ChallengeType_lastweek = "Last Week";
    public static final String ChallengeType_nextweek = "Next Week";

    public static final String challenge_type = "challenge_type";
    public static final String notificationdata = "notificationdata";
    public static final String accept_date = "accept_date";
    public static final String challenge_start_date = "challenge_start_date";
    public static final String duration = "duration";
    public static final String fromNotificationFlag = "fromNotificationFlag";
    public static final String challenge_value = "challenge_value";
    public static final String second_challenge_value = "second_challenge_value";
    public static final String third_challenge_value = "third_challenge_value";
    public static final String fourth_challenge_value = "fourth_challenge_value";
    public static final String validic_result = "validic_result";

    public static final String isFromSetting = "isFromSetting";
    public static final String steps = "steps";
    public static final String calories = "calories";

    //registration
    public static final String emailreg = "emailreg";
    public static final String first_name = "first_name";
    public static final String last_name = "last_name";
    public static final String other_names = "other_names";
    public static final String address = "address";
    public static final String contact_number = "contact_number";
    public static final String dob = "dob";
    public static final String occupation = "occupation";
    public static final String marital_status = "marital_status";
    public static final String gender = "gender";
    public static final String nic = "nic";
    public static final String nominee_nic = "nominee_nic";
    public static final String nominee_name = "nominee_name";
    public static final String nominee_address = "nominee_address";
    public static final String nominee_number = "nominee_number";
    public static final String no_of_children = "no_of_children";
    public static final String facebook_id = "facebook_id";
    public static final String user_exp_date = "user_exp_date";

    public static final String height = "height";
    public static final String height_scale = "height_scale";
    public static final String weight = "weight";
    public static final String weight_scale = "weight_scale";
    public static final String led_visible_status = "led_visible_status";

    //FCM
    public static final int NOTIFICATION_ID = 100;
    //badge
    public static final String totalbadge = "totalbadge";

    //notification for new challange
    public static final String NotificationType = "notification_type";
    public static final String NewChallange = "1";
    public static final String periodical_notification = "2";//periodical_notification
    public static final String GOYOHRStatus = "GOYOHRStatus";
    public static final String Upcomming_Challanges = "4";
    public static final String Maximum_user = "5";
    public static final String win_offer_expier = "6";
    public static final String demo_user_nitification = "7";
    public static final String manually_win_nitification = "8";
    public static final String manually_lose_nitification = "9";
    public static final String is_won = "is_won";

    public static final String sync_band_request_notification = "11";

    //user_type
    public static final String user_type = "user_type";

    //postDelay
    public static final int PostDelayMain = 60000;
    public static final int PostDelaySub = 5000;

    public static final String fcmserverkey = "AIzaSyAM0BNqZn6Av77CpF_b6p8SV8iUBXIriY8";

    //Badge
    public static final String CahllangeBadge = "CahllangeBadge";
    public static final String ProfileBadge = "ProfileBadge";

    public static final String mac_id = "mac_id";

    public static final String notification = "notification";
    public static final String first_launch = "first_launch";
    //public static final String first_launch = "first_launch";
    public static final String Scan_Declain = "Scan_Declain";
    public static final String isInProgress = "isInProgress";
    public static final String isPerPopVisible = "isPerPopVisible";
    public static final String isSyncing = "isSyncing";

    //Initial reards

    public static final String InsPopupTitle = "title";
    public static final String InsPopupText = "text";
    public static final String InsPopupFlag = "flag";
    public static final String InsPopupCount = "count";
    public static final String initial_reward = "initial_reward";
    public static final String fb_education = "fb_education";
    public static final String fb_groups = "fb_groups";
    public static final String fb_events = "fb_events";
    public static final String fb_work_exp = "fb_work_exp";
    public static final String education = "education";
    public static final String school = "school";
    public static final String groups = "groups";
    public static final String events = "events";
    public static final String work = "work";
    public static final String employer = "employer";


    public static String Key_inProgressList = "inProgressList";
    public static String Key_level_info = "levelinfo";

    public static String last_sync_date = "last_sync_date";
    public static String male = "Male";
    public static String female = "Female";

    public static final String key_refresh = "bluetoothsyncrefresh";
    public static final String id = "id";
    public static final String key_challenge_data = "challenge_data";
    public static final String key_level_data = "level_data";
    public static final String key_next_level_object = "nextlevelobject";

    public static final String level_per = "level_per";
    public static final String level_min = "level_min";

    public static final String challenge_per = "challenge_per";
    public static final String challenge_min = "challenge_min";


    public static final String NotificationBadge = "NotificationBadge";
    public static String isFromNextLevelScreen = "isFromNextLevelScreen";

    public static boolean refreshLevelBluetoothOff = false;


    public static String option_id = "option_id";
    public static String filter_id = "filter_id";
    public static String tab_id = "tab_id";


    public static String filter_daily = "1";
    public static String filter_weekly = "2";
    public static String filter_monthly = "3";

    public static String option_activechallenge = "1";
    public static String option_activelevel = "2";
    public static String option_calorie = "3";
    public static String option_steps = "4";
    public static String option_distance = "5";

    public static String tab_all = "1";
    public static String tab_mycompany = "2";
    public static String reminder_push_text = "reminder_push_text";
    public static String reminder_push_thumb_img = "reminder_push_thumb_img";
    public static String reminder_push_img = "reminder_push_img";

    // firebase analytics screens names
    public static final String Name_Leaderboard = "Leaderboard Screen";
    public static final String Name_Login = "Login Screen";
    public static final String Name_Register = "Register Screen";
    public static final String Name_Reward = "Reward Screen";
    public static final String Name_Challenges = "Challenges Screen";
    public static final String Name_Setting = "Setting Screen";
    public static final String Name_Timeline = "Timeline Screen";
    public static final String Name_Dashboard = "Dashboard Screen";

    public static final String Name_Calories_Day_Wise = "Calories Day Wise Screen";
    public static final String Name_Calories_Week_Wise = "Calories Week Wise Screen";
    public static final String Name_Calories_Month_Wise = "Calories Month Wise Screen";

    public static final String Name_Steps_Day_Wise = "Steps Day Wise Screen";
    public static final String Name_Steps_Week_Wise = "Steps Week Wise Screen";
    public static final String Name_Steps_Month_Wise = "Steps Month Wise Screen";


    public static final String Name_Distance_Day_Wise = "Distance Day Wise Screen";
    public static final String Name_Distance_Week_Wise = "Distance Week Wise Screen";
    public static final String Name_Distance_Month_Wise = "Distance Month Wise Screen";

    public static final String Name_Sleep = "Sleep Screen";
    public static final String Name_HeartRate = "HeartRate Screen";

    public static final String Name_Next_Level_Detail = "Next Level Detail Screen";
    public static final String Name_Challenge_Detail = "Challenge Detail Screen";
    public static final String Name_Level_Detail = "Level Detail Screen";
    public static final String Name_InProcess_Challenge_Detail = "InProcess Challenge Detail Screen";
    public static final String Name_Notification = "Notification Screen";
    public static final String Name_Scan_GOYO_Band = "Scan GOYO Band Screen";
    public static final String Name_About = "About Screen";
    public static final String Name_Invite_Friends = "Invite Friends Screen";
    public static final String Name_Offer_Detail_QR = "Offer Detail QR Screen";
    public static final String Name_Won = "Won Screen";
    public static final String Name_Offer_Detail = "Offer Detail Screen";

    public static final String Name_Splash = "Splash Screen";
    public static final String Name_ForgotPassword = "ForgotPassword Screen";
    public static final String Name_Register_Weight = "Register Weight Screen";
    public static final String Name_Register_Height = "Register Height Screen";
    public static final String Name_Registration_Height_Weight = "Registration Height Weight Screen";
    public static final String Name_Registration_Step1 = "Registration Step 1 Screen";
    public static final String Name_Registration_Step2 = "Registration Step 2 Screen";
    public static final String Name_Registration_Step3 = "Registration Step 3 Screen";
    public static final String Name_Registration_Step4 = "Registration Step 4 Screen";
    public static final String Name_Registration_AllSet_or_PersonalDetail = "Personal Detail Screen";
    public static final String Name_Terms_Condition = "Terms and Conditions Screen";
    public static final String Name_Tutorial = "Tutorial Screen";
    public static final String Name_Registration_Selection = "Registration Selection Screen";

}
