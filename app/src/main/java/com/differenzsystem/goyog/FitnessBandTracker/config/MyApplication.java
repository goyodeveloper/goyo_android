package com.differenzsystem.goyog.FitnessBandTracker.config;

import android.app.Application;

import com.veryfit.multi.nativeprotocol.ProtocolUtils;

public class MyApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		ProtocolUtils.getInstance().init(this);
	}
}
