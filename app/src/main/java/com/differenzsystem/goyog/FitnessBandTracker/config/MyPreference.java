package com.differenzsystem.goyog.FitnessBandTracker.config;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPreference {
	private static final String SAVE_NAME = "UserSaveName";
	private static MyPreference instance = null;
	private Context mContext = null;
	private static SharedPreferences settings = null;
	private static SharedPreferences.Editor editor = null;

	public static final String isFirst = "ISFIRST";//
	
	

	private MyPreference(Context context) {
		mContext = context;
	}
	
	public void clear(){
		editor.clear();
	}

	public static MyPreference getInstance(Context context) {
		if (instance == null) {
			instance = new MyPreference(context);
			settings = context.getSharedPreferences(SAVE_NAME, 0);
			editor = settings.edit();
		}
		return instance;
	}
    public void setIsFirst(Boolean value) {
    	editor.putBoolean(isFirst, value);
    	editor.commit();
    }
    
    public boolean getIsFirst() {
    	return settings.getBoolean(isFirst, false);
    }
    
  
}
