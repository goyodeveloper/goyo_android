package com.differenzsystem.goyog.FitnessBandTracker;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.adapter.ScanAdapter;
import com.differenzsystem.goyog.FitnessBandTracker.config.MyPreference;
import com.differenzsystem.goyog.FitnessBandTracker.view.BufferDialog;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.CheckMacId;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.userProfile.RegistrationActivity_1;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsPermission;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.veryfit.multi.entity.BleDevice;
import com.veryfit.multi.nativedatabase.BasicInfos;
import com.veryfit.multi.nativeprotocol.Protocol;
import com.veryfit.multi.nativeprotocol.ProtocolEvt;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;
import com.veryfit.multi.util.DebugLog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

public class ScanDeviceActivity extends BaseActivity implements OnClickListener, CheckMacId.OnCheckMacIdListener {
    LinearLayout ll_back, ll_logout, ll_scan;
    TextView tv_title, tv_devicecount, tv_back;
    private ListView lView;
    private Button btnScan, btnBind;
    private ScanAdapter adapter;
    private Handler mHandler = new Handler();
    private Handler mHandler1 = new Handler();
    private MyPreference pref;
    private ArrayList<BleDevice> list = new ArrayList<BleDevice>();
    private int index;
    private BufferDialog mBufferDialog;
    String isFrom;
    CheckMacId.OnCheckMacIdListener onCheckMacIdListener;
    String mac;
    BluetoothAdapter bluetooth;
    String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_device);
        getSupportActionBar().hide();
        UtilsPermission.checkFineLocation(ScanDeviceActivity.this);

        bluetooth = BluetoothAdapter.getDefaultAdapter();
        provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);


        initView();
        addListener();
    }

    // here check bluetooth is enabled or not
    public void CheckforBlutooth() {
        if (!bluetooth.isEnabled()) {//if blutooth is disabled
            Toast.makeText(this, "" + getString(R.string.bluetooth_turn_on), Toast.LENGTH_SHORT).show();
        }

        /*if (!provider.contains("gps")) { //if gps is disabled
            Toast.makeText(this, "Please turn ON your location", Toast.LENGTH_SHORT).show();
        }*/
    }

    public static boolean isBluetoothAvailable() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return (bluetoothAdapter != null && bluetoothAdapter.isEnabled());
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        unbundling();//test it works or not
        mBufferDialog.dismiss();
        refreshList();
        ProtocolUtils.getInstance().scanDevices(true);

        Application.recordScreenViews(ScanDeviceActivity.this, Constant.Name_Scan_GOYO_Band);
        CheckforBlutooth();
    }

    // initialize all controls define in xml layout
    public void initView() {
        pref = MyPreference.getInstance(this);
        mBufferDialog = new BufferDialog(this);
        lView = (ListView) findViewById(R.id.lv_device);
        btnBind = (Button) findViewById(R.id.btn_bind);
        btnScan = (Button) findViewById(R.id.btn_scan);

        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_back.setVisibility(View.VISIBLE);
        ll_logout = (LinearLayout) findViewById(R.id.ll_logout);
        ll_logout.setVisibility(View.INVISIBLE);
        ll_scan = (LinearLayout) findViewById(R.id.ll_scan);
        ll_scan.setVisibility(View.VISIBLE);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_devicecount = (TextView) findViewById(R.id.tv_devicecount);
        tv_back = (TextView) findViewById(R.id.tv_back);

        tv_title.setText(getString(R.string.looking_lbl));

        adapter = new ScanAdapter(this, null);
        lView.setAdapter(adapter);
        lView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                index = arg2;
                adapter.setSelectItem(arg2);
                DebugLog.d("bind");
                mBufferDialog.show();
                ProtocolUtils.getInstance().connect(list.get(index).mDeviceAddress);
            }
        });

        if (getIntent() != null) {
            Intent i = getIntent();
            isFrom = i.getStringExtra("isFrom");

            if (isFrom.equalsIgnoreCase("HomeFragment")) {
                tv_back.setText(getString(R.string.skip_lbl));
            }
        }
        onCheckMacIdListener = this;
    }

    // set click event listener
    public void addListener() {
        btnBind.setOnClickListener(this);
        btnScan.setOnClickListener(this);
        ll_scan.setOnClickListener(this);
        ll_back.setOnClickListener(this);
    }

    // handle click event
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btn_bind:
                DebugLog.d("bind");
                mBufferDialog.show();
                ProtocolUtils.getInstance().connect(list.get(index).mDeviceAddress);
                break;
            case R.id.btn_scan:
                list.clear();
                adapter.clear();
                ProtocolUtils.getInstance().scanDevices();
                break;
            case R.id.ll_scan:
                list.clear();
                adapter.clear();
                tv_devicecount.setText("");
                ProtocolUtils.getInstance().scanDevices();
                break;
            case R.id.ll_back:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    //Discovery
    @Override
    public void onFind(BleDevice device) {
        // TODO Auto-generated method stub
        super.onFind(device);
        showList(device);
        list.add(device);
        Collections.sort(list);
        tv_devicecount.setText(getString(R.string.device_count_lbl1) + " " + String.valueOf(list.size()) + " " + getString(R.string.device_count_lbl2));
    }

    //End of the scan
    @Override
    public void onFinish() {
        // TODO Auto-generated method stub
        super.onFinish();
        DebugLog.d("onFinish");
    }

    private void showList(final BleDevice device) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                adapter.upDada(device);
            }
        });
    }

    @Override
    public void onServiceDiscover(BluetoothGatt gatt, int status) {
        // TODO Auto-generated method stub
        super.onServiceDiscover(gatt, status);
        ProtocolUtils.getInstance().setBind();
    }

    @Override
    public void onSysEvt(int evt_base, int evt_type, int error, int value) {
        // TODO Auto-generated method stub
        super.onSysEvt(evt_base, evt_type, error, value);
        if (evt_type == ProtocolEvt.BIND_CMD_REQUEST.toIndex() && error == ProtocolEvt.SUCCESS) {
            pref.setIsFirst(true);
            mHandler1.post(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    ProtocolUtils.getInstance().setBindMode(Protocol.SYS_MODE_SET_BIND);
                    ProtocolUtils.getInstance().StartSyncConfigInfo();

                    //getBandID("F4:96:54:DB:43:5E");

                }
            });
        } else if (evt_type == ProtocolEvt.SYNC_EVT_CONFIG_SYNC_COMPLETE.toIndex() && error == ProtocolEvt.SUCCESS) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mBufferDialog != null) {
                        ProtocolUtils.getInstance().setUserinfo("test", 1990, 1, 1, 60 * 100, 5, 0); // Commented on 22 - 07 - 2017 to avoid overwrite of userInfos
                    }
                    if (Globals.homeDeviceMap.size() > 0)
                        Globals.homeDeviceMap.clear();

                    if (mac != null && mac.length() > 0) {
                        getBandID(mac);
                    }

                    //Toast.makeText(ScanDeviceActivity.this, "Synchronization configuration information successfully", Toast.LENGTH_LONG).show();
                }
            }, 200);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    @Override
    public void onDeviceInfo(final BasicInfos arg0) {
// TODO Auto-generated method stub
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mac = arg0.getMacAddress();
                UtilsPreferences.setString(getApplicationContext(), Constant.mac_id, mac);
                //getBandID(arg0.getMacAddress());
                //  tvDeviceData.setText("basicInfo：\n\n"+arg0.toString());
            }
        });
    }

    public void unbundling() {
        if (BleSharedPreferences.getInstance().getIsBind()) {
            if (ProtocolUtils.getInstance().isAvailable() != ProtocolUtils.SUCCESS) {//To determine whether a device is connected
                ProtocolUtils.getInstance().setBindMode(Protocol.SYS_MODE_SET_NOBIND);
                Calendar mCalendar1 = Calendar.getInstance();
                int year = mCalendar1.get(Calendar.YEAR);
                int month = mCalendar1.get(Calendar.MONTH);
                int day = mCalendar1.get(Calendar.DAY_OF_MONTH);
                ProtocolUtils.getInstance().enforceUnBind(new Date(year, month, day));
            } else {
                //If the device is connected, use the following method to solve tie
                ProtocolUtils.getInstance().setBindMode(Protocol.SYS_MODE_SET_NOBIND);
                ProtocolUtils.getInstance().setUnBind();
            }
            UtilsPreferences.setString(getApplicationContext(), Constant.GOYOHRStatus, "Unbundled");
            // Toast.makeText(getApplicationContext(), "Unpaired Successfully..", Toast.LENGTH_SHORT).show();
            //refreshList();
        }
    }

    // api call for CheckMacId
    public void getBandID(String macID) {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.mac_id, macID);
            String userID = UtilsPreferences.getString(getApplicationContext(), Constant.user_id);
            if (userID != null && userID.length() > 0) {
                data.put(Constant.user_id, userID);
                data.put(Constant.accesstoken, UtilsPreferences.getString(this, Constant.accesstoken));
            } else {
                data.put(Constant.user_id, 0);
            }

            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                CheckMacId task = new CheckMacId(getApplicationContext(), onCheckMacIdListener, data);
                task.execute();
            } else {
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // display alert dialog for already paired band
    public void openPopupDialog() {
        final Dialog alertDialog = new Dialog(ScanDeviceActivity.this, android.R.style.Theme_Light);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.chek_mac_popup);
        final LinearLayout ll_back;
        final Button btn_okay;

        ll_back = (LinearLayout) alertDialog.findViewById(R.id.ll_back);
        tv_title = (TextView) alertDialog.findViewById(R.id.tv_title);
        tv_title.setText("Oops...");

        btn_okay = (Button) alertDialog.findViewById(R.id.btn_okay);
        btn_okay.setTypeface(setFont(getApplicationContext(), R.string.app_bold));//.setTypeface(setFont(getContext(), R.string.app_bold));

        ll_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btn_okay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    // success listener of CheckMacId
    @Override
    public void onSucceedCheckMacId(String message) {
        mBufferDialog.dismiss();
        UtilsPreferences.setString(getApplicationContext(), Constant.GOYOHRStatus, "Bundled");
        if (isFrom != null && isFrom.length() > 0) {
            if (isFrom.equalsIgnoreCase("RegistrationSelectionActivity")) {//|| isFrom.equalsIgnoreCase("SettingsFragment")
                /*Intent intent = new Intent(ScanDeviceActivity.this, RegistrationActivity_1.class);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);*/
                openPopupDialog1();
            } else if (isFrom.equalsIgnoreCase("SettingsFragment")) {
                if (UtilsPreferences.getString(getApplicationContext(), Constant.user_type).equalsIgnoreCase("2")) {
                    openPopupDialog1();
                } else {
                    intentDashBoard();
                }
            } else {
                intentDashBoard();
            }
        } else {
            intentDashBoard();
        }
    }

    public void intentDashBoard() {
        Intent intent = new Intent(ScanDeviceActivity.this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


    @Override
    public void onFailedCheckMacId(String message) {
        mBufferDialog.dismiss();
        openPopupDialog();
        unbundling();
        refreshList();

    }

    public void openPopupDialog1() {
        final Dialog alertDialog = new Dialog(ScanDeviceActivity.this, android.R.style.Theme_Light);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.chek_mac_popup);
        final LinearLayout ll_back;
        final Button btn_okay;
        final TextView tv_desc;

        ll_back = (LinearLayout) alertDialog.findViewById(R.id.ll_back);
        tv_title = (TextView) alertDialog.findViewById(R.id.tv_title);
        tv_desc = (TextView) alertDialog.findViewById(R.id.tv_desc);
        tv_desc.setMovementMethod(new ScrollingMovementMethod());

        tv_desc.setText(R.string.step_5);
        tv_title.setText(R.string.step_5_title);
        ll_back.setVisibility(View.INVISIBLE);

        btn_okay = (Button) alertDialog.findViewById(R.id.btn_okay);
        btn_okay.setText(R.string.got_it);
        btn_okay.setTypeface(setFont(getApplicationContext(), R.string.app_bold));//.setTypeface(setFont(getContext(), R.string.app_bold));

        btn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScanDeviceActivity.this, RegistrationActivity_1.class);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public void refreshList() {
        list.clear();//for testing only
        adapter.clear();
        tv_devicecount.setText("");
    }
}
