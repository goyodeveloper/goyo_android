package com.differenzsystem.goyog.FitnessBandTracker;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.veryfit.multi.ble.ProtocalCallBack;
import com.veryfit.multi.entity.SportData;
import com.veryfit.multi.entity.SwitchDataAppEndReply;
import com.veryfit.multi.entity.SwitchDataAppIngReply;
import com.veryfit.multi.entity.SwitchDataAppStartReply;
import com.veryfit.multi.entity.SwitchDataBleEnd;
import com.veryfit.multi.entity.SwitchDataBleIng;
import com.veryfit.multi.entity.SwitchDataBleStart;
import com.veryfit.multi.nativedatabase.BasicInfos;
import com.veryfit.multi.nativedatabase.FunctionInfos;
import com.veryfit.multi.nativedatabase.GsensorParam;
import com.veryfit.multi.nativedatabase.HealthHeartRate;
import com.veryfit.multi.nativedatabase.HealthHeartRateAndItems;
import com.veryfit.multi.nativedatabase.HealthSport;
import com.veryfit.multi.nativedatabase.HealthSportAndItems;
import com.veryfit.multi.nativedatabase.HrSensorParam;
import com.veryfit.multi.nativedatabase.RealTimeHealthData;
import com.veryfit.multi.nativedatabase.healthSleep;
import com.veryfit.multi.nativedatabase.healthSleepAndItems;
import com.veryfit.multi.nativeprotocol.ProtocolEvt;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;

/**
 * Created by Union Assurance PLC on 16/11/16.
 */

public class syncdevice implements ProtocalCallBack {
    Onsyncdevice listener;
    private Context context;

    public syncdevice(Context context, Onsyncdevice listener) {
        this.context = context;
        this.listener = listener;
        ProtocolUtils.getInstance().setProtocalCallBack(this);
    }

    public interface Onsyncdevice {
        public void onSucceedsyncdevice();

        //public void onFailedsyncdevice();
    }

    public void syncdata() {

        ProtocolUtils.getInstance().StartSyncHealthData();
    }

    private Handler mHandler = new Handler();

    @Override
    public void onWriteDataToBle(byte[] bytes) {

    }

    @Override
    public void onDeviceInfo(BasicInfos basicInfos) {

    }

    @Override
    public void healthData(byte[] bytes) {

    }

    @Override
    public void onSensorData(byte[] bytes) {

    }

    @Override
    public void onFuncTable(FunctionInfos functionInfos) {

    }

    @Override
    public void onSleepData(healthSleep healthSleep, healthSleepAndItems healthSleepAndItems) {

    }

    @Override
    public void onHealthSport(final HealthSport arg0, HealthSportAndItems arg1) {
        if (arg0 != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("HealthSportData =>", String.valueOf(arg0.getTotalCalory()) + String.valueOf(arg0.getTotalDistance()) + String.valueOf(arg0.getTotalStepCount()));

                }
            }, 200);
        }
    }

    @Override
    public void onHealthHeartRate(final HealthHeartRate arg0, HealthHeartRateAndItems arg1) {
        if (arg0 != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            }, 200);
        }
    }

    @Override
    public void onLiveData(RealTimeHealthData realTimeHealthData) {

    }

    @Override
    public void onGsensorParam(GsensorParam gsensorParam) {

    }

    @Override
    public void onHrSensorParam(HrSensorParam hrSensorParam) {

    }

    @Override
    public void onMacAddr(byte[] bytes) {

    }

    @Override
    public void onSysEvt(int evt_base, final int evt_type, int error, final int value) {
        // TODO Auto-generated method stub
        if (value > 0) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            }, 200);
        } else if (evt_type == ProtocolEvt.SYNC_EVT_HEALTH_SYNC_COMPLETE.toIndex()) {//Synchronization is complete
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    listener.onSucceedsyncdevice();
                }
            }, 200);
        }
    }

    @Override
    public void onLogData(byte[] bytes, boolean b) {

    }

    @Override
    public void onSwitchDataAppStart(SwitchDataAppStartReply switchDataAppStartReply, int i) {

    }

    @Override
    public void onSwitchDataAppIng(SwitchDataAppIngReply switchDataAppIngReply, int i) {

    }

    @Override
    public void onSwitchDataAppEnd(SwitchDataAppEndReply switchDataAppEndReply, int i) {

    }

    @Override
    public void onSwitchDataBleStart(SwitchDataBleStart switchDataBleStart, int i) {

    }

    @Override
    public void onSwitchDataBleIng(SwitchDataBleIng switchDataBleIng, int i) {

    }

    @Override
    public void onSwitchDataBleEnd(SwitchDataBleEnd switchDataBleEnd, int i) {

    }

    @Override
    public void onActivityData(SportData sportData, int i) {

    }


}
