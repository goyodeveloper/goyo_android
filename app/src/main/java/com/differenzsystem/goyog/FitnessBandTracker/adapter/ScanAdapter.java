package com.differenzsystem.goyog.FitnessBandTracker.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.differenzsystem.goyog.R;
import com.veryfit.multi.entity.BleDevice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScanAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<BleDevice> entities;
    private int selectItem = -1;
    private Context context;

    public ScanAdapter(Context context, List<BleDevice> refCourse) {
        setEntities(refCourse);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public void setEntities(List<BleDevice> entities) {
        if (entities != null) {
            this.entities = entities;
        } else {
            this.entities = new ArrayList<BleDevice>();
        }

    }

    public void upDada(BleDevice device) {
        this.entities.add(device);
        Collections.sort(entities);
        this.notifyDataSetChanged();
    }

    public void clear() {
        this.entities.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return entities.size();
    }


    public void setSelectItem(int item) {
        this.selectItem = item;
        notifyDataSetChanged();
    }

    @Override
    public BleDevice getItem(int index) {
        // TODO Auto-generated method stub
        return entities.get(index);
    }

    @Override
    public long getItemId(int index) {
        // TODO Auto-generated method stub
        return index;
    }

    @SuppressLint("NewApi")
    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_device, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_basic_name);
            holder.tvMac = (TextView) convertView.findViewById(R.id.tv_basic_mac);
            holder.view = (View) convertView.findViewById(R.id.view_select);
            holder.tv_basic_number = (TextView) convertView.findViewById(R.id.tv_basic_number);
            holder.img = (ImageView) convertView.findViewById(R.id.img);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final BleDevice course = entities.get(position);
        holder.tv_basic_number.setText(String.valueOf(position + 1) + ".");
        int strength = Math.abs(course.mRssi);

        if (strength >= 0 && strength <= 70) {//near by
            holder.tvMac.setTextColor(Color.parseColor("#CDFF00"));
            holder.img.setImageResource(R.drawable.green);
            holder.tvMac.setText(context.getString(R.string.nearby_lbl));
        } else if (strength >= 71 && strength <= 120) { //away
            holder.tvMac.setTextColor(Color.parseColor("#FFD300"));
            holder.img.setImageResource(R.drawable.yello);
            holder.tvMac.setText(context.getString(R.string.away_lbl));
        } else {//far away
            holder.tvMac.setTextColor(Color.parseColor("#FF5400"));
            holder.img.setImageResource(R.drawable.red);
            holder.tvMac.setText(context.getString(R.string.far_away_lbl));
        }
        // holder.tvName.setText(course.mDeviceName);
        //holder.tvMac.setText(course.mDeviceAddress);
        if (selectItem == position) {
            //holder.view.setVisibility(View.VISIBLE);
        } else {
            //holder.view.setVisibility(View.GONE);
        }
        return convertView;
    }

    class ViewHolder {
        private TextView tvName, tv_basic_number;
        private TextView tvMac;
        private View view;
        ImageView img;

    }
}
