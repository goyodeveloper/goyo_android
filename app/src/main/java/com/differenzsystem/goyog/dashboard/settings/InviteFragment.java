package com.differenzsystem.goyog.dashboard.settings;

import android.classes.SideSelector;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.adapter.InviteAdapter;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.InviteModel;
import com.differenzsystem.goyog.utility.Debugger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */
public class InviteFragment extends Fragment implements View.OnClickListener, TextWatcher {
    LinearLayout ll_back;
    TextView tv_title;
    ListView lv_inviteContact;
    SideSelector sideSelector;
    EditText et_search;

    InviteAdapter contactAdapter;
    SimpleSectionAdapter<String> sectionAdapter;
    ArrayList<InviteModel> myContactsForEmail = new ArrayList<InviteModel>();

    // create the new instance of invite screen
    public static InviteFragment newInstance(Bundle extra) {
        InviteFragment fragment = new InviteFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.invite_fragment, container, false);
        initializeControls(view);
        initializeControlsAction();
        getNameEmailDetails();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Invite_Friends);
    }

    // initialize all controls define in xml layout
    public void initializeControls(View view) {
        try {
            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_title.setText(R.string.invite);
            et_search = (EditText) view.findViewById(R.id.et_search);
            et_search.setTypeface(setFont(getActivity(), R.string.app_regular));

            lv_inviteContact = (ListView) view.findViewById(R.id.lv_inviteContact);
            sideSelector = (SideSelector) view.findViewById(R.id.list_index);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
            et_search.addTextChangedListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event listener
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;

            default:
                break;
        }
    }

    // initialize and set the adapter
    public void setSectionAdapter(InviteAdapter contactAdapter) {
        this.contactAdapter = contactAdapter;
        // 3. Create your adapter
        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                R.layout.invite_colleagues_list_item, R.id.tv_contact_name, contacts);*/

        // 4. Create a Sectionizer
        Sectionizer<InviteModel> alphabetSectionizer = new Sectionizer<InviteModel>() {
            @Override
            public String getSectionTitleForItem(InviteModel name) {
                return name.getContactName().substring(0, 1).toUpperCase();
            }
        };

        // 5. Wrap your adapter within the SimpleSectionAdapter
        sectionAdapter = new SimpleSectionAdapter<String>(getContext(),
                contactAdapter, R.layout.row_list_section_invite, R.id.tv_section_name, alphabetSectionizer);

        // 6. Set the adapter to your ListView
        lv_inviteContact.setAdapter(sectionAdapter);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        contactAdapter.filter(String.valueOf(editable).toLowerCase());
        setContactsAdapter();
    }

    // initialize and load the adapter for contact list
    public void setContactsAdapter() {
        if (myContactsForEmail != null) {
            if (contactAdapter == null) {
                contactAdapter = new InviteAdapter(getActivity(), myContactsForEmail);
            }
            contactAdapter.doRefresh(myContactsForEmail);
            setSectionAdapter(contactAdapter);
        }
    }

    // get the list of all contacts from device and then sorting of contact list by alphabets
    public void getNameEmailDetails() {
        try {
            if (myContactsForEmail.size() > 0) {
            } else {
                ArrayList<String> NamesList = new ArrayList<String>();
                ArrayList<String> contactsAndEmailsList = new ArrayList<String>();

                ContentResolver contentResolver = getActivity().getContentResolver();

                String[] PROJECTION = new String[]{ContactsContract.RawContacts._ID,
                        ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts.PHOTO_ID,
                        ContactsContract.CommonDataKinds.Email.DATA,
                        ContactsContract.CommonDataKinds.Photo.CONTACT_ID};
                String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
                String order = "CASE WHEN " + ContactsContract.Contacts.DISPLAY_NAME + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                        + ContactsContract.Contacts.DISPLAY_NAME + ", " + ContactsContract.CommonDataKinds.Email.DATA
                        + " COLLATE NOCASE";

                Cursor cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                if (cursor.moveToFirst()) {
                    do {
                        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String newNumber = number.trim().replaceAll("[-*?()]", "");

                        Log.e("Name", name + "=>");

                        /*String email = cursor.getString(3);
                        email = email.replace(" ", "");
                        email = email.toLowerCase().toString();*/

                        if (name != null && !name.equals("")) {
                            if (newNumber.length() > 8) {
                                if (!contactsAndEmailsList.contains(newNumber)) {
                                    NamesList.add(name);
                                    contactsAndEmailsList.add(newNumber);
                                    InviteModel obj = new InviteModel(name, newNumber, true, false, false);
                                    myContactsForEmail.add(obj);
                                }
                            }
                        }
                    } while (cursor.moveToNext());
                }
                Debugger.debugE("Total contact", myContactsForEmail.size() + " =>");
                cursor.close();

                //////////////
                Collections.sort(myContactsForEmail, new Comparator<InviteModel>() {
                    @Override
                    public int compare(InviteModel obj1, InviteModel obj2) {
                        return (obj1.getContactName()).compareTo(obj2.getContactName());
                    }
                });


                contactAdapter = new InviteAdapter(getActivity(), myContactsForEmail);
                setSectionAdapter(contactAdapter);
                sideSelector.setListView(lv_inviteContact);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
