package com.differenzsystem.goyog.dashboard.home.sleep;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class SleepFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_back;
    TextView tv_title;

    ViewPager viewPager;
    SleepDayWiseFragment sleepDayWiseFragment;

    // create the new instance of SleepFragment
    public static SleepFragment newInstance(Bundle extra) {
        SleepFragment fragment = new SleepFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sleep_fragment, container, false);

        initializeControls(view);
        initializeControlsAction();
        return view;
    }

    // initialize all controls define in xml layout

    /**
     * @param view get the layout view
     */
    public void initializeControls(View view) {
        try {
            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_title.setText(getString(R.string.sleep_lbl));

            sleepDayWiseFragment = SleepDayWiseFragment.newInstance(null);

            viewPager = (ViewPager) view.findViewById(R.id.viewPager);
            viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click listener
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;

            default:
                break;
        }
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return sleepDayWiseFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 1;
        }
    }

    // disconnect GoogleApiClient
    @Override
    public void onStop() {
        super.onStop();
        if (HomeFragment.mClient != null && HomeFragment.mClient.isConnected()) {
            HomeFragment.mClient.stopAutoManage(getActivity());
            HomeFragment.mClient.disconnect();
        }
    }
}
