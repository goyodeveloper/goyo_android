package com.differenzsystem.goyog.dashboard.home.step;

import android.classes.textcounter.CounterView;
import android.classes.textcounter.Formatter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.syncdevice;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall.OnGetRoutineFromValidicListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.model.CategoryModel;
import com.differenzsystem.goyog.model.ValidicRoutineModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.veryfit.multi.nativedatabase.HealthSport;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static java.text.DateFormat.getTimeInstance;

public class StepMonthWiseFragment extends Fragment implements OnGetRoutineFromValidicListener, SwipeRefreshLayout.OnRefreshListener, syncdevice.Onsyncdevice {
    View rootview;
    CombinedChart mChart;
    TextView tv_step, tv_date, tv_week1, tv_week2, tv_week3, tv_week4, tv_week5;
    CounterView tv_total_step;
    LinearLayout lin_dayView;
    ArrayList<HashMap<String, String>> whole_lst_month_calory;
    ArrayList<HashMap<String, String>> lst_month_step;
    OnGetRoutineFromValidicListener onGetRoutineFromValidicListener;
    int total_step = 0, callCount = 0;
    public GregorianCalendar cal_month;
    long statTime = 0;
    long endTime = 0;
    String stepsData = "0";
    public static String TAG = "StepMonthWiseFragment";
    List<String> weekDay = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    syncdevice.Onsyncdevice onsyncdevice;

    public StepMonthWiseFragment() {
        // Required empty public constructor
    }

    // create new instance of StepMonthWiseFragment
    public static StepMonthWiseFragment newInstance(Bundle bundle) {
        StepMonthWiseFragment fragment = new StepMonthWiseFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        onsyncdevice = this;
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.step_month_wise_fragment, container, false);
            initcomopnets(rootview);
        } else
            container.removeView(rootview);
        return rootview;

    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Steps_Month_Wise);
    }

    // initialize all controls define in xml layout

    /**
     * @param rootview layout view
     */
    private void initcomopnets(View rootview) {
        try {
            swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.swipe_view);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(
                    R.color.colorPrimary);
            mChart = (CombinedChart) rootview.findViewById(R.id.chart_calories);

            lin_dayView = (LinearLayout) rootview.findViewById(R.id.lin_dayView);
            lst_month_step = new ArrayList<>();
            whole_lst_month_calory = new ArrayList<>();
            tv_step = (TextView) rootview.findViewById(R.id.tv_steps);
            tv_date = (TextView) rootview.findViewById(R.id.tv_date);
            tv_total_step = (CounterView) rootview.findViewById(R.id.tv_total_step);
            tv_week1 = (TextView) rootview.findViewById(R.id.tv_week1);
            tv_week2 = (TextView) rootview.findViewById(R.id.tv_week2);
            tv_week3 = (TextView) rootview.findViewById(R.id.tv_week3);
            tv_week4 = (TextView) rootview.findViewById(R.id.tv_week4);
            tv_week5 = (TextView) rootview.findViewById(R.id.tv_week5);

            cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
            onGetRoutineFromValidicListener = this;

            Log.e("global step month data", String.valueOf(Globals.StepMonthMap));

            setCurrentWeekColor();

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                //if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                //    getMonthwiseData();
                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    if (BleSharedPreferences.getInstance().getIsBind()) {
                        getMonthDataCustomize();
                    } else {
                        swipeRefreshLayout.setEnabled(false);
                        setupDemoUserMonth();
                    }
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                    //TODO:get Date from device
                    getDeviceStepData();
                }
            } else {
                swipeRefreshLayout.setEnabled(false);
                setupDemoUserMonth();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // setup data for demo user
    private void setupDemoUserMonth() {
        int weekNo1 = DashboardActivity.mCurrentweek;
        for (int i = 0; i < 5; i++) {
            HashMap<String, String> temp = new HashMap<>();
            temp.put(Constant.time, new Date().toString());
            int sum = 0;
            if (i < weekNo1) {
                temp.put(Constant.step, "0");
                lst_month_step.add(temp);
            } else {
                break;
            }
        }
        setDataWithCounter(tv_total_step, 0);
        UtilsCommon.destroyProgressBar();
        monthWiseGraph(mChart, tv_step, tv_date);

    }

    public void getMonthData() {
        try {
            int w1 = 0, w2 = 0, w3 = 0, w4 = 0, w5 = 0;

            Calendar mCalendar = Calendar.getInstance();
                   /* int year = mCalendar.get(Calendar.YEAR);
                    int month=mCalendar.get(Calendar.MONTH);*/
            int Today = mCalendar.get(Calendar.DAY_OF_MONTH);

            int year = 0, month = 0, day = 0;
            List<HealthSport> monthHealthSprort = ProtocolUtils.getInstance().getMonthHealthSprort(0);
            if (monthHealthSprort != null && monthHealthSprort.size() > 0) {
                int weekNo1 = DashboardActivity.mCurrentweek;
                for (int i = 1; i <= 5; i++) {
                    if (i <= weekNo1) {
                        Date startDate = monthHealthSprort.get(0).getDate();
                        int totalcal = 0;
                        HashMap<String, String> temp = new HashMap<>();

                        for (int ii = 0; ii < monthHealthSprort.size(); ii++) {
                            temp.put(Constant.time, UtilsCommon.ConvertStringIntoDateWeekName(startDate.toString()));
                            year = monthHealthSprort.get(i).getYear();
                            month = monthHealthSprort.get(i).getMonth();
                            day = monthHealthSprort.get(ii).getDay();

                            // totalcal = totalcal + monthHealthSprort.get(ii).getTotalStepCount();
                            Log.e("total calaries", String.valueOf(totalcal));

                            if (day <= Today) {
                                if (i == 1) {
                                    if (day >= 1 && day <= 7) {
                                        totalcal = totalcal + monthHealthSprort.get(ii).getTotalStepCount();
                                        if (day == 7) {
                                            w1 = w1 + monthHealthSprort.get(ii).getTotalStepCount();
                                            temp.put(Constant.step, String.valueOf(w1));
                                            break;
                                        } else {
                                            w1 = w1 + monthHealthSprort.get(ii).getTotalStepCount();
                                            temp.put(Constant.step, String.valueOf(w1));
                                        }
                                    }
                                } else if (i == 2) {
                                    if (day >= 8 && day <= 14) {
                                        totalcal = totalcal + monthHealthSprort.get(ii).getTotalStepCount();
                                        if (day == 14) {
                                            w2 = w2 + monthHealthSprort.get(ii).getTotalStepCount();
                                            temp.put(Constant.step, String.valueOf(w2));
                                            break;
                                        } else {
                                            w2 = w2 + monthHealthSprort.get(ii).getTotalStepCount();
                                            temp.put(Constant.step, String.valueOf(w2));
                                        }
                                    }
                                } else if (i == 3) {
                                    if (day >= 15 && day <= 21) {
                                        totalcal = totalcal + monthHealthSprort.get(ii).getTotalStepCount();
                                        if (day == 21) {
                                            w3 = w3 + monthHealthSprort.get(ii).getTotalStepCount();
                                            temp.put(Constant.step, String.valueOf(w3));
                                            break;
                                        } else {
                                            w3 = w3 + monthHealthSprort.get(ii).getTotalStepCount();
                                            temp.put(Constant.step, String.valueOf(w3));
                                        }
                                    }
                                } else if (i == 4) {
                                    if (day >= 22 && day <= 28) {
                                        totalcal = totalcal + monthHealthSprort.get(ii).getTotalStepCount();
                                        if (day == 28) {
                                            w4 = w4 + monthHealthSprort.get(ii).getTotalStepCount();
                                            temp.put(Constant.step, String.valueOf(w4));
                                            break;
                                        } else {
                                            w4 = w4 + monthHealthSprort.get(ii).getTotalStepCount();
                                            temp.put(Constant.step, String.valueOf(w4));
                                        }
                                    }
                                } else if (i == 5) {
                                    if (day > 29) {
                                        totalcal = totalcal + monthHealthSprort.get(ii).getTotalStepCount();
                                        w5 = w5 + monthHealthSprort.get(ii).getTotalStepCount();
                                        temp.put(Constant.step, String.valueOf(w5));
                                    }
                                }
                                //temp.put(Constant.step, String.valueOf(monthHealthSprort.get(ii).getTotalStepCount()));
                            } /*else {
                                        temp.put(Constant.step, "-1");
                                        break;
                                    }*/
                        }
                        lst_month_step.add(temp);
                        callCount++;
                        total_step = total_step + totalcal;
                        setDataWithCounter(tv_total_step, total_step);
                    }
                    //tv_total_step.setText(String.valueOf(total_step));
                    Debugger.debugE("month_step..", lst_month_step.size() + " " + lst_month_step.toString());

                }
                if (lst_month_step.size() == callCount) {
                    monthWiseGraph(mChart, tv_step, tv_date);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // get data from band for steps and populate on screen
    public void getMonthDataCustomize() {
        List<HealthSport> monthHealthSprort = ProtocolUtils.getInstance().getMonthHealthSprort(0);
        Date startDate = monthHealthSprort.get(0).getDate();

        List<Integer> stepsdata = new ArrayList<>();

        //int[] a=new int[31];

        int[] intArray = new int[31];

        if (monthHealthSprort != null && monthHealthSprort.size() > 0) {
            for (int i = 0; i < monthHealthSprort.size(); i++) {
                stepsdata.add(monthHealthSprort.get(i).getTotalStepCount());
                intArray[i] = monthHealthSprort.get(i).getTotalStepCount();
            }
        }

        int x = 7;  // chunk size
        int len = intArray.length;
        int counter = 0;
        int[][] newArray = new int[len][];

        for (int i = 0; i < len - x + 1; i += x)
            newArray[counter++] = Arrays.copyOfRange(intArray, i, i + x);

        if (len % x != 0)
            newArray[counter] = Arrays.copyOfRange(intArray, len - len % x, len);

        Log.e("Hours data:", newArray.toString());
        int weekNo1 = DashboardActivity.mCurrentweek;

        for (int i = 0; i < 5; i++) {
            HashMap<String, String> temp = new HashMap<>();
            temp.put(Constant.time, UtilsCommon.getDateDayName(startDate.toString()));
            int sum = 0;
            if (i < weekNo1) {
                for (int j = 0; j < newArray[i].length; j++) {
                    sum = sum + newArray[i][j];
                    temp.put(Constant.step, String.valueOf(sum));
                }
                total_step = total_step + sum;
                lst_month_step.add(temp);
            } else {
                break;
            }
        }
        setDataWithCounter(tv_total_step, total_step);

        Debugger.debugE("month_step..", lst_month_step.size() + " " + lst_month_step.toString());
        if (lst_month_step.size() == weekNo1) {
            monthWiseGraph(mChart, tv_step, tv_date);
        }
    }

    private void getDeviceStepData() {
        try {
            if (ConnectionDetector.internetCheck(getContext())) {
                buildFitnessClient();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Googleapiclient connect
    private void buildFitnessClient() {
        if (HomeFragment.mClient != null) {
            if (Globals.StepMonthMap.size() > 0)
                setDataFromGlobals();
            else {
                if (!swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(true);
                getCurrentWeekDay();
            }
        }
    }

    // get steps data from shared prefrences
    public void setDataFromGlobals() {
        HashMap<String, Object> map = Globals.StepMonthMap;
        total_step = (int) map.get(Constant.step);
        lst_month_step = (ArrayList<HashMap<String, String>>) map.get("monthlist");
        setDataWithCounter(tv_total_step, total_step);
        monthWiseGraph(mChart, tv_step, tv_date);
    }

    // get the day of the current week and bind the data
    public void getCurrentWeekDay() {
        SimpleDateFormat dayDateFormate = new SimpleDateFormat("dd MMMM");
        if (weekDay.size() > 0) weekDay.clear();
        cal_month.set(Calendar.DAY_OF_MONTH, cal_month.getActualMinimum(Calendar.DAY_OF_MONTH));

        int currentWeek = cal_month.get(Calendar.WEEK_OF_YEAR);
        int year = cal_month.get(Calendar.YEAR);
        cal_month.set(Calendar.HOUR_OF_DAY, 0);
        int daysInMonth = cal_month.getActualMaximum(Calendar.DAY_OF_MONTH);
        int weekcount = 0;
        Calendar date = Calendar.getInstance();
        //int daysInMonth = date.get(Calendar.DAY_OF_MONTH);
        String[] days = new String[daysInMonth];
        int weekNo1 = DashboardActivity.mCurrentweek;

        for (int i = 0; i < daysInMonth; i++) {
            if (lst_month_step.size() <= weekNo1) {
                days[i] = dayDateFormate.format(cal_month.getTime());
                weekDay.add(dayDateFormate.format(cal_month.getTime()));
                cal_month.add(GregorianCalendar.DAY_OF_WEEK, 1);
                cal_month.set(Calendar.HOUR_OF_DAY, 0);
                HashMap<String, String> temp = new HashMap<>();

                temp.put(Constant.time, weekDay.get(i));

                temp.put(Constant.step, "0");

                whole_lst_month_calory.add(temp);
                if (weekcount == 0) {
                    lst_month_step.add(temp);
                }
                weekcount++;
                if (weekcount >= 7) {
                    weekcount = 0;
                }
            } else {
                days[i] = dayDateFormate.format(cal_month.getTime());
                weekDay.add(dayDateFormate.format(cal_month.getTime()));
                cal_month.add(GregorianCalendar.DAY_OF_WEEK, 1);
                cal_month.set(Calendar.HOUR_OF_DAY, 0);
                HashMap<String, String> temp = new HashMap<>();

                temp.put(Constant.time, weekDay.get(i));

                temp.put(Constant.step, "-1");

                whole_lst_month_calory.add(temp);
                if (weekcount == 0) {
                    lst_month_step.add(temp);
                }
                weekcount++;
                if (weekcount >= 7) {
                    weekcount = 0;
                }
            }
        }
        new StepsCall().execute();
    }

    /**
     * @param counterView get counterview
     * @param val         total calories
     */
    public void setDataWithCounter(CounterView counterView, float val) {
        counterView.setFormatter(new Formatter() {
            @Override
            public String format(String prefix, String suffix, float value) {
                return prefix
                        + NumberFormat.getNumberInstance(Locale.US).format(value)
                        + suffix;
            }
        });
        counterView.setAutoStart(false);
        counterView.setStartValue(val);
        counterView.setEndValue(val);
        counterView.setIncrement(UtilsCommon.getInterval(val)); // the amount the number increments at each time interval
        counterView.setTimeInterval(Constant.getTimeInterval); // the time interval (ms) at which the text changes
        counterView.start(); // you can start anytime if autostart is set to false
    }

    // swipe to refresh call process
    @Override
    public void onRefresh() {
        callCount = 0;
        total_step = 0;
        lst_month_step = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        getAndSetData();

    }

    private void getAndSetData() {
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            getMonthwiseData();
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            syncdevice s = new syncdevice(getContext(), onsyncdevice);
            s.syncdata();
        } else {
            getDeviceStepData();
        }
    }

    // success listener of syncdevice
    @Override
    public void onSucceedsyncdevice() {
        swipeRefreshLayout.setRefreshing(false);
        //getMonthData();
        getMonthDataCustomize();
    }

   /* @Override
    public void onFailedsyncdevice() {

    }*/

   // StepsCall api
    private class StepsCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(HomeFragment.mClient, readRequest).await(1, TimeUnit.MINUTES);
                stepsData = readData(dataReadResult);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {

                //tv_total_step.setText(String.valueOf(total_step));
                Globals.StepMonthMap.put(Constant.step, total_step);

                setDataWithCounter(tv_total_step, total_step);
                if (whole_lst_month_calory.size() > 0) {
                    int weekcount = 0;
                    int cal = 0;
                    for (int i = 1; i <= whole_lst_month_calory.size(); i++) {
                        if (whole_lst_month_calory.get(i - 1).get(Constant.step).equalsIgnoreCase(""))
                            cal += 0;
                        else
                            cal += Math.round(Float.parseFloat(whole_lst_month_calory.get(i - 1).get(Constant.step)));
                        weekcount++;
                        if (weekcount > 6) {
                            switch (i) {
                                case 7:
                                    lst_month_step.get(0).put(Constant.step, cal + "");
                                    break;
                                case 14:
                                    lst_month_step.get(1).put(Constant.step, cal + "");
                                    break;
                                case 21:
                                    lst_month_step.get(2).put(Constant.step, cal + "");
                                    break;
                                case 28:
                                    lst_month_step.get(3).put(Constant.step, cal + "");
                                    break;
                            }
                            weekcount = 0;
                            cal = 0;

                        }
                    }
                    if (whole_lst_month_calory.size() % 7 > 0) {
                        lst_month_step.get(4).put(Constant.step, cal + "");
                    }
                }
                monthWiseGraph(mChart, tv_step, tv_date);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            {
                Calendar calendar = getCalendarForNow();
                calendar.set(Calendar.DAY_OF_MONTH,
                        calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                setTimeToBeginningOfDay(calendar);
                statTime = calendar.getTimeInMillis();
            }

            {
                Calendar calendar = getCalendarForNow();
                calendar.set(Calendar.DAY_OF_MONTH,
                        calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                setTimeToEndofDay(calendar);
                endTime = calendar.getTimeInMillis();
            }

            java.text.DateFormat dateFormat = DateFormat.getDateInstance();
            Log.e("History", "Range Start: " + dateFormat.format(statTime));
            Log.e("History", "Range End: " + dateFormat.format(endTime));

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(statTime, endTime, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    private static Calendar getCalendarForNow() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        return calendar;
    }

    private static void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private static void setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

    public String readData(DataReadResult dataReadResult) {
        String value = "";
        try {
            if (dataReadResult.getBuckets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getBuckets().size());
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                        total_step += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                    total_step += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                String sDate = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00").format(dp.getStartTime(TimeUnit.MILLISECONDS));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    if (needToSetData(sDate)) {
                        value = String.valueOf(dp.getValue(field));
                    }
                }
                HashMap<String, String> temp = new HashMap<>();
                String eDate = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00").format(dp.getEndTime(TimeUnit.MILLISECONDS));
                String time = sDate + "-" + eDate;
                temp.put(Constant.time, UtilsCommon.ConvertStringIntoDateWeekName(sDate));
                temp.put(Constant.step, value);
                for (int i = 0; i < whole_lst_month_calory.size(); i++) {
                    if (whole_lst_month_calory.get(i).get(Constant.time).equalsIgnoreCase(UtilsCommon.ConvertStringIntoDateWeekName(sDate))) {
                        if (needToSetData(sDate)) {
                            whole_lst_month_calory.set(i, temp);
                            break;
                        }
                    }

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean needToSetData(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00");
        try {
            Date dt = format.parse(date);
            Date cdt = new Date();
            return dt.before(cdt);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    // api call for GetRoutineDataFromValidic
    public void getMonthwiseData() {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            //UtilsCommon.showProgressDialog(getActivity());
            int weekNo1 = DashboardActivity.mCurrentweek;
            int day = 1;
            for (int i = 1; i <= 5; i++) {
                if (i <= weekNo1) {
                    GetRoutineDataFromValidicCall task = new GetRoutineDataFromValidicCall(getActivity(), onGetRoutineFromValidicListener, false, UtilsCommon.AddSevenDayInDate(day), UtilsCommon.AddSevenDayInDate(day + 7), Constant.month);
                    task.execute();
                    day += 7;
                    callCount++;
                } else {
                    break;
                }
            }
        } else {
            //Toast.makeText(getActivity(), getActivity().getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * @param mChart      get the chart for displaying steps progress
     * @param tv_step     get textview of steps
     * @param tv_date     get textview of date
     */
    private void monthWiseGraph(CombinedChart mChart, final TextView tv_step, final TextView tv_date) {
        Globals.StepMonthMap.put("monthlist", lst_month_step);
        try {
            mChart.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setHighlightPerDragEnabled(false);
            mChart.setPinchZoom(false);
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.setDescription("");
            mChart.setExtraOffsets(30, 30, 30, 30);
            mChart.setTouchEnabled(true);
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setPinchZoom(false);

            Legend l = mChart.getLegend();
            l.setEnabled(false);
            //l.setForm(LegendForm.LINE);

            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<CategoryModel> dataList = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            Calendar c = Calendar.getInstance();
            SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
            String month_name = month_date.format(c.getTime());

            for (int i = 0; i < 5; i++) {
                if (i < lst_month_step.size()) {
                    HashMap<String, String> temp = lst_month_step.get(i);

                    double cal = Double.parseDouble(temp.get(Constant.step));
                    entries.add(new Entry(i, (float) cal));

                    if (i == 4) {
                        dataList.add(new CategoryModel((float) cal, UtilsCommon.MonthArray[i] + "-" + cal_month.getActualMaximum(Calendar.DAY_OF_MONTH) + " " + month_name));
                        labels.add(UtilsCommon.MonthArray[i] + " " + month_name);
                    } else {
                        dataList.add(new CategoryModel((float) cal, UtilsCommon.MonthArray[i] + " " + month_name));
                        labels.add(UtilsCommon.MonthArray[i] + " " + month_name);
                    }
                    if (i == 0) {
                        tv_step.setText(temp.get(Constant.step) + " steps");
                        tv_date.setText(UtilsCommon.MonthArray[i] + " " + month_name);
                    }
                } else {
                    entries.add(new Entry(i, (float) -1));
                    dataList.add(new CategoryModel((float) -1, ""));
                    labels.add("");
                }
            }
            LineDataSet dataset = new LineDataSet(entries, "");

            CombinedData cData = new CombinedData();
            LineData data = new LineData();

            dataset.setColors(new int[]{getActivity().getResources().getColor(R.color.app_light_green)});
            dataset.setValueTextColor(getActivity().getResources().getColor(R.color.app_light_green));
            dataset.setCircleColor(getActivity().getResources().getColor(R.color.line_chart_circle));
            dataset.setCircleColorHole(getActivity().getResources().getColor(R.color.line_chart_circle_hole));
            dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataset.setDrawFilled(false);
            dataset.setCircleSize(10);
            dataset.setDrawValues(false);
            dataset.setDrawHighlightIndicators(false);
            dataset.setLineWidth((float) 2.5);

            data.addDataSet(dataset);
            cData.setData(data);
            mChart.setData(cData);
            mChart.animateX(2500);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setTextSize(0f);
            xAxis.setTextColor(Color.TRANSPARENT);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);
            xAxis.setAxisMaximum(data.getXMax() + 0.25f);
            xAxis.setPosition(XAxisPosition.BOTH_SIDED);
            xAxis.setAxisMinimum(0f);
            xAxis.setGranularity(1f);

            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setDrawGridLines(false);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setTextSize(0f);
            leftAxis.setTextColor(Color.TRANSPARENT);
            leftAxis.setDrawLimitLinesBehindData(true);
            leftAxis.setDrawGridLines(false);
            mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

                @Override
                public void onValueSelected(Entry e, Highlight h) {

                    CategoryModel object = dataList.get((int) e.getX());
                    tv_step.setText((int) object.item1 + " steps");
                    tv_date.setText(object.item2);
                }

                @Override
                public void onNothingSelected() {

                }
            });

            mChart.getAxisRight().setEnabled(false);
            mChart.invalidate();
            swipeRefreshLayout.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    // success listener for GetRoutineDataFromValidicCall and bind the data
    @Override
    public void onSucceedToGetRoutineFromValidic(ValidicSummaryModel summary, ValidicRoutineModel data, String chartFlag) {
        try {
            HashMap<String, String> temp = new HashMap<>();

            String startDate = summary.getStart_date();

            temp.put(Constant.time, UtilsCommon.ConvertStringIntoDateWeekName(startDate));

            if (chartFlag.equalsIgnoreCase(Constant.month)) {
                int totalcal = 0;
                if (summary.getResults() > 0) {
                    for (int i = 0; i < data.getRoutine().size(); i++) {
                        totalcal = totalcal + data.getRoutine().get(i).getSteps();
                    }
                    temp.put(Constant.step, String.valueOf(totalcal));
                    total_step = total_step + totalcal;
                } else {
                    temp.put(Constant.step, "0");
                }
                lst_month_step.add(temp);
                tv_total_step.setText(String.valueOf(total_step));
                Debugger.debugE("month_step..", lst_month_step.size() + " " + lst_month_step.toString());

            }
            if (lst_month_step.size() == callCount) {
                monthWiseGraph(mChart, tv_step, tv_date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener for GetRoutineDataFromValidicCall
    @Override
    public void onFaildToGetRoutineFromValidic(String error_msg) {

    }

    // get the current week no and set the text color
    public void setCurrentWeekColor() {
        int weekNo1 = DashboardActivity.mCurrentweek;
        switch (weekNo1) {
            case 1:
                tv_week1.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
            case 2:
                tv_week2.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
            case 3:
                tv_week3.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
            case 4:
                tv_week4.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
            case 5:
                tv_week5.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
        }
    }
}
