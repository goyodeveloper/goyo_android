package com.differenzsystem.goyog.dashboard.challenges;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.plattysoft.leonids.ParticleSystem;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */
public class WonActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout ll_back;
    TextView tv_name;
    TextView tv_desc;
    TextView tv_title;
    TextView tv_link;
    ImageView iv_won;
    Bundle extra = null;
    JSONObject notification_data;
    String url, url_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.won_activity);
        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
        init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        //addFragment(ChallengesAndOffersFragment.newInstance(null), "ChallengesAndOffersFragment");
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(WonActivity.this, Constant.Name_Won);
    }

    // return the context of won screen
    private Context getContext() {
        return WonActivity.this;
    }

    // initialize all controls define in xml layout
    public void initializeControls() {
        try {
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(R.string.success);

            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_name = (TextView) findViewById(R.id.tv_name);
            tv_desc = (TextView) findViewById(R.id.tv_desc);
            iv_won = (ImageView) findViewById(R.id.iv_won);
            tv_link = (TextView) findViewById(R.id.tv_link);
            tv_desc.setMovementMethod(new ScrollingMovementMethod());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
            iv_won.setOnClickListener(this);
            tv_link.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // initialze process when screen loads and set the value which get from intent bundle
    public void init() {
        extra = getIntent().getExtras();
        if (extra != null) {
            try {
                String isFrom = extra.getString("isFrom");
                if (isFrom != null && isFrom.length() > 0) {
                    ChallengesModel challengesModel = (ChallengesModel) extra.getSerializable("challengesModel");
                    if (challengesModel.getName() != null)
                        tv_name.setText(challengesModel.getName());
                    tv_desc.setText(challengesModel.getDescription()
                            .replace("\\n", System.getProperty("line.separator")));

                    Glide.with(this)
                            .load(getApplicationContext().getResources().getString(R.string.server_url)
                                    + getApplicationContext().getResources().getString(R.string.challenge_images_url)
                                    + challengesModel.getImage())//getImage getThumb_image
                            .centerCrop()
                            .placeholder(R.drawable.app_placeholder)
                            .into(iv_won);

                    //tv_link.setText(challengesModel.getReward_hyperlink_text());
                    url = challengesModel.getReward_hyperlink();
                    url_text = challengesModel.getReward_hyperlink_text();

                    if ((url != null && url.length() > 0) && (url_text != null && url_text.length() > 0)) {
                        tv_link.setText(challengesModel.getReward_hyperlink_text());
                        tv_link.setPaintFlags(tv_link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    } else {
                        tv_link.setVisibility(View.GONE);
                    }

                } else {
                    notification_data = new JSONObject(extra.getString(Constant.notificationdata));
                    tv_name.setText(notification_data.getString(Constant.name));
                    tv_desc.setText(notification_data.getString(Constant.description)
                            .replace("\\n", System.getProperty("line.separator")));
                    Glide.with(this)
                            .load(getApplicationContext().getResources().getString(R.string.server_url)
                                    + getApplicationContext().getResources().getString(R.string.challenge_images_url)
                                    + notification_data.getString(Constant.image))
                            .centerCrop()
                            .placeholder(R.drawable.app_placeholder)
                            .into(iv_won);

                    //tv_link.setText(notification_data.getString(Constant.reward_hyperlink_text));
                    url_text = notification_data.getString(Constant.reward_hyperlink_text);
                    url = notification_data.getString(Constant.reward_hyperlink);

                    if ((url != null && url.length() > 0) && (url_text != null && url_text.length() > 0)) {
                        tv_link.setText(notification_data.getString(Constant.reward_hyperlink_text));
                        tv_link.setPaintFlags(tv_link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    } else {
                        tv_link.setVisibility(View.GONE);
                    }
                }

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iv_won.performClick();
                    }
                }, 500);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            tv_name.setText(OffersDetailFragment.obj_Challenges.getOffer_title());
            tv_desc.setText(OffersDetailFragment.obj_Challenges.getOffer_description()
                    .replace("\\n", System.getProperty("line.separator")));
            Glide.with(this)
                    .load(getApplicationContext().getResources().getString(R.string.server_url)
                            + getApplicationContext().getResources().getString(R.string.challenge_images_url)
                            + OffersDetailFragment.obj_Challenges.getImage())//getImage
                    .centerCrop()
                    .placeholder(R.drawable.app_placeholder)
                    .into(iv_won);


            url = OffersDetailFragment.obj_Challenges.getReward_hyperlink();
            url_text = OffersDetailFragment.obj_Challenges.getReward_hyperlink_text();

            if ((url != null && url.length() > 0) && (url_text != null && url_text.length() > 0)) {
                tv_link.setText(OffersDetailFragment.obj_Challenges.getReward_hyperlink_text());
                tv_link.setPaintFlags(tv_link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                tv_link.setVisibility(View.GONE);
            }

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    iv_won.performClick();
                }
            }, 500);
        }
    }

    // handle click listener
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_won:
                ParticleSystem ps1 = new ParticleSystem(this, 40, R.drawable.pixel1, 5000);
                ps1.setStartTime(15000);
                ps1.setScaleRange(0.7f, 1.3f);
                ps1.setSpeedRange(0.1f, 0.25f);
                ps1.setRotationSpeedRange(90, 180);
                ps1.setFadeOut(200, new AccelerateInterpolator());
                ps1.oneShot(iv_won, 100);

                ParticleSystem ps2 = new ParticleSystem(this, 40, R.drawable.pixel2, 5000);
                ps2.setStartTime(5000);
                ps2.setScaleRange(0.7f, 1.3f);
                ps2.setSpeedRange(0.1f, 0.25f);
                ps2.setRotationSpeedRange(90, 180);
                ps2.setFadeOut(200, new AccelerateInterpolator());
                ps2.oneShot(iv_won, 100);

                ParticleSystem ps3 = new ParticleSystem(this, 40, R.drawable.pixel3, 5000);
                ps3.setStartTime(5000);
                ps3.setScaleRange(0.7f, 1.3f);
                ps3.setSpeedRange(0.1f, 0.25f);
                ps3.setRotationSpeedRange(90, 180);
                ps3.setFadeOut(200, new AccelerateInterpolator());
                ps3.oneShot(iv_won, 100);

                ParticleSystem ps4 = new ParticleSystem(this, 40, R.drawable.pixel4, 5000);
                ps4.setStartTime(5000);
                ps4.setScaleRange(0.7f, 1.3f);
                ps4.setSpeedRange(0.1f, 0.25f);
                ps4.setRotationSpeedRange(90, 180);
                ps4.setFadeOut(200, new AccelerateInterpolator());
                ps4.oneShot(iv_won, 100);

                break;
            case R.id.tv_link:
                UtilsCommon.openBrowser(url, getContext());
                break;

            default:
                break;
        }
    }

    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_main, fragment, null);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }
}
