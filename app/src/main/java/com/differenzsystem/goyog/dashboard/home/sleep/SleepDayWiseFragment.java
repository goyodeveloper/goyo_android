package com.differenzsystem.goyog.dashboard.home.sleep;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.GetSleepDataFromValidicCall;
import com.differenzsystem.goyog.api.GetSleepDataFromValidicCall.OnGetSleepDataFromValidicListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.CategoryModel;
import com.differenzsystem.goyog.model.ValidicSleepModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.SpriteFactory;
import com.github.ybq.android.spinkit.Style;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.veryfit.multi.nativedatabase.healthSleep;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class SleepDayWiseFragment extends Fragment implements OnGetSleepDataFromValidicListener {

    View rootview;
    //BarChart mChart;
    //StackedBarChart mChart;
    ArrayList<BarEntry> BARENTRY;
    ArrayList<String> BarEntryLabels;
    BarDataSet Bardataset;
    BarData BARDATA;
    TextView tv_time_in_bed, tv_date, tv_total_sleep;
    TextView tv_restless_time, tv_light_sleep, tv_deep_sleep;
    TextView tv_awakee_count, tv_sleep_timing;
    LinearLayout ll_no_sleep_data, ll_sleep_main, lin_graph;
    ArrayList<HashMap<String, String>> lst_day_sleep;
    OnGetSleepDataFromValidicListener onGetSleepDataFromValidicListener;
    TableRow tbl_main;
    // Button v_asleep, v_Restless, v_awake, v_blank;

    int total_sleep = 0;
    int hours;

    public SleepDayWiseFragment() {
        // Required empty public constructor
    }

    // create the new instance of SleepDayWiseFragment
    public static SleepDayWiseFragment newInstance(Bundle bundle) {
        SleepDayWiseFragment fragment = new SleepDayWiseFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.sleep_day_wise_fragment, container, false);
            initcomopnets(rootview);
        } else
            container.removeView(rootview);

        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Sleep);
    }

    // initialize all controls define in xml layout

    /**
     *
     * @param rootview layout view
     */
    private void initcomopnets(View rootview) {
        try {
            lst_day_sleep = new ArrayList<>();
            //mChart = (StackedBarChart) rootview.findViewById(R.id.chart_calories);
            tv_time_in_bed = (TextView) rootview.findViewById(R.id.tv_time_in_bed);
            tv_date = (TextView) rootview.findViewById(R.id.tv_date);
            tv_total_sleep = (TextView) rootview.findViewById(R.id.tv_total_sleep);
            ll_no_sleep_data = (LinearLayout) rootview.findViewById(R.id.ll_no_sleep_data);
            ll_sleep_main = (LinearLayout) rootview.findViewById(R.id.ll_sleep_main);
            lin_graph = (LinearLayout) rootview.findViewById(R.id.lin_graph);
            tv_restless_time = (TextView) rootview.findViewById(R.id.tv_restless_time);
            tv_light_sleep = (TextView) rootview.findViewById(R.id.tv_light_sleep);
            tv_deep_sleep = (TextView) rootview.findViewById(R.id.tv_deep_sleep);
            tv_awakee_count = (TextView) rootview.findViewById(R.id.tv_awakee_count);
            tv_sleep_timing = (TextView) rootview.findViewById(R.id.tv_sleep_timing);

            onGetSleepDataFromValidicListener = this;
            //ProtocolUtils.getInstance().getHealthSleep(new Date(year, month, day));// sleep by date
            // List<healthSleepItem> items = ProtocolUtils.getInstance().getHealthSleepItem(new Date(year, month, day)); //sleep item
            //List<healthSleep> monthSleeps=ProtocolUtils.getInstance().getMonthHealthSleep(0); //sleep month

            if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                //getDaywiseData();
                ll_no_sleep_data.setVisibility(View.VISIBLE);
                ll_sleep_main.setVisibility(View.GONE);
            } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                //TODO:get Date from device
                ll_no_sleep_data.setVisibility(View.VISIBLE);
                ll_sleep_main.setVisibility(View.GONE);
            } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                setupSleepGraph();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set the graph for sleep and bind the data
    public void setupSleepGraph() {
        try {

            int year, month, day;
            Calendar mCalendar = Calendar.getInstance();
            year = mCalendar.get(Calendar.YEAR);
            month = mCalendar.get(Calendar.MONTH);
            day = mCalendar.get(Calendar.DAY_OF_MONTH);

            // sleep by date

            healthSleep sleep = ProtocolUtils.getInstance().getHealthSleep(new Date(year, month, day));
            if (sleep != null) {
                //set static data for test sleep graph

            /*int sleepEndedTimeH = 6, sleepEndedTimeM = 17, totalSleepMinutes = 409,
                    lightSleepCount = 12, deepSleepCount = 9, awakeCount = 0,
                    lightSleepMinutes = 184, deepSleepMinutes = 218, maxsleepminuts = 480;*/

                int sleepEndedTimeH = sleep.getSleepEndedTimeH(), sleepEndedTimeM = sleep.getSleepEndedTimeM(),
                        totalSleepMinutes = sleep.getTotalSleepMinutes(), lightSleepCount = sleep.getLightSleepCount(),
                        deepSleepCount = sleep.getDeepSleepCount(), awakeCount = sleep.getAwakeCount(),
                        lightSleepMinutes = sleep.getLightSleepMinutes(), deepSleepMinutes = sleep.getDeepSleepMinutes(),
                        maxsleepminuts = 480;

                //calculation

                int LSHours = lightSleepMinutes / 60; //since both are ints, you get an int
                int LSMinutes = lightSleepMinutes % 60;

                int SDHours = deepSleepMinutes / 60;
                int DSMinutes = deepSleepMinutes % 60;

                int restless = totalSleepMinutes - lightSleepMinutes - deepSleepMinutes;

                int RLHours = restless / 60;
                int RLMinutes = restless % 60;

                int TSHours = totalSleepMinutes / 60;
                int TSMinutes = totalSleepMinutes % 60;

                //start & end time
                String formattedH = String.format("%02d", sleepEndedTimeH);
                String formattedM = String.format("%02d", sleepEndedTimeM);

                String starttime = String.valueOf(year) + "-" + String.valueOf(month) + "-" + String.valueOf(day)
                        + " " + formattedH + ":" + formattedM + ":00";

                SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
                Date date = (Date) formater.parse(starttime);
                long st = date.getTime() - TimeUnit.MINUTES.toMillis(totalSleepMinutes);
                long et = date.getTime();

                Date sdate = new Date(st);
                Date edate = new Date(et);
                SimpleDateFormat df2 = new SimpleDateFormat("hh:mm a");
                String Startdate = df2.format(sdate);
                String Enddate = df2.format(edate);
                Startdate = Startdate.toUpperCase();
                Enddate = Enddate.toUpperCase();

                tv_sleep_timing.setText("Sleep time " + Startdate + " - " + Enddate);

                //set data
                if (LSHours > 0)
                    tv_light_sleep.setText(LSHours + "h " + LSMinutes + "m");
                else
                    tv_light_sleep.setText(LSMinutes + "m");

                if (SDHours > 0)
                    tv_deep_sleep.setText(SDHours + "h " + DSMinutes + "m");
                else
                    tv_deep_sleep.setText(DSMinutes + "m");

                if (RLHours > 0)
                    tv_restless_time.setText(RLHours + "h " + RLMinutes + "m");//rest less time
                else
                    tv_restless_time.setText(RLMinutes + "m");//rest less time

                if (TSHours > 0)
                    tv_total_sleep.setText(TSHours + "h " + TSMinutes + "m");
                else
                    tv_total_sleep.setText(TSMinutes + "m");

                tv_awakee_count.setText(awakeCount + " time awake");

                tv_time_in_bed.setVisibility(View.GONE);

                SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                Date d = new Date();
                String dayOfTheWeek = sdf.format(d);

                tv_date.setText(day + " " + dayOfTheWeek);

                //Draw a graph

                View new_view1 = new View(getActivity());
                View new_view2 = new View(getActivity());
                View new_view3 = new View(getActivity());
                View new_view4 = new View(getActivity());

                /*new_view1.setBackgroundColor(Color.YELLOW);
                new_view2.setBackgroundColor(Color.RED);
                new_view3.setBackgroundColor(Color.GREEN);
                new_view4.setBackgroundColor(Color.BLUE);*/

                new_view1.setBackgroundColor(getResources().getColor(R.color.light_sleep));
                new_view2.setBackgroundColor(getResources().getColor(R.color.deep_sleep));
                new_view3.setBackgroundColor(getResources().getColor(R.color.restless_sleep));
                new_view4.setBackgroundColor(getResources().getColor(android.R.color.transparent));

                LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                LinearLayout.LayoutParams param2 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                LinearLayout.LayoutParams param3 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                LinearLayout.LayoutParams param4 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);

                float lightper, deepper, resper, oth;
                if (totalSleepMinutes >= maxsleepminuts) {
                    lightper = (float) lightSleepMinutes * 100 / totalSleepMinutes;
                    deepper = (float) deepSleepMinutes * 100 / totalSleepMinutes;
                    resper = 100 - (lightper + deepper);
                    oth = 0;
                } else {
                    lightper = lightSleepMinutes * 100 / maxsleepminuts;
                    deepper = deepSleepMinutes * 100 / maxsleepminuts;
                    resper = restless * 100 / maxsleepminuts;
                    oth = 100 - (lightper + deepper + resper);
                }

                param1.weight = lightper;
                param2.weight = deepper;
                param3.weight = resper;
                param4.weight = oth;

                /*param1.weight = 30;
                param2.weight = 20;
                param3.weight = 20;
                param4.weight = 30;*/

                lin_graph.addView(new_view1, param1);
                lin_graph.addView(new_view2, param2);
                lin_graph.addView(new_view3, param3);
                lin_graph.addView(new_view4, param4);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void dayWiseGraph(CombinedChart mChart, final TextView tv_sleep, final TextView tv_date) {
        try {
            mChart.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setHighlightPerDragEnabled(false);
            mChart.setPinchZoom(false);
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.setDescription("");
            mChart.setExtraOffsets(30, 30, 30, 30);
            mChart.setTouchEnabled(true);
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setPinchZoom(false);

            Legend l = mChart.getLegend();
            l.setEnabled(false);

            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<CategoryModel> dataList = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            for (int i = 0; i < 24; i++) {
                if (i < lst_day_sleep.size()) {
                    HashMap<String, String> temp = lst_day_sleep.get(i);
                    double cal = Double.parseDouble(temp.get(Constant.sleep));
                    entries.add(new Entry(i, (float) cal));
                    dataList.add(new CategoryModel((float) cal, UtilsCommon.dayArray[i]));
                    labels.add(UtilsCommon.dayArray[i]);
                    if (i == 0) {
                        tv_sleep.setText(temp.get(Constant.sleep) + " hours");
                        tv_date.setText(UtilsCommon.dayArray[i]);
                    }
                } else {
                    entries.add(new Entry(i, (float) 0));
                    dataList.add(new CategoryModel((float) 0, UtilsCommon.dayArray[i]));
                    labels.add(UtilsCommon.dayArray[i]);
                    if (i == 0) {
                        tv_sleep.setText(0 + " hours");
                        tv_date.setText(UtilsCommon.dayArray[i]);
                    }
                }
            }

            LineDataSet dataset = new LineDataSet(entries, "# of Calls");

            CombinedData cData = new CombinedData();
            LineData data = new LineData();

            dataset.setColors(new int[]{getActivity().getResources().getColor(R.color.app_light_green)});
            dataset.setValueTextColor(getActivity().getResources().getColor(R.color.app_light_green));
            dataset.setCircleColor(getActivity().getResources().getColor(R.color.line_chart_circle));
            dataset.setCircleColorHole(getActivity().getResources().getColor(R.color.line_chart_circle_hole));
            dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataset.setDrawFilled(false);
            dataset.setCircleSize(10);
            dataset.setDrawValues(false);
            dataset.setDrawHighlightIndicators(false);
            dataset.setLineWidth((float) 2.5);

            SpinKitView spinKitView = new SpinKitView(getActivity());
            Style style = Style.values()[4];
            Sprite drawable = SpriteFactory.create(style);
            spinKitView.setIndeterminateDrawable(drawable);
            dataset.setSpinKitView(spinKitView);

            data.addDataSet(dataset);
            cData.setData(data);
            mChart.setData(cData);
            mChart.animateX(2500);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setTextSize(0f);
            xAxis.setTextColor(Color.TRANSPARENT);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);
            xAxis.setAxisMaximum(data.getXMax() + 0.25f);
            xAxis.setPosition(XAxisPosition.BOTH_SIDED);
            xAxis.setAxisMinimum(0f);
            xAxis.setGranularity(1f);

            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setDrawGridLines(false);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setTextSize(0f);
            leftAxis.setTextColor(Color.TRANSPARENT);
            leftAxis.setDrawLimitLinesBehindData(true);
            leftAxis.setDrawGridLines(false);

            mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    CategoryModel object = dataList.get((int) e.getX());
                    tv_sleep.setText((int) object.item1 + " hours");
                    tv_date.setText(object.item2);

                }

                @Override
                public void onNothingSelected() {

                }
            });

            mChart.getAxisRight().setEnabled(false);
            mChart.invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDaywiseData() {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            UtilsCommon.showProgressDialog(getActivity());
            for (int i = 01; i <= hours; i++) {
                GetSleepDataFromValidicCall task = new GetSleepDataFromValidicCall(getActivity(), onGetSleepDataFromValidicListener, false, UtilsCommon.getDateByHour(i), UtilsCommon.getDateByHour(i + 1), Constant.day);
                task.execute();
            }
        } else {
            //Toast.makeText(getActivity(), getActivity().getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    // success listener of GetSleepDataFromValidicCall and bind the data
    @Override
    public void onSucceedToGetSleepDataFromValidic(ValidicSummaryModel summary, ValidicSleepModel data, String chartFlag) {
        try {
            HashMap<String, String> temp = new HashMap<>();

            String startDate = summary.getStart_date();
            String endDate = summary.getEnd_date();

            String sDate = startDate.substring(startDate.indexOf("T") + 1, startDate.indexOf("T") + 6);
            String eDate = endDate.substring(endDate.indexOf("T") + 1, endDate.indexOf("T") + 6);
            String time = sDate + "-" + eDate;
            temp.put(Constant.time, time);
            if (chartFlag.equalsIgnoreCase(Constant.day)) {

                if (summary.getResults() > 0) {
                    temp.put(Constant.sleep, String.valueOf(Math.round(data.getSleep().get(0).getTime_in_bed() / 60)));
                    total_sleep = total_sleep + Math.round(data.getSleep().get(0).getTime_in_bed() / 60);
                } else {
                    temp.put(Constant.sleep, "0");
                }
                lst_day_sleep.add(temp);
                tv_total_sleep.setText(String.valueOf(Math.round(total_sleep)) + " HOURS");
                Debugger.debugE("Day_sleep..", hours + " " + lst_day_sleep.size() + " " + lst_day_sleep.toString());
            }

            if (lst_day_sleep.size() == hours) {
                Debugger.debugE("inside_sleep..", lst_day_sleep.size() + " " + lst_day_sleep.toString());
                UtilsCommon.destroyProgressBar();
                if (total_sleep <= 0) {
                    ll_sleep_main.setVisibility(View.GONE);
                    ll_no_sleep_data.setVisibility(View.VISIBLE);
                } else {
                    // dayWiseGraph(mChart, tv_time_in_bed, tv_date);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener of GetSleepDataFromValidicCall
    @Override
    public void onFaildToGetSleepDataFromValidic(String error_msg) {

    }
}