package com.differenzsystem.goyog.dashboard.home;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.calories.CaloriesFragment;
import com.differenzsystem.goyog.dashboard.home.distance.DistanceFragment;
import com.differenzsystem.goyog.dashboard.home.heartRate.HeartRateFragment;
import com.differenzsystem.goyog.dashboard.home.sleep.SleepFragment;
import com.differenzsystem.goyog.dashboard.home.step.StepsFragment;
import com.differenzsystem.goyog.model.LevelInformationModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.differenzsystem.goyog.utility.UtilsValidation;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */
public class GraphFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    LinearLayout ll_calories, ll_steps, ll_distance, ll_sleep, ll_heartRate;
    LinearLayout ll_caloriesData, ll_stepsData, ll_distanceData, ll_sleepData, ll_heartRateData;
    LinearLayout ll_graph;
    ProgressBar progress_calories, progress_steps, progress_distance, progress_sleep, progress_heartRate;
    TextView tv_caloriesData, tv_distanceData, tv_stepsData, tv_heartRateData, tv_sleepData;
    TextView tv_caloriesTotalData, tv_distanceTotalData, tv_stepsTotalData, tv_total_sleepData, tv_stepPer, tv_distancePer, tv_caloriesPer, tv_heartPer, tv_sleep_per;
    RelativeLayout rl_calories, rl_steps, rl_distance, rl_sleep;
    int distance = 0, steps = 0, calories = 0, heartRate = 0, sleep = 0;
    int totalDistance = 0, totalSteps = 0, totalCalories = 0, totalHeartRate = 0, totalSleep = 0;
    int distancePer = 0, stepPer = 0, caloriesPer = 0, heartRatePer = 0, sleepPer = 0;

    LinearLayout lin_progress_calories, lin_progress_steps, lin_progress_distance, lin_progress_sleep, lnr_badge;

    SwipeRefreshLayout swipeRefreshLayout;

    TextView tv_desc, tv_level, tv_level_name, remain_time;
    ImageView iv_badge, iv_timer;
    LevelInformationModel data;

    int height = 0;
    String TAG = "Graph";
    float scale;
    boolean isChallengeComplete = false;

    public static GraphFragment newInstance(Bundle extra) {
        GraphFragment fragment = new GraphFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.graph_fragment, container, false);
        data = HomeFragment.levelData;
        initializeControls(view);
        initializeControlsAction();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Level_Detail);
    }

    // convert into dp
    public int getDP(int dps) {
        return (int) (dps * scale + 0.5f);
    }

    /**
     * @param view get layout view
     */
    public void initializeControls(View view) {
        scale = getContext().getResources().getDisplayMetrics().density;
        try {
            swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_view);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(
                    R.color.colorPrimary);

            ll_graph = (LinearLayout) view.findViewById(R.id.ll_graph);
            lnr_badge = (LinearLayout) view.findViewById(R.id.lnr_badge);

            // Header
            ll_calories = (LinearLayout) view.findViewById(R.id.ll_calories);
            ll_steps = (LinearLayout) view.findViewById(R.id.ll_steps);
            ll_distance = (LinearLayout) view.findViewById(R.id.ll_distance);
            ll_sleep = (LinearLayout) view.findViewById(R.id.ll_sleep);
            ll_heartRate = (LinearLayout) view.findViewById(R.id.ll_heartRate);

            ll_caloriesData = (LinearLayout) view.findViewById(R.id.ll_caloriesData);
            ll_stepsData = (LinearLayout) view.findViewById(R.id.ll_stepsData);
            ll_distanceData = (LinearLayout) view.findViewById(R.id.ll_distanceData);
            ll_sleepData = (LinearLayout) view.findViewById(R.id.ll_sleepData);
            ll_heartRateData = (LinearLayout) view.findViewById(R.id.ll_heartRateData);

            lin_progress_calories = (LinearLayout) view.findViewById(R.id.lin_progress_calories);
            lin_progress_steps = (LinearLayout) view.findViewById(R.id.lin_progress_steps);
            lin_progress_distance = (LinearLayout) view.findViewById(R.id.lin_progress_distance);
            lin_progress_sleep = (LinearLayout) view.findViewById(R.id.lin_progress_sleep);

            rl_calories = (RelativeLayout) view.findViewById(R.id.rl_calories);
            rl_distance = (RelativeLayout) view.findViewById(R.id.rl_distance);
            rl_steps = (RelativeLayout) view.findViewById(R.id.rl_step);
            rl_sleep = (RelativeLayout) view.findViewById(R.id.rl_sleep);

            remain_time = (TextView) view.findViewById(R.id.remain_time);

            tv_caloriesData = (TextView) view.findViewById(R.id.tv_caloriesData);
            tv_heartRateData = (TextView) view.findViewById(R.id.tv_heartRateData);
            tv_stepsData = (TextView) view.findViewById(R.id.tv_stepsData);
            tv_distanceData = (TextView) view.findViewById(R.id.tv_distanceData);
            tv_sleepData = (TextView) view.findViewById(R.id.tv_sleepData);

            tv_caloriesTotalData = (TextView) view.findViewById(R.id.tv_total_caloriesData);
            tv_stepsTotalData = (TextView) view.findViewById(R.id.tv_total_stepsData);
            tv_distanceTotalData = (TextView) view.findViewById(R.id.tv_total_distanceData);
            tv_total_sleepData = (TextView) view.findViewById(R.id.tv_total_sleepData);

            tv_distancePer = (TextView) view.findViewById(R.id.tv_distance_per);
            tv_caloriesPer = (TextView) view.findViewById(R.id.tv_calories_per);
            tv_stepPer = (TextView) view.findViewById(R.id.tv_steps_per);
            tv_sleep_per = (TextView) view.findViewById(R.id.tv_sleep_per);
            tv_heartPer = (TextView) view.findViewById(R.id.tv_heartRate_per);

            tv_desc = (TextView) view.findViewById(R.id.tv_desc);
            tv_level_name = (TextView) view.findViewById(R.id.tv_level_name);
            tv_level = (TextView) view.findViewById(R.id.tv_level);
            iv_badge = (ImageView) view.findViewById(R.id.iv_badge);
            iv_timer = (ImageView) view.findViewById(R.id.iv_timer);
            tv_desc.setMovementMethod(new ScrollingMovementMethod());

            progress_calories = (ProgressBar) view.findViewById(R.id.progress_calories);
            progress_steps = (ProgressBar) view.findViewById(R.id.progress_steps);
            progress_distance = (ProgressBar) view.findViewById(R.id.progress_distance);
            progress_sleep = (ProgressBar) view.findViewById(R.id.progress_sleep);
            progress_heartRate = (ProgressBar) view.findViewById(R.id.progress_heartRate);

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                setdata();
            } else {
                swipeRefreshLayout.setEnabled(false);
                setdata();
            }

            tv_desc.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            if (!swipeRefreshLayout.isRefreshing())
                                swipeRefreshLayout.setEnabled(false);
                            break;
                        case MotionEvent.ACTION_DOWN:
                            if (!swipeRefreshLayout.isRefreshing())
                                swipeRefreshLayout.setEnabled(false);
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            swipeRefreshLayout.setEnabled(true);
                            break;
                        case MotionEvent.ACTION_HOVER_EXIT:
                            swipeRefreshLayout.setEnabled(true);
                            break;
                    }
                    return false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set data in calories, steps, distance, sleeps etc
    public void setdata() {
        if (data != null) {
            tv_level.setText(data.getLevel_subtitle());
            tv_level_name.setText(data.getLevel_name());
            remain_time.setText(data.getRemain_result());
            tv_desc.setText(data.getLevel_description().replace("\\n", System.getProperty("line.separator")));

            String url = getActivity().getResources().getString(R.string.server_url)
                    + getActivity().getResources().getString(R.string.badges_image)
                    + data.getLevel_image();

            Glide.with(getActivity())
                    .load(url)
                    .placeholder(R.drawable.app_placeholder)
                    .into(iv_badge);

            if (data.getLevel_type() != null) {
                int leveltype = convertStringtoInt(data.getLevel_type());
                setData(leveltype);
            }
            /*if (data.getSecond_level_type() != null) {
                int secondLevelType = convertStringtoInt(data.getSecond_level_type());
                setData(secondLevelType, 2);
            }*/
        } else {
            tv_desc.setVisibility(View.INVISIBLE);
            rl_calories.setVisibility(View.INVISIBLE);
            rl_steps.setVisibility(View.INVISIBLE);
            rl_distance.setVisibility(View.INVISIBLE);
            rl_sleep.setVisibility(View.INVISIBLE);
            tv_heartRateData.setVisibility(View.INVISIBLE);
        }

        ViewTreeObserver observer = ll_graph.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                height = ll_graph.getHeight();
                Log.e(TAG, "Height " + height);
                if (rl_calories.getVisibility() == View.VISIBLE) {
                    if (caloriesPer >= 100) {
                        LinearLayout.LayoutParams caloriesParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                        progress_calories.setLayoutParams(caloriesParams);
                    } else {
                        LinearLayout.LayoutParams caloriesParams = new LinearLayout.LayoutParams(getDP(15), getDP(caloriesPer * 2));
                        progress_calories.setLayoutParams(caloriesParams);
                    }
                } else {
                    LinearLayout.LayoutParams caloriesParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_calories.setLayoutParams(caloriesParams);
                }
                if (rl_steps.getVisibility() == View.VISIBLE) {
                    if (stepPer >= 100) {
                        LinearLayout.LayoutParams stepsParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                        progress_steps.setLayoutParams(stepsParams);
                    } else {
                        LinearLayout.LayoutParams stepsParams = new LinearLayout.LayoutParams(getDP(15), getDP(stepPer * 2));
                        progress_steps.setLayoutParams(stepsParams);
                    }
                } else {
                    LinearLayout.LayoutParams stepsParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_steps.setLayoutParams(stepsParams);
                }
                if (rl_distance.getVisibility() == View.VISIBLE) {
                    if (distancePer >= 100) {
                        LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                        progress_distance.setLayoutParams(distanceParams);
                    } else {
                        LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(distancePer * 2));
                        progress_distance.setLayoutParams(distanceParams);
                    }
                } else {
                    LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_distance.setLayoutParams(distanceParams);
                }
               /* if (rl_sleep.getVisibility() == View.VISIBLE) {
                    LinearLayout.LayoutParams sleepParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_sleep.setLayoutParams(sleepParams);
                } else {
                    LinearLayout.LayoutParams sleepParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_sleep.setLayoutParams(sleepParams);
                }*/

                if (rl_sleep.getVisibility() == View.VISIBLE) {
                    if (sleepPer >= 100) {
                        LinearLayout.LayoutParams sleepParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                        progress_sleep.setLayoutParams(sleepParams);
                    } else {
                        LinearLayout.LayoutParams sleepParams = new LinearLayout.LayoutParams(getDP(15), getDP(sleepPer * 2));
                        progress_sleep.setLayoutParams(sleepParams);
                    }
                } else {
                    LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_sleep.setLayoutParams(distanceParams);
                }

                if (heartRatePer >= 100) {
                    LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_heartRate.setLayoutParams(distanceParams);
                } else {
                    LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(heartRatePer * 2));
                    progress_heartRate.setLayoutParams(distanceParams);
                }

                /*LinearLayout.LayoutParams heartRateParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                progress_heartRate.setLayoutParams(heartRateParams);*/

                ll_graph.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        ObjectAnimator caloriesAnimation = ObjectAnimator.ofInt(progress_calories, "progress", 0, 100);
        caloriesAnimation.setDuration(1000);
        caloriesAnimation.setInterpolator(new DecelerateInterpolator());
        caloriesAnimation.start();

        ObjectAnimator stepsAnimation = ObjectAnimator.ofInt(progress_steps, "progress", 0, 100);
        stepsAnimation.setDuration(1000);
        stepsAnimation.setInterpolator(new DecelerateInterpolator());
        stepsAnimation.start();

        ObjectAnimator distanceAnimation = ObjectAnimator.ofInt(progress_distance, "progress", 0, 100);
        distanceAnimation.setDuration(1000);
        distanceAnimation.setInterpolator(new DecelerateInterpolator());
        distanceAnimation.start();

        ObjectAnimator sleepAnimation = ObjectAnimator.ofInt(progress_sleep, "progress", 0, 100);
        sleepAnimation.setDuration(1000);
        sleepAnimation.setInterpolator(new DecelerateInterpolator());
        sleepAnimation.start();

        ObjectAnimator heartRateAnimation = ObjectAnimator.ofInt(progress_heartRate, "progress", 0, 100);
        heartRateAnimation.setDuration(1000);
        heartRateAnimation.setInterpolator(new DecelerateInterpolator());
        heartRateAnimation.start();

        swipeRefreshLayout.setRefreshing(false);
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            /*ll_calories.setOnClickListener(this);
            ll_steps.setOnClickListener(this);
            ll_distance.setOnClickListener(this);
            ll_sleep.setOnClickListener(this);
            ll_heartRate.setOnClickListener(this);*/
            ll_graph.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set view of request type based on calculation of challenge or level type
    public void setData(int type) {
        switch (type) {
            case 1://steps
                steps = convertStringtoInt(data.getSteps());
                totalSteps = convertStringtoInt(data.getLevel_value());

                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");
                if (stepPer >= 100)
                    isChallengeComplete = true;
                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);
                lin_progress_steps.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 2://calories
                calories = convertStringtoInt(data.getCalories());
                totalCalories = convertStringtoInt(data.getLevel_value());

                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");
                if (caloriesPer >= 100)
                    isChallengeComplete = true;
                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);
                lin_progress_calories.setAlpha(1);
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 3://distance
                distance = convertStringtoInt(data.getDistance());
                totalDistance = convertStringtoInt(data.getLevel_value());

                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(convertStringtoInt(data.getLevel_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");
                if (distancePer >= 100)
                    isChallengeComplete = true;
                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);
                lin_progress_distance.setAlpha(1);
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 4://sleep
                sleep = convertStringtoInt(data.getSleep());
                totalSleep = convertStringtoInt(data.getLevel_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer >= 100)
                    isChallengeComplete = true;
                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);

                lin_progress_sleep.setAlpha(1);
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 5://distance and sleep
                distance = convertStringtoInt(data.getDistance());
                totalDistance = convertStringtoInt(data.getLevel_value());
                //totalDistance = convertStringtoInt(obj_Challenges.getChallenge_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(convertStringtoInt(data.getLevel_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);

                sleep = convertStringtoInt(data.getSleep());
                totalSleep = convertStringtoInt(data.getSecond_level_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (distancePer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                if (sleepPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                lin_progress_sleep.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 6://step and distance
                steps = convertStringtoInt(data.getSteps());
                totalSteps = convertStringtoInt(data.getLevel_value());

                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");
                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                distance = convertStringtoInt(data.getDistance());
                totalDistance = convertStringtoInt(data.getSecond_level_value());

                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(convertStringtoInt(data.getSecond_level_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (stepPer >= 100 && distancePer >= 100)
                    isChallengeComplete = true;

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);

                lin_progress_steps.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 7://steps and calories
                steps = convertStringtoInt(data.getSteps());
                totalSteps = convertStringtoInt(data.getLevel_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");
                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                calories = convertStringtoInt(data.getCalories());
                totalCalories = convertStringtoInt(data.getSecond_level_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (stepPer >= 100 && caloriesPer >= 100)
                    isChallengeComplete = true;

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                lin_progress_steps.setAlpha(1);
                lin_progress_calories.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 8://steps and sleep
                steps = convertStringtoInt(data.getSteps());
                totalSteps = convertStringtoInt(data.getLevel_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                sleep = convertStringtoInt(data.getSleep());
                totalSleep = convertStringtoInt(data.getSecond_level_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (stepPer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);
                lin_progress_steps.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 9://sleep and calories
                sleep = convertStringtoInt(data.getSleep());
                totalSleep = convertStringtoInt(data.getLevel_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);

                calories = convertStringtoInt(data.getCalories());
                totalCalories = convertStringtoInt(data.getSecond_level_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");
                if (caloriesPer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;
                isChallengeComplete = true;
                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                lin_progress_calories.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 10://distance and calories
                distance = convertStringtoInt(data.getDistance());
                totalDistance = convertStringtoInt(data.getLevel_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(convertStringtoInt(data.getLevel_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);

                calories = convertStringtoInt(data.getCalories());
                totalCalories = convertStringtoInt(data.getSecond_level_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");
                if (caloriesPer >= 100 && distancePer >= 100)
                    isChallengeComplete = true;
                isChallengeComplete = true;
                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                lin_progress_calories.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));

                break;

            case 11: //steps and calories and distance
                steps = convertStringtoInt(data.getSteps());
                totalSteps = convertStringtoInt(data.getLevel_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                calories = convertStringtoInt(data.getCalories());
                totalCalories = convertStringtoInt(data.getSecond_level_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                distance = convertStringtoInt(data.getDistance());
                totalDistance = convertStringtoInt(data.getThird_level_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(convertStringtoInt(data.getThird_level_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);

                if (stepPer >= 100 && caloriesPer >= 100 && distancePer >= 100)
                    isChallengeComplete = true;

                lin_progress_steps.setAlpha(1);
                lin_progress_calories.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 12://steps and calories nad sleep
                steps = convertStringtoInt(data.getSteps());
                totalSteps = convertStringtoInt(data.getLevel_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                calories = convertStringtoInt(data.getCalories());
                totalCalories = convertStringtoInt(data.getSecond_level_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);


                sleep = convertStringtoInt(data.getSleep());
                totalSleep = convertStringtoInt(data.getThird_level_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);


                if (stepPer >= 100 && caloriesPer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                lin_progress_steps.setAlpha(1);
                lin_progress_calories.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 13://steps and distance and sleep
                steps = convertStringtoInt(data.getSteps());
                totalSteps = convertStringtoInt(data.getLevel_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                distance = convertStringtoInt(data.getDistance());
                totalDistance = convertStringtoInt(data.getSecond_level_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(convertStringtoInt(data.getSecond_level_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);


                sleep = convertStringtoInt(data.getSleep());
                totalSleep = convertStringtoInt(data.getThird_level_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);


                if (stepPer >= 100 && distancePer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                lin_progress_steps.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));

                break;
            case 14://calories and distance and sleep
                calories = convertStringtoInt(data.getCalories());
                totalCalories = convertStringtoInt(data.getLevel_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                distance = convertStringtoInt(data.getDistance());
                totalDistance = convertStringtoInt(data.getSecond_level_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(convertStringtoInt(data.getSecond_level_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);


                sleep = convertStringtoInt(data.getSleep());
                totalSleep = convertStringtoInt(data.getThird_level_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);


                if (caloriesPer >= 100 && distancePer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                lin_progress_calories.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));

                break;
            case 15://steps and calories and distance and sleep
                steps = convertStringtoInt(data.getSteps());
                totalSteps = convertStringtoInt(data.getLevel_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                calories = convertStringtoInt(data.getCalories());
                totalCalories = convertStringtoInt(data.getSecond_level_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);


                distance = convertStringtoInt(data.getDistance());
                totalDistance = convertStringtoInt(data.getThird_level_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(convertStringtoInt(data.getThird_level_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);

                sleep = convertStringtoInt(data.getSleep());
                totalSleep = convertStringtoInt(data.getForth_level_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);


                if (stepPer >= 100 && caloriesPer >= 100 && distancePer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                lin_progress_steps.setAlpha(1);
                lin_progress_calories.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
        }

        //Duration calculation

        if (UtilsValidation.isGreaterThanCurrentDate(data.getStart_date())) {
            iv_timer.setVisibility(View.INVISIBLE);
            remain_time.setVisibility(View.INVISIBLE);
        } else {
            iv_timer.setVisibility(View.VISIBLE);
            remain_time.setVisibility(View.VISIBLE);
        }

        if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) { // condition of live user
            heartRate = UtilsCommon.setupDuration(data.getStart_date());
            if (heartRate < 0) {
                heartRate = 0;
            }
            tv_heartRateData.setVisibility(View.VISIBLE);
            tv_heartRateData.setText(UtilsCommon.convertMinuts(heartRate, 1));

            heartRatePer = Math.round((heartRate * 100) / convertStringtoInt(data.getDuration()));
            heartRatePer = (heartRatePer > 100 ? 100 : heartRatePer);
            tv_heartPer.setText(heartRatePer + "%");
            progress_heartRate.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));

            int diff = convertStringtoInt(data.getDuration()) - heartRate;
            if (diff < 0) {
                remain_time.setText(" Times up!");
            } else {
                remain_time.setText(UtilsCommon.convertMinuts(diff, 2));
            }
            // remain_time.setText((convertStringtoInt(data.getDuration()) / 60) - (heartRate / 60) + " hrs to go!");
        } else {
            tv_heartRateData.setVisibility(View.VISIBLE);
            heartRate = convertStringtoInt(data.getDuration());
            int hrs = heartRate / 60;
            int min = heartRate % 60;
            if (hrs > 0) {
                if (min > 0)
                    tv_heartRateData.setText(hrs + " hrs " + min + " min");
                else
                    tv_heartRateData.setText(hrs + " hrs");
            } else {
                tv_heartRateData.setText(min + " min");
            }

            tv_heartPer.setText(0 + "%");
            remain_time.setVisibility(View.GONE);
            iv_timer.setVisibility(View.GONE);
            progress_heartRate.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
        }

        if (isChallengeComplete)
            lnr_badge.setAlpha(1);
    }

    // convert string to int
    public Integer convertStringtoInt(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    // handle click listener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_calories:
                addFragment(CaloriesFragment.newInstance(null), "CaloriesFragment");
                break;

            case R.id.ll_steps:
                addFragment(StepsFragment.newInstance(null), "StepsFragment");
                break;

            case R.id.ll_distance:
                addFragment(DistanceFragment.newInstance(null), "DistanceFragment");
                break;

            case R.id.ll_sleep:
                addFragment(SleepFragment.newInstance(null), "SleepFragment");
                break;

            case R.id.ll_heartRate:
                addFragment(HeartRateFragment.newInstance(null), "HeartRateFragment");
                break;

            case R.id.ll_graph:
                getActivity().onBackPressed();
                break;

            default:
                break;
        }
    }

    // open required screen

    /**
     * @param fragment add fragment
     */
    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_main, fragment, null);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    // swipe to refresh call
    @Override
    public void onRefresh() {
        distance = 0;
        steps = 0;
        calories = 0;
        heartRate = 0;
        totalDistance = 0;
        totalSteps = 0;
        totalCalories = 0;
        totalHeartRate = 0;
        distancePer = 0;
        stepPer = 0;
        caloriesPer = 0;
        heartRatePer = 0;
        swipeRefreshLayout.setRefreshing(true);
        setdata();
    }

    // disconnect GoogleApiClient
    @Override
    public void onStop() {
        super.onStop();
        if (HomeFragment.mClient != null && HomeFragment.mClient.isConnected()) {
            HomeFragment.mClient.stopAutoManage(getActivity());
            HomeFragment.mClient.disconnect();
        }
    }
}
