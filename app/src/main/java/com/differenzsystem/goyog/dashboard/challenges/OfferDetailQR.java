package com.differenzsystem.goyog.dashboard.challenges;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.OffersModel;
import com.differenzsystem.goyog.utility.UtilsCommon;

public class OfferDetailQR extends Fragment implements View.OnClickListener {
    static Bundle extra;
    TextView tv_title, tv_name, tv_desc;
    TextView tv_link;
    OffersModel offersModel;
    ImageView iv_img;
    LinearLayout ll_back;

    public OfferDetailQR() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    // create the instance of screen
    public static OfferDetailQR newInstance(Bundle extras) {
        OfferDetailQR fragment = new OfferDetailQR();
        fragment.setArguments(extras);
        extra = extras;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_offer_detail_qr, container, false);
        init(view);
        initializeControlsAction();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Offer_Detail_QR);
    }

    // initialize all controls define in xml layout
    private void init(View view) {
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        tv_desc = (TextView) view.findViewById(R.id.tv_desc);
        iv_img = (ImageView) view.findViewById(R.id.iv_img);
        ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_link = (TextView) view.findViewById(R.id.tv_link);


        tv_title.setText("");
        tv_desc.setMovementMethod(new ScrollingMovementMethod());
        if (extra != null) {
            offersModel = (OffersModel) extra.getSerializable("Object");
            tv_name.setText(offersModel.getOffer_title());
            tv_desc.setText(offersModel.getOffer_description().replace("\\n", System.getProperty("line.separator")));
            String url = getActivity().getResources().getString(R.string.server_url)
                    + getActivity().getResources().getString(R.string.offer_image)
                    + offersModel.getOffer_image();

            /*String url = getActivity().getResources().getString(R.string.server_url)
                    + getActivity().getResources().getString(R.string.challenge_images_url)
                    + offersModel.getThumb_image();*/
            Glide.with(getActivity())
                    .load(url)
                    .placeholder(R.drawable.banner_placeholder)
                    .into(iv_img);

            String url1 = offersModel.getReward_hyperlink();
            String url_text = offersModel.getReward_hyperlink_text();

            if ((url1 != null && url1.length() > 0) && (url_text != null && url_text.length() > 0)) {
                tv_link.setText(offersModel.getReward_hyperlink_text());
                tv_link.setPaintFlags(tv_link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                tv_link.setVisibility(View.GONE);
            }

        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
            tv_link.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;
            case R.id.tv_link:
                // open browser for offers detail
                UtilsCommon.openBrowser(offersModel.getReward_hyperlink(), getActivity());
                break;

            default:
                break;
        }
    }
}
