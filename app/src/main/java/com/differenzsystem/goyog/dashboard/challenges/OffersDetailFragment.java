package com.differenzsystem.goyog.dashboard.challenges;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.ChallengeAcceptCall;
import com.differenzsystem.goyog.api.ChallengeAcceptCall.OnChallengeAcceptListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.OffersModel;
import com.differenzsystem.goyog.utility.BaseFragment;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import org.json.JSONObject;


import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class OffersDetailFragment extends BaseFragment implements View.OnClickListener, OnChallengeAcceptListener {
    LinearLayout ll_back;
    TextView tv_title, tv_desc, tv_name;
    ImageView iv_offer;

    Button btn_submit;
    static Bundle extras;
    public static OffersModel obj_Challenges;
    OnChallengeAcceptListener onChallengeAcceptListener;

    // create the instance of OffersDetailFragment
    public static OffersDetailFragment newInstance(Bundle extra) {
        OffersDetailFragment fragment = new OffersDetailFragment();
        fragment.setArguments(extra);
        extras = extra;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.offers_detail_fragment, container, false);
        initializeControls(view);
        initializeControlsAction();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Offer_Detail);
    }

    // initialize all controls define in xml layout
    public void initializeControls(View view) {
        try {
            if (extras != null) {
                obj_Challenges = (OffersModel) extras.getSerializable("Object");
            }
            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_desc = (TextView) view.findViewById(R.id.tv_desc);
            iv_offer = (ImageView) view.findViewById(R.id.iv_offer);
            tv_title.setText("");

            btn_submit = (Button) view.findViewById(R.id.btn_submit);
            btn_submit.setTypeface(setFont(getContext(), R.string.app_semibold));

            setData(obj_Challenges);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
            btn_submit.setOnClickListener(this);
            onChallengeAcceptListener = this;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set the data from OffersModel which get from intent bundle and populate screen
    private void setData(OffersModel object) {
        try {

            String url = getActivity().getResources().getString(R.string.server_url)
                    + getActivity().getResources().getString(R.string.challenge_images_url)
                    + object.getImage();

//            Glide.with(getActivity())
//                    .load(url)
//                    .placeholder(R.drawable.banner_placeholder)
//                    .into(iv_offer);

            Glide.with(getActivity())
                    .load(url)
                    .dontAnimate()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                            return false;
                        }
                    })
                    .error(R.drawable.banner_placeholder)
                    .into(iv_offer);

            tv_name.setText(object.getName());
            tv_desc.setText(object.getDescription());

            // Date newDate;
            /*try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                newDate = format.parse(object.getDate());

                String dayNumberSuffix = UtilsCommon.getDayNumberSuffix(newDate.getDay());

                SimpleDateFormat newFormat = new SimpleDateFormat("d'" + dayNumberSuffix + " 'MMM, yyyy");
                String dateString = newFormat.format(newDate);
                tv_date.setText(Html.fromHtml(dateString));
            } catch (ParseException e) {
                e.printStackTrace();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click listener
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;

            case R.id.btn_submit:
                claimOffer(obj_Challenges);
                break;

            default:
                break;
        }
    }

    // call for claim offer

    /**
     * @param obj_Challenges get challenge details from OffersModel
     */
    public void claimOffer(OffersModel obj_Challenges) {
        try {
            JSONObject object = new JSONObject();
            object.put(Constant.user_id, UtilsPreferences.getString(getContext(), Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));
            object.put(Constant.challenge_id, obj_Challenges.getChallenge_id());
            object.put(Constant.challenge_status, "3");

            new ChallengeAcceptCall(getContext(), onChallengeAcceptListener, object, true).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener for ChallengeAcceptCall
    @Override
    public void onSucceedChallengeAccept(String message, JSONObject jsonResponse) {

        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(getActivity(), Constant.Key_inProgressList);

        try {
            startActivity(new Intent(getActivity(), WonActivity.class));
            getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            addFragment(ChallengesAndOffersFragment.newInstance(null), "ChallengesAndOffersFragment");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener for ChallengeAcceptCall
    @Override
    public void onFailedChallengeAccept(String message) {
        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(getActivity(), Constant.Key_inProgressList);
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    // open required screen

    /**
     * @param fragment add fragment
     */
    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_main, fragment, null);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }
}