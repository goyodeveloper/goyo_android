package com.differenzsystem.goyog.dashboard.home.heartRate;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.differenzsystem.goyog.FitnessBandTracker.syncdevice;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.GetFitnessDataFromValidicCall;
import com.differenzsystem.goyog.api.GetFitnessDataFromValidicCall.OnGetFitnessFromValidicListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.model.CategoryModel;
import com.differenzsystem.goyog.model.ValidicFitnessModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.veryfit.multi.nativedatabase.HealthHeartRate;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.differenzsystem.goyog.constant.Constant.time;
import static com.differenzsystem.goyog.dashboard.home.HomeFragment.mClient;
import static java.text.DateFormat.getTimeInstance;


public class HeartRateWeekWiseFragment extends Fragment implements OnGetFitnessFromValidicListener, SwipeRefreshLayout.OnRefreshListener, syncdevice.Onsyncdevice {
    View rootview;
    CombinedChart mChart;
    TextView tv_heart_rate, tv_date, tv_total_heart_rate, tv_mon, tv_tue, tv_wed, tv_thu, tv_fri, tv_sat, tv_sun;
    ArrayList<HashMap<String, String>> lst_week_heart_rate;
    OnGetFitnessFromValidicListener onGetFitnessFromValidicListener;
    int total_heart_rate = 0, callCount = 0;
    List<String> weekDay = new ArrayList<>();
    public GregorianCalendar month;
    long statTime = 0;
    long endTime = 0;
    int heart_rate = 0, dataCount = 0;
    String TAG = "HeartRateWeekWiseFragment";
    SwipeRefreshLayout swipeRefreshLayout;
    syncdevice.Onsyncdevice onsyncdevice;

    public HeartRateWeekWiseFragment() {
        // Required empty public constructor
    }

    // create the new instance of HeartRateWeekWiseFragment
    public static HeartRateWeekWiseFragment newInstance(Bundle bundle) {
        HeartRateWeekWiseFragment fragment = new HeartRateWeekWiseFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        onsyncdevice = this;
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.heart_rate_week_wise_fragment, container, false);
            initcomopnets(rootview);
        } else
            container.removeView(rootview);
        return rootview;

    }

    // initialize all controls define in xml layout

    /**
     *
     * @param rootview layout view
     */
    private void initcomopnets(View rootview) {
        try {
            swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.swipe_view);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(
                    R.color.colorPrimary);
            mChart = (CombinedChart) rootview.findViewById(R.id.chart_calories);

            lst_week_heart_rate = new ArrayList<>();

            tv_heart_rate = (TextView) rootview.findViewById(R.id.tv_avg_heart_rate);
            tv_date = (TextView) rootview.findViewById(R.id.tv_date);
            tv_total_heart_rate = (TextView) rootview.findViewById(R.id.tv_total_heart_rate);
            tv_mon = (TextView) rootview.findViewById(R.id.tv_mon);
            tv_tue = (TextView) rootview.findViewById(R.id.tv_tue);
            tv_wed = (TextView) rootview.findViewById(R.id.tv_wed);
            tv_thu = (TextView) rootview.findViewById(R.id.tv_thu);
            tv_fri = (TextView) rootview.findViewById(R.id.tv_fri);
            tv_sat = (TextView) rootview.findViewById(R.id.tv_sat);
            tv_sun = (TextView) rootview.findViewById(R.id.tv_sun);

            month = (GregorianCalendar) GregorianCalendar.getInstance();
            onGetFitnessFromValidicListener = this;

            setCurrentDayColor();

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) { // condition of live user
                //if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                //    getWeekwiseData();

                // condition for band connection
                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    getWeekData();
                } else {
                    //TODO:get Date from device
                    getDeviceHeartRateData();
                }
            } else {
                swipeRefreshLayout.setEnabled(false);
                setupDemoUserWeek();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // setup data for demo user
    private void setupDemoUserWeek() {
        Calendar mCalendar = Calendar.getInstance();
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
        int dayOfWeek = mCalendar.get(Calendar.DAY_OF_WEEK);

        for (int i = 0; i < 8; i++) {
            if (i != 0) {
                HashMap<String, String> temp = new HashMap<>();
                temp.put(Constant.time, String.valueOf(day) + " " + (String) android.text.format.DateFormat.format("MMMM", new Date()));

                if (i < dayOfWeek) {
                    temp.put(Constant.heart_rate, "0");
                } else {
                    temp.put(Constant.heart_rate, "-1");
                }
                lst_week_heart_rate.add(temp);
            }
        }

        tv_total_heart_rate.setText("0 BPM");

        UtilsCommon.destroyProgressBar();
        weekWiseGraph(mChart, tv_heart_rate, tv_date);

    }

    public void getWeekDataOld() {
        try {
            Calendar mCalendar = Calendar.getInstance();
                   /* int year = mCalendar.get(Calendar.YEAR);
                    int month=mCalendar.get(Calendar.MONTH);*/
            int Today = mCalendar.get(Calendar.DAY_OF_MONTH);
            int dayOfWeek = mCalendar.get(Calendar.DAY_OF_WEEK);
            Log.e("today day", String.valueOf(dayOfWeek));

            int year = 0, month = 0, day = 0;

            List<HealthHeartRate> weekHealthHeartRate = ProtocolUtils.getInstance().getWeekHealthHeartRate(0);
            if (weekHealthHeartRate != null && weekHealthHeartRate.size() > 0) {
                Date startDate = weekHealthHeartRate.get(0).getDate();
                int totalcal = 0;
                for (int i = 0; i < weekHealthHeartRate.size(); i++) {
                    if (i != 0) {
                        HashMap<String, String> temp = new HashMap<>();
                        //temp.put(Constant.time, UtilsCommon.getDateDayName(startDate.toString()));

                        year = weekHealthHeartRate.get(i).getYear();
                        month = weekHealthHeartRate.get(i).getMonth();
                        day = weekHealthHeartRate.get(i).getDay();

                        temp.put(Constant.time, String.valueOf(day) + " " + (String) android.text.format.DateFormat.format("MMMM", new Date()));

                        totalcal = totalcal + weekHealthHeartRate.get(i).getSilentHeart();
                        Log.e("total calaries", String.valueOf(totalcal));
                        callCount++;

                        if (i < dayOfWeek) {
                            temp.put(Constant.heart_rate, String.valueOf(weekHealthHeartRate.get(i).getSilentHeart()));
                            dataCount++;
                        } else {
                            temp.put(Constant.heart_rate, "-1");
                        }
                        lst_week_heart_rate.add(temp);
                    }
                }
                HealthHeartRate sundaydata = ProtocolUtils.getInstance().getHealthRate(new Date(year, month - 1, day + 1));
                HashMap<String, String> temp = new HashMap<>();
                callCount++;
                if (sundaydata != null) {
                    totalcal = totalcal + sundaydata.getSilentHeart();
                    if (sundaydata.getDay() <= Today) {
                        temp.put(Constant.time, String.valueOf(sundaydata.getDay()) + " " + (String) android.text.format.DateFormat.format("MMMM", new Date()));
                        temp.put(Constant.heart_rate, String.valueOf(totalcal));
                        dataCount++;
                    } else {
                        temp.put(Constant.heart_rate, "-1");
                    }
                } else {
                    temp.put(Constant.heart_rate, "-1");
                }
                lst_week_heart_rate.add(temp);

                total_heart_rate = total_heart_rate + totalcal;
                total_heart_rate = total_heart_rate / dataCount;//change
                //setDataWithCounter(tv_total_step, total_step);
                tv_total_heart_rate.setText(String.valueOf(total_heart_rate) + " BPM");
            }

            //tv_total_step.setText(String.valueOf(total_step));
            Debugger.debugE("Week_Calories..", callCount + "  " + lst_week_heart_rate.size());

            if (lst_week_heart_rate.size() == callCount) {
                weekWiseGraph(mChart, tv_heart_rate, tv_date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set the graph from the heart rate data
    public void getWeekData() {
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");//yyyy-MM-dd
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        int delta = -calendar.get(GregorianCalendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        calendar.add(Calendar.DAY_OF_MONTH, delta);

        String[] days = new String[7];
        int totalcal = 0;
        for (int i = 0; i < 7; i++) {
            days[i] = format.format(calendar.getTime());

            String start[] = days[i].split("/");
            int month = Integer.parseInt(start[0]) - 1;
            int day = Integer.parseInt(start[1]);
            int year = Integer.parseInt(start[2]);

            HashMap<String, String> temp = new HashMap<>();

            HealthHeartRate Heartdata = ProtocolUtils.getInstance().getHealthRate(new Date(year, month, day));
            temp.put(Constant.time, String.valueOf(day) + " " + (String) android.text.format.DateFormat.format("MMMM", new Date()));

            if (i < dayOfWeek) {
                if (Heartdata != null) {
                    totalcal = totalcal + Heartdata.getSilentHeart();
                    temp.put(Constant.heart_rate, String.valueOf(Heartdata.getSilentHeart()));
                    Log.e("Healthdata =>", Heartdata.toString());
                } else {
                    totalcal = totalcal + 0;
                    temp.put(Constant.heart_rate, "0");
                }
                lst_week_heart_rate.add(temp);
            }
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        Log.e("Week Date =>", Arrays.toString(days));

        total_heart_rate = (total_heart_rate + totalcal) / dayOfWeek;
        tv_total_heart_rate.setText(String.valueOf(total_heart_rate) + " BPM");

        if (lst_week_heart_rate.size() == dayOfWeek) {
            weekWiseGraph(mChart, tv_heart_rate, tv_date);
        }
    }

    private void getDeviceHeartRateData() {
        try {
            if (ConnectionDetector.internetCheck(getContext())) {
                buildFitnessClient();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void buildFitnessClient() {
        if (HomeFragment.mClient != null) {
            getCurrentWeekDay();
        }
    }

    // get the day of the current week and bind data
    public List<String> getCurrentWeekDay() {

        SimpleDateFormat dayDateFormate = new SimpleDateFormat("dd EEEE");
        if (weekDay.size() > 0) weekDay.clear();

        month.setFirstDayOfWeek(Calendar.MONDAY);
        month.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        int currentWeek = month.get(Calendar.WEEK_OF_YEAR);
        int year = month.get(Calendar.YEAR);
        month.set(Calendar.HOUR_OF_DAY, 0);

        String[] days = new String[7];
        for (int i = 0; i < 7; i++) {
            if (!UtilsCommon.AddDayInDateOfMonday(i).equalsIgnoreCase(UtilsCommon.getNextDaydate())) {
                if (i == 0) {
                    statTime = month.getTimeInMillis();
                }
                if (i == 6) {
                    endTime = month.getTimeInMillis();
                }
                days[i] = dayDateFormate.format(month.getTime());
                weekDay.add(dayDateFormate.format(month.getTime()));
                month.add(GregorianCalendar.DAY_OF_WEEK, 1);
                month.set(Calendar.HOUR_OF_DAY, 0);
                HashMap<String, String> temp = new HashMap<>();

                temp.put(Constant.time, weekDay.get(i));

                temp.put(Constant.heart_rate, "0");

                lst_week_heart_rate.add(temp);
            } else {
                if (i == 0) {
                    statTime = month.getTimeInMillis();
                }
                if (i == 6) {
                    endTime = month.getTimeInMillis();
                }
                days[i] = dayDateFormate.format(month.getTime());
                weekDay.add(dayDateFormate.format(month.getTime()));
                month.add(GregorianCalendar.DAY_OF_WEEK, 1);
                month.set(Calendar.HOUR_OF_DAY, 0);
                HashMap<String, String> temp = new HashMap<>();

                temp.put(Constant.time, weekDay.get(i));

                temp.put(Constant.heart_rate, "-1");

                lst_week_heart_rate.add(temp);
            }

        }
        new HeartRateCall().execute();
        return weekDay;
    }

    // swipe to refresh call
    @Override
    public void onRefresh() {
        callCount = 0;
        total_heart_rate = 0;
        lst_week_heart_rate = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        getAndSetData();

    }

    // success listener of syncdevice
    @Override
    public void onSucceedsyncdevice() {
        swipeRefreshLayout.setRefreshing(false);
        getWeekData();
    }

   /* @Override
    public void onFailedsyncdevice() {

    }*/

    private void getAndSetData() {
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            getWeekwiseData();
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            syncdevice s = new syncdevice(getContext(), onsyncdevice);
            s.syncdata();
        } else {
            new HeartRateCall().execute();
        }
    }

    // HeartRateCall
    private class HeartRateCall extends AsyncTask<Void, Void, Void> {
        String heartRateData;

        protected Void doInBackground(Void... params) {
            try {
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                heartRateData = readData(dataReadResult);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                int count = 0;
                for (int i = 0; i < lst_week_heart_rate.size(); i++) {
                    if (!lst_week_heart_rate.get(i).get(Constant.heart_rate).equalsIgnoreCase("0")) {
                        count++;
                    }
                }
                tv_total_heart_rate.setText(heart_rate / (count == 0 ? 1 : count) + " BPM");
                weekWiseGraph(mChart, tv_heart_rate, tv_date);
                Debugger.debugE("heartRateData", heartRateData + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // request data from google fit
    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            Calendar cal = Calendar.getInstance();
            Date now = new Date();
            cal.setTime(now);
            cal.add(Calendar.WEEK_OF_YEAR, -1);

            DateFormat dateFormat = DateFormat.getDateInstance();
            Log.e("History", "Range Start: " + dateFormat.format(statTime));
            Log.e("History", "Range End: " + dateFormat.format(endTime));

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(statTime, endTime, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    // read specific data by challenge type
    public String readData(DataReadResult dataReadResult) {
        String value = "";

        try {
            if (dataReadResult.getBuckets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getBuckets().size());
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                        heart_rate = Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));

                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                    heart_rate = Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }

                HashMap<String, String> temp = new HashMap<>();
                String sDate = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00").format(dp.getStartTime(TimeUnit.MILLISECONDS));
                temp.put(Constant.time, UtilsCommon.getDateDayName(sDate));

                temp.put(Constant.heart_rate, value);
                for (int i = 0; i < lst_week_heart_rate.size(); i++) {
                    if (lst_week_heart_rate.get(i).get(Constant.time).equalsIgnoreCase(UtilsCommon.getDateDayName(sDate))) {
                        lst_week_heart_rate.set(i, temp);
                    }
                }
                //lst_week_calory.add(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    // api call of GetFitnessDataFromValidic
    public void getWeekwiseData() {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            //UtilsCommon.showProgressDialog(getActivity());

            for (int i = 0; i < 7; i++) {
                if (!UtilsCommon.AddDayInDateOfMonday(i).equalsIgnoreCase(UtilsCommon.getNextDaydate())) {
                    GetFitnessDataFromValidicCall task = new GetFitnessDataFromValidicCall(getActivity(), onGetFitnessFromValidicListener, false, UtilsCommon.AddDayInDateOfMonday(i), UtilsCommon.AddDayInDateOfMonday(i + 1), Constant.week);
                    task.execute();
                    callCount++;
                } else {
                    break;
                }
            }
        } else {
            //Toast.makeText(getActivity(), getActivity().getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * @param mChart      get the chart for displaying HeartRate progress
     * @param tv_heart_rate get textview of HeartRate
     * @param tv_date     get textview of date
     */
    private void weekWiseGraph(CombinedChart mChart, final TextView tv_heart_rate, final TextView tv_date) {
        try {
            swipeRefreshLayout.setRefreshing(false);
            mChart.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setHighlightPerDragEnabled(false);
            mChart.setPinchZoom(false);
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.setDescription("");
            mChart.setExtraOffsets(30, 30, 30, 30);
            mChart.setTouchEnabled(true);
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setPinchZoom(false);

            Legend l = mChart.getLegend();
            l.setEnabled(false);

            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<CategoryModel> dataList = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            for (int i = 0; i < 7; i++) {
                if (i < lst_week_heart_rate.size()) {
                    HashMap<String, String> temp = lst_week_heart_rate.get(i);
                    double cal = Double.parseDouble(temp.get(Constant.heart_rate));
                    entries.add(new Entry(i, (float) cal));
                    dataList.add(new CategoryModel((float) cal, temp.get(time)));
                    labels.add(temp.get(time));
                    if (i == 0) {
                        tv_heart_rate.setText(temp.get(Constant.heart_rate) + " bpm");
                        tv_date.setText(temp.get(Constant.time));
                    }
                } else {
                    entries.add(new Entry(i, (float) -1));
                    dataList.add(new CategoryModel((float) -1, ""));
                    labels.add("");
                }
            }

            CombinedData cData = new CombinedData();
            LineData data = new LineData();
            LineDataSet dataset = new LineDataSet(entries, "");
            dataset.setColors(new int[]{getActivity().getResources().getColor(R.color.app_light_green)});
            dataset.setValueTextColor(getActivity().getResources().getColor(R.color.app_light_green));
            dataset.setCircleColor(getActivity().getResources().getColor(R.color.line_chart_circle));
            dataset.setCircleColorHole(getActivity().getResources().getColor(R.color.line_chart_circle_hole));
            dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataset.setDrawFilled(false);
            dataset.setLineWidth((float) 2.5);
            dataset.setCircleSize(10);
            dataset.setDrawValues(false);
            dataset.setDrawHighlightIndicators(false);

            data.addDataSet(dataset);
            cData.setData(data);
            mChart.setData(cData);
            mChart.animateX(2500);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setTextSize(0f);
            xAxis.setTextColor(Color.TRANSPARENT);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);
            xAxis.setAxisMaximum(data.getXMax() + 0.25f);
            xAxis.setPosition(XAxisPosition.BOTH_SIDED);
            xAxis.setAxisMinimum(0f);
            xAxis.setGranularity(1f);

            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setDrawGridLines(false);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setTextSize(0f);
            leftAxis.setTextColor(Color.TRANSPARENT);
            leftAxis.setDrawLimitLinesBehindData(true);
            leftAxis.setDrawGridLines(false);

            mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

                @Override
                public void onValueSelected(Entry e, Highlight h) {

                    CategoryModel object = dataList.get((int) e.getX());
                    tv_heart_rate.setText((int) object.item1 + " bpm");
                    tv_date.setText(object.item2);

                }

                @Override
                public void onNothingSelected() {

                }
            });
            mChart.getAxisRight().setEnabled(false);
            mChart.invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of GetFitnessDataFromValidicCall and bind the data
    @Override
    public void onSucceedToGetFitnessFromValidic(ValidicSummaryModel summary, ValidicFitnessModel data, String chartFlag) {
        try {
            HashMap<String, String> temp = new HashMap<>();

            String startDate = summary.getStart_date();

            temp.put(Constant.time, UtilsCommon.getDateDayName(startDate));
            if (chartFlag.equalsIgnoreCase(Constant.week)) {

                int totalcal = 0;
                if (summary.getResults() > 0) {
                    for (int i = 0; i < data.getFitness().size(); i++) {
                        totalcal = data.getFitness().get(i).getAverage_heart_rate();
                    }
                    temp.put(Constant.heart_rate, String.valueOf(totalcal));
                    total_heart_rate = total_heart_rate + totalcal;
                    dataCount++;
                } else {
                    temp.put(Constant.heart_rate, "0");
                }
                lst_week_heart_rate.add(temp);
                tv_total_heart_rate.setText(String.valueOf(total_heart_rate / (dataCount == 0 ? 1 : dataCount)) + " BPM");
                Debugger.debugE("Week_heart_rate..", temp.get(Constant.time) + " " + lst_week_heart_rate.toString());

            }
            if (lst_week_heart_rate.size() == callCount) {
                weekWiseGraph(mChart, tv_heart_rate, tv_date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener of GetFitnessDataFromValidicCall
    @Override
    public void onFaildToGetFitnessFromValidic(String error_msg) {

    }

    // get the current day no and set the text color
    public void setCurrentDayColor() {
        Calendar date = Calendar.getInstance();
        int day = date.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case 1:
                tv_sun.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 2:
                tv_mon.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 3:
                tv_tue.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 4:
                tv_wed.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 5:
                tv_thu.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 6:
                tv_fri.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 7:
                tv_sat.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
        }
    }
}
