package com.differenzsystem.goyog.dashboard.home.distance;

import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.utility.UtilsPermission;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class DistanceFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_back, ll_main;
    TextView tv_title;
    RelativeLayout ll_no_permission;
    Button btn_allow_access;
    View rootview;
    ViewPager viewPager;
    CirclePageIndicator indicator;
    DistanceDayWiseFragment distanceDayWiseFragment;
    DistanceWeekWiseFragment distanceWeekWiseFragment;
    DistanceMonthWiseFragment distanceMonthWiseFragment;

    public static DistanceFragment newInstance(Bundle extra) {
        DistanceFragment fragment = new DistanceFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.distance_fragment, container, false);

        initializeControls(rootview);
        initializeControlsAction();
        return rootview;
    }

    // initialize all controls define in xml layout

    /**
     *
     * @param view get layout view
     */
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    public void initializeControls(View view) {
        try {
            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            ll_main = (LinearLayout) view.findViewById(R.id.ll_main);
            ll_no_permission = (RelativeLayout) view.findViewById(R.id.ll_no_permission);
            btn_allow_access = (Button) view.findViewById(R.id.btn_allow_access);
            tv_title.setText(getString(R.string.distance_lbl));

            distanceDayWiseFragment = DistanceDayWiseFragment.newInstance(null);
            distanceWeekWiseFragment = DistanceWeekWiseFragment.newInstance(null);
            distanceMonthWiseFragment = DistanceMonthWiseFragment.newInstance(null);

            viewPager = (ViewPager) view.findViewById(R.id.viewPager);
            indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
            indicator.setStrokeWidth(2);
            indicator.setStrokeColor(getActivity().getResources().getColor(R.color.pager_selected));

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) { // condition for live user

                // condition of band connection
                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                    checkForPermission();
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));
                    indicator.setViewPager(viewPager);
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                    viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));
                    indicator.setViewPager(viewPager);
                }
            } else {
                viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));
                indicator.setViewPager(viewPager);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
            btn_allow_access.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // require permission BODY_SENSORS
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    public void checkForPermission() {
        boolean flag = UtilsPermission.checkBodySensor(getActivity());

        if (flag) {
            ll_main.setVisibility(View.VISIBLE);
            ll_no_permission.setVisibility(View.GONE);
            viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));
            indicator.setViewPager(viewPager);
        } else {
            ll_no_permission.setVisibility(View.VISIBLE);
            ll_main.setVisibility(View.GONE);
            Toast.makeText(getActivity(), getActivity().getString(R.string.msg_bodysensor_permission), Toast.LENGTH_SHORT).show();
        }
    }

    // handle click listener
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;
            case R.id.btn_allow_access:
                checkForPermission();
                break;

            default:
                break;
        }
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return distanceDayWiseFragment;
                case 1:
                    return distanceWeekWiseFragment;
                case 2:
                    return distanceMonthWiseFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    // disconnect GoogleApiClient and remove resources
    @Override
    public void onStop() {
        super.onStop();
        if (HomeFragment.mClient != null && HomeFragment.mClient.isConnected()) {
            HomeFragment.mClient.stopAutoManage(getActivity());
            HomeFragment.mClient.disconnect();
        }
    }

}
