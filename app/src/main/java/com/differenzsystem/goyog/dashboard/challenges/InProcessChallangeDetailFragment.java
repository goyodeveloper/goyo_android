package com.differenzsystem.goyog.dashboard.challenges;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.syncdevice;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.FitnessTrackerOperations;
import com.differenzsystem.goyog.api.GetChallangeValidicDataCall;
import com.differenzsystem.goyog.api.GetChallangeValidicDataCall.OnGetChallangeValidicDataListener;
import com.differenzsystem.goyog.api.GetGoogleFits;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.model.ChallangeValidicDataModel;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.utility.BaseFragment;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.veryfit.multi.share.BleSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class InProcessChallangeDetailFragment extends BaseFragment implements GetGoogleFits.OnGetFitsListener, GoogleApiClient.ConnectionCallbacks, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, OnGetChallangeValidicDataListener, FitnessTrackerOperations.FitnessTrackerOperationslistener, syncdevice.Onsyncdevice {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    String TAG = "Graph";
    ChallengesModel obj_Challenges;
    int distancePer = 0, stepPer = 0, caloriesPer = 0, heartRatePer = 0, sleepPer = 0;
    GetGoogleFits.OnGetFitsListener onGetFitsListener;
    OnGetChallangeValidicDataListener onGetChallangeValidicDataListener;
    FitnessTrackerOperations.FitnessTrackerOperationslistener fitnessTrackerOperationslistener;
    syncdevice.Onsyncdevice onsyncdevice;
    Boolean onPage = true;

    // TODO: Rename and change types of parameters
    static Bundle extras;
    GetGoogleFits getGoogleFits;

    ProgressBar progress_calories, progress_steps, progress_distance, progress_sleep, progress_heartRate;
    TextView tv_caloriesData, tv_distanceData, tv_stepsData, tv_heartRateData, remain_time, tv_sleepData;
    TextView tv_caloriesTotalData, tv_distanceTotalData, tv_stepsTotalData, tv_total_sleepData, tv_stepPer, tv_distancePer, tv_caloriesPer, tv_heartPer, tv_sleep_per;
    RelativeLayout rl_calories, rl_steps, rl_distance, rl_sleep;

    TextView tv_desc, tv_level, tv_level_name;
    ImageView iv_badge;

    int steps = 0, calories = 0, heartRate = 0, sleep = 0;
    int totalSteps = 0, totalCalories = 0, totalHeartRate = 0, totalSleep = 0, distance = 0, totalDistance = 0;
    //float ;

    LinearLayout ll_calories, ll_steps, ll_distance, ll_sleep, ll_heartRate;
    LinearLayout ll_caloriesData, ll_stepsData, ll_distanceData, ll_sleepData, ll_heartRateData;
    LinearLayout ll_graph, lnr_badge;

    LinearLayout lin_progress_calories, lin_progress_steps, lin_progress_distance, lin_progress_sleep, ll_progress_heartRate;

    LinearLayout ll_back;
    TextView tv_title;

    SwipeRefreshLayout swiperefresh;

    int height = 0;
    GoogleApiClient mClient;
    float scale;
    boolean isChallengeComplete = false;


    /**
     * Note
     * usertype 1 = live user
     * usertype 2 = demo user
     * connect_flag = connected_with_validic means get data from validic
     * connect_flag = connected_with_device means get data from google fit
     * connect_flag = connected_with_tracker means get data from band
     */

    public InProcessChallangeDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    // TODO: Rename and change types and number of parameters
    // create the new instance of screen
    public static InProcessChallangeDetailFragment newInstance(Bundle extra) {
        InProcessChallangeDetailFragment fragment = new InProcessChallangeDetailFragment();
        extras = extra;
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_in_process_challange_detail, container, false);
        initializeControls(view);
        init();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_InProcess_Challenge_Detail);
    }

    // initialize procees when screen loads
    public void init() {
        onGetFitsListener = this;
        onGetChallangeValidicDataListener = this;
        fitnessTrackerOperationslistener = this;
        onsyncdevice = this;
        scale = getContext().getResources().getDisplayMetrics().density;
        obj_Challenges = (ChallengesModel) extras.getSerializable("Object");

        // check if band is connected with device
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
            UtilsCommon.showProgressDialog(getActivity());

            mClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(Fitness.HISTORY_API)
                    .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                    .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
                    .addScope(new Scope(Scopes.FITNESS_BODY_READ))
                    .addConnectionCallbacks(this).build();
            mClient.connect();
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            UtilsCommon.showProgressDialog(getActivity());
            setChartForTracker();
        } else {
            if (ConnectionDetector.internetCheck(getActivity())) {
                JSONObject object = new JSONObject();
                try {
                    object.put(Constant.user_id, UtilsPreferences.getString(getContext(), Constant.user_id));
                    object.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));
                    object.put(Constant.device_token, UtilsPreferences.getString(getContext(), Constant.device_token));
                    object.put(Constant.validic_id, UtilsPreferences.getString(getContext(), Constant.validic_id));
                    object.put(Constant.challenge_id, obj_Challenges.getChallenge_id());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //call api GetChallangeValidicDataCall
                GetChallangeValidicDataCall task = new GetChallangeValidicDataCall(getActivity(), onGetChallangeValidicDataListener, object, true);
                task.execute();
            }
        }
    }

    // set chart full process by binding data from band
    public void setChartForTracker() {
        String startTimeString = "";
        int duration = Integer.parseInt(obj_Challenges.getDuration());
        String cStartDate = obj_Challenges.getChallenge_start_date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

/*
        if (cStartDate != null) {
            if (cStartDate.equalsIgnoreCase("0000-00-00 00:00:00") ||
                    cStartDate == null) {
                startTimeString = obj_Challenges.getAccept_date();
            } else {
                startTimeString = cStartDate;
            }
        } else {
            startTimeString = obj_Challenges.getAccept_date();
        }
*/
        startTimeString = obj_Challenges.getAccept_date();

        Date startDate = null;
        try {
            startDate = formater.parse(startTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar endCalendar = Calendar.getInstance();
        Date endDate = startDate;
        endCalendar.setTime(endDate);
        //int duration = Integer.parseInt(obj_Challenges.getDuration());
        endCalendar.add(Calendar.MINUTE, duration);
        //Date end = endCalendar.getTime();

        String chlngStartDate = formater.format(startDate);
        String chlngEndDate = formater.format(endCalendar.getTime());//
        Date endDiff = endCalendar.getTime();
        if (BleSharedPreferences.getInstance().getIsBind()) {
            FitnessTrackerOperations fitnessTrackerOperations = new FitnessTrackerOperations(getContext(), fitnessTrackerOperationslistener);
            fitnessTrackerOperations.readDatafromTracker(chlngStartDate, chlngEndDate, Integer.parseInt(obj_Challenges.getChallenge_type()));
        } else {
            doProcess(steps, calories, distance, heartRate, sleep);
        }
    }

    // success listener of FitnessTrackerOperations
    @Override
    public void onSucceedtoFitnessTrackerOperation(int steps, int cals, int dis, int hearts, int sleep) {
        doProcess(steps, cals, dis, hearts, sleep);
    }

    // set the chart by challenge type
    public void doProcess(int steps, int cals, int dis, int hearts, int sleep) {
        this.calories = cals;
        this.steps = steps;
        this.distance = dis;
        this.heartRate = hearts;
        this.sleep = sleep;

        switch (Integer.parseInt(obj_Challenges.getChallenge_type())) {
            case 1://steps
                setChart(steps);
                break;
            case 2://calories
                setChart(cals);
                break;
            case 3://distance
                setChart(dis);
                break;
            case 4://sleep
                setChart(sleep);
                //setChart(hearts);
                break;
            case 5://distance and sleep
                setChart(dis);
                this.distance = dis;
                setChart(sleep);
                break;
            case 6://step and distance
                setChart(steps);
                this.distance = dis;
                setChart(dis);
                break;
            case 7://steps and calories
                setChart(cals);
                setChart(steps);
                break;
            case 8://steps and sleep
                setChart(sleep);
                setChart(steps);
                break;
            case 9://Sleep & calories
                setChart(sleep);
                setChart(cals);
                break;
            case 10://Distance & calories
                setChart(dis);
                this.distance = dis;
                setChart(cals);
                break;
            case 11: //steps and calories and distance
                setChart(steps);
                this.distance = dis;
                setChart(cals);
                this.distance = dis;
                setChart(dis);
                break;
            case 12://steps and calories nad sleep
                setChart(steps);
                setChart(cals);
                setChart(sleep);
                break;
            case 13://steps and distance and sleep
                setChart(steps);
                this.distance = dis;
                setChart(dis);
                this.distance = dis;
                setChart(sleep);
                break;
            case 14://calories and distance and sleep
                setChart(cals);
                this.distance = dis;
                setChart(dis);
                this.distance = dis;
                setChart(sleep);
                break;
            case 15://steps and calories and distance and sleep
                setChart(steps);
                this.distance = dis;
                setChart(cals);
                this.distance = dis;
                setChart(dis);
                this.distance = dis;
                setChart(sleep);
                break;
        }
    }

    // initialize all controls define in xml layout
    public void initializeControls(View view) {
        try {
            swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_view);
            swiperefresh.setOnRefreshListener(this);
            swiperefresh.setColorSchemeResources(
                    R.color.colorPrimary);

            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            ll_back.setOnClickListener(this);
            ll_back.setVisibility(View.INVISIBLE);

            ll_graph = (LinearLayout) view.findViewById(R.id.ll_graph);
            lnr_badge = (LinearLayout) view.findViewById(R.id.lnr_badge);
            // Header
            ll_calories = (LinearLayout) view.findViewById(R.id.ll_calories);
            ll_steps = (LinearLayout) view.findViewById(R.id.ll_steps);
            ll_distance = (LinearLayout) view.findViewById(R.id.ll_distance);
            ll_sleep = (LinearLayout) view.findViewById(R.id.ll_sleep);
            ll_heartRate = (LinearLayout) view.findViewById(R.id.ll_heartRate);

            ll_caloriesData = (LinearLayout) view.findViewById(R.id.ll_caloriesData);
            ll_stepsData = (LinearLayout) view.findViewById(R.id.ll_stepsData);
            ll_distanceData = (LinearLayout) view.findViewById(R.id.ll_distanceData);
            ll_sleepData = (LinearLayout) view.findViewById(R.id.ll_sleepData);
            ll_heartRateData = (LinearLayout) view.findViewById(R.id.ll_heartRateData);

            lin_progress_calories = (LinearLayout) view.findViewById(R.id.lin_progress_calories);
            lin_progress_steps = (LinearLayout) view.findViewById(R.id.lin_progress_steps);
            lin_progress_distance = (LinearLayout) view.findViewById(R.id.lin_progress_distance);
            lin_progress_sleep = (LinearLayout) view.findViewById(R.id.lin_progress_sleep);
            ll_progress_heartRate = (LinearLayout) view.findViewById(R.id.ll_progress_heartRate);

            rl_calories = (RelativeLayout) view.findViewById(R.id.rl_calories);
            rl_distance = (RelativeLayout) view.findViewById(R.id.rl_distance);
            rl_steps = (RelativeLayout) view.findViewById(R.id.rl_step);
            rl_sleep = (RelativeLayout) view.findViewById(R.id.rl_sleep);

            remain_time = (TextView) view.findViewById(R.id.remain_time);
            tv_caloriesData = (TextView) view.findViewById(R.id.tv_caloriesData);
            tv_heartRateData = (TextView) view.findViewById(R.id.tv_heartRateData);
            tv_stepsData = (TextView) view.findViewById(R.id.tv_stepsData);
            tv_distanceData = (TextView) view.findViewById(R.id.tv_distanceData);
            tv_sleepData = (TextView) view.findViewById(R.id.tv_sleepData);

            tv_caloriesTotalData = (TextView) view.findViewById(R.id.tv_total_caloriesData);
            tv_stepsTotalData = (TextView) view.findViewById(R.id.tv_total_stepsData);
            tv_distanceTotalData = (TextView) view.findViewById(R.id.tv_total_distanceData);
            tv_total_sleepData = (TextView) view.findViewById(R.id.tv_total_sleepData);


            tv_distancePer = (TextView) view.findViewById(R.id.tv_distance_per);
            tv_caloriesPer = (TextView) view.findViewById(R.id.tv_calories_per);
            tv_stepPer = (TextView) view.findViewById(R.id.tv_steps_per);
            tv_sleep_per = (TextView) view.findViewById(R.id.tv_sleep_per);
            tv_heartPer = (TextView) view.findViewById(R.id.tv_heartRate_per);

            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_title.setText(getString(R.string.in_progress_lbl));

            tv_desc = (TextView) view.findViewById(R.id.tv_desc);
            tv_level_name = (TextView) view.findViewById(R.id.tv_level_name);
            tv_level = (TextView) view.findViewById(R.id.tv_level);
            iv_badge = (ImageView) view.findViewById(R.id.iv_badge);
            tv_desc.setMovementMethod(new ScrollingMovementMethod());

            progress_calories = (ProgressBar) view.findViewById(R.id.progress_calories);
            progress_steps = (ProgressBar) view.findViewById(R.id.progress_steps);
            progress_distance = (ProgressBar) view.findViewById(R.id.progress_distance);
            progress_sleep = (ProgressBar) view.findViewById(R.id.progress_sleep);
            progress_heartRate = (ProgressBar) view.findViewById(R.id.progress_heartRate);

            ll_graph.setOnClickListener(this);
            tv_desc.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            if (!swiperefresh.isRefreshing())
                                swiperefresh.setEnabled(false);
                            break;
                        case MotionEvent.ACTION_DOWN:
                            if (!swiperefresh.isRefreshing())
                                swiperefresh.setEnabled(false);
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            swiperefresh.setEnabled(true);
                            break;
                        case MotionEvent.ACTION_HOVER_EXIT:
                            swiperefresh.setEnabled(true);
                            break;
                    }
                    return false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getDP(int dps) {
        return (int) (dps * scale + 0.5f);
    }

    // set data in chart and populate in screen
    public void setupChart(final int type, int data) {
        if (ll_graph.getVisibility() == View.INVISIBLE)
            ll_graph.setVisibility(View.VISIBLE);
        if (obj_Challenges != null) {
            tv_level.setText(obj_Challenges.getName());
            //tv_level_name.setText(obj_Challenges.getDescription());
            tv_desc.setText(obj_Challenges.getDescription().replace("\\n", System.getProperty("line.separator")));
            remain_time.setText(obj_Challenges.getRemain_result());
            String url = getActivity().getResources().getString(R.string.server_url)
                    + getActivity().getResources().getString(R.string.challenge_images_url)
                    + obj_Challenges.getThumb_image();
            Glide.with(getActivity())
                    .load(url)
                    .placeholder(R.drawable.app_placeholder)
                    .into(iv_badge);
            setData(type);
        } else {
            tv_desc.setVisibility(View.INVISIBLE);
            rl_calories.setVisibility(View.INVISIBLE);
            rl_steps.setVisibility(View.INVISIBLE);
            rl_distance.setVisibility(View.INVISIBLE);
            rl_sleep.setVisibility(View.INVISIBLE);
            tv_heartRateData.setVisibility(View.INVISIBLE);
        }

        ViewTreeObserver observer = ll_graph.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                height = ll_graph.getHeight();
                Log.e(TAG, "Height " + height);
                if (rl_calories.getVisibility() == View.VISIBLE) {
                    if (caloriesPer >= 100) {
                        LinearLayout.LayoutParams caloriesParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                        progress_calories.setLayoutParams(caloriesParams);
                    } else {
                        LinearLayout.LayoutParams caloriesParams = new LinearLayout.LayoutParams(getDP(15), getDP(caloriesPer * 2));
                        progress_calories.setLayoutParams(caloriesParams);
                    }
                } else {
                    LinearLayout.LayoutParams caloriesParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_calories.setLayoutParams(caloriesParams);
                }
                if (rl_steps.getVisibility() == View.VISIBLE) {
                    if (stepPer >= 100) {
                        LinearLayout.LayoutParams stepsParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                        progress_steps.setLayoutParams(stepsParams);
                    } else {
                        LinearLayout.LayoutParams stepsParams = new LinearLayout.LayoutParams(getDP(15), getDP(stepPer * 2));
                        progress_steps.setLayoutParams(stepsParams);
                    }
                } else {
                    LinearLayout.LayoutParams stepsParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_steps.setLayoutParams(stepsParams);
                }
                if (rl_distance.getVisibility() == View.VISIBLE) {
                    if (distancePer >= 100) {
                        LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                        progress_distance.setLayoutParams(distanceParams);
                    } else {
                        LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(distancePer * 2));
                        progress_distance.setLayoutParams(distanceParams);
                    }
                } else {
                    LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_distance.setLayoutParams(distanceParams);
                }

                if (rl_sleep.getVisibility() == View.VISIBLE) {
                    if (sleepPer >= 100) {
                        LinearLayout.LayoutParams sleepParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                        progress_sleep.setLayoutParams(sleepParams);
                    } else {
                        LinearLayout.LayoutParams sleepParams = new LinearLayout.LayoutParams(getDP(15), getDP(sleepPer * 2));
                        progress_sleep.setLayoutParams(sleepParams);
                    }
                } else {
                    LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_sleep.setLayoutParams(distanceParams);
                }

                /*if (rl_sleep.getVisibility() == View.VISIBLE) {
                    LinearLayout.LayoutParams sleepParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_sleep.setLayoutParams(sleepParams);
                } else {
                    LinearLayout.LayoutParams sleepParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_sleep.setLayoutParams(sleepParams);
                }*/

               /* LinearLayout.LayoutParams heartRateParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                progress_heartRate.setLayoutParams(heartRateParams);*/

                if (heartRatePer >= 100) {
                    LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(200));
                    progress_heartRate.setLayoutParams(distanceParams);
                } else {
                    LinearLayout.LayoutParams distanceParams = new LinearLayout.LayoutParams(getDP(15), getDP(heartRatePer * 2));
                    progress_heartRate.setLayoutParams(distanceParams);
                }

                ll_graph.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        ObjectAnimator caloriesAnimation = ObjectAnimator.ofInt(progress_calories, "progress", 0, 100);
        caloriesAnimation.setDuration(1000);
        caloriesAnimation.setInterpolator(new DecelerateInterpolator());
        caloriesAnimation.start();

        ObjectAnimator stepsAnimation = ObjectAnimator.ofInt(progress_steps, "progress", 0, 100);
        stepsAnimation.setDuration(1000);
        stepsAnimation.setInterpolator(new DecelerateInterpolator());
        stepsAnimation.start();

        ObjectAnimator distanceAnimation = ObjectAnimator.ofInt(progress_distance, "progress", 0, 100);
        distanceAnimation.setDuration(1000);
        distanceAnimation.setInterpolator(new DecelerateInterpolator());
        distanceAnimation.start();

        ObjectAnimator sleepAnimation = ObjectAnimator.ofInt(progress_sleep, "progress", 0, 100);
        sleepAnimation.setDuration(1000);
        sleepAnimation.setInterpolator(new DecelerateInterpolator());
        sleepAnimation.start();

        ObjectAnimator heartRateAnimation = ObjectAnimator.ofInt(progress_heartRate, "progress", 0, 100);
        heartRateAnimation.setDuration(1000);
        heartRateAnimation.setInterpolator(new DecelerateInterpolator());
        heartRateAnimation.start();
    }

    /**
     * @param type set data for particular change type
     */
    public void setData(int type) {
        switch (type) {
            case 1://steps
                totalSteps = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");
                if (stepPer >= 100)
                    isChallengeComplete = true;
                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);
                lin_progress_steps.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 2://calories
                totalCalories = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");
                if (caloriesPer >= 100)
                    isChallengeComplete = true;
                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);
                lin_progress_calories.setAlpha(1);
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 3://distance
                //distance = UtilsCommon.getMiles(distance);
                //totalDistance = UtilsCommon.getMiles(Integer.parseInt(obj_Challenges.getSecond_challenge_value()));
                totalDistance = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(Integer.parseInt(obj_Challenges.getChallenge_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");
                if (distancePer >= 100)
                    isChallengeComplete = true;
                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);
                lin_progress_distance.setAlpha(1);
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 4://sleep
                totalSleep = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer >= 100)
                    isChallengeComplete = true;
                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);

                lin_progress_sleep.setAlpha(1);
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 5://distance and sleep
                totalDistance = Integer.parseInt(obj_Challenges.getChallenge_value());
                //totalDistance = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(Integer.parseInt(obj_Challenges.getChallenge_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);

                totalSleep = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (distancePer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                if (sleepPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                lin_progress_sleep.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 6://step and distance
                totalSteps = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                //distance = UtilsCommon.getMiles(distance);
                totalDistance = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(Integer.parseInt(obj_Challenges.getSecond_challenge_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (stepPer >= 100 && distancePer >= 100)
                    isChallengeComplete = true;
                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);
                lin_progress_steps.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 7://steps and calories
                totalSteps = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                totalCalories = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (stepPer >= 100 && caloriesPer >= 100)
                    isChallengeComplete = true;

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);
                lin_progress_steps.setAlpha(1);
                lin_progress_calories.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 8://steps and sleep
                totalSteps = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                totalSleep = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (stepPer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);
                lin_progress_steps.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 9://sleep and calories
                totalSleep = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);

                totalCalories = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");
                if (caloriesPer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;
                isChallengeComplete = true;
                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                lin_progress_calories.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 10://distance and calories
                totalDistance = Integer.parseInt(obj_Challenges.getChallenge_value());
                //totalDistance = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(Integer.parseInt(obj_Challenges.getChallenge_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);

                totalCalories = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");
                if (caloriesPer >= 100 && distancePer >= 100)
                    isChallengeComplete = true;
                isChallengeComplete = true;
                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                lin_progress_calories.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));

                break;

            case 11: //steps and calories and distance
                totalSteps = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                totalCalories = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);


                totalDistance = Integer.parseInt(obj_Challenges.getThird_challenge_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(Integer.parseInt(obj_Challenges.getThird_challenge_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);

                if (stepPer >= 100 && caloriesPer >= 100 && distancePer >= 100)
                    isChallengeComplete = true;

                lin_progress_steps.setAlpha(1);
                lin_progress_calories.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 12://steps and calories nad sleep
                totalSteps = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                totalCalories = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);


                totalSleep = Integer.parseInt(obj_Challenges.getThird_challenge_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);


                if (stepPer >= 100 && caloriesPer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                lin_progress_steps.setAlpha(1);
                lin_progress_calories.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;
            case 13://steps and distance and sleep
                totalSteps = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                totalDistance = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(Integer.parseInt(obj_Challenges.getSecond_challenge_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);


                totalSleep = Integer.parseInt(obj_Challenges.getThird_challenge_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);


                if (stepPer >= 100 && distancePer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                lin_progress_steps.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));

                break;
            case 14://calories and distance and sleep
                totalCalories = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);

                totalDistance = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(Integer.parseInt(obj_Challenges.getSecond_challenge_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);


                totalSleep = Integer.parseInt(obj_Challenges.getThird_challenge_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);


                if (caloriesPer >= 100 && distancePer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                lin_progress_calories.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));

                break;
            case 15://steps and calories and distance and sleep
                totalSteps = Integer.parseInt(obj_Challenges.getChallenge_value());
                rl_steps.setVisibility(View.VISIBLE);
                tv_stepsData.setText(String.valueOf(steps));
                tv_stepsTotalData.setText(String.valueOf(totalSteps));
                stepPer = Math.round((steps * 100) / totalSteps);
                stepPer = (stepPer > 100 ? 100 : stepPer);
                tv_stepPer.setText(stepPer + "%");

                if (stepPer <= 0)
                    lin_progress_steps.setVisibility(View.GONE);
                else
                    lin_progress_steps.setVisibility(View.VISIBLE);

                totalCalories = Integer.parseInt(obj_Challenges.getSecond_challenge_value());
                rl_calories.setVisibility(View.VISIBLE);
                tv_caloriesData.setText(String.valueOf(calories));
                tv_caloriesTotalData.setText(String.valueOf(totalCalories));
                caloriesPer = Math.round((calories * 100) / totalCalories);
                caloriesPer = (caloriesPer > 100 ? 100 : caloriesPer);
                tv_caloriesPer.setText(caloriesPer + "%");

                if (caloriesPer <= 0)
                    lin_progress_calories.setVisibility(View.GONE);
                else
                    lin_progress_calories.setVisibility(View.VISIBLE);


                totalDistance = Integer.parseInt(obj_Challenges.getThird_challenge_value());
                rl_distance.setVisibility(View.VISIBLE);
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles(distance)));
                tv_distanceTotalData.setText(String.valueOf(UtilsCommon.getMiles(Integer.parseInt(obj_Challenges.getThird_challenge_value()))));
                distancePer = Math.round((distance * 100) / totalDistance);
                distancePer = (distancePer > 100 ? 100 : distancePer);
                tv_distancePer.setText(distancePer + "%");

                if (distancePer <= 0)
                    lin_progress_distance.setVisibility(View.GONE);
                else
                    lin_progress_distance.setVisibility(View.VISIBLE);

                totalSleep = Integer.parseInt(obj_Challenges.getFourth_challenge_value());
                rl_sleep.setVisibility(View.VISIBLE);
                tv_sleepData.setText(String.valueOf(sleep / 60) + " h");
                tv_total_sleepData.setText(String.valueOf(totalSleep / 60) + " h");
                sleepPer = Math.round((sleep * 100) / totalSleep);
                sleepPer = (sleepPer > 100 ? 100 : sleepPer);
                tv_sleep_per.setText(sleepPer + "%");

                if (sleepPer <= 0)
                    lin_progress_sleep.setVisibility(View.GONE);
                else
                    lin_progress_sleep.setVisibility(View.VISIBLE);


                if (stepPer >= 100 && caloriesPer >= 100 && distancePer >= 100 && sleepPer >= 100)
                    isChallengeComplete = true;

                lin_progress_steps.setAlpha(1);
                lin_progress_calories.setAlpha(1);
                lin_progress_distance.setAlpha(1);
                lin_progress_sleep.setAlpha(1);
                progress_steps.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_calories.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_distance.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                progress_sleep.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));
                break;

            default:
                break;
        }
        //Duration calculation
        heartRate = UtilsCommon.setupDuration(obj_Challenges.getAccept_date());
        if (heartRate < 0) {
            heartRate = 0;
        }
        tv_heartRateData.setVisibility(View.VISIBLE);
        tv_heartRateData.setText(UtilsCommon.convertMinuts(heartRate, 1));

        heartRatePer = Math.round((heartRate * 100) / Integer.parseInt(obj_Challenges.getDuration()));
        heartRatePer = (heartRatePer > 100 ? 100 : heartRatePer);
        tv_heartPer.setText(heartRatePer + "%");
        progress_heartRate.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_anim));

        int diff = Integer.parseInt(obj_Challenges.getDuration()) - heartRate;
        if (diff < 0) {
            remain_time.setText(" Times up!");
        } else {
            remain_time.setText(UtilsCommon.convertMinuts(diff, 2));
        }

        //remain_time.setText((Integer.parseInt(obj_Challenges.getDuration()) / 60) - (heartRate / 60) + " hrs to go!");

        if (isChallengeComplete)
            lnr_badge.setAlpha(1);
    }

    /*public int setupDuration(String sDate) {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date startDate = null;
        Date curDate = null;
        try {
            startDate = formater.parse(sDate);
            curDate = formater.parse(formater.format(c.getTime()));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diffInMs = ((curDate.getTime() - startDate.getTime()));
        int min = (int) (TimeUnit.MILLISECONDS.toMinutes(diffInMs));
        //int min = (int) (TimeUnit.MILLISECONDS.toMinutes(diffInMs) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diffInMs)));
        Log.e("diffInMs", String.valueOf(diffInMs));

        return min;
    }*/

    // get calorie listener of CaloriesCall
    @Override
    public void onGetCalories(int cal) {
        calories = cal;
        setChart(cal);
    }


    //get steps listener of StepsCall
    @Override
    public void onGetSteps(int step) {
        steps = step;
        if (obj_Challenges.getChallenge_type().equalsIgnoreCase("6"))
            getGoogleFits.new DistanceCall().execute();
        else if (obj_Challenges.getChallenge_type().equalsIgnoreCase("7"))
            getGoogleFits.new CaloriesCall().execute();
        else
            setChart(step);
    }

    //get distance listener of DistanceCall
    @Override
    public void onGetDistance(int dist) {
        distance = dist;
        setChart(dist);
    }

    //get Heart rate listener of HeartRateCall
    @Override
    public void onGetHeartRate(int heart) {
        heartRate = heart;
        setChart(heart);
    }

    public void setChart(int data) {
        UtilsCommon.destroyProgressBar();
        swiperefresh.setRefreshing(false);
        setupChart(Integer.parseInt(obj_Challenges.getChallenge_type()), data);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            performAction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // get google fit data based on challenge type
    public void performAction() {
        if (extras != null) {
            getGoogleFits = new GetGoogleFits(getActivity(), false, obj_Challenges, mClient, onGetFitsListener);
                /*obj_Challenges.setChallenge_type("6");
                obj_Challenges.setSecond_challenge_value("80");*/
            if (obj_Challenges.getChallenge_type().equalsIgnoreCase("1")
                    || obj_Challenges.getChallenge_type().equalsIgnoreCase("6")
                    || obj_Challenges.getChallenge_type().equalsIgnoreCase("7"))
                getGoogleFits.new StepsCall().execute();
            else if (obj_Challenges.getChallenge_type().equalsIgnoreCase("2"))
                getGoogleFits.new CaloriesCall().execute();
            else if (obj_Challenges.getChallenge_type().equalsIgnoreCase("3"))
                getGoogleFits.new DistanceCall().execute();
            else if (obj_Challenges.getChallenge_type().equalsIgnoreCase("4"))
                getGoogleFits.new HeartRateCall().execute();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        UtilsCommon.destroyProgressBar();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                //addFragment(ChallengesAndOffersFragment.newInstance(null), Constant.challenges);
                break;
            case R.id.ll_graph:
                getActivity().onBackPressed();
                break;

            default:
                break;
        }
    }

    // swipe to refresh call and refresh all the data from band and update the layout
    @Override
    public void onRefresh() {
        swiperefresh.setRefreshing(true);
        steps = 0;
        calories = 0;
        heartRate = 0;
        totalDistance = 0;
        totalSteps = 0;
        totalCalories = 0;
        totalHeartRate = 0;
        distance = 0;

        getAndSetData();
    }

    // sync with band
    private void getAndSetData() {
        String connect_flag = UtilsPreferences.getString(getActivity(), Constant.connect_flag);
        if (connect_flag == null || connect_flag.isEmpty() || connect_flag.equalsIgnoreCase(Constant.connected_with_tracker)) {
            syncdevice s = new syncdevice(getActivity(), onsyncdevice);
            s.syncdata();
        } else {
            performAction();
        }
    }

    // success listener for GetChallangeValidicDataCall
    @Override
    public void onSucceedToGetChallangeValidicData(ChallangeValidicDataModel obj) {
        steps = Integer.parseInt(obj.getSteps());
        calories = Integer.parseInt(obj.getCalories());
        distance = Integer.parseInt(obj.getDistance());
        heartRate = Integer.parseInt(obj.getResting_heart_rate());
        setupChart(Integer.parseInt(obj_Challenges.getChallenge_type()), 0);

    }

    // failed listener for GetChallangeValidicDataCall
    @Override
    public void onFaildToGetChallangeValidicData(String error_msg) {

    }

    // remove resources
    @Override
    public void onStop() {
        super.onStop();
        if (HomeFragment.mClient != null && HomeFragment.mClient.isConnected()) {
            HomeFragment.mClient.stopAutoManage(getActivity());
            HomeFragment.mClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onPage = false;// managing fitness band sync
        if (HomeFragment.mClient != null && HomeFragment.mClient.isConnected()) {
            HomeFragment.mClient.stopAutoManage(getActivity());
            HomeFragment.mClient.disconnect();
        }
    }

    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
        fragmentTransaction.add(R.id.frame_main, fragment, null);//change add to replace
        fragmentTransaction.addToBackStack(null);//change tag to null
        fragmentTransaction.commit();
    }

    // success listener for sync device and set chart
    @Override
    public void onSucceedsyncdevice() {
        if (onPage) {
            swiperefresh.setRefreshing(false);
            setChartForTracker();
        }
    }
}
