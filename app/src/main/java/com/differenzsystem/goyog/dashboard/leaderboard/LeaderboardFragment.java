package com.differenzsystem.goyog.dashboard.leaderboard;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.adapter.LeaderboardAdapter;
import com.differenzsystem.goyog.api.LeaderBoardApi;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.LeaderboardDataModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class LeaderboardFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, LeaderBoardApi.OnLeaderBoardListener {


    public static String TAG = "LeaderboardFragment";
    public static LeaderboardFragment mContext;
    boolean isVisible = false;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.ll_back)
    LinearLayout ll_back;

    @BindView(R.id.tv_all)
    TextView tv_all;

    @BindView(R.id.tv_my_company)
    TextView tv_my_company;

    @BindView(R.id.recy_leaderboard_list)
    RecyclerView recy_leaderboard_list;

    @BindView(R.id.swipe_view_leaderboard_list)
    SwipeRefreshLayout swipe_view_leaderboard_list;

    @BindView(R.id.ll_active_challenge)
    LinearLayout ll_active_challenge;

    @BindView(R.id.ll_active_level)
    LinearLayout ll_active_level;

    @BindView(R.id.ll_calories)
    LinearLayout ll_calories;

    @BindView(R.id.ll_steps)
    LinearLayout ll_steps;

    @BindView(R.id.ll_distance)
    LinearLayout ll_distance;

    @BindView(R.id.sp_select_days)
    Spinner sp_select_days;

    @BindView(R.id.ll_select_days)
    LinearLayout ll_select_days;

    @BindView(R.id.tv_select_days_type)
    TextView tv_select_days_type;

    @BindView(R.id.img_active_level)
    CircleImageView img_active_level;

    @BindView(R.id.img_active_challenge)
    CircleImageView img_active_challenge;

    @BindView(R.id.img_calorie)
    ImageView img_calorie;

    @BindView(R.id.img_steps)
    ImageView img_steps;

    @BindView(R.id.img_distance)
    ImageView img_distance;

    @BindView(R.id.lin_my_company_no_data)
    LinearLayout lin_my_company_no_data;

    @BindView(R.id.tv_no_data_list)
    TextView tv_no_data_list;

    @BindView(R.id.ll_header)
    LinearLayout ll_header;

    @BindView(R.id.ll_tab)
    LinearLayout ll_tab;

    @BindView(R.id.tv_demouser)
    TextView tv_demouser;

    LeaderboardAdapter leaderboardAdapter;

    String[] select_days = {"Daily", "Weekly", "Monthly"};
    LeaderBoardApi.OnLeaderBoardListener onLeaderBoardListener;

    ArrayList<LeaderboardDataModel.Data> dataArrayList = new ArrayList<>();

    // dialy = 1, weekly = 2, monthly = 3
    String str_filter_id = Constant.filter_daily;
    // active challenge = 1, active level = 2, calorie = 3, steps = 4, distance = 5
    String str_option_id = Constant.option_activechallenge;
    // all = 1, My Company = 2
    String str_tab_id = Constant.tab_all;

    private boolean isChallengesAvailable = false;

    boolean isCompanyClick = false;
    int check = 0; // to avoid spinner calling onitemselectedlistener during initialization

//    private FirebaseAnalytics mFirebaseAnalytics;


    /**
     * Note
     * usertype 1 = live user
     * usertype 2 = demo user
     * connect_flag = connected_with_validic means get data from validic
     * connect_flag = connected_with_device means get data from google fit
     * connect_flag = connected_with_tracker means get data from band
     */


    // create the new instance of screen
    public static LeaderboardFragment newInstance(Bundle extra) {
        LeaderboardFragment fragment = new LeaderboardFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    public static LeaderboardFragment getInstance() {
        // return the instance of screen
        return mContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            isVisible = true;
        else
            isVisible = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.leaderboard_fragment, container, false);
        ButterKnife.bind(this, view);
        mContext = this;
        onLeaderBoardListener = this;

        // initialize controls for live users
        if (!UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
            ll_header.setVisibility(View.GONE);
            ll_tab.setVisibility(View.GONE);
            swipe_view_leaderboard_list.setVisibility(View.GONE);
            ll_back.setVisibility(View.INVISIBLE);
            tv_title.setText(R.string.leaderboard);
            tv_demouser.setVisibility(View.VISIBLE);
            return view;
        }

        swipe_view_leaderboard_list.setOnRefreshListener(this);
        ll_back.setVisibility(View.INVISIBLE);
        tv_title.setText(R.string.leaderboard);
        ll_select_days.setVisibility(View.VISIBLE);

        // default tab when screen loads
        defaultFilterTab();

        ArrayAdapter daysAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_layout, select_days) {

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTypeface(setFont(getContext(), R.string.sf_regular));

                return view;
            }

            @NonNull
            public TextView getView(int position, View convertView, @NonNull ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(setFont(getContext(), R.string.sf_regular));
                return v;
            }

        };
        sp_select_days.setAdapter(daysAdapter);
        sp_select_days.setDropDownVerticalOffset(85); // set the vertical padding below textview

        // select days or weekly or monthly
        sp_select_days.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
//                Toast.makeText(getActivity(), "selected", Toast.LENGTH_SHORT).show();
                tv_select_days_type.setText(sp_select_days.getSelectedItem().toString());

                switch (pos) {
                    case 0:
                        str_filter_id = Constant.filter_daily;
                        if (++check > 1) {
                            callCommonFunctionsApi();
                        }
                        break;
                    case 1:
                        str_filter_id = Constant.filter_weekly;
                        if (++check > 1) {
                            callCommonFunctionsApi();
                        }
                        break;
                    case 2:
                        str_filter_id = Constant.filter_monthly;
                        if (++check > 1) {
                            callCommonFunctionsApi();
                        }
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
//                Toast.makeText(getActivity(), "not selected", Toast.LENGTH_SHORT).show();
            }
        });

        tv_select_days_type.setText(sp_select_days.getSelectedItem().toString());

        return view;
    }

    // select days or weekly or monthly
    @OnClick(R.id.ll_select_days)
    public void openSpinner() {
        sp_select_days.performClick();
    }

    // load adapter for leaderboard list
    private void initializeAdapters() {
        recy_leaderboard_list.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recy_leaderboard_list.setLayoutManager(layoutManager);
        leaderboardAdapter = new LeaderboardAdapter(getActivity(), dataArrayList, str_filter_id, str_option_id, str_tab_id);
        recy_leaderboard_list.setAdapter(leaderboardAdapter);
        leaderboardAdapter.notifyDataSetChanged();

        scrollToPosition();
    }

    // scroll to position to current user
    private void scrollToPosition() {
        try {
            int pos = 0;
            for (int i = 0; i < dataArrayList.size(); i++) {
                if (dataArrayList.get(i).user_id.equalsIgnoreCase(UtilsPreferences.getString(getActivity(), Constant.user_id)))
                    pos = i;
            }
            recy_leaderboard_list.getLayoutManager().scrollToPosition(pos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // default filter selection when screen loads
    private void defaultFilterTab() {
        tv_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        tv_my_company.setTextColor(ContextCompat.getColor(getActivity(), R.color.tab));
        tv_all.setBackgroundResource(R.drawable.bg_tab_left_without_corner);
        tv_my_company.setBackgroundResource(R.drawable.bg_tab_transparent);
        str_filter_id = Constant.filter_daily; // by default
        str_tab_id = Constant.tab_all;
        isCompanyClick = false;

        if (Globals.inPChallengesChartDataMode != null) {

            if (Globals.inPChallengesChartDataMode.size() > 0) {
                ll_active_level.setVisibility(View.VISIBLE);

                str_option_id = Constant.option_activelevel;

                String level_image_url = getResources().getString(R.string.server_url)
                        + getResources().getString(R.string.badges_image)
                        + Globals.inPChallengesChartDataMode.get(0).getLevelInformationModel().getLevel_image();

                Glide.with(mContext)
                        .load(level_image_url)
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .dontAnimate()
                        .into(img_active_level);

                img_active_level.setBorderColor(Color.WHITE);
                img_active_level.setBorderWidth(5);
                ll_select_days.setVisibility(View.INVISIBLE);
                ll_select_days.setEnabled(false);
                ll_select_days.setClickable(false);

                isChallengesAvailable = false;
            }

            if (Globals.inPChallengesChartDataMode.size() > 1) {

                if (Globals.inPChallengesChartDataMode.get(1).getChallengesModels() != null) {
                    isChallengesAvailable = true;
                    ll_active_challenge.setVisibility(View.VISIBLE);

                    str_option_id = Constant.option_activechallenge;

                    String active_challenge_url = getResources().getString(R.string.server_url)
                            + getResources().getString(R.string.challenge_images_url)
                            + Globals.inPChallengesChartDataMode.get(1).getChallengesModels().getThumb_image();

                    Glide.with(mContext)
                            .load(active_challenge_url)
                            .centerCrop()
                            .placeholder(R.mipmap.ic_launcher)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .dontAnimate()
                            .into(img_active_challenge);

                    img_active_level.setBorderWidth(0);
                    img_active_challenge.setBorderColor(Color.WHITE);
                    img_active_challenge.setBorderWidth(5);
                    ll_select_days.setVisibility(View.INVISIBLE);
                    ll_select_days.setEnabled(false);
                    ll_select_days.setClickable(false);

                    isChallengesAvailable = true;

                }
            }

            if (isChallengesAvailable) {
                callLeaderBoardApi(getLeaderBoardActiveChallengeJsonObject());
            } else {
                callLeaderBoardApi(getLeaderBoardActiveLevelJsonObject());
            }
        }
    }

    // handle all click
    @OnClick(R.id.tv_all)
    public void allClick() {
        tv_no_data_list.setVisibility(View.GONE);
        lin_my_company_no_data.setVisibility(View.GONE);
        recy_leaderboard_list.setVisibility(View.VISIBLE);
        str_tab_id = Constant.tab_all;
        isCompanyClick = false;

        tv_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        tv_my_company.setTextColor(ContextCompat.getColor(getActivity(), R.color.tab));
        tv_all.setBackgroundResource(R.drawable.bg_tab_left_without_corner);
        tv_my_company.setBackgroundResource(R.drawable.bg_tab_transparent);

        callCommonFunctionsApi();
    }

    // here call leaderboard api based on option id
    // option id can be any one of following
    // active challenge = 1, active level = 2, calorie = 3, steps = 4, distance = 5
    private void callCommonFunctionsApi() {
        switch (str_option_id) {
            case "1":
                callLeaderBoardApi(getLeaderBoardActiveChallengeJsonObject());
                break;
            case "2":
                callLeaderBoardApi(getLeaderBoardActiveLevelJsonObject());
                break;
            case "3":
                callLeaderBoardApi(getLeaderBoardCalorieStepsDistanceJsonObject());
                break;
            case "4":
                callLeaderBoardApi(getLeaderBoardCalorieStepsDistanceJsonObject());
                break;
            case "5":
                callLeaderBoardApi(getLeaderBoardCalorieStepsDistanceJsonObject());
                break;
        }
    }

    // handle my company click
    @OnClick(R.id.tv_my_company)
    public void myCompanyClick() {

        isCompanyClick = true;
        str_tab_id = Constant.tab_mycompany;

        tv_my_company.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        tv_all.setTextColor(ContextCompat.getColor(getActivity(), R.color.tab));
        tv_my_company.setBackgroundResource(R.drawable.bg_tab_left_without_corner);
        tv_all.setBackgroundResource(R.drawable.bg_tab_transparent);
        callCommonFunctionsApi();

    }

    // handle active challenge click
    @OnClick(R.id.ll_active_challenge)
    public void activeChallengeClick() {
        tv_no_data_list.setVisibility(View.GONE);
        lin_my_company_no_data.setVisibility(View.GONE);
        if (!str_option_id.equalsIgnoreCase(Constant.option_activechallenge)) {
            str_option_id = Constant.option_activechallenge;
            callLeaderBoardApi(getLeaderBoardActiveChallengeJsonObject());
        }

        sp_select_days.setSelection(0);

//        Toast.makeText(getActivity(), "Active Challenge Click", Toast.LENGTH_SHORT).show();
        ll_select_days.setVisibility(View.INVISIBLE);
        ll_select_days.setEnabled(false);
        ll_select_days.setClickable(false);
        img_active_challenge.setBorderColor(Color.WHITE);
        img_active_challenge.setBorderWidth(5);
        img_active_level.setBorderWidth(0);
        img_calorie.setImageResource(R.drawable.calories);
        img_steps.setImageResource(R.drawable.steps);
        img_distance.setImageResource(R.drawable.distance);

    }

    // handle active level click
    @OnClick(R.id.ll_active_level)
    public void activeLevelClick() {
        tv_no_data_list.setVisibility(View.GONE);
        lin_my_company_no_data.setVisibility(View.GONE);
        if (!str_option_id.equalsIgnoreCase(Constant.option_activelevel)) {
            str_option_id = Constant.option_activelevel;
            callLeaderBoardApi(getLeaderBoardActiveLevelJsonObject());
        }


        sp_select_days.setSelection(0);

//        Toast.makeText(getActivity(), "Active Level Click", Toast.LENGTH_SHORT).show();
        ll_select_days.setVisibility(View.INVISIBLE);
        ll_select_days.setEnabled(false);
        ll_select_days.setClickable(false);
        img_active_level.setBorderColor(Color.WHITE);
        img_active_level.setBorderWidth(5);
        img_active_challenge.setBorderWidth(0);
        img_calorie.setImageResource(R.drawable.calories);
        img_steps.setImageResource(R.drawable.steps);
        img_distance.setImageResource(R.drawable.distance);
    }

    // handle calorie click
    @OnClick(R.id.ll_calories)
    public void caloriesClick() {
        tv_no_data_list.setVisibility(View.GONE);
        lin_my_company_no_data.setVisibility(View.GONE);

        if (!str_option_id.equalsIgnoreCase(Constant.option_calorie)) {
            str_option_id = Constant.option_calorie;
            callLeaderBoardApi(getLeaderBoardCalorieStepsDistanceJsonObject());
        }

//        Toast.makeText(getActivity(), "Calories Click", Toast.LENGTH_SHORT).show();
        ll_select_days.setVisibility(View.VISIBLE);
        ll_select_days.setEnabled(true);
        ll_select_days.setClickable(true);
        img_calorie.setImageResource(R.drawable.calories_new1);
        img_steps.setImageResource(R.drawable.steps);
        img_distance.setImageResource(R.drawable.distance);
        img_active_level.setBorderWidth(0);
        img_active_challenge.setBorderWidth(0);


    }

    // handle steps click
    @OnClick(R.id.ll_steps)
    public void stepsClick() {
        tv_no_data_list.setVisibility(View.GONE);
        lin_my_company_no_data.setVisibility(View.GONE);

        if (!str_option_id.equalsIgnoreCase(Constant.option_steps)) {
            str_option_id = Constant.option_steps;
            callLeaderBoardApi(getLeaderBoardCalorieStepsDistanceJsonObject());
        }


//        Toast.makeText(getActivity(), "Steps Click", Toast.LENGTH_SHORT).show();
        ll_select_days.setVisibility(View.VISIBLE);
        ll_select_days.setEnabled(true);
        ll_select_days.setClickable(true);
        img_steps.setImageResource(R.drawable.steps_new1);
        img_calorie.setImageResource(R.drawable.calories);
        img_distance.setImageResource(R.drawable.distance);
        img_active_level.setBorderWidth(0);
        img_active_challenge.setBorderWidth(0);

    }

    // handle distance click
    @OnClick(R.id.ll_distance)
    public void distanceClick() {
        tv_no_data_list.setVisibility(View.GONE);
        lin_my_company_no_data.setVisibility(View.GONE);
        if (!str_option_id.equalsIgnoreCase(Constant.option_distance)) {
            str_option_id = Constant.option_distance;
            callLeaderBoardApi(getLeaderBoardCalorieStepsDistanceJsonObject());
        }

//        Toast.makeText(getActivity(), "Distance Click", Toast.LENGTH_SHORT).show();
        ll_select_days.setVisibility(View.VISIBLE);
        ll_select_days.setEnabled(true);
        ll_select_days.setClickable(true);
        img_distance.setImageResource(R.drawable.distance_new1);
        img_calorie.setImageResource(R.drawable.calories);
        img_steps.setImageResource(R.drawable.steps);
        img_active_level.setBorderWidth(0);
        img_active_challenge.setBorderWidth(0);

        callLeaderBoardApi(getLeaderBoardCalorieStepsDistanceJsonObject());

    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Leaderboard);
    }

    private void recordScreenViews() {
        // [START set_current_screen]
//        mFirebaseAnalytics.setCurrentScreen(getActivity(), "Leaderboard Screen", null /* class override */);
        // [END set_current_screen]
    }

    // swipe to refresh process
    @Override
    public void onRefresh() {
        swipe_view_leaderboard_list.setRefreshing(false);
        callCommonFunctionsApi();
    }

    // here create the JsonObject for active challenge which is used as Json Post request in LeaderBoardApi Call
    private JSONObject getLeaderBoardActiveChallengeJsonObject() {

        JSONObject data = new JSONObject();

        if (Globals.inPChallengesChartDataMode != null && Globals.inPChallengesChartDataMode.size() > 0) {

            String str_Challenge_percentage = "0", str_challenge_id = "0", str_Challenge_minutes = "0";

            if (Globals.inPChallengesChartDataMode.size() > 1) {

                str_Challenge_minutes = UtilsCommon.getTotalLevelChallengeMins(Globals.inPChallengesChartDataMode.get(1).getChallengesModels().getAccept_date());
                str_Challenge_percentage = String.valueOf(UtilsCommon.getPercentageValue(1)); // 1 for challenge
                str_challenge_id = Globals.inPChallengesChartDataMode.get(1).getChallengesModels().getChallenge_id();

            }

            try {

                data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
                data.put(Constant.challenge_per, str_Challenge_percentage);
                data.put(Constant.challenge_id, str_challenge_id);
                data.put(Constant.challenge_min, str_Challenge_minutes);
                data.put(Constant.option_id, Constant.option_activechallenge);
                if (isCompanyClick)
                    data.put(Constant.tab_id, str_tab_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return data;
    }

    // here create the JsonObject for active level which is used as Json Post request in LeaderBoardApi Call
    private JSONObject getLeaderBoardActiveLevelJsonObject() {

        JSONObject data = new JSONObject();

        if (Globals.inPChallengesChartDataMode != null && Globals.inPChallengesChartDataMode.size() > 0) {

            String str_level_id = "0";

            if (Globals.inPChallengesChartDataMode.size() > 0) {
                str_level_id = Globals.inPChallengesChartDataMode.get(0).getLevelInformationModel().getBadges_level_id();
            }

            try {

                data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
                data.put(Constant.level_id, str_level_id);
                data.put(Constant.option_id, Constant.option_activelevel);
                if (isCompanyClick)
                    data.put(Constant.tab_id, str_tab_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return data;
    }

    // here create the JsonObject for calorie, steps, distance which is used as Json Post request in LeaderBoardApi Call
    private JSONObject getLeaderBoardCalorieStepsDistanceJsonObject() {

        JSONObject data = new JSONObject();

        if (Globals.inPChallengesChartDataMode != null && Globals.inPChallengesChartDataMode.size() > 0) {

            String str_level_id = "0";

            if (Globals.inPChallengesChartDataMode.size() > 0) {
                str_level_id = Globals.inPChallengesChartDataMode.get(0).getLevelInformationModel().getBadges_level_id();
            }

            try {

                data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
                data.put(Constant.option_id, str_option_id);
                data.put(Constant.filter_id, str_filter_id);
                if (isCompanyClick)
                    data.put(Constant.tab_id, str_tab_id);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return data;
    }

    /**
     * @param data Json post request
     */
    private void callLeaderBoardApi(JSONObject data) {
        try {
            swipe_view_leaderboard_list.setRefreshing(false);
            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
            if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                LeaderBoardApi leaderBoardApi = new LeaderBoardApi(getActivity(), onLeaderBoardListener, data, true);
                leaderBoardApi.execute();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // success listener of LeaderBoardApi and then bind the data
    @Override
    public void onSucceedToLeaderBoard(LeaderboardDataModel obj) {
        tv_no_data_list.setVisibility(View.GONE);
        dataArrayList.clear();
        if (leaderboardAdapter != null) {
            leaderboardAdapter.notifyDataSetChanged();
        }

        dataArrayList = (ArrayList<LeaderboardDataModel.Data>) obj.data;
        if (dataArrayList != null && dataArrayList.size() > 0) {
            initializeAdapters();
        } else {
            if (isCompanyClick) {
                lin_my_company_no_data.setVisibility(View.VISIBLE);
            } else {
                tv_no_data_list.setVisibility(View.VISIBLE);
            }
        }
    }

    // failed listener of LeaderBoardApi
    @Override
    public void onFaildToLeaderBoard(String error_msg) {
        Log.d(TAG, " onFaildToLeaderBoard : " + error_msg);
        dataArrayList.clear();
        if (leaderboardAdapter != null) {
            leaderboardAdapter.notifyDataSetChanged();
        }
        if (isCompanyClick) {
            tv_no_data_list.setVisibility(View.GONE);
            lin_my_company_no_data.setVisibility(View.VISIBLE);
        } else {
            lin_my_company_no_data.setVisibility(View.GONE);
            tv_no_data_list.setVisibility(View.VISIBLE);
        }
    }

}

