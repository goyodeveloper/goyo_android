package com.differenzsystem.goyog.dashboard.challenges;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.BaseFragment;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class ChallengesAndOffersFragment extends BaseFragment implements View.OnClickListener {

    TextView tv_challenges, tv_offers;
    FrameLayout frame_challengesAndOffers;
    boolean isVisible = false;
    public static ChallengesAndOffersFragment mContext;

    //sync animation
    LinearLayout ll_sync_progress;
    ProgressBar pb_sync;
    TextView tv_syncstatus;
    ImageView img_sync;
    final Handler handler = new Handler();
    Runnable runnable;

    public static ChallengesAndOffersFragment newInstance(Bundle extra) {
        ChallengesAndOffersFragment fragment = new ChallengesAndOffersFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.challenges_and_offers_fragment, container, false);
        initializeControls(view);
        initializeControlsAction();
        defaultFragment();
        if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
            scheduleThread();
        }
        return view;
    }

    // sync animation
    public void scheduleThread() {
        handler.postDelayed(new Runnable() {
            public void run() {
                runnable = this;
                if (UtilsCommon.checkBlutoothConnectivity()) {

                    UtilsCommon.expand(ll_sync_progress);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 200ms
                            UtilsCommon.collapse(ll_sync_progress);
                        }
                    }, Constant.PostDelaySub);
                }
                handler.postDelayed(runnable, Constant.PostDelayMain);
            }
        }, Constant.PostDelayMain);
    }

    public static ChallengesAndOffersFragment getInstance() {
        // return the context of ChallengesAndOffersFragment
        return mContext;
    }

    // initialize all controls define in xml layout
    public void initializeControls(View view) {
        try {
            mContext = this;
            tv_challenges = (TextView) view.findViewById(R.id.tv_challenges);
            tv_offers = (TextView) view.findViewById(R.id.tv_offers);
            frame_challengesAndOffers = (FrameLayout) view.findViewById(R.id.frame_challengesAndOffers);

            //sync animation
            ll_sync_progress = (LinearLayout) view.findViewById(R.id.ll_sync_progress);
            pb_sync = (ProgressBar) view.findViewById(R.id.pb_sync);
            tv_syncstatus = (TextView) view.findViewById(R.id.tv_syncstatus);
            img_sync = (ImageView) view.findViewById(R.id.img_sync);

            ll_sync_progress.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            tv_challenges.setOnClickListener(this);
            tv_offers.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            isVisible = true;
        else
            isVisible = false;
    }

    // handle click event
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_challenges:
                addFragment(ChallengesFragment.newInstance(null), "");
                tv_challenges.setTextColor(getResources().getColor(R.color.black));
                tv_offers.setTextColor(getResources().getColor(R.color.tab));
                tv_challenges.setBackgroundResource(R.drawable.bg_tab_left);
                tv_offers.setBackgroundResource(R.drawable.bg_tab_transparent);
                break;

            case R.id.tv_offers:
                addFragment(OffersFragment.newInstance(null), "");
                tv_challenges.setTextColor(getResources().getColor(R.color.tab));
                tv_offers.setTextColor(getResources().getColor(R.color.black));
                tv_challenges.setBackgroundResource(R.drawable.bg_tab_transparent);
                tv_offers.setBackgroundResource(R.drawable.bg_tab_right);
                break;

            default:
                break;
        }
    }

    // open required screen
    /**
     *
     * @param fragment add fragment
     */
    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getChildFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_challengesAndOffers, fragment, null);
        fragmentTransaction.commit();
    }

    // set the default screen
    public void defaultFragment() {
        addFragment(ChallengesFragment.newInstance(null), "");
        tv_challenges.setTextColor(getResources().getColor(R.color.black));
        tv_offers.setTextColor(getResources().getColor(R.color.tab));
        tv_challenges.setBackgroundResource(R.drawable.bg_tab_left);
        tv_offers.setBackgroundResource(R.drawable.bg_tab_transparent);
    }

    @Override
    public void onPause() {
        handler.removeCallbacks(runnable); // remove handler
        super.onPause();
    }
}
