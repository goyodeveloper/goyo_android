package com.differenzsystem.goyog.dashboard.home;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.classes.textcounter.CounterView;
import android.classes.textcounter.Formatter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.fitchart.FitChartValue;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.BandBaseFragment;
import com.differenzsystem.goyog.FitnessBandTracker.ScanDeviceActivity;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.adapter.HomeSlidingChartAdapter;
import com.differenzsystem.goyog.api.FitnessTrackerOperations;
import com.differenzsystem.goyog.api.FitnessTrackerOperations.FitnessTrackerOperationslistener;
import com.differenzsystem.goyog.api.GetFitnessDataFromValidicCall;
import com.differenzsystem.goyog.api.GetFitnessDataFromValidicCall.OnGetFitnessFromValidicListener;
import com.differenzsystem.goyog.api.GetLevelInformationCall;
import com.differenzsystem.goyog.api.GetLevelInformationCall.OnGetLevelInformationListener;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall.OnGetRoutineFromValidicListener;
import com.differenzsystem.goyog.api.GetSleepDataFromValidicCall;
import com.differenzsystem.goyog.api.GetSleepDataFromValidicCall.OnGetSleepDataFromValidicListener;
import com.differenzsystem.goyog.api.NextLevelChallengeListing;
import com.differenzsystem.goyog.api.StepsFromDeviceCall.OnGetStepDataListener;
import com.differenzsystem.goyog.api.UpdateChallengesStatusInBackground;
import com.differenzsystem.goyog.api.UpdateChallengesStatusInBackgroundForTracker;
import com.differenzsystem.goyog.api.UpdateDeviceDataInBackground;
import com.differenzsystem.goyog.api.UpdateLevel;
import com.differenzsystem.goyog.api.UserLastChallengeLevelSyncApi;
import com.differenzsystem.goyog.api.User_last_sync;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.dashboard.challenges.WonActivity;
import com.differenzsystem.goyog.dashboard.home.calories.CaloriesFragment;
import com.differenzsystem.goyog.dashboard.home.distance.DistanceFragment;
import com.differenzsystem.goyog.dashboard.home.heartRate.HeartRateFragment;
import com.differenzsystem.goyog.dashboard.home.sleep.SleepFragment;
import com.differenzsystem.goyog.dashboard.home.step.StepsFragment;
import com.differenzsystem.goyog.dashboard.settings.SettingsFragment;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.model.ChartDataModel;
import com.differenzsystem.goyog.model.LevelInformationModel;
import com.differenzsystem.goyog.model.NextLevelChallengeModel;
import com.differenzsystem.goyog.model.ValidicFitnessModel;
import com.differenzsystem.goyog.model.ValidicRoutineModel;
import com.differenzsystem.goyog.model.ValidicSleepModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPermission;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.veryfit.multi.nativedatabase.HealthHeartRate;
import com.veryfit.multi.nativedatabase.HealthHeartRateAndItems;
import com.veryfit.multi.nativedatabase.HealthSport;
import com.veryfit.multi.nativedatabase.HealthSportAndItems;
import com.veryfit.multi.nativedatabase.healthSleep;
import com.veryfit.multi.nativedatabase.healthSleepAndItems;
import com.veryfit.multi.nativeprotocol.ProtocolEvt;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


import static com.differenzsystem.goyog.Application.mFirebaseAnalytics;
import static java.text.DateFormat.getTimeInstance;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class HomeFragment extends BandBaseFragment implements OnClickListener, OnGetFitnessFromValidicListener,
        OnGetRoutineFromValidicListener, OnGetSleepDataFromValidicListener, OnGetLevelInformationListener,
        ConnectionCallbacks, OnGetStepDataListener, SwipeRefreshLayout.OnRefreshListener,
        UpdateChallengesStatusInBackground.OnUpdateChallengesStatusInBackgroundListener,
        UpdateChallengesStatusInBackgroundForTracker.onUpdateChallengesStatusInBackgroundForTracker,
        FitnessTrackerOperations.FitnessTrackerOperationslistener, UpdateLevel.OnUpdateLevelListener, User_last_sync.OnGetUserLastSyncData,
        NextLevelChallengeListing.OnGetNextLevelChallengeListingListener, UserLastChallengeLevelSyncApi.OnUserLastChallengeLevelSyncApi {
    public static String TAG = "HomeFragment";

    LinearLayout ll_calories, ll_steps, ll_distance, ll_sleep, ll_heartRate;
    CounterView tv_caloriesData, tv_stepsData, tv_distanceData, tv_heartRateData;
    TextView tv_sleepData;
    // TextView tv_level, tv_level_name, tv_sleepData;
    //  ImageView iv_home_badge;
    LinearLayout lin_main;
    // FitChart fitChart;

    ImageView img_left_swipe, img_right_swipe;

    SwipeRefreshLayout swiperefresh;

    public static GoogleApiClient mClient = null;
    public static GoogleApiClient mGoogleApiFitnessClient = null;
    private static final String AUTH_PENDING = "auth_state_pending";
    private static boolean authInProgress = false;
    public static LevelInformationModel levelData = null;
    int totalLevelCount = 0, totalLevel = 1;
    String levelStartDate = null, levelendDate = null;

    String caloriesData = "0", stepsData = "0", distanceData = "0", heartRateData = "0";
    String levelCaloriesData = "0", levelStepsData = "0", levelDistanceData = "0", levelHeartRateData = "0", levelSleepData = "0";
    int totalStep = 0, totalCal = 0, totalDistance = 0;

    OnGetFitnessFromValidicListener onGetFitnessFromValidicListener;
    OnGetRoutineFromValidicListener onGetRoutineFromValidicListener;
    OnGetSleepDataFromValidicListener onGetSleepDataFromValidicListener;
    OnGetLevelInformationListener onGetLevelInformationListener;
    OnGetStepDataListener OnGetStepDataListener;
    FitnessTrackerOperations.FitnessTrackerOperationslistener fitnessTrackerOperationslistener;
    UpdateLevel.OnUpdateLevelListener onUpdateLevelListener;
    User_last_sync.OnGetUserLastSyncData onGetUserLastSyncData;

    //change
    UpdateChallengesStatusInBackground.OnUpdateChallengesStatusInBackgroundListener UpdateChallengesStatusInBackgroundListener;
    UpdateChallengesStatusInBackgroundForTracker.onUpdateChallengesStatusInBackgroundForTracker onUpdateChallengesStatusInBackgroundForTracker;

    boolean isVisible = true;
    public static HomeFragment mContext;

    int levelPer = 0, secondLevelPer = 0;
    String status;

    //change
    static ViewPager viewPager;
    static CirclePageIndicator indicator;
    static HomeSlidingChartAdapter adapter;
    ArrayList<ChartDataModel> chartmodellist = new ArrayList<>();

    boolean isChallengesAvailable = false;
    boolean isRefresh = false;

    //sync animation
    LinearLayout ll_sync_progress_main;
    ProgressBar pb_sync_main;
    TextView tv_syncstatus_main, tv_goyohr_main;
    ImageView img_sync, img_err;
    final Handler handler1 = new Handler();
    Runnable runnable;
    LinearLayout ll_sync_progress;
    private ImageView img_header_sync, img_header_setting;

    private int progressStatus = 0;
    private Handler handler = new Handler();
    private Handler mHandler = new Handler();
    HealthSport hs = new HealthSport();
    boolean syncavailable = true;
    boolean onpage = true;
    LinearLayout ll_chart;
    Bundle savedInstanceState;
    boolean isBleConnect = false;
    //boolean isUpdateLevel = false;
    String InsPopupTitle, InsPopupText;

    public static NextLevelChallengeModel nextLevelChallengeModel;
    // to get sync notification data
    Bundle extra;
    boolean flag_showsyncnotification = true;

    NextLevelChallengeListing.OnGetNextLevelChallengeListingListener onGetNextLevelChallengeListingListener;
    UserLastChallengeLevelSyncApi.OnUserLastChallengeLevelSyncApi onUserLastChallengeLevelSyncApi;

//    private FirebaseAnalytics mFirebaseAnalytics;

    /**
     * Note
     * usertype 1 = live user
     * usertype 2 = demo user
     * connect_flag = connected_with_validic means get data from validic
     * connect_flag = connected_with_device means get data from google fit
     * connect_flag = connected_with_tracker means get data from band
     */

    // create the instance of screen
    public static HomeFragment newInstance(Bundle extra) {
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
    }

    // get instance of screen
    public static HomeFragment getInstance() {
        return mContext;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            isVisible = true;
        else
            isVisible = false;
    }

    // remove resources
    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            mClient.stopAutoManage(getActivity());
            mClient.disconnect();
            getActivity().unregisterReceiver(mReceiver);
        } catch (Exception e) {

        }
    }

    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.home_fragment, container, false);
        initializeControls(view);
        initializeControlsAction();

        // Register for broadcasts on BluetoothAdapter state change
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        getActivity().registerReceiver(mReceiver, filter);

        //scheduleThread();

        // initialize listener
        this.savedInstanceState = savedInstanceState;
        mContext = this;
        onGetFitnessFromValidicListener = this;

        onGetRoutineFromValidicListener = this;
        onGetSleepDataFromValidicListener = this;
        onGetLevelInformationListener = this;
        OnGetStepDataListener = this;
        UpdateChallengesStatusInBackgroundListener = this;
        onUpdateChallengesStatusInBackgroundForTracker = this;
        fitnessTrackerOperationslistener = this;
        onUpdateLevelListener = this;
        onGetUserLastSyncData = this;
        onGetNextLevelChallengeListingListener = this;
        onUserLastChallengeLevelSyncApi = this;

        setOverLay();

        // syncing band when click on OK in notification when app is in foreground
        extra = getArguments();
        if (extra != null) {
            if (extra.containsKey(Constant.key_refresh)) {
                if (extra.getBoolean(Constant.key_refresh))
                    onRefresh();
            }
        }
        chartmodellist = new ArrayList<>();
        img_header_setting.setVisibility(View.VISIBLE); // added on 26-07-2017 available for all user
        String userType = UtilsPreferences.getString(getActivity(), Constant.user_type);
        if (userType != null && !userType.isEmpty()) {
            if (userType.equalsIgnoreCase("1")) {
                img_header_sync.setVisibility(View.VISIBLE); // added on 26-07-2017 only available for live user
                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                    getHeartRateData();
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    ProtocolUtils.getInstance().setCanConnect(true);
                    onpage = true; // added on 02-10-2017
                    connectTracker();
                } else {
                    // check permission for body sensor
                    if (UtilsPermission.checkBodySensor(getActivity())) {
                        if (ConnectionDetector.internetCheck(getActivity())) {
                            buildFitnessClient();
                        }
                        if (savedInstanceState != null) {
                            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
                            Debugger.debugE("authInProgress", authInProgress + "=>");
                        }
                    } else {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.msg_bodysensor_permission), Toast.LENGTH_SHORT).show();
                    }
                }
                //InstantWinPopup();
                //getLevelInformation();
            } else {
                setupDemoUser();
            }
        } else {
            Application global = (Application) getActivity().getApplicationContext();
            global.doLogout(getActivity());
        }

        ViewPager.OnPageChangeListener myOnPageChangeListener = new ViewPager.OnPageChangeListener() {

            //declare key
            Boolean first = true;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (first && positionOffset == 0 && positionOffsetPixels == 0) {
                    onPageSelected(position);
                    first = false;
                }
                if (positionOffset > 0 && positionOffsetPixels > 0) {
                    img_right_swipe.setVisibility(View.GONE);
                    img_left_swipe.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (adapter != null) {
                    if (adapter.getCount() == 0 || adapter.getCount() == 1) {
                        img_right_swipe.setVisibility(View.GONE);
                        img_left_swipe.setVisibility(View.GONE);
                    } else if (adapter.getCount() > 1) {
                        if (adapter.getCount() - 1 == position) {
                            img_right_swipe.setVisibility(View.GONE);
                            img_left_swipe.setVisibility(View.VISIBLE);
                        } else if (position == 0) {
                            img_left_swipe.setVisibility(View.GONE);
                            img_right_swipe.setVisibility(View.VISIBLE);
                        } else {
                            img_left_swipe.setVisibility(View.VISIBLE);
                            img_right_swipe.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1"))
                    toggleRefreshing(state == ViewPager.SCROLL_STATE_IDLE);

                if (ViewPager.SCROLL_STATE_IDLE == state) {
                    onPageSelected(viewPager.getCurrentItem());
                }
            }
        };

        viewPager.addOnPageChangeListener(myOnPageChangeListener);

        return view;
    }

    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    public void setOverLay() {
        if (UtilsPreferences.getBoolean(getActivity(), Constant.first_launch, true)) {
            //UtilsPreferences.getBoolean(getActivity(), Constant.first_launch, true)
            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            openPopupDialog();
            UtilsPreferences.setBoolean(getActivity(), Constant.first_launch, false);
            //prefs.edit().putBoolean("firstrun", false).commit();
        } else {
            //actions();
            // openPopupDialog();
        }
    }

    // display popup for take rest when level or challenge is complete
    public void TakeRestPopup(String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        alertDialog.setCancelable(false);
        // Setting Dialog Title
        alertDialog.setTitle(getString(R.string.app_name));

        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton(getActivity().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                flag_showsyncnotification = true;
                showSyncPopupProcess();
                // Write your code here to invoke YES event
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    // display popup when wining challenge
    public void InstantWinPopup() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        alertDialog.setCancelable(false);
        // Setting Dialog Title
        alertDialog.setTitle(InsPopupTitle);

        // Setting Dialog Message
        alertDialog.setMessage(InsPopupText);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton(getActivity().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                // Write your code here to invoke YES event
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    // app tour dialog
    public void openPopupDialog() {
        final Dialog alertDialog = new Dialog(getActivity(), android.R.style.Theme_Light);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.setContentView(R.layout.app_guide_overlay);
        alertDialog.setCancelable(false);
        final ImageView img_overlay;

        img_overlay = (ImageView) alertDialog.findViewById(R.id.img_overlay);

        if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
            img_overlay.setImageResource(R.drawable.screen_overlay_live_user);
        } else {
            img_overlay.setImageResource(R.drawable.screen_overlay_demo_user);
        }

        img_overlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                setPopupData();
                //actions();
            }
        });
        alertDialog.show();
    }

    // setting badge count
    public void setPopupData() {
        if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
            if (Globals.Intial_reward != null) {
                try {
                    InsPopupTitle = Globals.Intial_reward.getString(Constant.InsPopupTitle);
                    InsPopupText = Globals.Intial_reward.getString(Constant.InsPopupText);
                    Boolean flag = Globals.Intial_reward.getBoolean(Constant.InsPopupFlag);
                    if (flag) {
                        int count = Integer.parseInt(Globals.Intial_reward.getString(Constant.InsPopupCount));
                        UtilsPreferences.setInt(getActivity(), Constant.ProfileBadge, count);
                        UtilsPreferences.setInt(getActivity(), Constant.CahllangeBadge, count);
                        InstantWinPopup();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // setup data for demo user
    private void setupDemoUser() {
        swiperefresh.setEnabled(false);
        if (Globals.inPChallengesChartDataMode != null && Globals.inPChallengesChartDataMode.size() > 0) {
            setDataFromGlobal();
        } else {
            getLevelInformation(true);
        }
    }

    // sync event of band and its override method
    @Override
    public void onSysEvt(int evt_base, final int evt_type, int error, final int value) {

        Log.e("Value : ", "" + value);
        // TODO Auto-generated method stub
        if (value > 0) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    pb_sync_main.setProgress(value);
                    if (isAdded())
                        tv_syncstatus_main.setText(getActivity().getResources().getString(R.string.syncing_lbl));
                    //setTitle("Synced : " + value + "%");
                    /*if (value == 1) {
                        expand(ll_sync_progress);
                        swiperefresh.setRefreshing(false);
                    }*/
                    if (syncavailable) {
                        UtilsPreferences.setBoolean(getActivity(), Constant.isSyncing, true);
                        if (isRefresh) {
                            syncavailable = false;
                            expand(ll_sync_progress_main);
                            swiperefresh.setRefreshing(false);
                        }
                    }

                }
            }, 200);
        } else if (evt_type == ProtocolEvt.SYNC_EVT_HEALTH_SYNC_COMPLETE.toIndex()) {//Synchronization is complete
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //dialog.dismiss();
                    Log.e("evt_type Value : ", "" + evt_type);
                    Log.e("index Value : ", "" + ProtocolEvt.SYNC_EVT_HEALTH_SYNC_COMPLETE.toIndex());
                    Log.e("onpage Value : ", "" + onpage);
                    UtilsPreferences.setBoolean(getActivity(), Constant.isSyncing, false);
                    if (onpage) {
                        onpage = false;
                        if (isAdded())
                            tv_syncstatus_main.setText(getActivity().getResources().getString(R.string.connected_lbl));
                        collapse(ll_sync_progress_main);
                        // call last user sync data added on 21-07-2017
                        callLastSyncData();
                        if (!isRefresh) {
                            UtilsCommon.destroyProgressBar();
                        }
                        getChallengesInfo();
                    }
                }
            }, 200);
        }
    }

    @Override
    public void onHealthSport(final HealthSport arg0, HealthSportAndItems arg1) {
        // TODO Auto-generated method stub
        if (arg0 != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("HealthSportData =>", String.valueOf(arg0.getTotalCalory()) + String.valueOf(arg0.getTotalDistance()) + String.valueOf(arg0.getTotalStepCount()));
                    Calendar c = Calendar.getInstance();
                    Date currentDate = c.getTime();

                    SimpleDateFormat formater = new SimpleDateFormat("MM-dd");

                    String argtDate = formater.format(arg0.getDate());
                    String currDate = formater.format(currentDate);//

                    if (argtDate.equals(currDate)) {
                        Float cal, dis, stp;
                        Double kilometers = arg0.getTotalDistance() * 0.001;//0.001s
                        cal = (float) (arg0.getTotalCalory());
                        dis = Float.valueOf(String.valueOf(kilometers));
                        stp = (float) arg0.getTotalStepCount();

                        setDataWithCounter(tv_caloriesData, cal);
                        setDataWithCounter(tv_stepsData, stp);
                        //setDataWithCounter(tv_distanceData, dis);
                        setDataWithCounter(tv_distanceData, UtilsCommon.getMiles((float) arg0.getTotalDistance()));
                        //UtilsCommon.getMiles((float) arg0.getTotalDistance());
                        tv_distanceData.setSuffix(" km");
                    /*tv_caloriesData.setText(cal);
                    tv_stepsData.setText(stp);
                    tv_distanceData.setText(dis);*/

                        //set data in global
                        Globals.homeDeviceMap.put(Constant.calories_burned, cal);

                        Globals.homeDeviceMap.put(Constant.step, stp);
                        Globals.homeDeviceMap.put(Constant.distance, UtilsCommon.getMiles((float) arg0.getTotalDistance()));
                    }

                    //tv_heartRateData.setText(String.valueOf(arg0.getTotalCalory()));
                    /*sportSb.append(arg0.toString()+"\n");
                    tvSportData.setText("Motion data:\n\n" + sportSb.toString());*/

                }
            }, 200);
        }
    }

    @Override
    public void onHealthHeartRate(final HealthHeartRate arg0, HealthHeartRateAndItems arg1) {
        // TODO Auto-generated method stub
        if (arg0 != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Float hr = Float.valueOf(arg0.getSilentHeart());
                    Globals.homeDeviceMap.put(Constant.heart_rate, hr);
                    //tv_heartRateData.setText(hr);
                    setDataWithCounter(tv_heartRateData, hr);
                    tv_heartRateData.setSuffix(" bpm");
                }
            }, 200);
        }
    }

    @Override
    public void onSleepData(final healthSleep arg0, healthSleepAndItems arg1) {
        // TODO Auto-generated method stub
        if (arg0 != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //setDataWithCounter(tv_sleepData, arg0.getSleepEndedTimeH());
                    int TSHours = arg0.getTotalSleepMinutes() / 60;
                    int TSMinutes = arg0.getTotalSleepMinutes() % 60;
                    tv_sleepData.setText(TSHours + " hrs");
                    Globals.homeDeviceMap.put(Constant.sleep, TSHours);
                }
            }, 200);
        }
    }

    // sync animation
    public void RotateImage() {
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(1000);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setRepeatMode(Animation.RESTART);
        rotate.setInterpolator(new LinearInterpolator());
        img_sync.startAnimation(rotate);
    }

    // initialize all controls define in xml layout
    public void initializeControls(View view) {
        try {
            //ProtocolUtils.getInstance().setCanConnect(true);
            ProtocolUtils.getInstance().setProtocalCallBack(this);

            swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_view);
            swiperefresh.setOnRefreshListener(this);
            swiperefresh.setColorSchemeResources(
                    R.color.colorPrimary);

            ll_calories = (LinearLayout) view.findViewById(R.id.ll_calories);
            ll_steps = (LinearLayout) view.findViewById(R.id.ll_steps);
            ll_distance = (LinearLayout) view.findViewById(R.id.ll_distance);
            ll_sleep = (LinearLayout) view.findViewById(R.id.ll_sleep);
            ll_heartRate = (LinearLayout) view.findViewById(R.id.ll_heartRate);

            tv_caloriesData = (CounterView) view.findViewById(R.id.tv_caloriesData);
            tv_stepsData = (CounterView) view.findViewById(R.id.tv_stepsData);

            tv_distanceData = (CounterView) view.findViewById(R.id.tv_distanceData);
            tv_sleepData = (TextView) view.findViewById(R.id.tv_sleepData);

            tv_heartRateData = (CounterView) view.findViewById(R.id.tv_heartRateData);
            lin_main = (LinearLayout) view.findViewById(R.id.lin_main);
            img_header_sync = (ImageView) view.findViewById(R.id.img_header_sync);
            img_header_setting = (ImageView) view.findViewById(R.id.img_header_setting);

            //tv_level = (TextView) view.findViewById(R.id.tv_level);
            //tv_level_name = (TextView) view.findViewById(R.id.tv_level_name);
            //iv_home_badge = (ImageView) view.findViewById(R.id.iv_home_badge);
            //fitChart = (FitChart) view.findViewById(R.id.fitChart);

            //change
            viewPager = (ViewPager) view.findViewById(R.id.viewPager);
            indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
            indicator.setStrokeWidth(2);
            indicator.setStrokeColor(getActivity().getResources().getColor(R.color.pager_selected));

            //sync animation
            ll_sync_progress_main = (LinearLayout) view.findViewById(R.id.ll_sync_progress_main);
            pb_sync_main = (ProgressBar) view.findViewById(R.id.pb_sync_main);
            tv_syncstatus_main = (TextView) view.findViewById(R.id.tv_syncstatus_main);
            img_sync = (ImageView) view.findViewById(R.id.img_sync);
            img_err = (ImageView) view.findViewById(R.id.img_err);
            tv_goyohr_main = (TextView) view.findViewById(R.id.tv_syncstatus_main);

            img_left_swipe = (ImageView) view.findViewById(R.id.img_left_swipe);
            img_right_swipe = (ImageView) view.findViewById(R.id.img_right_swipe);

            ll_sync_progress_main.setVisibility(View.GONE);
            ll_chart = (LinearLayout) view.findViewById(R.id.ll_chart);
            ll_chart.setVisibility(View.GONE);

            ll_sync_progress = (LinearLayout) view.findViewById(R.id.ll_sync_progress);
            ll_sync_progress.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // syncing expand animation
    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewPager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        //a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        a.setDuration(200);
        v.startAnimation(a);
    }

    // syncing collapse animation
    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        //a.setDuration(SPEED_ANIMATION_TRANSITION);
        //a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        a.setDuration(200);
        v.startAnimation(a);
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_calories.setOnClickListener(this);
            ll_steps.setOnClickListener(this);
            ll_distance.setOnClickListener(this);
            ll_sleep.setOnClickListener(this);
            ll_heartRate.setOnClickListener(this);

            lin_main.setOnClickListener(this);
            img_header_sync.setOnClickListener(this);
            img_header_setting.setOnClickListener(this);

            Log.e("global data", String.valueOf(Globals.homeDeviceMap));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click listener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_calories:
                addFragment(new CaloriesFragment(), "CaloriesFragment");
                //syncanimation();
                break;

            case R.id.ll_steps:
                addFragment(StepsFragment.newInstance(null), "StepsFragment");
                break;

            case R.id.ll_distance:
                addFragment(DistanceFragment.newInstance(null), "DistanceFragment");
                break;

            case R.id.ll_sleep:
                addFragment(SleepFragment.newInstance(null), "SleepFragment");
                break;

            case R.id.ll_heartRate:
                addFragment(HeartRateFragment.newInstance(null), "HeartRateFragment");
                break;

            case R.id.lin_main:
                addFragment(GraphFragment.newInstance(null), "GraphFragment");
                break;
            case R.id.img_header_sync:
                onRefresh();
                break;
            case R.id.img_header_setting:
                addFragment(SettingsFragment.newInstance(null), Constant.settings);
                break;
            default:
                break;
        }
    }

    // check whether LevelInformationModel is null or not
    public void getLevelInformation(boolean flag) {
        //TODO

        LevelInformationModel obj = UtilsPreferences.getLevelInformationSharedPrefrences(getActivity(), Constant.Key_level_info);
        if (obj != null) {
            onSucceedToGetLevelInformationProcess(obj);
        } else {
            callGetLevelInformation(flag);
        }
    }

    // api call for level information
    public void callGetLevelInformation(boolean flag) {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
            data.put(Constant.validic_id, UtilsPreferences.getString(getActivity(), Constant.validic_id, ""));

            if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                GetLevelInformationCall task = new GetLevelInformationCall(getActivity(), onGetLevelInformationListener, data, flag);
                task.execute();
            } else {
                swiperefresh.setEnabled(true);
                // Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of GetLevelInformationCall
    @Override
    public void onSucceedToGetLevelInformation(LevelInformationModel obj) {
        //if (Globals.homeDeviceMap.size() == 0)
        UtilsPreferences.saveInLevelInformationSharedPrefrences(getActivity(), Constant.Key_level_info, obj);
        onSucceedToGetLevelInformationProcess(obj);

    }

    // process after getting success listener of GetLevelInformationCall
    public void onSucceedToGetLevelInformationProcess(LevelInformationModel obj) {
        callUserLastChallengeLevelSyncApi();
        String user_type = UtilsPreferences.getString(getActivity(), Constant.user_type);
        if (user_type != null && user_type.length() > 0) {
            if (user_type.equalsIgnoreCase("1"))
                swiperefresh.setEnabled(true);
        }
        levelData = obj;

        if (obj.getLevel_number() != null) {
            levelStartDate = obj.getStart_date();
            //levelendDate = obj.getComplete_date();

            //levelStartDate = obj.getLevel_start_date();
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
            try {
                Date startDate = formater.parse(levelStartDate);
                Calendar cal = Calendar.getInstance();
                cal.setTime(startDate);
                cal.add(Calendar.MINUTE, Integer.parseInt(obj.getDuration()));
                levelendDate = formater.format(cal.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //tv_level.setText(obj.getLevel_subtitle());
            //tv_level_name.setText(obj.getLevel_name());
            String url = getActivity().getResources().getString(R.string.server_url)
                    + getActivity().getResources().getString(R.string.badges_image)
                    + obj.getLevel_image();
            /*Picasso.with(getActivity())
                    .load(url)
                    .into(iv_home_badge);*/
        }
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            setChartForValidic(obj);

        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            int levelType = 0;
            int secondLevelType;
            if (obj.getSecond_level_type() != null) {
                secondLevelType = Integer.parseInt(obj.getSecond_level_type());
                if (secondLevelType > 0) {
                    totalLevel = 2;
                }
            }

            if (obj.getLevel_type() != null) {
                levelType = Integer.parseInt(obj.getLevel_type());
                totalLevelCount++;
            }

            if (obj.getSecond_level_type() != null && obj.getSecond_level_type() != "0") {
                secondLevelType = Integer.parseInt(obj.getSecond_level_type());
                //totalLevelCount++;
                //trackerleveldata(levelStartDate, levelendDate);
                //connectTracker();
            }
            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                // set condition here if it is future date then check here
                trackerleveldata(levelStartDate, levelendDate, levelType);
            } else {
                setChartForTracker();
            }
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
            int levelType;
            int secondLevelType;
            if (obj.getSecond_level_type() != null) {
                secondLevelType = Integer.parseInt(obj.getSecond_level_type());
                if (secondLevelType > 0) {
                    totalLevel = 2;
                }
            }

            if (obj.getLevel_type() != null) {
                levelType = Integer.parseInt(obj.getLevel_type());
                switch (levelType) {
                    case 1://steps
                        totalLevelCount++;
                        new StepsCall(true).execute();
                        break;
                    case 2://calories
                        totalLevelCount++;
                        new CaloriesCall(true).execute();
                        break;
                    case 3://distance
                        totalLevelCount++;
                        new DistanceCall(true).execute();
                        break;
                    case 4://heart_rate
                        totalLevelCount++;
                        new HeartRateCall(true).execute();
                        break;
                    case 7:// steps and calories
                        totalLevelCount++;
                        new StepsCall(true).execute();
                }
            }

            if (obj.getSecond_level_type() != null) {
                secondLevelType = Integer.parseInt(obj.getSecond_level_type());
                switch (secondLevelType) {
                    case 1://steps
                        new StepsCall(true).execute();
                        break;
                    case 2://calories
                        new CaloriesCall(true).execute();
                        break;
                    case 3://distance
                        new DistanceCall(true).execute();
                        break;
                    case 4://heart_rate
                        new HeartRateCall(true).execute();
                        break;
                }
            }
        }

        // call user_last_challenge_level_sync.php

        //callUserLastChallengeLevelSyncApi();

    }

    // send the data for latest level and challenges
    private void callUserLastChallengeLevelSyncApi() {
        String str_Challenge_percentage = "0", str_challenge_id = "0", str_Challenge_minutes = "0";
        String str_level_percentage = "0", str_level_id = "0", str_level_minutes = "0";
        final String str_steps = "0", str_calories = "0", str_heartRate = "0", str_distance = "0", str_sleep = "0";

        if (Globals.inPChallengesChartDataMode != null && Globals.inPChallengesChartDataMode.size() > 0) {

            if (Globals.inPChallengesChartDataMode.size() > 1 && Globals.inPChallengesChartDataMode.get(1).getChallengesModels() != null) {

                str_Challenge_minutes = UtilsCommon.getTotalLevelChallengeMins(Globals.inPChallengesChartDataMode.get(1).getChallengesModels().getAccept_date());
                str_Challenge_percentage = String.valueOf(UtilsCommon.getPercentageValue(1)); // 1 for challenges
                str_challenge_id = Globals.inPChallengesChartDataMode.get(1).getChallengesModels().getChallenge_id();

            }

            if (Globals.inPChallengesChartDataMode.size() > 0 && Globals.inPChallengesChartDataMode.get(0).getLevelInformationModel() != null) {

                str_level_percentage = String.valueOf(UtilsCommon.getPercentageValue(0)); // 0 for level
                str_level_id = Globals.inPChallengesChartDataMode.get(0).getLevelInformationModel().getBadges_level_id();
                str_level_minutes = UtilsCommon.getTotalLevelChallengeMins(Globals.inPChallengesChartDataMode.get(0).getLevelInformationModel().getStart_date());

            }
        }

        final JSONObject data = new JSONObject();
        if (BleSharedPreferences.getInstance().getIsBind()) {

            String chlngStartDate = "";
            String chlngEndDate = "";

            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
            if (Globals.inPChallengesChartDataMode != null && Globals.inPChallengesChartDataMode.size() > 1 && Globals.inPChallengesChartDataMode.get(1).getChallengesModels() != null) {
                String start_time = Globals.inPChallengesChartDataMode.get(1).getChallengesModels().getAccept_date();

                Date startDate = null;
                try {
                    startDate = formater.parse(start_time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                chlngStartDate = formater.format(startDate);
                int duration = Integer.parseInt(Globals.inPChallengesChartDataMode.get(1).getChallengesModels().getDuration());
                Calendar endCalendar = Calendar.getInstance();
                Date endDate = startDate;
                endCalendar.setTime(endDate);
                endCalendar.add(Calendar.MINUTE, duration);
                chlngEndDate = formater.format(endCalendar.getTime());

                final String finalStr_Challenge_percentage = str_Challenge_percentage;
                final String finalStr_challenge_id = str_challenge_id;
                final String finalStr_Challenge_minutes = str_Challenge_minutes;
                final String finalStr_level_percentage = str_level_percentage;
                final String finalStr_level_id = str_level_id;
                final String finalStr_level_minutes = str_level_minutes;

                new FitnessTrackerOperations(getActivity(), new FitnessTrackerOperationslistener() {
                    @Override
                    public void onSucceedtoFitnessTrackerOperation(int steps, int cals, int dis, int hearts, int sleep) {

                        try {
                            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
                            data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
                            data.put(Constant.challenge_per, finalStr_Challenge_percentage);
                            data.put(Constant.challenge_id, finalStr_challenge_id);
                            data.put(Constant.challenge_min, finalStr_Challenge_minutes);
                            data.put(Constant.calories, String.valueOf(cals));
                            data.put(Constant.steps, String.valueOf(steps));
                            data.put(Constant.distance, String.valueOf((double) dis / 1000));
                            data.put(Constant.heart_rate, String.valueOf(hearts));
                            data.put(Constant.sleep, String.valueOf(sleep));
                            data.put(Constant.level_per, finalStr_level_percentage);
                            data.put(Constant.level_id, finalStr_level_id);
                            data.put(Constant.level_min, finalStr_level_minutes);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }).readDatafromTracker(chlngStartDate,
                        chlngEndDate, Globals.inPChallengesChartDataMode.get(1).getChallengetype());
            } else {
                try {
                    data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
                    data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
                    data.put(Constant.level_per, str_level_percentage);
                    data.put(Constant.level_id, str_level_id);
                    data.put(Constant.level_min, str_level_minutes);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
                data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
                data.put(Constant.level_per, str_level_percentage);
                data.put(Constant.level_id, str_level_id);
                data.put(Constant.level_min, str_level_minutes);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        UserLastChallengeLevelSyncApi userLastChallengeLevelSyncApi = new UserLastChallengeLevelSyncApi(getActivity(), onUserLastChallengeLevelSyncApi, data, true);
        userLastChallengeLevelSyncApi.execute();

    }

    // success listener of UserLastChallengeLevelSyncApi
    @Override
    public void onSucceedToUserLastChallengeLevelSyncApi(String msg) {
        Log.d(TAG, " onSucceedToUserLastChallengeLevelSyncApi : " + msg);
        UtilsCommon.destroyProgressBar();
    }

    // failed listener of UserLastChallengeLevelSyncApi
    @Override
    public void onFaildToUserLastChallengeLevelSyncApi(String error_msg) {
        Log.d(TAG, " onFaildToUserLastChallengeLevelSyncApi : " + error_msg);
        UtilsCommon.destroyProgressBar();
    }

    // failed listener of GetLevelInformationCall
    @Override
    public void onFaildToGetLevelInformation(String error_msg) {
        UtilsCommon.destroyProgressBar();
        if (error_msg != null && !error_msg.isEmpty())
            Toast.makeText(getActivity(), error_msg, Toast.LENGTH_SHORT).show();
        swiperefresh.setEnabled(true);
    }

    // open required screen

    /**
     * @param fragment add fragment
     */
    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
        fragmentTransaction.replace(R.id.frame_main, fragment, null);//change add to replace
        fragmentTransaction.addToBackStack(null);//change tag to null
        fragmentTransaction.commit();
    }

    // api call for GetFitnessDataFromValidic
    public void getHeartRateData() {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            GetFitnessDataFromValidicCall task = new GetFitnessDataFromValidicCall(getActivity(), onGetFitnessFromValidicListener, true, UtilsCommon.getCurrentdate(), UtilsCommon.getNextDaydate(), Constant.dashboard);
            task.execute();
        } else {
            //Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    // api call for GetRoutineDataFromValidic
    public void getRoutineData() {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            GetRoutineDataFromValidicCall task = new GetRoutineDataFromValidicCall(getActivity(), onGetRoutineFromValidicListener, true, /*"2016-09-03'T'00:00:00+00:00"*/ UtilsCommon.getCurrentdate(), /*"2016-09-04'T'00:00:00+00:00"*/ UtilsCommon.getNextDaydate(), Constant.dashboard);
            task.execute();
        } else {
            //Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    // api call for GetSleepDataFromValidic
    public void getSleepData() {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            GetSleepDataFromValidicCall task = new GetSleepDataFromValidicCall(getActivity(), onGetSleepDataFromValidicListener, true, UtilsCommon.getCurrentdate(), UtilsCommon.getNextDaydate(), Constant.dashboard);
            task.execute();
        } else {
            //Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    // connect Google Api Client
    private void buildFitnessClient() {
        if (Globals.homeDeviceMap.size() <= 0)
            UtilsCommon.showProgressDialog(getActivity());
        mClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Fitness.HISTORY_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
                .addScope(new Scope(Scopes.FITNESS_BODY_READ))
                .addConnectionCallbacks(
                        new ConnectionCallbacks() {
                            @Override
                            public void onConnected(Bundle bundle) {
                                Debugger.debugE(TAG, "Connected!!!");
                                //UtilsCommon.destroyProgressBar();
                                if (Globals.homeDeviceMap.size() > 0) {
                                    setDataFromGlobal();
                                } else {

                                    UpdateChallengesStatusInBackground updateChallengesStatusInBackground = new UpdateChallengesStatusInBackground(getActivity(), mClient, UpdateChallengesStatusInBackgroundListener);
                                    updateChallengesStatusInBackground.getData();

                                    UpdateDeviceDataInBackground task = new UpdateDeviceDataInBackground(getActivity(), HomeFragment.mClient);
                                    task.getData();

                                    new CaloriesCall(false).execute();
                                }
                            }

                            @Override
                            public void onConnectionSuspended(int i) {
                                UtilsCommon.destroyProgressBar();
                                if (i == ConnectionCallbacks.CAUSE_NETWORK_LOST) {
                                    Debugger.debugE(TAG, "Connection lost Cause: Network Lost");
                                } else if (i == ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
                                    Debugger.debugE(TAG, "Connection lost Cause: Service Disconnected");
                                }
                            }
                        }
                )
                .enableAutoManage(getActivity(), 0, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        UtilsCommon.destroyProgressBar();
                        Debugger.debugE(TAG, "Google fit: " + result.getErrorMessage() + "=>");
                        Debugger.debugE(TAG, "Google fit connection failed Cause: " + result.toString());
                    }
                })
                .build();
    }

    // success listener of UpdateChallengesStatusInBackground
    @Override
    public void onSucceedUpdateChallengesStatusInBackground(ArrayList<ChartDataModel> chartmodellist) {
        if (chartmodellist != null && chartmodellist.size() > 0) {
            isChallengesAvailable = true;
            this.chartmodellist.addAll(chartmodellist);
            setupAdapter();
        } else {
            isChallengesAvailable = false;
        }
    }

    public void syncanimation() {
        // Set the progress status zero on each button click
        progressStatus = 0;
        /*
                    A Thread is a concurrent unit of execution. It has its own call stack for
                    methods being invoked, their arguments and local variables. Each application
                    has at least one thread running when it is started, the main thread,
                    in the main ThreadGroup. The runtime keeps its own threads
                    in the system thread group.
                */

        // Start the lengthy operation in a background thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressStatus < 100) {

                    if (progressStatus == 0) {
                        expand(ll_sync_progress_main);

                    }
                    // Update the progress status
                    progressStatus += 1;

                    // Try to sleep the thread for 20 milliseconds
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // Update the progress bar
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            pb_sync_main.setProgress(progressStatus);
                            // Show the progress on TextView
                            tv_syncstatus_main.setText(progressStatus + "");

                            if (progressStatus == 100) {
                                collapse(ll_sync_progress_main);
                            }
                        }
                    });
                }
            }
        }).start(); // Start the operation
    }

    // get fitness data from shared prefrences if its available otherwise it will get from band
    public void setDataFromGlobal() {
        HashMap<String, Object> DeviceMap = Globals.homeDeviceMap;
        //setDataWithCounter(tv_heartRateData, 100);//tv_heartRateData.setPrefix("bpm");
        if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
            if (mClient != null) {
                tv_caloriesData.setText(String.valueOf(DeviceMap.get(Constant.calories_burned)));
                tv_stepsData.setText(String.valueOf(DeviceMap.get(Constant.step)));
                tv_distanceData.setText(String.valueOf(DeviceMap.get(Constant.distance)));
                tv_heartRateData.setText(String.valueOf(DeviceMap.get(Constant.heart_rate)));

                adapter = new HomeSlidingChartAdapter(getActivity(), Globals.inPChallengesChartDataMode, nextLevelChallengeModel);
                viewPager.setAdapter(adapter);
                UtilsCommon.destroyProgressBar();

                setLevelCurrentScreen(getActivity());
                indicator.setViewPager(viewPager);
            } else {

                if (isBleConnect) {

                    setDataWithCounter(tv_caloriesData, (float) DeviceMap.get(Constant.calories_burned));
                    setDataWithCounter(tv_stepsData, (float) DeviceMap.get(Constant.step));
                    setDataWithCounter(tv_distanceData, (float) DeviceMap.get(Constant.distance));//(Float) DeviceMap.get(Constant.distance)
                    tv_distanceData.setSuffix(" km");
                    if (DeviceMap.get(Constant.heart_rate) != null) {
                        setDataWithCounter(tv_heartRateData, (float) DeviceMap.get(Constant.heart_rate));
                        tv_heartRateData.setSuffix(" bpm");
                    } else {
                        setDataWithCounter(tv_heartRateData, 0);
                        tv_heartRateData.setSuffix(" bpm");
                    }
                    if (DeviceMap.get(Constant.sleep) != null) {
                        tv_sleepData.setText(DeviceMap.get(Constant.sleep).toString() + " hrs");
                    }
                } else {
                    setDatafromBand();
                }

                levelData = Globals.inPChallengesChartDataMode.get(0).getLevelInformationModel();
                chartmodellist.addAll(Globals.inPChallengesChartDataMode);
            }
        }

        //change
        if (Globals.inPChallengesChartDataMode != null && Globals.inPChallengesChartDataMode.size() > 0) {

            adapter = new HomeSlidingChartAdapter(getActivity(), Globals.inPChallengesChartDataMode, nextLevelChallengeModel);
            viewPager.setAdapter(adapter);
            UtilsCommon.destroyProgressBar();

            setLevelCurrentScreen(getActivity());
            indicator.setViewPager(viewPager);

            if (Globals.inPChallengesChartDataMode.size() == 1)
                isChallengesAvailable = false;
            else
                isChallengesAvailable = true;
        }

       /* if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            chartmodellist.add(Globals.inPChallengesChartDataMode.get(0));
        }*/
    }

    // success listener of GetFitnessDataFromValidic
    @Override
    public void onSucceedToGetFitnessFromValidic(ValidicSummaryModel summary, ValidicFitnessModel data, String chartFlag) {
        if (summary.getResults() > 0) {
            for (int i = 0; i < data.getFitness().size(); i++) {
                //if (data.getFitness().get(i).getTimestamp().contains(UtilsCommon.getCurrentDateOnly())) {
                tv_heartRateData.setText(String.valueOf(data.getFitness().get(i).getAverage_heart_rate()) + " bpm");
                break;
                //}
            }
        }
        getRoutineData();
    }

    // failed listener of GetFitnessDataFromValidic
    @Override
    public void onFaildToGetFitnessFromValidic(String error_msg) {

    }

    // success listener of GetRoutineDataFromValidic
    @Override
    public void onSucceedToGetRoutineFromValidic(ValidicSummaryModel summary, ValidicRoutineModel data, String chartFlag) {
        if (summary.getResults() > 0 && chartFlag.equalsIgnoreCase(Constant.dashboard)) {
            for (int i = 0; i < data.getRoutine().size(); i++) {
                //if (data.getRoutine().get(i).getTimestamp().contains(UtilsCommon.getCurrentDateOnly())) {
                tv_caloriesData.setText(String.valueOf(data.getRoutine().get(i).getCalories_burned()));
                tv_stepsData.setText(String.valueOf(data.getRoutine().get(i).getSteps()));
                tv_distanceData.setText(String.valueOf(UtilsCommon.getMiles((float) data.getRoutine().get(i).getDistance()) + " miles"));
                break;
                //}
            }
        }
        getSleepData();
    }

    // failed listener of GetRoutineDataFromValidic
    @Override
    public void onFaildToGetRoutineFromValidic(String error_msg) {

    }

    // success listener of GetSleepDataFromValidic
    @Override
    public void onSucceedToGetSleepDataFromValidic(ValidicSummaryModel summary, ValidicSleepModel data, String chartFlag) {
        if (summary.getResults() > 0) {
            for (int i = 0; i < data.getSleep().size(); i++) {
                Debugger.debugE("Sleep..", data.getSleep().get(i).getMinutes_asleep() / 60 + " " + data.getSleep().get(i).getAwake_count());
                //if (data.getSleep().get(i).getTimestamp().contains(UtilsCommon.getCurrentDateOnly())) {
                int sleep = data.getSleep().get(i).getMinutes_asleep() / 60;
                tv_sleepData.setText(String.valueOf(sleep) + " hour");
                break;
                //}
            }
        }
    }

    // failed listener of GetSleepDataFromValidic
    @Override
    public void onFaildToGetSleepDataFromValidic(String error_msg) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    // set data with counting animation

    /**
     * @param counterView get counterview
     * @param val         total calories
     */
    public void setDataWithCounter(CounterView counterView, float val) {
        counterView.setFormatter(new Formatter() {
            @Override
            public String format(String prefix, String suffix, float value) {
                return prefix
                        + NumberFormat.getNumberInstance(Locale.US).format(value)
                        + suffix;
            }
        });
        counterView.setAutoStart(false);
        counterView.setStartValue(val);
        counterView.setEndValue(val);
        counterView.setIncrement(10); // the amount the number increments at each time interval
        counterView.setTimeInterval(1); // the time interval (ms) at which the text changes
        counterView.start(); // you can start anytime if autostart is set to false
    }

    // set data with counting animation distance

    /**
     * @param counterView get counterview
     * @param val         total distance
     * @param suff        suffix value
     */
    public void setDataWithCounterDis(CounterView counterView, float val, final String suff) {
        counterView.setFormatter(new Formatter() {
            @Override
            public String format(String prefix, String suffix, float value) {
                return prefix
                        + NumberFormat.getNumberInstance(Locale.US).format(value)
                        + suff;
            }
        });
        counterView.setAutoStart(false);
        counterView.setStartValue(val);
        counterView.setEndValue(val);
        counterView.setIncrement(10); // the amount the number increments at each time interval
        counterView.setTimeInterval(1); // the time interval (ms) at which the text changes
        counterView.start(); // you can start anytime if autostart is set to false
    }

    // swipe onrefresh call and refresh all the data from band and update the layout
    @Override
    public void onRefresh() {
        //change
        if (UtilsCommon.checkBlutoothConnectivity()) {
            swiperefresh.setRefreshing(false);
            scheduleThread();
            if (Constant.refreshLevelBluetoothOff) {
                Constant.refreshLevelBluetoothOff = false;
                if (chartmodellist != null && chartmodellist.size() > 0) {
                    chartmodellist.clear();
                    if (adapter != null)
                        adapter.notifyDataSetChanged();
                    getChallengesInfo();
                }
            }
        } else {
            Constant.refreshLevelBluetoothOff = false;
            isRefresh = true;
            syncavailable = true;
            onpage = true;

            swiperefresh.setRefreshing(true);
            totalLevelCount = 0;
            totalLevel = 1;
            totalCal = 0;
            totalDistance = 0;
            totalStep = 0;
            totalLevelCount = 0;
            totalLevel = 1;

            levelCaloriesData = "0";
            levelStepsData = "0";
            levelDistanceData = "0";
            levelHeartRateData = "0";

            if (Globals.homeDeviceMap.size() > 0)
                Globals.homeDeviceMap.clear();

            //getLevelInformation();
            if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                getHeartRateData();
            } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                //chartmodellist.clear();
                swiperefresh.setEnabled(false);
                connectTracker();
            } else {
                if (chartmodellist != null && chartmodellist.size() > 0) {
                    this.chartmodellist.subList(1, this.chartmodellist.size()).clear();
                }
                Globals.inPChallengesChartDataMode.clear();

                UpdateChallengesStatusInBackground updateChallengesStatusInBackground = new UpdateChallengesStatusInBackground(getActivity(), mClient, UpdateChallengesStatusInBackgroundListener);
                updateChallengesStatusInBackground.getData();

                new CaloriesCall(false).execute();

            }
        }
        /*} else {
            Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            swiperefresh.setRefreshing(false);
        }*/
    }

    // here check condition if band is connected or not
    // if not connected then redirect to scan screen else it will load the data from band
    public void connectTracker() {
        try {
            //if (ConnectionDetector.isConnectingToInternet(getActivity())) {

            if (BleSharedPreferences.getInstance().getIsBind()) {
                if (!ConnectionDetector.internetCheck(getActivity())) {
                    return;
                } else if (!ConnectionDetector.isGpsEnable(getActivity())) {
                    return;
                }
                if (Globals.homeDeviceMap.size() > 0) {
                    setDataFromGlobal();
                    onpage = false;
                    showSyncPopupProcess();
                } else {
                    if (!isRefresh) {
                        UtilsCommon.showProgressDialog(getActivity());
                    }
                    if (ProtocolUtils.getInstance().isAvailable() == ProtocolUtils.SUCCESS) {
                        isBleConnect = true;
                        ProtocolUtils.getInstance().StartSyncHealthData();
                        RotateImage();
                    } else {
                        //setup stored data while band is not connect
                        UtilsCommon.destroyProgressBar();
                        setDatafromBand();
                        //getLevelInformation(true);
                        getChallengesInfo();
                    }
                }
            } else {
                /*if (Globals.homeDeviceMap.size() > 0) {
                    setDataFromGlobal();
                    onpage = false;
                    showSyncPopupProcess();
                } else {*/
                Intent intent = new Intent(getActivity(), ScanDeviceActivity.class);
                intent.putExtra("isFrom", "HomeFragment");
                startActivity(intent);
                getChallengesInfo();
                //setDatafromBand();
                onpage = false;
                showSyncPopupProcess();
                //}
            }
            /*} else {
                Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // api call for next level challenge listeing
    public void getChallengesInfo() {
        callNextLevelChallengeListingApi();
    }

    // success listener of UpdateChallengesStatusInBackgroundForTracker
    @Override
    public void onSucceedtoUpdateChallengesStatusInBackgroundForTracker(ArrayList<ChartDataModel> chartmodellist) {
        swiperefresh.setRefreshing(false);
        if (this.chartmodellist.size() > 1)
            this.chartmodellist.subList(1, this.chartmodellist.size()).clear();
        if (chartmodellist != null && chartmodellist.size() > 0)
            this.chartmodellist.addAll(chartmodellist);

        getLevelInformation(false);
    }

    // success listener onSucceedtoUpdateWiningstatus
    @Override
    public void onSucceedtoUpdateWiningstatus(ChallengesModel challengesModel) {

        //TODO
        Intent i = new Intent(getActivity(), WonActivity.class);
        i.putExtra("challengesModel", (Serializable) challengesModel);
        i.putExtra("isFrom", "Forground");
        startActivity(i);
        UtilsCommon.destroyProgressBar();
    }

    // get data from band and populate on screen
    public void setDatafromBand() {
        int year, month, day;
        Calendar mCalendar = Calendar.getInstance();
        year = mCalendar.get(Calendar.YEAR);
        month = mCalendar.get(Calendar.MONTH);
        day = mCalendar.get(Calendar.DAY_OF_MONTH);

        if (BleSharedPreferences.getInstance().getIsBind()) {
            HealthSport Healthdata = ProtocolUtils.getInstance().getHealthSport(new Date(year, month, day));
            HealthHeartRate Heartdata = ProtocolUtils.getInstance().getHealthRate(new Date(year, month, day));

            if (Healthdata != null && Heartdata != null) {

                Globals.homeDeviceMap.put(Constant.heart_rate, (float) Heartdata.getSilentHeart());
                Globals.homeDeviceMap.put(Constant.calories_burned, (float) Healthdata.getTotalCalory());
                Globals.homeDeviceMap.put(Constant.step, (float) Healthdata.getTotalStepCount());
                Globals.homeDeviceMap.put(Constant.distance, (UtilsCommon.getMiles((float) Healthdata.getTotalDistance())));

                setDataWithCounter(tv_caloriesData, (float) Globals.homeDeviceMap.get(Constant.calories_burned));
                setDataWithCounter(tv_stepsData, (float) Globals.homeDeviceMap.get(Constant.step));
                setDataWithCounter(tv_heartRateData, (float) Globals.homeDeviceMap.get(Constant.heart_rate));
                tv_heartRateData.setSuffix(" bpm");
                setDataWithCounter(tv_distanceData, (float) Globals.homeDeviceMap.get(Constant.distance));//(Float) DeviceMap.get(Constant.distance)
                tv_distanceData.setSuffix(" km");
            }
        } else {
            Globals.homeDeviceMap.put(Constant.heart_rate, 0);
            Globals.homeDeviceMap.put(Constant.calories_burned, 0);
            Globals.homeDeviceMap.put(Constant.step, 0);
            Globals.homeDeviceMap.put(Constant.distance, 0);
        }
    }

    // success listener of User_last_sync
    @Override
    public void onSucceedToUserLastSyncData(String msg) {
        Log.d(TAG, " onSucceedToUserLastSyncData : " + msg);
    }

    // failed listener of User_last_sync
    @Override
    public void onFaildToUserLastSyncData(String error_msg) {
        Log.d(TAG, " onFaildToUserLastSyncData : " + error_msg);
    }

    // calorie call api for google fit
    private class CaloriesCall extends AsyncTask<Void, Void, Void> {
        boolean levelFlag;

        public CaloriesCall(boolean levelFlag) {
            this.levelFlag = levelFlag;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*if (!levelFlag)
                UtilsCommon.showProgressDialog(getActivity());*/
        }

        protected Void doInBackground(Void... params) {
            try {
                if (levelFlag) {
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED, levelFlag, "CaloriesCall");
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    levelCaloriesData = readData(dataReadResult, levelFlag, 2);
                } else {
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED, levelFlag, "CaloriesCall");
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    caloriesData = readData(dataReadResult, levelFlag, 2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (!levelFlag) {
                    new StepsCall(false).execute();
                    int cal = Math.round(Float.parseFloat(caloriesData.equalsIgnoreCase("") ? "0" : caloriesData));
                    setDataWithCounter(tv_caloriesData, cal);

                    Globals.homeDeviceMap.put(Constant.calories_burned, cal);
                } else {
                    levelCaloriesData = String.valueOf(totalCal);
                    if (totalLevel == totalLevelCount) {
                        setChartForDevice();
                    }
                    totalLevelCount++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // steps call api for google fit
    private class StepsCall extends AsyncTask<Void, Void, Void> {
        boolean levelFlag;

        public StepsCall(boolean levelFlag) {
            this.levelFlag = levelFlag;
        }

        protected Void doInBackground(Void... params) {
            try {
                if (levelFlag) {
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA, levelFlag, "StepsCall");
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    levelStepsData = readData(dataReadResult, levelFlag, 1);
                } else {
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA, levelFlag, "StepsCall");
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    stepsData = readData(dataReadResult, levelFlag, 1);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (!levelFlag) {
                    new DistanceCall(false).execute();
                    //tv_stepsData.setText(stepsData.equalsIgnoreCase("") ? "0" : stepsData);
                    int stp = stepsData.equalsIgnoreCase("") ? 0 : Integer.parseInt(stepsData);
                    setDataWithCounter(tv_stepsData, stp);
                    Globals.homeDeviceMap.put(Constant.step, stp);
                } else {
                    levelStepsData = String.valueOf(totalStep);
                    if (levelData.getLevel_type().equalsIgnoreCase("7")) {
                        new CaloriesCall(true).execute();
                    } else {
                        Debugger.debugE("After...", levelStepsData);
                        if (totalLevel == totalLevelCount) {
                            setChartForDevice();
                        }
                        totalLevelCount++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // distance call api for google fit
    private class DistanceCall extends AsyncTask<Void, Void, Void> {
        boolean levelFlag;

        public DistanceCall(boolean levelFlag) {
            this.levelFlag = levelFlag;
        }

        protected Void doInBackground(Void... params) {
            try {
                if (levelFlag) {
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA, levelFlag, "DistanceCall");
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    levelDistanceData = readData(dataReadResult, levelFlag, 3);
                } else {
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA, levelFlag, "DistanceCall");
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    distanceData = readData(dataReadResult, levelFlag, 3);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (!levelFlag) {
                    new HeartRateCall(false).execute();
                    //tv_distanceData.setText(distanceData.equalsIgnoreCase("") ? "0.0 miles" : String.valueOf(UtilsCommon.getMiles(Float.parseFloat(distanceData))) + " miles");
                    float dis = distanceData.equalsIgnoreCase("") ? Float.parseFloat("0.0") : UtilsCommon.getMiles(Float.parseFloat(distanceData));
                    setDataWithCounterDis(tv_distanceData, dis, " km");
                    Globals.homeDeviceMap.put(Constant.distance, dis + " km");
                } else {
                    levelDistanceData = (String.valueOf(UtilsCommon.getMiles((float) totalDistance)));
                    if (totalLevel == totalLevelCount) {
                        setChartForDevice();
                    }
                    totalLevelCount++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // heart rate call api for google fit
    private class HeartRateCall extends AsyncTask<Void, Void, Void> {
        boolean levelFlag;

        public HeartRateCall(boolean levelFlag) {
            this.levelFlag = levelFlag;
        }

        protected Void doInBackground(Void... params) {
            try {
                if (levelFlag) {
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY, levelFlag, "HeartRateCall");
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    levelHeartRateData = readData(dataReadResult, levelFlag, 4);
                } else {
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY, levelFlag, "HeartRateCall");
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    heartRateData = readData(dataReadResult, levelFlag, 4);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (!levelFlag) {
                    UtilsCommon.destroyProgressBar();
                    swiperefresh.setRefreshing(false);
                    String hr = heartRateData.equalsIgnoreCase("") ? "0 bpm" : heartRateData + " bpm";
                    tv_heartRateData.setText(hr);
                    Globals.homeDeviceMap.put(Constant.heart_rate, hr);
                } else {
                    levelHeartRateData = (levelHeartRateData.equalsIgnoreCase("") ? "0" : levelHeartRateData);
                    if (totalLevel == totalLevelCount) {
                        setChartForDevice();
                    }
                    totalLevelCount++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // request data from google fit
    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2, boolean levelFlag, String forCall) {
        Debugger.debugE("request", forCall);
        DataReadRequest readRequest = null;
        if (levelFlag) {
            try {
                SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss");

                Date startDate = null;
                Date endDate = null;
                try {
                    startDate = formater.parse(levelStartDate);
                    endDate = formater.parse(levelendDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long startMillisecond = startDate.getTime();
                long endMillisecond = endDate.getTime();

                Debugger.debugE("level-Start time", startDate + "=>");
                Debugger.debugE("Level-End time", endDate + "=>");

                readRequest = new DataReadRequest.Builder()
                        .aggregate(datatype1, datatype2)
                        .bucketByTime(1, TimeUnit.DAYS)
                        .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                Calendar startCalendar = Calendar.getInstance();
                long startTime = startCalendar.getTimeInMillis();

                SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
                String startTimeString = formater.format(startTime) + " 12:01 AM";
                Date startDate = null;
                try {
                    startDate = formater.parse(startTimeString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long startMillisecond = startDate.getTime();

                Calendar endCalendar = Calendar.getInstance();
                Date endDate = new Date();
                endCalendar.setTime(endDate);
                long endMillisecond = endCalendar.getTimeInMillis();

                Debugger.debugE("Start time", startDate + "=>");
                Debugger.debugE("End time", endDate + "=>");

                readRequest = new DataReadRequest.Builder()
                        .aggregate(datatype1, datatype2)
                        .bucketByTime(1, TimeUnit.DAYS)
                        .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return readRequest;
    }

    // read specific data by challenge type
    public String readData(DataReadResult dataReadResult, boolean levelFlag, int type) {
        String value = "";
        try {
            if (dataReadResult.getBuckets().size() > 0) {
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        if (levelFlag) {
                            value = getDataSet(dataSet);
                            switch (type) {
                                case 1://step
                                    totalStep += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                                    break;
                                case 2://calories
                                    totalCal += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                                    break;
                                case 3://distance
                                    totalDistance += Float.parseFloat(value.equalsIgnoreCase("") ? "0.0" : value);
                                    break;
                            }
                        } else {
                            value = getDataSet(dataSet);
                        }
                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    if (levelFlag) {
                        value = getDataSet(dataSet);
                        switch (type) {
                            case 1://step
                                totalStep += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                                break;
                            case 2://calories
                                totalCal += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                                break;
                            case 3://distance
                                totalDistance += Float.parseFloat(value.equalsIgnoreCase("") ? "0.0" : value);
                                break;
                        }

                    } else {
                        value = getDataSet(dataSet);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Debugger.debugE("vaLUES.....", value);
        return value;
    }

    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    // set chart based on Validic data
    public void setChartForValidic(LevelInformationModel obj) {

        int leveltype = Integer.parseInt(obj.getLevel_type());
        int leveltotalvalue = Integer.parseInt(obj.getLevel_value());
        int secondLeveltotalvalue = 0, secondLevelType = 0;
        int levelvalue = 0, levelPer = 0, secondLevelValue = 0, secondLevelPer = 0;
        //fitChart.setMinValue(0f);
        //fitChart.setMaxValue(100f);
        Collection<FitChartValue> values = new ArrayList<>();

        switch (leveltype) {
            case 1://steps
                levelvalue = Integer.parseInt(obj.getSteps());
                break;
            case 2://calories
                levelvalue = Integer.parseInt(obj.getCalories());
                break;
            case 3://distance
                levelvalue = Integer.parseInt(obj.getDistance());
                break;
            case 4://heartrate
                break;
        }
        if (levelvalue <= leveltotalvalue) {
            levelPer = Math.round((levelvalue * 100) / leveltotalvalue);
        } else if (levelvalue > leveltotalvalue) {
            levelPer = 100;
        }

        if (obj.getSecond_level_type() != null) {
            secondLevelType = Integer.parseInt(obj.getSecond_level_type());
            secondLeveltotalvalue = Integer.parseInt(obj.getSecond_level_value());
            switch (secondLevelType) {
                case 1://steps
                    secondLevelValue = Integer.parseInt(obj.getSteps());
                    break;
                case 2://calories
                    secondLevelValue = Integer.parseInt(obj.getCalories());
                    break;
                case 3://distance
                    secondLevelValue = Integer.parseInt(obj.getDistance());
                    break;
                case 4://heartrate
                    break;
            }
        }

        if (secondLevelType != 0) {
            if (secondLevelValue <= secondLeveltotalvalue) {
                secondLevelPer = Math.round((secondLevelValue * 100) / secondLeveltotalvalue);
                levelPer = Math.round(levelPer / 2);
                secondLevelPer = Math.round(secondLevelPer / 2);
            } else if (secondLevelValue > secondLeveltotalvalue) {
                levelPer = Math.round(levelPer / 2);
                secondLevelPer = 50;
            }
            values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress5)));
            values.add(new FitChartValue((float) secondLevelPer, getResources().getColor(R.color.progress2)));
        } else {
            if (levelData.getLevel_type().equalsIgnoreCase("1"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress2)));
            else if (levelData.getLevel_type().equalsIgnoreCase("2"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress3)));
            else if (levelData.getLevel_type().equalsIgnoreCase("3"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress4)));
            else if (levelData.getLevel_type().equalsIgnoreCase("4"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress5)));
            else if (levelData.getLevel_type().equalsIgnoreCase("5"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress1)));
        }
        //fitChart.setValues(values);
    }

    // set chart based on google fit
    public void setChartForDevice() {
        int leveltype = Integer.parseInt(levelData.getLevel_type());
        int leveltotalvalue = Integer.parseInt(levelData.getLevel_value());
        int secondLeveltotalvalue = 0, secondLevelType = 0;
        int levelvalue = 0, secondLevelValue = 0;
        //fitChart.setMinValue(0f);
        // fitChart.setMaxValue(100f);
        Collection<FitChartValue> values = new ArrayList<>();
        ChartDataModel chartmodel = new ChartDataModel();
        switch (leveltype) {
            case 1://steps
                levelvalue = Integer.parseInt(levelStepsData);
                if (levelvalue < Integer.parseInt(levelData.getLevel_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setSteps(levelStepsData);
                //change
                chartmodel.setChallengetype(1);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                //chartmodel.setVal(Integer.valueOf(stepsData));
                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 2://calories
                levelvalue = Integer.parseInt(levelCaloriesData);
                if (levelvalue < Integer.parseInt(levelData.getLevel_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setCalories(levelCaloriesData);
                //change
                chartmodel.setChallengetype(2);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                //chartmodel.setVal(Integer.valueOf(caloriesData));
                chartmodel.setVal(Integer.valueOf(levelCaloriesData));
                chartmodel.setSecval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 3://distance
                levelvalue = Integer.parseInt(levelDistanceData);
                if (levelvalue < Integer.parseInt(levelData.getLevel_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setDistance(levelDistanceData);
                //change
                chartmodel.setChallengetype(3);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                //chartmodel.setVal(Integer.valueOf(distanceData));
                chartmodel.setVal(Integer.valueOf(levelDistanceData));
                chartmodel.setSecval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 4://heartrate
                levelvalue = Integer.parseInt(levelHeartRateData);
                if (levelvalue < Integer.parseInt(levelData.getLevel_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setResting_heart_rate(levelHeartRateData);
                //change
                chartmodel.setChallengetype(4);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                //chartmodel.setVal(Integer.valueOf(heartRateData));
                chartmodel.setVal(Integer.valueOf(levelHeartRateData));
                chartmodel.setSecval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 7://steps and calories
                levelvalue = Integer.parseInt(levelStepsData);
                if (Integer.parseInt(levelStepsData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Float.parseFloat(caloriesData) < Integer.parseInt(levelData.getSecond_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setSteps(levelStepsData);
                //change
                chartmodel.setChallengetype(7);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                //chartmodel.setVal(Integer.valueOf(stepsData));
                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(Integer.valueOf(caloriesData));
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
        }

        if (levelvalue <= leveltotalvalue) {
            levelPer = Math.round((levelvalue * 100) / leveltotalvalue);
        } else if (levelvalue > leveltotalvalue) {
            levelPer = 100;
        }
        if (levelData.getLevel_type().equalsIgnoreCase("6") || levelData.getLevel_type().equalsIgnoreCase("7")) {
            switch (Integer.parseInt(levelData.getLevel_type())) {
                case 6: //steps and distance
                    secondLevelType = Constant.level_distance;
                    secondLevelValue = Integer.parseInt(levelDistanceData);
                    secondLeveltotalvalue = Integer.parseInt(levelData.getSecond_level_value());
                    levelData.setCalories(levelDistanceData);
                    chartmodel.setLevelInformationModel(levelData);
                    //change
                    chartmodel.setChallengetype(6);
                    chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                    chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                    chartmodel.setVal(Integer.valueOf(levelStepsData));
                    chartmodel.setSecval(Integer.valueOf(levelDistanceData));
                    //chartmodellist.add(0, chartmodel);
                    if (isRefresh) {
                        chartmodellist.set(0, chartmodel);
                        isRefresh = false;
                    } else {
                        chartmodellist.add(0, chartmodel);
                    }

                    break;
                case 7://steps and calories
                    secondLevelType = Constant.level_calories;
                    secondLevelValue = Integer.parseInt(levelCaloriesData);
                    secondLeveltotalvalue = Integer.parseInt(levelData.getSecond_level_value());
                    levelData.setCalories(levelCaloriesData);
                    //change
                    chartmodel.setChallengetype(7);
                    chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                    chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                    chartmodel.setVal(Integer.valueOf(levelStepsData));
                    chartmodel.setSecval(Integer.valueOf(levelCaloriesData));
                    chartmodel.setLevelInformationModel(levelData);
                    //chartmodellist.add(0, chartmodel);
                    if (isRefresh) {
                        chartmodellist.set(0, chartmodel);
                        isRefresh = false;
                    } else {
                        chartmodellist.add(0, chartmodel);
                    }

                    break;
            }
        }

        if (secondLevelType != 0) {
            if (secondLevelValue <= secondLeveltotalvalue) {
                secondLevelPer = Math.round((secondLevelValue * 100) / secondLeveltotalvalue);
                levelPer = Math.round(levelPer / 2);
                secondLevelPer = Math.round(secondLevelPer / 2);
            } else if (secondLevelValue > secondLeveltotalvalue) {
                levelPer = Math.round(levelPer / 2);
                secondLevelPer = 50;
            }
            if (levelData.getLevel_type().equalsIgnoreCase("6")) {
                // for show step first and than distance in graph
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress2)));
                if (secondLevelType == 2)
                    values.add(new FitChartValue((float) secondLevelPer, getResources().getColor(R.color.progress3)));
                else if (secondLevelType == 3)
                    values.add(new FitChartValue((float) secondLevelPer, getResources().getColor(R.color.progress4)));
            } else if (levelData.getLevel_type().equalsIgnoreCase("7")) {
                // for show steps first and than calories in graph
                if (secondLevelType == 2)
                    values.add(new FitChartValue((float) secondLevelPer, getResources().getColor(R.color.progress3)));
                else if (secondLevelType == 3)
                    values.add(new FitChartValue((float) secondLevelPer, getResources().getColor(R.color.progress4)));
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress2)));
            }
        } else {
            if (levelData.getLevel_type().equalsIgnoreCase("1"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress2)));
            else if (levelData.getLevel_type().equalsIgnoreCase("2"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress3)));
            else if (levelData.getLevel_type().equalsIgnoreCase("3"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress4)));
            else if (levelData.getLevel_type().equalsIgnoreCase("4"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress5)));
            else if (levelData.getLevel_type().equalsIgnoreCase("5"))
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress1)));
            else if (levelData.getLevel_type().equalsIgnoreCase("7")) {
                values.add(new FitChartValue((float) levelPer, getResources().getColor(R.color.progress2)));
                values.add(new FitChartValue((float) secondLevelPer, getResources().getColor(R.color.progress3)));
            }
        }

        if (!isChallengesAvailable) {
            setupAdapter();
        }
        UpdateChallageStatus(status);
        //fitChart.setValues(values);
    }

    // api call for update level
    public void UpdateChallageStatus(String status) {
        try {
            JSONObject object = new JSONObject();
            object.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
            object.put(Constant.level_id, levelData.getLevel_number());
            object.put(Constant.status, status);

            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            object.put(Constant.timezone, tz.getID());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateandTime = sdf.format(new Date());
            object.put(Constant.complete_date, currentDateandTime);

            String cDate;
            Calendar c = Calendar.getInstance();
            Date currentDate = c.getTime();
            cDate = sdf.format(currentDate);
            try {
                currentDate = sdf.parse(cDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date end = sdf.parse(levelendDate);

            if (status.equalsIgnoreCase("0")) {
                if (currentDate.after(end)) {
                    UpdateLevel updateLevel = new UpdateLevel(getActivity(), object, onUpdateLevelListener, status);
                    updateLevel.execute();
                } else {
                    Log.e("Note => ", " No level updated");
                    showSyncPopupProcess();
                }
            } else {
                UpdateLevel updateLevel = new UpdateLevel(getActivity(), object, onUpdateLevelListener, status);
                updateLevel.execute();
                // set badge on profile
                int profileadge = UtilsPreferences.getInt(getActivity(), Constant.ProfileBadge);
                UtilsPreferences.setInt(getActivity(), Constant.ProfileBadge, profileadge + 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        syncData();
    }

    // success listener of UpdateLevel
    @Override
    public void onSucceedToUpdateLevel(LevelInformationModel obj, String msg, String status) {
        //getLevelInformation(false);

//        if (UtilsValidation.isLesserThanCurrentDate(levelStartDate)) {  // added on 11-07-2017 it is set to clear the duplicate list
//            if (status.equals("0")) {
//                if (chartmodellist != null && chartmodellist.size() > 0) {
//                    chartmodellist.clear();
//                }
//            }
//        }

        // clear LevelInformation prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(getActivity(), Constant.Key_level_info);

        // show popup
        TakeRestPopup(msg);
        flag_showsyncnotification = false;
        if (chartmodellist != null && chartmodellist.size() > 0) {
            chartmodellist.clear();
            if (adapter != null)
                adapter.notifyDataSetChanged();
        }

        getChallengesInfo();
    }

    // failed listener of UpdateLevel
    @Override
    public void onFaildToUpdateLevel(String error_msg, String status) {

        // clear LevelInformation prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(getActivity(), Constant.Key_level_info);

        if (error_msg != null && !error_msg.isEmpty())
            Toast.makeText(getActivity(), error_msg, Toast.LENGTH_SHORT).show();
        UtilsCommon.destroyProgressBar();
    }

    // sync data on server
    public void syncData() {
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
            mClient.connect();
            UpdateDeviceDataInBackground task = new UpdateDeviceDataInBackground(getActivity(), mClient);
            task.getData();
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            UpdateDeviceDataInBackground task = new UpdateDeviceDataInBackground(getActivity());
            task.getData();
        }
    }

    // success listener of StepsFromDeviceCall
    @Override
    public void onSucceedToGetStepData(String value) {
        Toast.makeText(getActivity(), value, Toast.LENGTH_SHORT).show();
    }

    // get data from fitness band and then set chart
    public void trackerleveldata(String levelStartDate, String levelendDate, int type) {
        if (BleSharedPreferences.getInstance().getIsBind()) {
            FitnessTrackerOperations fitnessTrackerOperations = new FitnessTrackerOperations(getActivity(), fitnessTrackerOperationslistener);
            fitnessTrackerOperations.readDatafromTracker(levelStartDate, levelendDate, type);
        } else {
            levelCaloriesData = "0";
            levelStepsData = "0";
            levelDistanceData = "0";
            levelHeartRateData = "0";
            levelSleepData = "0";
            setChartForTracker();
        }

    }

    // success listener of FitnessTrackerOperations
    @Override
    public void onSucceedtoFitnessTrackerOperation(int steps, int cals, int dis, int hearts, int sleep) {
        levelCaloriesData = String.valueOf(cals);
        levelStepsData = String.valueOf(steps);
        levelDistanceData = String.valueOf(dis);
        levelHeartRateData = String.valueOf(hearts);
        levelSleepData = String.valueOf(sleep);

        // added on 11-07-2017, to overite the fitness data after date change
/*        Float cal, stp;
        cal = (float) (cals);
        stp = (float) steps;

        setDataWithCounter(tv_caloriesData, cal);
        setDataWithCounter(tv_stepsData, stp);
        setDataWithCounter(tv_distanceData, UtilsCommon.getMiles((float) dis));
        tv_distanceData.setSuffix(" km");

        //set data in global
        Globals.homeDeviceMap.put(Constant.calories_burned, cal);

        Globals.homeDeviceMap.put(Constant.step, stp);
        Globals.homeDeviceMap.put(Constant.distance, UtilsCommon.getMiles((float) dis));*/

        setChartForTracker();
    }

    // set a chart based on level and challenge type
    public void setChartForTracker() {
        int leveltype = Integer.parseInt(levelData.getLevel_type());
        int leveltotalvalue = Integer.parseInt(levelData.getLevel_value());
        int secondLeveltotalvalue = 0, secondLevelType = 0;
        int levelvalue = 0, secondLevelValue = 0;
        //fitChart.setMinValue(0f);
        // fitChart.setMaxValue(100f);
        Collection<FitChartValue> values = new ArrayList<>();
        ChartDataModel chartmodel = new ChartDataModel();

        if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("2")) {
            if (this.chartmodellist.size() > 0)
                this.chartmodellist.clear();
        }

        switch (leveltype) {
            case 1://steps
                levelvalue = Integer.parseInt(levelStepsData);
                if (levelvalue < Integer.parseInt(levelData.getLevel_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setSteps(levelStepsData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(0);
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);
                //chartmodel.setVal(Integer.valueOf(stepsData));
                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(0);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 2://calories
                levelvalue = Integer.parseInt(levelCaloriesData);
                if (levelvalue < Integer.parseInt(levelData.getLevel_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setCalories(levelCaloriesData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(0);
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);
                //chartmodel.setVal(Integer.valueOf(caloriesData));
                chartmodel.setVal(Integer.valueOf(levelCaloriesData));
                chartmodel.setSecval(0);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 3://distance
                levelvalue = Integer.parseInt(levelDistanceData);
                if (levelvalue < Integer.parseInt(levelData.getLevel_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setDistance(levelDistanceData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(0);
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);
                //chartmodel.setVal(Integer.valueOf(distanceData));
                chartmodel.setVal(Integer.valueOf(levelDistanceData));
                chartmodel.setSecval(0);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 4://sleep
                levelvalue = Integer.parseInt(levelHeartRateData);
                if (levelvalue < Integer.parseInt(levelData.getLevel_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setResting_heart_rate(levelHeartRateData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(0);
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);
                //chartmodel.setVal(Integer.valueOf(heartRateData));
                chartmodel.setVal(Integer.valueOf(levelHeartRateData));
                chartmodel.setSecval(0);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 5://distance and sleep
                levelvalue = Integer.parseInt(levelStepsData);
                if (Integer.parseInt(levelDistanceData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelSleepData) < Integer.parseInt(levelData.getSecond_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setDistance(levelDistanceData);
                levelData.setSleep(levelSleepData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);
                //chartmodel.setVal(Integer.valueOf(heartRateData));
                chartmodel.setVal(Integer.valueOf(levelDistanceData));
                chartmodel.setSecval(Integer.valueOf(levelSleepData));
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 6: //steps and distance
                levelvalue = Integer.parseInt(levelStepsData);
                if (Integer.parseInt(levelStepsData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelDistanceData) < Integer.parseInt(levelData.getSecond_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }

                levelData.setSteps(levelStepsData);
                levelData.setDistance(levelDistanceData);

                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(Integer.valueOf(levelDistanceData));
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 7://steps and calories
                levelvalue = Integer.parseInt(levelStepsData);
                if (Integer.parseInt(levelStepsData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelCaloriesData) < Integer.parseInt(levelData.getSecond_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setSteps(levelStepsData);
                levelData.setCalories(levelCaloriesData);

                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                //chartmodel.setVal(Integer.valueOf(stepsData));
                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(Integer.valueOf(levelCaloriesData));
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 8://steps and sleep
                levelvalue = Integer.parseInt(levelStepsData);
                if (Integer.parseInt(levelStepsData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelSleepData) < Integer.parseInt(levelData.getSecond_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }
                levelData.setSteps(levelStepsData);
                levelData.setSleep(levelSleepData);

                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                //chartmodel.setVal(Integer.valueOf(stepsData));
                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(Integer.valueOf(levelSleepData));
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;

            case 9://sleep and calories
                levelvalue = Integer.parseInt(levelSleepData);
                if (Integer.parseInt(levelStepsData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelCaloriesData) < Integer.parseInt(levelData.getSecond_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }

                levelData.setSleep(levelSleepData);
                levelData.setCalories(levelCaloriesData);

                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(Integer.valueOf(levelSleepData));
                chartmodel.setSecval(Integer.valueOf(levelCaloriesData));
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 10://distance and calories

                levelvalue = Integer.parseInt(levelDistanceData);
                if (Integer.parseInt(levelDistanceData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelCaloriesData) < Integer.parseInt(levelData.getSecond_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }

                levelData.setDistance(levelDistanceData);
                levelData.setCalories(levelCaloriesData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(Integer.valueOf(levelDistanceData));
                chartmodel.setSecval(Integer.valueOf(levelCaloriesData));
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 11: //steps and calories and distance
                if (Integer.parseInt(levelStepsData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelCaloriesData) < Integer.parseInt(levelData.getSecond_level_value()) ||
                        Integer.parseInt(levelDistanceData) < Integer.parseInt(levelData.getThird_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }

                levelData.setSteps(levelStepsData);
                levelData.setDistance(levelDistanceData);
                levelData.setCalories(levelCaloriesData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(Integer.parseInt(levelData.getThird_level_value()));
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(Integer.valueOf(levelCaloriesData));
                chartmodel.setThirdval(Integer.valueOf(levelDistanceData));
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 12://steps and calories nad sleep
                if (Integer.parseInt(levelStepsData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelCaloriesData) < Integer.parseInt(levelData.getSecond_level_value()) ||
                        Integer.parseInt(levelSleepData) < Integer.parseInt(levelData.getThird_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }

                levelData.setSteps(levelStepsData);
                levelData.setSleep(levelSleepData);
                levelData.setCalories(levelCaloriesData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(Integer.parseInt(levelData.getThird_level_value()));
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(Integer.valueOf(levelCaloriesData));
                chartmodel.setThirdval(Integer.valueOf(levelSleepData));
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 13://steps and distance and sleep
                if (Integer.parseInt(levelStepsData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelDistanceData) < Integer.parseInt(levelData.getSecond_level_value()) ||
                        Integer.parseInt(levelSleepData) < Integer.parseInt(levelData.getThird_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }

                levelData.setSteps(levelStepsData);
                levelData.setSleep(levelSleepData);
                levelData.setDistance(levelDistanceData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(Integer.parseInt(levelData.getThird_level_value()));
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(Integer.valueOf(levelDistanceData));
                chartmodel.setThirdval(Integer.valueOf(levelSleepData));
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 14://calories and distance and sleep
                if (Integer.parseInt(levelCaloriesData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelDistanceData) < Integer.parseInt(levelData.getSecond_level_value()) ||
                        Integer.parseInt(levelSleepData) < Integer.parseInt(levelData.getThird_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }

                levelData.setCalories(levelCaloriesData);
                levelData.setSleep(levelSleepData);
                levelData.setDistance(levelDistanceData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(Integer.parseInt(levelData.getThird_level_value()));
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(Integer.valueOf(levelCaloriesData));
                chartmodel.setSecval(Integer.valueOf(levelDistanceData));
                chartmodel.setThirdval(Integer.valueOf(levelSleepData));
                chartmodel.setFourthval(0);
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
            case 15://steps and calories and distance and sleep
                if (Integer.parseInt(levelStepsData) < Integer.parseInt(levelData.getLevel_value()) ||
                        Integer.parseInt(levelCaloriesData) < Integer.parseInt(levelData.getSecond_level_value()) ||
                        Integer.parseInt(levelDistanceData) < Integer.parseInt(levelData.getThird_level_value()) ||
                        Integer.parseInt(levelSleepData) < Integer.parseInt(levelData.getForth_level_value())) {
                    status = "0";
                } else {
                    status = "1";
                }

                levelData.setSteps(levelStepsData);
                levelData.setCalories(levelCaloriesData);
                levelData.setSleep(levelSleepData);
                levelData.setDistance(levelDistanceData);
                //change
                chartmodel.setChallengetype(leveltype);
                chartmodel.setTotalval(Integer.valueOf(levelData.getLevel_value()));
                chartmodel.setSectotalval(Integer.valueOf(levelData.getSecond_level_value()));
                chartmodel.setThirdtotalval(Integer.parseInt(levelData.getThird_level_value()));
                chartmodel.setFourthtotalval(Integer.parseInt(levelData.getForth_level_value()));

                chartmodel.setVal(Integer.valueOf(levelStepsData));
                chartmodel.setSecval(Integer.valueOf(levelCaloriesData));
                chartmodel.setThirdval(Integer.valueOf(levelDistanceData));
                chartmodel.setFourthval(Integer.valueOf(levelSleepData));
                chartmodel.setLevelInformationModel(levelData);
                //chartmodellist.add(0, chartmodel);
                if (isRefresh) {
                    chartmodellist.set(0, chartmodel);
                    isRefresh = false;
                } else {
                    chartmodellist.add(0, chartmodel);
                }
                break;
        }
        setupAdapter();

        if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
            UpdateChallageStatus(status);
        }

//        else {
//            setupAdapter();
//        }

    }

    // next level listing adapter
    private void setupAdapter() {

        adapter = new HomeSlidingChartAdapter(getActivity(), this.chartmodellist, nextLevelChallengeModel);
//        adapter = new HomeSlidingChartAdapter(getActivity(), this.chartmodellist);
        viewPager.setAdapter(adapter);

        setLevelCurrentScreen(getActivity());
        indicator.setViewPager(viewPager);
        Globals.inPChallengesChartDataMode.clear();
        Globals.inPChallengesChartDataMode.addAll(this.chartmodellist);
        UtilsCommon.destroyProgressBar();
    }

    @Override
    public void onPause() {
        handler1.removeCallbacks(runnable);
        getActivity().unregisterReceiver(mReceiver); // unregister HANDLER
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        String userType = UtilsPreferences.getString(getActivity(), Constant.user_type);
        if (userType != null && !userType.isEmpty()) {
            IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            getActivity().registerReceiver(mReceiver, filter);
            if (userType.equalsIgnoreCase("1")) // Added on 25-07-2017
                swiperefresh.setEnabled(true);
            Application.recordScreenViews(getActivity(), Constant.Name_Dashboard);
        } else {
            Application global = (Application) getActivity().getApplicationContext();
            global.doLogout(getActivity());
        }
    }

    private void recordScreenViews() {
        // [START set_current_screen]
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "Dashboard Screen", null /* class override */);
        // [END set_current_screen]
    }

    // load layout screen
    public static void setChartforNotificationtype() {
        if (Globals.inPChallengesChartDataMode.size() > 1) {
            Globals.inPChallengesChartDataMode.subList(1, Globals.inPChallengesChartDataMode.size()).clear();

            adapter = new HomeSlidingChartAdapter
                    (DashboardActivity.getInstance(), Globals.inPChallengesChartDataMode, nextLevelChallengeModel);
            viewPager.setAdapter(adapter);
            UtilsCommon.destroyProgressBar();

            if (mContext != null)
                setLevelCurrentScreen(mContext.getActivity());
            indicator.setViewPager(viewPager);
        }
    }

    // register broadcast receiver for bluetooth
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        //setButtonText("Bluetooth off");
                        // Toast.makeText(context, "Ble off", Toast.LENGTH_SHORT).show();
                        String user_type = UtilsPreferences.getString(getActivity(), Constant.user_type);
                        if (user_type != null && user_type.length() > 0) {
                            if (user_type.equalsIgnoreCase("1"))
                                scheduleThread();
                        }
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        // setButtonText("Turning Bluetooth off...");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        // setButtonText("Bluetooth on");
                        // Toast.makeText(context, "Ble on", Toast.LENGTH_SHORT).show();
                        ProtocolUtils.getInstance().setCanConnect(true);
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        // setButtonText("Turning Bluetooth on...");
                        break;
                }
            }
        }
    };

    // sync animation
    public void scheduleThread() {
        if (UtilsCommon.checkBlutoothConnectivity()) {
            UtilsCommon.expand(ll_sync_progress);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 200ms
                    UtilsCommon.collapse(ll_sync_progress);
                }
            }, Constant.PostDelaySub);
        }
    }

    // api call for User_last_sync
    private void callLastSyncData() {
        String userType = UtilsPreferences.getString(getActivity(), Constant.user_type);
        if (userType != null && userType.length() > 0) {
            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                String cDate;
                Calendar c = Calendar.getInstance();
                Date currentDate = c.getTime();
                SimpleDateFormat formaterN = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                cDate = formaterN.format(currentDate);
                JSONObject data = new JSONObject();
                try {
                    data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
                    data.put(Constant.last_sync_date, cDate);
                    data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                User_last_sync user_last_sync = new User_last_sync(getActivity(), onGetUserLastSyncData, data, false);
                user_last_sync.execute();
            }
        }

    }

    // display level progress screen by default
    private static void setLevelCurrentScreen(Context context) {
        if (UtilsPreferences.getString(context, Constant.user_type).equalsIgnoreCase("1"))
            if (adapter.getCount() >= 2)
                viewPager.setCurrentItem(1);
    }

    // default alert dialog to display request message
    private void SyncPopup(String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder
                (getActivity(), R.style.MyAlertDialogStyle);
        alertDialog.setCancelable(false);
        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onRefresh();
                // Write your code here to invoke YES event
            }
        });

        alertDialog.show();
    }

    private void showSyncPopupProcess() {
        if (flag_showsyncnotification) {
            extra = getArguments();
            if (extra != null) {
                if (extra.containsKey(Constant.notification)) {
                    flag_showsyncnotification = false;
                    SyncPopup(extra.getString(Constant.notification));
                }
            }
        }

    }

    // api call for NextLevelChallengeListing
    private void callNextLevelChallengeListingApi() {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));

            if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                NextLevelChallengeListing task = new NextLevelChallengeListing(getActivity(), onGetNextLevelChallengeListingListener, data, true);
                task.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of NextLevelChallengeListing
    @Override
    public void onSucceedToGetNextLevelChallengeListingListener(NextLevelChallengeModel nextLevelChallengeModel) {
        UtilsCommon.destroyProgressBar();
        HomeFragment.nextLevelChallengeModel = nextLevelChallengeModel;
        getChallengesInfoProcess();
    }

    // failed listener of NextLevelChallengeListing
    @Override
    public void onFaildToGetNextLevelChallengeListingListener(String error_msg) {
        UtilsCommon.destroyProgressBar();
        getChallengesInfoProcess();
    }

    // get in process challenge list
    private void getChallengesInfoProcess() {
        //TODO
        ArrayList<ChallengesModel> inProgressList = UtilsPreferences.getInProgressChallengesSharedPrefrences(getActivity(), Constant.Key_inProgressList);
        UpdateChallengesStatusInBackgroundForTracker updateChallengesStatusInBackgroundForTracker = new UpdateChallengesStatusInBackgroundForTracker(getActivity(), onUpdateChallengesStatusInBackgroundForTracker);

        if (inProgressList != null && inProgressList.size() > 0) {
            Log.e("getShared : ", "" + inProgressList.size());
            updateChallengesStatusInBackgroundForTracker.onSucceedChallengesProcess(inProgressList);
        } else {
            UtilsCommon.showProgressDialog(getActivity());
            updateChallengesStatusInBackgroundForTracker.getData();
        }
    }

    // enable or disabled swipe to refresh
    public void toggleRefreshing(boolean enabled) {
        if (swiperefresh != null) {
            swiperefresh.setEnabled(enabled);
        }
    }

}