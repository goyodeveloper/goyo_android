package com.differenzsystem.goyog.dashboard.challenges;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.BandBaseFragment;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.adapter.ChallangesPageAdapter;
import com.differenzsystem.goyog.adapter.ChallengesAdapter;
import com.differenzsystem.goyog.api.ChallengesCall;
import com.differenzsystem.goyog.api.ChallengesCall.OnChallengesListener;
import com.differenzsystem.goyog.api.UpdateDeviceDataInBackground;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.differenzsystem.goyog.utility.WrapContentViewPager;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class ChallengesFragment extends BandBaseFragment implements OnChallengesListener, SwipeRefreshLayout.OnRefreshListener {
    WrapContentViewPager viewPager;
    CirclePageIndicator indicator;
    ListView list_data;
    LinearLayout ll_main, ll_no_challanges;

    OnChallengesListener onChallengesListener;
    ChallengesAdapter adapter;
    ArrayList<ChallengesModel> dataList = new ArrayList<>();
    public static ChallengesFragment mContext;
    SwipeRefreshLayout swiperefresh;

//    private FirebaseAnalytics mFirebaseAnalytics;

    public static ChallengesFragment newInstance(Bundle extra) {
        ChallengesFragment fragment = new ChallengesFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.challenges_fragment, container, false);
        initializeControls(view);
        initializeControlsAction();
        getList();
        if (!UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) { // enable swipe to refresh only for live user
            swiperefresh.setEnabled(false);
        }
        return view;
    }

    // initialize all controls define in xml layout
    public void initializeControls(View view) {
        try {
            swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_view);
            swiperefresh.setOnRefreshListener(this);
            swiperefresh.setColorSchemeResources(
                    R.color.colorPrimary);
            mContext = this;
            viewPager = (WrapContentViewPager) view.findViewById(R.id.viewPager);
            indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
            indicator.setStrokeWidth(2);
            indicator.setStrokeColor(getActivity().getResources().getColor(R.color.pager_selected));
            list_data = (ListView) view.findViewById(R.id.list_data);
            ll_main = (LinearLayout) view.findViewById(R.id.ll_main);
            ll_no_challanges = (LinearLayout) view.findViewById(R.id.ll_no_challanges);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            onChallengesListener = this;
            list_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ChallengesModel object = dataList.get(position);
                    Bundle extras = new Bundle();
                    extras.putSerializable("Object", object);
                    if (object.getWeek_flag().equalsIgnoreCase("1")) {
                        addFragment(InProcessChallangeDetailFragment.newInstance(extras), "InProcessChallengesDetailFragment");
                    } else {
                        addFragment(ChallengesDetailFragment.newInstance(extras), "ChallengesDetailFragment");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        ViewPager.OnPageChangeListener myOnPageChangeListener = new ViewPager.OnPageChangeListener() {

            //declare key
            Boolean first = true;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                toggleRefreshing(state == ViewPager.SCROLL_STATE_IDLE);
            }
        };

        viewPager.addOnPageChangeListener(myOnPageChangeListener);
    }

    public void toggleRefreshing(boolean enabled) {
        if (swiperefresh != null) {
            swiperefresh.setEnabled(enabled);
        }
    }

    public static ChallengesFragment getInstance() {
        return mContext;
    }

    // open required screen

    /**
     * @param fragment add fragment
     */
    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = this.getParentFragment().getFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
        fragmentTransaction.add(R.id.frame_main, fragment, null);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    // get the list of challenges
    public void getList() {
        try {
            swiperefresh.setRefreshing(false);
            JSONObject object = new JSONObject();
            object.put(Constant.user_id, UtilsPreferences.getString(getContext(), Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));

            if (ConnectionDetector.isConnectingToInternet(getActivity()))
                new ChallengesCall(getContext(), onChallengesListener, object, true).execute();
            else {
                // Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of ChallengesCall and based on current and last week all challenges are appear in list
    @Override
    public void onSucceedChallenges(ArrayList<ChallengesModel> inProcessList, ArrayList<ChallengesModel> thisWeekList, ArrayList<ChallengesModel> lastWeekList, ArrayList<ChallengesModel> nextWeekList) {
        try {
            if (inProcessList.size() > 0) {
                ChallengesModel inProcessObject = new ChallengesModel();
                inProcessObject.setSectionName(Constant.ChallengeType_Inprogress);
                inProcessObject.setSection(true);
                dataList.add(inProcessObject);
                dataList.addAll(inProcessList);
                UtilsPreferences.setBoolean(getActivity(), Constant.isInProgress, true);
            } else {
                UtilsPreferences.setBoolean(getActivity(), Constant.isInProgress, false);
            }

            if (thisWeekList.size() > 0) {
                ChallengesModel thisWeekObject = new ChallengesModel();
                thisWeekObject.setSectionName(Constant.ChallengeType_thisweek);
                thisWeekObject.setSection(true);
                dataList.add(thisWeekObject);
                dataList.addAll(thisWeekList);
            }

            if (lastWeekList.size() > 0) {
                ChallengesModel lastWeekObject = new ChallengesModel();
                lastWeekObject.setSectionName(Constant.ChallengeType_lastweek);
                lastWeekObject.setSection(true);
                dataList.add(lastWeekObject);
                dataList.addAll(lastWeekList);
            }

            if (nextWeekList.size() > 0) {
                ChallengesModel nextWeekObject = new ChallengesModel();
                nextWeekObject.setSectionName(Constant.ChallengeType_nextweek);
                nextWeekObject.setSection(true);
                dataList.add(nextWeekObject);
                dataList.addAll(nextWeekList);
            }

            if (dataList.size() > 0) {
                adapter = new ChallengesAdapter(getActivity(), dataList);
                list_data.setAdapter(adapter);
                ll_main.setVisibility(View.VISIBLE);
                ll_no_challanges.setVisibility(View.GONE);
            } else {
                ll_main.setVisibility(View.GONE);
                ll_no_challanges.setVisibility(View.VISIBLE);
            }

            if (dataList.size() > 0) {
                ArrayList<ChallengesModel> pagerList = new ArrayList<>();
                for (int i = 0; i < dataList.size(); i++) {
                    ChallengesModel object = dataList.get(i);
                    if (!object.isSection()) {
                        if (dataList.get(i).getBanner_flag().equalsIgnoreCase("1")) {
                            ChallengesModel sectionObject = object;
                            pagerList.add(sectionObject);
                        }
                    }
                }

                viewPager.setAdapter(new ChallangesPageAdapter(this, getActivity(), pagerList));
                indicator.setViewPager(viewPager);
                viewPager.setOffscreenPageLimit(pagerList.size() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        syncData();
    }

    // get the data from band
    public void syncData() {
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
            HomeFragment.mClient.connect();
            UpdateDeviceDataInBackground task = new UpdateDeviceDataInBackground(getActivity(), HomeFragment.mClient);
            task.getData();
        }
    }

    // failed listener of ChallengesCall
    @Override
    public void onFailedChallenges(String message) {
        if (message != null && message.length() > 0)
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    // swipe to refresh call to refresh the challenge list
    @Override
    public void onRefresh() {
        swiperefresh.setRefreshing(true);
        dataList = new ArrayList<>();
        getList(); // get the list of challenges
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Challenges);
    }

    private void recordScreenViews() {
        // [START set_current_screen]
//        mFirebaseAnalytics.setCurrentScreen(getActivity(), "Challenges Screen", null /* class override */);
        // [END set_current_screen]
    }

}
