package com.differenzsystem.goyog.dashboard.home;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.NextLevelChallengeModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Union Assurance PLC on 7/26/17.
 */

public class NextLevelDetailFragment extends Fragment {

    @BindView(R.id.img_level)
    CircleImageView img_level;

    @BindView(R.id.tv_level_name)
    TextView tv_level_name;

    @BindView(R.id.tv_level_desc)
    TextView tv_level_desc;

    @BindView(R.id.img_close)
    ImageView img_close;

    NextLevelChallengeModel.Level_data level_data;

    Bundle extra;

    // create the new instance of screen
    public static NextLevelDetailFragment newInstance(Bundle extra) {
        NextLevelDetailFragment fragment = new NextLevelDetailFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_next_level_detail, container, false);
        ButterKnife.bind(this, view);

        // bind the data of level detail when user click on any cell of next level
        extra = getArguments();
        if (extra != null) {
            if (extra.containsKey(Constant.key_next_level_object)) {
                level_data = (NextLevelChallengeModel.Level_data) extra.getSerializable(Constant.key_next_level_object);
            }
        }
        if (level_data != null) {
            tv_level_name.setText(level_data.level_subtitle);
            if (level_data.level_description != null)
                tv_level_desc.setText(level_data.level_description.trim());

            String url = getResources().getString(R.string.server_url)
                    + getResources().getString(R.string.badges_image)
                    + level_data.level_image;
            Glide.with(getActivity())
                    .load(url)
                    .centerCrop()
                    .placeholder(R.drawable.app_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .into(img_level);

        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Next_Level_Detail);
    }

    @OnClick(R.id.img_close)
    public void closeClick() {
        getActivity().onBackPressed();
    }
}
