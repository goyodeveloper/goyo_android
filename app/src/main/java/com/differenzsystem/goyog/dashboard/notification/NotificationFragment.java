package com.differenzsystem.goyog.dashboard.notification;


import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.adapter.NotificationFragmentAdapter;
import com.differenzsystem.goyog.api.DeleteNotificationFromList;
import com.differenzsystem.goyog.api.GetNotificationList;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.NotificationModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Union Assurance PLC on 7/26/17.
 */

public class NotificationFragment extends Fragment implements GetNotificationList.OnGetNotificationListListener, NotificationFragmentAdapter.OnItemClickListener, DeleteNotificationFromList.OnDeleteNotificationFromListListener,
        SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recy_notification)
    RecyclerView recy_notification;

    @BindView(R.id.ll_back)
    LinearLayout ll_back;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.swipe_view)
    SwipeRefreshLayout swipe_view;

    @BindView(R.id.tv_no_notification_data)
    TextView tv_no_notification_data;

    GetNotificationList.OnGetNotificationListListener notificationListListener;
    NotificationFragmentAdapter.OnItemClickListener onItemClickListener;
    DeleteNotificationFromList.OnDeleteNotificationFromListListener onDeleteNotificationFromListListener;

    NotificationModel notificationModel;

    private NotificationFragmentAdapter notificationFragmentAdapter;
    public static NotificationFragment mContext;

    private Paint p = new Paint();

    // create the new instance of screen
    public static NotificationFragment newInstance(Bundle extra) {
        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static NotificationFragment getInstance() {
        // return context of NotificationFragment
        return mContext;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_notification, container, false);
        ButterKnife.bind(this, view);

        initializeStartProcess();
        // call notification
        callGetNotificationListApi(true);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Notification);
    }

    // initialize process when screen loads
    private void initializeStartProcess() {
        onItemClickListener = this;
        notificationListListener = this;
        onDeleteNotificationFromListListener = this;
        swipe_view.setOnRefreshListener(this);
        ll_back.setVisibility(View.INVISIBLE);
        tv_title.setText(getString(R.string.notification_title));
    }

    // load adapter for notification list
    private void initializeAdapters() {
        recy_notification.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recy_notification.setLayoutManager(layoutManager);
        notificationFragmentAdapter = new NotificationFragmentAdapter(getActivity(), notificationModel, onItemClickListener, tv_no_notification_data);
//        recy_notification.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity(), R.drawable.divider_drawable)));
        recy_notification.setAdapter(notificationFragmentAdapter);
        notificationFragmentAdapter.notifyDataSetChanged();
//        initSwipe();
//        setUpItemTouchHelper();
        showNoNotificationFound(notificationModel);
    }

    // api call for GetNotificationList

    /**
     * @param flag boolean value for enabling swipe to refresh
     */
    private void callGetNotificationListApi(boolean flag) {
        try {
            swipe_view.setRefreshing(false);
            JSONObject data = new JSONObject();
            data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));

            if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                GetNotificationList task = new GetNotificationList(getActivity(), notificationListListener, data, flag);
                task.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener for GetNotificationList and bind the data
    @Override
    public void onSucceedToGetNotificationListListener(NotificationModel notificationModel) {
        tv_no_notification_data.setVisibility(View.GONE);
        this.notificationModel = notificationModel;
        initializeAdapters();
    }

    // failed listener for GetNotificationList
    @Override
    public void onFaildToGetNotificationListListener(String error_msg) {
        showNoNotificationFound(notificationModel);
    }

    // show no notification label when notificationModel is null or empty
    void showNoNotificationFound(NotificationModel notificationModel) {
        if (notificationModel != null && notificationModel.data != null && notificationModel.data.size() > 0)
            tv_no_notification_data.setVisibility(View.GONE);
        else
            tv_no_notification_data.setVisibility(View.VISIBLE);
    }

    // process for delete notification

    /**
     * @param flag boolean for showing progress dialog
     * @param id   id for notification
     * @param pos  get position in list
     */
    private void callDeleteNotificationFromList(boolean flag, String id, int pos) {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
            data.put(Constant.id, id);

            if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                DeleteNotificationFromList task = new DeleteNotificationFromList(getActivity(), pos, onDeleteNotificationFromListListener, data, flag);
                task.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // here when user click on delete then api call for deleting the selectable notification will be call
    @Override
    public void onItemClick(String id, int position) {
        callDeleteNotificationFromList(true, id, position);
    }

    @Override
    public void onItemLongClick(int position) {

    }

    // remove the deleted notification from list after the success of DeleteNotificationFromList api
    @Override
    public void onSucceedToDeleteNotificationFromListListener(int pos) {
        notificationFragmentAdapter.removeItem(pos);
    }

    @Override
    public void onFaildToDeleteNotificationFromListListener(String error_msg) {

    }

    // swipe to refresh process and rebind the rfresh data in list
    @Override
    public void onRefresh() {
        swipe_view.setRefreshing(true);
        callGetNotificationListApi(true);
    }
}
