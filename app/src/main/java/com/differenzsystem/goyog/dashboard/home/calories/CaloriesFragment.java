package com.differenzsystem.goyog.dashboard.home.calories;

import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.utility.UtilsPermission;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by root4 on 12/8/16.
 */

public class CaloriesFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_back, ll_main;
    RelativeLayout ll_no_permission;
    Button btn_allow_access;
    TextView tv_title;
    View rootview;
    ViewPager viewPager;
    CirclePageIndicator indicator;
    ScreenSlidePagerAdapter slidePagerAdapter;
    CaloriesOfDayFragment caloriesOfDayFragment;
    CaloriesWeekWiseFragment caloriesWeekWiseFragment;
    CaloriesMonthWiseFragment caloriesMonthWiseFragment;

    // create the newInstance of CaloriesFragment
    public static CaloriesFragment newInstance(Bundle extra) {
        CaloriesFragment fragment = new CaloriesFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slidePagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager());
    }

    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.calories_fragment, container, false);

        initializeControls(rootview);
        initializeControlsAction();

        return rootview;

    }

    // initialize all controls define in xml layout

    /**
     *
     * @param view get layout view
     */
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    public void initializeControls(View view) {
        try {
            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            ll_main = (LinearLayout) view.findViewById(R.id.ll_main);
            ll_no_permission = (RelativeLayout) view.findViewById(R.id.ll_no_permission);
            btn_allow_access = (Button) view.findViewById(R.id.btn_allow_access);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_title.setText(getString(R.string.calories_lbl));

            caloriesOfDayFragment = CaloriesOfDayFragment.newInstance(null);
            caloriesWeekWiseFragment = CaloriesWeekWiseFragment.newInstance(null);
            caloriesMonthWiseFragment = CaloriesMonthWiseFragment.newInstance(null);

            viewPager = (ViewPager) view.findViewById(R.id.viewPager);
            indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
            indicator.setStrokeWidth(2);
            indicator.setStrokeColor(getActivity().getResources().getColor(R.color.pager_selected));

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) { // condition for live user

                // condition for band connection
                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                    checkForPermission();
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    viewPager.setAdapter(slidePagerAdapter);
                    indicator.setViewPager(viewPager);
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                    viewPager.setAdapter(slidePagerAdapter);
                    indicator.setViewPager(viewPager);
                }
            } else {
                // load adapter
                viewPager.setAdapter(slidePagerAdapter);
                indicator.setViewPager(viewPager);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
            btn_allow_access.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // require permission BODY_SENSORS
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    public void checkForPermission() {
        boolean flag = UtilsPermission.checkBodySensor(getActivity());

        if (flag) {
            ll_main.setVisibility(View.VISIBLE);
            ll_no_permission.setVisibility(View.GONE);
            viewPager.setAdapter(slidePagerAdapter);
            indicator.setViewPager(viewPager);
        } else {
            ll_no_permission.setVisibility(View.VISIBLE);
            ll_main.setVisibility(View.GONE);
            Toast.makeText(getActivity(), getActivity().getString(R.string.msg_bodysensor_permission), Toast.LENGTH_SHORT).show();
        }
    }

    // handle click listener
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ll_back:
              /*  if (HomeFragment.mClient != null && HomeFragment.mClient.isConnected()) {
                    HomeFragment.mClient.stopAutoManage(getActivity());
                    HomeFragment.mClient.disconnect();
                }*/
                getActivity().onBackPressed();
                break;
            case R.id.btn_allow_access:
                checkForPermission();
                break;

            default:
                break;
        }
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return caloriesOfDayFragment;
                case 1:
                    return caloriesWeekWiseFragment;
                case 2:
                    return caloriesMonthWiseFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    // disconnect GoogleApiClient and remove resources
    @Override
    public void onStop() {
        super.onStop();
        if (HomeFragment.mClient != null && HomeFragment.mClient.isConnected()) {
            HomeFragment.mClient.stopAutoManage(getActivity());
            HomeFragment.mClient.disconnect();
        }
    }
}
