package com.differenzsystem.goyog.dashboard.home.heartRate;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link HeartRateEmptyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HeartRateEmptyFragment extends Fragment implements View.OnClickListener {
    View rootview;
    LinearLayout ll_back;
    TextView tv_title;

    public HeartRateEmptyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HeartRateEmptyFragment.
     */
    // TODO: Rename and change types and number of parameters
    // create the new instance of HeartRateEmptyFragment
    public static HeartRateEmptyFragment newInstance(Bundle bundle) {
        HeartRateEmptyFragment fragment = new HeartRateEmptyFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.fragment_heart_rate_empty, container, false);
            initcomopnets(rootview);
        } else
            container.removeView(rootview);

        return rootview;
    }

    // initialize all controls define in xml layout and set click listener

    /**
     *
     * @param rootview layout view
     */
    private void initcomopnets(View rootview) {
        try {
            ll_back = (LinearLayout) rootview.findViewById(R.id.ll_back);
            tv_title = (TextView) rootview.findViewById(R.id.tv_title);
            tv_title.setText("Heart Rate emp");
            ll_back.setOnClickListener(this);
        } catch (Exception e) {
        }
    }

    // handle click listener
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;
            default:
                break;
        }
    }
}
