package com.differenzsystem.goyog.dashboard.home.distance;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.syncdevice;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall.OnGetRoutineFromValidicListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.model.CategoryModel;
import com.differenzsystem.goyog.model.ValidicRoutineModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.SpriteFactory;
import com.github.ybq.android.spinkit.Style;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.veryfit.multi.nativedatabase.HealthSportItem;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;

import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;
import static java.text.DateFormat.getTimeInstance;

public class DistanceDayWiseFragment extends Fragment implements OnGetRoutineFromValidicListener, SwipeRefreshLayout.OnRefreshListener, syncdevice.Onsyncdevice {

    View rootview;
    CombinedChart mChart;
    TextView tv_distance, tv_date, tv_total_miles;
    LinearLayout lin_dayView;
    ArrayList<HashMap<String, String>> lst_day_distance;
    OnGetRoutineFromValidicListener onGetRoutineFromValidicListener;
    float total_distance = 0;
    int hours;
    String distanceData = "0";
    SwipeRefreshLayout swipeRefreshLayout;
    syncdevice.Onsyncdevice onsyncdevice;

    /**
     * Note
     * usertype 1 = live user
     * usertype 2 = demo user
     * connect_flag = connected_with_validic means get data from validic
     * connect_flag = connected_with_device means get data from google fit
     * connect_flag = connected_with_tracker means get data from band
     */

    public DistanceDayWiseFragment() {
        // Required empty public constructor
    }

    // create the new instance of DistanceDayWiseFragment
    public static DistanceDayWiseFragment newInstance(Bundle bundle) {
        DistanceDayWiseFragment fragment = new DistanceDayWiseFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onsyncdevice = this;
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.distance_day_wise_fragment, container, false);
            initcomopnets(rootview);
        } else {
            container.removeView(rootview);
        }

        return rootview;
    }


    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Distance_Day_Wise);
    }

    // initialize all controls define in xml layout

    /**
     *
     * @param rootview layout view
     */
    private void initcomopnets(View rootview) {
        try {
            swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.swipe_view);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(
                    R.color.colorPrimary);
            mChart = (CombinedChart) rootview.findViewById(R.id.chart_calories);

            lin_dayView = (LinearLayout) rootview.findViewById(R.id.lin_dayView);
            lst_day_distance = new ArrayList<>();

            tv_distance = (TextView) rootview.findViewById(R.id.tv_distance);
            tv_date = (TextView) rootview.findViewById(R.id.tv_date);
            tv_total_miles = (TextView) rootview.findViewById(R.id.tv_total_miles);
            lin_dayView.setVisibility(View.VISIBLE);
            hours = new Time(System.currentTimeMillis()).getHours();

            Log.e("global dis data", String.valueOf(Globals.DisDayMap));

            onGetRoutineFromValidicListener = this;

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                //if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                //    getDaywiseData(true);
                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    if (BleSharedPreferences.getInstance().getIsBind()) {
                        getDayData();
                    }else{
                        swipeRefreshLayout.setEnabled(false);
                        setupDemoUserDay();
                    }
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                    //TODO:get Date from device
                    getDeviceDistanceyData();
                }
            } else {
                swipeRefreshLayout.setEnabled(false);
                setupDemoUserDay();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // setup data for demo user
    private void setupDemoUserDay() {
        hours = new Time(System.currentTimeMillis()).getHours() + 1;

        for (int i = 0; i < 24; i++) {
            HashMap<String, String> temp = new HashMap<>();
            temp.put(Constant.time, new Date().toString());
            int sum = 0;
            if (i < hours) {
                temp.put(Constant.distance, "0");
                lst_day_distance.add(temp);
            } else {
                break;
            }
        }
        tv_total_miles.setText("0 KM");

        UtilsCommon.destroyProgressBar();
        dayWiseGraph(mChart, tv_distance, tv_date);

    }

    // get data from band
    public void getDayData() {
        Calendar mCalendar = Calendar.getInstance();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
        hours = new Time(System.currentTimeMillis()).getHours() + 1;

        List<HealthSportItem> dateHealthSportItem = ProtocolUtils.getInstance().getHealthSportItem(new Date(year, month, day));

        if (dateHealthSportItem != null && dateHealthSportItem.size() > 0) {
            Date startDate = dateHealthSportItem.get(0).getDate();
            List<Integer> stepsdata = new ArrayList<>();

            int[] intArray = new int[96];
            for (int i = 0; i < dateHealthSportItem.size(); i++)
                intArray[i] = dateHealthSportItem.get(i).getDistance();
            //stepsdata.add(dateHealthSportItem.get(i).getDistance());

            //int[] intArray = new int[stepsdata.size()];

            int x = 4;  // chunk size
            int len = intArray.length;
            int counter = 0;
            int[][] newArray = new int[len][];

            for (int i = 0; i < len - x + 1; i += x)
                newArray[counter++] = Arrays.copyOfRange(intArray, i, i + x);

            if (len % x != 0)
                newArray[counter] = Arrays.copyOfRange(intArray, len - len % x, len);

            Log.e("Hours data:", newArray.toString());

            for (int i = 0; i < 23; i++) {
                HashMap<String, String> temp = new HashMap<>();
                temp.put(Constant.time, UtilsCommon.getDateDayName(startDate.toString()));
                int sum = 0;
                Double kilometers = 0.0;
                if (i < hours) {
                    for (int j = 0; j < newArray[i].length; j++) {
                        sum += newArray[i][j];
                        kilometers = sum * 0.001;
                        String distenceinkm = String.valueOf(new DecimalFormat("##.##").format(kilometers));
                        temp.put(Constant.distance, String.valueOf(distenceinkm));
                    }
                    total_distance = (float) (total_distance + kilometers);
                    lst_day_distance.add(temp);
                } else {
                    break;
                }
            }

            //tv_total_miles.setText(String.valueOf(total_distance) + " KM");
            tv_total_miles.setText(String.format("%.1f", total_distance) + " KM");
            // setDataWithCounter(tv_total_calories, total_calories);
            Debugger.debugE("Day_steps..", hours + " " + lst_day_distance.size() + " " + lst_day_distance.toString());

            if (lst_day_distance.size() == hours) {
                Debugger.debugE("inside_Calories..", lst_day_distance.size() + " " + lst_day_distance.toString());
                UtilsCommon.destroyProgressBar();
                dayWiseGraph(mChart, tv_distance, tv_date);
            }
        }else{
            setupDemoUserDay();
        }
    }

    private void getDeviceDistanceyData() {
        try {
            if (ConnectionDetector.internetCheck(getContext())) {
                buildFitnessClient();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Googleapiclient connect
    private void buildFitnessClient() {
        if (HomeFragment.mClient != null) {
            if (Globals.DisDayMap.size() > 0)
                setDataFromGlobals();
            else {
                UtilsCommon.showProgressDialog(getActivity());
                new DistanceCall().execute();
            }
        }
    }

    // get distance data from shared prefrences
    public void setDataFromGlobals() {
        HashMap<String, Object> map = Globals.DisDayMap;
        tv_total_miles.setText(String.valueOf(map.get(Constant.distance)));
        lst_day_distance = (ArrayList<HashMap<String, String>>) map.get("daylist");
        dayWiseGraph(mChart, tv_distance, tv_date);
    }

    // swipe to refresh call and rebind the refresh data
    @Override
    public void onRefresh() {
        total_distance = 0;
        lst_day_distance = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        getAndSetData();
    }

    // success listener of syncdevice
    @Override
    public void onSucceedsyncdevice() {
        swipeRefreshLayout.setRefreshing(false);
        getDayData();
    }

   /* @Override
    public void onFailedsyncdevice() {

    }*/

    // here get distance data from band
    private void getAndSetData() {
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            getDaywiseData(false);
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            syncdevice s = new syncdevice(getContext(), onsyncdevice);
            s.syncdata();
        } else {
            new DistanceCall().execute();
        }
    }

    // request data for distance
    private class DistanceCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(HomeFragment.mClient, readRequest).await(1, TimeUnit.MINUTES);
                distanceData = readData(dataReadResult);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                UtilsCommon.destroyProgressBar();
                String number = String.valueOf(total_distance); // Convert to string
                if ((number.substring(number.indexOf(".")).length() - 1) > 2) {
                    tv_total_miles.setText(String.format("%.2f", total_distance) + " KM");
                    Globals.DisDayMap.put(Constant.distance, String.format("%.2f", total_distance) + " Km");
                } else {
                    tv_total_miles.setText(String.valueOf(total_distance) + " Km");
                    Globals.DisDayMap.put(Constant.distance, String.valueOf(total_distance) + " Km");
                }
                dayWiseGraph(mChart, tv_distance, tv_date);
                // tv_distanceData.setText(distanceData.equalsIgnoreCase("")?"0 mile":distanceData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // request data from google fit
    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            Calendar startCalendar = Calendar.getInstance();
            long startTime = startCalendar.getTimeInMillis();

            SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
            String startTimeString = formater.format(startTime) + " 12:01 AM";
            Date startDate = null;
            try {
                startDate = formater.parse(startTimeString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long startMillisecond = startDate.getTime();

            Calendar endCalendar = Calendar.getInstance();
            Date endDate = new Date();
            endCalendar.setTime(endDate);
            long endMillisecond = endCalendar.getTimeInMillis();

            Debugger.debugE("Start time", startDate + "=>");
            Debugger.debugE("End time", endDate + "=>");

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.HOURS)
                    .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    // read specific data by challenge type
    public String readData(DataReadResult dataReadResult) {
        String value = "";

        try {
            if (dataReadResult.getBuckets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getBuckets().size());
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                        total_distance += Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value);
                        if (value.equalsIgnoreCase("")) {
                            HashMap<String, String> temp = new HashMap<>();
                            temp.put(Constant.time, "");
                            temp.put(Constant.distance, "0");
                            lst_day_distance.add(temp);
                        }
                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                    total_distance += Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value);
                    if (value.equalsIgnoreCase("")) {
                        HashMap<String, String> temp = new HashMap<>();
                        temp.put(Constant.time, "");
                        temp.put(Constant.distance, "0");
                        lst_day_distance.add(temp);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }
                value = String.valueOf(UtilsCommon.getMiles(Float.parseFloat(value)));
                HashMap<String, String> temp = new HashMap<>();
                String sDate = new SimpleDateFormat("hh:mm").format(dp.getStartTime(TimeUnit.MILLISECONDS));
                String eDate = new SimpleDateFormat("hh:mm").format(dp.getEndTime(TimeUnit.MILLISECONDS));
                String time = sDate + "-" + eDate;
                temp.put(Constant.time, time);
                temp.put(Constant.distance, value);

                lst_day_distance.add(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * @param mChart      get the chart for displaying distance progress
     * @param tv_distance get textview of distance
     * @param tv_date     get textview of date
     */
    private void dayWiseGraph(CombinedChart mChart, final TextView tv_distance, final TextView tv_date) {
        Globals.DisDayMap.put("daylist", lst_day_distance);
        swipeRefreshLayout.setRefreshing(false);
        try {
            mChart.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setHighlightPerDragEnabled(false);
            mChart.setPinchZoom(false);
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.setDescription("");
            mChart.setExtraOffsets(30, 30, 30, 30);
            mChart.setTouchEnabled(true);
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setPinchZoom(false);

            Legend l = mChart.getLegend();
            l.setEnabled(false);

            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<CategoryModel> dataList = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            for (int i = 0; i < 24; i++) {
                if (i < lst_day_distance.size()) {
                    HashMap<String, String> temp = lst_day_distance.get(i);
                    float cal = Float.parseFloat(temp.get(Constant.distance));
                    entries.add(new Entry(i, cal));
                    dataList.add(new CategoryModel(cal, UtilsCommon.dayArray[i]));
                    labels.add(UtilsCommon.dayArray[i]);
                    if (i == 0) {
                        tv_distance.setText(temp.get(Constant.distance) + " km");
                        tv_date.setText(UtilsCommon.dayArray[i]);
                    }
                } else {
                    entries.add(new Entry(i, (float) -1));
                    dataList.add(new CategoryModel((float) -1, UtilsCommon.dayArray[i]));
                    labels.add(UtilsCommon.dayArray[i]);
                    labels.add(UtilsCommon.dayArray[i]);
                    if (i == 0) {
                        tv_distance.setText(0 + " miles");
                        tv_date.setText(UtilsCommon.dayArray[i]);
                    }
                }
            }
            LineDataSet dataset = new LineDataSet(entries, "# of Calls");

            CombinedData cData = new CombinedData();
            LineData data = new LineData();

            dataset.setColors(new int[]{getActivity().getResources().getColor(R.color.app_light_green)});
            dataset.setValueTextColor(getActivity().getResources().getColor(R.color.app_light_green));
            dataset.setCircleColor(getActivity().getResources().getColor(R.color.line_chart_circle));
            dataset.setCircleColorHole(getActivity().getResources().getColor(R.color.line_chart_circle_hole));
            dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataset.setDrawFilled(false);
            dataset.setCircleSize(10);
            dataset.setDrawValues(false);
            dataset.setDrawHighlightIndicators(false);
            dataset.setLineWidth((float) 2.5);

            SpinKitView spinKitView = new SpinKitView(getActivity());
            Style style = Style.values()[4];
            Sprite drawable = SpriteFactory.create(style);
            spinKitView.setIndeterminateDrawable(drawable);
            dataset.setSpinKitView(spinKitView);

            data.addDataSet(dataset);
            cData.setData(data);
            mChart.setData(cData);
            mChart.animateX(2500);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setTextSize(0f);
            xAxis.setTextColor(Color.TRANSPARENT);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);
            xAxis.setAxisMaximum(data.getXMax() + 0.25f);
            xAxis.setPosition(XAxisPosition.BOTH_SIDED);
            xAxis.setGranularity(1f);

            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setDrawGridLines(false);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setTextSize(0f);
            leftAxis.setTextColor(Color.TRANSPARENT);
            leftAxis.setDrawLimitLinesBehindData(true);
            leftAxis.setDrawGridLines(false);

            mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    CategoryModel object = dataList.get((int) e.getX());
                    tv_distance.setText(String.valueOf(object.item1) + " km");
                    tv_date.setText(object.item2);
                }

                @Override
                public void onNothingSelected() {

                }
            });

            mChart.getAxisRight().setEnabled(false);
            mChart.invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // GetRoutineDataFromValidicCall api call

    /**
     *
     * @param showProgress boolean for showing progressbar
     */
    public void getDaywiseData(boolean showProgress) {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            if (showProgress)
                UtilsCommon.showProgressDialog(getActivity());
            for (int i = 01; i <= hours; i++) {
                GetRoutineDataFromValidicCall task = new GetRoutineDataFromValidicCall(getActivity(), onGetRoutineFromValidicListener, false, UtilsCommon.getDateByHour(i), UtilsCommon.getDateByHour(i + 1), Constant.day);
                task.execute();
            }
        } else {
            //Toast.makeText(getActivity(), getActivity().getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    // success listener of GetRoutineDataFromValidicCall and bind the data
    @Override
    public void onSucceedToGetRoutineFromValidic(ValidicSummaryModel summary, ValidicRoutineModel data, String chartFlag) {
        try {

            HashMap<String, String> temp = new HashMap<>();

            String startDate = summary.getStart_date();
            String endDate = summary.getEnd_date();

            String sDate = startDate.substring(startDate.indexOf("T") + 1, startDate.indexOf("T") + 6);
            String eDate = endDate.substring(endDate.indexOf("T") + 1, endDate.indexOf("T") + 6);
            String time = sDate + "-" + eDate;
            temp.put(Constant.time, time);
            if (chartFlag.equalsIgnoreCase(Constant.day)) {

                if (summary.getResults() > 0) {
                    temp.put(Constant.distance, String.valueOf(Math.round(UtilsCommon.getMiles((float) data.getRoutine().get(0).getDistance()))));
                    total_distance = total_distance + UtilsCommon.getMiles((float) data.getRoutine().get(0).getDistance());
                } else {
                    temp.put(Constant.distance, "0");
                }
                lst_day_distance.add(temp);
                tv_total_miles.setText(String.valueOf(total_distance) + " KM");
                Debugger.debugE("Day_Calories..", total_distance + " " + lst_day_distance.size() + " " + lst_day_distance.toString());
            }

            if (lst_day_distance.size() == hours) {
                Debugger.debugE("inside_Calories..", lst_day_distance.size() + " " + lst_day_distance.toString());
                UtilsCommon.destroyProgressBar();
                dayWiseGraph(mChart, tv_distance, tv_date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener of GetRoutineDataFromValidicCall
    @Override
    public void onFaildToGetRoutineFromValidic(String error_msg) {

    }
}