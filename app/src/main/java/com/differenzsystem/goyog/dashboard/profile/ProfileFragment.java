package com.differenzsystem.goyog.dashboard.profile;

import android.annotation.TargetApi;
import android.classes.HorizontalListView;
import android.classes.NonScrollListView;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.adapter.ProfileBadgesImageAdapter;
import com.differenzsystem.goyog.adapter.ProfileGoalImageAdapter;
import com.differenzsystem.goyog.adapter.TimelineAdapter;
import com.differenzsystem.goyog.api.GetCompletedChallangeData;
import com.differenzsystem.goyog.api.UpdateChallengesStatusInBackground;
import com.differenzsystem.goyog.api.UpdateChallengesStatusInBackgroundForTracker;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.model.CompletedBadge;
import com.differenzsystem.goyog.model.CompletedChallenge;
import com.differenzsystem.goyog.model.LevelInformationModel;
import com.differenzsystem.goyog.model.TimeLine;
import com.differenzsystem.goyog.model.TimeLineResult;
import com.differenzsystem.goyog.model.completedchallbadges;
import com.differenzsystem.goyog.utility.BaseFragment;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class ProfileFragment extends BaseFragment implements OnClickListener, GetCompletedChallangeData.OnCompletedChallengesListener, SwipeRefreshLayout.OnRefreshListener {
    LinearLayout ll_back, ll_timeline, ll_tier, ll_point;
    TextView tv_title, tv_timeline, tv_level, tv_level_name, no_badges, no_challanges;
    TextView tv_goal, tv_badges, tv_point;
    ImageView iv_level_badge, infinity;

    HorizontalListView lst_goal, lst_badges;
    NonScrollListView lst_timeline;

    ArrayList<String> arlst_goal;
    ArrayList<Integer> arlst_badges;
    ArrayList<HashMap<String, String>> arlst_timeline;

    ProfileGoalImageAdapter goalImageAdapter;
    ProfileBadgesImageAdapter badgesImageAdapter;
    TimelineAdapter timelineAdapter;
    LevelInformationModel levelData;

    String[] skill = {"Lean Machine", "Shooting Star", "Happy Hill", "Rocket Boot", "DoGooder", "SkyDiver", "Rocket Boot"};
    String[] month = {"Aug", "Aug", "Jul", "Jul", "Jun", "Jun", "Jun"};
    int[] badges = {R.drawable.badge1, R.drawable.badge2, R.drawable.badge3, R.drawable.badge4, R.drawable.badge1, R.drawable.badge2, R.drawable.badge3};
    boolean isVisible = false;
    public static ProfileFragment mContext;

    completedchallbadges completedchallbadges;

    GetCompletedChallangeData.OnCompletedChallengesListener onCompletedChallengesListener;

    List<TimeLine> timeLines = new ArrayList<>();
    SwipeRefreshLayout swipe_view;

    //sync animation
    LinearLayout ll_sync_progress;
    ProgressBar pb_sync;
    TextView tv_syncstatus;
    ImageView img_sync;
    final Handler handler = new Handler();
    Runnable runnable;

//    private FirebaseAnalytics mFirebaseAnalytics;


    /**
     * Note
     * usertype 1 = live user
     * usertype 2 = demo user
     * connect_flag = connected_with_validic means get data from validic
     * connect_flag = connected_with_device means get data from google fit
     * connect_flag = connected_with_tracker means get data from band
     */

    // create the new instance of screen
    public static ProfileFragment newInstance(Bundle extra) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
    }

    public static ProfileFragment getInstance() {
        // return context of ProfileFragment
        return mContext;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            isVisible = true;
        else
            isVisible = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);

        initControl(view);
        initControlAction();
        if (Globals.completedchallbadges != null) {
            completedchallbadges = Globals.completedchallbadges;
            setAdapter();
        } else {
            if (ConnectionDetector.isConnectingToInternet(getActivity()))
                makeCall(true);
            else {
                //Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        }

        if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
            scheduleThread();
        } else {
            swipe_view.setEnabled(false);
            ll_tier.setVisibility(View.GONE);
            ll_point.setVisibility(View.GONE);
        }
        return view;
    }

    // sync animation
    public void scheduleThread() {
        handler.postDelayed(new Runnable() {
            public void run() {
                runnable = this;
                if (UtilsCommon.checkBlutoothConnectivity()) {

                    UtilsCommon.expand(ll_sync_progress);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 200ms
                            UtilsCommon.collapse(ll_sync_progress);
                        }
                    }, Constant.PostDelaySub);
                }
                handler.postDelayed(runnable, Constant.PostDelayMain);
            }
        }, Constant.PostDelayMain);
    }

    // GetCompletedChallangeData api call

    /**
     * @param showProgressDialog boolean for showing progress dialog
     */
    public void makeCall(boolean showProgressDialog) {
        JSONObject data = new JSONObject();
        try {
            data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
            GetCompletedChallangeData getCompletedChallangeData = new GetCompletedChallangeData(getActivity(), data, onCompletedChallengesListener, showProgressDialog);
            getCompletedChallangeData.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // initialize all controls define in xml layout
    public void initControl(View view) {
        try {
            mContext = this;
            onCompletedChallengesListener = this;

            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            ll_back.setVisibility(View.INVISIBLE);

            tv_goal = (TextView) view.findViewById(R.id.tv_goal);
            tv_badges = (TextView) view.findViewById(R.id.tv_badges);

            tv_point = (TextView) view.findViewById(R.id.tv_point);

            no_badges = (TextView) view.findViewById(R.id.no_badges);
            no_challanges = (TextView) view.findViewById(R.id.no_challanges);

            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_timeline = (TextView) view.findViewById(R.id.tv_timeline);
            tv_level = (TextView) view.findViewById(R.id.tv_level);
            tv_level_name = (TextView) view.findViewById(R.id.tv_level_name);

            iv_level_badge = (ImageView) view.findViewById(R.id.iv_level_badge);
            infinity = (ImageView) view.findViewById(R.id.infinity);

            swipe_view = (SwipeRefreshLayout) view.findViewById(R.id.swipe_view);

            ll_timeline = (LinearLayout) view.findViewById(R.id.ll_timeline);
            lst_goal = (HorizontalListView) view.findViewById(R.id.lst_goal);
            lst_badges = (HorizontalListView) view.findViewById(R.id.lst_badges);
            lst_timeline = (NonScrollListView) view.findViewById(R.id.lst_timeline);
            lst_timeline.setFocusable(false);

            arlst_badges = new ArrayList<Integer>();
            arlst_goal = new ArrayList<String>();
            arlst_timeline = new ArrayList<HashMap<String, String>>();
            levelData = HomeFragment.levelData;

            //sync animation
            ll_sync_progress = (LinearLayout) view.findViewById(R.id.ll_sync_progress);
            pb_sync = (ProgressBar) view.findViewById(R.id.pb_sync);
            tv_syncstatus = (TextView) view.findViewById(R.id.tv_syncstatus);
            img_sync = (ImageView) view.findViewById(R.id.img_sync);

            ll_sync_progress.setVisibility(View.GONE);
            ll_point = (LinearLayout) view.findViewById(R.id.ll_point);
            ll_tier = (LinearLayout) view.findViewById(R.id.ll_tier);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initControlAction() {
        try {
            //iv_timeline.setOnClickListener(this);
            ll_timeline.setOnClickListener(this);
            swipe_view.setOnRefreshListener(this);
            tv_title.setText(R.string.profile);

            if (levelData != null) {
                tv_level.setText(levelData.getLevel_subtitle());
                tv_level_name.setText(levelData.getLevel_name());
                Glide.with(getActivity())
                        .load(getActivity().getResources().getString(R.string.server_url)
                                + getActivity().getResources().getString(R.string.badges_image)
                                + levelData.getLevel_image())
                        .placeholder(R.drawable.app_placeholder)
                        .into(iv_level_badge);
            }

            for (int i = 0; i < skill.length; i++) {
                HashMap<String, String> temp = new HashMap<String, String>();
                temp.put(Constant.skill, skill[i]);
                temp.put(Constant.month, month[i]);

                arlst_timeline.add(temp);
                arlst_goal.add(skill[i]);
                arlst_badges.add(badges[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // load the adapter for challenges images
    public void setChallanges() {
        goalImageAdapter = new ProfileGoalImageAdapter(getActivity(), completedchallbadges);
        lst_goal.setAdapter(goalImageAdapter);
    }

    // load the adapter for badge image
    public void setBadges() {
        badgesImageAdapter = new ProfileBadgesImageAdapter(getActivity(), completedchallbadges);
        lst_badges.setAdapter(badgesImageAdapter);
    }

    // handle click event
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            /*case R.id.iv_timeline:
                if (ll_timeline.getVisibility() == View.GONE) {
                    ll_timeline.setVisibility(View.VISIBLE);
                    ll_timeline.requestFocus();
                    // getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                } else {
                    ll_timeline.clearFocus();
                    ll_timeline.setVisibility(View.GONE);
                }
                break;*/
        }
    }

    // success listener for GetCompletedChallangeData and bind all the data
    @Override
    public void onSucceedChallenges(completedchallbadges completedchallbadges) {
        if (completedchallbadges != null) {
            Log.e("data", String.valueOf(completedchallbadges));
            swipe_view.setRefreshing(false);
            this.completedchallbadges = completedchallbadges;
            Globals.completedchallbadges = completedchallbadges;
            setAdapter();
        }
    }

    // failed listener for GetCompletedChallangeData
    @Override
    public void onFailedChallenges(String message) {
        if (message != null && message.isEmpty()) {
//            Log.e("data error...", message);
            Toast.makeText(mContext.getActivity(), message, Toast.LENGTH_SHORT).show();
        }
        swipe_view.setRefreshing(false);
    }

    // initialize and set the adapter after getting response and display the list
    private void setAdapter() {
        if (completedchallbadges.getCompletedChallenges().size() <= 0) {
            no_challanges.setVisibility(View.VISIBLE);
            lst_goal.setVisibility(View.INVISIBLE);
        } else {
            no_challanges.setVisibility(View.GONE);
            lst_goal.setVisibility(View.VISIBLE);
            tv_goal.setText(String.valueOf(completedchallbadges.getCompletedChallenges().size()));
            if (completedchallbadges.getChallenges_point() > 0) {
                tv_point.setVisibility(View.VISIBLE);
                infinity.setVisibility(View.GONE);
                tv_point.setText(String.valueOf(completedchallbadges.getChallenges_point()));
            } else {
                tv_point.setVisibility(View.GONE);
                infinity.setVisibility(View.VISIBLE);
            }
            setChallanges();
        }
        if (completedchallbadges.getCompletedBadges().size() <= 0) {
            no_badges.setVisibility(View.VISIBLE);
            lst_badges.setVisibility(View.INVISIBLE);
        } else {
            no_badges.setVisibility(View.GONE);
            lst_badges.setVisibility(View.VISIBLE);
            tv_badges.setText(String.valueOf(completedchallbadges.getCompletedBadges().size()));
            setBadges();
        }
        prepareTimeLineData();
    }

    // load the list and set the response data into adapter
    public void prepareTimeLineData() {
        //putChallangeToTimeline(completedchallbadges.getCompletedChallenges());
        //putBadgesToTimeline(completedchallbadges.getCompletedBadges());
        putTimeLineData(completedchallbadges.getTimeLineResults());
        //Collections.sort(timeLines, new CustomComparator());
        timelineAdapter = new TimelineAdapter(getActivity(), timeLines);
        lst_timeline.setAdapter(timelineAdapter);
    }

    SimpleDateFormat month_m = new SimpleDateFormat("MMM");
    SimpleDateFormat month_date = new SimpleDateFormat("MMM d");

    // for displaying the details of complete challenges
    public void putTimeLineData(List<TimeLineResult> timeLineResults) {
        for (int i = 0; i < timeLineResults.size(); i++) {
            if (Integer.parseInt(timeLineResults.get(i).getBadge_id()) > 0) {
                timeLines.add(new TimeLine(timeLineResults.get(i).getComplete_date(),
                        timeLineResults.get(i).getLevel_name(),
                        "Level " + timeLineResults.get(i).getLevel_number(),
                        "badge",
                        month_m.format(stringToDate(timeLineResults.get(i).getComplete_date(), "yyyy-MM-dd HH:mm:ss")),
                        month_date.format(stringToDate(timeLineResults.get(i).getComplete_date(), "yyyy-MM-dd HH:mm:ss")),
                        timeLineResults.get(i).getLevel_image(),
                        ""));
            } else {
                timeLines.add(new TimeLine(timeLineResults.get(i).getComplete_date(),
                        timeLineResults.get(i).getOffer_title(),
                        timeLineResults.get(i).getLevel_name(),
                        "challange",
                        month_m.format(stringToDate(timeLineResults.get(i).getComplete_date(), "yyyy-MM-dd HH:mm:ss")),
                        month_date.format(stringToDate(timeLineResults.get(i).getComplete_date(), "yyyy-MM-dd HH:mm:ss")),
                        timeLineResults.get(i).getThumb_image(),
                        timeLineResults.get(i).getOffer_title()));
            }
        }
    }

    public void putChallangeToTimeline(List<CompletedChallenge> completedChallenges) {
        for (int i = 0; i < completedChallenges.size(); i++) {
            timeLines.add(new TimeLine(completedChallenges.get(i).getCompleted_date(),
                    completedChallenges.get(i).getSubtitle(),
                    completedChallenges.get(i).getName(),
                    "challange",
                    month_m.format(stringToDate(completedChallenges.get(i).getCompleted_date(), "yyyy-MM-dd HH:mm:ss")),
                    month_date.format(stringToDate(completedChallenges.get(i).getCompleted_date(), "yyyy-MM-dd HH:mm:ss")),
                    completedChallenges.get(i).getThumb_image(),
                    completedChallenges.get(i).getOffer_title()));
        }
    }

    public void putBadgesToTimeline(List<CompletedBadge> completedBadges) {
        for (int i = 0; i < completedBadges.size(); i++) {
            timeLines.add(new TimeLine(completedBadges.get(i).getComplete_date(),
                    completedBadges.get(i).getLevel_name(),
                    "Level " + completedBadges.get(i).getLevel_number(),
                    "badge",
                    month_m.format(stringToDate(completedBadges.get(i).getComplete_date(), "yyyy-MM-dd HH:mm:ss")),
                    month_date.format(stringToDate(completedBadges.get(i).getComplete_date(), "yyyy-MM-dd HH:mm:ss")),
                    completedBadges.get(i).getLevel_image(),
                    ""));
        }
    }

    // convert string to date
    private Date stringToDate(String aDate, String aFormat) {
        if (aDate == null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate, pos);
        return stringDate;
    }

    // swipe to refresh process and rebind the refresh data on screen
    @Override
    public void onRefresh() {
        completedchallbadges = null;
        Globals.completedchallbadges = null;
        timeLines = new ArrayList<>();
        swipe_view.setRefreshing(true);

        if (ConnectionDetector.isConnectingToInternet(getActivity()))
            makeCall(false);
        else {
            //   Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }

        UpdateChallengesStatus();
    }

    // updating challenge status
    private void UpdateChallengesStatus() {
        if ((UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device))) {
            HomeFragment.mClient.connect();
            UpdateChallengesStatusInBackground updateChallengesStatusInBackground = new UpdateChallengesStatusInBackground(getActivity(), HomeFragment.mClient);
            updateChallengesStatusInBackground.getData();
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            UpdateChallengesStatusInBackgroundForTracker updateChallengesStatusInBackgroundForTracker = new UpdateChallengesStatusInBackgroundForTracker(getActivity());
            updateChallengesStatusInBackgroundForTracker.getData();
            swipe_view.setRefreshing(false);
        }
    }

    public static class CustomComparator implements Comparator<TimeLine> {
        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public int compare(TimeLine lhs, TimeLine rhs) {
            try {
                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(lhs.getCompleted_date().toString())
                        .compareTo(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rhs.getCompleted_date().toString()));
            } catch (ParseException e) {
                return 0;
            }
        }
    }

    @Override
    public void onPause() {
        handler.removeCallbacks(runnable); // remove the handler
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Timeline);
    }

    private void recordScreenViews() {
        // [START set_current_screen]
//        mFirebaseAnalytics.setCurrentScreen(getActivity(), "Timeline Screen", null /* class override */);
        // [END set_current_screen]
    }
}

