package com.differenzsystem.goyog.dashboard.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.R;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class SuccessActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout ll_back;
    TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.success_activity);
        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private Context getContext() {
        return SuccessActivity.this;
    }

    // initialize all controls define in xml layout
    public void initializeControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(R.string.success);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click event listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;

            default:
                break;
        }
    }
}
