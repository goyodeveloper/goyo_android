package com.differenzsystem.goyog.dashboard.home.heartRate;

import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.utility.UtilsPermission;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class HeartRateFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_back, ll_main;
    TextView tv_title;
    RelativeLayout ll_no_permission;
    Button btn_allow_access;
    View rootview;

    ViewPager viewPager;
    CirclePageIndicator indicator;
    HeartRateDayWiseFragment heartRateDayWiseFragment;
    HeartRateWeekWiseFragment heartRateWeekWiseFragment;
    HeartRateMonthWiseFragment heartRateMonthWiseFragment;

    // create the new instance of HeartRateFragment
    public static HeartRateFragment newInstance(Bundle extra) {
        HeartRateFragment fragment = new HeartRateFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.heartrate_fragment, container, false);

        initializeControls(view);
        initializeControlsAction();
        return view;
    }

    // initialize all controls define in xml layout

    /**
     *
     * @param view layout view
     */
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    public void initializeControls(View view) {
        try {
            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            ll_main = (LinearLayout) view.findViewById(R.id.ll_main);
            ll_no_permission = (RelativeLayout) view.findViewById(R.id.ll_no_permission);
            btn_allow_access = (Button) view.findViewById(R.id.btn_allow_access);
            tv_title.setText(getString(R.string.heart_rate_lbl));

            heartRateDayWiseFragment = HeartRateDayWiseFragment.newInstance(null);
            heartRateWeekWiseFragment = HeartRateWeekWiseFragment.newInstance(null);
            heartRateMonthWiseFragment = HeartRateMonthWiseFragment.newInstance(null);

            viewPager = (ViewPager) view.findViewById(R.id.viewPager);
            indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
            indicator.setStrokeWidth(2);
            indicator.setStrokeColor(getActivity().getResources().getColor(R.color.pager_selected));

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {

                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                    ll_no_permission.setVisibility(View.VISIBLE);
                    ll_main.setVisibility(View.GONE);
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    ll_no_permission.setVisibility(View.GONE);
                    ll_main.setVisibility(View.VISIBLE);
                    viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));
                    indicator.setViewPager(viewPager);
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                    ll_no_permission.setVisibility(View.GONE);
                    ll_main.setVisibility(View.VISIBLE);
                    viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));
                    indicator.setViewPager(viewPager);
                }
                indicator.setVisibility(View.GONE);
            } else {
                ll_no_permission.setVisibility(View.GONE);
                ll_main.setVisibility(View.VISIBLE);
                viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));
                indicator.setViewPager(viewPager);
            }
            indicator.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // require permission BODY_SENSORS
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    public void checkForPermission() {
        boolean flag = UtilsPermission.checkBodySensor(getActivity());

       /* if (flag) {
            ll_main.setVisibility(View.VISIBLE);
            ll_no_permission.setVisibility(View.GONE);
            viewPager.setAdapter(new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager()));
            indicator.setViewPager(viewPager);
        } else {*/
        ll_no_permission.setVisibility(View.VISIBLE);
        ll_main.setVisibility(View.GONE);
           /* Toast.makeText(getActivity(), getActivity().getString(R.string.msg_bodysensor_permission), Toast.LENGTH_SHORT).show();
        }*/
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
            btn_allow_access.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click listener
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;
            case R.id.btn_allow_access:
                checkForPermission();
                break;

            default:
                break;
        }
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return heartRateDayWiseFragment;
                /*case 1:
                    return heartRateWeekWiseFragment;
                case 2:
                    return heartRateMonthWiseFragment;*/
            }
            return null;
        }

        @Override
        public int getCount() {
            return 1;
        }
    }

    // disconnect GoogleApiClient
    @Override
    public void onStop() {
        super.onStop();
        if (HomeFragment.mClient != null && HomeFragment.mClient.isConnected()) {
            HomeFragment.mClient.stopAutoManage(getActivity());
            HomeFragment.mClient.disconnect();
        }
    }
}
