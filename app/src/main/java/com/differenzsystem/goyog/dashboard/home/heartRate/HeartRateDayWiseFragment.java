package com.differenzsystem.goyog.dashboard.home.heartRate;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.syncdevice;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.GetFitnessDataFromValidicCall;
import com.differenzsystem.goyog.api.GetFitnessDataFromValidicCall.OnGetFitnessFromValidicListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.CategoryModel;
import com.differenzsystem.goyog.model.ValidicFitnessModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.SpriteFactory;
import com.github.ybq.android.spinkit.Style;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.veryfit.multi.nativedatabase.HealthHeartRateItem;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.differenzsystem.goyog.dashboard.home.HomeFragment.mClient;
import static java.text.DateFormat.getTimeInstance;

public class HeartRateDayWiseFragment extends Fragment implements OnGetFitnessFromValidicListener, SwipeRefreshLayout.OnRefreshListener, syncdevice.Onsyncdevice {

    View rootview;
    CombinedChart mChart;
    TextView tv_heart_rate, tv_date, tv_total_heart_rate;
    ArrayList<HashMap<String, String>> lst_day_heart_rate;
    OnGetFitnessFromValidicListener onGetFitnessFromValidicListener;
    int total_heart_rate = 0;
    int hours;
    String TAG = "HeartRateDayWiseFragment";
    SwipeRefreshLayout swipeRefreshLayout;
    syncdevice.Onsyncdevice onsyncdevice;
    int count = 0;

    public HeartRateDayWiseFragment() {
        // Required empty public constructor
    }

    // create the new instance of HeartRateDayWiseFragment
    public static HeartRateDayWiseFragment newInstance(Bundle bundle) {
        HeartRateDayWiseFragment fragment = new HeartRateDayWiseFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        onsyncdevice = this;
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.heart_rate_day_wise_fragment, container, false);
            initcomopnets(rootview);
        } else
            container.removeView(rootview);

        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_HeartRate);
    }

    // initialize all controls define in xml layout

    /**
     * @param rootview layout view
     */
    private void initcomopnets(View rootview) {
        try {

            swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.swipe_view);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(
                    R.color.colorPrimary);
            mChart = (CombinedChart) rootview.findViewById(R.id.chart_calories);

            lst_day_heart_rate = new ArrayList<>();

            tv_heart_rate = (TextView) rootview.findViewById(R.id.tv_avg_heart_rate);
            tv_date = (TextView) rootview.findViewById(R.id.tv_date);
            tv_total_heart_rate = (TextView) rootview.findViewById(R.id.tv_total_heart_rate);

            hours = new Time(System.currentTimeMillis()).getHours();

            onGetFitnessFromValidicListener = this;

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                //if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                //    getDaywiseData(true);
                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    if (BleSharedPreferences.getInstance().getIsBind()) {
                        getDayData();
                    } else {
                        swipeRefreshLayout.setEnabled(false);
                        setupDemoUserDay();
                    }
                } else {
                    //TODO:get Date from device
                    getDeviceHeartRateData();
                }
            } else {
                swipeRefreshLayout.setEnabled(false);
                setupDemoUserDay();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // setup data for demo user
    private void setupDemoUserDay() {
        hours = new Time(System.currentTimeMillis()).getHours() + 1;

        for (int i = 0; i < 24; i++) {
            HashMap<String, String> temp = new HashMap<>();
            temp.put(Constant.time, new Date().toString());
            int sum = 0;
            if (i < hours) {
                temp.put(Constant.heart_rate, "0");
                lst_day_heart_rate.add(temp);
            } else {
                break;
            }
        }
        tv_total_heart_rate.setText("0 BPM");

        UtilsCommon.destroyProgressBar();
        dayWiseGraph(mChart, tv_heart_rate, tv_date);

    }

    // get data from band
    public void getDayData() {
        Calendar mCalendar = Calendar.getInstance();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
        hours = new Time(System.currentTimeMillis()).getHours() + 1;

        int min = (int) Math.round(hours * 60) + new Time(System.currentTimeMillis()).getMinutes();

        List<HealthHeartRateItem> dateHealthHeartRateItem = ProtocolUtils.getInstance().getHeartRateItems(new Date(year, month, day));
        Date startDate;
        // Date startDate = dateHealthHeartRateItem.get(0).getDate();
        int sum = 0, counter = 0, heartrate = 0, avg = 0, totalheartrate = 0, totalMin = 60;
        ;
        ArrayList<Integer> hoursArray = new ArrayList<Integer>();
        ArrayList<Integer> temphours = new ArrayList<Integer>();

        if (dateHealthHeartRateItem != null && dateHealthHeartRateItem.size() > 0) {
            startDate = dateHealthHeartRateItem.get(0).getDate();

            for (int i = 0; i < dateHealthHeartRateItem.size(); i++) {
                heartrate = heartrate + dateHealthHeartRateItem.get(i).getHeartRaveValue();
                counter = counter + dateHealthHeartRateItem.get(i).getOffsetMinute();

                if (counter <= totalMin) {
                    temphours.add(dateHealthHeartRateItem.get(i).getHeartRaveValue());

                    if (i + 1 == dateHealthHeartRateItem.size()) {
                        sum = 0;
                        for (int k = 0; k < temphours.size(); k++) {
                            sum += temphours.get(k);
                        }
                        hoursArray.add(sum / temphours.size());
                        totalMin = 60 + (60 * (hoursArray.size()));
                    }
                } else {
                    if (temphours.size() > 0) {
                        sum = 0;
                        for (int k = 0; k < temphours.size(); k++) {
                            sum += temphours.get(k);
                        }
                        hoursArray.add(sum / temphours.size());
                        totalMin = 60 + (60 * (hoursArray.size()));
                        temphours.clear();

                        if (counter <= totalMin) {
                            temphours.add(dateHealthHeartRateItem.get(i).getHeartRaveValue());
                        } else {
                            int ii = 1;
                            while (ii != 0) {
                                if ((ii * 60) > counter) {
                                    int updateIndex = ii - 1;
                                    int len;
                                    if (hoursArray.size() > 0)
                                        len = hoursArray.size();
                                    else
                                        len = 0;

                                    for (int j = len; j < updateIndex; j++) {
                                        hoursArray.add(0);
                                    }
                                    hoursArray.add(dateHealthHeartRateItem.get(i).getHeartRaveValue());
                                    totalMin = 60 + (60 * (hoursArray.size()));
                                    temphours.clear();
                                    ii = 0;
                                } else {
                                    ii = ii + 1;
                                }
                            }
                        }
                    } else {
                        int ii = 1;
                        while (ii != 0) {
                            if ((ii * 60) > counter) {
                                int updateIndex = ii - 1;
                                int len;
                                if (hoursArray.size() > 0)
                                    len = hoursArray.size();
                                else
                                    len = 0;

                                for (int j = len; j < updateIndex; j++) {
                                    hoursArray.add(0);
                                }
                                hoursArray.add(dateHealthHeartRateItem.get(i).getHeartRaveValue());
                                totalMin = 60 + (60 * (hoursArray.size()));
                                temphours.clear();
                                ii = 0;
                            } else {
                                ii = ii + 1;
                            }
                        }
                    }
                }

                if (dateHealthHeartRateItem.get(i).getHeartRaveValue() > 0) {
                    count = count + 1;
                }
                totalheartrate = totalheartrate + dateHealthHeartRateItem.get(i).getHeartRaveValue();
            }
            Log.e("offset Minutes =>", String.valueOf(sum));
            Log.e("Current Minutes =>", String.valueOf(min));
            Log.e("Hours Array =>", hoursArray.toString());
            Log.e("Total heartrate =>", String.valueOf(totalheartrate / dateHealthHeartRateItem.size()));
        } else {
            startDate = new Date();
            for (int i = 0; i < hours; i++) {
                hoursArray.add(0);
            }
        }

        if (hoursArray.size() < hours) {
            int diff = hours - hoursArray.size();

            for (int i = 0; i < diff; i++) {
                hoursArray.add(0);
            }
        }

        for (int i = 0; i < hoursArray.size(); i++) {
            HashMap<String, String> temp = new HashMap<>();
            temp.put(Constant.time, UtilsCommon.getDateDayName(startDate.toString()));
            if (i < hours) {
                temp.put(Constant.heart_rate, String.valueOf(hoursArray.get(i)));
                lst_day_heart_rate.add(temp);
            } else {
                break;
            }
        }
        if (dateHealthHeartRateItem != null && dateHealthHeartRateItem.size() > 0)
            total_heart_rate = totalheartrate / count;
        else
            total_heart_rate = 0;

        tv_total_heart_rate.setText(String.valueOf(total_heart_rate) + " BPM");
        Debugger.debugE("Day_steps..", hours + " " + lst_day_heart_rate.size() + " " + lst_day_heart_rate.toString());

        if (lst_day_heart_rate.size() == hours) {

            Debugger.debugE("inside_heart_rate..", lst_day_heart_rate.size() + " " + lst_day_heart_rate.toString());
            UtilsCommon.destroyProgressBar();
            dayWiseGraph(mChart, tv_heart_rate, tv_date);
        }
    }

//    public void getDayData() {
//        Calendar mCalendar = Calendar.getInstance();
//        int year = mCalendar.get(Calendar.YEAR);
//        int month = mCalendar.get(Calendar.MONTH);
//        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
//        hours = new Time(System.currentTimeMillis()).getHours() + 1;
//
//        int min = (int) Math.round(hours * 60) + new Time(System.currentTimeMillis()).getMinutes();
//
//        List<HealthHeartRateItem> dateHealthHeartRateItem = ProtocolUtils.getInstance().getHeartRateItems(new Date(year, month, day));
//        Date startDate;
//        // Date startDate = dateHealthHeartRateItem.get(0).getDate();
//        int sum = 0, counter = 0, heartrate = 0, avg = 0, totalheartrate = 0, totalMin = 60;
//        ;
//        ArrayList<Integer> hoursArray = new ArrayList<Integer>();
//        ArrayList<Integer> temphours = new ArrayList<Integer>();
//
//        if (dateHealthHeartRateItem != null && dateHealthHeartRateItem.size() > 0) {
//            startDate = dateHealthHeartRateItem.get(0).getDate();
//
//            for (int i = 0; i < dateHealthHeartRateItem.size(); i++) {
//                heartrate = heartrate + dateHealthHeartRateItem.get(i).getHeartRaveValue();
//                counter = counter + dateHealthHeartRateItem.get(i).getOffsetMinute();
//
//                if (counter <= totalMin) {
//                    temphours.add(dateHealthHeartRateItem.get(i).getHeartRaveValue());
//
//                    if (i + 1 == dateHealthHeartRateItem.size()) {
//                        sum = 0;
//                        for (int k = 0; k < temphours.size(); k++) {
//                            sum += temphours.get(k);
//                        }
//                        hoursArray.add(sum / temphours.size());
//                        totalMin = 60 + (60 * (hoursArray.size() - 1));
//                    }
//                } else {
//                    if (temphours.size() > 0) {
//                        sum = 0;
//                        for (int k = 0; k < temphours.size(); k++) {
//                            sum += temphours.get(k);
//                        }
//                        hoursArray.add(sum / temphours.size());
//                        totalMin = 60 + (60 * (hoursArray.size() - 1));
//                        temphours.clear();
//
//                        if (counter <= totalMin) {
//                            temphours.add(dateHealthHeartRateItem.get(i).getHeartRaveValue());
//                        } else {
//                            int offeset = dateHealthHeartRateItem.get(i).getOffsetMinute();
//                            int offsetcount = offeset / 60;
//                            if (offsetcount > 1) {
//                                for (int j = 1; j <= offsetcount; j++) {
//                                    hoursArray.add(0);
//                                }
//                            }
//                            hoursArray.add(dateHealthHeartRateItem.get(i).getHeartRaveValue());
//                            totalMin = 60 + (60 * (hoursArray.size() - 1));
//                            temphours.clear();
//                        }
//                    } else {
//                        int offeset = dateHealthHeartRateItem.get(i).getOffsetMinute();
//                        int offsetcount = offeset / 60;
//                        if (offsetcount > 1) {
//                            for (int j = 1; j <= offsetcount; j++) {
//                                hoursArray.add(0);
//                            }
//                        }
//                        hoursArray.add(dateHealthHeartRateItem.get(i).getHeartRaveValue());
//                        totalMin = 60 + (60 * (hoursArray.size() - 1));
//                        temphours.clear();
//                    }
//                }
//
//                if (dateHealthHeartRateItem.get(i).getHeartRaveValue() > 0) {
//                    count = count + 1;
//                }
//                totalheartrate = totalheartrate + dateHealthHeartRateItem.get(i).getHeartRaveValue();
//            }
//            Log.e("offset Minutes =>", String.valueOf(sum));
//            Log.e("Current Minutes =>", String.valueOf(min));
//            Log.e("Hours Array =>", hoursArray.toString());
//            Log.e("Total heartrate =>", String.valueOf(totalheartrate / dateHealthHeartRateItem.size()));
//        } else {
//            startDate = new Date();
//            for (int i = 0; i < hours; i++) {
//                hoursArray.add(0);
//            }
//        }
//
//        if (hoursArray.size() < hours) {
//            int diff = hours - hoursArray.size();
//
//            for (int i = 0; i < diff; i++) {
//                hoursArray.add(0);
//            }
//        }
//
//        for (int i = 0; i < hoursArray.size(); i++) {
//            HashMap<String, String> temp = new HashMap<>();
//            temp.put(Constant.time, UtilsCommon.getDateDayName(startDate.toString()));
//            if (i < hours) {
//                temp.put(Constant.heart_rate, String.valueOf(hoursArray.get(i)));
//                lst_day_heart_rate.add(temp);
//            } else {
//                break;
//            }
//        }
//        if (dateHealthHeartRateItem != null && dateHealthHeartRateItem.size() > 0)
//            total_heart_rate = totalheartrate / count;
//        else
//            total_heart_rate = 0;
//
//        tv_total_heart_rate.setText(String.valueOf(total_heart_rate) + " BPM");
//        Debugger.debugE("Day_steps..", hours + " " + lst_day_heart_rate.size() + " " + lst_day_heart_rate.toString());
//
//        if (lst_day_heart_rate.size() == hours) {
//
//            Debugger.debugE("inside_heart_rate..", lst_day_heart_rate.size() + " " + lst_day_heart_rate.toString());
//            UtilsCommon.destroyProgressBar();
//            dayWiseGraph(mChart, tv_heart_rate, tv_date);
//        }
//
//    }

    public void getDeviceHeartRateData() {
        try {
            if (ConnectionDetector.internetCheck(getContext())) {
                buildFitnessClient();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // HeartRateCall
    private void buildFitnessClient() {
        if (mClient != null) {
            UtilsCommon.showProgressDialog(getActivity());
            new HeartRateCall().execute();
        }
    }

    // swipe to refresh call and rebind the data
    @Override
    public void onRefresh() {
        total_heart_rate = 0;
        lst_day_heart_rate = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        getAndSetData();

    }

    // HeartRateCall
    private class HeartRateCall extends AsyncTask<Void, Void, Void> {
        String heartRateData;

        protected Void doInBackground(Void... params) {
            try {
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                heartRateData = readData(dataReadResult);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                UtilsCommon.destroyProgressBar();
                int count = 0;
                for (int i = 0; i < lst_day_heart_rate.size(); i++) {
                    if (!lst_day_heart_rate.get(i).get(Constant.heart_rate).equalsIgnoreCase("0")) {
                        count++;
                    }
                }
                tv_total_heart_rate.setText(total_heart_rate / (count == 0 ? 1 : count) + " BPM");
                dayWiseGraph(mChart, tv_heart_rate, tv_date);
                Debugger.debugE("heartRateData", heartRateData + "");
                //tv_heartRateData.setText(heartRateData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // request data from google fit
    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            Calendar startCalendar = Calendar.getInstance();
            long startTime = startCalendar.getTimeInMillis();

            SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
            String startTimeString = formater.format(startTime) + " 12:01 AM";
            Date startDate = null;
            try {
                startDate = formater.parse(startTimeString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long startMillisecond = startDate.getTime();

            Calendar endCalendar = Calendar.getInstance();
            Date endDate = new Date();
            endCalendar.setTime(endDate);
            long endMillisecond = endCalendar.getTimeInMillis();

            Debugger.debugE("Start time", startDate + "=>");
            Debugger.debugE("End time", endDate + "=>");

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.HOURS)
                    .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    // read specific data by challenge type
    public String readData(DataReadResult dataReadResult) {
        String value = "";
        total_heart_rate = 0;
        try {
            if (dataReadResult.getBuckets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getBuckets().size());
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                        total_heart_rate += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                        if (value.equalsIgnoreCase("")) {
                            HashMap<String, String> temp = new HashMap<>();
                            temp.put(Constant.time, "");
                            temp.put(Constant.heart_rate, "0");
                            lst_day_heart_rate.add(temp);
                        }
                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                    total_heart_rate += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                    if (value.equalsIgnoreCase("")) {
                        HashMap<String, String> temp = new HashMap<>();
                        temp.put(Constant.time, "");
                        temp.put(Constant.heart_rate, "0");
                        lst_day_heart_rate.add(temp);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }

                HashMap<String, String> temp = new HashMap<>();
                String sDate = new SimpleDateFormat("hh:mm").format(dp.getStartTime(TimeUnit.MILLISECONDS));
                String eDate = new SimpleDateFormat("hh:mm").format(dp.getEndTime(TimeUnit.MILLISECONDS));
                String time = sDate + "-" + eDate;
                temp.put(Constant.time, time);
                temp.put(Constant.heart_rate, value);

                lst_day_heart_rate.add(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    // success listener of syncdevice
    @Override
    public void onSucceedsyncdevice() {
        swipeRefreshLayout.setRefreshing(false);
        getDayData();
    }

   /* @Override
    public void onFailedsyncdevice() {

    }*/

    // here get heart rate data from band
    private void getAndSetData() {
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            getDaywiseData(false);
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            syncdevice s = new syncdevice(getContext(), onsyncdevice);
            s.syncdata();
        } else {
            new HeartRateCall().execute();
        }
    }

    /**
     * @param mChart        get the chart for displaying HeartRate progress
     * @param tv_heart_rate get textview of HeartRate
     * @param tv_date       get textview of date
     */
    private void dayWiseGraph(CombinedChart mChart, final TextView tv_heart_rate, final TextView tv_date) {
        swipeRefreshLayout.setRefreshing(false);
        try {
            mChart.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setHighlightPerDragEnabled(false);
            mChart.setPinchZoom(false);
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.setDescription("");
            mChart.setExtraOffsets(30, 30, 30, 30);
            mChart.setTouchEnabled(true);
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setPinchZoom(false);

            Legend l = mChart.getLegend();
            l.setEnabled(false);

            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<CategoryModel> dataList = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            for (int i = 0; i < 24; i++) {
                if (i < lst_day_heart_rate.size()) {
                    HashMap<String, String> temp = lst_day_heart_rate.get(i);
                    double cal = Double.parseDouble(temp.get(Constant.heart_rate));
                    entries.add(new Entry(i, (float) cal));
                    dataList.add(new CategoryModel((float) cal, UtilsCommon.dayArray[i]));
                    labels.add(UtilsCommon.dayArray[i]);
                    if (i == 0) {
                        tv_heart_rate.setText(temp.get(Constant.heart_rate) + " bpm");
                        tv_date.setText(UtilsCommon.dayArray[i]);
                    }
                } else {
                    entries.add(new Entry(i, (float) -1));
                    dataList.add(new CategoryModel((float) -1, UtilsCommon.dayArray[i]));
                    labels.add(UtilsCommon.dayArray[i]);
                    if (i == 0) {
                        tv_heart_rate.setText(0 + " bpm");
                        tv_date.setText(UtilsCommon.dayArray[i]);
                    }
                }
            }
            LineDataSet dataset = new LineDataSet(entries, "# of Calls");

            CombinedData cData = new CombinedData();
            LineData data = new LineData();

            dataset.setColors(new int[]{getActivity().getResources().getColor(R.color.app_light_green)});
            dataset.setValueTextColor(getActivity().getResources().getColor(R.color.app_light_green));
            dataset.setCircleColor(getActivity().getResources().getColor(R.color.line_chart_circle));
            dataset.setCircleColorHole(getActivity().getResources().getColor(R.color.line_chart_circle_hole));
            dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataset.setDrawFilled(false);
            dataset.setCircleSize(10);
            dataset.setDrawValues(false);
            dataset.setDrawHighlightIndicators(false);
            dataset.setLineWidth((float) 2.5);

            SpinKitView spinKitView = new SpinKitView(getActivity());
            Style style = Style.values()[4];
            Sprite drawable = SpriteFactory.create(style);
            spinKitView.setIndeterminateDrawable(drawable);
            dataset.setSpinKitView(spinKitView);

            data.addDataSet(dataset);
            cData.setData(data);
            mChart.setData(cData);
            mChart.animateX(2500);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setTextSize(0f);
            xAxis.setTextColor(Color.TRANSPARENT);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);
            xAxis.setAxisMaximum(data.getXMax() + 0.25f);
            xAxis.setPosition(XAxisPosition.BOTH_SIDED);
            xAxis.setAxisMinimum(0f);
            xAxis.setGranularity(1f);

            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setDrawGridLines(false);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setTextSize(0f);
            leftAxis.setTextColor(Color.TRANSPARENT);
            leftAxis.setDrawLimitLinesBehindData(true);
            leftAxis.setDrawGridLines(false);

            mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    CategoryModel object = dataList.get((int) e.getX());
                    tv_heart_rate.setText((int) object.item1 + " bpm");
                    tv_date.setText(object.item2);

                }

                @Override
                public void onNothingSelected() {

                }
            });
            mChart.getAxisRight().setEnabled(false);
            mChart.invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // GetFitnessDataFromValidicCall api call

    /**
     * @param isDialog boolean for showing progressbar
     */
    public void getDaywiseData(boolean isDialog) {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            if (isDialog)
                UtilsCommon.showProgressDialog(getActivity());
            for (int i = 01; i <= hours; i++) {
                GetFitnessDataFromValidicCall task = new GetFitnessDataFromValidicCall(getActivity(), onGetFitnessFromValidicListener, false, UtilsCommon.getDateByHour(i), UtilsCommon.getDateByHour(i + 1), Constant.day);
                task.execute();
            }
        } else {
            //Toast.makeText(getActivity(), getActivity().getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    // success listener of GetFitnessDataFromValidicCall and bind the data
    @Override
    public void onSucceedToGetFitnessFromValidic(ValidicSummaryModel summary, ValidicFitnessModel data, String chartFlag) {
        try {

            HashMap<String, String> temp = new HashMap<>();

            String startDate = summary.getStart_date();
            String endDate = summary.getEnd_date();

            String sDate = startDate.substring(startDate.indexOf("T") + 1, startDate.indexOf("T") + 6);
            String eDate = endDate.substring(endDate.indexOf("T") + 1, endDate.indexOf("T") + 6);
            String time = sDate + "-" + eDate;
            temp.put(Constant.time, time);
            if (chartFlag.equalsIgnoreCase(Constant.day)) {

                if (summary.getResults() > 0) {
                    temp.put(Constant.heart_rate, String.valueOf(data.getFitness().get(0).getAverage_heart_rate()));
                    total_heart_rate = total_heart_rate + data.getFitness().get(0).getAverage_heart_rate();
                } else {
                    temp.put(Constant.heart_rate, "0");
                }
                lst_day_heart_rate.add(temp);
                tv_total_heart_rate.setText(String.valueOf(total_heart_rate) + " BPM");
                Debugger.debugE("Day_heart_rate..", hours + " " + lst_day_heart_rate.size() + " " + lst_day_heart_rate.toString());

            }

            if (lst_day_heart_rate.size() == hours) {

                Debugger.debugE("inside_heart_rate..", lst_day_heart_rate.size() + " " + lst_day_heart_rate.toString());
                UtilsCommon.destroyProgressBar();
                dayWiseGraph(mChart, tv_heart_rate, tv_date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener of GetFitnessDataFromValidicCall
    @Override
    public void onFaildToGetFitnessFromValidic(String error_msg) {

    }
}