package com.differenzsystem.goyog.dashboard;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.R;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

public class PopupChallanges extends AppCompatActivity implements View.OnClickListener{
    TextView tv_challengeper, tv_challengedesc, tv_challengedesc1,tv_title;
    ImageView iv_challengeimg;
    LinearLayout ll_back;
    Button btn_gotit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_challanges_activity);
        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
    }

    private Context getContext() {
        // return the PopupChallanges context
        return PopupChallanges.this;
    }

    // initialize all controls define in xml layout
    void initializeControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText("");

            btn_gotit = (Button) findViewById(R.id.btn_gotit);

            tv_challengeper = (TextView) findViewById(R.id.tv_challengeper);
            tv_challengedesc = (TextView) findViewById(R.id.tv_challengedesc);
            tv_challengedesc1 = (TextView) findViewById(R.id.tv_challengedesc1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // set click listener
    public void initializeControlsAction() {
        try {
            btn_gotit.setOnClickListener(this);
            ll_back.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event listener
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.btn_gotit:
                break;
        }
    }

    // on back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
