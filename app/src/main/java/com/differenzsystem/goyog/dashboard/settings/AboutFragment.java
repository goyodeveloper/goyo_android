package com.differenzsystem.goyog.dashboard.settings;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;

public class AboutFragment extends Fragment implements OnClickListener {
    TextView tv_title, tv_version;
    LinearLayout ll_back;
    ImageView iv_goyo_share, iv_fb_share, iv_youtube_share;

    // create the new instance of screen
    public static AboutFragment newInstance(Bundle extra) {
        AboutFragment fragment = new AboutFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.about_fragment, container, false);
        init(view);
        initAction();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_About);
    }

    // initialize all controls define in xml layout
    public void init(View view) {
        try {
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_version = (TextView) view.findViewById(R.id.tv_version);
            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            tv_title.setText(R.string.about);
            PackageManager manager = getActivity().getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    getActivity().getPackageName(), 0);
            String Currentversion = info.versionName;
            tv_version.setText(getString(R.string.version_text) + " " + Currentversion);

            iv_goyo_share = (ImageView) view.findViewById(R.id.iv_goyo_share);
            iv_fb_share = (ImageView) view.findViewById(R.id.iv_fb_share);
            iv_youtube_share = (ImageView) view.findViewById(R.id.iv_youtube_share);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // set click listener
    public void initAction() {
        try {
            ll_back.setOnClickListener(this);

            iv_goyo_share.setOnClickListener(this);
            iv_fb_share.setOnClickListener(this);
            iv_youtube_share.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event listener
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;
            case R.id.iv_goyo_share:
                buyWatch("http://www.goyo.lk");
                break;
            case R.id.iv_fb_share:
                Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                try {
                    String facebookUrl = getFacebookPageURL();
                    facebookIntent.setData(Uri.parse(facebookUrl));
                    startActivity(facebookIntent);
                } catch (Exception e) {
                    String facebookUrl = "https://www.facebook.com/GOYOAPP";
                    facebookIntent.setData(Uri.parse(facebookUrl));
                    startActivity(facebookIntent);
                }
                break;
            case R.id.iv_youtube_share:
                buyWatch("https://www.youtube.com/channel/UCNB-nPGE7sEz4IcVAtFDfWA");
                break;
        }
    }

    //method to get the right URL to use in the intent
    public String getFacebookPageURL() {
        String FACEBOOK_URL = "https://www.facebook.com/GOYOAPP";
        String FACEBOOK_PAGE_ID = "GOYOAPP";

        PackageManager packageManager = getActivity().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    // intent to url for buy band
    public void buyWatch(String address) {
        String url = address;//"http://www.goyo.lk/"

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    // open required screen

    /**
     * @param fragment add fragment
     */
    public void addFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame_main, fragment, null);
        ft.commit();
    }
}
