package com.differenzsystem.goyog.dashboard;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.FitnessTrackerOperations;
import com.differenzsystem.goyog.api.GetGoogleFits;
import com.differenzsystem.goyog.api.GetPopupStatus;
import com.differenzsystem.goyog.api.UpdatePopupStatus;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.challenges.ChallengesAndOffersFragment;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.dashboard.leaderboard.LeaderboardFragment;
import com.differenzsystem.goyog.dashboard.notification.NotificationFragment;
import com.differenzsystem.goyog.dashboard.profile.ProfileFragment;
import com.differenzsystem.goyog.model.ChallangesPopupModel;
import com.differenzsystem.goyog.model.ChartDataModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener, GetPopupStatus.OnGetPopupStatusListener, GetGoogleFits.OnGetFitsListener, UpdatePopupStatus.OnUpdatePopupStatusListener, FitnessTrackerOperations.FitnessTrackerOperationslistener {
    public static ImageView iv_home, iv_challenges, iv_leaderboard, iv_profile, iv_settings;
    boolean doubleBackToExitPressedOnce = false;
    Bundle extra = null;
    public static DashboardActivity mContext;
    public static int mCurrentweek;
    GetPopupStatus.OnGetPopupStatusListener GetPopupStatusListener;
    UpdatePopupStatus.OnUpdatePopupStatusListener UpdatePopupStatusListener;
    ArrayList<ChallangesPopupModel> inProcessList;
    int cnt = 0;
    int maxCnt = 0;
    int steps = 0, cals = 0, dis = 0, hearts = 0, sleep = 0;
    int type, chllng_val = 0, sec_chllng_val = 0, third_chllng_val, fourth_chllng_val;
    GetGoogleFits getGoogleFits;
    GetGoogleFits.OnGetFitsListener onGetFitsListener;
    int levelPer = 0, popStatus;
    TextView tv_badge_challanges, tv_badge_profile, tv_badge_notification;
    private Handler mHandler = new Handler();
    JSONObject notification;
    Timer myTimer, bubbleTimer;
    final Handler handler1 = new Handler();
    Runnable runnable;
    int level1, level2, level3, level4, level5;
    String NotificationType;
    FitnessTrackerOperations.FitnessTrackerOperationslistener fitnessTrackerOperationslistener;
    String userType;
    int perText;
    String alert;
    Dialog alertDialogMain;

    /**
     * Note
     * usertype 1 = live user
     * usertype 2 = demo user
     * connect_flag = connected_with_validic means get data from validic
     * connect_flag = connected_with_device means get data from google fit
     * connect_flag = connected_with_tracker means get data from band
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_activity);
        getSupportActionBar().hide();

        initializeControls();
        initializeControlsAction();
        init();

        userType = UtilsPreferences.getString(getApplicationContext(), Constant.user_type);
        if (userType != null && userType.length() > 0) {
            if (UtilsPreferences.getString(getApplicationContext(), Constant.user_type).equalsIgnoreCase("1")) { // condition for live user
                ChekforPopup();
                CheckforBubbles();
            }
        }
        // scheduleThread();
    }

    public void CheckforBubbles() {
        bubbleTimer = new Timer();
        bubbleTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (BleSharedPreferences.getInstance().getIsBind()) {
                            if (ProtocolUtils.getInstance().isAvailable() != ProtocolUtils.SUCCESS)
                                ProtocolUtils.getInstance().setCanConnect(true);
                        }
                        setBadge();
                    }
                });
            }
        }, 0, 5000);
    }

    public void ChekforPopup() {
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                if (userType != null && userType.length() > 0) {
                    String connect_flag = UtilsPreferences.getString(getApplicationContext(), Constant.connect_flag);
                    if (connect_flag != null && connect_flag.length() > 0) {
                        if (UtilsPreferences.getString(getApplicationContext(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                            //connectTracker();
                            getPopup();
                        }
                    } else {
                        if (HomeFragment.mClient != null) {
                            HomeFragment.mClient.connect();
                            getPopup();
                        }
                    }
                }
            }
        }, 0, 10000);
    }

    // set notification badge for challenge and notification screen
    public void setBadge() {
        int bdgcunt = UtilsPreferences.getInt(DashboardActivity.this, Constant.CahllangeBadge);
        if (bdgcunt > 0) {
            tv_badge_challanges.setVisibility(View.VISIBLE);
            tv_badge_challanges.setText(String.valueOf(bdgcunt));
        }

        int bdgcunt1 = UtilsPreferences.getInt(DashboardActivity.this, Constant.ProfileBadge);
        if (bdgcunt1 > 0) {
            if (Globals.completedchallbadges != null)
                Globals.completedchallbadges = null;

            tv_badge_profile.setVisibility(View.VISIBLE);
            tv_badge_profile.setText(String.valueOf(bdgcunt1));
        }

        int bdgcunt_notification = UtilsPreferences.getInt(DashboardActivity.this, Constant.NotificationBadge);
        if (bdgcunt_notification > 0) {

            tv_badge_notification.setVisibility(View.VISIBLE);
            tv_badge_notification.setText(String.valueOf(bdgcunt_notification));
        }

    }

    // back press click event
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            Debugger.debugE("Dashboard", "Backstack count : " + getSupportFragmentManager().getBackStackEntryCount());
            FragmentManager fm = DashboardActivity.this.getSupportFragmentManager();
            String currentFragmentTag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            if (currentFragmentTag != null && currentFragmentTag.length() > 0) {
                Log.e("FragName", currentFragmentTag);
                if (currentFragmentTag.equalsIgnoreCase("challanges")) {
                    setupChallengesAndOffersFragment();
                } else {
                    super.onBackPressed();
                }
            } else {
                super.onBackPressed();
                if (Constant.refreshLevelBluetoothOff) {
                    HomeFragment.getInstance().onRefresh();
                }

            }

            return;
        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            return;
        } else {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getString(R.string.msg_press_back_twice), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    // api call for GetPopupStatus
    public void getPopup() {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.user_id, UtilsPreferences.getString(this, Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));
            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                GetPopupStatus login = new GetPopupStatus(DashboardActivity.this, data, GetPopupStatusListener);
                login.execute();
            } else {
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Context getContext() {
        // return context of Dashboard Activity
        return DashboardActivity.this;
    }

    public static DashboardActivity getInstance() {
        // return context of Dashboard Activity
        return mContext;
    }

    // initialize process when screen appears
    public void init() {
        mContext = this;
        extra = getIntent().getExtras();
        if (extra != null) {
            if (extra.containsKey(Constant.fromNotificationFlag)) {
                if (extra.containsKey("ChallangeComplete")) {
                    setupHomeFragment(null);
                } else {
                    try {// show notification of according to notification type
                        notification = new JSONObject(extra.getString(Constant.notification));
                        //String NotificationType = notification.getString(Constant.NotificationType);
                        if (notification.has(Constant.NotificationType)) {
                            NotificationType = notification.getString(Constant.NotificationType);
                            alert = notification.getString(Constant.alert);
                        }

                        if (notification.has(Constant.notificationdata)) {
                            String data = notification.getString(Constant.notificationdata);
                            if (data != null && !data.isEmpty()) {
                                JSONObject notification_data = new JSONObject(notification.getString(Constant.notificationdata));
                                if (notification_data.has(Constant.NotificationType)) {
                                    NotificationType = notification_data.getString(Constant.NotificationType);
                                }
                            }
                        }

                        if (!NotificationType.isEmpty()) {
                            if (NotificationType.equalsIgnoreCase(Constant.manually_win_nitification)) {
                                setupHomeFragment(null);
                                InstantWinPopup(alert);
                            } else if (NotificationType.equalsIgnoreCase(Constant.sync_band_request_notification)) {
                                Bundle extra = new Bundle();
                                extra.putString(Constant.notification, alert);
                                setupHomeFragment(extra);
//                                InstantWinPopup(alert);
                            } else {
                                setupChallengesAndOffersFragment();
                            }
                        } else {
                            addFragment(ChallengesAndOffersFragment.newInstance(null), Constant.challenges);
                            iv_home.setImageResource(R.drawable.home);
                            iv_challenges.setImageResource(R.drawable.challenge2);
                            iv_leaderboard.setImageResource(R.drawable.leaderboard);
                            iv_profile.setImageResource(R.drawable.timeline);
                            iv_settings.setImageResource(R.drawable.profile);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (extra.containsKey(Constant.key_refresh)) {
                if (extra.getBoolean(Constant.key_refresh)) {
                    Bundle extra = new Bundle();
                    extra.putBoolean(Constant.key_refresh, true);
                    setupHomeFragment(extra);
                }
            } else {
                setupHomeFragment(null);
            }
            extra = null;//for test
        } else {
            setupHomeFragment(null);
        }
        getCurrentWeek();
    }

    // show dialog for wining popup
    public static void InstantWinPopup(String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder
                (getInstance(), R.style.MyAlertDialogStyle);
        alertDialog.setCancelable(false);
        HomeFragment.setChartforNotificationtype();
        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //HomeFragment.setChartforNotificationtype();
                dialog.cancel();
                // Write your code here to invoke YES event
            }
        });

        alertDialog.show();
    }

    // initialize all controls define in xml layout
    public void initializeControls() {
        try {
            GetPopupStatusListener = this;
            UpdatePopupStatusListener = this;
            onGetFitsListener = this;
            fitnessTrackerOperationslistener = this;
            iv_home = (ImageView) findViewById(R.id.iv_home);
            iv_challenges = (ImageView) findViewById(R.id.iv_challenges);
            iv_leaderboard = (ImageView) findViewById(R.id.iv_leaderboard);
            iv_profile = (ImageView) findViewById(R.id.iv_profile);
            iv_settings = (ImageView) findViewById(R.id.iv_settings);
            tv_badge_challanges = (TextView) findViewById(R.id.tv_badge_challanges);
            tv_badge_profile = (TextView) findViewById(R.id.tv_badge_profile);
            tv_badge_notification = (TextView) findViewById(R.id.tv_badge_notification);

           /* if (UtilsPreferences.getInt(getApplicationContext(), "ProfileBadge") > 0) {
                tv_badge_profile.setVisibility(View.VISIBLE);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            iv_home.setOnClickListener(this);
            iv_challenges.setOnClickListener(this);
            iv_leaderboard.setOnClickListener(this);
            iv_profile.setOnClickListener(this);
            iv_settings.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event listener
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_home:
                if (HomeFragment.getInstance() == null) {
                    setupHomeFragment(null);
                } else {
                    if (!HomeFragment.getInstance().isVisible()) {
                        setupHomeFragment(null);
                    }
                }
                break;

            case R.id.iv_challenges:
                if (ChallengesAndOffersFragment.getInstance() == null) {
                    setupChallengesAndOffersFragment();
                } else {
                    if (!ChallengesAndOffersFragment.getInstance().isVisible()) {
                        setupChallengesAndOffersFragment();
                    }
                }
                break;

            case R.id.iv_leaderboard:
                if (LeaderboardFragment.getInstance() == null) {
                    setupLeaderboardFragment();
                } else {
                    if (!LeaderboardFragment.getInstance().isVisible()) {
                        setupLeaderboardFragment();
                    }
                }
                break;

            case R.id.iv_profile:
                if (ProfileFragment.getInstance() == null) {
                    setupProfileFragment();
                } else {
                    if (!ProfileFragment.getInstance().isVisible()) {
                        setupProfileFragment();
                    }
                }
                break;

            case R.id.iv_settings:
                if (NotificationFragment.getInstance() == null) {
                    setupNotificationFragment();
                } else {
                    if (!NotificationFragment.getInstance().isVisible()) {
                        setupNotificationFragment();
                    }
                }
                break;

            default:
                break;
        }
    }

    // open home screen
    public void setupHomeFragment(Bundle extra) {
        addFragment(HomeFragment.newInstance(extra), Constant.home);
        iv_home.setImageResource(R.drawable.home2);
        iv_challenges.setImageResource(R.drawable.challenge);
        iv_leaderboard.setImageResource(R.drawable.leaderboard);
        iv_profile.setImageResource(R.drawable.timeline);
        iv_settings.setImageResource(R.drawable.profile);
    }

    // open challenges screen
    public void setupChallengesAndOffersFragment() {
        addFragment(ChallengesAndOffersFragment.newInstance(null), Constant.challenges);
        iv_home.setImageResource(R.drawable.home);
        iv_challenges.setImageResource(R.drawable.challenge2);
        iv_leaderboard.setImageResource(R.drawable.leaderboard);
        iv_profile.setImageResource(R.drawable.timeline);
        iv_settings.setImageResource(R.drawable.profile);
        UtilsPreferences.setInt(getApplicationContext(), Constant.CahllangeBadge, 0);
        tv_badge_challanges.setVisibility(View.GONE);
    }

    // open leaderboard screen
    public void setupLeaderboardFragment() {
        addFragment(LeaderboardFragment.newInstance(null), Constant.leaderboard);
        iv_home.setImageResource(R.drawable.home);
        iv_challenges.setImageResource(R.drawable.challenge);
        iv_leaderboard.setImageResource(R.drawable.leaderboard2);
        iv_profile.setImageResource(R.drawable.timeline);
        iv_settings.setImageResource(R.drawable.profile);
    }

    // open profile screen
    public void setupProfileFragment() {
        addFragment(ProfileFragment.newInstance(null), Constant.profile);
        iv_home.setImageResource(R.drawable.home);
        iv_challenges.setImageResource(R.drawable.challenge);
        iv_leaderboard.setImageResource(R.drawable.leaderboard);
        iv_profile.setImageResource(R.drawable.timeline_selected);
        iv_settings.setImageResource(R.drawable.profile);
        UtilsPreferences.setInt(getApplicationContext(), Constant.ProfileBadge, 0);
        tv_badge_profile.setVisibility(View.GONE);
    }

    // open notification screen
    public void setupNotificationFragment() {
        addFragment(NotificationFragment.newInstance(null), Constant.Notification);
        iv_home.setImageResource(R.drawable.home);
        iv_challenges.setImageResource(R.drawable.challenge);
        iv_leaderboard.setImageResource(R.drawable.leaderboard);
        iv_profile.setImageResource(R.drawable.timeline);
        iv_settings.setImageResource(R.drawable.profile2);
        UtilsPreferences.setInt(getApplicationContext(), Constant.NotificationBadge, 0);
        tv_badge_notification.setVisibility(View.GONE);
    }

    // open required screen

    /**
     * @param fragment add fragment
     */
    public void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_main, fragment, null);
        fragmentTransaction.commit();
    }

    // get the current week number of month
    public void getCurrentWeek() {
        final Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);

        if (day >= 1 && day <= 7) {
            mCurrentweek = 1;
        } else if (day >= 8 && day <= 14) {
            mCurrentweek = 2;
        } else if (day >= 15 && day <= 21) {
            mCurrentweek = 3;
        } else if (day >= 22 && day <= 28) {
            mCurrentweek = 4;
        } else if (day >= 29) {
            mCurrentweek = 5;
        }
        Debugger.debugE("DayOfMonth...", day + " " + mCurrentweek);

    }

    // success listener of GetPopupStatus
    @Override
    public void onSucceedGetPopupStatus(ArrayList<ChallangesPopupModel> inProcessList) {
        Log.e("data in p", String.valueOf(inProcessList));
        this.inProcessList = inProcessList;
        if (inProcessList.size() > 0) {
            cnt = 0;
            maxCnt = 0;
            maxCnt = inProcessList.size();
            doProcess();
        } else {
            Log.e("no date", "for update status");
        }
        /*if (HomeFragment.mClient != null) {
            HomeFragment.mClient.connect();
            UpdateChallengesStatusInBackground updateChallengesStatusInBackground = new UpdateChallengesStatusInBackground(this,  HomeFragment.mClient);
            updateChallengesStatusInBackground.getData();
        }*/
    }

    // failed listener of GetPopupStatus
    @Override
    public void onFailedGetPopupStatus(String message) {

    }

    // process for getting challenges value
    public void doProcess() {
        if (cnt < maxCnt) {
            type = Integer.parseInt(inProcessList.get(cnt).getChallenge_type());
            chllng_val = Integer.parseInt(inProcessList.get(cnt).getChallenge_value());
            String val = inProcessList.get(cnt).getSecond_challenge_value();
            if (val != null && val.length() > 0)
                sec_chllng_val = Integer.parseInt(inProcessList.get(cnt).getSecond_challenge_value());

            val = inProcessList.get(cnt).getThird_challenge_value();
            if (val != null && val.length() > 0)
                third_chllng_val = Integer.parseInt(inProcessList.get(cnt).getThird_challenge_value());

            val = inProcessList.get(cnt).getFourth_challenge_value();
            if (val != null && val.length() > 0)
                fourth_chllng_val = Integer.parseInt(inProcessList.get(cnt).getFourth_challenge_value());

            if (UtilsPreferences.getString(getApplicationContext(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                getChallangesData();
            } else {
                if (HomeFragment.mClient != null) {
                    getGoogleFits = new GetGoogleFits(this, false, inProcessList.get(cnt), HomeFragment.mClient, onGetFitsListener, true);

                    switch (type) {
                        case 1:
                            getGoogleFits.new StepsCall().execute();
                            break;
                        case 2:
                            getGoogleFits.new CaloriesCall().execute();
                            break;
                        case 3:
                            getGoogleFits.new DistanceCall().execute();
                            break;
                        case 4:
                            getGoogleFits.new HeartRateCall().execute();
                            break;
                        case 6:
                            sec_chllng_val = Integer.parseInt(inProcessList.get(cnt).getSecond_challenge_value());
                            getGoogleFits.new StepsCall().execute();
                            break;
                        case 7:
                            sec_chllng_val = Integer.parseInt(inProcessList.get(cnt).getSecond_challenge_value());
                            getGoogleFits.new StepsCall().execute();
                            break;

                    }
                }
            }
        } else {
            Log.e("back call", "completed");
        }
    }

    // get calorie listener of CaloriesCall
    @Override
    public void onGetCalories(int cal) {
        cals = cal;
        checkNupdate();
    }

    //get steps listener of StepsCall
    @Override
    public void onGetSteps(int step) {
        steps = step;
        if (type == 7) {
            getGoogleFits.new CaloriesCall().execute();
        } else if (type == 6) {
            getGoogleFits.new DistanceCall().execute();
        } else {
            checkNupdate();
        }
    }

    //get distance listener of DistanceCall
    @Override
    public void onGetDistance(int distance) {
        dis = distance;
        checkNupdate();
    }

    //get Heart rate listener of HeartRateCall
    @Override
    public void onGetHeartRate(int heart) {
        hearts = heart;
        checkNupdate();
    }

    public void checkNupdate() {
        ChartDataModel chartmodel = new ChartDataModel();
        switch (type) {
            case 1://steps
                levelPer = Math.round((steps * 100) / chllng_val);
                //openPopupDialog();
                break;
            case 2://calories
                levelPer = Math.round((cals * 100) / chllng_val);
                //openPopupDialog();
                break;
            case 3://distance
                levelPer = Math.round((dis * 100) / chllng_val);
                //openPopupDialog();
                break;
            case 4://sleep
                //levelPer = Math.round((hearts * 100) / chllng_val);
                levelPer = Math.round((sleep * 100) / chllng_val);
                //openPopupDialog();
                break;
            case 5://distance and sleep
                //levelPer = Math.round((sleep * 100) / chllng_val);
                findChallangePer(dis, sleep, 0, 0, 2);
                break;
            case 6://steps and distance
                findChallangePer(steps, dis, 0, 0, 2);
                break;
            case 7://steps and calories
                findChallangePer(steps, cals, 0, 0, 2);
                break;
            case 8://steps and sleep
                findChallangePer(steps, sleep, 0, 0, 2);
                break;
            case 9://sleep and calories
                findChallangePer(sleep, cals, 0, 0, 2);
                break;
            case 10://distance and calories
                findChallangePer(dis, cals, 0, 0, 2);
                break;
            case 11: //steps and calories and distance
                findChallangePer(steps, cals, dis, 0, 3);
                break;
            case 12://steps and calories nad sleep
                findChallangePer(steps, cals, sleep, 0, 3);
                break;
            case 13://steps and distance and sleep
                findChallangePer(steps, dis, sleep, 0, 3);
                break;
            case 14://calories and distance and sleep
                findChallangePer(cals, dis, sleep, 0, 3);
                break;
            case 15://steps and calories and distance and sleep
                findChallangePer(steps, cals, dis, sleep, 4);
                break;
        }

        //openPopupDialog();
        popStatus = Integer.parseInt(inProcessList.get(cnt).getPop_level_status());

        level1 = Integer.parseInt(inProcessList.get(cnt).getPop_level_1());
        level2 = Integer.parseInt(inProcessList.get(cnt).getPop_level_2());
        level3 = Integer.parseInt(inProcessList.get(cnt).getPop_level_3());
        level4 = Integer.parseInt(inProcessList.get(cnt).getPop_level_4());
        level5 = Integer.parseInt(inProcessList.get(cnt).getPop_level_5());

        //for test only
        if (level5 == 0)
            level5 = levelPer + 1;

        if (level4 == 0)
            level4 = levelPer + 1;

        if (level3 == 0)
            level3 = levelPer + 1;

        if (level2 == 0)
            level2 = levelPer + 1;

        switch (popStatus) {
            case 0:
                checkstatus(levelPer, level1);
                break;
            case 1:
                checkstatus(levelPer, level2);
                break;
            case 2:
                checkstatus(levelPer, level3);
                break;
            case 3:
                checkstatus(levelPer, level4);
                break;
            case 4:
                checkstatus(levelPer, level5);
                break;
            case 5:
                cnt++;
                doProcess();
                break;
        }
    }

    // get challenge percentage

    /**
     * @param p1   get steps data
     * @param p2   get calories data
     * @param p3   get distance data
     * @param p4   get sleep data
     * @param type get the type of challenge
     */
    public void findChallangePer(int p1, int p2, int p3, int p4, int type) {
        int per1, per2, per3, per4;

        per1 = (p1 * 100) / chllng_val;
        per2 = (p2 * 100) / sec_chllng_val;
        per3 = (p3 * 100) / third_chllng_val;
        per4 = (p4 * 100) / fourth_chllng_val;

        if (type == 2) {
            levelPer = Math.round(Math.min(per1, Math.min(per1, per2)));
        } else if (type == 3) {
            levelPer = Math.round(Math.min(per1, Math.min(per2, per3)));
        } else {
            levelPer = Math.round(Math.min(Math.min(per1, per2), Math.min(per3, per4)));
        }
    }

    /**
     * @param per      percentage value
     * @param levelval level no
     */
    public void checkstatus(int per, int levelval) {
        if (per >= 100) {
            cnt++;
            doProcess();
        } else if (per >= levelval) {
            if (levelPer >= level5) {
                popStatus = 4;
                perText = level5;
            } else if (levelPer >= level4) {
                popStatus = 3;
                perText = level4;
            } else if (levelPer >= level3) {
                popStatus = 2;
                perText = level3;
            } else if (levelPer >= level2) {
                popStatus = 1;
                perText = level2;
            } else {
                popStatus = 0;
                perText = level1;
            }

            if (!UtilsPreferences.getBoolean(getContext(), Constant.isPerPopVisible)) {
                if (!UtilsPreferences.getBoolean(getContext(), Constant.isSyncing))
                    openPopupDialog();
            }

        } else {
            cnt++;
            doProcess();
        }
    }

    // challenge popup
    public void openPopupDialog() {
        UtilsCommon.destroyProgressBar();
        alertDialogMain = new Dialog(getContext(), android.R.style.Theme_Light);
        alertDialogMain.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogMain.setContentView(R.layout.popup_challanges_activity);
        alertDialogMain.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogMain.setCancelable(false);
        //alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        final TextView tv_challengeper, tv_challengedesc, tv_challengedesc1, tv_title, tv_link;
        final ImageView iv_challengeimg;
        final LinearLayout ll_back;
        final Button btn_gotit;

        ll_back = (LinearLayout) alertDialogMain.findViewById(R.id.ll_back);
        tv_title = (TextView) alertDialogMain.findViewById(R.id.tv_title);
        tv_title.setText("");

        btn_gotit = (Button) alertDialogMain.findViewById(R.id.btn_gotit);
        btn_gotit.setTypeface(setFont(getContext(), R.string.app_bold));//.setTypeface(setFont(getContext(), R.string.app_bold));

        tv_challengeper = (TextView) alertDialogMain.findViewById(R.id.tv_challengeper);
        tv_challengedesc = (TextView) alertDialogMain.findViewById(R.id.tv_challengedesc);
        tv_challengedesc1 = (TextView) alertDialogMain.findViewById(R.id.tv_challengedesc1);
        iv_challengeimg = (ImageView) alertDialogMain.findViewById(R.id.iv_challengeimg);
        tv_link = (TextView) alertDialogMain.findViewById(R.id.tv_link);
        tv_challengedesc.setMovementMethod(new ScrollingMovementMethod());

        if (levelPer >= 100) {
            tv_challengeper.setText("100 % - " + getResources().getString(R.string.almost_there));
        } else {
            tv_challengeper.setText(String.valueOf(perText) + " % - " + getResources().getString(R.string.almost_there));
        }

        String url = this.getResources().getString(R.string.server_url)
                + this.getResources().getString(R.string.challenge_images_url)
                + inProcessList.get(cnt).getImage();

        Glide.with(this).load(url).placeholder(R.drawable.app_placeholder).into(iv_challengeimg);

        String url1 = inProcessList.get(cnt).getReward_hyperlink();
        String url_text = inProcessList.get(cnt).getReward_hyperlink_text();

        if ((url1 != null && url1.length() > 0) && (url_text != null && url_text.length() > 0)) {
            tv_link.setText(inProcessList.get(cnt).getReward_hyperlink_text());
            tv_link.setPaintFlags(tv_link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        } else {
            tv_link.setVisibility(View.GONE);
        }

        // tv_link.setText(inProcessList.get(cnt).getReward_hyperlink_text());

        switch (popStatus) {
            case 0:
                //string.replace("\\n", System.getProperty("line.separator"));
                tv_challengedesc.setText(inProcessList.get(cnt).getPop_level_text1().replace("\\n", System.getProperty("line.separator")));
                break;
            case 1:
                tv_challengedesc.setText(inProcessList.get(cnt).getPop_level_text2().replace("\\n", System.getProperty("line.separator")));
                break;
            case 2:
                tv_challengedesc.setText(inProcessList.get(cnt).getPop_level_text3().replace("\\n", System.getProperty("line.separator")));
                break;
            case 3:
                tv_challengedesc.setText(inProcessList.get(cnt).getPop_level_text4().replace("\\n", System.getProperty("line.separator")));
                break;
            case 4:
                tv_challengedesc.setText(inProcessList.get(cnt).getPop_level_text5().replace("\\n", System.getProperty("line.separator")));
                break;
        }

        btn_gotit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePopupStatus();
            }
        });

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilsPreferences.setBoolean(getContext(), Constant.isPerPopVisible, false);
                alertDialogMain.dismiss();
                cnt++;
                doProcess();
            }
        });

        tv_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBrowser(inProcessList.get(cnt).getReward_hyperlink());
            }
        });

        UtilsPreferences.setBoolean(getContext(), Constant.isPerPopVisible, true);
        alertDialogMain.show();

    }

    // intent to url for goyo page
    public void openBrowser(String address) {
        String url = address;//"http://www.goyo.lk/"

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    // api call for Update Popup Status
    public void updatePopupStatus() {
        JSONObject data = new JSONObject();
        try {
            data.put(Constant.user_id, UtilsPreferences.getString(this, Constant.user_id));
            data.put(Constant.challenge_id, inProcessList.get(cnt).getChallenge_id());
            data.put("pop_level_status", String.valueOf(popStatus + 1));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));

            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                UtilsCommon.showProgressDialog(DashboardActivity.this);
                UpdatePopupStatus updatePop = new UpdatePopupStatus(DashboardActivity.this, data, UpdatePopupStatusListener);
                updatePop.execute();
            } else {
                // Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of UpdatePopupStatus
    @Override
    public void onSucceedUpdatePopupStatus(String message) {
        UtilsCommon.destroyProgressBar();
        UtilsPreferences.setBoolean(getContext(), Constant.isPerPopVisible, false);
        alertDialogMain.dismiss();
        cnt++;
        doProcess();
    }

    // failed listener of UpdatePopupStatus
    @Override
    public void onFailedUpdatePopupStatus(String message) {
        UtilsCommon.destroyProgressBar();
        UtilsPreferences.setBoolean(getContext(), Constant.isPerPopVisible, false);
        alertDialogMain.dismiss();
    }

    // get challenges data
    public void getChallangesData() {

        String startTimeString = "";
        int duration = Integer.parseInt(inProcessList.get(cnt).getDuration());
        String cStartDate = inProcessList.get(cnt).getChallenge_start_date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

/*        if (cStartDate != null) {
            if (cStartDate.equalsIgnoreCase("0000-00-00 00:00:00") ||
                    cStartDate == null) {
                startTimeString = inProcessList.get(cnt).getAccept_date();
            } else {
                startTimeString = cStartDate;
            }
        } else {
            startTimeString = inProcessList.get(cnt).getAccept_date();
        }
*/
        startTimeString = inProcessList.get(cnt).getAccept_date();

        Date startDate = null;
        try {
            startDate = formater.parse(startTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar endCalendar = Calendar.getInstance();
        Date endDate = startDate;
        endCalendar.setTime(endDate);
        //int duration = Integer.parseInt(obj_Challenges.getDuration());
        endCalendar.add(Calendar.MINUTE, duration);
        //Date end = endCalendar.getTime();

        String chlngStartDate = formater.format(startDate);
        String chlngEndDate = formater.format(endCalendar.getTime());//
        Date endDiff = endCalendar.getTime();

        FitnessTrackerOperations fitnessTrackerOperations = new FitnessTrackerOperations(getContext(), fitnessTrackerOperationslistener);
        fitnessTrackerOperations.readDatafromTracker(chlngStartDate, chlngEndDate, Integer.parseInt(inProcessList.get(cnt).getChallenge_type()));

    }

    // success listener of FitnessTrackerOperations and bind the data
    @Override
    public void onSucceedtoFitnessTrackerOperation(int steps, int cals, int dis, int hearts, int sleep) {
        this.cals = cals;
        this.steps = steps;
        this.dis = dis;
        this.sleep = sleep;
        // UtilsCommon.setHealthDataGlobal(steps, cals, dis, hearts);
        checkNupdate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myTimer != null) {
            myTimer.cancel(); // cancel the timer
            myTimer.purge();
        }
        if (bubbleTimer != null) {
            bubbleTimer.cancel(); // cancel the bubble timer
            bubbleTimer.purge();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
/*onsyncdevice = this;
            syncdevice.Onsyncdevice,
            syncdevice.Onsyncdevice onsyncdevice;*/