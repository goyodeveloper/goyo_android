package com.differenzsystem.goyog.dashboard.challenges;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.ChallengeAcceptCall;
import com.differenzsystem.goyog.api.ChallengeAcceptCall.OnChallengeAcceptListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.utility.BaseFragment;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class ChallengesDetailFragment extends BaseFragment implements View.OnClickListener, OnChallengeAcceptListener {
    LinearLayout ll_back;
    TextView tv_title;
    ImageView iv_item;
    TextView tv_name, tv_subtitle, tv_description, tv_date, txt_date;
    Button btn_submit;

    OnChallengeAcceptListener onChallengeAcceptListener;
    static Bundle extras;
    ChallengesModel obj_Challenges;

    boolean isFromNextLevelSCreen = false;

    /**
     * @param extra pass the object in bundle
     * @return fragment
     */
    public static ChallengesDetailFragment newInstance(Bundle extra) {
        ChallengesDetailFragment fragment = new ChallengesDetailFragment();
        extras = extra;
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.challenges_detail_fragment, container, false);
        try {
            if (extras != null) {// get the object of ChallengesModel
                obj_Challenges = (ChallengesModel) extras.getSerializable("Object");
                if (extras.containsKey(Constant.isFromNextLevelScreen)) {
                    if (extras.getBoolean(Constant.isFromNextLevelScreen)) {
                        isFromNextLevelSCreen = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        initializeControls(view);
        initializeControlsAction();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Challenge_Detail);
    }

    // initialize all controls define in xml layout
    public void initializeControls(View view) {
        try {
            onChallengeAcceptListener = this;
            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_title.setText("");
            tv_subtitle = (TextView) view.findViewById(R.id.tv_subtitle);

            iv_item = (ImageView) view.findViewById(R.id.iv_item);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_description = (TextView) view.findViewById(R.id.tv_description);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            txt_date = (TextView) view.findViewById(R.id.txt_date);
            btn_submit = (Button) view.findViewById(R.id.btn_submit);
            btn_submit.setTypeface(setFont(getContext(), R.string.app_semibold));

            tv_description.setMovementMethod(new ScrollingMovementMethod());

            setData(obj_Challenges);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            ll_back.setOnClickListener(this);
            btn_submit.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set the value from the ChallengesModel object
    public void setData(ChallengesModel object) {
        try {
            String url = getActivity().getResources().getString(R.string.server_url)
                    + getActivity().getResources().getString(R.string.challenge_images_url)
                    + object.getImage();

//            Glide.with(getActivity())
//                    .load(url)
//                    .placeholder(R.drawable.banner_placeholder)
//                    .into(iv_item);

            Glide.with(getActivity())
                    .load(url)
                    .dontAnimate()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                            return false;
                        }
                    })
                    .error(R.drawable.banner_placeholder)
                    .into(iv_item);

            tv_name.setText(object.getName());
            tv_subtitle.setText(object.getSubtitle());
            if (object.getDescription() != null)
                tv_description.setText(object.getDescription().replace("\\n", System.getProperty("line.separator")));

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                if (UtilsPreferences.getBoolean(getActivity(), Constant.isInProgress)) {
                    btn_submit.setVisibility(View.INVISIBLE);
                } else {
                    if (object.getAccept_flag() != null) {
                        if (object.getAccept_flag().equalsIgnoreCase("1")) {
                            btn_submit.setVisibility(View.VISIBLE);
                        }
                    }
                }
            } else {
                btn_submit.setVisibility(View.INVISIBLE);
            }

            Date startDate;
            Date endDate;
            String strStartDate, strEndDate;
            String dateString = "";
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                endDate = format.parse(object.getChallenges_exp_date()); // Added on 25-07-2017
//                endDate = format.parse(object.getOfferexpire_date());

                if (object.getChallenge_start_date() != null && object.getChallenge_start_date().length() > 0) {
                    if (!object.getChallenge_start_date().equalsIgnoreCase("")
                            && !object.getChallenge_start_date().equalsIgnoreCase("0000-00-00 00:00:00")) {
                        txt_date.setVisibility(View.GONE);
                        startDate = format.parse(object.getChallenge_start_date());
                        String startDayNumberSuffix = UtilsCommon.getDaySuffix(startDate.getDate());
                        String endDayNumberSuffix = UtilsCommon.getDaySuffix(startDate.getDate());

                        SimpleDateFormat getMonth = new SimpleDateFormat("MMMM");
                        SimpleDateFormat getDay = new SimpleDateFormat("d'" + startDayNumberSuffix + "'");
                        SimpleDateFormat getYear = new SimpleDateFormat(" yyyy");

                        strStartDate = getColoredSpanned(getMonth.format(startDate), "#828844") + " " + getDay.format(startDate);

                        strEndDate = getColoredSpanned(getMonth.format(endDate), "#828844") + " " + getDay.format(endDate) +
                                getColoredSpanned(getYear.format(endDate), "#828844");
                        dateString = strStartDate + getColoredSpanned(" - ", "#828844") + strEndDate;
                    } else {
                        txt_date.setVisibility(View.VISIBLE);
                        String dayNumberSuffix = UtilsCommon.getDaySuffix(endDate.getDate());
                        SimpleDateFormat newFormat = new SimpleDateFormat("MMMM " + "d'" + dayNumberSuffix + " ' yyyy");
                        dateString = newFormat.format(endDate);
                    }
                } else {
                    txt_date.setVisibility(View.VISIBLE);
                    String dayNumberSuffix = UtilsCommon.getDaySuffix(endDate.getDate());
                    SimpleDateFormat newFormat = new SimpleDateFormat("MMMM " + "d'" + dayNumberSuffix + " ' yyyy");
                    dateString = newFormat.format(endDate);
                }
                tv_date.setText(Html.fromHtml(dateString));

            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;

            case R.id.btn_submit:
                acceptChallenge(obj_Challenges);
                break;

            default:
                break;
        }
    }

    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.pull_in_left, R.anim.push_out_right);
        fragmentTransaction.add(R.id.frame_main, fragment, null);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    // accept challenge

    /**
     * @param obj_Challenges pass the ChallengesModel details
     */
    public void acceptChallenge(ChallengesModel obj_Challenges) {
        try {
            JSONObject object = new JSONObject();
            object.put(Constant.user_id, UtilsPreferences.getString(getContext(), Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));
            object.put(Constant.challenge_id, obj_Challenges.getChallenge_id());
            object.put(Constant.device_type, Constant.device_type_value);
            object.put(Constant.challenge_status, "1");
            /*if (obj_Challenges.getStatus() != null) {
                if (obj_Challenges.getStatus().equalsIgnoreCase("1"))
                    object.put(Constant.challenge_status, "2");
                else
                    object.put(Constant.challenge_status, "1");
            } else {*/
            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            object.put(Constant.timezone, tz.getID());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateandTime = sdf.format(new Date());
            object.put(Constant.date, currentDateandTime);
            //}

            new ChallengeAcceptCall(getContext(), onChallengeAcceptListener, object, true).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of ChallengeAcceptCall
    @Override
    public void onSucceedChallengeAccept(String message, JSONObject jsonResponse) {
        //addFragment(ChallengesAndOffersFragment.newInstance(null), "ChallengesAndOffersFragment");
        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(getActivity(), Constant.Key_inProgressList);

        UtilsPreferences.setBoolean(getActivity(), Constant.isInProgress, true);
        if (isFromNextLevelSCreen) {
            Constant.refreshLevelBluetoothOff = true;  // this to refresh the level detail in home screen when bluetooth is off
        }
        getActivity().onBackPressed();
        ChallengesAndOffersFragment.getInstance().tv_challenges.performClick();

    }

    // failed listener of ChallengeAcceptCall
    @Override
    public void onFailedChallengeAccept(String message) {
        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(getActivity(), Constant.Key_inProgressList);
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    // to set the required text font color
    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
}
