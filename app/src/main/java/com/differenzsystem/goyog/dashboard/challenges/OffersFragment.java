package com.differenzsystem.goyog.dashboard.challenges;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.adapter.OffersAdapter;
import com.differenzsystem.goyog.adapter.OffersPageAdapter;
import com.differenzsystem.goyog.api.OffersCall;
import com.differenzsystem.goyog.api.OffersCall.OnOffersListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.OffersModel;
import com.differenzsystem.goyog.utility.BaseFragment;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class OffersFragment extends BaseFragment implements OnOffersListener {
    ViewPager viewPager;
    CirclePageIndicator indicator;
    ListView list_data;
    LinearLayout ll_main_offer, ll_no_offer;

    OnOffersListener onOffersListener;
    OffersAdapter adapter;
    ArrayList<OffersModel> inProcessList = new ArrayList<>();

//    private FirebaseAnalytics mFirebaseAnalytics;

    // create the instance of OffersFragment
    public static OffersFragment newInstance(Bundle extra) {
        OffersFragment fragment = new OffersFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.offers_fragment, container, false);
        initializeControls(view);
        initializeControlsAction();
        getList();
        return view;
    }

    // initialize all controls define in xml layout
    public void initializeControls(View view) {
        try {
            ll_main_offer = (LinearLayout) view.findViewById(R.id.ll_main_offer);
            ll_no_offer = (LinearLayout) view.findViewById(R.id.ll_no_offer);
            viewPager = (ViewPager) view.findViewById(R.id.viewPager);
            indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
            indicator.setStrokeWidth(2);
            indicator.setStrokeColor(getActivity().getResources().getColor(R.color.pager_selected));
            list_data = (ListView) view.findViewById(R.id.list_data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            onOffersListener = this;
            list_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    OffersModel object = inProcessList.get(position);
                    Bundle extras = new Bundle();
                    extras.putSerializable("Object", object);
                    addFragment(OfferDetailQR.newInstance(extras), "OffersDetailQRFragment");
                    /*if (object.getType() == "0") {//charity
                        addFragment(OfferDetailQR.newInstance(extras), "OffersDetailQRFragment");
                    } else if (object.getType() == "1") {//QR COde
                        addFragment(OffersDetailFragment.newInstance(extras), "OffersDetailFragment");
                    } else {//point
                        addFragment(OffersDetailFragment.newInstance(extras), "OffersDetailFragment");
                    }*/

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // open required screen

    /**
     * @param fragment add fragment
     */
    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = this.getParentFragment().getFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
        fragmentTransaction.add(R.id.frame_main, fragment, null);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    // get the list of challenges
    public void getList() {
        try {
            JSONObject object = new JSONObject();
            object.put(Constant.user_id, UtilsPreferences.getString(getContext(), Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));

            if (ConnectionDetector.isConnectingToInternet(getActivity()))
                new OffersCall(getContext(), onOffersListener, object, true).execute();
            else {
                //Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of OffersCall
    public void onSucceedOffers(ArrayList<OffersModel> inProcessList) {
        this.inProcessList = inProcessList;
        try {
            if (inProcessList.size() > 0) {
                adapter = new OffersAdapter(getActivity(), inProcessList);
                list_data.setAdapter(adapter);
            } else {
                ll_main_offer.setVisibility(View.GONE);
                ll_no_offer.setVisibility(View.VISIBLE);
            }

            if (inProcessList.size() > 0) {
                viewPager.setAdapter(new OffersPageAdapter(this, getActivity(), inProcessList));
                indicator.setViewPager(viewPager);
                viewPager.setOffscreenPageLimit(inProcessList.size() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener of OffersCall
    @Override
    public void onFailedOffers(String message) {
        if (message != null && message.length() > 0)
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Reward);
    }

    private void recordScreenViews() {
        // [START set_current_screen]
//        mFirebaseAnalytics.setCurrentScreen(getActivity(), "Reward Screen", null /* class override */);
        // [END set_current_screen]
    }
}
