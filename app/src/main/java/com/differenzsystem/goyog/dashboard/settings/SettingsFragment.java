package com.differenzsystem.goyog.dashboard.settings;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.ScanDeviceActivity;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.Logout;
import com.differenzsystem.goyog.api.UpdateProfileImageCall;
import com.differenzsystem.goyog.api.UpdateProfileImageCall.OnUpdateProfileImageListener;
import com.differenzsystem.goyog.api.UpdateUserDetailCall;
import com.differenzsystem.goyog.api.UpdateUserDetailCall.OnUpdateUserDetailListener;
import com.differenzsystem.goyog.api.Update_Led_Visible_Status;
import com.differenzsystem.goyog.api.get_user_sync_data;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.GetUserSyncDataModel;
import com.differenzsystem.goyog.model.RegisterModel;
import com.differenzsystem.goyog.userProfile.GetStartedActivity;
import com.differenzsystem.goyog.userProfile.LoginActivity;
import com.differenzsystem.goyog.userProfile.RegistrationActivity_Allset;
import com.differenzsystem.goyog.userProfile.RegistrationActivity_Height_Weight;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPermission;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.veryfit.multi.nativeprotocol.Protocol;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static android.app.Activity.RESULT_OK;
import static com.differenzsystem.goyog.Application.mFirebaseAnalytics;
import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class SettingsFragment extends Fragment implements OnClickListener, OnUpdateUserDetailListener, OnUpdateProfileImageListener, get_user_sync_data.OnGetUserSyncData, Update_Led_Visible_Status.OnUpdateLedVisibleStatus {
    LinearLayout ll_back, ll_logout;
    TextView tv_title, tv_full_name, tv_joined_date, tv_membership_exp_date, tv_goyopair;
    LinearLayout ll_height_weight, ll_invite, ll_about, ll_leaderboard, ll_sync, ll_personal_detail, ll_goyohr, ll_buygoyo;
    ImageView iv_edit_name;
    CircleImageView iv_profile, iv_profile_hide;

    LinearLayout ll_policy_number;

    TextView tv_policy_expiry_date, tv_policy_no;

    OnUpdateUserDetailListener onUpdateUserDetailListener;
    OnUpdateProfileImageListener onUpdateProfileImageListener;

    Update_Led_Visible_Status.OnUpdateLedVisibleStatus onUpdateLedVisibleStatus;

    String full_name = null, Profile_image = null;

    boolean isVisible = false;
    public static SettingsFragment mContext;
    AlertDialog.Builder builder;
    View v;
    Dialog d = null;

    TextView tv_connect_validic;
    TextView tv_connect_phone;

    ToggleButton tg_leaderboard;
    TextView hidemestatus;
    final Handler handler = new Handler();
    Runnable runnable;
    final int FIVE_SECONDS = 60000;
    //sync animation
    LinearLayout ll_sync_progress;
    ProgressBar pb_sync;
    TextView tv_syncstatus;
    ImageView img_sync;
    private long mLastClickTime = 0;

//    private FirebaseAnalytics mFirebaseAnalytics;

    get_user_sync_data.OnGetUserSyncData onGetUserSyncData;
    private boolean is_led_visible = false;


    /**
     * Note
     * usertype 1 = live user
     * usertype 2 = demo user
     * connect_flag = connected_with_validic means get data from validic
     * connect_flag = connected_with_device means get data from google fit
     * connect_flag = connected_with_tracker means get data from band
     */


    // create the new instance of setting screen
    public static SettingsFragment newInstance(Bundle extra) {
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(extra);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
    }

    public static SettingsFragment getInstance() {
        // return the context of SettingsFragment
        return mContext;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            isVisible = true;
        else
            isVisible = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        FacebookSdk.sdkInitialize(getActivity());
        /*EasyImage.configuration(getActivity())
                .setImagesFolderName(getString(R.string.app_name))
                .saveInAppExternalFilesDir();*/
        EasyImage.configuration(getActivity()).setImagesFolderName(getString(R.string.app_name)).saveInRootPicturesDirectory();
        initializeControls(view);
        initializeControlsAction();
        if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
            scheduleThread();
//            tv_membership_exp_date.setVisibility(View.VISIBLE);

            // check condition for displaying policy expired text by comparing expiry date
            if (UtilsCommon.CompareDatesString(UtilsCommon.ConvertStringIntoDateSetting(UtilsPreferences.getString(getActivity(), Constant.user_exp_date)))) {
                tv_policy_no.setVisibility(View.GONE);
                tv_policy_expiry_date.setText(getString(R.string.expiry_date_text_1)
                        + UtilsCommon.ConvertStringIntoDateSetting(UtilsPreferences.getString(getActivity(), Constant.user_exp_date))
                        + getString(R.string.expiry_date_text_2));
            } else {
                // call getusersyncdata
                callGetUserSyncData();

            }

        } else {
            ll_buygoyo.setVisibility(View.VISIBLE);

            ll_height_weight.setVisibility(View.GONE);
            ll_policy_number.setVisibility(View.GONE);
        }

        return view;
    }

    // initialize all controls define in xml layout
    public void initializeControls(View view) {
        try {
            mContext = this;

            tg_leaderboard = (ToggleButton) view.findViewById(R.id.tg_leaderboard);
            hidemestatus = (TextView) view.findViewById(R.id.hidemestatus);

            ll_back = (LinearLayout) view.findViewById(R.id.ll_back);
            ll_back.setVisibility(View.VISIBLE);
            ll_logout = (LinearLayout) view.findViewById(R.id.ll_logout);
            ll_logout.setVisibility(View.VISIBLE);

            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_full_name = (TextView) view.findViewById(R.id.tv_full_name);
            tv_joined_date = (TextView) view.findViewById(R.id.tv_joined_date);
            tv_membership_exp_date = (TextView) view.findViewById(R.id.tv_membership_exp_date);
            tv_title.setText(getActivity().getString(R.string.settings));

            ll_height_weight = (LinearLayout) view.findViewById(R.id.ll_height_weight);
            tv_policy_expiry_date = (TextView) view.findViewById(R.id.tv_policy_expiry_date);
            tv_policy_no = (TextView) view.findViewById(R.id.tv_policy_no);
            ll_policy_number = (LinearLayout) view.findViewById(R.id.ll_policy_number);
            ll_invite = (LinearLayout) view.findViewById(R.id.ll_invite);
            ll_sync = (LinearLayout) view.findViewById(R.id.ll_sync);
            ll_about = (LinearLayout) view.findViewById(R.id.ll_about);
            ll_leaderboard = (LinearLayout) view.findViewById(R.id.ll_leaderboard);
            ll_personal_detail = (LinearLayout) view.findViewById(R.id.ll_personal_detail);
            ll_goyohr = (LinearLayout) view.findViewById(R.id.ll_goyohr);
            ll_buygoyo = (LinearLayout) view.findViewById(R.id.ll_buygoyo);
            iv_edit_name = (ImageView) view.findViewById(R.id.iv_edit_name);
            iv_profile = (CircleImageView) view.findViewById(R.id.iv_profile);
            iv_profile_hide = (CircleImageView) view.findViewById(R.id.iv_profile_hide);
            iv_profile.setBorderWidth(3);
            iv_profile.setBorderColor(getResources().getColor(R.color.yellow_text));

            //sync animation
            ll_sync_progress = (LinearLayout) view.findViewById(R.id.ll_sync_progress);
            pb_sync = (ProgressBar) view.findViewById(R.id.pb_sync);
            tv_syncstatus = (TextView) view.findViewById(R.id.tv_syncstatus);
            img_sync = (ImageView) view.findViewById(R.id.img_sync);


            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) { // check whether current user is live or demo
                String led_status = UtilsPreferences.getString(getActivity(), Constant.led_visible_status);
                if (led_status == null || led_status.isEmpty() || led_status.equalsIgnoreCase("1")) { // check the visible status of current user in leaderboard list
                    is_led_visible = true;
                    tg_leaderboard.setChecked(true);
                    hidemestatus.setText(Constant.hidemeon);
                } else {
                    is_led_visible = false;
                    tg_leaderboard.setChecked(false);
                    hidemestatus.setText(Constant.hidemeoff);
                }
            }

            // toggle leaderboard visibilty click event
            tg_leaderboard.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (is_led_visible) {
                        call_Update_Led_Visible_Status(!is_led_visible);
                    } else {
                        call_Update_Led_Visible_Status(!is_led_visible);
                    }
                }
            });

            tv_full_name.setText(UtilsPreferences.getString(getActivity(), Constant.full_name));
            full_name = tv_full_name.getText().toString().trim();

            Profile_image = getActivity().getResources().getString(R.string.server_url)
                    + getActivity().getResources().getString(R.string.profile_image)
                    + UtilsPreferences.getString(getActivity(), Constant.profile_image);

            Glide.with(getActivity())
                    .load(Profile_image)
                    .placeholder(R.drawable.user)
                    .dontAnimate()
                    .into(iv_profile);

                /*Picasso.with(getActivity())
                        .load(UtilsPreferences.getString(getActivity(), Constant.profile_image))
                        .placeholder(R.drawable.user)
                        .into(iv_profile);*/

            if (UtilsPreferences.getString(getActivity(), Constant.registration_date) != null) { // condition for registration date isNull
                tv_joined_date.setText(getString(R.string.joined_text) + " " + UtilsCommon.ConvertStringIntoDateSetting(UtilsPreferences.getString(getActivity(), Constant.registration_date)));
            }

            if (UtilsPreferences.getString(getActivity(), Constant.user_exp_date) != null) {  // condition for user expiry date isNull
                tv_membership_exp_date.setText(getString(R.string.membership_expiry_text) + " " + UtilsCommon.ConvertStringIntoDateSetting(UtilsPreferences.getString(getActivity(), Constant.user_exp_date)));
            }

            UtilsPermission.checkCamera(getActivity());
            UtilsPermission.checkWriteExternalStorage(getActivity());

            tv_goyopair = (TextView) view.findViewById(R.id.tv_goyopair);

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {  // condition for live user

                if (UtilsPreferences.getString(getActivity(), Constant.GOYOHRStatus) != null) {
                    if (UtilsPreferences.getString(getActivity(), Constant.GOYOHRStatus).equalsIgnoreCase("Bundled")) { // condition for band connection
                        tv_goyopair.setText(R.string.goyohr_unpair_sub);
                    } else {
                        tv_goyopair.setText(R.string.goyohr_pair_sub);
                    }
                } else {
                    tv_goyopair.setText(R.string.goyohr_pair_sub);
                }
            } else {
                tv_goyopair.setText(R.string.goyohr_pair_sub);
            }

            ll_sync_progress.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            onGetUserSyncData = this;
            ll_back.setOnClickListener(this);
            ll_logout.setOnClickListener(this);
            ll_height_weight.setOnClickListener(this);
            ll_invite.setOnClickListener(this);
            ll_about.setOnClickListener(this);
            ll_sync.setOnClickListener(this);

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                ll_leaderboard.setOnClickListener(this);
            } else {
                tg_leaderboard.setChecked(false);
                tg_leaderboard.setEnabled(false);
                ll_leaderboard.setEnabled(false);
                ll_leaderboard.setAlpha(.5f);
            }

            iv_edit_name.setOnClickListener(this);
            iv_profile.setOnClickListener(this);
            ll_personal_detail.setOnClickListener(this);
            ll_goyohr.setOnClickListener(this);
            ll_buygoyo.setOnClickListener(this);
            onUpdateUserDetailListener = this;
            onUpdateProfileImageListener = this;
            onUpdateLedVisibleStatus = this;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // sync animation
    public void scheduleThread() {
        handler.postDelayed(new Runnable() {
            public void run() {
                runnable = this;
                if (UtilsCommon.checkBlutoothConnectivity()) {

                    UtilsCommon.expand(ll_sync_progress);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 200ms
                            UtilsCommon.collapse(ll_sync_progress);
                        }
                    }, Constant.PostDelaySub);
                }
                handler.postDelayed(runnable, Constant.PostDelayMain);
            }
        }, Constant.PostDelayMain);
    }

    // handle click event listener
    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        switch (view.getId()) {
            case R.id.ll_logout:
                /*if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                    openEditDialog("logout");
                } else {
                    doLogout();
                }*/
                doLogout();
                break;
            case R.id.ll_height_weight:
                HeightWeightActivityIntent();
                break;
            case R.id.ll_invite:
                if (UtilsPermission.checkContact(getActivity())) {
                    addFragment(InviteFragment.newInstance(null), "InviteFragment");
                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msg_contact_permission), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ll_about:
                addFragment(AboutFragment.newInstance(null), "AboutFragment");
                break;
            case R.id.iv_edit_name:
                openEditDialog("edit");
                break;
            case R.id.iv_profile:
                if (UtilsPermission.checkWriteExternalStorage(getActivity())) {
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msg_image_library_permission), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ll_sync:
                openSyncActivity();
                break;
            case R.id.ll_personal_detail:
                openAllsetActivity();
                break;
            case R.id.ll_goyohr:
                if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                    String bundle = UtilsPreferences.getString(getActivity(), Constant.GOYOHRStatus);
                    if (bundle != null && bundle.length() > 0) {
                        if (UtilsPreferences.getString(getActivity(), Constant.GOYOHRStatus).equalsIgnoreCase("Bundled")) {
                            openEditDialog("unpair");
                        } else {
                            ScanActivityIntent();
                        }
                    } else {
                        ScanActivityIntent();
                    }
                } else {
                    ScanActivityIntent();
                }
                break;
            case R.id.ll_buygoyo:
                buyWatch();
                break;
            case R.id.ll_back:
                getActivity().onBackPressed();
                break;
        }
    }

    // intent to url for buy band
    public void buyWatch() {
        String url = "http://goyo.lk/Shop/index.php?route=product/product&product_id=42";

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    // open scan screen
    public void ScanActivityIntent() {
        Intent i = new Intent(getContext(), ScanDeviceActivity.class);
        i.putExtra("isFrom", "SettingsFragment");
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // open height and weight screen
    public void HeightWeightActivityIntent() {
        Intent i = new Intent(getContext(), RegistrationActivity_Height_Weight.class);
        i.putExtra("isFrom", "SettingsFragment");
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // unpaired band process
    public void unbundling() {
        if (BleSharedPreferences.getInstance().getIsBind()) {
            if (ProtocolUtils.getInstance().isAvailable() != ProtocolUtils.SUCCESS) {//To determine whether a device is connected
                ProtocolUtils.getInstance().setBindMode(Protocol.SYS_MODE_SET_NOBIND);
                Calendar mCalendar1 = Calendar.getInstance();
                int year = mCalendar1.get(Calendar.YEAR);
                int month = mCalendar1.get(Calendar.MONTH);
                int day = mCalendar1.get(Calendar.DAY_OF_MONTH);
                ProtocolUtils.getInstance().enforceUnBind(new Date(year, month, day));
            } else {
                //If the device is connected, use the following method to solve tie
                ProtocolUtils.getInstance().setBindMode(Protocol.SYS_MODE_SET_NOBIND);
                ProtocolUtils.getInstance().setUnBind();
            }
            tv_goyopair.setText(R.string.goyohr_pair_sub);
            Toast.makeText(getActivity(), getString(R.string.toast_unpair_successfully), Toast.LENGTH_SHORT).show();
            UtilsPreferences.setString(getActivity(), Constant.GOYOHRStatus, "Unbundled");
            Globals.clearData();
            //UtilsPreferences.saveInLevelInformationSharedPrefrences(getActivity(), Constant.Key_level_info, null);
        } else {
            ScanActivityIntent();
            /*Intent intent = new Intent(getActivity(), ScanDeviceActivity.class);
            startActivity(intent);*/
        }
    }

    // open user detail screen
    private void openAllsetActivity() {
        Intent intent = new Intent(getActivity(), RegistrationActivity_Allset.class);
        intent.putExtra(Constant.isFromSetting, true);// here it will be true as it is intent from setting screen
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // open goyo about screen
    public void openSyncActivity() {
        Intent intent = new Intent(getActivity(), GetStartedActivity.class);
        intent.putExtra(Constant.isFromSetting, true);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // logout from app
    public void doLogout() {
        LogoutCall();
        String deviceToken = UtilsPreferences.getString(getActivity(), Constant.devicegcmid);
        Globals.clearData();
        startActivity(new Intent(getActivity(), LoginActivity.class));
        UtilsPreferences.clearPreferences(getActivity());
        UtilsPreferences.setString(getActivity(), Constant.devicegcmid, deviceToken);
        UtilsPreferences.setBoolean(getActivity(), Constant.tutorial_flag, true);
        UtilsPreferences.setBoolean(getActivity(), Constant.first_launch, false);
        if (BleSharedPreferences.getInstance().getIsBind()) {
            UtilsPreferences.setString(getActivity(), Constant.GOYOHRStatus, "Bundled");
        } else {
            UtilsPreferences.setString(getActivity(), Constant.GOYOHRStatus, "Unbundled");
        }
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        getActivity().finish();

        // clear challenges and levelinformation prefrences
        UtilsPreferences.clearKeyPreferences(getActivity(), Constant.Key_inProgressList);
        UtilsPreferences.clearKeyPreferences(getActivity(), Constant.Key_level_info);

        if (LoginManager.getInstance() != null)
            LoginManager.getInstance().logOut();
    }

    // api call for logout
    public void LogoutCall() {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));

            if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                Logout reg = new Logout(getActivity(), data);
                reg.execute();
            } else {
                //Toast.makeText(getActivity(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // alert dialog for unpair band
    public void openEditDialog(final String msg) {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.setting_edit_name_dialog);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

        final EditText edt_name = (EditText) alertDialog.findViewById(R.id.et_name);
        TextView tv_cancel = (TextView) alertDialog.findViewById(R.id.tv_cancel);
        TextView tv_change = (TextView) alertDialog.findViewById(R.id.tv_change);
        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        TextView tv_wrn_text = (TextView) alertDialog.findViewById(R.id.tv_wrn_text);
        edt_name.setTypeface(setFont(getActivity(), R.string.app_bold));//

        if (msg.equalsIgnoreCase("unpair") || msg.equalsIgnoreCase("logout")) {
            edt_name.setVisibility(View.GONE);
            tv_popup_title.setText(R.string.sync_warning);
            tv_wrn_text.setVisibility(View.VISIBLE);
            tv_change.setText(getString(R.string.action_ok));
            tv_popup_title.setText(getString(R.string.warning_lbl));
        } else {
            edt_name.setVisibility(View.VISIBLE);
            edt_name.setText(tv_full_name.getText().toString().trim());
            tv_wrn_text.setVisibility(View.GONE);
        }

        alertDialog.show();
        tv_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                alertDialog.dismiss();
            }
        });
        tv_change.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (msg.equalsIgnoreCase("unpair")) {
                    unbundling();
                    alertDialog.dismiss();
                } else if (msg.equalsIgnoreCase("logout")) {
                    unbundling();
                    alertDialog.dismiss();
                    doLogout();
                } else {
                    if (edt_name.getText().length() > 0) {
                        full_name = edt_name.getText().toString().trim();
                        updateUserDetail(null);
                        getActivity().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                        alertDialog.dismiss();
                    } else {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.err_msg_please_enter_name), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    // update user detail api call

    /**
     * @param str get the profile image path
     */
    public void updateUserDetail(String str) {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            try {
                JSONObject data = new JSONObject();
                data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
                data.put(Constant.full_name, full_name);
                //data.put(Constant.other_names, full_name);
                data.put(Constant.profile_image, str);
                data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));

                UpdateUserDetailCall task = new UpdateUserDetailCall(getActivity(), onUpdateUserDetailListener, data, true);
                task.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // upload profile image api call

    /**
     * @param file get the file of profile image
     */
    public void UpdateProfileImage(File file) {

        try {
            JSONObject data = new JSONObject();
            data.put(Constant.profile_image, file);
            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
            if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                UpdateProfileImageCall task = new UpdateProfileImageCall(getActivity(), onUpdateProfileImageListener, file, true);
                task.execute();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // process for select image from camera or gallery
    private void selectImage() {

        final CharSequence[] options = {"Camera", "Library"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].toString().equalsIgnoreCase("Camera")) {
                    if (UtilsPermission.checkCamera(getActivity())) {
                       /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, 1);*/

                        //For nugat
                        /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                        Uri photoURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", f);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(intent, 1);*/

                        EasyImage.openCamera(SettingsFragment.this, 1);

                        //EasyImage.openChooserWithGallery(getActivity(), "Select", 0);

                    } else {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.msg_camera_permission), Toast.LENGTH_SHORT).show();
                    }
                } else if (options[item].toString().equalsIgnoreCase("Library")) {
                    if (UtilsPermission.checkWriteExternalStorage(getActivity())) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, 2);
                    } else {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.msg_image_library_permission), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 2) { // call when request code is 2
                try {
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();
                    Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                    iv_profile.setImageBitmap(thumbnail);
                    OutputStream outFile = null;
                    File file = new File(android.os.Environment.getExternalStorageDirectory() + File.separator, String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        file.createNewFile();
                        outFile = new FileOutputStream(file);
                        thumbnail.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    UpdateProfileImage(file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
                    @Override
                    public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                        //Some error handling
                    }

                    @Override
                    public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                        //Handle the image for crop
                        Bitmap bitmap;
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                        bitmapOptions.inSampleSize = 8;

                        bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(),
                                bitmapOptions);

                        iv_profile.setImageBitmap(bitmap);

                        /*Picasso.with(getActivity())
                                .load(imageFile)
                                .placeholder(R.drawable.user)
                                .into(iv_profile);*/
                        UpdateProfileImage(imageFile);
                    }

                    @Override
                    public void onCanceled(EasyImage.ImageSource source, int type) {
                        //Cancel handling, you might wanna remove taken photo if it was canceled
                        if (source == EasyImage.ImageSource.CAMERA) {
                            File photoFile = EasyImage.lastlyTakenButCanceledPhoto(getActivity());
                            if (photoFile != null) photoFile.delete();
                        }
                    }
                });
            }

        }
    }

    // open required screen

    /**
     * @param fragment add fragment
     */
    void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
        fragmentTransaction.add(R.id.frame_main, fragment, null);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    // success listener of UpdateUserDetailCall
    @Override
    public void onSucceedToUpdateUserDetail(RegisterModel obj) {
        if (isAdded()) {
            tv_full_name.setText(obj.getFirst_name() + " " + obj.getLast_name());
            UtilsPreferences.setString(getActivity(), Constant.full_name, obj.getFirst_name() + " " + obj.getLast_name());
            UtilsPreferences.setString(getActivity(), Constant.profile_image, obj.getProfile_image());

            Profile_image = getActivity().getResources().getString(R.string.server_url)
                    + getActivity().getResources().getString(R.string.profile_image)
                    + UtilsPreferences.getString(getActivity(), Constant.profile_image);

            Glide.with(getActivity())
                    .load(Profile_image)
                    .placeholder(R.drawable.user)
                    .dontAnimate()
                    .into(iv_profile_hide);
        }
    }

    // failed listener of UpdateUserDetailCall
    @Override
    public void onFaildToUpdateUserDetail(String error_msg) {
        if (isAdded()) {
            Toast.makeText(getActivity(), error_msg, Toast.LENGTH_SHORT).show();
        }
    }

    // success listener of UpdateProfileImageCall
    @Override
    public void onSucceedToUpdateProfileImage(String msg) {
        if (isAdded()) {
            Profile_image = msg;
            updateUserDetail(msg);
        }
    }

    // failed listener of UpdateProfileImageCall
    @Override
    public void onFaildToUpdateProfileImage(String error_msg) {
        if (isAdded()) {
            Toast.makeText(getActivity(), error_msg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        handler.removeCallbacks(runnable); // remove the handler
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Setting);
    }

    private void recordScreenViews() {
        // [START set_current_screen]
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "Setting Screen", null /* class override */);
        // [END set_current_screen]
    }

    // api call for get user sync data
    private void callGetUserSyncData() {
        JSONObject data = new JSONObject();
        try {
            data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            get_user_sync_data get_user_sync_call = new get_user_sync_data(getActivity(), onGetUserSyncData, data, true);
            get_user_sync_call.execute();
        }
    }

    // success listener of get_user_sync_data
    @Override
    public void onSucceedToUserSyncData(GetUserSyncDataModel obj) {
        if (isAdded()) {
            ll_policy_number.setVisibility(View.VISIBLE);
            if (obj.is_policy_user == 0) {
                tv_policy_no.setVisibility(View.GONE);
                tv_policy_expiry_date.setText(getString(R.string.expiry_date_text_3));
            } else {
                if (obj.data.sync_flag.equalsIgnoreCase("1")) {
                    tv_policy_no.setVisibility(View.GONE);
                    tv_policy_expiry_date.setText(getString(R.string.expiry_date_text_onfail));
                } else {
                    tv_policy_no.setVisibility(View.VISIBLE);
                    if (obj.data.proposal_no != null) {
                        String proposal_no = getString(R.string.lbl_policy_no_prefix) + getPolicyNumber(obj.data.proposal_no);
                        tv_policy_no.setText(proposal_no);
                    }
                    String str_exp_date_value = getString(R.string.expired_date_text) + ": " + UtilsCommon.ConvertStringIntoDateSetting(UtilsPreferences.getString(getActivity(), Constant.user_exp_date));
                    tv_policy_expiry_date.setText(str_exp_date_value);
                }
            }
        }
    }

    // failed listener of get_user_sync_data
    @Override
    public void onFaildToUserSyncData(String error_msg) {
        if (isAdded()) {
            ll_policy_number.setVisibility(View.VISIBLE);
            tv_policy_no.setVisibility(View.GONE);
            tv_policy_expiry_date.setText(getString(R.string.expiry_date_text_onfail));
        }
    }

    // get policy number
    private String getPolicyNumber(String str_policy_no) {
        String final_result = "";
        // format 4 digit - 3 digit - remaining digit
        try {
            final_result = str_policy_no.substring(0, 4);
            final_result = final_result + "-" + str_policy_no.substring(4, 7);
            final_result = final_result + "-" + str_policy_no.substring(7, str_policy_no.length());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return final_result;
    }

    // api call for updating leaderboard visibility of logged in user
    private void call_Update_Led_Visible_Status(boolean isVisible) {
        JSONObject data = new JSONObject();
        try {
            data.put(Constant.user_id, UtilsPreferences.getString(getActivity(), Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getActivity(), Constant.accesstoken));
            data.put(Constant.status, (isVisible) ? "1" : "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            Update_Led_Visible_Status update_led_visible_status = new Update_Led_Visible_Status(getActivity(), onUpdateLedVisibleStatus, data, true);
            update_led_visible_status.execute();
        }
    }

    // success listener of Update_Led_Visible_Status
    @Override
    public void onSucceedToUpdateLedVisibleStatus(String msg) {
        if (isAdded()) {
            Toast.makeText(getActivity(), "" + msg, Toast.LENGTH_SHORT).show();

            if (is_led_visible) {
                is_led_visible = false;
                tg_leaderboard.setChecked(false);
                hidemestatus.setText(Constant.hidemeoff);
                UtilsPreferences.setString(getContext(), Constant.led_visible_status, "0");

            } else {
                is_led_visible = true;
                tg_leaderboard.setChecked(true);
                hidemestatus.setText(Constant.hidemeon);
                UtilsPreferences.setString(getContext(), Constant.led_visible_status, "1");
            }
        }
    }

    // failed listener of Update_Led_Visible_Status
    @Override
    public void onFaildToUpdateLedVisibleStatus(String error_msg) {
        if (isAdded()) {
            Toast.makeText(getActivity(), "" + error_msg, Toast.LENGTH_SHORT).show();

            // reverting
            tg_leaderboard.setChecked(is_led_visible);
        }
    }
}
