package com.differenzsystem.goyog.dashboard.home.heartRate;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.differenzsystem.goyog.FitnessBandTracker.syncdevice;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.GetFitnessDataFromValidicCall;
import com.differenzsystem.goyog.api.GetFitnessDataFromValidicCall.OnGetFitnessFromValidicListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.model.CategoryModel;
import com.differenzsystem.goyog.model.ValidicFitnessModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.veryfit.multi.nativedatabase.HealthHeartRate;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.text.DateFormat.getTimeInstance;


public class HeartRateMonthWiseFragment extends Fragment implements OnGetFitnessFromValidicListener, SwipeRefreshLayout.OnRefreshListener, syncdevice.Onsyncdevice {
    View rootview;
    CombinedChart mChart;
    TextView tv_heart_rate, tv_date, tv_total_heart_rate, tv_week1, tv_week2, tv_week3, tv_week4, tv_week5;
    ArrayList<HashMap<String, String>> lst_month_heart_rate;
    ArrayList<HashMap<String, String>> whole_lst_month_calory;
    OnGetFitnessFromValidicListener onGetFitnessFromValidicListener;
    int total_heart_rate = 0, dataCount = 0, callCount = 0;
    List<String> weekDay = new ArrayList<>();
    public GregorianCalendar cal_month;
    long statTime = 0;
    long endTime = 0;
    String TAG = "HeartRateMonthWiseFragment";
    SwipeRefreshLayout swipeRefreshLayout;
    syncdevice.Onsyncdevice onsyncdevice;

    public HeartRateMonthWiseFragment() {
        // Required empty public constructor
    }

    // create new instance of HeartRateMonthWiseFragment
    public static HeartRateMonthWiseFragment newInstance(Bundle bundle) {
        HeartRateMonthWiseFragment fragment = new HeartRateMonthWiseFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        onsyncdevice = this;
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.heart_rate_month_wise_fragment, container, false);
            initcomopnets(rootview);
        } else
            container.removeView(rootview);
        return rootview;

    }

    // initialize all controls define in xml layout

    /**
     * @param rootview layout view
     */
    private void initcomopnets(View rootview) {
        try {
            swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.swipe_view);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(
                    R.color.colorPrimary);
            mChart = (CombinedChart) rootview.findViewById(R.id.chart_calories);

            lst_month_heart_rate = new ArrayList<>();
            whole_lst_month_calory = new ArrayList<>();

            tv_heart_rate = (TextView) rootview.findViewById(R.id.tv_avg_heart_rate);
            tv_date = (TextView) rootview.findViewById(R.id.tv_date);
            tv_total_heart_rate = (TextView) rootview.findViewById(R.id.tv_total_heart_rate);
            tv_week1 = (TextView) rootview.findViewById(R.id.tv_week1);
            tv_week2 = (TextView) rootview.findViewById(R.id.tv_week2);
            tv_week3 = (TextView) rootview.findViewById(R.id.tv_week3);
            tv_week4 = (TextView) rootview.findViewById(R.id.tv_week4);
            tv_week5 = (TextView) rootview.findViewById(R.id.tv_week5);

            cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
            onGetFitnessFromValidicListener = this;
            setCurrentWeekColor();

            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) { // condition of live user
                //if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                //    getMonthwiseData();

                // condition for band connection
                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    //getMonthData();
                    getMonthDataCustomize();
                } else {
                    //TODO:get Date from device
                    getDeviceHeartRateData();
                }
            } else {
                swipeRefreshLayout.setEnabled(false);
                setupDemoUserMonth();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // setup data for demo user
    private void setupDemoUserMonth() {
        int weekNo1 = DashboardActivity.mCurrentweek;
        for (int i = 0; i < 5; i++) {
            HashMap<String, String> temp = new HashMap<>();
            temp.put(Constant.time, new Date().toString());
            int sum = 0;
            if (i < weekNo1) {
                temp.put(Constant.heart_rate, "0");
                lst_month_heart_rate.add(temp);
            } else {
                break;
            }
        }
        tv_total_heart_rate.setText("0 BPM");
        UtilsCommon.destroyProgressBar();
        monthWiseGraph(mChart, tv_heart_rate, tv_date);

    }

    public void getMonthData() {
        try {
            int w1 = 0, w2 = 0, w3 = 0, w4 = 0, w5 = 0;

            Calendar mCalendar = Calendar.getInstance();
                   /* int year = mCalendar.get(Calendar.YEAR);
                    int month=mCalendar.get(Calendar.MONTH);*/
            int Today = mCalendar.get(Calendar.DAY_OF_MONTH);

            int year = 0, month = 0, day = 0;
            List<HealthHeartRate> monthHealthHeartRate = ProtocolUtils.getInstance().getMonthHeartRate(0);
            if (monthHealthHeartRate != null && monthHealthHeartRate.size() > 0) {
                int weekNo1 = DashboardActivity.mCurrentweek;
                for (int i = 1; i <= 5; i++) {
                    if (i <= weekNo1) {
                        Date startDate = monthHealthHeartRate.get(0).getDate();
                        int totalcal = 0;
                        HashMap<String, String> temp = new HashMap<>();

                        for (int ii = 0; ii < monthHealthHeartRate.size(); ii++) {
                            temp.put(Constant.time, UtilsCommon.ConvertStringIntoDateWeekName(startDate.toString()));
                            year = monthHealthHeartRate.get(i).getYear();
                            month = monthHealthHeartRate.get(i).getMonth();
                            day = monthHealthHeartRate.get(ii).getDay();

                            //totalcal = totalcal + monthHealthHeartRate.get(ii).getSilentHeart();
                            Log.e("total calaries", String.valueOf(totalcal));

                            if (day <= Today) {
                                if (i == 1) {
                                    if (day >= 1 && day <= 7) {
                                        totalcal = totalcal + monthHealthHeartRate.get(ii).getSilentHeart();
                                        if (day == 7) {
                                            w1 = w1 + monthHealthHeartRate.get(ii).getSilentHeart();
                                            temp.put(Constant.heart_rate, String.valueOf(w1));
                                            break;
                                        } else {
                                            w1 = w1 + monthHealthHeartRate.get(ii).getSilentHeart();
                                            temp.put(Constant.heart_rate, String.valueOf(w1));
                                        }
                                    }
                                } else if (i == 2) {
                                    if (day >= 8 && day <= 14) {
                                        totalcal = totalcal + monthHealthHeartRate.get(ii).getSilentHeart();
                                        if (day == 14) {
                                            w2 = w2 + monthHealthHeartRate.get(ii).getSilentHeart();
                                            temp.put(Constant.heart_rate, String.valueOf(w2));
                                            break;
                                        } else {
                                            w2 = w2 + monthHealthHeartRate.get(ii).getSilentHeart();
                                            temp.put(Constant.heart_rate, String.valueOf(w2));
                                        }
                                    }
                                } else if (i == 3) {
                                    if (day >= 15 && day <= 21) {
                                        totalcal = totalcal + monthHealthHeartRate.get(ii).getSilentHeart();
                                        if (day == 21) {
                                            w3 = w3 + monthHealthHeartRate.get(ii).getSilentHeart();
                                            temp.put(Constant.heart_rate, String.valueOf(w3));
                                            break;
                                        } else {
                                            w3 = w3 + monthHealthHeartRate.get(ii).getSilentHeart();
                                            temp.put(Constant.heart_rate, String.valueOf(w3));
                                        }
                                    }
                                } else if (i == 4) {
                                    if (day >= 22 && day <= 28) {
                                        totalcal = totalcal + monthHealthHeartRate.get(ii).getSilentHeart();
                                        if (day == 28) {
                                            w4 = w4 + monthHealthHeartRate.get(ii).getSilentHeart();
                                            temp.put(Constant.heart_rate, String.valueOf(w4));
                                            break;
                                        } else {
                                            w4 = w4 + monthHealthHeartRate.get(ii).getSilentHeart();
                                            temp.put(Constant.heart_rate, String.valueOf(w4));
                                        }
                                    }
                                } else if (i == 5) {
                                    if (day > 29) {
                                        totalcal = totalcal + monthHealthHeartRate.get(ii).getSilentHeart();
                                        w5 = w5 + monthHealthHeartRate.get(ii).getSilentHeart();
                                        temp.put(Constant.heart_rate, String.valueOf(w5));
                                    }
                                }
                                //temp.put(Constant.step, String.valueOf(monthHealthSprort.get(ii).getTotalStepCount()));
                            } /*else {
                                        temp.put(Constant.step, "-1");
                                        break;
                                    }*/
                        }
                        lst_month_heart_rate.add(temp);
                        callCount++;
                        total_heart_rate = total_heart_rate + totalcal;
                        //tv_total_heart_rate.setText(String.valueOf(total_heart_rate / (dataCount == 0 ? 1 : dataCount)) + " BPM");
                        tv_total_heart_rate.setText(String.valueOf(total_heart_rate + " BPM"));
                    }
                    //tv_total_step.setText(String.valueOf(total_step));
                    Debugger.debugE("month_heart_rate..", lst_month_heart_rate.size() + " " + lst_month_heart_rate.toString());

                }
                if (lst_month_heart_rate.size() == callCount) {
                    tv_total_heart_rate.setText(String.valueOf(total_heart_rate / (dataCount == 0 ? 1 : dataCount)) + " BPM");
                    monthWiseGraph(mChart, tv_heart_rate, tv_date);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // get data from band for heart rate and populate on screen
    public void getMonthDataCustomize() {
        List<HealthHeartRate> monthHealthHeartRate = ProtocolUtils.getInstance().getMonthHeartRate(0);
        Date startDate = monthHealthHeartRate.get(0).getDate();
        //int[] a=new int[31];

        int[] intArray = new int[31];

        if (monthHealthHeartRate != null && monthHealthHeartRate.size() > 0) {
            for (int i = 0; i < monthHealthHeartRate.size(); i++) {
                intArray[i] = monthHealthHeartRate.get(i).getSilentHeart();
            }
        }

        int x = 7;  // chunk size
        int len = intArray.length;
        int counter = 0;
        int[][] newArray = new int[len][];

        for (int i = 0; i < len - x + 1; i += x)
            newArray[counter++] = Arrays.copyOfRange(intArray, i, i + x);

        if (len % x != 0)
            newArray[counter] = Arrays.copyOfRange(intArray, len - len % x, len);

        Log.e("Hours data:", newArray.toString());
        int weekNo1 = DashboardActivity.mCurrentweek;

        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");//yyyy-MM-dd
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        for (int i = 0; i < 5; i++) {
            HashMap<String, String> temp = new HashMap<>();
            temp.put(Constant.time, UtilsCommon.getDateDayName(startDate.toString()));
            int sum = 0;
            int count = 0;
            if (i < weekNo1) {
                for (int j = 0; j < newArray[i].length; j++) {
                    sum = sum + newArray[i][j];
                }
                if (i + 1 == weekNo1)
                    sum = sum / dayOfWeek;
                else
                    sum = sum / 7;

                temp.put(Constant.heart_rate, String.valueOf(sum));
                total_heart_rate = total_heart_rate + sum;
                lst_month_heart_rate.add(temp);
            } else {
                break;
            }
        }
        total_heart_rate = total_heart_rate / weekNo1; //change
        tv_total_heart_rate.setText(String.valueOf(total_heart_rate + " BPM"));

        Debugger.debugE("month_heart_rate..", lst_month_heart_rate.size() + " " + lst_month_heart_rate.toString());

        if (lst_month_heart_rate.size() == weekNo1) {
            monthWiseGraph(mChart, tv_heart_rate, tv_date);
        }
    }

    private void getDeviceHeartRateData() {
        try {
            if (ConnectionDetector.internetCheck(getContext())) {
                buildFitnessClient();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void buildFitnessClient() {
        if (HomeFragment.mClient != null) {

            getCurrentWeekDay();
        }
    }

    // get the day of the current week and bind the data
    public List<String> getCurrentWeekDay() {

        SimpleDateFormat dayDateFormate = new SimpleDateFormat("dd MMMM");
        if (weekDay.size() > 0) weekDay.clear();
        cal_month.set(Calendar.DAY_OF_MONTH, cal_month.getActualMinimum(Calendar.DAY_OF_MONTH));

        int currentWeek = cal_month.get(Calendar.WEEK_OF_YEAR);
        int year = cal_month.get(Calendar.YEAR);
        cal_month.set(Calendar.HOUR_OF_DAY, 0);
        int daysInMonth = cal_month.getActualMaximum(Calendar.DAY_OF_MONTH);
        String[] days = new String[daysInMonth];
        int weekcount = 0;
        Calendar date = Calendar.getInstance();
        int weekNo1 = DashboardActivity.mCurrentweek;

        for (int i = 0; i < daysInMonth; i++) {
            if (lst_month_heart_rate.size() <= weekNo1) {
                days[i] = dayDateFormate.format(cal_month.getTime());
                weekDay.add(dayDateFormate.format(cal_month.getTime()));
                cal_month.add(GregorianCalendar.DAY_OF_WEEK, 1);
                cal_month.set(Calendar.HOUR_OF_DAY, 0);
                HashMap<String, String> temp = new HashMap<>();

                temp.put(Constant.time, weekDay.get(i));

                temp.put(Constant.heart_rate, "0");

                whole_lst_month_calory.add(temp);
                if (weekcount == 0) {
                    lst_month_heart_rate.add(temp);
                }
                weekcount++;
                if (weekcount >= 7) {
                    weekcount = 0;
                }
            } else {
                days[i] = dayDateFormate.format(cal_month.getTime());
                weekDay.add(dayDateFormate.format(cal_month.getTime()));
                cal_month.add(GregorianCalendar.DAY_OF_WEEK, 1);
                cal_month.set(Calendar.HOUR_OF_DAY, 0);
                HashMap<String, String> temp = new HashMap<>();

                temp.put(Constant.time, weekDay.get(i));

                temp.put(Constant.heart_rate, "-1");

                whole_lst_month_calory.add(temp);
                if (weekcount == 0) {
                    lst_month_heart_rate.add(temp);
                }
                weekcount++;
                if (weekcount >= 7) {
                    weekcount = 0;
                }
            }

        }
        new HeartRateCall().execute();
        return weekDay;
    }

    // swipe to refresh call process
    @Override
    public void onRefresh() {
        callCount = 0;
        total_heart_rate = 0;
        lst_month_heart_rate = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        getAndSetData();

    }

    // success listener of syncdevice
    @Override
    public void onSucceedsyncdevice() {
        swipeRefreshLayout.setRefreshing(false);
        //getMonthData();
        getMonthDataCustomize();
    }

   /* @Override
    public void onFailedsyncdevice() {

    }*/

    private void getAndSetData() {
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            getMonthwiseData();
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            syncdevice s = new syncdevice(getContext(), onsyncdevice);
            s.syncdata();
        } else {
            new HeartRateCall().execute();
        }
    }

    // HeartRateCall api
    private class HeartRateCall extends AsyncTask<Void, Void, Void> {
        String heartRateData;

        protected Void doInBackground(Void... params) {

            try {
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(HomeFragment.mClient, readRequest).await(1, TimeUnit.MINUTES);
                heartRateData = readData(dataReadResult);
                Debugger.debugE("heartRateData", heartRateData + "");
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                //  UtilsCommon.destroyProgressBar();
                tv_total_heart_rate.setText(total_heart_rate + " BPM");

                if (whole_lst_month_calory.size() > 0) {
                    int weekcount = 0;
                    int cal = 0;
                    for (int i = 0; i < whole_lst_month_calory.size(); i++) {
                        if (whole_lst_month_calory.get(i).get(Constant.heart_rate).equalsIgnoreCase(""))
                            cal += 0;
                        else
                            cal += Math.round(Float.parseFloat(whole_lst_month_calory.get(i).get(Constant.heart_rate)));

                        weekcount++;
                        if (weekcount > 6) {
                            switch (i) {
                                case 6:
                                    lst_month_heart_rate.get(0).put(Constant.heart_rate, cal / 7 + "");
                                    break;
                                case 13:
                                    lst_month_heart_rate.get(1).put(Constant.heart_rate, cal / 7 + "");
                                    break;
                                case 20:
                                    lst_month_heart_rate.get(2).put(Constant.heart_rate, cal / 7 + "");
                                    break;
                                case 27:
                                    lst_month_heart_rate.get(3).put(Constant.heart_rate, cal / 7 + "");
                                    break;
                            }
                            weekcount = 0;
                            cal = 0;

                        }
                    }
                    if (whole_lst_month_calory.size() % 7 > 0) {
                        lst_month_heart_rate.get(4).put(Constant.heart_rate, cal / 7 + "");
                    }


                }
                monthWiseGraph(mChart, tv_heart_rate, tv_date);
                // dayWiseGraph(mChart, tv_calories, tv_date);
                Debugger.debugE("heartRateData", heartRateData + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            {
                Calendar calendar = getCalendarForNow();
                calendar.set(Calendar.DAY_OF_MONTH,
                        calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                setTimeToBeginningOfDay(calendar);
                statTime = calendar.getTimeInMillis();
            }

            {
                Calendar calendar = getCalendarForNow();
                calendar.set(Calendar.DAY_OF_MONTH,
                        calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                setTimeToEndofDay(calendar);
                endTime = calendar.getTimeInMillis();
            }

            java.text.DateFormat dateFormat = DateFormat.getDateInstance();
            Log.e("History", "Range Start: " + dateFormat.format(statTime));
            Log.e("History", "Range End: " + dateFormat.format(endTime));

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(statTime, endTime, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    private static Calendar getCalendarForNow() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        return calendar;
    }

    private static void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private static void setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

    public String readData(DataReadResult dataReadResult) {
        String value = "";
        total_heart_rate = 0;
        try {
            if (dataReadResult.getBuckets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getBuckets().size());
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                        total_heart_rate += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));

                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                    total_heart_rate += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }

                HashMap<String, String> temp = new HashMap<>();
                String sDate = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00").format(dp.getStartTime(TimeUnit.MILLISECONDS));
                temp.put(Constant.time, UtilsCommon.ConvertStringIntoDateWeekName(sDate));

                temp.put(Constant.heart_rate, value);

                for (int i = 0; i < whole_lst_month_calory.size(); i++) {
                    if (whole_lst_month_calory.get(i).get(Constant.time).equalsIgnoreCase(UtilsCommon.ConvertStringIntoDateWeekName(sDate))) {
                        whole_lst_month_calory.set(i, temp);
                    }

                }
                //lst_week_calory.add(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    // api call for GetFitnessDataFromValidic
    public void getMonthwiseData() {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            //UtilsCommon.showProgressDialog(getActivity());
            int weekNo1 = DashboardActivity.mCurrentweek;
            int day = 1;
            for (int i = 1; i <= 5; i++) {
                if (i <= weekNo1) {
                    GetFitnessDataFromValidicCall task = new GetFitnessDataFromValidicCall(getActivity(), onGetFitnessFromValidicListener, false, UtilsCommon.AddSevenDayInDate(day), UtilsCommon.AddSevenDayInDate(day + 7), Constant.month);
                    task.execute();
                    day += 7;
                    callCount++;
                } else {
                    break;
                }
            }
        } else {
           // Toast.makeText(getActivity(), getActivity().getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * @param mChart      get the chart for displaying Heartrate progress
     * @param tv_heart_rate get textview of Heartrate
     * @param tv_date     get textview of date
     */
    private void monthWiseGraph(CombinedChart mChart, final TextView tv_heart_rate, final TextView tv_date) {
        try {
            swipeRefreshLayout.setRefreshing(false);
            mChart.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setHighlightPerDragEnabled(false);
            mChart.setPinchZoom(false);
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.setDescription("");
            mChart.setExtraOffsets(30, 30, 30, 30);
            mChart.setTouchEnabled(true);
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setPinchZoom(false);

            Legend l = mChart.getLegend();
            l.setEnabled(false);
            //l.setForm(LegendForm.LINE);

            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<CategoryModel> dataList = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            Calendar c = Calendar.getInstance();
            SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
            String month_name = month_date.format(c.getTime());

            for (int i = 0; i < 5; i++) {
                if (i < lst_month_heart_rate.size()) {
                    HashMap<String, String> temp = lst_month_heart_rate.get(i);

                    double cal = Double.parseDouble(temp.get(Constant.heart_rate));
                    entries.add(new Entry(i, (float) cal));

                    /*dataList.add(new CategoryModel((float) cal, temp.get(time)));
                    labels.add(temp.get(time));*/

                    if (i == 4) {
                        dataList.add(new CategoryModel((float) cal, UtilsCommon.MonthArray[i] + "-" + cal_month.getActualMaximum(Calendar.DAY_OF_MONTH) + " " + month_name));
                        labels.add(UtilsCommon.MonthArray[i] + " " + month_name);
                    } else {
                        dataList.add(new CategoryModel((float) cal, UtilsCommon.MonthArray[i] + " " + month_name));
                        labels.add(UtilsCommon.MonthArray[i] + " " + month_name);
                    }

                    if (i == 0) {
                        tv_heart_rate.setText((temp.get(Constant.heart_rate) + " bpm"));
                        tv_date.setText(UtilsCommon.MonthArray[i] + " " + month_name);
                        //tv_date.setText(temp.get(Constant.time));
                    }
                } else {
                    entries.add(new Entry(i, (float) -1));
                    dataList.add(new CategoryModel((float) -1, ""));
                    labels.add("");
                }
            }

            LineDataSet dataset = new LineDataSet(entries, "# of Calls");

            CombinedData cData = new CombinedData();
            LineData data = new LineData();

            dataset.setColors(new int[]{getActivity().getResources().getColor(R.color.app_light_green)});
            dataset.setValueTextColor(getActivity().getResources().getColor(R.color.app_light_green));
            dataset.setCircleColor(getActivity().getResources().getColor(R.color.line_chart_circle));
            dataset.setCircleColorHole(getActivity().getResources().getColor(R.color.line_chart_circle_hole));
            dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataset.setDrawFilled(false);
            dataset.setCircleSize(10);
            dataset.setLineWidth((float) 2.5);
            dataset.setDrawValues(false);
            dataset.setDrawHighlightIndicators(false);

            data.addDataSet(dataset);
            cData.setData(data);
            mChart.setData(cData);
            mChart.animateX(2500);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setTextSize(0f);
            xAxis.setTextColor(Color.TRANSPARENT);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);
            xAxis.setAxisMaximum(data.getXMax() + 0.25f);
            xAxis.setPosition(XAxisPosition.BOTH_SIDED);
            xAxis.setAxisMinimum(0f);
            xAxis.setGranularity(1f);

            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setDrawGridLines(false);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setTextSize(0f);
            leftAxis.setTextColor(Color.TRANSPARENT);
            leftAxis.setDrawLimitLinesBehindData(true);
            leftAxis.setDrawGridLines(false);
            mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

                @Override
                public void onValueSelected(Entry e, Highlight h) {

                    CategoryModel object = dataList.get((int) e.getX());
                    tv_heart_rate.setText((int) object.item1 + " bpm");
                    tv_date.setText(object.item2);

                }

                @Override
                public void onNothingSelected() {

                }
            });
            mChart.getAxisRight().setEnabled(false);
            mChart.invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of GetFitnessDataFromValidicCall and bind the data
    @Override
    public void onSucceedToGetFitnessFromValidic(ValidicSummaryModel summary, ValidicFitnessModel data, String chartFlag) {
        try {
            HashMap<String, String> temp = new HashMap<>();
            String startDate = summary.getStart_date();
            temp.put(Constant.time, UtilsCommon.ConvertStringIntoDateWeekName(startDate));
            int totalcal = 0;
            if (chartFlag.equalsIgnoreCase(Constant.month)) {
                if (summary.getResults() > 0) {
                    for (int i = 0; i < data.getFitness().size(); i++) {
                        totalcal = data.getFitness().get(i).getAverage_heart_rate();
                    }
                    temp.put(Constant.heart_rate, String.valueOf(totalcal));
                    total_heart_rate = total_heart_rate + totalcal;
                    dataCount++;
                } else {
                    temp.put(Constant.heart_rate, "0");
                }
                lst_month_heart_rate.add(temp);
                Debugger.debugE("month_heart_rate..", lst_month_heart_rate.size() + " " + lst_month_heart_rate.toString());
            }

            if (lst_month_heart_rate.size() == callCount) {
                tv_total_heart_rate.setText(String.valueOf(total_heart_rate / (dataCount == 0 ? 1 : dataCount)) + " BPM");
                monthWiseGraph(mChart, tv_heart_rate, tv_date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener of GetFitnessDataFromValidicCall
    @Override
    public void onFaildToGetFitnessFromValidic(String error_msg) {

    }

    // get the current week no and set the text color
    public void setCurrentWeekColor() {
        int weekNo1 = DashboardActivity.mCurrentweek;
        switch (weekNo1) {
            case 1:
                tv_week1.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
            case 2:
                tv_week2.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
            case 3:
                tv_week3.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
            case 4:
                tv_week4.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
            case 5:
                tv_week5.setTextColor(getActivity().getResources().getColor(R.color.yellow_text));
                break;
        }
    }
}
