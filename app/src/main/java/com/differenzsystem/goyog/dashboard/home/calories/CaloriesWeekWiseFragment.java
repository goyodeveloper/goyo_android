package com.differenzsystem.goyog.dashboard.home.calories;

import android.classes.textcounter.CounterView;
import android.classes.textcounter.Formatter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.syncdevice;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall.OnGetRoutineFromValidicListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.model.CategoryModel;
import com.differenzsystem.goyog.model.ValidicRoutineModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.veryfit.multi.nativedatabase.HealthSport;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static java.text.DateFormat.getTimeInstance;

public class CaloriesWeekWiseFragment extends Fragment implements OnGetRoutineFromValidicListener, SwipeRefreshLayout.OnRefreshListener, syncdevice.Onsyncdevice {
    public static String TAG = "CaloriesWeekWiseFragment";
    View rootview;
    CombinedChart mChart;
    TextView tv_calories, tv_date, tv_mon, tv_tue, tv_wed, tv_thu, tv_fri, tv_sat, tv_sun;
    CounterView tv_total_calories;
    LinearLayout lin_dayView;
    ArrayList<HashMap<String, String>> lst_week_calory;
    OnGetRoutineFromValidicListener onGetRoutineFromValidicListener;
    int total_calories = 0, callCount = 0;
    List<String> weekDay = new ArrayList<>();
    public GregorianCalendar cal_month;
    long statTime = 0;
    long endTime = 0;
    boolean isRefresh = false;

    SwipeRefreshLayout swipeRefreshLayout;
    syncdevice.Onsyncdevice onsyncdevice;

    public CaloriesWeekWiseFragment() {
    }

    // create the newInstance of CaloriesWeekWiseFragment
    public static CaloriesWeekWiseFragment newInstance(Bundle bundle) {
        CaloriesWeekWiseFragment fragment = new CaloriesWeekWiseFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onsyncdevice = this;
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.calories_week_wise_fragment, container, false);
            initcomopnets(rootview);
        } else
            container.removeView(rootview);

        return rootview;

    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Calories_Week_Wise);
    }


    // initialize all controls define in xml layout

    /**
     * @param rootview layout view
     */
    private void initcomopnets(View rootview) {
        try {
            swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.swipe_view);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(
                    R.color.colorPrimary);

            mChart = (CombinedChart) rootview.findViewById(R.id.chart_calories);

            lin_dayView = (LinearLayout) rootview.findViewById(R.id.lin_dayView);
            lst_week_calory = new ArrayList<>();

            tv_calories = (TextView) rootview.findViewById(R.id.tv_calories);
            tv_date = (TextView) rootview.findViewById(R.id.tv_date);
            tv_total_calories = (CounterView) rootview.findViewById(R.id.tv_total_calories);
            tv_mon = (TextView) rootview.findViewById(R.id.tv_mon);
            tv_tue = (TextView) rootview.findViewById(R.id.tv_tue);
            tv_wed = (TextView) rootview.findViewById(R.id.tv_wed);
            tv_thu = (TextView) rootview.findViewById(R.id.tv_thu);
            tv_fri = (TextView) rootview.findViewById(R.id.tv_fri);
            tv_sat = (TextView) rootview.findViewById(R.id.tv_sat);
            tv_sun = (TextView) rootview.findViewById(R.id.tv_sun);

            Log.e("global cal week data", String.valueOf(Globals.CaloriesWeekMap));

            onGetRoutineFromValidicListener = this;
            cal_month = (GregorianCalendar) GregorianCalendar.getInstance();

            setCurrentDayColor();
            //if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            //    getWeekwiseData();
            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {
                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    if (BleSharedPreferences.getInstance().getIsBind()) {
                        getWeekData();
                    } else {
                        swipeRefreshLayout.setEnabled(false);
                        setupDemoUserWeek();
                    }
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                    //TODO:get Date from device
                    if (Globals.CaloriesDayMap.size() > 0)
                        setDataFromGlobals();
                    else
                        getDeviceCaloryData();
                }
            } else {
                swipeRefreshLayout.setEnabled(false);
                setupDemoUserWeek();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // setup data for demo user
    private void setupDemoUserWeek() {
        Calendar mCalendar = Calendar.getInstance();
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
        int dayOfWeek = mCalendar.get(Calendar.DAY_OF_WEEK);

        for (int i = 0; i < 8; i++) {
            if (i != 0) {
                HashMap<String, String> temp = new HashMap<>();
                temp.put(Constant.time, String.valueOf(day) + " " + (String) android.text.format.DateFormat.format("MMMM", new Date()));

                if (i < dayOfWeek) {
                    temp.put(Constant.calories_burned, "0");
                } else {
                    temp.put(Constant.calories_burned, "-1");
                }
                lst_week_calory.add(temp);
            }
        }

        setDataWithCounter(tv_total_calories, 0);

        UtilsCommon.destroyProgressBar();
        weekWiseGraph(mChart, tv_calories, tv_date);

    }

    public void getWeekDataOld() {
        try {
            Calendar mCalendar = Calendar.getInstance();
                   /* int year = mCalendar.get(Calendar.YEAR);
                    int month=mCalendar.get(Calendar.MONTH);*/
            int Today = mCalendar.get(Calendar.DAY_OF_MONTH);
            int dayOfWeek = mCalendar.get(Calendar.DAY_OF_WEEK);

            int year = 0, month = 0, day = 0;

            List<HealthSport> weekHealthSport = ProtocolUtils.getInstance().getWeekHealthSport(0);
            if (weekHealthSport != null && weekHealthSport.size() > 0) {
                Date startDate = weekHealthSport.get(0).getDate();
                int totalcal = 0;
                for (int i = 0; i < weekHealthSport.size(); i++) {
                    if (i != 0) {
                        HashMap<String, String> temp = new HashMap<>();
                        //temp.put(Constant.time, UtilsCommon.getDateDayName(startDate.toString()));
                        year = weekHealthSport.get(i).getYear();
                        month = weekHealthSport.get(i).getMonth();
                        day = weekHealthSport.get(i).getDay();

                        temp.put(Constant.time, String.valueOf(day) + " " + (String) android.text.format.DateFormat.format("MMMM", new Date()));

                        totalcal = totalcal + weekHealthSport.get(i).getTotalCalory();
                        Log.e("total calaries", String.valueOf(totalcal));
                        callCount++;

                        if (i < dayOfWeek) {
                            temp.put(Constant.calories_burned, String.valueOf(weekHealthSport.get(i).getTotalCalory()));
                        } else {
                            temp.put(Constant.calories_burned, "-1");
                        }
                        lst_week_calory.add(temp);
                    }
                }
                HealthSport sundaydata = ProtocolUtils.getInstance().getHealthSport(new Date(year, month - 1, day + 1));
                HashMap<String, String> temp = new HashMap<>();
                callCount++;
                if (sundaydata != null) {
                    totalcal = totalcal + sundaydata.getTotalCalory();
                    if (sundaydata.getDay() <= Today) {
                        temp.put(Constant.time, String.valueOf(sundaydata.getDay()) + " " + (String) android.text.format.DateFormat.format("MMMM", new Date()));
                        temp.put(Constant.calories_burned, String.valueOf(totalcal));
                    } else {
                        temp.put(Constant.calories_burned, "-1");
                    }
                } else {
                    temp.put(Constant.calories_burned, "-1");
                }
                lst_week_calory.add(temp);
                total_calories = total_calories + totalcal;
                setDataWithCounter(tv_total_calories, total_calories);
            }

            //tv_total_step.setText(String.valueOf(total_step));
            Debugger.debugE("Week_Calories..", callCount + "  " + lst_week_calory.size());

            if (lst_week_calory.size() == callCount) {
                //UtilsCommon.destroyProgressBar();0
                weekWiseGraph(mChart, tv_calories, tv_date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set the graph from the calorie data
    public void getWeekData() {
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy");//yyyy-MM-dd
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        int delta = -calendar.get(GregorianCalendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        calendar.add(Calendar.DAY_OF_MONTH, delta);

        String[] days = new String[7];
        int totalcal = 0;
        for (int i = 0; i < 7; i++) {
            days[i] = format.format(calendar.getTime());

            String start[] = days[i].split("/");
            int month = Integer.parseInt(start[0]) - 1;
            int day = Integer.parseInt(start[1]);
            int year = Integer.parseInt(start[2]);

            HashMap<String, String> temp = new HashMap<>();

            HealthSport Healthdata = ProtocolUtils.getInstance().getHealthSport(new Date(year, month, day));
            temp.put(Constant.time, String.valueOf(day) + " " + (String) android.text.format.DateFormat.format("EEEE", calendar.getTime()));

            if (i < dayOfWeek) {
                if (Healthdata != null) {
                    totalcal = totalcal + Healthdata.getTotalCalory();
                    temp.put(Constant.calories_burned, String.valueOf(Healthdata.getTotalCalory()));
                    Log.e("Healthdata =>", Healthdata.toString());
                } else {
                    totalcal = totalcal + 0;
                    temp.put(Constant.calories_burned, "0");
                }
                lst_week_calory.add(temp);
            }
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        Log.e("Week Date =>", Arrays.toString(days));

        total_calories = total_calories + totalcal;
        setDataWithCounter(tv_total_calories, total_calories);


        if (lst_week_calory.size() == dayOfWeek) {
            weekWiseGraph(mChart, tv_calories, tv_date);
        }
    }

    // get calories data from shared prefrences
    public void setDataFromGlobals() {
        HashMap<String, Object> map = Globals.CaloriesWeekMap;
        total_calories = (int) map.get(Constant.calories_burned);
        lst_week_calory = (ArrayList<HashMap<String, String>>) map.get("weeklist");
        setDataWithCounter(tv_total_calories, total_calories);
        weekWiseGraph(mChart, tv_calories, tv_date);
    }

    // success listener of syncdevice
    @Override
    public void onSucceedsyncdevice() {
        swipeRefreshLayout.setRefreshing(false);
        getWeekData();
    }

   /* @Override
    public void onFailedsyncdevice() {

    }*/

    // here get calorie data from band
    private void getAndSetData() {
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            getWeekwiseData();
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            syncdevice s = new syncdevice(getContext(), onsyncdevice);
            s.syncdata();
        } else {
            getDeviceCaloryData();
        }
    }

    private void getDeviceCaloryData() {
        try {
            if (ConnectionDetector.internetCheck(getContext())) {
                buildFitnessClient();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Googleapiclient connect
    private void buildFitnessClient() {
        if (HomeFragment.mClient != null) {
            if (!swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(true);
            getCurrentWeekDay();
        }
    }

    // set data with counting animation

    /**
     * @param counterView get counterview
     * @param val         total calories
     */
    public void setDataWithCounter(CounterView counterView, float val) {
        counterView.setFormatter(new Formatter() {
            @Override
            public String format(String prefix, String suffix, float value) {
                return prefix
                        + NumberFormat.getNumberInstance(Locale.US).format(value)
                        + suffix;
            }
        });
        counterView.setAutoStart(false);
        counterView.setStartValue(val);
        counterView.setEndValue(val);
        counterView.setIncrement(Constant.getTimeInterval); // the amount the number increments at each time interval
        counterView.setIncrement(UtilsCommon.getInterval(val)); // the amount the number increments at each time interval
        counterView.start(); // you can start anytime if autostart is set to false
    }

    // swipe to refresh call and rebind the data
    @Override
    public void onRefresh() {
        statTime = 0;
        endTime = 0;
        callCount = 0;
        total_calories = 0;
        lst_week_calory = new ArrayList<>();
        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();

        if (Globals.CaloriesWeekMap.size() > 0)
            Globals.CaloriesWeekMap.clear();

        isRefresh = true;
        getAndSetData();
    }

    // calories call from google fit
    private class CaloriesCall extends AsyncTask<Void, Void, Void> {
        String caloriesData;

        protected Void doInBackground(Void... params) {
            try {
                HomeFragment.mClient.connect();
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(HomeFragment.mClient, readRequest).await(1, TimeUnit.MINUTES);
                caloriesData = readData(dataReadResult);
                Debugger.debugE("caloriesData", caloriesData + "");
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            isRefresh = false;
            try {
                //tv_total_calories.setText(total_calories + "");
                Globals.CaloriesWeekMap.put(Constant.calories_burned, total_calories);

                setDataWithCounter(tv_total_calories, total_calories);
                weekWiseGraph(mChart, tv_calories, tv_date);
                Debugger.debugE("caloriesData", caloriesData + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    // request data from google fit
    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            Calendar cal = Calendar.getInstance();
            Date now = new Date();
            cal.setTime(now);
            cal.add(Calendar.WEEK_OF_YEAR, -1);

            java.text.DateFormat dateFormat = DateFormat.getDateInstance();
            Log.e("History", "Range Start: " + dateFormat.format(statTime));
            Log.e("History", "Range End: " + dateFormat.format(endTime));

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(statTime, endTime, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    // read specific data by challenge type
    public String readData(DataReadResult dataReadResult) {
        String value = "";
        total_calories = 0;
        try {
            if (dataReadResult.getBuckets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getBuckets().size());
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                        total_calories += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                    total_calories += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }

                HashMap<String, String> temp = new HashMap<>();
                String sDate = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00").format(dp.getStartTime(TimeUnit.MILLISECONDS));
                temp.put(Constant.time, UtilsCommon.getDateDayName(sDate));

                temp.put(Constant.calories_burned, value);
                for (int i = 0; i < lst_week_calory.size(); i++) {
                    if (lst_week_calory.get(i).get(Constant.time).equalsIgnoreCase(UtilsCommon.getDateDayName(sDate))) {
                        lst_week_calory.set(i, temp);
                    }
                }
                //lst_week_calory.add(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    // get the day of the current week and bind the data accordingly
    public void getCurrentWeekDay() {

        SimpleDateFormat dayDateFormate = new SimpleDateFormat("dd EEEE");
        if (weekDay.size() > 0) weekDay.clear();

        cal_month.setFirstDayOfWeek(Calendar.MONDAY);
        cal_month.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        int currentWeek = cal_month.get(Calendar.WEEK_OF_YEAR);
        int year = cal_month.get(Calendar.YEAR);
        cal_month.set(Calendar.HOUR_OF_DAY, 0);

        String[] days = new String[7];
        for (int i = 0; i < 7; i++) {
            if (!UtilsCommon.AddDayInDateOfMonday(i).equalsIgnoreCase(UtilsCommon.getNextDaydate())) {

                if (i == 0) {
                    statTime = cal_month.getTimeInMillis();
                }
                if (i == 6) {
                    endTime = cal_month.getTimeInMillis();
                }
                days[i] = dayDateFormate.format(cal_month.getTime());
                weekDay.add(dayDateFormate.format(cal_month.getTime()));
                cal_month.add(GregorianCalendar.DAY_OF_WEEK, 1);
                cal_month.set(Calendar.HOUR_OF_DAY, 0);
                HashMap<String, String> temp = new HashMap<>();

                temp.put(Constant.time, weekDay.get(i));

                temp.put(Constant.calories_burned, "0");

                lst_week_calory.add(temp);

            } else {
                if (i == 0) {
                    statTime = cal_month.getTimeInMillis();
                }
                if (i == 6) {
                    endTime = cal_month.getTimeInMillis();
                }
                days[i] = dayDateFormate.format(cal_month.getTime());
                weekDay.add(dayDateFormate.format(cal_month.getTime()));
                cal_month.add(GregorianCalendar.DAY_OF_WEEK, 1);
                cal_month.set(Calendar.HOUR_OF_DAY, 0);
                HashMap<String, String> temp = new HashMap<>();

                temp.put(Constant.time, weekDay.get(i));

                temp.put(Constant.calories_burned, "-1");

                lst_week_calory.add(temp);
            }
        }
        if (isRefresh)
            swipeRefreshLayout.setRefreshing(true);
        new CaloriesCall().execute();
    }

    // api call for GetRoutineDataFromValidic
    public void getWeekwiseData() {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            // UtilsCommon.showProgressDialog(getActivity());

            for (int i = 0; i < 7; i++) {
                if (!UtilsCommon.AddDayInDateOfMonday(i).equalsIgnoreCase(UtilsCommon.getNextDaydate())) {
                    GetRoutineDataFromValidicCall task = new GetRoutineDataFromValidicCall(getActivity(), onGetRoutineFromValidicListener, false, UtilsCommon.AddDayInDateOfMonday(i), UtilsCommon.AddDayInDateOfMonday(i + 1), Constant.week);
                    task.execute();
                    callCount++;
                } else {
                    break;
                }
            }
        } else {
            //Toast.makeText(getActivity(), getActivity().getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * @param mChart      get the chart for displaying calorie progress
     * @param tv_calories get textview of calorie
     * @param tv_date     get textview of date
     */
    private void weekWiseGraph(CombinedChart mChart, final TextView tv_calories, final TextView tv_date) {
        Globals.CaloriesWeekMap.put("weeklist", lst_week_calory);

        try {
            mChart.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setHighlightPerDragEnabled(false);
            mChart.setPinchZoom(false);
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.setDescription("");
            mChart.setExtraOffsets(30, 30, 30, 30);
            mChart.setTouchEnabled(true);
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setPinchZoom(false);

            Legend l = mChart.getLegend();
            l.setEnabled(false);

            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<CategoryModel> dataList = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            for (int i = 0; i < 7; i++) {
                if (i < lst_week_calory.size()) {
                    HashMap<String, String> temp = lst_week_calory.get(i);

                    double cal = Double.parseDouble(temp.get(Constant.calories_burned));
                    entries.add(new Entry(i, (float) cal));
                    dataList.add(new CategoryModel((float) cal, temp.get(Constant.time)));
                    labels.add(temp.get(Constant.time));
                    if (i == 0) {
                        tv_calories.setText(String.valueOf((int) Float.parseFloat(temp.get(Constant.calories_burned))) + " Cal");
                        tv_date.setText(temp.get(Constant.time));
                    }
                } else {
                    entries.add(new Entry(i, (float) -1));
                    dataList.add(new CategoryModel((float) -1, ""));
                    labels.add("");
                }
            }

            CombinedData cData = new CombinedData();
            LineData data = new LineData();
            LineDataSet dataset = new LineDataSet(entries, "");
            dataset.setColors(new int[]{getActivity().getResources().getColor(R.color.app_light_green)});
            dataset.setValueTextColor(getActivity().getResources().getColor(R.color.app_light_green));
            dataset.setCircleColor(getActivity().getResources().getColor(R.color.line_chart_circle));
            dataset.setCircleColorHole(getActivity().getResources().getColor(R.color.line_chart_circle_hole));
            dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataset.setDrawFilled(false);
            dataset.setLineWidth((float) 2.5);
            dataset.setCircleSize(10);
            dataset.setDrawValues(false);
            dataset.setDrawHighlightIndicators(false);

            data.addDataSet(dataset);
            cData.setData(data);
            mChart.setData(cData);
            mChart.animateX(2500);

            mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

                @Override
                public void onValueSelected(Entry e, Highlight h) {

                    CategoryModel object = dataList.get((int) e.getX());
                    tv_calories.setText((int) object.item1 + " Cal");
                    tv_date.setText(object.item2);

                }

                @Override
                public void onNothingSelected() {

                }
            });
            mChart.getAxisRight().setEnabled(false);
            mChart.invalidate();
            swipeRefreshLayout.setRefreshing(false);
        } catch (Exception e) {
            swipeRefreshLayout.setRefreshing(false);
            e.printStackTrace();
        }
    }

    // success listener of GetRoutineDataFromValidicCall and bind the data
    @Override
    public void onSucceedToGetRoutineFromValidic(ValidicSummaryModel summary, ValidicRoutineModel data, String chartFlag) {
        try {
            HashMap<String, String> temp = new HashMap<>();

            String startDate = summary.getStart_date();

            temp.put(Constant.time, UtilsCommon.getDateDayName(startDate));
            if (chartFlag.equalsIgnoreCase(Constant.week)) {

                int totalcal = 0;
                if (summary.getResults() > 0) {
                    for (int i = 0; i < data.getRoutine().size(); i++) {
                        totalcal = totalcal + data.getRoutine().get(i).getCalories_burned();
                    }
                    temp.put(Constant.calories_burned, String.valueOf(totalcal));
                    total_calories = total_calories + totalcal;
                } else {
                    temp.put(Constant.calories_burned, "0");
                }
                lst_week_calory.add(temp);
                tv_total_calories.setText(String.valueOf(total_calories));
                Debugger.debugE("Week_Calories..", callCount + "  " + lst_week_calory.size() + "  " + startDate + "  " + summary.getEnd_date());
            }
            if (lst_week_calory.size() == callCount) {
                //UtilsCommon.destroyProgressBar();
                weekWiseGraph(mChart, tv_calories, tv_date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener of GetRoutineDataFromValidicCall
    @Override
    public void onFaildToGetRoutineFromValidic(String error_msg) {

    }

    // get the current day number and set the text color
    public void setCurrentDayColor() {
        Calendar date = Calendar.getInstance();
        int day = date.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case 1:
                tv_sun.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 2:
                tv_mon.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 3:
                tv_tue.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 4:
                tv_wed.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 5:
                tv_thu.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 6:
                tv_fri.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
            case 7:
                tv_sat.setTextColor(getResources().getColor(R.color.yellow_text));
                break;
        }
    }
}
