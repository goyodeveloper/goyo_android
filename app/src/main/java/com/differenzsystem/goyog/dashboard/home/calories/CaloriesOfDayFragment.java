package com.differenzsystem.goyog.dashboard.home.calories;

import android.classes.textcounter.CounterView;
import android.classes.textcounter.Formatter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.syncdevice;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall;
import com.differenzsystem.goyog.api.GetRoutineDataFromValidicCall.OnGetRoutineFromValidicListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.home.HomeFragment;
import com.differenzsystem.goyog.model.CategoryModel;
import com.differenzsystem.goyog.model.ValidicRoutineModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.SpriteFactory;
import com.github.ybq.android.spinkit.Style;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.veryfit.multi.nativedatabase.HealthSportItem;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;

import java.sql.Time;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static java.text.DateFormat.getTimeInstance;

public class CaloriesOfDayFragment extends Fragment implements OnGetRoutineFromValidicListener, SwipeRefreshLayout.OnRefreshListener, syncdevice.Onsyncdevice {
    public static String TAG = "CaloriesOfDayFragment";
    View rootview;
    CombinedChart mChart;
    TextView tv_calories, tv_date;
    CounterView tv_total_calories;
    LinearLayout lin_dayView;
    ArrayList<HashMap<String, String>> lst_day_calory;
    OnGetRoutineFromValidicListener onGetRoutineFromValidicListener;
    int total_calories = 0;
    int hours;

    SwipeRefreshLayout swipeRefreshLayout;
    syncdevice.Onsyncdevice onsyncdevice;

    public CaloriesOfDayFragment() {
    }

    public static CaloriesOfDayFragment newInstance(Bundle bundle) {
        CaloriesOfDayFragment fragment = new CaloriesOfDayFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        onsyncdevice = this;
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.calories_of_day_fragment, container, false);
            initcomopnets(rootview);
        } else
            container.removeView(rootview);

        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.recordScreenViews(getActivity(), Constant.Name_Calories_Day_Wise);
    }

    // initialize all controls define in xml layout

    /**
     *
     * @param rootview layout view
     */
    private void initcomopnets(View rootview) {
        try {
            swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.swipe_view);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(
                    R.color.colorPrimary);

            mChart = (CombinedChart) rootview.findViewById(R.id.chart_calories);

            lin_dayView = (LinearLayout) rootview.findViewById(R.id.lin_dayView);
            lst_day_calory = new ArrayList<>();

            tv_calories = (TextView) rootview.findViewById(R.id.tv_calories);
            tv_date = (TextView) rootview.findViewById(R.id.tv_date);
            tv_total_calories = (CounterView) rootview.findViewById(R.id.tv_total_calories);
            lin_dayView.setVisibility(View.VISIBLE);
            hours = new Time(System.currentTimeMillis()).getHours();

            Log.e("global cal data", String.valueOf(Globals.CaloriesDayMap));

            onGetRoutineFromValidicListener = this;
            //f (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            //   getDaywiseData(true);
            if (UtilsPreferences.getString(getActivity(), Constant.user_type).equalsIgnoreCase("1")) {

                if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    if (BleSharedPreferences.getInstance().getIsBind()) {
                        getDayData();
                    } else {
                        swipeRefreshLayout.setEnabled(false);
                        setupDemoUserDay();
                    }
                } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                    if (Globals.CaloriesDayMap.size() > 0)
                        setDataFromGlobals();
                    else
                        getDeviceCaloryData();
                }
            } else {
                swipeRefreshLayout.setEnabled(false);
                setupDemoUserDay();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // graph for day wise
    private void setupDemoUserDay() {
        hours = new Time(System.currentTimeMillis()).getHours() + 1;

        for (int i = 0; i < 24; i++) {
            HashMap<String, String> temp = new HashMap<>();
            temp.put(Constant.time, new Date().toString());
            int sum = 0;
            if (i < hours) {
                temp.put(Constant.calories_burned, "0");
                lst_day_calory.add(temp);
            } else {
                break;
            }
        }
        setDataWithCounter(tv_total_calories, 0);

        UtilsCommon.destroyProgressBar();
        dayWiseGraph(mChart, tv_calories, tv_date);

    }


    public void getDayData() {
        Calendar mCalendar = Calendar.getInstance();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
        hours = new Time(System.currentTimeMillis()).getHours() + 1;

        List<HealthSportItem> dateHealthSportItem = ProtocolUtils.getInstance().getHealthSportItem(new Date(year, month, day));
        if (dateHealthSportItem != null && dateHealthSportItem.size() > 0) {
            Date startDate = dateHealthSportItem.get(0).getDate();
            List<Integer> stepsdata = new ArrayList<>();

            int[] intArray = new int[96];
            for (int i = 0; i < dateHealthSportItem.size(); i++)
                intArray[i] = dateHealthSportItem.get(i).getCalory();
            //stepsdata.add(dateHealthSportItem.get(i).getCalory());

            //int[] intArray = new int[stepsdata.size()];

            int x = 4;  // chunk size
            int len = intArray.length;
            int counter = 0;
            int[][] newArray = new int[len][];

            for (int i = 0; i < len - x + 1; i += x)
                newArray[counter++] = Arrays.copyOfRange(intArray, i, i + x);

            if (len % x != 0)
                newArray[counter] = Arrays.copyOfRange(intArray, len - len % x, len);

            Log.e("Hours data:", newArray.toString());

            for (int i = 0; i < 24; i++) {
                HashMap<String, String> temp = new HashMap<>();
                temp.put(Constant.time, UtilsCommon.getDateDayName(startDate.toString()));
                int sum = 0;
                if (i < hours) {
                    for (int j = 0; j < newArray[i].length; j++) {
                        sum += newArray[i][j];
                        temp.put(Constant.calories_burned, String.valueOf(sum));
                    }
                    total_calories = total_calories + sum;
                    lst_day_calory.add(temp);
                } else {
                    break;
                }
            }

            setDataWithCounter(tv_total_calories, total_calories);
            Debugger.debugE("Day_steps..", hours + " " + lst_day_calory.size() + " " + lst_day_calory.toString());

            if (lst_day_calory.size() == hours) {

                Debugger.debugE("inside_Calories..", lst_day_calory.size() + " " + lst_day_calory.toString());
                UtilsCommon.destroyProgressBar();
                dayWiseGraph(mChart, tv_calories, tv_date);
            }
        } else {
            setupDemoUserDay();
        }

    }

    // get calories data from shared prefrences
    public void setDataFromGlobals() {
        HashMap<String, Object> map = Globals.CaloriesDayMap;
        total_calories = (int) map.get(Constant.calories_burned);
        lst_day_calory = (ArrayList<HashMap<String, String>>) map.get("daylist");
        setDataWithCounter(tv_total_calories, total_calories);
        dayWiseGraph(mChart, tv_calories, tv_date);
    }

    // success listener of syncdevice
    @Override
    public void onSucceedsyncdevice() {
        swipeRefreshLayout.setRefreshing(false);
        getDayData();
    }

   /* @Override
    public void onFailedsyncdevice() {

    }*/

    // here get calorie data from band
    private void getAndSetData() {
        if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
            getDaywiseData(false);
        } else if (UtilsPreferences.getString(getActivity(), Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            syncdevice s = new syncdevice(getContext(), onsyncdevice);
            s.syncdata();
        } else {
            new CaloriesCall().execute();
        }
    }

    private void getDeviceCaloryData() {
        try {
            if (ConnectionDetector.internetCheck(getContext())) {
                buildFitnessClient();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Googleapiclient connect
    private void buildFitnessClient() {
        if (HomeFragment.mClient != null) {
            UtilsCommon.showProgressDialog(getActivity());
            new CaloriesCall().execute();
        }
    }

    // swipe to refresh call
    @Override
    public void onRefresh() {
        total_calories = 0;
        lst_day_calory = new ArrayList<>();
        if (Globals.CaloriesDayMap.size() > 0)
            Globals.CaloriesDayMap.clear();
        swipeRefreshLayout.setRefreshing(true);
        getAndSetData();
    }

    // request data for calorie
    private class CaloriesCall extends AsyncTask<Void, Void, Void> {
        String caloriesData;

        protected Void doInBackground(Void... params) {

            try {
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(HomeFragment.mClient, readRequest).await(1, TimeUnit.MINUTES);
                caloriesData = readData(dataReadResult);
                Debugger.debugE("caloriesData", caloriesData + "");
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                UtilsCommon.destroyProgressBar();
                //tv_total_calories.setText(total_calories + "");
                Globals.CaloriesDayMap.put(Constant.calories_burned, total_calories);

                setDataWithCounter(tv_total_calories, total_calories);
                dayWiseGraph(mChart, tv_calories, tv_date);
                Debugger.debugE("caloriesData", caloriesData + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // set data with counting animation

    /**
     *
     * @param counterView get counterview
     * @param val         total calories
     */
    public void setDataWithCounter(CounterView counterView, float val) {
        counterView.setFormatter(new Formatter() {
            @Override
            public String format(String prefix, String suffix, float value) {
                return prefix
                        + NumberFormat.getNumberInstance(Locale.US).format(value)
                        + suffix;
            }
        });
        counterView.setAutoStart(false);
        counterView.setStartValue(val);
        counterView.setEndValue(val);
        counterView.setIncrement(UtilsCommon.getInterval(val)); // the amount the number increments at each time interval
        counterView.setTimeInterval(Constant.getTimeInterval); // the time interval (ms) at which the text changes
        counterView.start(); // you can start anytime if autostart is set to false
    }

    // request data from google fit
    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            Calendar startCalendar = Calendar.getInstance();
            long startTime = startCalendar.getTimeInMillis();

            SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
            String startTimeString = formater.format(startTime) + " 12:01 AM";
            Date startDate = null;
            try {
                startDate = formater.parse(startTimeString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long startMillisecond = startDate.getTime();

            Calendar endCalendar = Calendar.getInstance();
            Date endDate = new Date();
            endCalendar.setTime(endDate);
            long endMillisecond = endCalendar.getTimeInMillis();

            Debugger.debugE("Start time", startDate + "=>");
            Debugger.debugE("End time", endDate + "=>");

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.HOURS)
                    .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    // read specific data by challenge type
    public String readData(DataReadResult dataReadResult) {
        String value = "";
        total_calories = 0;
        try {
            if (dataReadResult.getBuckets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getBuckets().size());
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                        total_calories += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                        if (value.equalsIgnoreCase("")) {
                            HashMap<String, String> temp = new HashMap<>();
                            temp.put(Constant.time, "");
                            temp.put(Constant.calories_burned, "0");
                            lst_day_calory.add(temp);
                        }
                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
//                Debugger.debugE(TAG, "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                    total_calories += Math.round(Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value));
                    if (value.equalsIgnoreCase("")) {
                        HashMap<String, String> temp = new HashMap<>();
                        temp.put(Constant.time, "");
                        temp.put(Constant.calories_burned, "0");
                        lst_day_calory.add(temp);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }

                HashMap<String, String> temp = new HashMap<>();
                String sDate = new SimpleDateFormat("hh:mm").format(dp.getStartTime(TimeUnit.MILLISECONDS));
                String eDate = new SimpleDateFormat("hh:mm").format(dp.getEndTime(TimeUnit.MILLISECONDS));
                String time = sDate + "-" + eDate;
                temp.put(Constant.time, time);
                temp.put(Constant.calories_burned, value);

                lst_day_calory.add(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * @param mChart      get the chart for displaying calorie progress
     * @param tv_calories get textview of calorie
     * @param tv_date     get textview of date
     */
    private void dayWiseGraph(CombinedChart mChart, final TextView tv_calories, final TextView tv_date) {
        swipeRefreshLayout.setRefreshing(false);
        Globals.CaloriesDayMap.put("daylist", lst_day_calory);
        try {
            mChart.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setHighlightPerDragEnabled(false);
            mChart.setPinchZoom(false);
            mChart.setExtraOffsets(30, 30, 30, 30);
            mChart.getAxisRight().setEnabled(false);
            mChart.getAxisLeft().setEnabled(false);
            mChart.setDescription("");
            mChart.setTouchEnabled(true);
            mChart.setDragEnabled(false);
            mChart.setScaleEnabled(false);
            mChart.setPinchZoom(false);

            Legend l = mChart.getLegend();
            l.setEnabled(false);

            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<CategoryModel> dataList = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            for (int i = 0; i < 24; i++) {
                if (i < lst_day_calory.size()) {
                    HashMap<String, String> temp = lst_day_calory.get(i);
                    double cal = Double.parseDouble(temp.get(Constant.calories_burned));
                    entries.add(new Entry(i, (float) cal));
                    dataList.add(new CategoryModel((float) cal, UtilsCommon.dayArray[i]));
                    labels.add(UtilsCommon.dayArray[i]);
                    if (i == 0) {
                        tv_calories.setText(String.valueOf((int) Float.parseFloat(temp.get(Constant.calories_burned))) + " Cal");
                        tv_date.setText(UtilsCommon.dayArray[i]);
                    }
                } else {
                    entries.add(new Entry(i, (float) -1));
                    dataList.add(new CategoryModel((float) -1, UtilsCommon.dayArray[i]));
                    labels.add(UtilsCommon.dayArray[i]);
                    if (i == 0) {
                        tv_calories.setText(0 + " Cal");
                        tv_date.setText(UtilsCommon.dayArray[i]);
                    }
                }
            }

            LineDataSet dataset = new LineDataSet(entries, "# of Calls");

            CombinedData cData = new CombinedData();
            final LineData data = new LineData();

            dataset.setColors(new int[]{getActivity().getResources().getColor(R.color.app_light_green)});
            dataset.setValueTextColor(getActivity().getResources().getColor(R.color.app_light_green));
            dataset.setCircleColor(getActivity().getResources().getColor(R.color.line_chart_circle));
            dataset.setCircleColorHole(getActivity().getResources().getColor(R.color.line_chart_circle_hole));
            dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataset.setDrawFilled(false);
            dataset.setCircleSize(10);
            dataset.setDrawValues(false);
            dataset.setDrawHighlightIndicators(false);
            dataset.setLineWidth((float) 2.5);

            SpinKitView spinKitView = new SpinKitView(getActivity());
            Style style = Style.values()[4];
            Sprite drawable = SpriteFactory.create(style);
            spinKitView.setIndeterminateDrawable(drawable);
            dataset.setSpinKitView(spinKitView);

            data.addDataSet(dataset);
            cData.setData(data);
            mChart.setData(cData);
            mChart.animateX(2500);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setTextSize(0f);
            xAxis.setTextColor(Color.TRANSPARENT);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);
            xAxis.setAxisMaximum(data.getXMax() + 0.25f);
            xAxis.setPosition(XAxisPosition.BOTH_SIDED);
            xAxis.setAxisMinimum(0f);
            xAxis.setGranularity(1f);

            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setDrawGridLines(false);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setTextSize(0f);
            leftAxis.setTextColor(Color.TRANSPARENT);
            leftAxis.setDrawLimitLinesBehindData(true);
            leftAxis.setDrawGridLines(false);

            mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

                @Override
                public void onValueSelected(Entry e, Highlight h) {
                    CategoryModel object = dataList.get((int) e.getX());
                    tv_calories.setText((int) object.item1 + " Cal");
                    tv_date.setText(object.item2);
                }

                @Override
                public void onNothingSelected() {

                }
            });

            mChart.getAxisRight().setEnabled(false);
            mChart.invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // GetRoutineDataFromValidicCall api call

    /**
     *
     * @param showProgress boolean for showing progressbar
     */
    public void getDaywiseData(boolean showProgress) {
        if (ConnectionDetector.isConnectingToInternet(getActivity())) {
            if (showProgress)
                UtilsCommon.showProgressDialog(getActivity());
            for (int i = 01; i <= hours; i++) {
                GetRoutineDataFromValidicCall task = new GetRoutineDataFromValidicCall(getActivity(), onGetRoutineFromValidicListener, false, UtilsCommon.getDateByHour(i), UtilsCommon.getDateByHour(i + 1), Constant.day);
                task.execute();
            }
        } else {
            //Toast.makeText(getActivity(), getActivity().getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
        }
    }

    // success listener of GetRoutineDataFromValidicCall and bind the data
    @Override
    public void onSucceedToGetRoutineFromValidic(ValidicSummaryModel summary, ValidicRoutineModel data, String chartFlag) {
        try {

            HashMap<String, String> temp = new HashMap<>();

            String startDate = summary.getStart_date();
            String endDate = summary.getEnd_date();

            String sDate = startDate.substring(startDate.indexOf("T") + 1, startDate.indexOf("T") + 6);
            String eDate = endDate.substring(endDate.indexOf("T") + 1, endDate.indexOf("T") + 6);
            String time = sDate + "-" + eDate;
            temp.put(time, time);
            if (chartFlag.equalsIgnoreCase(Constant.day)) {

                if (summary.getResults() > 0) {
                    temp.put(Constant.calories_burned, String.valueOf(data.getRoutine().get(0).getCalories_burned()));
                    total_calories = total_calories + data.getRoutine().get(0).getCalories_burned();
                } else {
                    temp.put(Constant.calories_burned, "0");
                }
                lst_day_calory.add(temp);
                tv_total_calories.setText(String.valueOf(total_calories));
                Debugger.debugE("Day_Calories..", hours + " " + lst_day_calory.size() + " " + lst_day_calory.toString());

            }

            if (lst_day_calory.size() == hours) {

                Debugger.debugE("inside_Calories..", lst_day_calory.size() + " " + lst_day_calory.toString());
                UtilsCommon.destroyProgressBar();
                dayWiseGraph(mChart, tv_calories, tv_date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener of GetRoutineDataFromValidicCall
    @Override
    public void onFaildToGetRoutineFromValidic(String error_msg) {

    }
}
