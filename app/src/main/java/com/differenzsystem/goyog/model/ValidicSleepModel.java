package com.differenzsystem.goyog.model;

import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 8/29/16.
 */

public class ValidicSleepModel {
    private ArrayList<Sleep> sleep;


    public ArrayList<Sleep> getSleep() {
        return sleep;
    }

    public void setSleep(ArrayList<Sleep> sleep) {
        this.sleep = sleep;
    }


    public class Sleep {

        private String last_updated;
        private String timestamp;
        private int awake_count;
        private int restless_count;
        private int minutes_asleep;
        private int time_in_bed;


        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public int getAwake_count() {
            return awake_count;
        }

        public void setAwake_count(int awake_count) {
            this.awake_count = awake_count;
        }

        public int getRestless_count() {
            return restless_count;
        }

        public void setRestless_count(int restless_count) {
            this.restless_count = restless_count;
        }

        public int getMinutes_asleep() {
            return minutes_asleep;
        }

        public void setMinutes_asleep(int minutes_asleep) {
            this.minutes_asleep = minutes_asleep;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public int getTime_in_bed() {
            return time_in_bed;
        }

        public void setTime_in_bed(int time_in_bed) {
            this.time_in_bed = time_in_bed;
        }
    }

}
