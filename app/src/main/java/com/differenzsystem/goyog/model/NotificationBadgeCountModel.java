package com.differenzsystem.goyog.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Union Assurance PLC on 8/2/17.
 */

public class NotificationBadgeCountModel {

    @SerializedName("flag")
    public boolean flag;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public int data;
}
