package com.differenzsystem.goyog.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Union Assurance PLC on 8/1/17.
 */

public class NextLevelChallengeModel implements Serializable {

    @SerializedName("flag")
    public boolean flag;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public Data data;

    public static class Challenge_data implements Serializable {
        @SerializedName("challenge_id")
        public String challenge_id;
        @SerializedName("name")
        public String name;
        @SerializedName("subtitle")
        public String subtitle;
        @SerializedName("challenge_type")
        public String challenge_type;
        @SerializedName("challenge_value")
        public String challenge_value;
        @SerializedName("second_challenge_value")
        public String second_challenge_value;
        @SerializedName("third_challenge_value")
        public String third_challenge_value;
        @SerializedName("fourth_challenge_value")
        public String fourth_challenge_value;
        @SerializedName("description")
        public String description;
        @SerializedName("offer_description")
        public String offer_description;
        @SerializedName("image")
        public String image;
        @SerializedName("banner_flag")
        public String banner_flag;
        @SerializedName("thumb_image")
        public String thumb_image;
        @SerializedName("type")
        public String type;
        @SerializedName("qrcode")
        public String qrcode;
        @SerializedName("point")
        public String point;
        @SerializedName("date")
        public String date;
        @SerializedName("challenges_exp_date")
        public String challenges_exp_date;
        @SerializedName("offerexpire_date")
        public String offerexpire_date;
        @SerializedName("duration")
        public String duration;
        @SerializedName("challenge_start_date")
        public String challenge_start_date;
        @SerializedName("visible_date")
        public String visible_date;
        @SerializedName("offer_image")
        public String offer_image;
        @SerializedName("offer_title")
        public String offer_title;
        @SerializedName("offer_detail")
        public String offer_detail;
        @SerializedName("pop_level_1")
        public String pop_level_1;
        @SerializedName("pop_level_2")
        public String pop_level_2;
        @SerializedName("pop_level_3")
        public String pop_level_3;
        @SerializedName("pop_level_4")
        public String pop_level_4;
        @SerializedName("pop_level_5")
        public String pop_level_5;
        @SerializedName("pop_level_text1")
        public String pop_level_text1;
        @SerializedName("pop_level_text2")
        public String pop_level_text2;
        @SerializedName("pop_level_text3")
        public String pop_level_text3;
        @SerializedName("pop_level_text4")
        public String pop_level_text4;
        @SerializedName("pop_level_text5")
        public String pop_level_text5;
        @SerializedName("first_period_notification")
        public String first_period_notification;
        @SerializedName("second_period_notification")
        public String second_period_notification;
        @SerializedName("third_period_notification")
        public String third_period_notification;
        @SerializedName("forth_period_notification")
        public String forth_period_notification;
        @SerializedName("fifth_period_notification")
        public String fifth_period_notification;
        @SerializedName("sixth_period_notification")
        public String sixth_period_notification;
        @SerializedName("limit_win_user")
        public String limit_win_user;
        @SerializedName("first_upcoming_reminder")
        public String first_upcoming_reminder;
        @SerializedName("second_upcoming_reminder")
        public String second_upcoming_reminder;
        @SerializedName("third_upcoming_reminder")
        public String third_upcoming_reminder;
        @SerializedName("first_upcoming_status")
        public String first_upcoming_status;
        @SerializedName("second_upcoming_status")
        public String second_upcoming_status;
        @SerializedName("third_upcoming_status")
        public String third_upcoming_status;
        @SerializedName("first_claim_reminder")
        public String first_claim_reminder;
        @SerializedName("second_claim_reminder")
        public String second_claim_reminder;
        @SerializedName("third_claim_reminder")
        public String third_claim_reminder;
        @SerializedName("first_period_text")
        public String first_period_text;
        @SerializedName("second_period_text")
        public String second_period_text;
        @SerializedName("third_period_text")
        public String third_period_text;
        @SerializedName("forth_period_text")
        public String forth_period_text;
        @SerializedName("fifth_period_text")
        public String fifth_period_text;
        @SerializedName("sixth_period_text")
        public String sixth_period_text;
        @SerializedName("initial_reward")
        public String initial_reward;
        @SerializedName("max_initial_reward_user")
        public String max_initial_reward_user;
        @SerializedName("initial_reward_used")
        public String initial_reward_used;
        @SerializedName("win_type")
        public String win_type;
        @SerializedName("max_limit_ch_user")
        public String max_limit_ch_user;
        @SerializedName("count_ch_user")
        public String count_ch_user;
        @SerializedName("reward_hyperlink_text")
        public String reward_hyperlink_text;
        @SerializedName("reward_hyperlink")
        public String reward_hyperlink;
        @SerializedName("selected_win_user_message")
        public String selected_win_user_message;
        @SerializedName("not_selected_user_message")
        public String not_selected_user_message;
        @SerializedName("manually_wait_title")
        public String manually_wait_title;
        @SerializedName("manually_wait_message")
        public String manually_wait_message;
        @SerializedName("accept_flag")
        public String accept_flag;
    }

    public static class Level_data implements Serializable {
        @SerializedName("badges_level_id")
        public String badges_level_id;
        @SerializedName("level_number")
        public String level_number;
        @SerializedName("level_name")
        public String level_name;
        @SerializedName("level_subtitle")
        public String level_subtitle;
        @SerializedName("level_description")
        public String level_description;
        @SerializedName("level_type")
        public String level_type;
        @SerializedName("level_value")
        public String level_value;
        @SerializedName("second_level_value")
        public String second_level_value;
        @SerializedName("duration")
        public String duration;
        @SerializedName("level_image")
        public String level_image;
        @SerializedName("level_start_date")
        public String level_start_date;
        @SerializedName("third_level_value")
        public String third_level_value;
        @SerializedName("forth_level_value")
        public String forth_level_value;
    }

    public static class Data implements Serializable {
        @SerializedName("challenge_data")
        public List<Challenge_data> challenge_data;
        @SerializedName("level_data")
        public List<Level_data> level_data;
    }
}
