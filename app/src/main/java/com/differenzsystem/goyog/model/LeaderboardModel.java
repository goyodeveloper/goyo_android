package com.differenzsystem.goyog.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 3/10/16.
 */
public class LeaderboardModel {
    private String message;

    private boolean flag;

    private List<LeaderboardDataModel> data = new ArrayList<LeaderboardDataModel>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<LeaderboardDataModel> getData() {
        return data;
    }

    public void setData(List<LeaderboardDataModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", flag = " + flag + ", data = " + data + "]";
    }
}
