package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 8/31/16.
 */

public class ValidicSummaryModel {
    private String end_date;
    private String start_date;
    private int results;

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public int getResults() {
        return results;
    }

    public void setResults(int results) {
        this.results = results;
    }
}
