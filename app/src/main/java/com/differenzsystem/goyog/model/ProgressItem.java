package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public class ProgressItem {
    public int color;
    public float progressItemPercentage;
}
