package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public class HintModel {

    public final String title;
    public final String description;
    public final Integer filePath;

    public HintModel(String title, String description, Integer filePath) {
        this.title = title;
        this.description = description;
        this.filePath = filePath;
    }
}
