package com.differenzsystem.goyog.model;

import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 8/29/16.
 */

public class ValidicFitnessModel {
    private ArrayList<Fitness> fitness;


    public ArrayList<Fitness> getFitness() {
        return fitness;
    }

    public void setFitness(ArrayList<Fitness> fitness) {
        this.fitness = fitness;
    }


    public class Fitness {
        private int average_heart_rate;
        private String last_updated;
        private String timestamp;
        private ArrayList<ActivityLevel> activity_level;
        private ArrayList<HeartRateZone> heart_rate_zones;

        public int getAverage_heart_rate() {
            return average_heart_rate;
        }

        public void setAverage_heart_rate(int average_heart_rate) {
            this.average_heart_rate = average_heart_rate;
        }

        public ArrayList<ActivityLevel> getActivity_level() {
            return activity_level;
        }

        public void setActivity_level(ArrayList<ActivityLevel> activity_level) {
            this.activity_level = activity_level;
        }

        public ArrayList<HeartRateZone> getHeart_rate_zones() {
            return heart_rate_zones;
        }

        public void setHeart_rate_zones(ArrayList<HeartRateZone> heart_rate_zones) {
            this.heart_rate_zones = heart_rate_zones;
        }

        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }

    public class ActivityLevel {
        private String minutes;
        private String name;

        public String getMinutes() {
            return minutes;
        }

        public void setMinutes(String minutes) {
            this.minutes = minutes;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class HeartRateZone {
        private String max;
        private String min;
        private String minutes;
        private String name;

        public String getMax() {
            return max;
        }

        public void setMax(String max) {
            this.max = max;
        }

        public String getMin() {
            return min;
        }

        public void setMin(String min) {
            this.min = min;
        }

        public String getMinutes() {
            return minutes;
        }

        public void setMinutes(String minutes) {
            this.minutes = minutes;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
