package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 8/16/16.
 */

public class LoginModel {
    private String user_id;
    private String validic_id;
    private String validic_access_token;
    private String full_name;
    private String email;
    private String accesstoken;
    private String password;
    private String connect_flag;
    private String profile_image;
    private String registration_date;
    private String badges_level_id;
    private String points;
    private String medals;
    private String last_update_date;
    private String device_token;

    //change
    private String first_name;
    private String last_name;
    private String other_names;
    private String address;
    private String contact_number;
    private String dob;
    private String nic;

    private String marital_status;
    private String no_of_children;
    private String gender;
    private String occupation;
    //private String medals;//leader_badges_points,profile_image

    private String nominee_address;
    private String nominee_name;
    private String nominee_nic;
    private String nominee_number;


    private String timezone;
    private String device_type;
    private String user_type;
    private String mac_id;
    private String facebook_id;
    private String user_exp_date;

    private String height;
    private String height_scale;
    private String weight;
    private String weight_scale;
    private String led_visible_status;


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getValidic_id() {
        return validic_id;
    }

    public void setValidic_id(String validic_id) {
        this.validic_id = validic_id;
    }

    public String getValidic_access_token() {
        return validic_access_token;
    }

    public void setValidic_access_token(String validic_access_token) {
        this.validic_access_token = validic_access_token;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccesstoken() {
        return accesstoken;
    }

    public void setAccesstoken(String accesstoken) {
        this.accesstoken = accesstoken;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConnect_flag() {
        return connect_flag;
    }

    public void setConnect_flag(String connect_flag) {
        this.connect_flag = connect_flag;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(String registration_date) {
        this.registration_date = registration_date;
    }

    public String getBadges_level_id() {
        return badges_level_id;
    }

    public void setBadges_level_id(String badges_level_id) {
        this.badges_level_id = badges_level_id;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getMedals() {
        return medals;
    }

    public void setMedals(String medals) {
        this.medals = medals;
    }

    public String getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(String last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    //change

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getNominee_address() {
        return nominee_address;
    }

    public void setNominee_address(String nominee_address) {
        this.nominee_address = nominee_address;
    }

    public String getNominee_name() {
        return nominee_name;
    }

    public void setNominee_name(String nominee_name) {
        this.nominee_name = nominee_name;
    }

    public String getNominee_nic() {
        return nominee_nic;
    }

    public void setNominee_nic(String nominee_nic) {
        this.nominee_nic = nominee_nic;
    }

    public String getNominee_number() {
        return nominee_number;
    }

    public void setNominee_number(String nominee_number) {
        this.nominee_number = nominee_number;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOther_names() {
        return other_names;
    }

    public void setOther_names(String other_names) {
        this.other_names = other_names;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getMac_id() {
        return mac_id;
    }

    public void setMac_id(String mac_id) {
        this.mac_id = mac_id;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getUser_exp_date() {
        return user_exp_date;
    }

    public void setUser_exp_date(String user_exp_date) {
        this.user_exp_date = user_exp_date;
    }

    public String getNo_of_children() {
        return no_of_children;
    }

    public void setNo_of_children(String no_of_children) {
        this.no_of_children = no_of_children;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHeight_scale() {
        return height_scale;
    }

    public void setHeight_scale(String height_scale) {
        this.height_scale = height_scale;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeight_scale() {
        return weight_scale;
    }

    public void setWeight_scale(String weight_scale) {
        this.weight_scale = weight_scale;
    }

    public String getLed_visible_status() {
        return led_visible_status;
    }

    public void setLed_visible_status(String led_visible_status) {
        this.led_visible_status = led_visible_status;
    }
}
