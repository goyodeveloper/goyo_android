package com.differenzsystem.goyog.model;

import java.io.Serializable;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public class ChallengesModel implements Serializable {

    private String challenge_id;
    private String name;
    private String description;
    private String image;
    private String type;
    private String qrcode;
    private String point;
    private String date;
    private String status;
    private String sectionName;
    private boolean isSection;
    private String week_flag;
    private String duration;
    private String challenge_value;
    private String second_challenge_value;
    private String third_challenge_value;
    private String fourth_challenge_value;
    private String challenge_type;
    private String accept_date;
    private String accept_flag;
    private String challenges_exp_date;
    private String offerexpire_date;
    private String subtitle;
    private String challenge_start_date;
    private String remain_result;
    private String thumb_image;
    private String banner_flag;
    private String reward_hyperlink_text;
    private String reward_hyperlink;


    public ChallengesModel() {
    }

    public ChallengesModel(String challenge_id, String name, String description, String image, String type, String qrcode, String point, String date, String duration, String challenge_value, String second_challenge_value, String third_challenge_value, String fourth_challenge_value, String challenge_type, String challenges_exp_date, String offerexpire_date, String subtitle, String challenge_start_date, String thumb_image, String reward_hyperlink_text, String reward_hyperlink, String accept_flag) {
        this.challenge_id = challenge_id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.type = type;
        this.qrcode = qrcode;
        this.point = point;
        this.date = date;
        this.duration = duration;
        this.challenge_value = challenge_value;
        this.second_challenge_value = second_challenge_value;
        this.third_challenge_value = third_challenge_value;
        this.fourth_challenge_value = fourth_challenge_value;
        this.challenge_type = challenge_type;
        this.challenges_exp_date = challenges_exp_date;
        this.offerexpire_date = offerexpire_date;
        this.subtitle = subtitle;
        this.challenge_start_date = challenge_start_date;
        this.thumb_image = thumb_image;
        this.reward_hyperlink_text = reward_hyperlink_text;
        this.reward_hyperlink = reward_hyperlink;
        this.accept_flag = accept_flag;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getAccept_flag() {
        return accept_flag;
    }

    public String getRemain_result() {
        return remain_result;
    }

    public void setRemain_result(String remain_result) {
        this.remain_result = remain_result;
    }

    public void setAccept_flag(String accept_flag) {
        this.accept_flag = accept_flag;
    }

    public String getChallenge_start_date() {
        return challenge_start_date;
    }

    public void setChallenge_start_date(String challenge_start_date) {
        this.challenge_start_date = challenge_start_date;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getChallenge_value() {
        return challenge_value;
    }

    public void setChallenge_value(String challenge_value) {
        this.challenge_value = challenge_value;
    }

    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public boolean isSection() {
        return isSection;
    }

    public void setSection(boolean section) {
        isSection = section;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWeek_flag() {
        return week_flag;
    }

    public void setWeek_flag(String week_flag) {
        this.week_flag = week_flag;
    }

    public String getOfferexpire_date() {
        return offerexpire_date;
    }

    public void setOfferexpire_date(String offerexpire_date) {
        this.offerexpire_date = offerexpire_date;
    }

    public String getChallenge_type() {
        return challenge_type;
    }

    public void setChallenge_type(String challenge_type) {
        this.challenge_type = challenge_type;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSecond_challenge_value() {
        return second_challenge_value;
    }

    public String getAccept_date() {
        return accept_date;
    }

    public void setAccept_date(String accept_date) {
        this.accept_date = accept_date;
    }

    public void setSecond_challenge_value(String second_challenge_value) {
        this.second_challenge_value = second_challenge_value;
    }

    public String getBanner_flag() {
        return banner_flag;
    }

    public void setBanner_flag(String banner_flag) {
        this.banner_flag = banner_flag;
    }

    public String getFourth_challenge_value() {
        return fourth_challenge_value;
    }

    public void setFourth_challenge_value(String fourth_challenge_value) {
        this.fourth_challenge_value = fourth_challenge_value;
    }

    public String getThird_challenge_value() {
        return third_challenge_value;
    }

    public void setThird_challenge_value(String third_challenge_value) {
        this.third_challenge_value = third_challenge_value;
    }

    public String getReward_hyperlink() {
        return reward_hyperlink;
    }

    public void setReward_hyperlink(String reward_hyperlink) {
        this.reward_hyperlink = reward_hyperlink;
    }

    public String getReward_hyperlink_text() {
        return reward_hyperlink_text;
    }

    public void setReward_hyperlink_text(String reward_hyperlink_text) {
        this.reward_hyperlink_text = reward_hyperlink_text;
    }

    public String getChallenges_exp_date() {
        return challenges_exp_date;
    }

    public void setChallenges_exp_date(String challenges_exp_date) {
        this.challenges_exp_date = challenges_exp_date;
    }
}
