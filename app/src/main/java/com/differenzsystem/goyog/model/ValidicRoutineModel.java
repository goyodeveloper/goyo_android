package com.differenzsystem.goyog.model;

import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 8/29/16.
 */

public class ValidicRoutineModel {
    private ArrayList<Routine> routine;


    public ArrayList<Routine> getRoutine() {
        return routine;
    }

    public void setRoutine(ArrayList<Routine> routine) {
        this.routine = routine;
    }


    public class Routine {

        private String last_updated;
        private String timestamp;
        private int calories_burned;
        private int steps;
        private double distance;


        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public int getCalories_burned() {
            return calories_burned;
        }

        public void setCalories_burned(int calories_burned) {
            this.calories_burned = calories_burned;
        }

        public int getSteps() {
            return steps;
        }

        public void setSteps(int steps) {
            this.steps = steps;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }

}
