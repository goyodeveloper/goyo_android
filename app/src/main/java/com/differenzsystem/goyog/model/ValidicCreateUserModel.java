package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 8/27/16.
 */

public class ValidicCreateUserModel {
    /*
        {
            "code":201,
                "message":"Ok",
                "user":{
            "_id":"57c109c99ab1636484000439",
                    "uid":"0",
                    "access_token":"ESxvV4ArWdQspSoY42GX",
                    "profile":null
        }
        }    */
    private String _id;
    private String uid;
    private String access_token;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
