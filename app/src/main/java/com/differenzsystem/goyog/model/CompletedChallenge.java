
package com.differenzsystem.goyog.model;

public class CompletedChallenge {

    private String offer_id;

    private String status;

    private String claim_date;

    private String accept_date;

    private String challenge_value;

    private String image;

    private String thumb_image;

    private String offer_description;

    private String visible_date;

    private String date;

    private String type;

    private String second_challenge_type;

    private String challenge_start_date;

    private String completed_date;

    private String point;

    private String duration;

    private String challenge_id;

    private String timezone;

    private String offerexpire_date;

    private String description;

    private String qrcode;

    private String name;

    private String user_id;

    private String subtitle;

    private String second_challenge_value;

    private String challenge_type;

    private String offer_title;

    private String offer_detail;

    public String getOffer_title() {
        return offer_title;
    }

    public String getOffer_detail() {
        return offer_detail;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClaim_date() {
        return claim_date;
    }

    public void setClaim_date(String claim_date) {
        this.claim_date = claim_date;
    }

    public String getAccept_date() {
        return accept_date;
    }

    public void setAccept_date(String accept_date) {
        this.accept_date = accept_date;
    }

    public String getChallenge_value() {
        return challenge_value;
    }

    public void setChallenge_value(String challenge_value) {
        this.challenge_value = challenge_value;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOffer_description() {
        return offer_description;
    }

    public void setOffer_description(String offer_description) {
        this.offer_description = offer_description;
    }

    public String getVisible_date() {
        return visible_date;
    }

    public void setVisible_date(String visible_date) {
        this.visible_date = visible_date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSecond_challenge_type() {
        return second_challenge_type;
    }

    public void setSecond_challenge_type(String second_challenge_type) {
        this.second_challenge_type = second_challenge_type;
    }

    public String getChallenge_start_date() {
        return challenge_start_date;
    }

    public void setChallenge_start_date(String challenge_start_date) {
        this.challenge_start_date = challenge_start_date;
    }

    public String getCompleted_date() {
        return completed_date;
    }

    public void setCompleted_date(String completed_date) {
        this.completed_date = completed_date;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getOfferexpire_date() {
        return offerexpire_date;
    }

    public void setOfferexpire_date(String offerexpire_date) {
        this.offerexpire_date = offerexpire_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSecond_challenge_value() {
        return second_challenge_value;
    }

    public void setSecond_challenge_value(String second_challenge_value) {
        this.second_challenge_value = second_challenge_value;
    }

    public String getChallenge_type() {
        return challenge_type;
    }

    public void setChallenge_type(String challenge_type) {
        this.challenge_type = challenge_type;
    }

    @Override
    public String toString() {
        return "ClassPojo [offer_id = " + offer_id + ", status = " + status + ", claim_date = " + claim_date + ", accept_date = " + accept_date + ", challenge_value = " + challenge_value + ", image = " + image + ", offer_description = " + offer_description + ", visible_date = " + visible_date + ", date = " + date + ", type = " + type + ", second_challenge_type = " + second_challenge_type + ", challenge_start_date = " + challenge_start_date + ", completed_date = " + completed_date + ", point = " + point + ", duration = " + duration + ", challenge_id = " + challenge_id + ", timezone = " + timezone + ", offerexpire_date = " + offerexpire_date + ", description = " + description + ", qrcode = " + qrcode + ", name = " + name + ", user_id = " + user_id + ", second_challenge_value = " + second_challenge_value + ", challenge_type = " + challenge_type + "]";
    }

}
