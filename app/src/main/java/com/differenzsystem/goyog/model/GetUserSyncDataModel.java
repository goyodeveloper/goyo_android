package com.differenzsystem.goyog.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Union Assurance PLC on 7/18/17.
 */

public class GetUserSyncDataModel implements Serializable{


    @SerializedName("flag")
    public boolean flag;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public Data data;
    @SerializedName("is_policy_user")
    public int is_policy_user;

    public static class Data implements Serializable{
        @SerializedName("user_id")
        public String user_id;
        @SerializedName("proposal_no")
        public String proposal_no;
        @SerializedName("proposal_url")
        public String proposal_url;
        @SerializedName("sync_flag")
        public String sync_flag;
        @SerializedName("sync_timestamp")
        public String sync_timestamp;
        @SerializedName("email_flag")
        public String email_flag;
    }
}
