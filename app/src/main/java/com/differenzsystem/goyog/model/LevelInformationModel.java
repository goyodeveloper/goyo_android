package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 8/16/16.
 */

public class LevelInformationModel {
    private String badges_level_id;
    private String level_number;
    private String level_name;
    private String level_description;
    private String level_type;
    private String level_value;
    private String second_level_type;
    private String second_level_value;
    private String level_subtitle;
    private String duration;
    private String level_image;
    private String calories;
    private String distance;
    private String steps;
    private String sleep;
    private String start_date;
    private String complete_date;
    private String resting_heart_rate;
    private String remain_time;
    private String remain_result;
    private String level_start_date;
    private String third_level_value;//third_level_value
    private String forth_level_value;//forth_level_value


    public String getRemain_time() {
        return remain_time;
    }

    public void setRemain_time(String remain_time) {
        this.remain_time = remain_time;
    }

    public String getRemain_result() {
        return remain_result;
    }

    public void setRemain_result(String remain_result) {
        this.remain_result = remain_result;
    }

    public String getBadges_level_id() {
        return badges_level_id;
    }

    public void setBadges_level_id(String badges_level_id) {
        this.badges_level_id = badges_level_id;
    }

    public String getLevel_number() {
        return level_number;
    }

    public void setLevel_number(String level_number) {
        this.level_number = level_number;
    }

    public String getLevel_name() {
        return level_name;
    }

    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }

    public String getLevel_description() {
        return level_description;
    }

    public void setLevel_description(String level_description) {
        this.level_description = level_description;
    }

    public String getLevel_type() {
        return level_type;
    }

    public void setLevel_type(String level_type) {
        this.level_type = level_type;
    }

    public String getLevel_value() {
        return level_value;
    }

    public void setLevel_value(String level_value) {
        this.level_value = level_value;
    }

    public String getSecond_level_type() {
        return second_level_type;
    }

    public void setSecond_level_type(String second_level_type) {
        this.second_level_type = second_level_type;
    }

    public String getSecond_level_value() {
        return second_level_value;
    }

    public void setSecond_level_value(String second_level_value) {
        this.second_level_value = second_level_value;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getLevel_image() {
        return level_image;
    }

    public void setLevel_image(String level_image) {
        this.level_image = level_image;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getComplete_date() {
        return complete_date;
    }

    public void setComplete_date(String complete_date) {
        this.complete_date = complete_date;
    }

    public String getResting_heart_rate() {
        return resting_heart_rate;
    }

    public void setResting_heart_rate(String resting_heart_rate) {
        this.resting_heart_rate = resting_heart_rate;
    }

    public String getLevel_subtitle() {
        return level_subtitle;
    }

    public void setLevel_subtitle(String level_subtitle) {
        this.level_subtitle = level_subtitle;
    }

    public String getLevel_start_date() {
        return level_start_date;
    }

    public void setLevel_start_date(String level_start_date) {
        this.level_start_date = level_start_date;
    }

    public String getSleep() {
        return sleep;
    }

    public void setSleep(String sleep) {
        this.sleep = sleep;
    }

    public String getForth_level_value() {
        return forth_level_value;
    }

    public void setForth_level_value(String forth_level_value) {
        this.forth_level_value = forth_level_value;
    }

    public String getThird_level_value() {
        return third_level_value;
    }

    public void setThird_level_value(String third_level_value) {
        this.third_level_value = third_level_value;
    }

}
