package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 28/10/16.
 */

public class ChartDataModel {

    private Integer challengetype;
    private Integer totalval;
    private Integer sectotalval;
    private Integer thirdtotalval;
    private Integer fourthtotalval;
    private Integer val;
    private Integer secval;
    private Integer thirdval;
    private Integer fourthval;
    private ChallengesModel challengesModels;
    private LevelInformationModel levelInformationModel;




    public Integer getChallengetype() {
        return challengetype;
    }

    public void setChallengetype(Integer challengetype) {
        this.challengetype = challengetype;
    }

    public Integer getSectotalval() {
        return sectotalval;
    }

    public void setSectotalval(Integer sectotalval) {
        this.sectotalval = sectotalval;
    }

    public Integer getSecval() {
        return secval;
    }

    public void setSecval(Integer secval) {
        this.secval = secval;
    }

    public Integer getTotalval() {
        return totalval;
    }

    public void setTotalval(Integer totalval) {
        this.totalval = totalval;
    }

    public Integer getVal() {
        return val;
    }

    public void setVal(Integer val) {
        this.val = val;
    }

    public ChallengesModel getChallengesModels() {
        return challengesModels;
    }

    public void setChallengesModels(ChallengesModel challengesModels) {
        this.challengesModels = challengesModels;
    }

    public LevelInformationModel getLevelInformationModel() {
        return levelInformationModel;
    }

    public void setLevelInformationModel(LevelInformationModel levelInformationModel) {
        this.levelInformationModel = levelInformationModel;
    }


    public Integer getFourthtotalval() {
        return fourthtotalval;
    }

    public void setFourthtotalval(Integer fourthtotalval) {
        this.fourthtotalval = fourthtotalval;
    }

    public Integer getThirdtotalval() {
        return thirdtotalval;
    }

    public void setThirdtotalval(Integer thirdtotalval) {
        this.thirdtotalval = thirdtotalval;
    }

    public Integer getThirdval() {
        return thirdval;
    }

    public void setThirdval(Integer thirdval) {
        this.thirdval = thirdval;
    }

    public Integer getFourthval() {
        return fourthval;
    }

    public void setFourthval(Integer fourthval) {
        this.fourthval = fourthval;
    }
}
