package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public class InviteModel {

    private String contactName;
    private String email;
    private boolean isNumber;
    private boolean isChecked;
    private boolean isInvited;

    public InviteModel(String contactName, String email,
                       boolean isNumber, boolean isChecked, boolean isInvited) {

        this.contactName = contactName;
        this.email = email;
        this.isNumber = isNumber;
        this.isChecked = isChecked;
        this.isInvited = isInvited;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isNumber() {
        return isNumber;
    }

    public void setNumber(boolean number) {
        isNumber = number;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public void setInvited(boolean invited) {
        isInvited = invited;
    }
}
