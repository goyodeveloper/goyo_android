
package com.differenzsystem.goyog.model;

public class CompletedBadge {

    private String level_type;

    private String status;

    private String level_value;

    private String second_level_type;

    private String complete_date;

    private String second_level_value;

    private String id;

    private String level_name;

    private String level_start_date;

    private String duration;

    private String level_subtitle;

    private String level_image;

    private String badge_id;

    private String badges_level_id;

    private String user_id;

    private String level_number;

    private String level_description;

    private String start_date;

    public String getLevel_type() {
        return level_type;
    }

    public void setLevel_type(String level_type) {
        this.level_type = level_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLevel_value() {
        return level_value;
    }

    public void setLevel_value(String level_value) {
        this.level_value = level_value;
    }

    public String getSecond_level_type() {
        return second_level_type;
    }

    public void setSecond_level_type(String second_level_type) {
        this.second_level_type = second_level_type;
    }

    public String getComplete_date() {
        return complete_date;
    }

    public void setComplete_date(String complete_date) {
        this.complete_date = complete_date;
    }

    public String getSecond_level_value() {
        return second_level_value;
    }

    public void setSecond_level_value(String second_level_value) {
        this.second_level_value = second_level_value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel_name() {
        return level_name;
    }

    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }

    public String getLevel_start_date() {
        return level_start_date;
    }

    public void setLevel_start_date(String level_start_date) {
        this.level_start_date = level_start_date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getLevel_subtitle() {
        return level_subtitle;
    }

    public void setLevel_subtitle(String level_subtitle) {
        this.level_subtitle = level_subtitle;
    }

    public String getLevel_image() {
        return level_image;
    }

    public void setLevel_image(String level_image) {
        this.level_image = level_image;
    }

    public String getBadge_id() {
        return badge_id;
    }

    public void setBadge_id(String badge_id) {
        this.badge_id = badge_id;
    }

    public String getBadges_level_id() {
        return badges_level_id;
    }

    public void setBadges_level_id(String badges_level_id) {
        this.badges_level_id = badges_level_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLevel_number() {
        return level_number;
    }

    public void setLevel_number(String level_number) {
        this.level_number = level_number;
    }

    public String getLevel_description() {
        return level_description;
    }

    public void setLevel_description(String level_description) {
        this.level_description = level_description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    @Override
    public String toString() {
        return "ClassPojo [level_type = " + level_type + ", status = " + status + ", level_value = " + level_value + ", second_level_type = " + second_level_type + ", complete_date = " + complete_date + ", second_level_value = " + second_level_value + ", id = " + id + ", level_name = " + level_name + ", level_start_date = " + level_start_date + ", duration = " + duration + ", level_subtitle = " + level_subtitle + ", level_image = " + level_image + ", badge_id = " + badge_id + ", badges_level_id = " + badges_level_id + ", user_id = " + user_id + ", level_number = " + level_number + ", level_description = " + level_description + ", start_date = " + start_date + "]";
    }
}
