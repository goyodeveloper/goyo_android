package com.differenzsystem.goyog.model;

/**
 * Created on 14/10/16.
 */
public class TimeLine {
    private String completed_date;
    private String title;
    private String subtitle;
    private String type;
    private String month;
    private String monthdate;
    private String thumb_image;
    private String offer_title;

    public TimeLine(String completed_date, String title, String subtitle, String type, String month, String monthdate, String thumb_image, String offer_title) {
        this.completed_date = completed_date;
        this.title = title;
        this.subtitle = subtitle;
        this.type = type;
        this.month = month;
        this.monthdate = monthdate;
        this.thumb_image = thumb_image;
        this.offer_title = offer_title;
    }

    public String getOffer_title() {
        return offer_title;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getMonthdate() {
        return monthdate;
    }

    public void setMonthdate(String monthdate) {
        this.monthdate = monthdate;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getCompleted_date() {
        return completed_date;
    }

    public void setCompleted_date(String completed_date) {
        this.completed_date = completed_date;
    }
}
