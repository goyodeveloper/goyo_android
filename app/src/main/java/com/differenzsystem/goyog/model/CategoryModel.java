package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public class CategoryModel {

    public final float item1;
    public final String item2;

    public CategoryModel(float item1, String item2) {
        this.item1 = item1;
        this.item2 = item2;
    }
}
