package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 26/11/16.
 */

public class ChallangesPopupModel {

    private String pop_level_4;

    private String offer_title;

    private String pop_level_3;

    private String pop_level_2;

    private String thumb_image;

    private String pop_level_1;

    private String offer_id;

    private String notification_status;

    private String claim_date;

    private String pop_level_5;

    private String accept_date;

    private String challenge_value;

    private String visible_date;

    private String pop_level_status;

    private String type;

    private String date;

    private String challenge_start_date;

    private String timezone;

    private String challenge_id;

    private String description;

    private String name;

    private String user_id;

    private String popcounter;

    private String second_challenge_value;

    private String offer_image;

    private String offer_detail;

    private String status;

    private String offer_description;

    private String image;

    private String second_challenge_type;

    private String completed_date;

    private String pop_level_text1;

    private String point;

    private String duration;

    private String pop_level_text4;

    private String pop_level_text5;

    private String pop_level_text2;

    private String offerexpire_date;

    private String pop_level_text3;

    private String qrcode;

    private String subtitle;

    private String challenge_type;

    private String third_challenge_value;

    private String fourth_challenge_value;

    private String reward_hyperlink_text;

    private String reward_hyperlink;

    public String getPop_level_4() {
        return pop_level_4;
    }

    public void setPop_level_4(String pop_level_4) {
        this.pop_level_4 = pop_level_4;
    }

    public String getOffer_title() {
        return offer_title;
    }

    public void setOffer_title(String offer_title) {
        this.offer_title = offer_title;
    }

    public String getPop_level_3() {
        return pop_level_3;
    }

    public void setPop_level_3(String pop_level_3) {
        this.pop_level_3 = pop_level_3;
    }

    public String getPop_level_2() {
        return pop_level_2;
    }

    public void setPop_level_2(String pop_level_2) {
        this.pop_level_2 = pop_level_2;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getPop_level_1() {
        return pop_level_1;
    }

    public void setPop_level_1(String pop_level_1) {
        this.pop_level_1 = pop_level_1;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    public String getNotification_status() {
        return notification_status;
    }

    public void setNotification_status(String notification_status) {
        this.notification_status = notification_status;
    }

    public String getClaim_date() {
        return claim_date;
    }

    public void setClaim_date(String claim_date) {
        this.claim_date = claim_date;
    }

    public String getPop_level_5() {
        return pop_level_5;
    }

    public void setPop_level_5(String pop_level_5) {
        this.pop_level_5 = pop_level_5;
    }

    public String getAccept_date() {
        return accept_date;
    }

    public void setAccept_date(String accept_date) {
        this.accept_date = accept_date;
    }

    public String getChallenge_value() {
        return challenge_value;
    }

    public void setChallenge_value(String challenge_value) {
        this.challenge_value = challenge_value;
    }

    public String getVisible_date() {
        return visible_date;
    }

    public void setVisible_date(String visible_date) {
        this.visible_date = visible_date;
    }

    public String getPop_level_status() {
        return pop_level_status;
    }

    public void setPop_level_status(String pop_level_status) {
        this.pop_level_status = pop_level_status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getChallenge_start_date() {
        return challenge_start_date;
    }

    public void setChallenge_start_date(String challenge_start_date) {
        this.challenge_start_date = challenge_start_date;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPopcounter() {
        return popcounter;
    }

    public void setPopcounter(String popcounter) {
        this.popcounter = popcounter;
    }

    public String getSecond_challenge_value() {
        return second_challenge_value;
    }

    public void setSecond_challenge_value(String second_challenge_value) {
        this.second_challenge_value = second_challenge_value;
    }

    public String getOffer_image() {
        return offer_image;
    }

    public void setOffer_image(String offer_image) {
        this.offer_image = offer_image;
    }

    public String getOffer_detail() {
        return offer_detail;
    }

    public void setOffer_detail(String offer_detail) {
        this.offer_detail = offer_detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOffer_description() {
        return offer_description;
    }

    public void setOffer_description(String offer_description) {
        this.offer_description = offer_description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSecond_challenge_type() {
        return second_challenge_type;
    }

    public void setSecond_challenge_type(String second_challenge_type) {
        this.second_challenge_type = second_challenge_type;
    }

    public String getCompleted_date() {
        return completed_date;
    }

    public void setCompleted_date(String completed_date) {
        this.completed_date = completed_date;
    }

    public String getPop_level_text1() {
        return pop_level_text1;
    }

    public void setPop_level_text1(String pop_level_text1) {
        this.pop_level_text1 = pop_level_text1;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPop_level_text4() {
        return pop_level_text4;
    }

    public void setPop_level_text4(String pop_level_text4) {
        this.pop_level_text4 = pop_level_text4;
    }

    public String getPop_level_text5() {
        return pop_level_text5;
    }

    public void setPop_level_text5(String pop_level_text5) {
        this.pop_level_text5 = pop_level_text5;
    }

    public String getPop_level_text2() {
        return pop_level_text2;
    }

    public void setPop_level_text2(String pop_level_text2) {
        this.pop_level_text2 = pop_level_text2;
    }

    public String getOfferexpire_date() {
        return offerexpire_date;
    }

    public void setOfferexpire_date(String offerexpire_date) {
        this.offerexpire_date = offerexpire_date;
    }

    public String getPop_level_text3() {
        return pop_level_text3;
    }

    public void setPop_level_text3(String pop_level_text3) {
        this.pop_level_text3 = pop_level_text3;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getChallenge_type() {
        return challenge_type;
    }

    public void setChallenge_type(String challenge_type) {
        this.challenge_type = challenge_type;
    }

    public String getFourth_challenge_value() {
        return fourth_challenge_value;
    }

    public void setFourth_challenge_value(String fourth_challenge_value) {
        this.fourth_challenge_value = fourth_challenge_value;
    }

    public String getThird_challenge_value() {
        return third_challenge_value;
    }

    public void setThird_challenge_value(String third_challenge_value) {
        this.third_challenge_value = third_challenge_value;
    }

    public String getReward_hyperlink() {
        return reward_hyperlink;
    }

    public void setReward_hyperlink(String reward_hyperlink) {
        this.reward_hyperlink = reward_hyperlink;
    }

    public String getReward_hyperlink_text() {
        return reward_hyperlink_text;
    }

    public void setReward_hyperlink_text(String reward_hyperlink_text) {
        this.reward_hyperlink_text = reward_hyperlink_text;
    }

    @Override
    public String toString() {
        return "ClassPojo [pop_level_4 = " + pop_level_4 + ", offer_title = " + offer_title + ", pop_level_3 = " + pop_level_3 + ", pop_level_2 = " + pop_level_2 + ", thumb_image = " + thumb_image + ", pop_level_1 = " + pop_level_1 + ", offer_id = " + offer_id + ", notification_status = " + notification_status + ", claim_date = " + claim_date + ", pop_level_5 = " + pop_level_5 + ", accept_date = " + accept_date + ", challenge_value = " + challenge_value + ", visible_date = " + visible_date + ", pop_level_status = " + pop_level_status + ", type = " + type + ", date = " + date + ", challenge_start_date = " + challenge_start_date + ", timezone = " + timezone + ", challenge_id = " + challenge_id + ", description = " + description + ", name = " + name + ", user_id = " + user_id + ", popcounter = " + popcounter + ", second_challenge_value = " + second_challenge_value + ", offer_image = " + offer_image + ", offer_detail = " + offer_detail + ", status = " + status + ", offer_description = " + offer_description + ", image = " + image + ", second_challenge_type = " + second_challenge_type + ", completed_date = " + completed_date + ", pop_level_text1 = " + pop_level_text1 + ", point = " + point + ", duration = " + duration + ", pop_level_text4 = " + pop_level_text4 + ", pop_level_text5 = " + pop_level_text5 + ", pop_level_text2 = " + pop_level_text2 + ", offerexpire_date = " + offerexpire_date + ", pop_level_text3 = " + pop_level_text3 + ", qrcode = " + qrcode + ", subtitle = " + subtitle + ", challenge_type = " + challenge_type + "]";
    }

}
