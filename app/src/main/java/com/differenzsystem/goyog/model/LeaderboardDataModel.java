package com.differenzsystem.goyog.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created on 3/10/16.
 */
public class LeaderboardDataModel implements Serializable{


    @SerializedName("flag")
    public boolean flag;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;

    public static class Data implements Serializable{
        @SerializedName("rank")
        public int rank;
        @SerializedName("user_id")
        public String user_id;
        @SerializedName("total_percentage")
        public String total_percentage;
        @SerializedName("total_minutes")
        public String total_minutes;
        @SerializedName("first_name")
        public String first_name;
        @SerializedName("last_name")
        public String last_name;
        @SerializedName("profile_image")
        public String profile_image;
        @SerializedName("fitness_data")
        public String fitness_data;
    }
}


//public class LeaderboardDataModel {
//    private String medals;
//
//    private String total_steps;
//
//    private String email;
//
//    private String profile_image;
//
//    private String badges_level_id;
//
//    private String full_name;
//
//    private String user_id;
//
//    private String level_name;
//
//    private String first_name;
//    private String last_name;
//
//    public String getUser_id() {
//        return user_id;
//    }
//
//    public void setUser_id(String user_id) {
//        this.user_id = user_id;
//    }
//
//    public String getLevel_name() {
//        return level_name;
//    }
//
//    public void setLevel_name(String level_name) {
//        this.level_name = level_name;
//    }
//
//    public String getMedals() {
//        return medals;
//    }
//
//    public void setMedals(String medals) {
//        this.medals = medals;
//    }
//
//    public String getTotal_steps() {
//        return total_steps + " Steps";
//    }
//
//    public void setTotal_steps(String total_steps) {
//        this.total_steps = total_steps;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getProfile_image() {
//        return profile_image;
//    }
//
//    public void setProfile_image(String profile_image) {
//        this.profile_image = profile_image;
//    }
//
//    public String getBadges_level_id() {
//        return "Level " + badges_level_id;
//    }
//
//    public void setBadges_level_id(String badges_level_id) {
//        this.badges_level_id = badges_level_id;
//    }
//
//    public String getFull_name() {
//        return full_name;
//    }
//
//    public void setFull_name(String full_name) {
//        this.full_name = full_name;
//    }
//
//    public String getFirst_name() {
//        return first_name;
//    }
//
//    public void setFirst_name(String first_name) {
//        this.first_name = first_name;
//    }
//
//    public String getLast_name() {
//        return last_name;
//    }
//
//    public void setLast_name(String last_name) {
//        this.last_name = last_name;
//    }
//
//    @Override
//    public String toString() {
//        return "ClassPojo [medals = " + medals + ", total_steps = " + total_steps + ", email = " + email + ", profile_image = " + profile_image + ", badges_level_id = " + badges_level_id + ", full_name = " + full_name + ", first_name = " + first_name + ", last_name = " + last_name + "]";
//    }
//}
