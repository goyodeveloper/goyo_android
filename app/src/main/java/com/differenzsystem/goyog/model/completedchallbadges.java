
package com.differenzsystem.goyog.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class completedchallbadges {

    private Boolean flag;
    private int challenges_point;
    private List<CompletedChallenge> completedChallenges = new ArrayList<CompletedChallenge>();
    private List<CompletedBadge> completedBadges = new ArrayList<CompletedBadge>();
    private List<TimeLineResult> timeLineResults = new ArrayList<TimeLineResult>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public int getChallenges_point() {
        return challenges_point;
    }

    public void setChallenges_point(int challenges_point) {
        this.challenges_point = challenges_point;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<CompletedChallenge> getCompletedChallenges() {
        return completedChallenges;
    }

    public void setCompletedBadges(List<CompletedBadge> completedBadges) {
        this.completedBadges = completedBadges;
    }

    public List<CompletedBadge> getCompletedBadges() {
        return completedBadges;
    }

    public void setCompletedChallenges(List<CompletedChallenge> completedChallenges) {
        this.completedChallenges = completedChallenges;
    }

    public List<TimeLineResult> getTimeLineResults() {
        return timeLineResults;
    }

    public void setTimeLineResults(List<TimeLineResult> timeLineResults) {
        this.timeLineResults = timeLineResults;
    }

    @Override
    public String toString() {
        return "ClassPojo [challenges_point = " + challenges_point + ", flag = " + flag + ", completed_badges = " + completedBadges + ", completed_challenges = " + completedChallenges + ",timeLineResults = " + timeLineResults + "]";
    }
}
