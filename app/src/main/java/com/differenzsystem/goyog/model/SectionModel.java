package com.differenzsystem.goyog.model;

/**
 * Created by root4 on 13/8/16.
 */

public class SectionModel implements Section {

    private final String title;

    public SectionModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean isSection() {
        return true;
    }
}
