package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public interface Section {
    public boolean isSection();
}
