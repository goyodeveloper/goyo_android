package com.differenzsystem.goyog.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Union Assurance PLC on 7/27/17.
 */

public class NotificationModel implements Serializable{


    @SerializedName("flag")
    public boolean flag;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public List<Data> data;
    @SerializedName("avtar_data")
    public Avtar_data avtar_data;

    public static class Data implements Serializable{
        @SerializedName("id")
        public String id;
        @SerializedName("user_id")
        public String user_id;
        @SerializedName("notification_type")
        public String notification_type;
        @SerializedName("notification_date")
        public String notification_date;
        @SerializedName("notification_message")
        public String notification_message;
        @SerializedName("view_status")
        public String view_status;
    }

    public static class Avtar_data implements Serializable{
        @SerializedName("id")
        public String id;
        @SerializedName("male_avatar")
        public String male_avatar;
        @SerializedName("female_avatar")
        public String female_avatar;
    }
}
