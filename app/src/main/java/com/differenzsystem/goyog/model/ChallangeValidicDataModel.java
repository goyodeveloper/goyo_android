package com.differenzsystem.goyog.model;

/**
 * Created by Union Assurance PLC on 10/3/16.
 */

public class ChallangeValidicDataModel {

    private String calories;
    private String distance;
    private String steps;
    private String resting_heart_rate;


    public String getResting_heart_rate() {
        return resting_heart_rate;
    }

    public void setResting_heart_rate(String resting_heart_rate) {
        this.resting_heart_rate = resting_heart_rate;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }
}
