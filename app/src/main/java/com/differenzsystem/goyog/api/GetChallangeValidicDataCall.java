package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.ChallangeValidicDataModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class GetChallangeValidicDataCall extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnGetChallangeValidicDataListener listener;

    JSONObject postData;
    private boolean isDialog = false;

    //create an listener for success & failure of service call
    public interface OnGetChallangeValidicDataListener {
        /**
         * @param obj return GUserSyncDataModel
         */
        void onSucceedToGetChallangeValidicData(ChallangeValidicDataModel obj);

        /**
         * @param error_msg  return error message
         */
        void onFaildToGetChallangeValidicData(String error_msg);
    }

    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param data     JSON post request
     * @param isDialog boolean for showing progress dialog
     */
    public GetChallangeValidicDataCall(Context context, OnGetChallangeValidicDataListener listener, JSONObject data, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        try {
            // performing network call (service call)
            String url = context.getResources().getString(R.string.live_server_url) + context.getResources().getString(R.string.challenge_validic_detail_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response;// return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            // get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject newJsonObject = jsonObject.getJSONObject(Constant.data);
                    ChallangeValidicDataModel obj = new Gson().fromJson(newJsonObject.toString(), ChallangeValidicDataModel.class);
                    if (obj != null) {
                        listener.onSucceedToGetChallangeValidicData(obj);// returning success listener
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                listener.onFaildToGetChallangeValidicData(res.getMessage());;// returning failure  listener
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {
                UtilsCommon.destroyProgressBar();// destroy dialog
            }
        }
    }
}