package com.differenzsystem.goyog.api;

import android.content.Context;
import android.util.Log;

import com.veryfit.multi.nativedatabase.HealthSportItem;
import com.veryfit.multi.nativedatabase.healthSleep;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Union Assurance PLC on 16/12/16.
 */

public class FitnessTrackerOperations {

    FitnessTrackerOperationslistener listener;
    Context context;
    int steps = 0, cals = 0, dis = 0, hearts = 0, sleep = 0;
    int Startyear, Startmonth, Startday, StartHours, StartMinutes;
    int Endyear, Endmonth, Endday, EndHours, EndMinutes;
    List<ArrayList<HashMap<Integer, HealthSportItem>>> FinalData = new ArrayList<>();
    List<healthSleep> FinalDataSleep = new ArrayList<>();
    int position, positionlast;


    //create an listener for Fitness Tracker Operation
    public interface FitnessTrackerOperationslistener {

        /**
         * @param steps  return total steps from start date to end date
         * @param cals   return total calories from start date to end date
         * @param dis    return total distance from start date to end date
         * @param hearts return total heart rate from start date to end date
         * @param sleep  return total sleep from start date to end date
         */
        public void onSucceedtoFitnessTrackerOperation(int steps, int cals, int dis, int hearts, int sleep);

        // public void onSucceedtoUpdateWiningstatus(ChallengesModel challengesModel);
    }

    /**
     * @param context  get context of particular screen
     * @param listener listener of Fitness Tracker Operation
     */
    public FitnessTrackerOperations(Context context, FitnessTrackerOperationslistener listener) {
        this.context = context;
        this.listener = listener;
    }

    public FitnessTrackerOperations(Context context) {
        this.context = context;
    }

    // perform operation for getting data from fitness band
    private void ChallangesData() {
        if (FinalData != null && FinalData.size() > 0) {
            for (int i = 0; i < FinalData.size(); i++) {
                position = i;
                //positionlast = FinalData.size() - 1;
                if (FinalData.get(i) != null)

                    ChallangesDataHourly(FinalData.get(i));

                if (FinalDataSleep != null && FinalDataSleep.size() > 0) {
                    if (FinalDataSleep.get(i) != null)
                        SleepData(FinalDataSleep.get(i));
                }
            }
            if (listener != null)
                listener.onSucceedtoFitnessTrackerOperation(steps, cals, dis, hearts, sleep);
        } else {
            if (listener != null)
                listener.onSucceedtoFitnessTrackerOperation(steps, cals, dis, hearts, sleep);
        }
    }

    /**
     * @param data find total sleep data
     */
    private void SleepData(healthSleep data) {
        sleep = sleep + data.totalSleepMinutes;
    }

    /**
     * @param data hourly data of particular data and find total health records (steps, calories & distance)
     *             it will calculate all the health records.
     */
    private void ChallangesDataHourly(ArrayList<HashMap<Integer, HealthSportItem>> data) {

        int cal = 0, step = 0, distance = 0, heart = 0;
        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                if (Startmonth == Endmonth) {
                    if (Startday == Endday) {
                        int loop = 1;
                        if (StartHours == EndHours) {
                            if (i == StartHours) {
                                if (StartMinutes >= 0 && StartMinutes <= 15)
                                    loop = 1;
                                else if (StartMinutes > 15 && StartMinutes <= 30)
                                    loop = 2;
                                else if (StartMinutes > 30 && StartMinutes <= 45)
                                    loop = 3;
                                else
                                    loop = 4;

                                int end;
                                if (EndMinutes >= 0 && EndMinutes <= 15)
                                    end = 1;
                                else if (EndMinutes > 15 && EndMinutes <= 30)
                                    end = 2;
                                else if (EndMinutes > 30 && EndMinutes <= 45)
                                    end = 3;
                                else
                                    end = 4;

                                for (int j = loop; j <= end; j++) {
                                    step = step + data.get(i).get(j).getStepCount();
                                    distance = distance + data.get(i).get(j).getDistance();
                                    cal = cal + data.get(i).get(j).getCalory();
                                }
                            }
                        } else if (i >= StartHours) {
                            if (i == StartHours) {

                                if (StartMinutes >= 0 && StartMinutes <= 15)
                                    loop = 1;
                                else if (StartMinutes > 15 && StartMinutes <= 30)
                                    loop = 2;
                                else if (StartMinutes > 30 && StartMinutes <= 45)
                                    loop = 3;
                                else
                                    loop = 4;

                                for (int j = loop; j <= 4; j++) {
                                    step = step + data.get(i).get(j).getStepCount();
                                    distance = distance + data.get(i).get(j).getDistance();
                                    cal = cal + data.get(i).get(j).getCalory();
                                }
                            } else if (i == EndHours) {
                                if (EndMinutes >= 0 && EndMinutes <= 15)
                                    loop = 1;
                                else if (EndMinutes > 15 && EndMinutes <= 30)
                                    loop = 2;
                                else if (EndMinutes > 30 && EndMinutes <= 45)
                                    loop = 3;
                                else
                                    loop = 4;

                                for (int j = 1; j <= loop; j++) {
                                    step = step + data.get(i).get(j).getStepCount();
                                    distance = distance + data.get(i).get(j).getDistance();
                                    cal = cal + data.get(i).get(j).getCalory();
                                }
                            } else {
                                for (int j = loop; j <= 4; j++) {
                                    step = step + data.get(i).get(j).getStepCount();
                                    distance = distance + data.get(i).get(j).getDistance();
                                    cal = cal + data.get(i).get(j).getCalory();
                                }
                            }
                        }
                    } else {
                        int loop = 1;
                        if (position == 0) {
                            if (i >= StartHours) {

                                if (i == StartHours) {

                                    if (StartMinutes >= 0 && StartMinutes <= 15)
                                        loop = 1;
                                    else if (StartMinutes > 15 && StartMinutes <= 30)
                                        loop = 2;
                                    else if (StartMinutes > 30 && StartMinutes <= 45)
                                        loop = 3;
                                    else
                                        loop = 4;
                                }
                                for (int j = loop; j <= 4; j++) {
                                    step = step + data.get(i).get(j).getStepCount();
                                    distance = distance + data.get(i).get(j).getDistance();
                                    cal = cal + data.get(i).get(j).getCalory();
                                }
                            }
                        } else if (position == positionlast - 1) {
                            if (i <= EndHours) {
                                if (i == EndHours) {

                                    if (StartMinutes >= 0 && StartMinutes <= 15)
                                        loop = 1;
                                    else if (StartMinutes > 15 && StartMinutes <= 30)
                                        loop = 2;
                                    else if (StartMinutes > 30 && StartMinutes <= 45)
                                        loop = 3;
                                    else
                                        loop = 4;

                                    for (int j = 1; j <= loop; j++) {
                                        step = step + data.get(i).get(j).getStepCount();
                                        distance = distance + data.get(i).get(j).getDistance();
                                        cal = cal + data.get(i).get(j).getCalory();
                                    }
                                } else {
                                    for (int j = loop; j <= 4; j++) {
                                        step = step + data.get(i).get(j).getStepCount();
                                        distance = distance + data.get(i).get(j).getDistance();
                                        cal = cal + data.get(i).get(j).getCalory();
                                    }
                                }
                            }
                        } else {
                            for (int j = loop; j <= 4; j++) {
                                step = step + data.get(i).get(j).getStepCount();
                                distance = distance + data.get(i).get(j).getDistance();
                                cal = cal + data.get(i).get(j).getCalory();
                            }
                        }
                    }
                } else {
                    int loop = 1;
                    if (position == 0) {
                        if (i >= StartHours) {

                            if (i == StartHours) {

                                if (StartMinutes >= 0 && StartMinutes <= 15)
                                    loop = 1;
                                else if (StartMinutes > 15 && StartMinutes <= 30)
                                    loop = 2;
                                else if (StartMinutes > 30 && StartMinutes <= 45)
                                    loop = 3;
                                else
                                    loop = 4;
                            }
                            for (int j = loop; j <= 4; j++) {
                                step = step + data.get(i).get(j).getStepCount();
                                distance = distance + data.get(i).get(j).getDistance();
                                cal = cal + data.get(i).get(j).getCalory();
                            }
                        }
                    } else if (position == positionlast - 1) {
                        if (i <= EndHours) {
                            if (i == EndHours) {

                                if (StartMinutes >= 0 && StartMinutes <= 15)
                                    loop = 1;
                                else if (StartMinutes > 15 && StartMinutes <= 30)
                                    loop = 2;
                                else if (StartMinutes > 30 && StartMinutes <= 45)
                                    loop = 3;
                                else
                                    loop = 4;

                                for (int j = 1; j <= loop; j++) {
                                    step = step + data.get(i).get(j).getStepCount();
                                    distance = distance + data.get(i).get(j).getDistance();
                                    cal = cal + data.get(i).get(j).getCalory();
                                }
                            } else {
                                for (int j = loop; j <= 4; j++) {
                                    step = step + data.get(i).get(j).getStepCount();
                                    distance = distance + data.get(i).get(j).getDistance();
                                    cal = cal + data.get(i).get(j).getCalory();
                                }
                            }
                        }
                    } else {
                        for (int j = loop; j <= 4; j++) {
                            step = step + data.get(i).get(j).getStepCount();
                            distance = distance + data.get(i).get(j).getDistance();
                            cal = cal + data.get(i).get(j).getCalory();
                        }
                    }
                }
            }
            cals = cals + cal;
            steps = steps + step;
            dis = dis + distance;
        }
    }

    /**
     * @param dateHealthSportItem get arraylist od healt data of particular item
     */
    public void DaywiseData(List<HealthSportItem> dateHealthSportItem) {

        ArrayList<HashMap<Integer, HealthSportItem>> map = new ArrayList<HashMap<Integer, HealthSportItem>>();

        if (dateHealthSportItem != null && dateHealthSportItem.size() > 0) {

            int count = 1;
            HashMap<Integer, HealthSportItem> temp = new HashMap<>();
            for (int i = 1; i <= dateHealthSportItem.size(); i++) {

                HealthSportItem healthSportItem = dateHealthSportItem.get(i - 1);

                temp.put(count, healthSportItem);

                if (i % 4 == 0) {
                    map.add(temp);
                    count = 0;
                    temp = new HashMap<>();
                }
                count++;
            }
            Log.e("Map", map.toString());

            Log.e("1st value Map", map.get(1).get(1).toString());

            FinalData.add(map);
            Log.e("Final list =>", FinalData.toString());
        } else {
            FinalData.add(null);
        }

    }

    /**
     * @param chlngStartDate challenge start date
     * @param chlngEndDate   challenge end date
     * @param CType          challenge type
     */
    public void readDatafromTracker(String chlngStartDate, String chlngEndDate, int CType) {

        String start[] = chlngStartDate.split("-");
        Startyear = Integer.parseInt(start[0]);
        Startmonth = Integer.parseInt(start[1]) - 1;
        Startday = Integer.parseInt(start[2].substring(0, 2));

        String end[] = chlngEndDate.split("-");
        Endyear = Integer.parseInt(end[0]);
        Endmonth = Integer.parseInt(end[1]) - 1;
        Endday = Integer.parseInt(end[2].substring(0, 2));

        String[] StartTime = chlngStartDate.split("\\s+");
        String[] SHM = StartTime[1].split(":");
        StartHours = Integer.parseInt(SHM[0]);
        StartMinutes = Integer.parseInt(SHM[1]);

        String[] EndTime = chlngEndDate.split("\\s+");
        String[] EHM = EndTime[1].split(":");
        EndHours = Integer.parseInt(EHM[0]);
        EndMinutes = Integer.parseInt(EHM[1]);

        //Calendar mycal = new GregorianCalendar(Startyear, Startmonth, Startday);
        try {
            if (Startmonth == Endmonth) {
                if (Startday == Endday) {
                    healthSleep sleep;
                    List<HealthSportItem> dateHealthSportItem = ProtocolUtils.getInstance().getHealthSportItem(new Date(Startyear, Startmonth, Startday));
                    DaywiseData(dateHealthSportItem);
                    if (CType == 4 || CType == 5 || CType == 8 || CType == 9 || CType >= 12) {
                        sleep = ProtocolUtils.getInstance().getHealthSleep(new Date(Startyear, Startmonth, Startday));//sleep
                        DaywiseDataSleep(sleep);
                    }
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
                    Date sDate = sdf.parse(chlngStartDate);
                    Date eDate = sdf.parse(chlngEndDate);

                    GregorianCalendar gcal = new GregorianCalendar();
                    gcal.setTime(sDate);

                    while (!gcal.getTime().after(eDate)) {
                        Date d = gcal.getTime();
                        healthSleep sleep;
                        List<HealthSportItem> dateHealthSportItem = ProtocolUtils.getInstance().getHealthSportItem(new Date(gcal.get(Calendar.YEAR), gcal.get(Calendar.MONTH), gcal.get(Calendar.DAY_OF_MONTH)));
                        DaywiseData(dateHealthSportItem);
                        if (CType == 4 || CType == 5 || CType == 8 || CType == 9 || CType >= 12) {
                            sleep = ProtocolUtils.getInstance().getHealthSleep(new Date(gcal.get(Calendar.YEAR), gcal.get(Calendar.MONTH), gcal.get(Calendar.DAY_OF_MONTH)));//sleep
                            DaywiseDataSleep(sleep);
                        }
                        gcal.add(Calendar.DAY_OF_MONTH, 1);
                        positionlast++;
                    }
                }
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
                Date sDate = sdf.parse(chlngStartDate);
                Date eDate = sdf.parse(chlngEndDate);

                GregorianCalendar gcal = new GregorianCalendar();
                gcal.setTime(sDate);

                while (!gcal.getTime().after(eDate)) {
                    Date d = gcal.getTime();
                    healthSleep sleep;
                    List<HealthSportItem> dateHealthSportItem = ProtocolUtils.getInstance().getHealthSportItem(new Date(gcal.get(Calendar.YEAR), gcal.get(Calendar.MONTH), gcal.get(Calendar.DAY_OF_MONTH)));
                    DaywiseData(dateHealthSportItem);
                    if (CType == 4 || CType == 5 || CType == 8 || CType == 9 || CType >= 12) {
                        sleep = ProtocolUtils.getInstance().getHealthSleep(new Date(gcal.get(Calendar.YEAR), gcal.get(Calendar.MONTH), gcal.get(Calendar.DAY_OF_MONTH)));//sleep
                        DaywiseDataSleep(sleep);
                    }
                    gcal.add(Calendar.DAY_OF_MONTH, 1);
                    positionlast++;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // find health records.
        ChallangesData();
    }

    /**
     * @param sleep sleep data of particular date
     */
    public void DaywiseDataSleep(healthSleep sleep) {
        if (sleep != null) {
            FinalDataSleep.add(sleep);
            Log.e("Final list =>", FinalData.toString());
        } else {
            FinalDataSleep.add(null);
        }

    }
}
