package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.NotificationModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 7/31/17.
 */

public class GetNotificationList extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnGetNotificationListListener listener;

    JSONObject postData;
    private boolean isDialog = false;

    //create an listener for success & failure of service call
    public interface OnGetNotificationListListener {

        /**
         * @param notificationModel return notification model
         */
        void onSucceedToGetNotificationListListener(NotificationModel notificationModel);

        /**
         * @param error_msg return error message
         */
        void onFaildToGetNotificationListListener(String error_msg);
    }

    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param data     JSON post request
     * @param isDialog boolean for showing progress dialog
     */
    public GetNotificationList(Context context, OnGetNotificationListListener listener, JSONObject data, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        try {
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.get_notification_list_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response; // return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        try {
            // get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            NotificationModel obj = new Gson().fromJson(result, NotificationModel.class);
            if (obj.flag) {
                listener.onSucceedToGetNotificationListListener(obj); // returning success listener
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFaildToGetNotificationListListener(obj.message);// returning failure  listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            listener.onFaildToGetNotificationListListener(e.toString());// returning failure  listener
        } finally {
            if (isDialog) {// destroy dialog
                UtilsCommon.destroyProgressBar();
            }
        }


//        try {// performing network call (service call)
//            JSONResponse res = gson.fromJson(result, JSONResponse.class);
//            if (res.getFlag()) {
//                try {// get response from network call (service call) & perform operation on it.
//                    JSONObject jsonObject = new JSONObject(result);
//                    JSONArray array_noti_list = new JSONArray();
//                    ArrayList<NotificationModel> notificationModelArrayList = new ArrayList<>();
//
//                    array_noti_list = jsonObject.getJSONArray(Constant.data);
//                    GsonBuilder gsonBuilder = new GsonBuilder();
//                    Gson gson = gsonBuilder.create();
//                    Type typeList = new TypeToken<ArrayList<NotificationModel>>() {
//                    }.getType();
//
//                    notificationModelArrayList = gson.fromJson(array_noti_list.toString(), typeList);
//                    listener.onSucceedToGetNotificationListListener(notificationModelArrayList); // returning success listener
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                listener.onFaildToGetNotificationListListener(res.getMessage()); // returning failure  listener
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (isDialog) {// manage progress dialog visibility conditionally.
//                UtilsCommon.destroyProgressBar();
//            }
//        }
    }
}