package com.differenzsystem.goyog.api;

import android.app.Activity;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.LeaderboardDataModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 8/25/17.
 */

public class LeaderBoardApi extends AsyncTask<String, Integer, String> {
    private Activity activity;
    JSONObject postData;
    OnLeaderBoardListener listener;
    Gson gson;
    private boolean isDialog = false;

    //create an listener for success & failure of service call
    public interface OnLeaderBoardListener {

        /**
         * @param obj return LeaderboardDataModel model
         */
        void onSucceedToLeaderBoard(LeaderboardDataModel obj);

        /**
         * @param error_msg return error message
         */
        void onFaildToLeaderBoard(String error_msg);
    }

    /**
     * @param activity get activity of particular screen
     * @param listener listener of success & failure of service call
     * @param postData JSON post request
     * @param isDialog boolean for showing progress dialog
     */
    public LeaderBoardApi(Activity activity, OnLeaderBoardListener listener, JSONObject postData, boolean isDialog) {
        this.activity = activity;
        this.postData = postData;
        this.listener = listener;
        this.isDialog = isDialog;
        gson = new Gson();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {// performing network call (service call)
            String url = activity.getResources().getString(R.string.server_url) + activity.getResources().getString(R.string.leader_board_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response; // return response from network call (service call)
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(activity);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            LeaderboardDataModel obj = new Gson().fromJson(result, LeaderboardDataModel.class);
            if (obj.flag) {
                listener.onSucceedToLeaderBoard(obj);       // returning success listener
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFaildToLeaderBoard(obj.message); // returning failure  listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) activity.getApplicationContext();
                    global.doLogout(activity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {// manage progress dialog visibility conditionally.
                UtilsCommon.destroyProgressBar();
            }
        }
    }
}

