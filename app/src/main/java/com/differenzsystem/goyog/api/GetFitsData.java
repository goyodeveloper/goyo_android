package com.differenzsystem.goyog.api;

import java.util.concurrent.TimeUnit;

/**
 * Created on 20/10/16.
 */

public class GetFitsData {
    // define fitlistener
    OnGetFitsListener onGetFitsListener;

    public interface OnGetFitsListener {
        public void onGetCalories(int cal);

        public void onGetSteps(int step);

        public void onGetSleep(int step);

        public void onGetDistance(int distance);

        public void onGetHeartRate(int heart);
    }


    /* Pass startMillisecond, endMillisecond and TimeUnit parameter in request
    TimeUnit you can pass in  NANOSECONDS, MICROSECONDS, MILLISECONDS, SECONDS, MINUTES, HOURS or DAYS*/

    // Prefer TimeUnit is MILLISECONDS
    public void getStepsData(long startMillisecond, long endMillisecond, TimeUnit timeUnit) {
        // Return steps data in array (ArrayList<Hashmap<Key name as String , value as Object>>)

        //Read total steps from returned array list
        // 20 are the steps return  from fit reuest (from startMillisecond to endMillisecond)
        onGetFitsListener.onGetSteps(20);
    }

    public void getCaloriesData(long startMillisecond, long endMillisecond, TimeUnit timeUnit) {
        // Return steps data in array (ArrayList<Hashmap<Key name as String , value as Object>>)

        //Read total Calories from returned array list
        // 20 are the Calories return from fit reuest (from startMillisecond to endMillisecond)
        onGetFitsListener.onGetCalories(20);
    }

    public void getSleepData(long startMillisecond, long endMillisecond, TimeUnit timeUnit) {
        // Return steps data in array (ArrayList<Hashmap<Key name as String , value as Object>>)

        //Read total Sleep from returned array list
        // 20 are the Sleep return from fit reuest (from startMillisecond to endMillisecond)
        onGetFitsListener.onGetSleep(20);
    }

    public void getDistanceData(long startMillisecond, long endMillisecond, TimeUnit timeUnit) {
        // Return steps data in array (ArrayList<Hashmap<Key name as String , value as Object>>)

        //Read total Distance from returned array list
        // 20 are the Distance data return from fit reuest (from startMillisecond to endMillisecond)
        onGetFitsListener.onGetDistance(20);
    }

    public void getHeartData(long startMillisecond, long endMillisecond, TimeUnit timeUnit) {
        // Return steps data in array (ArrayList<Hashmap<Key name as String , value as Object>>)

        //Read total Heart from returned array list
        // 20 are the Heart return from fit reuest (from startMillisecond to endMillisecond)
        onGetFitsListener.onGetHeartRate(20);
    }
}
