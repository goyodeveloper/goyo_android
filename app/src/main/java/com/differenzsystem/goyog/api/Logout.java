package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 18/1/17.
 */

public class Logout extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    //OnLogoutListener listener;

    /**
     *
     * @param context  get context of particular screen
     * @param postData Json post request
     */
    public Logout(Context context, JSONObject postData) {
        this.context = context;
        this.postData = postData;
        //this.listener = listener;
        gson = new Gson();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.logout);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response; // return response from network call (service call)
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(String result) {
        try {// get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                JSONObject jsonObject = new JSONObject(result);
                //JSONArray jsonObjectJSONArray = jsonObject.getJSONArray(Constant.data);
                //listener.onSucceedLogout(res.getMessage());
            } else {
                // listener.onFailedLogout(res.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public interface OnLogoutListener {
        public void onSucceedLogout(String message);

        public void onFailedLogout(String message);
    }*/
}