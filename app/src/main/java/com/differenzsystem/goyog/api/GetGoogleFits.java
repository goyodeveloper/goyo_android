package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.model.ChallangesPopupModel;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.utility.Debugger;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created on 23/9/16.
 */
public class GetGoogleFits {
    // get class name for tag
    String TAG = getClass().getName();
    ChallengesModel obj_Challenges;
    ChallangesPopupModel obj_PopupChallenges;

    //define client
    GoogleApiClient mClient;

    // define and initialize variables
    String caloriesData = "0", stepsData = "0", distanceData = "0", heartRateData = "0";
    int cal = 0, step = 0, heart_rate = 0, distance = 0;
    Context activity;
    boolean isForSync = false;
    boolean isPopup = false;
    Date startDate;
    float totalData = 0;
    boolean useAcceptDate = false;
    // define fitlistener
    OnGetFitsListener onGetFitsListener;

    public GetGoogleFits(Context activity, boolean useAcceptDate, ChallengesModel obj_Challenges, GoogleApiClient mClient, OnGetFitsListener onGetFitsListener) {
        this.obj_Challenges = obj_Challenges;
        this.mClient = mClient;
        this.onGetFitsListener = onGetFitsListener;
        this.useAcceptDate = useAcceptDate;
        this.activity = activity;
    }

    public GetGoogleFits(Context activity, GoogleApiClient mClient, OnGetFitsListener onGetFitsListener, Date startDate, boolean isForSync) {
        this.mClient = mClient;
        this.onGetFitsListener = onGetFitsListener;
        this.activity = activity;
        this.isForSync = isForSync;
        this.startDate = startDate;
    }

    public GetGoogleFits(Context activity, boolean useAcceptDate, ChallangesPopupModel obj_PopupChallenges, GoogleApiClient mClient, OnGetFitsListener onGetFitsListener, boolean isPopup) {
        this.obj_PopupChallenges = obj_PopupChallenges;
        this.mClient = mClient;
        this.onGetFitsListener = onGetFitsListener;
        this.useAcceptDate = useAcceptDate;
        this.isPopup = isPopup;
        this.activity = activity;
    }

    public interface OnGetFitsListener {
        public void onGetCalories(int cal);

        public void onGetSteps(int step);

        public void onGetDistance(int distance);

        public void onGetHeartRate(int heart);
    }

    // AsyncTask for get steps data from fit
    public class StepsCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                totalData = 0;
                Debugger.debugE("Steps..", "inside");
                if (isForSync) {
                    // Read data according to DataType passed i.e. DataType.TYPE_STEP_COUNT_DELTA
                    DataReadRequest readRequest = requestFitnessDataForSync(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA);
                    // Get DataReadResult from reuest
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    stepsData = readData(dataReadResult);
                } else if (isPopup) {

                    // Read data according to DataType passed i.e. DataType.TYPE_STEP_COUNT_DELTA
                    DataReadRequest readRequest = requestFitnessDataforPopup(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA);
                    // Get actual data from reuest
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    stepsData = readData(dataReadResult);

                } else {
                    // Read data according to DataType passed i.e. DataType.TYPE_STEP_COUNT_DELTA
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA);
                    // Get actual data from reuest
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    stepsData = readData(dataReadResult);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                Debugger.debugE("ONPost...", stepsData);
                step = Math.round(totalData <= 0 ? 0 : totalData);
                // call interface of step
                onGetFitsListener.onGetSteps(step);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // AsyncTask for get Calories data from fit
    public class CaloriesCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                totalData = 0;
                if (isForSync) {
                    // Read data according to DataType passed i.e. DataType.TYPE_CALORIES_EXPENDED
                    DataReadRequest readRequest = requestFitnessDataForSync(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED);
                    // Read data from DataReadResult
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    caloriesData = readData(dataReadResult);
                } else if (isPopup) {

                    // Read data according to DataType passed i.e. DataType.TYPE_STEP_COUNT_DELTA
                    DataReadRequest readRequest = requestFitnessDataforPopup(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED);
                    // Get actual data from reuest
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    caloriesData = readData(dataReadResult);

                } else {
                    // Read data according to DataType passed i.e. DataType.TYPE_CALORIES_EXPENDED
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED);
                    // Read data from DataReadResult
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    caloriesData = readData(dataReadResult);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                cal = Math.round(totalData <= 0 ? 0 : totalData);
                // call interface of calories
                onGetFitsListener.onGetCalories(cal);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // api distance call
    public class DistanceCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                totalData = 0;
                if (isForSync) {
                    // Read data according to DataType passed i.e. DataType.TYPE_DISTANCE_DELTA
                    DataReadRequest readRequest = requestFitnessDataForSync(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA);
                    // Read data from DataReadResult
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    distanceData = readData(dataReadResult);
                } else if (isPopup) {

                    // Read data according to DataType passed i.e. DataType.TYPE_STEP_COUNT_DELTA
                    DataReadRequest readRequest = requestFitnessDataforPopup(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA);
                    // Get actual data from reuest
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    distanceData = readData(dataReadResult);

                } else {
                    // Read data according to DataType passed i.e. DataType.TYPE_DISTANCE_DELTA
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA);
                    // Read data from DataReadResult
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    distanceData = readData(dataReadResult);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                //distance = UtilsCommon.getMiles(totalData <= 0 ? 0 : totalData);
                distance = Math.round(totalData <= 0 ? 0 : totalData);
                Debugger.debugE("Distance..", distanceData);
                // call interface of calories
                onGetFitsListener.onGetDistance(distance);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // api heart rate call
    public class HeartRateCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                totalData = 0;
                if (isForSync) {
                    // Read data according to DataType passed i.e. DataType.TYPE_HEART_RATE_BPM
                    DataReadRequest readRequest = requestFitnessDataForSync(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY);
                    // Read data from DataReadResult
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    heartRateData = readData(dataReadResult);
                } else if (isPopup) {

                    // Read data according to DataType passed i.e. DataType.TYPE_STEP_COUNT_DELTA
                    DataReadRequest readRequest = requestFitnessDataforPopup(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY);
                    // Get actual data from reuest
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    heartRateData = readData(dataReadResult);

                } else {
                    // Read data according to DataType passed i.e. DataType.TYPE_HEART_RATE_BPM
                    DataReadRequest readRequest = requestFitnessData(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY);
                    // Read data from DataReadResult
                    DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                    // Read data from DataReadResult
                    heartRateData = readData(dataReadResult);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                heart_rate = Math.round(totalData <= 0 ? 0 : totalData);
                // call interface of calories
                onGetFitsListener.onGetHeartRate(heart_rate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // request data from band from particular date range
    private DataReadRequest requestFitnessDataForSync(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {

            long startMillisecond = startDate.getTime();
            long endMillisecond;
            Date end;
            Calendar endCalendar = Calendar.getInstance();
            if (startDate.getDate() == new Date().getDate()) {
                long startTime = endCalendar.getTimeInMillis();
                SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
                String startTimeString = formater.format(startTime) + " 12:01 AM";
                startDate = null;
                try {
                    startDate = formater.parse(startTimeString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                startMillisecond = startDate.getTime();

                Date endDate = new Date();
                endCalendar.setTime(endDate);
                end = endCalendar.getTime();
                endMillisecond = endCalendar.getTimeInMillis();

                Debugger.debugE("Start time_GoogleFit", startDate + "=>");
                Debugger.debugE("End time_GoogleFit", end + "=>");
            } else {
                endCalendar.setTime(startDate);
                endCalendar.add(Calendar.DAY_OF_YEAR, 1);
                end = endCalendar.getTime();
                endMillisecond = end.getTime();

                Debugger.debugE("Start time_GoogleFit", startDate + "=>");
                Debugger.debugE("End time_GoogleFit", end + "=>");
            }

            /* Pass startMillisecond, endMillisecond and TimeUnit parameter in request
               TimeUnit ypu can pass in  NANOSECONDS, MICROSECONDS, MILLISECONDS, SECONDS, MINUTES, HOURS or DAYS*/
            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    // request data from particular date range
    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            Calendar startCalendar = Calendar.getInstance();
            long startTime = startCalendar.getTimeInMillis();

            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss");
            String startTimeString = "";
            if (useAcceptDate) {
                startTimeString = obj_Challenges.getAccept_date();
            } else {
                if (obj_Challenges.getChallenge_start_date().equalsIgnoreCase("0000-00-00 00:00:00") ||
                        obj_Challenges.getChallenge_start_date() == null) {
                    startTimeString = obj_Challenges.getAccept_date();
                } else {
                    startTimeString = obj_Challenges.getChallenge_start_date();
                }
            }
            Date startDate = null;
            try {
                startDate = formater.parse(startTimeString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long startMillisecond = startDate.getTime();

            Calendar endCalendar = Calendar.getInstance();
            Date endDate = startDate;
            endCalendar.setTime(endDate);
            int duration = Integer.parseInt(obj_Challenges.getDuration());
            endCalendar.add(Calendar.MINUTE, duration);
            Date end = endCalendar.getTime();
            long endMillisecond = end.getTime();

            Debugger.debugE("Start time", startDate + "=>");
            Debugger.debugE("End time", end + "=>");

             /* Pass startMillisecond, endMillisecond and TimeUnit parameter in request
               TimeUnit ypu can pass in  NANOSECONDS, MICROSECONDS, MILLISECONDS, SECONDS, MINUTES, HOURS or DAYS*/

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    // read dataset from data
    public String readData(DataReadResult dataReadResult) {
        String value = "";
        Debugger.debugE("REadDAte..", dataReadResult.toString());
        try {
            // Check if Bucket size is greater than zero
            if (dataReadResult.getBuckets().size() > 0) {
                // Read data from buckets
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                        totalData += Float.parseFloat(value.equalsIgnoreCase("") ? "0" : value);
                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    // Read actual data of steps/calories/heart/distance
    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            // Read actual data of steps/calories/heart/distance
            for (DataPoint dp : dataSet.getDataPoints()) {
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dp.getStartTime(TimeUnit.MILLISECONDS));
                Debugger.debugE(TAG, "\tEnd: " + dp.getEndTime(TimeUnit.MILLISECONDS));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

     // popup request data from particular date range
    private DataReadRequest requestFitnessDataforPopup(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            Calendar startCalendar = Calendar.getInstance();
            long startTime = startCalendar.getTimeInMillis();

            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss");
            String startTimeString = "";
            if (useAcceptDate) {
                startTimeString = obj_PopupChallenges.getAccept_date();
            } else {
                if (obj_PopupChallenges.getChallenge_start_date().equalsIgnoreCase("0000-00-00 00:00:00") ||
                        obj_PopupChallenges.getChallenge_start_date() == null) {
                    startTimeString = obj_PopupChallenges.getAccept_date();
                } else {
                    startTimeString = obj_PopupChallenges.getChallenge_start_date();
                }
            }
            Date startDate = null;
            try {
                startDate = formater.parse(startTimeString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long startMillisecond = startDate.getTime();

            Calendar endCalendar = Calendar.getInstance();
            Date endDate = startDate;
            endCalendar.setTime(endDate);
            int duration = Integer.parseInt(obj_PopupChallenges.getDuration());
            endCalendar.add(Calendar.MINUTE, duration);
            Date end = endCalendar.getTime();
            long endMillisecond = end.getTime();

            Debugger.debugE("Start time", startDate + "=>");
            Debugger.debugE("End time", end + "=>");

             /* Pass startMillisecond, endMillisecond and TimeUnit parameter in request
               TimeUnit ypu can pass in  NANOSECONDS, MICROSECONDS, MILLISECONDS, SECONDS, MINUTES, HOURS or DAYS*/

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }
}
