package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.LeaderboardDataModel;
import com.differenzsystem.goyog.model.LeaderboardModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;

public class GetLeaderboardData extends AsyncTask<String, Integer, String> {
    @Override
    protected String doInBackground(String... strings) {
        return null;
    }
//    private Context context;
//    Gson gson;
//    OnGetLeaderboardDataListener listener;
//    boolean showDialog;
//    JSONObject postData;
//
//    public interface OnGetLeaderboardDataListener {
//        public void onSucceedtoGetLeaderboardData(LeaderboardModel message);
//
//        public void onFailedtoGetLeaderboardData(String message);
//    }
//
//    public GetLeaderboardData(Context context, JSONObject data, OnGetLeaderboardDataListener listener, boolean showDialog) {
//        this.context = context;
//        this.listener = listener;
//        postData = data;
//        this.showDialog = showDialog;
//        gson = new Gson();
//    }
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        if (showDialog)
//            UtilsCommon.showProgressDialog(context);
//    }
//
//    @Override
//    protected String doInBackground(String... params) {
//        String str_response = null;
//        try {
//            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.top_20);
//            str_response = JSONParse.postJSON(url, postData);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return str_response;
//    }
//
//    @Override
//    protected void onPostExecute(String result) {
//        super.onPostExecute(result);
//        try {
//            JSONResponse res = gson.fromJson(result, JSONResponse.class);
//            if (res.getFlag()) {
//                JSONObject jsonObject = new JSONObject(result);
//                JSONArray data_leaderboards = new JSONArray();
//                ArrayList<LeaderboardDataModel> leaderboards = new ArrayList<>();
//
//                data_leaderboards = jsonObject.getJSONArray(Constant.data);
//                GsonBuilder gsonBuilder = new GsonBuilder();
//                Gson gson = gsonBuilder.create();
//                Type listType = new TypeToken<ArrayList<LeaderboardDataModel>>() {
//                }.getType();
//                leaderboards = gson.fromJson(data_leaderboards.toString(), listType);
//                LeaderboardModel leaderboardModel = new LeaderboardModel();
//                leaderboardModel.setFlag(jsonObject.getBoolean("flag"));
//                leaderboardModel.setMessage(jsonObject.getString("message"));
//                leaderboardModel.setData(leaderboards);
//                listener.onSucceedtoGetLeaderboardData(leaderboardModel);
//            } else {
//                listener.onFailedtoGetLeaderboardData(res.getMessage());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            UtilsCommon.destroyProgressBar();
//        }
//    }
//
//    public class MyComparator implements Comparator<LeaderboardDataModel> {
//        @Override
//        public int compare(LeaderboardDataModel o1, LeaderboardDataModel o2) {
//            return o1.getTotal_steps().compareTo(o2.getTotal_steps());
//        }
//    }
}