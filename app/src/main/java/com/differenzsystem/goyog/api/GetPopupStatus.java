package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.ChallangesPopupModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 26/11/16.
 */

public class GetPopupStatus extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    OnGetPopupStatusListener listener;

    /**
     * @param context  get context of particular screen
     * @param postData JSON post request
     * @param listener listener of success & failure of service call
     */
    public GetPopupStatus(Context context, JSONObject postData, OnGetPopupStatusListener listener) {
        this.context = context;
        this.postData = postData;
        this.listener = listener;
        gson = new Gson();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.get_popup_status);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response; // return response from network call (service call)
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        try {// get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonObjectJSONArray = jsonObject.getJSONArray(Constant.data);
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                Type listType = new TypeToken<ArrayList<ChallangesPopupModel>>() {
                }.getType();
                ArrayList<ChallangesPopupModel> inProcessList = new ArrayList<>();
                inProcessList = gson.fromJson(jsonObjectJSONArray.toString(), listType);
                listener.onSucceedGetPopupStatus(inProcessList); // returning success listener
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFailedGetPopupStatus(res.getMessage()); // returning failure  listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //create an listener for success & failure of service call
    public interface OnGetPopupStatusListener {

        /**
         * @param inProcessList return ChallangesPopupModel model
         */
        public void onSucceedGetPopupStatus(ArrayList<ChallangesPopupModel> inProcessList);

        /**
         * @param message return error message
         */
        public void onFailedGetPopupStatus(String message);
    }
}
