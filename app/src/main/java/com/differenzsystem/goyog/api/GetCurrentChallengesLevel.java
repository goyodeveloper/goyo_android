package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.ChartDataModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 9/12/16.
 */

public class GetCurrentChallengesLevel extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    OnGetCurrentChallengesLevelListener listener;

    public GetCurrentChallengesLevel(Context context, JSONObject postData, OnGetCurrentChallengesLevelListener listener) {
        this.context = context;
        this.postData = postData;
        this.listener = listener;
        gson = new Gson();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.current_challenges_level);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonObjectJSONArray = jsonObject.getJSONArray(Constant.data);
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                /*Type listType = new TypeToken<ArrayList<ChallangesPopupModel>>() {
                }.getType();
                ArrayList<ChallangesPopupModel> inProcessList = new ArrayList<>();
                inProcessList = gson.fromJson(jsonObjectJSONArray.toString(), listType);
                listener.onSucceedGetCurrentChallengesLevel(inProcessList);*/
            } else {
                listener.onFailedGetCurrentChallengesLevel(res.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnGetCurrentChallengesLevelListener {
        public void onSucceedGetCurrentChallengesLevel(ArrayList<ChartDataModel> inProcessList);

        public void onFailedGetCurrentChallengesLevel(String message);
    }
}
