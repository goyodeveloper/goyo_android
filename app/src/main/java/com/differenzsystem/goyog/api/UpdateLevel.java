package com.differenzsystem.goyog.api;

import android.app.Activity;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.LevelInformationModel;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created on 30/9/16.
 */
public class UpdateLevel extends AsyncTask<String, Integer, String> {
    Activity activity;
    JSONObject postData;
    OnUpdateLevelListener listener;
    Gson gson;
    String status;

    /**
     *
     * @param activity return activity of particular screen
     * @param postData Json post request
     * @param listener listener of success & failure of service call
     */
    public UpdateLevel(Activity activity, JSONObject postData,OnUpdateLevelListener listener) {
        this.activity = activity;
        this.postData = postData;
        this.listener = listener;
        gson = new Gson();
    }

    /**
     *
     * @param activity return activity of particular screen
     * @param postData Json post request
     * @param listener listener of success & failure of service call
     * @param status   get status
     */
    public UpdateLevel(Activity activity, JSONObject postData,OnUpdateLevelListener listener, String status) {
        this.activity = activity;
        this.postData = postData;
        this.listener = listener;
        this.status = status;
        gson = new Gson();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {// performing network call (service call)
            String url = activity.getResources().getString(R.string.server_url) + activity.getResources().getString(R.string.update_level_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response; // return response from network call (service call)
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        try {// get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String str_msg = jsonObject.getString(Constant.message);
                    JSONObject newJsonObject = jsonObject.getJSONObject(Constant.data);
                    LevelInformationModel obj = new Gson().fromJson(newJsonObject.toString(), LevelInformationModel.class);
                    if (obj != null) {
                        listener.onSucceedToUpdateLevel(obj, str_msg, status); // returning success listener
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFaildToUpdateLevel(res.getMessage(), status);
                } else { // Logout user when access token is invalid
                    Application global = (Application) activity.getApplicationContext();// returning failure listener
                    global.doLogout(activity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* try {
            JSONObject jsonObject = new JSONObject(result);
            Log.e("update level", "succ : " + jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }

    //create an listener for success & failure of service call
    public interface OnUpdateLevelListener {

        /**
         *
         * @param obj    return LevelInformationModel model
         * @param msg    return message from response
         * @param status return status
         */
        void onSucceedToUpdateLevel(LevelInformationModel obj, String msg, String status);

        /**
         *
         * @param error_msg return error message
         * @param status    return status
         */
        void onFaildToUpdateLevel(String error_msg, String status);
    }
}
