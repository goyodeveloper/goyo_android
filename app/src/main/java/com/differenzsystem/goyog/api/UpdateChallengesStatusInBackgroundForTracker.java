package com.differenzsystem.goyog.api;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.model.ChartDataModel;
import com.differenzsystem.goyog.model.LevelInformationModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.veryfit.multi.share.BleSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Union Assurance PLC on 7/11/16.
 */

public class UpdateChallengesStatusInBackgroundForTracker implements GetInProgressData.OnGetInProgressChallengesListener, ChallengeAcceptCall.OnChallengeAcceptListener, FitnessTrackerOperations.FitnessTrackerOperationslistener {
    Context context;
    GetInProgressData.OnGetInProgressChallengesListener onGetInProgressChallengesListener;
    int cnt = 0;
    int maxCnt = 0;
    ArrayList<ChallengesModel> inProcessList;
    int steps = 0, cals = 0, hearts = 0, sleep = 0, dis = 0;
    //float dis = 0;
    int type, chllng_val = 0, sec_chllng_val = 0, third_chllng_val, fourth_chllng_val;
    String status;
    ArrayList<ChartDataModel> chartmodellist = new ArrayList<>();

    ChallengeAcceptCall.OnChallengeAcceptListener onChallengeAcceptListener;
    FitnessTrackerOperations.FitnessTrackerOperationslistener fitnessTrackerOperationslistener;
    String request_status;

    //change
    onUpdateChallengesStatusInBackgroundForTracker listener;
    LevelInformationModel data;

    public interface onUpdateChallengesStatusInBackgroundForTracker {
        public void onSucceedtoUpdateChallengesStatusInBackgroundForTracker(ArrayList<ChartDataModel> chartmodellist);

        public void onSucceedtoUpdateWiningstatus(ChallengesModel challengesModel);
    }

    public UpdateChallengesStatusInBackgroundForTracker(Context context, onUpdateChallengesStatusInBackgroundForTracker listener) {
        this.context = context;
        onGetInProgressChallengesListener = this;
        onChallengeAcceptListener = this;
        this.listener = listener;
        fitnessTrackerOperationslistener = this;
    }

    public UpdateChallengesStatusInBackgroundForTracker(Context context) {
        this.context = context;
        onGetInProgressChallengesListener = this;
        onChallengeAcceptListener = this;
    }

    /*public UpdateChallengesStatusInBackgroundForTracker(Context context, onUpdateChallengesStatusInBackgroundForTracker listener, LevelInformationModel data) {
        this.context = context;
        onGetInProgressChallengesListener = this;
        onChallengeAcceptListener = this;
        this.data = data;
        this.listener = listener;
    }*/

    // api call fo GetInProgressData
    public void getData() {
        JSONObject object = new JSONObject();
        try {
            object.put(Constant.user_id, UtilsPreferences.getString(context, Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(context, Constant.accesstoken));

            if (ConnectionDetector.isConnectingToInternet(context)) {
                GetInProgressData getInProgressData = new GetInProgressData(context, object, onGetInProgressChallengesListener, true);
                getInProgressData.execute();
            } else {
                //Toast.makeText(context, context.getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // success listener of GetInProgressData
    @Override
    public void onSucceedChallenges(ArrayList<ChallengesModel> inProcessList) {
        UtilsPreferences.saveInProgressChallengesSharedPrefrences(context, Constant.Key_inProgressList, inProcessList);
        onSucceedChallengesProcess(inProcessList);
    }

    // process call on success listener of GetInProgressData
    public void onSucceedChallengesProcess(ArrayList<ChallengesModel> inProcessList) {
        Log.e("data in p", String.valueOf(inProcessList));
        this.inProcessList = inProcessList;
        if (inProcessList.size() > 0) {
            maxCnt = inProcessList.size();
            doProcess();
        } else {
            Log.e("no date", "for update status");
        }
    }

    // failed listener of GetInProgressData
    @Override
    public void onFailedChallenges(String message) {
        Log.e("data in p failed", message);
        if (listener != null) {
            UtilsPreferences.saveInProgressChallengesSharedPrefrences(context, Constant.Key_inProgressList, inProcessList);
            listener.onSucceedtoUpdateChallengesStatusInBackgroundForTracker(chartmodellist);
        }
    }

    // get challenge data as per request type
    public void doProcess() {
        if (cnt < maxCnt) {
            type = Integer.parseInt(inProcessList.get(cnt).getChallenge_type());
            chllng_val = Integer.parseInt(inProcessList.get(cnt).getChallenge_value());
            String val = inProcessList.get(cnt).getSecond_challenge_value();
            if (val != null && val.length() > 0) {
                sec_chllng_val = Integer.parseInt(inProcessList.get(cnt).getSecond_challenge_value());
            }

            val = inProcessList.get(cnt).getThird_challenge_value();
            if (val != null && val.length() > 0) {
                third_chllng_val = Integer.parseInt(inProcessList.get(cnt).getThird_challenge_value());
            }

            val = inProcessList.get(cnt).getFourth_challenge_value();
            if (val != null && val.length() > 0) {
                fourth_chllng_val = Integer.parseInt(inProcessList.get(cnt).getFourth_challenge_value());
            }

            getChallangesData();
        } else {
            Log.e("back call", "completed");
            if (listener != null)
                listener.onSucceedtoUpdateChallengesStatusInBackgroundForTracker(chartmodellist);
        }
    }


    // request data from FitnessTrackerOperations
    public void getChallangesData() {

        String startTimeString = "";
        int duration = Integer.parseInt(inProcessList.get(cnt).getDuration());
        String cStartDate = inProcessList.get(cnt).getChallenge_start_date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

/*
        if (cStartDate != null) {
            if (cStartDate.equalsIgnoreCase("0000-00-00 00:00:00") ||
                    cStartDate == null) {
                startTimeString = inProcessList.get(cnt).getAccept_date();
            } else {
                startTimeString = cStartDate;
            }
        } else {
            startTimeString = inProcessList.get(cnt).getAccept_date();
        }
*/
        startTimeString = inProcessList.get(cnt).getAccept_date();

        Date startDate = null;
        try {
            startDate = formater.parse(startTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar endCalendar = Calendar.getInstance();
        Date endDate = startDate;
        endCalendar.setTime(endDate);
        //int duration = Integer.parseInt(obj_Challenges.getDuration());
        endCalendar.add(Calendar.MINUTE, duration);
        //Date end = endCalendar.getTime();

        String chlngStartDate = formater.format(startDate);
        String chlngEndDate = formater.format(endCalendar.getTime());//
        Date endDiff = endCalendar.getTime();
        if (BleSharedPreferences.getInstance().getIsBind()) {
            FitnessTrackerOperations fitnessTrackerOperations = new FitnessTrackerOperations(context, fitnessTrackerOperationslistener);
            fitnessTrackerOperations.readDatafromTracker(chlngStartDate, chlngEndDate, Integer.parseInt(inProcessList.get(cnt).getChallenge_type()));
        } else {
            checkNupdate();
        }
    }

    // success listener of FitnessTrackerOperations
    @Override
    public void onSucceedtoFitnessTrackerOperation(int steps, int cals, int dis, int hearts, int sleep) {
        this.cals = cals;
        this.steps = steps;
        this.dis = dis;
        this.sleep = sleep;

        checkNupdate();
    }

    // set up chart module as per request type
    public void checkNupdate() {
        ChartDataModel chartmodel = new ChartDataModel();
        switch (type) {
            case 1://steps
                if (steps < chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                //chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setSectotalval(0);
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);
                chartmodel.setVal(steps);
                chartmodel.setSecval(0);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 2://calories
                if (cals < chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                //chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setSectotalval(0);
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(cals);
                chartmodel.setSecval(0);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 3://distance
                if (dis < chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                //chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setSectotalval(0);
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(dis);
                chartmodel.setSecval(0);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 4://sleep
                if (hearts < chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(0);
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(sleep);
                chartmodel.setSecval(0);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 5://distance and sleep
                if (dis < chllng_val || sleep < sec_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(dis);
                chartmodel.setSecval(sleep);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 6://step & distance
                if (steps < chllng_val || dis < sec_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(steps);
                chartmodel.setSecval(dis);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 7://steps and calories
                if (steps < chllng_val || cals < sec_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(steps);
                chartmodel.setSecval(cals);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 8://Sleep & step
                if (steps < chllng_val || sleep < sec_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(steps);
                chartmodel.setSecval(sleep);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 9://sleep and calories
                if (sleep < chllng_val || cals < sec_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(sleep);
                chartmodel.setSecval(cals);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 10://distance and calories
                if (dis < chllng_val || cals < sec_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(0);
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(dis);
                chartmodel.setSecval(cals);
                chartmodel.setThirdval(0);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 11: //steps and calories and distance
                if (steps < chllng_val || cals < sec_chllng_val || dis < third_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(Integer.valueOf(inProcessList.get(cnt).getThird_challenge_value()));
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(steps);
                chartmodel.setSecval(cals);
                chartmodel.setThirdval(dis);
                chartmodel.setFourthval(0);

                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 12://steps and calories nad sleep
                if (steps < chllng_val || cals < sec_chllng_val || sleep < third_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(Integer.valueOf(inProcessList.get(cnt).getThird_challenge_value()));
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(steps);
                chartmodel.setSecval(cals);
                chartmodel.setThirdval(sleep);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 13://steps and distance and sleep
                if (steps < chllng_val || dis < sec_chllng_val || sleep < third_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(Integer.valueOf(inProcessList.get(cnt).getThird_challenge_value()));
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(steps);
                chartmodel.setSecval(dis);
                chartmodel.setThirdval(sleep);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 14://calories and distance and sleep
                if (cals < chllng_val || dis < sec_chllng_val || sleep < third_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(Integer.valueOf(inProcessList.get(cnt).getThird_challenge_value()));
                chartmodel.setFourthtotalval(0);

                chartmodel.setVal(cals);
                chartmodel.setSecval(dis);
                chartmodel.setThirdval(sleep);
                chartmodel.setFourthval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 15://steps and calories and distance and sleep
                if (steps < chllng_val || cals < sec_chllng_val || dis < third_chllng_val || sleep < fourth_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(type);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setThirdtotalval(Integer.valueOf(inProcessList.get(cnt).getThird_challenge_value()));
                chartmodel.setFourthtotalval(Integer.valueOf(inProcessList.get(cnt).getFourth_challenge_value()));
                chartmodel.setVal(steps);
                chartmodel.setSecval(cals);
                chartmodel.setThirdval(dis);
                chartmodel.setFourthval(sleep);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;


        }
        UpdateChallageStatus(status);
    }

    public void UpdateChallageStatus(String status) {
        try {
            JSONObject object = new JSONObject();
            object.put(Constant.user_id, UtilsPreferences.getString(context, Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(context, Constant.accesstoken));
            object.put(Constant.challenge_id, inProcessList.get(cnt).getChallenge_id());
            object.put(Constant.challenge_status, status);

            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            object.put(Constant.timezone, tz.getID());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateandTime = sdf.format(new Date());
            object.put(Constant.date, currentDateandTime);
            checkNmakeCall(object, status);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // api call for ChallengeAcceptCall
    private void checkNmakeCall(JSONObject object, String status) {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        String startTimeString = "";
        /*if (inProcessList.get(cnt).getChallenge_start_date() != null) {
            if (inProcessList.get(cnt).getChallenge_start_date().equalsIgnoreCase("0000-00-00 00:00:00") ||
                    inProcessList.get(cnt).getChallenge_start_date() == null) {
                startTimeString = inProcessList.get(cnt).getAccept_date();
            } else {
                startTimeString = inProcessList.get(cnt).getChallenge_start_date();
            }
        } else {
            startTimeString = inProcessList.get(cnt).getAccept_date();
        }*/
        startTimeString = inProcessList.get(cnt).getAccept_date();

        Date startDate = null;
        try {
            startDate = formater.parse(startTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar endCalendar = Calendar.getInstance();
        Date endDate = startDate;
        endCalendar.setTime(endDate);
        int duration = Integer.parseInt(inProcessList.get(cnt).getDuration());
        endCalendar.add(Calendar.MINUTE, duration);
        Date end = endCalendar.getTime();

        String cDate;
        Calendar c = Calendar.getInstance();
        Date currentDate = c.getTime();
        SimpleDateFormat formaterN = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cDate = formaterN.format(currentDate);
        try {
            currentDate = formaterN.parse(cDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.e("currNend", currentDate + " : " + end);
        if (status.equalsIgnoreCase("4")) {
            if (currentDate.after(end)) {
                request_status = status;
                new ChallengeAcceptCall(context, onChallengeAcceptListener, object, false).execute();
                //chartmodellist.remove(cnt);
            } else {
                Log.e("not update", "now");
                cnt++;
                doProcess();
            }
        } else {
            request_status = status;
            new ChallengeAcceptCall(context, onChallengeAcceptListener, object, false).execute();
        }
    }

    // success listener for ChallengeAcceptCall
    @Override
    public void onSucceedChallengeAccept(String message, JSONObject jsonResponse) {

        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(context, Constant.Key_inProgressList);
        String response_status = "";
        String title = "";
        try {
            response_status = jsonResponse.getString(Constant.challenge_status);
            title = jsonResponse.getString(Constant.title);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (response_status.equalsIgnoreCase("2")) {
            int profileadge = UtilsPreferences.getInt(context, Constant.ProfileBadge);
            UtilsPreferences.setInt(context, Constant.ProfileBadge, profileadge + 1);

            int count = UtilsPreferences.getInt(context, Constant.CahllangeBadge);
            UtilsPreferences.setInt(context, Constant.CahllangeBadge, count + 1);

            listener.onSucceedtoUpdateWiningstatus(inProcessList.get(cnt));
            UtilsPreferences.setBoolean(context, Constant.isInProgress, false);
        } else {
            if (request_status.equals("2") && response_status.equals("4")) {
                UtilsPreferences.setBoolean(context, Constant.isInProgress, false);
                InstantWinPopup(message, title);
            }
        }
        chartmodellist.remove(cnt);
       /* int profileadge = UtilsPreferences.getInt(context, Constant.ProfileBadge);
        UtilsPreferences.setInt(context, Constant.ProfileBadge, profileadge + 1);
        listener.onSucceedtoUpdateWiningstatus(inProcessList.get(cnt));
        UtilsPreferences.setBoolean(context, Constant.isInProgress, false);
        chartmodellist.remove(cnt);*/

        cnt++;
        doProcess();
    }

    // failed listener for ChallengeAcceptCall
    @Override
    public void onFailedChallengeAccept(String message) {
        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(context, Constant.Key_inProgressList);
        cnt++;
        doProcess();
    }

    // show popup after wining challenge
    public void InstantWinPopup(String msg, String title) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle);
        alertDialog.setCancelable(false);
        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                // Write your code here to invoke YES event
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

//    public interface OnUpdateChallengesStatusInBackgroundListener {
//        public void onSucceedUpdateChallengesStatusInBackground(ArrayList<ChartDataModel> chartmodellist);
//
//        // public void onFailedUpdateChallengesStatusInBackground(String message);
//    }

}
