package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 11/24/17.
 */

public class UpdateDeviceToken extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    UpdateDeviceToken.OnUpdateDeviceTokenListener listener;
    private boolean isDialog = false;

    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param isDialog boolean for showing progress dialog
     */
    public UpdateDeviceToken(Context context, UpdateDeviceToken.OnUpdateDeviceTokenListener listener, JSONObject data, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        this.postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {

            // performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.update_device_token_url);
            str_response = JSONParse.postJSON(url, postData);
            Log.e("UpdateDeviceToken url :", url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response;//return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            Log.e("UpdateDeviceToken res: ", result);
            // get response from network call (service call) & perform operation on it.
            listener.onSucceedUpdateDeviceToken(result);
//            JSONResponse res = gson.fromJson(result, JSONResponse.class);
//            if (res.getFlag()) {
//                JSONObject jsonObject = new JSONObject(result);
//                JSONObject newJsonObject = jsonObject.getJSONObject(Constant.data);// returning success listener
//                listener.onSucceedUpdateDeviceToken(result);
//            } else {
//                listener.onFailedUpdateDeviceToken(res.getMessage());// returning failure  listener
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {// destroy dialog
                UtilsCommon.destroyProgressBar();
            }
        }
    }

    public interface OnUpdateDeviceTokenListener {

        /**
         * @param jsonResponse return service call response
         */
        public void onSucceedUpdateDeviceToken(String jsonResponse);

        /**
         * @param message return error message
         */
        public void onFailedUpdateDeviceToken(String message);
    }
}
