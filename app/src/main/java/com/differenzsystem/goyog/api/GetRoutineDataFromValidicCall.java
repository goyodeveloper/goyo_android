package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.model.ValidicRoutineModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.gson.Gson;

import org.json.JSONObject;

public class GetRoutineDataFromValidicCall extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnGetRoutineFromValidicListener listener;

    private boolean isDialog = false;
    String startDate;
    String endDate, chartFlag;

    //201=success

    //create an listener for success & failure of service call
    public interface OnGetRoutineFromValidicListener {

        /**
         *
         * @param summary   return ValidicSummaryModel model
         * @param data      return ValidicRoutineModel model
         * @param chartFlag return chart for eg day, week, hour etc
         */
        public void onSucceedToGetRoutineFromValidic(ValidicSummaryModel summary, ValidicRoutineModel data, String chartFlag);

        /**
         *
         * @param error_msg return error message
         */
        public void onFaildToGetRoutineFromValidic(String error_msg);
    }

    /**
     *
     * @param context   get context of particular screen
     * @param listener  listener of success & failure of service call
     * @param isDialog  boolean for showing progress dialog
     * @param startDate get start date
     * @param endDate   get end date
     * @param chartFlag get chart for eg day, week, hour etc
     */
    public GetRoutineDataFromValidicCall(Context context, OnGetRoutineFromValidicListener listener, boolean isDialog, String startDate, String endDate, String chartFlag) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        this.startDate = startDate;
        this.endDate = endDate;
        this.chartFlag = chartFlag;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        String url;
        try {// performing network call (service call)
            url = context.getResources().getString(R.string.validic_url) + UtilsPreferences.getString(context, Constant.validic_id)
                    + "/routine.json?access_token=" + Constant.access_token_value
                    + "&start_date=" + startDate + "&end_date=" + endDate + "&expanded=1";

            str_response = JSONParse.GetJSON(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response; // return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.destroyProgressBar();
        }
        try {// get response from network call (service call) & perform operation on it.
            JSONObject temp = new JSONObject(result);
            JSONObject res = temp.getJSONObject(Constant.summary);
            ValidicRoutineModel data;
            ValidicSummaryModel summary;
            if (res.getInt(Constant.status) == 200) {
                try {
                    data = new Gson().fromJson(temp.toString(), ValidicRoutineModel.class);
                    summary = new Gson().fromJson(res.toString(), ValidicSummaryModel.class);
                    if (data != null) {
                        listener.onSucceedToGetRoutineFromValidic(summary, data, chartFlag); // returning success listener
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                listener.onFaildToGetRoutineFromValidic(res.getString(Constant.message)); // returning failure listener
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}