package com.differenzsystem.goyog.api;

import android.content.Context;
import android.util.Log;

import com.differenzsystem.goyog.api.SyncFitData.OnSyncDataListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.android.gms.common.api.GoogleApiClient;
import com.veryfit.multi.nativedatabase.HealthHeartRate;
import com.veryfit.multi.nativedatabase.HealthSport;
import com.veryfit.multi.nativedatabase.healthSleep;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Union Assurance PLC on 10/4/16.
 */

public class UpdateDeviceDataInBackground implements GetGoogleFits.OnGetFitsListener, OnSyncDataListener {
    GoogleApiClient mClient;
    Context context;

    GetGoogleFits getGoogleFits;
    OnSyncDataListener onSyncDataListener;
    GetGoogleFits.OnGetFitsListener onGetFitsListener;
    int steps = 0, cals = 0, dis = 0, hearts = 0, sleep = 0;
    Date startDate;
    String cDate;
    OnUpdateDeviceDataInBackgroundListener onUpdateDeviceDataInBackgroundListener;

    public UpdateDeviceDataInBackground(Context context, GoogleApiClient mClient) {
        this.mClient = mClient;
        this.context = context;
        onGetFitsListener = this;
        onSyncDataListener = this;
    }

    public UpdateDeviceDataInBackground(Context context, GoogleApiClient mClient, OnUpdateDeviceDataInBackgroundListener onUpdateDeviceDataInBackgroundListener) {
        this.mClient = mClient;
        this.context = context;
        this.onUpdateDeviceDataInBackgroundListener = onUpdateDeviceDataInBackgroundListener;
        onGetFitsListener = this;
        onSyncDataListener = this;
    }

    public UpdateDeviceDataInBackground(Context context) {
        this.context = context;
        onGetFitsListener = this;
        onSyncDataListener = this;
    }

    public UpdateDeviceDataInBackground(Context context, OnUpdateDeviceDataInBackgroundListener onUpdateDeviceDataInBackgroundListener) {
        this.context = context;
        this.onUpdateDeviceDataInBackgroundListener = onUpdateDeviceDataInBackgroundListener;
        onGetFitsListener = this;
        onSyncDataListener = this;
    }

    // set up listener
    public interface OnUpdateDeviceDataInBackgroundListener {
        public void onSucceedUpdateDeviceDataInBackground();
    }

    // request data from band during syncing
    public void getData() {
        Calendar c = Calendar.getInstance();
        Date currentDate = c.getTime();
        String sDate = UtilsPreferences.getString(context, Constant.last_update_date);
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cDate = formater.format(currentDate);
        try {
            currentDate = formater.parse(cDate);
            startDate = formater.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long different = currentDate.getTime() - startDate.getTime();
        long differenceDay = different / (24 * 60 * 60 * 1000);
        long diffInMin = TimeUnit.MILLISECONDS.toMinutes(different);
        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(different);
        int diffmin = (int) (TimeUnit.MILLISECONDS.toMinutes(different) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(different)));
        Debugger.debugE("UpdateData..", differenceDay + " " + startDate + " " + currentDate);

        doProcess(); // added on 10-10-2017

//        if (differenceDay > 0) {
//            Log.e("day diff", "call");
//            doProcess();
////        } else if (diffInMin > 0) {
//        } else if (diffInSec > 10) {  // added on 14-09-2017
//            Log.e("min diff", "call");
//            doProcess();
//        } else {
//            if (onUpdateDeviceDataInBackgroundListener != null)
//                onUpdateDeviceDataInBackgroundListener.onSucceedUpdateDeviceDataInBackground();
//        }
    }

    // request for data from band
    public void doProcess() {
        if (UtilsPreferences.getString(context, Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
            updateDataFromTracker();
        } else {
            getGoogleFits = new GetGoogleFits(context, mClient, onGetFitsListener, startDate, true);
            getGoogleFits.new StepsCall().execute();
        }
    }

    // update the data from tracker
    public void updateDataFromTracker() {
        int year, month, day;

        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String stDate = formater.format(startDate); // commented on 04-11-2017
        String stDate = cDate; // added on 04-11-2017

        String start[] = stDate.split("-");
        year = Integer.parseInt(start[0]);
        month = Integer.parseInt(start[1]) - 1;
        day = Integer.parseInt(start[2].substring(0, 2));

        HealthSport healthSport = ProtocolUtils.getInstance().getHealthSport(new Date(year, month, day));
        HealthHeartRate healthHeartRate = ProtocolUtils.getInstance().getHealthRate(new Date(year, month, day));
        healthSleep sleep = ProtocolUtils.getInstance().getHealthSleep(new Date(year, month, day));

        if (ConnectionDetector.internetCheck(context)) {
            JSONObject object = new JSONObject();
            try {
//                String cDate = formater.format(startDate); // commented on 04-11-2017

                object.put(Constant.user_id, UtilsPreferences.getString(context, Constant.user_id));
                object.put(Constant.accesstoken, UtilsPreferences.getString(context, Constant.accesstoken));
                object.put(Constant.date, cDate);
                if (healthSport != null) {
                    object.put(Constant.calories, healthSport.getTotalCalory());
                    object.put(Constant.steps, healthSport.getTotalStepCount());
//                    object.put(Constant.distance, healthSport.getTotalStepCount());
                    object.put(Constant.distance, (double) healthSport.getTotalDistance() / 1000); // added on 09-08-2017
                } else {
                    object.put(Constant.calories, "0");
                    object.put(Constant.steps, "0");
                    object.put(Constant.distance, "0");
                }

                if (healthHeartRate != null)
                    object.put(Constant.heart_rate, healthHeartRate.getSilentHeart());
                else
                    object.put(Constant.heart_rate, "0");

                if (sleep != null)
                    object.put(Constant.sleep, sleep.getTotalSleepMinutes());
                else
                    object.put(Constant.sleep, "0");

                //object.put(Constant.connect_flag, UtilsPreferences.getString(context, Constant.connect_flag));
                object.put(Constant.connect_flag, "3");
                object.put(Constant.device_type, Constant.device_type_value);

                Log.e("Update Device Record", "called");
                SyncFitData task = new SyncFitData(context, object, onSyncDataListener);
                task.execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    // success listener of steps
    @Override
    public void onGetSteps(int step) {
        steps = step;
        getGoogleFits.new CaloriesCall().execute();
    }

    // success listener of calorie
    @Override
    public void onGetCalories(int cal) {
        cals = cal;
        getGoogleFits.new DistanceCall().execute();
    }

    // success listener of distance
    @Override
    public void onGetDistance(int distance) {
        dis = distance;
        getGoogleFits.new HeartRateCall().execute();
    }

    @Override
    public void onGetHeartRate(int heart) {
        hearts = heart;

        // api call for SyncFitData
        if (ConnectionDetector.internetCheck(context)) {
            JSONObject object = new JSONObject();
            try {
                SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String cDate = formater.format(startDate);

                object.put(Constant.user_id, UtilsPreferences.getString(context, Constant.user_id));
                object.put(Constant.date, cDate);
                object.put(Constant.calories, cals);
                object.put(Constant.steps, steps);
                object.put(Constant.distance, dis);
                object.put(Constant.sleep, "0");
                object.put(Constant.heart_rate, hearts);
                //object.put(Constant.connect_flag, UtilsPreferences.getString(context, Constant.connect_flag));
                object.put(Constant.connect_flag, "2");
                object.put(Constant.device_type, Constant.device_type_value);
                object.put(Constant.accesstoken, UtilsPreferences.getString(context, Constant.accesstoken));

                Log.e("Update Device Record", "called");
                SyncFitData task = new SyncFitData(context, object, onSyncDataListener);
                task.execute();
//                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    // success listener of SyncFitData
    @Override
    public void onSucceedSyncData(String message) {
        if (onUpdateDeviceDataInBackgroundListener != null)
            onUpdateDeviceDataInBackgroundListener.onSucceedUpdateDeviceDataInBackground();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");


        UtilsPreferences.setString(context, Constant.last_update_date, cDate); // added on 04-11-2017

        // commented on 04-11-2017
//        if (startDate.getDate() == new Date().getDate()) {
//            cDate = formater.format(new Date());
//            UtilsPreferences.setString(context, Constant.last_update_date, cDate);
//        } else {
//            Calendar endCalendar = Calendar.getInstance();
//            endCalendar.setTime(startDate);
//            endCalendar.add(Calendar.DAY_OF_YEAR, 1);
//            startDate = endCalendar.getTime();
//            String crrDate = formater.format(startDate);
//            UtilsPreferences.setString(context, Constant.last_update_date, crrDate);
//        }
        //getData();  // added on 12-10-2017
    }

    // failed listener of SyncFitData
    @Override
    public void onFailedSyncData(String message) {
        if (onUpdateDeviceDataInBackgroundListener != null)
            onUpdateDeviceDataInBackgroundListener.onSucceedUpdateDeviceDataInBackground();
    }
}
