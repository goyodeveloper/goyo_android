package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;

public class UpdateProfileImageCall extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnUpdateProfileImageListener listener;

    File postData;
    private boolean isDialog = false;

    //create an listener for success & failure of service call
    public interface OnUpdateProfileImageListener {

        /**
         * @param msg return message from success response
         */
        public void onSucceedToUpdateProfileImage(String msg);

        /**
         * @param error_msg return error message
         */
        public void onFaildToUpdateProfileImage(String error_msg);
    }

    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param data     Json post request
     * @param isDialog boolean for showing progress dialog
     */
    public UpdateProfileImageCall(Context context, OnUpdateProfileImageListener listener, File data, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.update_profile_image_url);
            str_response = JSONParse.callPostWithFile(url, postData);
            Log.e("Upload img Request:", url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response;  // return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.destroyProgressBar();
        }
        try {// get response from network call (service call) & perform operation on it.
            Log.e("Upload img Response:", result);

            JSONObject obj = new JSONObject(result);
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                try {
                    listener.onSucceedToUpdateProfileImage(obj.getString(Constant.data));  // returning success listener
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFaildToUpdateProfileImage(res.getMessage());  // returning failure listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}