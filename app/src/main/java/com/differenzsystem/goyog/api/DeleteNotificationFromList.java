package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 7/31/17.
 */

public class DeleteNotificationFromList extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnDeleteNotificationFromListListener listener;

    JSONObject postData;
    private boolean isDialog = false;

    int pos;

    public interface OnDeleteNotificationFromListListener {
        /**
         * @param pos return position of list
         */
        void onSucceedToDeleteNotificationFromListListener(int pos);

        /**
         * @param error_msg return error message
         */
        void onFaildToDeleteNotificationFromListListener(String error_msg);
    }

    /**
     * @param context  get context of particular screen
     * @param pos      posotion of array list
     * @param listener listener of success & failure of service call
     * @param data     JSON post request
     * @param isDialog boolean for showing progress dialog
     */
    public DeleteNotificationFromList(Context context, int pos, OnDeleteNotificationFromListListener listener, JSONObject data, boolean isDialog) {
        this.context = context;
        this.pos = pos;
        this.listener = listener;
        this.isDialog = isDialog;
        postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        try {
            // performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.delete_notification_from_list_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response;// return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            // get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                listener.onSucceedToDeleteNotificationFromListListener(pos);// returning success listener
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFaildToDeleteNotificationFromListListener(res.getMessage()); // returning failure  listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {// destroy dialog
                UtilsCommon.destroyProgressBar();
            }
        }
    }
}