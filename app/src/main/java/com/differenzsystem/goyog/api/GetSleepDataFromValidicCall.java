package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.model.ValidicSleepModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.gson.Gson;

import org.json.JSONObject;

public class GetSleepDataFromValidicCall extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnGetSleepDataFromValidicListener listener;

    private boolean isDialog = false;
    String startDate, endDate, chartFlag;

    //201=success

    //create an listener for success & failure of service call
    public interface OnGetSleepDataFromValidicListener {

        /**
         *
         * @param summary     return ValidicSummaryModel model
         * @param data        return ValidicSleepModel model
         * @param chartFlag   return chart for eg day, week, hour etc
         */
        public void onSucceedToGetSleepDataFromValidic(ValidicSummaryModel summary, ValidicSleepModel data, String chartFlag);

        /**
         *
         * @param error_msg return error message
         */
        public void onFaildToGetSleepDataFromValidic(String error_msg);
    }

    /**
     *
     * @param context    get context of particular screen
     * @param listener   listener of success & failure of service call
     * @param isDialog   boolean for showing progress dialog
     * @param startDate  get start date
     * @param endDate    get end date
     * @param chartFlag  get chart for eg day, week, hour etc
     */
    public GetSleepDataFromValidicCall(Context context, OnGetSleepDataFromValidicListener listener, boolean isDialog, String startDate, String endDate, String chartFlag) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        this.startDate = startDate;
        this.endDate = endDate;
        this.chartFlag = chartFlag;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        String url;
        try {// performing network call (service call)
            url = context.getResources().getString(R.string.validic_url) + UtilsPreferences.getString(context, Constant.validic_id)
                    + "/sleep.json?access_token=" + Constant.access_token_value
                    + "&start_date=" + startDate + "&end_date=" + endDate + "&expanded=1";

            str_response = JSONParse.GetJSON(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response;  // return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.destroyProgressBar();
        }
        try {
            JSONObject temp = new JSONObject(result);
            JSONObject res = temp.getJSONObject(Constant.summary);
            ValidicSleepModel data;
            ValidicSummaryModel summary;
            if (res.getInt(Constant.status) == 200) {
                try {
                    data = new Gson().fromJson(temp.toString(), ValidicSleepModel.class);
                    summary = new Gson().fromJson(res.toString(), ValidicSummaryModel.class);
                    if (data != null) {
                        listener.onSucceedToGetSleepDataFromValidic(summary, data, chartFlag); // returning success listener
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                listener.onFaildToGetSleepDataFromValidic(res.getString(Constant.message)); // returning failure  listener
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}