package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created on 4/10/16.
 */
public class GetInProgressData extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    OnGetInProgressChallengesListener onGetInProgressChallengesListener;
    private boolean isDialog = false;

    /**
     * @param context                           get context of particular screen
     * @param postData                          JSON post request
     * @param onGetInProgressChallengesListener listener of success & failure of service call
     * @param isDialog                          boolean for showing progress dialog
     */
    public GetInProgressData(Context context, JSONObject postData, OnGetInProgressChallengesListener onGetInProgressChallengesListener, boolean isDialog) {
        this.context = context;
        this.postData = postData;
        this.onGetInProgressChallengesListener = onGetInProgressChallengesListener;
        this.isDialog = isDialog;
        gson = new Gson();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.current_challenge_of_user);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response; // return response from network call (service call)
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        try {// get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonObjectJSONArray = jsonObject.getJSONArray(Constant.data);
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                Type listType = new TypeToken<ArrayList<ChallengesModel>>() {
                }.getType();
                ArrayList<ChallengesModel> inProcessList = new ArrayList<>();
                inProcessList = gson.fromJson(jsonObjectJSONArray.toString(), listType);
                onGetInProgressChallengesListener.onSucceedChallenges(inProcessList); // returning success listener
            } else {
                if (res.getIs_accesstoken_valid()) {
                    onGetInProgressChallengesListener.onFailedChallenges(res.getMessage()); // returning failure  listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {// manage progress dialog visibility conditionally.
                UtilsCommon.destroyProgressBar();
            }
        }
    }

    //create an listener for success & failure of service call
    public interface OnGetInProgressChallengesListener {

        /**
         * @param inProcessList return ChallengesModel model
         */
        public void onSucceedChallenges(ArrayList<ChallengesModel> inProcessList);

        /**
         * @param message return error message
         */
        public void onFailedChallenges(String message);
    }
}
