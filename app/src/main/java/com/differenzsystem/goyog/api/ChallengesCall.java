package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ChallengesCall extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnChallengesListener listener;

    JSONObject postData;
    private boolean isDialog = false;

    public interface OnChallengesListener {
        /**
         * @param inProcessList return in process list of challanges
         * @param thisWeekList  return current week list of challanges
         * @param lastWeekList  return last week list of challanges
         * @param nextWeekList  return next week list of challanges
         */
        public void onSucceedChallenges(ArrayList<ChallengesModel> inProcessList,
                                        ArrayList<ChallengesModel> thisWeekList,
                                        ArrayList<ChallengesModel> lastWeekList,
                                        ArrayList<ChallengesModel> nextWeekList);

        /**
         * @param message return error message
         */
        public void onFailedChallenges(String message);
    }

    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param data     JSON post request
     * @param isDialog boolean for showing progress dialog
     */
    public ChallengesCall(Context context, OnChallengesListener listener, JSONObject data, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        try {
            // performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.challenges_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response;// return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            // get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject dataJsonObject = jsonObject.getJSONObject(Constant.data);

                    JSONArray data_inprocess = new JSONArray();
                    JSONArray data_thisweek = new JSONArray();
                    JSONArray data_lastweek = new JSONArray();
                    JSONArray data_nextweek = new JSONArray();

                    if (dataJsonObject.has(Constant.process) && !dataJsonObject.isNull(Constant.process)) {
                        data_inprocess = dataJsonObject.getJSONArray(Constant.process);
                    }

                    if (dataJsonObject.has(Constant.thisweek) && !dataJsonObject.isNull(Constant.thisweek)) {
                        data_thisweek = dataJsonObject.getJSONArray(Constant.thisweek);
                    }

                    if (dataJsonObject.has(Constant.lastweek) && !dataJsonObject.isNull(Constant.lastweek)) {
                        data_lastweek = dataJsonObject.getJSONArray(Constant.lastweek);
                    }

                    if (dataJsonObject.has(Constant.nextweek) && !dataJsonObject.isNull(Constant.nextweek)) {
                        data_nextweek = dataJsonObject.getJSONArray(Constant.nextweek);
                    }

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    Type listType = new TypeToken<ArrayList<ChallengesModel>>() {
                    }.getType();

                    ArrayList<ChallengesModel> inProcessList = new ArrayList<>();
                    ArrayList<ChallengesModel> thisWeekList = new ArrayList<>();
                    ArrayList<ChallengesModel> lastWeekList = new ArrayList<>();
                    ArrayList<ChallengesModel> nextWeekList = new ArrayList<>();

                    if (data_inprocess != null) {
                        inProcessList = gson.fromJson(data_inprocess.toString(), listType);
                    }

                    if (data_thisweek != null) {
                        thisWeekList = gson.fromJson(data_thisweek.toString(), listType);
                    }

                    if (data_lastweek != null) {
                        lastWeekList = gson.fromJson(data_lastweek.toString(), listType);
                    }

                    if (data_nextweek != null) {
                        nextWeekList = gson.fromJson(data_nextweek.toString(), listType);
                    }

                    listener.onSucceedChallenges(inProcessList, thisWeekList, lastWeekList, nextWeekList);// returning success listener

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFailedChallenges(res.getMessage());// returning failure  listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {
                UtilsCommon.destroyProgressBar();
            }
        }
    }
}