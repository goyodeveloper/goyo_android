package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.GetUserSyncDataModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 7/18/17.
 */

public class get_user_sync_data extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnGetUserSyncData listener;

    JSONObject postData;
    private boolean isDialog = false;

    //create an listener for success & failure of service call
    public interface OnGetUserSyncData {
        /**
         * @param obj return GUserSyncDataModel
         */
        public void onSucceedToUserSyncData(GetUserSyncDataModel obj);

        /**
         * @param error_msg return error message
         */
        public void onFaildToUserSyncData(String error_msg);
    }

    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param data     JSON post request
     * @param isDialog boolean for showing progress dialog
     */
    public get_user_sync_data(Context context, OnGetUserSyncData listener, JSONObject data, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        try {
            // performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.get_user_sync_data);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response;// return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            // get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            GetUserSyncDataModel obj = new Gson().fromJson(result, GetUserSyncDataModel.class);
            if (obj.flag) {
                listener.onSucceedToUserSyncData(obj);// returning success listener
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFaildToUserSyncData(obj.message);// returning failure  listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            listener.onFaildToUserSyncData(e.toString());// returning failure  listener
        } finally {
            if (isDialog) {// destroy dialog
                UtilsCommon.destroyProgressBar();
            }
        }
    }
}