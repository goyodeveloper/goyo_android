package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 23/1/17.
 */

public class ForceUpdate extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    OnForceUpdateListener listener;
    private boolean isDialog = false;

    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param isDialog boolean for showing progress dialog
     */
    public ForceUpdate(Context context, OnForceUpdateListener listener, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {
            // performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.force_update);
            str_response = JSONParse.postJSON(url, new JSONObject());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response;//return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            // get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                JSONObject jsonObject = new JSONObject(result);
                JSONObject newJsonObject = jsonObject.getJSONObject(Constant.data);// returning success listener
                listener.onSucceedForceUpdate(newJsonObject);
            } else {
                listener.onFailedForceUpdate(res.getMessage());// returning failure  listener
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {// destroy dialog
                UtilsCommon.destroyProgressBar();
            }
        }
    }

    public interface OnForceUpdateListener {

        /**
         * @param jsonResponse return service call response
         */
        public void onSucceedForceUpdate(JSONObject jsonResponse);

        /**
         * @param message return error message
         */
        public void onFailedForceUpdate(String message);
    }
}
