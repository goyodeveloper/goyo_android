package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 12/12/16.
 */

public class CheckMacId extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    OnCheckMacIdListener listener;

    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param postData JSON post request
     */
    public CheckMacId(Context context, OnCheckMacIdListener listener, JSONObject postData) {
        this.context = context;
        this.postData = postData;
        this.listener = listener;
        gson = new Gson();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {
            // performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.check_mac_id);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response;// return response from network call (service call)
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            // get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                listener.onSucceedCheckMacId(res.getMessage());// returning success listener
            } else {
                listener.onFailedCheckMacId(res.getMessage());// returning failure  listener
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnCheckMacIdListener {
        /**
         * @param message return success message
         */
        public void onSucceedCheckMacId(String message);

        /**
         * @param message return error message
         */
        public void onFailedCheckMacId(String message);
    }
}