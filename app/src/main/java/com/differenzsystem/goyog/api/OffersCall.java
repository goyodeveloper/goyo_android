package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.OffersModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class OffersCall extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnOffersListener listener;

    JSONObject postData;
    private boolean isDialog = false;

    //create an listener for success & failure of service call
    public interface OnOffersListener {

        /**
         * @param inProcessList return OffersModel model
         */
        public void onSucceedOffers(ArrayList<OffersModel> inProcessList);

        /**
         * @param message return error message
         */
        public void onFailedOffers(String message);
    }

    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param data     Json post request
     * @param isDialog boolean for showing progress dialog
     */
    public OffersCall(Context context, OnOffersListener listener, JSONObject data, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.offers_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response; // return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {// get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray dataJsonObject = jsonObject.getJSONArray(Constant.data);

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    Type listType = new TypeToken<ArrayList<OffersModel>>() {
                    }.getType();

                    ArrayList<OffersModel> inProcessList = new ArrayList<>();
                    JSONArray data_inprocess = new JSONArray();
                    data_inprocess = jsonObject.getJSONArray(Constant.data);

                    inProcessList = gson.fromJson(data_inprocess.toString(), listType);
                    listener.onSucceedOffers(inProcessList); // returning success listener

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFailedOffers(res.getMessage()); // returning failure listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {// manage progress dialog visibility conditionally.
                UtilsCommon.destroyProgressBar();
            }
        }
    }
}