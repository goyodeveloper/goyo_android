package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 26/11/16.
 */

public class UpdatePopupStatus extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    OnUpdatePopupStatusListener listener;

    /**
     * @param context  get context of particular screen
     * @param postData Json post request
     * @param listener listener of success & failure of service call
     */
    public UpdatePopupStatus(Context context, JSONObject postData, OnUpdatePopupStatusListener listener) {
        this.context = context;
        this.postData = postData;
        this.listener = listener;
        gson = new Gson();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.update_popup_status);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response;  // return response from network call (service call)
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        try {// get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                JSONObject jsonObject = new JSONObject(result);
                //JSONArray jsonObjectJSONArray = jsonObject.getJSONArray(Constant.data);
                listener.onSucceedUpdatePopupStatus(res.getMessage());  // returning success listener
            } else {
                if (res.getIs_accesstoken_valid()) {
                    listener.onFailedUpdatePopupStatus(res.getMessage());   // returning failure listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //create an listener for success & failure of service call
    public interface OnUpdatePopupStatusListener {
        /**
         * @param message return message from success response
         */
        public void onSucceedUpdatePopupStatus(String message);

        /**
         * @param message return error message
         */
        public void onFailedUpdatePopupStatus(String message);
    }
}
