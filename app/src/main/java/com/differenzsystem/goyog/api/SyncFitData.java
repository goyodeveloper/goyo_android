package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created on 4/10/16.
 */
public class SyncFitData extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    OnSyncDataListener onSyncDataListener;
    Gson gson;

    /**
     * @param context            get context of particular screen
     * @param postData           Json post request
     * @param onSyncDataListener listener of success & failure of service call
     */
    public SyncFitData(Context context, JSONObject postData, OnSyncDataListener onSyncDataListener) {
        this.context = context;
        this.postData = postData;
        this.onSyncDataListener = onSyncDataListener;
        gson = new Gson();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.update_device_records);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response;  // return response from network call (service call)
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {// get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                onSyncDataListener.onSucceedSyncData(res.getMessage()); // returning success listener
            } else {
                if (res.getIs_accesstoken_valid()) {
                    onSyncDataListener.onFailedSyncData(res.getMessage()); // returning failure listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //create an listener for success & failure of service call
    public interface OnSyncDataListener {

        /**
         * @param message return message from response
         */
        public void onSucceedSyncData(String message);

        /**
         * @param message return error message
         */
        public void onFailedSyncData(String message);
    }
}
