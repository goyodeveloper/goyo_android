package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.CompletedBadge;
import com.differenzsystem.goyog.model.CompletedChallenge;
import com.differenzsystem.goyog.model.TimeLineResult;
import com.differenzsystem.goyog.model.completedchallbadges;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created on 27/9/16.
 */
public class GetCompletedChallangeData extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    boolean showProgressDialog;
    OnCompletedChallengesListener onCompletedChallengesListener;

    /**
     * @param context                       get context of particular screen
     * @param postData                      JSON post request
     * @param onCompletedChallengesListener listener of success & failure of service call
     * @param showProgressDialog            boolean for showing progress dialog
     */
    public GetCompletedChallangeData(Context context, JSONObject postData, OnCompletedChallengesListener onCompletedChallengesListener, boolean showProgressDialog) {
        this.context = context;
        this.postData = postData;
        this.showProgressDialog = showProgressDialog;
        this.onCompletedChallengesListener = onCompletedChallengesListener;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showProgressDialog)// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
    }

    @Override
    protected String doInBackground(String... voids) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.completed_challenges_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response;// return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        UtilsCommon.destroyProgressBar();
        try {// get response from network call (service call) & perform operation on it.

            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {

                JSONObject jsonObject = new JSONObject(result);

                JSONArray data_challanges = new JSONArray();
                JSONArray data_badges = new JSONArray();
                JSONArray data_timeline = new JSONArray();

                ArrayList<CompletedChallenge> compchallages = new ArrayList<>();
                ArrayList<CompletedBadge> compbadges = new ArrayList<>();
                ArrayList<TimeLineResult> timeLineResults = new ArrayList<>();

                data_challanges = jsonObject.getJSONArray(Constant.completed_challenges);
                data_badges = jsonObject.getJSONArray(Constant.completed_badges);
                data_timeline = jsonObject.getJSONArray(Constant.time_line_result);

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                Type listType = new TypeToken<ArrayList<CompletedChallenge>>() {
                }.getType();

                Type listTypebadge = new TypeToken<ArrayList<CompletedBadge>>() {
                }.getType();

                Type listTypeTimeLine = new TypeToken<ArrayList<TimeLineResult>>() {
                }.getType();

                compchallages = gson.fromJson(data_challanges.toString(), listType);
                compbadges = gson.fromJson(data_badges.toString(), listTypebadge);
                timeLineResults = gson.fromJson(data_timeline.toString(), listTypeTimeLine);

                completedchallbadges completedchallbadges = new completedchallbadges();
                completedchallbadges.setFlag(jsonObject.getBoolean("flag"));
                completedchallbadges.setChallenges_point(jsonObject.getInt("challenges_point"));
                completedchallbadges.setCompletedChallenges(compchallages);
                completedchallbadges.setCompletedBadges(compbadges);
                completedchallbadges.setTimeLineResults(timeLineResults);

                onCompletedChallengesListener.onSucceedChallenges(completedchallbadges);// returning success listener
            } else {
                if (res.getIs_accesstoken_valid()) {
                    onCompletedChallengesListener.onFailedChallenges(res.getMessage());// returning failure  listener
                } else { // Logout user when access token is invalid
                    Application global = (Application) context.getApplicationContext();
                    global.doLogout(context);
                }
            }
        } catch (Exception e) {
            onCompletedChallengesListener.onFailedChallenges(e.getMessage());// returning failure  listener
        }
    }

    //create an listener for success & failure of service call
    public interface OnCompletedChallengesListener {

        /**
         * @param completedchallbadges return completedchallbadges model
         */
        public void onSucceedChallenges(completedchallbadges completedchallbadges);

        /**
         * @param message return error message
         */
        public void onFailedChallenges(String message);
    }
}
