package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.model.RegisterModel;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 8/16/16.
 */

public class RegisterCall extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnRegisterUserListener listener;

    JSONObject postData;
    private boolean isDialog = false;

    //create an listener for success & failure of service call
    public interface OnRegisterUserListener {

        /**
         *
         * @param obj return RegisterModel model
         */
        public void onSucceedToRegister(RegisterModel obj);

        /**
         *
         * @param error_msg return error message
         */
        public void onFaildToRegister(String error_msg);
    }

    /**
     *
     * @param context   get context of particular screen
     * @param listener  listener of success & failure of service call
     * @param data      Json post request
     * @param isDialog  boolean for showing progress dialog
     */
    public RegisterCall(Context context, OnRegisterUserListener listener, JSONObject data, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.register_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response; // return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {// get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject newJsonObject = jsonObject.getJSONObject(Constant.data);
                    RegisterModel obj = new Gson().fromJson(newJsonObject.toString(), RegisterModel.class);

                    if (newJsonObject.has(Constant.initial_reward))
                        Globals.Intial_reward = newJsonObject.getJSONObject(Constant.initial_reward);

                    if (obj != null) {
                        listener.onSucceedToRegister(obj);   // returning success listener
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                listener.onFaildToRegister(res.getMessage()); // returning failure listener
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {// manage progress dialog visibility conditionally.
                UtilsCommon.destroyProgressBar();
            }
        }
    }
}