package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.http.JSONResponse;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 18/1/17.
 */

public class CheckEmail extends AsyncTask<String, Integer, String> {
    Context context;
    JSONObject postData;
    Gson gson;
    OnCheckEmailListener listener;
    private boolean isDialog = false;


    /**
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param postData JSON post request
     * @param isDialog boolean for showing progress dialog
     */
    public CheckEmail(Context context, OnCheckEmailListener listener, JSONObject postData, boolean isDialog) {
        this.context = context;
        this.postData = postData;
        this.listener = listener;
        this.isDialog = isDialog;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        String str_response = null;
        try {
            // performing network call (service call)
            String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.check_email);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str_response;// return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            // get response from network call (service call) & perform operation on it.
            JSONResponse res = gson.fromJson(result, JSONResponse.class);
            if (res.getFlag()) {
                listener.onSucceedCheckEmail(res.getMessage()); // returning success listener
            } else {
                listener.onFailedCheckEmaild(res.getMessage()); // returning failure  listener
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (isDialog) {
                UtilsCommon.destroyProgressBar();
            }
        }
    }

    //create an listener for success & failure of service call
    public interface OnCheckEmailListener {
        /**
         * @param message return success message
         */
        public void onSucceedCheckEmail(String message);

        /**
         * @param message return error message
         */
        public void onFailedCheckEmaild(String message);
    }
}