package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.model.ValidicFitnessModel;
import com.differenzsystem.goyog.model.ValidicSummaryModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.gson.Gson;

import org.json.JSONObject;

public class GetFitnessDataFromValidicCall extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnGetFitnessFromValidicListener listener;

    private boolean isDialog = false;
    String startDate, endDate, chartFlag;

    //201=success

    //create an listener for success & failure of service call
    public interface OnGetFitnessFromValidicListener {
        /**
         * @param summary   return summary model
         * @param data      return ValidicFitnessModel
         * @param chartFlag return chart flag.
         */
        public void onSucceedToGetFitnessFromValidic(ValidicSummaryModel summary, ValidicFitnessModel data, String chartFlag);

        public void onFaildToGetFitnessFromValidic(String error_msg);
    }

    /**
     * @param context   context of particular screen
     * @param listener  listener of success & failure of service call
     * @param isDialog  boolean for showing progress dialog
     * @param startDate start date (from date)
     * @param endDate   end date (to date)
     * @param chartFlag chart flag
     */
    public GetFitnessDataFromValidicCall(Context context, OnGetFitnessFromValidicListener listener, boolean isDialog, String startDate, String endDate, String chartFlag) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        this.startDate = startDate;
        this.endDate = endDate;
        this.chartFlag = chartFlag;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.  performing
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        String url;
        try {// performing network call (service call) on validic server
            url = context.getResources().getString(R.string.validic_url) + UtilsPreferences.getString(context, Constant.validic_id)
                    + "/fitness.json?access_token=" + Constant.access_token_value
                    + "&start_date=" + startDate + "&end_date=" + endDate + "&expanded=1";

            str_response = JSONParse.GetJSON(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response;// return response from network call (service call)  performing
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (isDialog) {// destroy dialog
            UtilsCommon.destroyProgressBar();
        }
        try {// get response from network call (service call) & perform operation on it.
            JSONObject temp = new JSONObject(result);
            JSONObject res = temp.getJSONObject(Constant.summary);
            ValidicFitnessModel data;
            ValidicSummaryModel summary;
            if (res.getInt(Constant.status) == 200) {
                try {
                    data = new Gson().fromJson(temp.toString(), ValidicFitnessModel.class);
                    summary = new Gson().fromJson(res.toString(), ValidicSummaryModel.class);
                    if (data != null) {
                        listener.onSucceedToGetFitnessFromValidic(summary, data, chartFlag);// returning success listener
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                listener.onFaildToGetFitnessFromValidic(res.getString(Constant.message));// returning failure  listener
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}