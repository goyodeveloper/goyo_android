package com.differenzsystem.goyog.api;

import android.content.Context;
import android.os.AsyncTask;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.model.ValidicCreateUserModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class ValidicCreateUserCall extends AsyncTask<String, Integer, String> {
    private Context context;
    Gson gson;
    OnCreateValidicUserListener listener;

    JSONObject postData;
    private boolean isDialog = false;

    //201=success

    //create an listener for success & failure of service call
    public interface OnCreateValidicUserListener {

        /**
         *
         * @param obj return ValidicCreateUserModel model
         */
        public void onSucceedToCreateValidicUser(ValidicCreateUserModel obj);

        /**
         *
         * @param error_msg return error message
         */
        public void onFaildToCreateValidicUser(String error_msg);
    }

    /**
     *
     * @param context  get context of particular screen
     * @param listener listener of success & failure of service call
     * @param data     Json post request
     * @param isDialog boolean for showing progress dialog
     */
    public ValidicCreateUserCall(Context context, OnCreateValidicUserListener listener, JSONObject data, boolean isDialog) {
        this.context = context;
        this.listener = listener;
        this.isDialog = isDialog;
        postData = data;
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.showProgressDialog(context);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String str_response = null;
        try {// performing network call (service call)
            String url = context.getResources().getString(R.string.validic_create_user_url);
            str_response = JSONParse.postJSON(url, postData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str_response;  // return response from network call (service call)
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (isDialog) {// manage progress dialog visibility conditionally.
            UtilsCommon.destroyProgressBar();
        }
        try {// get response from network call (service call) & perform operation on it.
            JSONObject res = new JSONObject(result);
            if (res.getInt(Constant.code) == 201) {
                try {
                    JSONObject newJsonObject = res.getJSONObject(Constant.user);
                    ValidicCreateUserModel obj = new Gson().fromJson(newJsonObject.toString(), ValidicCreateUserModel.class);
                    if (obj != null) {
                        listener.onSucceedToCreateValidicUser(obj);                    // returning success listener
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                listener.onFaildToCreateValidicUser(res.getString(Constant.message));  // returning failure listener
            }
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
}