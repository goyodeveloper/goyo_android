package com.differenzsystem.goyog.api;

import android.content.Context;
import android.util.Log;

import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.model.ChartDataModel;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created on 4/10/16.
 */
public class UpdateChallengesStatusInBackground implements GetInProgressData.OnGetInProgressChallengesListener, GetGoogleFits.OnGetFitsListener, ChallengeAcceptCall.OnChallengeAcceptListener {
    GoogleApiClient mClient;
    Context context;
    GetInProgressData.OnGetInProgressChallengesListener onGetInProgressChallengesListener;
    int cnt = 0;
    int maxCnt = 0;
    ArrayList<ChallengesModel> inProcessList;
    GetGoogleFits getGoogleFits;
    GetGoogleFits.OnGetFitsListener onGetFitsListener;
    int steps = 0, cals = 0, dis = 0, hearts = 0;
    int type, chllng_val = 0, sec_chllng_val = 0;
    String status;
    ArrayList<ChartDataModel> chartmodellist = new ArrayList<>();

    ChallengeAcceptCall.OnChallengeAcceptListener onChallengeAcceptListener;

    //change
    OnUpdateChallengesStatusInBackgroundListener listener;

    public UpdateChallengesStatusInBackground(Context context, GoogleApiClient mClient) {
        this.mClient = mClient;
        this.context = context;
        onGetInProgressChallengesListener = this;
        onGetFitsListener = this;
        onChallengeAcceptListener = this;
    }

    public UpdateChallengesStatusInBackground(Context context, GoogleApiClient mClient, OnUpdateChallengesStatusInBackgroundListener listener) {
        this.mClient = mClient;
        this.context = context;
        onGetInProgressChallengesListener = this;
        onGetFitsListener = this;
        onChallengeAcceptListener = this;

        this.listener = listener;
    }

    public void getData() {
        JSONObject object = new JSONObject();
        try {
            object.put(Constant.user_id, UtilsPreferences.getString(context, Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(context, Constant.accesstoken));
            GetInProgressData getInProgressData = new GetInProgressData(context, object, onGetInProgressChallengesListener, false);
            getInProgressData.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // call steps, calorie, distance request data as per pequest type
    public void doProcess() {
        if (cnt < maxCnt) {
            type = Integer.parseInt(inProcessList.get(cnt).getChallenge_type());
            chllng_val = Integer.parseInt(inProcessList.get(cnt).getChallenge_value());
            getGoogleFits = new GetGoogleFits(context, false, inProcessList.get(cnt), mClient, onGetFitsListener);

            switch (type) {
                case 1:
                    getGoogleFits.new StepsCall().execute();
                    break;
                case 2:
                    getGoogleFits.new CaloriesCall().execute();
                    break;
                case 3:
                    getGoogleFits.new DistanceCall().execute();
                    break;
                case 4:
                    getGoogleFits.new HeartRateCall().execute();
                    break;
                case 6:
                    sec_chllng_val = Integer.parseInt(inProcessList.get(cnt).getSecond_challenge_value());
                    getGoogleFits.new StepsCall().execute();
                    break;
                case 7:
                    sec_chllng_val = Integer.parseInt(inProcessList.get(cnt).getSecond_challenge_value());
                    getGoogleFits.new StepsCall().execute();
                    break;
            }
        } else {
            Log.e("back call", "completed");
            if (listener != null)
                listener.onSucceedUpdateChallengesStatusInBackground(chartmodellist);
        }
    }

    // success listener of GetInProgressData
    @Override
    public void onSucceedChallenges(ArrayList<ChallengesModel> inProcessList) {
        Log.e("data in p", String.valueOf(inProcessList));
        this.inProcessList = inProcessList;
        if (inProcessList.size() > 0) {
            maxCnt = inProcessList.size();
            doProcess();
        } else {
            Log.e("no date", "for update status");
        }
    }

    // failed listener of GetInProgressData
    @Override
    public void onFailedChallenges(String message) {
        Log.e("data in p failed", message);
        if (listener != null)
            listener.onSucceedUpdateChallengesStatusInBackground(chartmodellist);
    }

    // success listener of calorie
    @Override
    public void onGetCalories(int cal) {
        cals = cal;
        checkNupdate();
    }

    // success listener of steps
    @Override
    public void onGetSteps(int step) {
        steps = step;
        if (type == 7) {
            getGoogleFits.new CaloriesCall().execute();
        } else if (type == 6) {
            getGoogleFits.new DistanceCall().execute();
        } else {
            checkNupdate();
        }
    }

    // success listener of distance
    @Override
    public void onGetDistance(int distance) {
        dis = distance;
        checkNupdate();
    }

    @Override
    public void onGetHeartRate(int heart) {
        hearts = heart;
        checkNupdate();
    }

    // set data in chartmodel as per request type
    public void checkNupdate() {
        ChartDataModel chartmodel = new ChartDataModel();
        switch (type) {
            case 1:
                if (steps < chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(1);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setVal(steps);
                chartmodel.setSecval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 2:
                if (cals < chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(2);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setVal(cals);
                chartmodel.setSecval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 3:
                if (dis < chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(3);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setVal(dis);
                chartmodel.setSecval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 4:
                if (hearts < chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(1);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setVal(hearts);
                chartmodel.setSecval(0);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 6:
                if (steps < chllng_val || dis < sec_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(6);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setVal(steps);
                chartmodel.setSecval(dis);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
            case 7:
                if (steps < chllng_val || cals < sec_chllng_val) {
                    status = "4";
                } else {
                    status = "2";
                }
                chartmodel.setChallengetype(7);
                chartmodel.setTotalval(Integer.valueOf(inProcessList.get(cnt).getChallenge_value()));
                chartmodel.setSectotalval(Integer.valueOf(inProcessList.get(cnt).getSecond_challenge_value()));
                chartmodel.setVal(steps);
                chartmodel.setSecval(cals);
                chartmodel.setChallengesModels(inProcessList.get(cnt));
                chartmodellist.add(chartmodel);
                break;
        }
        UpdateChallageStatus(status);
    }

    /**
     * // update challenge status call
     * @param status get status
     */
    public void UpdateChallageStatus(String status) {
        try {
            JSONObject object = new JSONObject();
            object.put(Constant.user_id, UtilsPreferences.getString(context, Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(context, Constant.accesstoken));
            object.put(Constant.challenge_id, inProcessList.get(cnt).getChallenge_id());
            object.put(Constant.challenge_status, status);

            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            object.put(Constant.timezone, tz.getID());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateandTime = sdf.format(new Date());
            object.put(Constant.date, currentDateandTime);
            checkNmakeCall(object, status);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // api call for update challenge status
    private void checkNmakeCall(JSONObject object, String status) {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss");
        String startTimeString = "";
        if (inProcessList.get(cnt).getChallenge_start_date().equalsIgnoreCase("0000-00-00 00:00:00") ||
                inProcessList.get(cnt).getChallenge_start_date() == null) {
            startTimeString = inProcessList.get(cnt).getAccept_date();
        } else {
            startTimeString = inProcessList.get(cnt).getChallenge_start_date();
        }
        Date startDate = null;
        try {
            startDate = formater.parse(startTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar endCalendar = Calendar.getInstance();
        Date endDate = startDate;
        endCalendar.setTime(endDate);
        int duration = Integer.parseInt(inProcessList.get(cnt).getDuration());
        endCalendar.add(Calendar.MINUTE, duration);
        Date end = endCalendar.getTime();

        String cDate;
        Calendar c = Calendar.getInstance();
        Date currentDate = c.getTime();
        SimpleDateFormat formaterN = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cDate = formaterN.format(currentDate);
        try {
            currentDate = formaterN.parse(cDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.e("currNend", currentDate + " : " + end);
        if (status.equalsIgnoreCase("4")) {
            if (currentDate.after(end))
                new ChallengeAcceptCall(context, onChallengeAcceptListener, object, false).execute();
            else {
                Log.e("not update", "now");
                cnt++;
                doProcess();
            }
        } else {
            new ChallengeAcceptCall(context, onChallengeAcceptListener, object, false).execute();
        }
    }

    // success listener of ChallengeAcceptCall
    @Override
    public void onSucceedChallengeAccept(String message, JSONObject jsonResponse) {
        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(context, Constant.Key_inProgressList);
        cnt++;
        doProcess();
        int profileadge = UtilsPreferences.getInt(context, "ProfileBadge");
        UtilsPreferences.setInt(context, "ProfileBadge", profileadge + 1);
    }

    // failed listener of ChallengeAcceptCall
    @Override
    public void onFailedChallengeAccept(String message) {
        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(context, Constant.Key_inProgressList);
        cnt++;
        doProcess();
    }

    // set up listener
    public interface OnUpdateChallengesStatusInBackgroundListener {
        public void onSucceedUpdateChallengesStatusInBackground(ArrayList<ChartDataModel> chartmodellist);

        // public void onFailedUpdateChallengesStatusInBackground(String message);
    }
}
