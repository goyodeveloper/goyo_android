package com.differenzsystem.goyog.api;

import android.os.AsyncTask;

import com.differenzsystem.goyog.utility.Debugger;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.text.DateFormat.getTimeInstance;

/**
 * Created by Union Assurance PLC on 9/17/16.
 */
public class StepsFromDeviceCall extends AsyncTask<Void, Void, Void> {
    String TAG = getClass().getName();
    String stepsData = "0";
    GoogleApiClient mClient = null;
    String startDate, endDate;
    OnGetStepDataListener listener;

    /**
     *
     * @param mClient   get api client of particular screen
     * @param startDate get start date
     * @param endDate   get end date
     * @param listener  listener of success & failure of service call
     */
    public StepsFromDeviceCall(GoogleApiClient mClient, String startDate, String endDate, OnGetStepDataListener listener) {
        this.mClient = mClient;
        this.startDate = startDate;
        this.endDate = endDate;
        this.listener = listener;
    }

    //create an listener for success & failure of service call
    public interface OnGetStepDataListener {

        /**
         *
         * @param value return value
         */
        public void onSucceedToGetStepData(String value);

    }

    protected Void doInBackground(Void... params) {
        try {// performing network call (service call)
            DataReadRequest readRequest = requestFitnessData(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA);
            DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
            stepsData = readData(dataReadResult);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        try {
            listener.onSucceedToGetStepData((stepsData.equalsIgnoreCase("") ? "0" : stepsData)); // returning success listener
            // tv_stepsData.setText(stepsData.equalsIgnoreCase("") ? "0" : stepsData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // return the fitness data from range date

    /**
     *
     * @param datatype1
     * @param datatype2
     * @return
     */
    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss");

            Date sDate = null;
            Date eDate = null;
            try {
                sDate = formater.parse(startDate);
                eDate = formater.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long startMillisecond = sDate.getTime();
            long endMillisecond = eDate.getTime();

            Debugger.debugE("level-Start time", sDate + "=>");
            Debugger.debugE("Level-End time", eDate + "=>");

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return readRequest;
    }

    // request data
    public String readData(DataReadResult dataReadResult) {
        String value = "";
        try {
            if (dataReadResult.getBuckets().size() > 0) {
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    // Read actual data of steps/calories/heart/distance
    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }
}
