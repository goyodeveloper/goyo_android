package com.differenzsystem.goyog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.differenzsystem.goyog.FitnessBandTracker.ScanDeviceActivity;
import com.differenzsystem.goyog.api.ForceUpdate;
import com.differenzsystem.goyog.api.Logout;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.JSONParse;
import com.differenzsystem.goyog.model.NotificationBadgeCountModel;
import com.differenzsystem.goyog.userProfile.LoginActivity;
import com.differenzsystem.goyog.utility.Comparator;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import me.leolin.shortcutbadger.ShortcutBadger;


/**
 * Created by Union Assurance PLC on 5/7/16.
 */

public class Application extends MultiDexApplication implements ForceUpdate.OnForceUpdateListener {

    @SuppressLint("StaticFieldLeak")
    public static Context context;
    static ForceUpdate.OnForceUpdateListener onForceUpdateListener;
    static Activity activity;

    public static FirebaseAnalytics mFirebaseAnalytics;

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * Note
     * usertype 1 = live user
     * usertype 2 = demo user
     * connect_flag = connected_with_validic means get data from validic
     * connect_flag = connected_with_device means get data from google fit
     * connect_flag = connected_with_tracker means get data from band
     */

    @Override
    public void onCreate() {
        super.onCreate();
        this.context = getApplicationContext();
        ProtocolUtils.getInstance().init(getApplicationContext());
        onForceUpdateListener = this;

        if (mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        }

        if (UtilsPreferences.getString(getApplicationContext(), Constant.connect_flag) != null) {
            if (UtilsPreferences.getString(getApplicationContext(), Constant.user_type).equalsIgnoreCase("1")) { // condition for live user
                if (UtilsPreferences.getString(this, Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    if (BleSharedPreferences.getInstance().getIsBind()) {
                        BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
//                        bluetooth.enable(); // commented on 24-07-2017
                        ProtocolUtils.getInstance().setCanConnect(true);
                    }
                }
            }
        }
        registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks());
    }

    public static Context getContext() {
        return context;
    }

    // call for force update api
    public static void upgradeVersion(Activity act) {
        try {
            activity = act;
            new ForceUpdate(activity, onForceUpdateListener, false).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void recordScreenViews(Activity context, String name) {
        // [START set_current_screen]
        if (mFirebaseAnalytics != null) {
            mFirebaseAnalytics.setCurrentScreen(context, name, null /* class override */);
        }
        // [END set_current_screen]
    }

    // success listener of ForceUpdate
    @Override
    public void onSucceedForceUpdate(JSONObject jsonResponse) {
        try {
            String ServerVersion = jsonResponse.getString("android_version");
            int flag = Integer.parseInt(jsonResponse.getString("flag"));

            if (flag != 0) {
                if (Comparator.isVersionDownloadableNewer(this, ServerVersion)) {//1.7
                    //open popup for update version
                    ForceUpdatePopup();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // show dialog popup for force update
    public void ForceUpdatePopup() {
        final Dialog alertDialog = new Dialog(activity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.setting_edit_name_dialog);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        alertDialog.setCancelable(false);

        final EditText edt_name = (EditText) alertDialog.findViewById(R.id.et_name);
        TextView tv_cancel = (TextView) alertDialog.findViewById(R.id.tv_cancel);
        TextView tv_change = (TextView) alertDialog.findViewById(R.id.tv_change);
        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        TextView tv_wrn_text = (TextView) alertDialog.findViewById(R.id.tv_wrn_text);
        View vi_devider = (View) alertDialog.findViewById(R.id.vi_devider);

        edt_name.setVisibility(View.GONE);
        tv_cancel.setVisibility(View.GONE);
        vi_devider.setVisibility(View.GONE);
        tv_wrn_text.setVisibility(View.VISIBLE);

        tv_change.setText(R.string.update);
        tv_popup_title.setText(R.string.new_version);
        tv_wrn_text.setText(R.string.update_text);//R.string.sync_warning

        tv_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + appPackageName))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName))
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            }
        });

        alertDialog.show();
    }

    @Override
    public void onFailedForceUpdate(String message) {

    }

    // callback for activty lifecycle stage
    public static class MyActivityLifecycleCallbacks implements android.app.Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            Log.i(activity.getClass().getSimpleName(), "onCreate(Bundle)");
        }

        @Override
        public void onActivityStarted(Activity activity) {
            Log.i(activity.getClass().getSimpleName(), "onStart()");
        }

        @Override
        public void onActivityResumed(Activity activity) {
            Log.e(activity.getClass().getSimpleName(), "onResume()");
            ShortcutBadger.removeCount(context);
            UtilsPreferences.setInt(context, Constant.totalbadge, 0);
            if (!activity.getClass().getSimpleName().equalsIgnoreCase("SplashActivity")) {
                upgradeVersion(activity);
            }
            if (ConnectionDetector.internetCheck(activity)) {
                if (activity instanceof ScanDeviceActivity) {
                    ConnectionDetector.isGpsEnable(activity);
                }
                String user_id = UtilsPreferences.getString(context, Constant.user_id);
                String accesstoken = UtilsPreferences.getString(context, Constant.accesstoken);
                if (user_id != null && !user_id.isEmpty() && accesstoken != null && !accesstoken.isEmpty()) {
                    callNotificationBadgeCountApi(activity);
                }
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
            Log.i(activity.getClass().getSimpleName(), "onPause()");
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            Log.i(activity.getClass().getSimpleName(), "onSaveInstanceState(Bundle)");
        }

        @Override
        public void onActivityStopped(Activity activity) {
            Log.i(activity.getClass().getSimpleName(), "onStop()");
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            Log.i(activity.getClass().getSimpleName(), "onDestroy()");
        }
    }

    /**
     * // api call for NotificationBadgeCount
     *
     * @param activity get activity for particular screen
     */
    static void callNotificationBadgeCountApi(Activity activity) {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.user_id, UtilsPreferences.getString(activity, Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(activity, Constant.accesstoken));

            NotificationBadgeCountCall task = new NotificationBadgeCountCall(activity, data, false);
            task.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // api NotificationBadgeCount
    public static class NotificationBadgeCountCall extends AsyncTask<String, Integer, String> {
        private Context context;
        Gson gson;

        JSONObject postData;
        private boolean isDialog = false;


        public NotificationBadgeCountCall(Context context, JSONObject data, boolean isDialog) {
            this.context = context;
            this.isDialog = isDialog;
            postData = data;
            gson = new Gson();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isDialog) {
                UtilsCommon.showProgressDialog(context);
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String str_response = null;
            try {
                String url = context.getResources().getString(R.string.server_url) + context.getResources().getString(R.string.notification_badge_count_url);
                str_response = JSONParse.postJSON(url, postData);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return str_response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                NotificationBadgeCountModel notificationBadgeCountModel = new Gson().fromJson(result, NotificationBadgeCountModel.class);
                if (notificationBadgeCountModel.flag) {
                    UtilsPreferences.setInt(context, Constant.NotificationBadge, notificationBadgeCountModel.data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (isDialog) {
                    UtilsCommon.destroyProgressBar();
                }
            }
        }
    }

    public void doLogout(Context mContext) {
        LogoutCall(mContext);
        String deviceToken = UtilsPreferences.getString(mContext, Constant.devicegcmid);
        Globals.clearData();
        UtilsPreferences.clearPreferences(mContext);
        UtilsPreferences.setString(mContext, Constant.devicegcmid, deviceToken);
        UtilsPreferences.setBoolean(mContext, Constant.tutorial_flag, true);
        UtilsPreferences.setBoolean(mContext, Constant.first_launch, false);
        if (BleSharedPreferences.getInstance().getIsBind()) {
            UtilsPreferences.setString(mContext, Constant.GOYOHRStatus, "Bundled");
        } else {
            UtilsPreferences.setString(mContext, Constant.GOYOHRStatus, "Unbundled");
        }
        ((Activity) mContext).overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);


        // clear challenges and levelinformation prefrences
        UtilsPreferences.clearKeyPreferences(mContext, Constant.Key_inProgressList);
        UtilsPreferences.clearKeyPreferences(mContext, Constant.Key_level_info);
        FacebookSdk.sdkInitialize(mContext);
        if (LoginManager.getInstance() != null)
            LoginManager.getInstance().logOut();

        startActivity(new Intent(mContext, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        ((Activity) mContext).finishAffinity();
    }


    // api call for logout
    public void LogoutCall(Context mContext) {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.user_id, UtilsPreferences.getString(mContext, Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));

            if (ConnectionDetector.isConnectingToInternet(mContext)) {
                Logout reg = new Logout(mContext, data);
                reg.execute();
            } else {
                //Toast.makeText(mContext, getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
