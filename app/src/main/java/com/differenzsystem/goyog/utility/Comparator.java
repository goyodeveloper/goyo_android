package com.differenzsystem.goyog.utility;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by Union Assurance PLC on 15/2/17.
 */

public class Comparator {


    public static boolean isVersionDownloadableNewer(Context mActivity, String versionDownloadable) {
        String versionInstalled = null;
        try {
            versionInstalled = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        if (versionInstalled.equals(versionDownloadable)) { // If it is equal, no new version downloadable
            return false;
        } else {
            return versionCompareNumerically(versionDownloadable, versionInstalled) > 0; // Return if the versionDownloadble is newer than the installed
        }
    }


    public static Integer versionCompareNumerically(String str1, String str2) {
        String[] vals1 = str1.split("\\.");
        String[] vals2 = str2.split("\\.");
        int i = 0;
        // set index to first non-equal ordinal or length of shortest version string
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
            i++;
        }
        try {
            // compare first non-equal ordinal number
            if (i < vals1.length && i < vals2.length) {
                int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
                return Integer.signum(diff);
            }
            // the strings are equal or one string is a substring of the other
            // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
            else {
                return Integer.signum(vals1.length - vals2.length);
            }
        } catch (NumberFormatException e) {
            // Possibly there are different versions of the app in the store, so we can't check.
            return 0;
        }
    }

}
