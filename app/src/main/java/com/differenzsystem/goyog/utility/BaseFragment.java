package com.differenzsystem.goyog.utility;

import android.support.v4.app.Fragment;

/**
 * Created by Union Assurance PLC on 3/16/17.
 */

public class BaseFragment extends Fragment {
    @Override
    public void onResume() {
        super.onResume();
        if (!ConnectionDetector.internetCheck(getActivity())) {
            return;
        }
    }
}
