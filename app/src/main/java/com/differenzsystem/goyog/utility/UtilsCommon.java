package com.differenzsystem.goyog.utility;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.ChartDataModel;
import com.differenzsystem.goyog.userProfile.InputFilterMinMax;
import com.veryfit.multi.nativedatabase.Userinfos;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Union Assurance PLC on 7/5/16.
 */
public class UtilsCommon {

    // set suffixes string
    static String[] suffixes =
            // 0     1     2     3     4     5     6     7     8     9
            {"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                    // 10    11    12    13    14    15    16    17    18    19
                    "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
                    // 20    21    22    23    24    25    26    27    28    29
                    "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                    // 30    31
                    "th", "st"};

    // get suffix for day

    /**
     * @param day get day no
     * @return suffix string of day
     */
    public static String getDaySuffix(int day) {
        //return "<sup>" + suffixes[day] + "</sup>";
        return suffixes[day];
    }

    /**
     * @param val get the value
     * @return interval value
     */
    public static int getInterval(float val) {
        if (val > 0 && val <= 10)
            return 2;
        else if (val > 10 && val <= 50)
            return 5;
        else if (val > 50 && val <= 100)
            return 10;
        else if (val > 100 && val <= 500)
            return 50;
        else
            return 100;
    }

    public static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "<sup>th</sup>";
        }
        switch (day % 10) {
            case 1:
                return "<sup>st</sup>";
            case 2:
                return "<sup>nd</sup>";
            case 3:
                return "<sup>rd</sup>";
            default:
                return "<sup>th</sup>";
        }
    }

    public static void Logout(Context mContext, String PREFERENCE_NAME) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }

    public static String[] dayArray = {"00:00-01:00", "01:00-02:00", "02:00-03:00", "03:00-04:00", "04:00-05:00",
            "05:00-06:00", "06:00-07:00", "07:00-08:00", "08:00-09:00", "09:00-10:00", "10:00-11:00",
            "11:00-12:00", "12:00-13:00", "13:00-14:00", "14:00-15:00", "15:00-16:00", "16:00-17:00",
            "17:00-18:00", "18:00-19:00", "19:00-20:00", "20:00-21:00", "21:00-22:00", "22:00-23:00", "23:00-23:59"};
    public static String[] MonthArray = {"1-7", "8-14", "15-21", "22-28", "29"};

    static ProgressDialog mProgress;

    // initialize and show progress dialog
    public static ProgressDialog showProgressDialog(Context context) {
//        mProgress = new ProgressDialog(context, R.style.dialogTheme);
//        mProgress.setCancelable(false);
//        mProgress.show();
//        return mProgress;
        if (mProgress == null) {
            mProgress = new ProgressDialog(context, R.style.dialogTheme);
            mProgress.setCancelable(false);
            mProgress.show();
        } else {
            mProgress.dismiss();
            mProgress = null;
            mProgress = new ProgressDialog(context, R.style.dialogTheme);
            mProgress.setCancelable(false);
            mProgress.show();

        }
        return mProgress;
    }

    // hide progressbar
    public static void destroyProgressBar() {
        try {
            if (mProgress != null)
                mProgress.dismiss();
        } catch (Exception e) {
        }
    }

    // set font

    /**
     * @param mContext get context of particular screen
     * @param fontType get the font type
     * @return
     */
    public static Typeface setFont(Context mContext, int fontType) {
        return Typeface.createFromAsset(mContext.getAssets(), mContext.getResources().getString(fontType));
    }

    // get current date in particular format
    public static String getCurrentdate() {
        Date date = new Date();
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00");
        String endDate = dfm.format(date);
        return endDate;
    }

    // get next date in particular format
    public static String getNextDaydate() {
        Date date = new Date();
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date newDate = calendar.getTime();
        return dfm.format(newDate);
    }

    // get next day of monday date in particular format
    public static String AddDayInDateOfMonday(int i) {
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00");
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        c.add(Calendar.DAY_OF_YEAR, i);
        Date newDate = c.getTime();
        return dfm.format(newDate);
    }

    // get date and hour in particular format
    public static String getDateByHour(int hour) {
        Date date = new Date();
        DateFormat dfm;
        if (hour == 24)
            dfm = new SimpleDateFormat("yyyy-MM-dd'T'23:59:00+00:00");
            //dfm = new SimpleDateFormat("yyyy-MM-" + 21 + "'T'23:59:00+00:00");

        else
            dfm = new SimpleDateFormat("yyyy-MM-dd'T'" + hour + ":00:00+00:00");
        // dfm = new SimpleDateFormat("yyyy-MM-" + 21 + "'T'" + hour + ":00:00+00:00");
        return dfm.format(date);
    }

    public static String getCurrentDateOnly() {
        Date date = new Date();
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00");
        String endDate = dfm.format(date);
        return endDate;
    }

    // get date in particular format
    public static String AddSevenDayInDate(int i) {
        DateFormat dfm;
        if (i > 30) {
            dfm = new SimpleDateFormat("yyyy-MM-" + 30 + "'T'00:00:00+00:00");
        } else {
            dfm = new SimpleDateFormat("yyyy-MM-" + i + "'T'00:00:00+00:00");
        }
        Calendar c = Calendar.getInstance();
        Date newDate = c.getTime();
        return dfm.format(newDate);
    }

 /*   public static String AddSevenDay(int i) {
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.add(Calendar.DAY_OF_YEAR, i);
        Date newDate = c.getTime();
        return dfm.format(newDate);
    }*/

    // convert string date into date week format

    /**
     * @param startdate get the date in string
     * @return
     */
    public static String ConvertStringIntoDateWeekName(String startdate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00");
        String datetime = "1 january";
        try {
            Date date = format.parse(startdate);
            SimpleDateFormat dateformat = new SimpleDateFormat("dd MMMM");

            datetime = dateformat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return datetime;

    }

    // get the day name from date

    /**
     * @param startdate get the date in string
     * @return
     */
    public static String getDateDayName(String startdate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00+00:00");
        String datetime = "1 Monday";
        try {
            Date date = format.parse(startdate);
            SimpleDateFormat dateformat = new SimpleDateFormat("dd EEEE");
            datetime = dateformat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Debugger.debugE("Date...", datetime);
        return datetime;

    }

    public static String ConvertStringIntoDate(String startdate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd' '00:00:00");
        String datetime = "1 january";
        try {
            Date date = format.parse(startdate);
            SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

            datetime = dateformat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return datetime;
    }

    // convert string date into dd/MM/yyyy format

    /**
     * @param startdate get the date in string
     * @return
     */
    public static String ConvertStringIntoDateSetting(String startdate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String datetime = "1 january";
        try {
            Date date = format.parse(startdate);
            SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

            datetime = dateformat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return datetime;

    }

    /*public static float getMiles(float i) {
        double data = i * 0.000621371192;
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        return Float.valueOf(decimalFormat.format(data));
    }*/

    /**
     * @param i distance value
     * @return return in km
     */
    public static float getMiles(float i) {
        // returns in km
        double data = i / 1000;
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        return Float.valueOf(decimalFormat.format(data));
    }


    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewPager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        //a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        a.setDuration(200);
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        //a.setDuration(SPEED_ANIMATION_TRANSITION);
        //a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        a.setDuration(200);
        v.startAnimation(a);
    }

    // check system bluetooth is ON or OFF
    public static Boolean checkBlutoothConnectivity() {
        Boolean isDisable = true;
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            isDisable = false;
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                isDisable = true;
            } else {
                isDisable = false;
            }
        }
        return isDisable;
    }

    public static void setHealthDataGlobal(int steps, int cals, int dis, int hearts) {
        Globals.homeDeviceMap.put(Constant.calories_burned, cals);
        Globals.homeDeviceMap.put(Constant.step, steps);
        Globals.homeDeviceMap.put(Constant.distance, dis);
        Globals.homeDeviceMap.put(Constant.heart_rate, hearts);
    }

    // show dialof for entering number of childrens
    public static void openEditDialog(final Context context) {
        final Dialog alertDialog = new Dialog(context);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.setting_edit_name_dialog);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        //alertDialog.setCancelable(false);

        final EditText et_childcount = (EditText) alertDialog.findViewById(R.id.et_name);
        TextView tv_cancel = (TextView) alertDialog.findViewById(R.id.tv_cancel);
        TextView tv_change = (TextView) alertDialog.findViewById(R.id.tv_change);
        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        View vi_devider = (View) alertDialog.findViewById(R.id.vi_devider);

        et_childcount.setHint("Enter Number Of Children");
        et_childcount.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        et_childcount.setFilters(new InputFilter[]{new InputFilterMinMax("0", "12")});

        tv_popup_title.setText("Got kids?");

        tv_cancel.setVisibility(View.GONE);
        vi_devider.setVisibility(View.GONE);
        tv_change.setText(R.string.okay);

        et_childcount.setTypeface(setFont(context, R.string.app_regular));
        tv_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_childcount.getText().toString().length() != 0) {
                    UtilsPreferences.setString(context, Constant.no_of_children, et_childcount.getText().toString().trim());
                    alertDialog.dismiss();
                } else {
                    et_childcount.setError(context.getString(R.string.enter_value_lbl));
                }

            }
        });

        alertDialog.show();
    }

    /**
     * @param sDate get the date in string
     * @return return the difference in minute from current date till request date
     */
    public static int setupDuration(String sDate) {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date startDate = null;
        Date curDate = null;
        try {
            startDate = formater.parse(sDate);
            curDate = formater.parse(formater.format(c.getTime()));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diffInMs = ((curDate.getTime() - startDate.getTime()));
        int min = (int) (TimeUnit.MILLISECONDS.toMinutes(diffInMs));
        //int min = (int) (TimeUnit.MILLISECONDS.toMinutes(diffInMs) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diffInMs)));
        Log.e("diffInMs", String.valueOf(diffInMs));

        return min;
    }

    /**
     * @param min  get the minute
     * @param type type = 1  for percentage time * type = 2 is remaining time
     * @return the time ago format
     */
    public static String convertMinuts(int min, int type) {
        int weeks = (int) (TimeUnit.MINUTES.toDays(min) / 7);
        int days = (int) (TimeUnit.MINUTES.toDays(min) - 7 * weeks);
        long hours = TimeUnit.MINUTES.toHours(min) - TimeUnit.DAYS.toHours(days) - TimeUnit.DAYS.toHours(7 * weeks);
        int minuts = min % 60;
        String timeAgo = "";

        //type = 1  for percentage time * type = 2 is remaining time
        if (type == 1) {
            if (min >= 0) {
                timeAgo = minuts + " m";
            }
            if (hours > 0) {
                timeAgo = hours + " h " + minuts + " m";
            }
            if (days > 0) {
                timeAgo = days + " d " + hours + " h";
            }
            if (weeks > 0) {
                timeAgo = weeks + " w " + days + " d";
            }
        } else {
            if (min >= 0) {
                if (min > 1) {
                    timeAgo = minuts + " Minutes to go";
                } else {
                    timeAgo = minuts + " Minute to go";
                }
            }
            if (hours > 0) {
                if (hours > 1) {
                    timeAgo = hours + " Hrs " + minuts + " Min to go";
                } else {
                    timeAgo = hours + " Hr " + minuts + " Min to go";
                }
            }
            if (days > 0) {
                if (days > 1) {
                    timeAgo = days + " Days " + hours + " Hrs to go";
                } else {
                    timeAgo = days + " Day " + hours + " Hrs to go";
                }
            }
            if (weeks > 0) {
                if (weeks > 1) {
                    timeAgo = weeks + " Weeks " + days + " Days to go";
                } else {
                    timeAgo = weeks + " Week " + days + " Days to go";
                }
            }
        }
        return timeAgo;
    }


    // method for close the keyboard popup
    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken()
                    , InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            Debugger.debugE("Hide_Keyboard", "Exception : Try to close keyboard that is already not visible");
        }
    }

    /*public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }*/

    // method to open browser
    public static void openBrowser(String address, Context context) {
        String url = address;//"http://www.goyo.lk/"

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }

    // convert feet to cm
    public static String ftToCentimeter(String feet) {
        double dCentimeter = 0d;
        if (!TextUtils.isEmpty(feet)) {
            if (feet.contains("-")) {
                String[] str_arr = feet.split("-");
                String tempfeet = str_arr[0];
                String tempinch = str_arr[1];
                if (!TextUtils.isEmpty(tempfeet)) {
                    dCentimeter += ((Double.valueOf(tempfeet)) * 30.48);
                }
                if (!TextUtils.isEmpty(tempinch)) {
                    dCentimeter += ((Double.valueOf(tempinch)) * 2.54);
                }
            }
        }
        return String.valueOf((int) dCentimeter);
        //Format to decimal digit as per your requirement
    }

    public static void addSDkFunctionOrder(Context context) {

        if (UtilsPreferences.getString(context, Constant.user_type).equalsIgnoreCase("1")) {

            String str_name = "";
            String str_cm = "";
            String str_lbs = "";
            String str_gender = "1"; // in every case we are set this value to 1 as per client discussion
            String str_year = "";
            String str_month = "";
            String str_day = "";

            try {
                str_name = UtilsPreferences.getString(context, Constant.first_name);

                if (UtilsPreferences.getString(context, Constant.height_scale).equalsIgnoreCase("1")) {
                    if (UtilsPreferences.getString(context, Constant.height) != null && UtilsPreferences.getString(context, Constant.height).contains("-")) {
                        String[] str_arr = UtilsPreferences.getString(context, Constant.height).split("-");
                        String str_ft_in = str_arr[0] + "-" + str_arr[1];
                        str_cm = ftToCentimeter(str_ft_in);
                    }


                } else if (UtilsPreferences.getString(context, Constant.height_scale).equalsIgnoreCase("2")) {
                    if (UtilsPreferences.getString(context, Constant.height) != null) {
                        str_cm = UtilsPreferences.getString(context, Constant.height);
                    }

                }

                if (UtilsPreferences.getString(context, Constant.weight_scale).equalsIgnoreCase("1")) {
                    String str_kg = UtilsPreferences.getString(context, Constant.weight);
                    int kg = (int) Double.parseDouble(str_kg);
                    int lbs = (int) (Math.round((kg / .45359237) * 10) / 10.0);
                    str_lbs = String.valueOf(lbs);

                } else if (UtilsPreferences.getString(context, Constant.weight_scale).equalsIgnoreCase("2")) {
                    String str_lb = UtilsPreferences.getString(context, Constant.weight);
                    int lb = (int) Double.parseDouble(str_lb);
                    str_lbs = String.valueOf(lb);
                }


                String str_date = UtilsPreferences.getString(context, Constant.dob);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                Date myDate = null;
                try {
                    myDate = sdf.parse(str_date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.US);
                SimpleDateFormat monthFormat = new SimpleDateFormat("MM", Locale.US);
                SimpleDateFormat dayFormat = new SimpleDateFormat("dd", Locale.US);
                str_year = yearFormat.format(myDate);
                str_month = monthFormat.format(myDate);
                str_day = dayFormat.format(myDate);

                Log.e("str_name", "" + str_name);
                Log.e("str_cm", "" + str_cm);
                Log.e("str_lbs", "" + str_lbs);
                Log.e("str_year", "" + str_year);
                Log.e("str_month", "" + str_month);
                Log.e("str_day", "" + str_day);
                Log.e("str_gender", "" + str_gender);

                Userinfos userinfos = new Userinfos();
                userinfos.setUserName(str_name);
                userinfos.setHeight(stringToInt(str_cm));
                userinfos.setWeight(stringToInt(str_lbs));
                userinfos.setGender(stringToInt(str_gender));
                userinfos.setYear(stringToInt(str_year));
                userinfos.setMonth(stringToInt(str_month));
                userinfos.setDay(stringToInt(str_day));

                if (ProtocolUtils.getInstance() != null)
                    ProtocolUtils.getInstance().setUserinfo(userinfos);

            } catch (Exception e) {
                return;
            }


        }

    }

    // convert string to int
    public static int stringToInt(String val) {
        try {
            return Integer.parseInt(val);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * @param context get the context of particular screen
     * @param time    get the time in string
     * @return return the particular time stamp for eg today, yesterday or particular date format
     */
    public static String timeStampClass(Context context, String time) {
        if (time.equalsIgnoreCase("")) {
            return "";
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat getformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String dataSot = formatter.format(new Date());
        Log.e("", "TimeStampClass: " + dataSot);


        String agoformater = "";

        Date CurrentDate = null;
        Date CreateDate = null;

        try {
            Date getFetchDate = getformatter.parse(time);
            agoformater = formatter.format(getFetchDate);

            CurrentDate = formatter.parse(dataSot);
            CreateDate = formatter.parse(agoformater);

            long different = Math.abs(CurrentDate.getTime() - CreateDate.getTime());

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;

            if (elapsedDays == 0) {
                return context.getString(R.string.today_lbl);

            } else if (elapsedDays == 1) {
                return context.getString(R.string.yesterday_lbl);
            } else {

                SimpleDateFormat formatter_result = new SimpleDateFormat("dd/MM/yy", Locale.US);
                Date FinalResultDate = formatter.parse(agoformater);
                return formatter_result.format(FinalResultDate);
            }

        } catch (java.text.ParseException e) {
            e.printStackTrace();

        }
        return "";
    }

    /**
     * @param AcceptDate get the string accept challenge date
     * @return return the difference in minute from request date to current date
     */
    public static String getTotalLevelChallengeMins(String AcceptDate) {
        String cDate;
        Calendar c = Calendar.getInstance();
        Date currentDate = c.getTime();

        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date Date1 = null;

        try {
            Date1 = formater.parse(AcceptDate);
            cDate = formater.format(currentDate);
            Date Date2 = formater.parse(cDate);

            long diffInTime = (long) ((Date2.getTime() - Date1.getTime()) / (1000 * 60));
            return String.valueOf(diffInTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return "0";
        }
    }

    // get the percentage value of level progress
    public static float getPercentageValue(int position) {

        ChartDataModel chartDataModel = new ChartDataModel();
        chartDataModel = Globals.inPChallengesChartDataMode.get(position);

        int leveltype = chartDataModel.getChallengetype();
        int leveltotalvalue = chartDataModel.getTotalval();
        int secondLeveltotalvalue = chartDataModel.getSectotalval();
        int thirdLeveltotalvalue = chartDataModel.getThirdtotalval();
        int fourthLeveltotalvalue = chartDataModel.getFourthtotalval();

        float levelPer = 0, secondLevelPer = 0, thirdLevelPer = 0, fourthLevelPer = 0;

        int levelvalue = chartDataModel.getVal() == 0 ? 1 : chartDataModel.getVal();
        int secondLevelValue = chartDataModel.getSecval() == 0 ? 1 : chartDataModel.getSecval();
        int thirdLevelValue = chartDataModel.getThirdval() == 0 ? 1 : chartDataModel.getThirdval();
        int fourthLevelValue = chartDataModel.getFourthval() == 0 ? 1 : chartDataModel.getFourthval();

        if (levelvalue <= leveltotalvalue) {
            levelPer = (float) (levelvalue * 100) / leveltotalvalue;
        } else if (levelvalue > leveltotalvalue) {
            levelPer = 100;
        }

        if (leveltype == 5 || leveltype == 6 || leveltype == 7 || leveltype == 8 || leveltype == 9 || leveltype == 10) {

            if (secondLevelValue <= secondLeveltotalvalue) {
                secondLevelPer = (float) (secondLevelValue * 100) / secondLeveltotalvalue;
                levelPer /= 2;
                secondLevelPer /= 2;
            } else if (secondLevelValue > secondLeveltotalvalue) {
                levelPer /= 2;
                secondLevelPer = 50;
            }
        }

        if (leveltype == 11 || leveltype == 12 || leveltype == 13 || leveltype == 14) {

            if (secondLevelValue <= secondLeveltotalvalue) {
                secondLevelPer = (float) (secondLevelValue * 100) / secondLeveltotalvalue;
                secondLevelPer = secondLevelPer / 3;
            } else if (secondLevelValue > secondLeveltotalvalue) {
                secondLevelPer = 33;
            }

            if (thirdLevelValue <= thirdLeveltotalvalue) {
                thirdLevelPer = (float) (thirdLevelValue * 100) / thirdLeveltotalvalue;
                thirdLevelPer = thirdLevelPer / 3;
            } else if (thirdLevelValue > thirdLeveltotalvalue) {
                thirdLevelPer = 33;
            }

            if (levelPer >= 100)
                levelPer = 34;
            else
                levelPer /= 3;

        }

        if (leveltype == 15) {

            if (secondLevelValue <= secondLeveltotalvalue) {
                secondLevelPer = (float) (secondLevelValue * 100) / secondLeveltotalvalue;
                secondLevelPer = secondLevelPer / 4;
            } else if (secondLevelValue > secondLeveltotalvalue) {
                secondLevelPer = 25;
            }

            if (thirdLevelValue <= thirdLeveltotalvalue) {
                thirdLevelPer = (float) (thirdLevelValue * 100) / thirdLeveltotalvalue;
                thirdLevelPer = thirdLevelPer / 4;
            } else if (thirdLevelValue > thirdLeveltotalvalue) {
                thirdLevelPer = 25;
            }

            if (fourthLevelValue <= fourthLeveltotalvalue) {
                fourthLevelPer = (float) (fourthLevelValue * 100) / fourthLeveltotalvalue;
                fourthLevelPer /= 4;
            } else if (fourthLevelValue > fourthLeveltotalvalue) {
                fourthLevelPer = 25;
            }

            levelPer /= 4;
        }
        return levelPer + secondLevelPer + thirdLevelPer + fourthLevelPer;
    }

    /**
     * @param date2 get the date string
     * @return return true or false
     */
    public static boolean CompareDatesString(String date2) {
        String date = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat reqformater = new SimpleDateFormat("dd/mm/yyyy", Locale.US);
        String curr_date = reqformater.format(c.getTime());
        Date curr_d = new Date();
        Date d2 = new Date();

        try {
            curr_d = reqformater.parse(curr_date);
            d2 = reqformater.parse(date2);
            if (curr_d.after(d2)) {
                // return true if request date is after current date
                return true;
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }

    /**
     * @return return the hour format in string
     */
    public static String getCurrentDateMonth() {
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat reqformater = new SimpleDateFormat("HH:mm", Locale.US);
            return reqformater.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}