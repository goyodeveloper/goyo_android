package com.differenzsystem.goyog.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;

import com.differenzsystem.goyog.FitnessBandTracker.ScanDeviceActivity;
import com.differenzsystem.goyog.R;


public class ConnectionDetector {
    static AlertDialog internetBuilder, gpsBuilder;

    // open dialog for intent to system setting screen
    public static void LaunchInternetSetting(Context mContext) {
        try {
            Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
            mContext.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // open dialog for intent to system setting screen to enable GPS
    public static void LaunchGpsSetting(Context mContext) {
        try {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            mContext.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // check condition for internet connection
    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    // return true if it is connected
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            // return true if it is connected
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean internetCheck(Context context) {
        if (isDialogInternetOpen()) {
            internetBuilder.dismiss();
        }
        if (isConnectingToInternet(context)) {
            return true;
        } else {
            showAlertDialog(context);
            return false;
        }

    }

    public static Boolean isDialogInternetOpen() {
        return internetBuilder != null && internetBuilder.isShowing();
    }

    public static Boolean isDialogGpsOpen() {
        return gpsBuilder != null && gpsBuilder.isShowing();
    }

    // show no internet dialog
    private static void showAlertDialog(final Context context) {
        if (isDialogInternetOpen()) {
            return;
        }
        internetBuilder = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(R.string.msg_NO_INTERNET_TITLE)
                .setCancelable(false)
                .setMessage(R.string.msg_NO_INTERNET_MSG)
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        LaunchInternetSetting(context);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        if (context instanceof ScanDeviceActivity) {
                            ConnectionDetector.isGpsEnable(context);
                        }
                    }
                }).show();
    }

    // check condition for GPS
    public static boolean isGpsEnable(Context context) {
        if (isDialogGpsOpen()) {
            gpsBuilder.dismiss();
        }
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps(context);
            return false;
        }
        return true;
    }

    // show no GPS dialog
    private static void buildAlertMessageNoGps(final Context context) {
        if (isDialogGpsOpen()) {
            return;
        }
        gpsBuilder = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(R.string.msg_NO_GPS_TITLE)
                .setCancelable(false)
                .setMessage(R.string.msg_NO_GPS_MSG)
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        LaunchGpsSetting(context);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }

   /* public static void showConnectivSnackBar(Context context, View view, boolean isConnectivityPresent) {
        Snackbar snackbar = Snackbar
                .make(view, context.getString(isConnectivityPresent ? R.string.sbar_msg_connectivity_present : R.string.sbar_msg_no_connectivity), Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        if (!isConnectivityPresent) {
            sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.red_no_internet));
        } else {
            sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.green_no_internet));
        }

        // Changing action button text color
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(1);
        snackbar.show();
    }*/
}
