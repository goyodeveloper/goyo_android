package com.differenzsystem.goyog.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.model.LevelInformationModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 7/5/16.
 */
public class UtilsPreferences {

    // ********** clear prefrences **********//
    public static void clearPreferences(Context mContext) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();
    }

    // ********** set string prefrences **********//

    /**
     * @param mContext get context of particular screen
     * @param key      get unique key name
     * @param value    the string value to store in shared prefrences
     * @return return true after commit
     */
    public static boolean setString(Context mContext, String key, String value) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    // ********** get string prefrences **********//

    /**
     * @param mContext get context of particular screen
     * @param key      unique key name
     * @return the string value
     */
    public static String getString(Context mContext, String key) {
        return getString(mContext, key, null);
    }

    // ********** get string prefrences **********//

    /**
     * @param mContext     get context of particular screen
     * @param key          unique key name
     * @param defaultValue default string if key value is empty
     * @return the string value
     */
    public static String getString(Context mContext, String key, String defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getString(key, defaultValue);
    }

    // ********** Int **********//

    /**
     * @param mContext get context of particular screen
     * @param key      unique key name
     * @param value    default int if key value is empty
     * @return true after commit
     */
    public static boolean setInt(Context mContext, String key, int value) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    /**
     * @param mContext get context of particular screen
     * @param key      unique key name
     * @return return int value of key
     */
    public static int getInt(Context mContext, String key) {
        return getInt(mContext, key, -1);
    }

    /**
     * @param mContext     get context of particular screen
     * @param key          unique key name
     * @param defaultValue default int if key value is empty
     * @return return int value of key
     */
    public static int getInt(Context mContext, String key, int defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getInt(key, defaultValue);
    }

    // ********** Long **********//

    public static boolean setLong(Context mContext, String key, long value) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static long getLong(Context mContext, String key) {
        return getLong(mContext, key, -1);
    }

    public static long getLong(Context mContext, String key, long defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getLong(key, defaultValue);
    }

    // ********** Float **********//

    public static boolean setFloat(Context mContext, String key, float value) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(key, value);
        return editor.commit();
    }

    public static float getFloat(Context mContext, String key) {
        return getFloat(mContext, key, -1);
    }

    /**
     * @param mContext     get context of particular screen
     * @param key          unique key name
     * @param defaultValue default float if key value is empty
     * @return return float value of key
     */
    public static float getFloat(Context mContext, String key, float defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getFloat(key, defaultValue);
    }

    // ********** Boolean **********//

    /**
     * @param mContext get context of particular screen
     * @param key      unique key name
     * @param value    default boolean if key value is empty
     * @return return boolean value of key
     */
    public static boolean setBoolean(Context mContext, String key, boolean value) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    /**
     * @param mContext get context of particular screen
     * @param key      unique key name
     * @return return boolean value of key
     */
    public static boolean getBoolean(Context mContext, String key) {
        return getBoolean(mContext, key, false);
    }

    /**
     * @param mContext     get context of particular screen
     * @param key          unique key name
     * @param defaultValue default boolean if key value is empty
     * @return return boolean value of key
     */
    public static boolean getBoolean(Context mContext, String key, boolean defaultValue) {
        SharedPreferences settings = mContext.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getBoolean(key, defaultValue);
    }

    /**
     * @param context       get context of particular screen
     * @param key           unique key name
     * @param inProcessList ChallengesModel
     */
    public static void saveInProgressChallengesSharedPrefrences(Context context, String key, ArrayList<ChallengesModel> inProcessList) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditorUserList;
        sharedPreferencesEditorUserList = sharedPreferences.edit();

        //Retrieve the values
        Gson gson = new Gson();

        String jsonFinalText = gson.toJson(inProcessList);
        sharedPreferencesEditorUserList.putString(key, jsonFinalText);
        sharedPreferencesEditorUserList.apply();
    }

    /**
     * @param context get context of particular screen
     * @param key     unique key name
     * @return ChallengesModel from key
     */
    public static ArrayList<ChallengesModel> getInProgressChallengesSharedPrefrences(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        //Retrieve the values
        Gson gson = new Gson();
        String jsonText = sharedPreferences.getString(key, null);
        //Set the values

        ArrayList<ChallengesModel> inProgressList = (ArrayList<ChallengesModel>) gson.fromJson(jsonText,
                new TypeToken<ArrayList<ChallengesModel>>() {
                }.getType());

        return inProgressList;
    }

    /**
     * @param context    get context of particular screen
     * @param key        unique key name
     * @param level_info LevelInformationModel from key
     */
    public static void saveInLevelInformationSharedPrefrences(Context context, String key, LevelInformationModel level_info) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditorUserList;
        sharedPreferencesEditorUserList = sharedPreferences.edit();

        //Retrieve the values
        Gson gson = new Gson();

        String jsonFinalText = gson.toJson(level_info);
        sharedPreferencesEditorUserList.putString(key, jsonFinalText);
        sharedPreferencesEditorUserList.apply();
    }

    /**
     * @param context get context of particular screen
     * @param key     unique key name
     * @return LevelInformationModel
     */
    public static LevelInformationModel getLevelInformationSharedPrefrences(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        //Retrieve the values
        Gson gson = new Gson();
        String jsonText = sharedPreferences.getString(key, null);
        //Set the values

        LevelInformationModel level_info = gson.fromJson(jsonText, LevelInformationModel.class);

        return level_info;
    }

    // clear shared prefrences

    /**
     * @param context get context of particular screen
     * @param key     unique key name
     */
    public static void clearKeyPreferences(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditorUserList;
        sharedPreferencesEditorUserList = sharedPreferences.edit();
        sharedPreferencesEditorUserList.clear().apply();
    }
}
