package com.differenzsystem.goyog.utility;

import com.differenzsystem.goyog.model.ChartDataModel;
import com.differenzsystem.goyog.model.LeaderboardModel;
import com.differenzsystem.goyog.model.completedchallbadges;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created on 29/9/16.
 */
public class Globals {
    public static HashMap<String, Object> homeDeviceMap = new HashMap<>();
    public static HashMap<String, Object> CaloriesDayMap = new HashMap<>();
    public static HashMap<String, Object> CaloriesWeekMap = new HashMap<>();
    public static HashMap<String, Object> CaloriesMonthMap = new HashMap<>();

    public static HashMap<String, Object> StepDayMap = new HashMap<>();
    public static HashMap<String, Object> StepWeekMap = new HashMap<>();
    public static HashMap<String, Object> StepMonthMap = new HashMap<>();

    public static HashMap<String, Object> DisDayMap = new HashMap<>();
    public static HashMap<String, Object> DisWeekMap = new HashMap<>();
    public static HashMap<String, Object> DisMonthMap = new HashMap<>();

    public static LeaderboardModel leaderboardModel;
    public static completedchallbadges completedchallbadges;

    //inprocess challenges to show in pager
    public static HashMap<String, Object> inProgressChallengesmap = new HashMap<>();
    public static ArrayList<ChartDataModel> inPChallengesChartDataMode = new ArrayList<>();

    public static void clearData() {
        homeDeviceMap.clear();
        inPChallengesChartDataMode.clear();
        completedchallbadges = null;
    }

    public static JSONObject Intial_reward=new JSONObject();

}
