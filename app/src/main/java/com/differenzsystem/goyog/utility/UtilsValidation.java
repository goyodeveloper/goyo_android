package com.differenzsystem.goyog.utility;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilsValidation {

    // ********** Validation ********** //

    /*public final static Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9+._%-+]{1,256}" + "@"
            + "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" + "(" + "." + "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" + ")+");*/

    /**
     *
     * @param context     get context of particular screen
     * @param et_email    get the email edittext
     * @param isShowError boolean for showing prrogress dialog
     * @return            true if email is valid
     */
    public static boolean isValidEmail(Context context, EditText et_email, boolean isShowError) {
        if (et_email.getText().toString().length() == 0) {
            if (isShowError)
                setErrorMsg(et_email, context.getString(R.string.err_msg_please_enter_email_address));
            //Toast.makeText(context, R.string.err_msg_please_enter_email_address, Toast.LENGTH_SHORT).show();
            return false;
        } else if (!et_email.getText().toString().trim().matches("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")) {
            if (isShowError)
                setErrorMsg(et_email, context.getString(R.string.err_msg_please_enter_valid_email_address));
            //Toast.makeText(context, R.string.err_msg_please_enter_valid_email_address, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            et_email.setError(null);
            return true;
        }
    }

    /**
     *
     * @param context     get context of particular screen
     * @param et_password    get the password edittext
     * @param isShowError boolean for showing progress dialog
     * @return            true if password is valid
     */
    public static boolean isValidPassword(Context context, EditText et_password, boolean isShowError, boolean reg) {
        if (et_password.getText().toString().length() == 0) {
            if (isShowError)
                setErrorMsg(et_password, context.getString(R.string.err_msg_please_enter_password));
            //Toast.makeText(context, R.string.err_msg_please_enter_password, Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isValidPassword(et_password.getText().toString())) {
            //!et_password.getText().toString().trim().matches(context.getString(R.string.password_valid))
            if (isShowError)
                setErrorMsg(et_password, context.getString(R.string.err_msg_password_should_be_8_16_char));
            //Toast.makeText(context, R.string.err_msg_password_should_be_8_16_char, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            et_password.setError(null);
            return true;
        }
    }

    /**
     *
     * @param password get password string
     * @return         true if password is valid
     */
    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;//"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{8,16}$"
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[@#$%^&+=!])(?=\\S+$).{8,16}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean isValidConfPassword(Context context, EditText et_password, EditText et_conf_password, boolean isShowError) {
        if (et_conf_password.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_conf_password, context.getString(R.string.err_msg_please_enter_conf_password));
            //Toast.makeText(context, R.string.err_msg_please_enter_conf_password, Toast.LENGTH_SHORT).show();
            return false;
        } else if (!et_conf_password.getText().toString().trim().equals(et_password.getText().toString().trim())) {
            if (isShowError)
                setErrorMsg(et_conf_password, context.getString(R.string.err_msg_conf_password_not_match));
            //Toast.makeText(context, R.string.err_msg_conf_password_not_match, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            et_conf_password.setError(null);
            return true;
        }
    }

    /**
     *
     * @param context     get context of particular screen
     * @param et_fname    edittext first name
     * @param isShowError boolean for showing progress dialog
     * @return            true if first name is valid
     */
    public static boolean isValidFname(Context context, EditText et_fname, boolean isShowError) {
        if (et_fname.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_fname, context.getString(R.string.err_msg_please_enter_fname));
            //Toast.makeText(context, R.string.err_msg_please_enter_fname, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_fname.getText().length() < 3) {
            if (isShowError)
                setErrorMsg(et_fname, context.getString(R.string.err_msg_fname_must_be_less_than_3_character));
            //Toast.makeText(context, R.string.err_msg_name_must_be_less_than_30_character, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_fname.getText().length() > 30) {
            if (isShowError)
                setErrorMsg(et_fname, context.getString(R.string.err_msg_fname_must_be_less_than_30_character));
            //Toast.makeText(context, R.string.err_msg_name_must_be_less_than_30_character, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            et_fname.setError(null);
            return true;
        }
    }

    /**
     *
     * @param context     get context of particular screen
     * @param et_name    edittext name
     * @param isShowError boolean for showing progress dialog
     * @return            true if name is valid
     */
    public static boolean isValidName(Context context, EditText et_name, boolean isShowError) {
        if (et_name.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_name, context.getString(R.string.err_msg_please_enter_name));
            //Toast.makeText(context, R.string.err_msg_please_enter_fname, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_name.getText().length() < 3) {
            if (isShowError)
                setErrorMsg(et_name, context.getString(R.string.err_msg_name_must_be_less_than_3_character));
            //Toast.makeText(context, R.string.err_msg_name_must_be_less_than_30_character, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_name.getText().length() > 30) {
            if (isShowError)
                setErrorMsg(et_name, context.getString(R.string.err_msg_name_must_be_less_than_30_character));
            //Toast.makeText(context, R.string.err_msg_name_must_be_less_than_30_character, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            et_name.setError(null);
            return true;
        }
    }

    /**
     *
     * @param context     get context of particular screen
     * @param et_lname    edittext last name
     * @param isShowError boolean for showing progress dialog
     * @return            true if last name is valid
     */
    public static boolean isValidLname(Context context, EditText et_lname, boolean isShowError) {
        if (et_lname.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_lname, context.getString(R.string.err_msg_please_enter_lname));
            //Toast.makeText(context, R.string.err_msg_please_enter_lname, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_lname.getText().length() < 3) {
            if (isShowError)
                setErrorMsg(et_lname, context.getString(R.string.err_msg_lname_must_be_less_than_3_character));
            //Toast.makeText(context, R.string.err_msg_name_must_be_less_than_30_character, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_lname.getText().length() > 30) {
            if (isShowError)
                setErrorMsg(et_lname, context.getString(R.string.err_msg_lname_must_be_less_than_30_character));
            //Toast.makeText(context, R.string.err_msg_name_must_be_less_than_30_character, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            et_lname.setError(null);
            return true;
        }
    }

    //change

    public static boolean isValidOthername(Context context, EditText et_other_name, boolean isShowError) {
        if (et_other_name.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_other_name, context.getString(R.string.err_msg_please_enter_fname));
            //Toast.makeText(context, R.string.err_msg_please_enter_fname, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_other_name.getText().length() < 3) {
            if (isShowError)
                setErrorMsg(et_other_name, context.getString(R.string.err_msg_name_must_be_less_than_3_character));
            //Toast.makeText(context, R.string.err_msg_name_must_be_less_than_30_character, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_other_name.getText().length() > 30) {
            if (isShowError)
                setErrorMsg(et_other_name, context.getString(R.string.err_msg_name_must_be_less_than_30_character));
            //Toast.makeText(context, R.string.err_msg_name_must_be_less_than_30_character, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            et_other_name.setError(null);
            return true;
        }
    }

    /**
     *
     * @param context     get context of particular screen
     * @param et_address  edittext address
     * @param isShowError boolean for showing progress dialog
     * @return            true if address is valid
     */
    public static boolean isValidAddress(Context context, EditText et_address, boolean isShowError) {
        if (et_address.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_address, context.getString(R.string.err_msg_please_enter_address));
            //Toast.makeText(context, R.string.err_msg_please_enter_fname, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            et_address.setError(null);
            return true;
        }
    }

    /**
     *
     * @param context           get context of particular screen
     * @param et_contact_number edittext contact number
     * @param isShowError       boolean for showing progress dialog
     * @return                  true if contact number is valid
     */
    public static boolean isValidContactNo(Context context, EditText et_contact_number, boolean isShowError) {
        if (et_contact_number.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_contact_number, context.getString(R.string.err_msg_please_enter_contactno));
            //Toast.makeText(context, R.string.err_msg_please_enter_fname, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_contact_number.getText().length() < 10) {
            if (isShowError)
                setErrorMsg(et_contact_number, context.getString(R.string.err_msg_contactno_should_be_10_digit));
            //Toast.makeText(context, R.string.err_msg_name_must_be_less_than_30_character, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            et_contact_number.setError(null);
            return true;
        }
    }

    /**
     *
     * @param context     get context of particular screen
     * @param et_nic      edittext nic
     * @param isShowError boolean for showing progress dialog
     * @return            true if nic is valid
     */
    public static boolean isValidNICNominee(Context context, EditText et_nic, boolean isShowError) {
        if (et_nic.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_nic, context.getString(R.string.err_msg_please_enter_nic));
            //Toast.makeText(context, R.string.err_msg_please_enter_fname, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_nic.getText().length() == 10) {
            if (!et_nic.getText().toString().trim().matches("\\d{9}[XV]")) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_nic_pattern));
                return false;
            }
            return true;
        } else if (et_nic.getText().length() == 12) {
            if (!et_nic.getText().toString().trim().matches("\\d{12}")) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_invalid_nic));
                return false;
            }
            return true;
        } else if (et_nic.getText().length() < 12 || et_nic.getText().length() > 12) {
            if (isShowError)
                setErrorMsg(et_nic, context.getString(R.string.err_msg_nicno_should_be_10_char));
            return false;
        } else {
            et_nic.setError(null);
            return true;
        }
    }

    /**
     *
     * @param context     get context of particular screen
     * @param et_nic      edittext nic
     * @param isShowError boolean for showing progress dialog
     * @return            true if nic is valid
     */
    public static boolean isValidNICMain(Context context, EditText et_nic, boolean isShowError) {

        String dob = UtilsPreferences.getString(context, Constant.dob);
        String[] split = dob.split("-");
        String year = split[0];

        if (et_nic.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_nic, context.getString(R.string.err_msg_please_enter_nic));
            //Toast.makeText(context, R.string.err_msg_please_enter_fname, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_nic.getText().length() == 10) {
            if (!et_nic.getText().toString().trim().matches("\\d{9}[XV]")) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_nic_pattern));
                return false;
            } else if (!et_nic.getText().toString().startsWith(year.substring(2))) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_invalid_nic));
                return false;
            }
            return true;
        } else if (et_nic.getText().length() == 12) {
            if (!et_nic.getText().toString().trim().matches("\\d{12}")) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_invalid_nic));
                return false;
            } else if (!et_nic.getText().toString().startsWith(year)) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_invalid_nic));
                return false;
            }
            return true;
        } else if (et_nic.getText().length() < 12 || et_nic.getText().length() > 12) {
            if (isShowError)
                setErrorMsg(et_nic, context.getString(R.string.err_msg_nicno_should_be_10_char));
            return false;
        } else {
            et_nic.setError(null);
            return true;
        }
    }

    /**
     *
     * @param context     get context of particular screen
     * @param et_nic      edittext nic
     * @param dob         string date of birth
     * @param isShowError boolean for showing progress dialog
     * @return            true if nic and dob is valid
     */
    public static boolean isValidNICMainAllSet(Context context, EditText et_nic, String dob, boolean isShowError) {

        String[] split = dob.split("-");
        String year = split[0];

        if (et_nic.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_nic, context.getString(R.string.err_msg_please_enter_nic));
            //Toast.makeText(context, R.string.err_msg_please_enter_fname, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_nic.getText().length() == 10) {
            if (!et_nic.getText().toString().trim().matches("\\d{9}[XV]")) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_nic_pattern));
                return false;
            } else if (!et_nic.getText().toString().startsWith(year.substring(2))) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_invalid_nic));
                return false;
            }
            return true;
        } else if (et_nic.getText().length() == 12) {
            if (!et_nic.getText().toString().trim().matches("\\d{12}")) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_invalid_nic));
                return false;
            } else if (!et_nic.getText().toString().startsWith(year)) {
                if (isShowError)//\d{8}[XV]
                    setErrorMsg(et_nic, context.getString(R.string.err_msg_invalid_nic));
                return false;
            }
            return true;
        } else if (et_nic.getText().length() < 12 || et_nic.getText().length() > 12) {
            if (isShowError)
                setErrorMsg(et_nic, context.getString(R.string.err_msg_nicno_should_be_10_char));
            return false;
        } else {
            et_nic.setError(null);
            return true;
        }
    }

    // isValidNICNominee
    public static boolean isValidNIC(Context context, EditText et_nic, boolean isShowError) {
        if (et_nic.getText().length() == 0) {
            if (isShowError)
                setErrorMsg(et_nic, context.getString(R.string.err_msg_please_enter_nic));
            //Toast.makeText(context, R.string.err_msg_please_enter_fname, Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_nic.getText().length() < 10) {
            if (isShowError)
                setErrorMsg(et_nic, context.getString(R.string.err_msg_nicno_should_be_10_char));
            return false;
        } else if (!et_nic.getText().toString().trim().matches("\\d{9}[XV]")) {
            if (isShowError)//\d{8}[XV]
                setErrorMsg(et_nic, context.getString(R.string.err_msg_nic_pattern));
            return false;
        } else {
            et_nic.setError(null);
            return true;
        }
    }

    /*public static boolean isValidEmailAddress(String emailAddress) {
        String expression = "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";
        CharSequence inputStr = emailAddress;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        return matcher.matches();
    }*/


    public static boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    public static boolean isValidUrl(String url) {
        if (url.contains("www.") || url.contains("http://")) {
            return Patterns.WEB_URL.matcher(url).matches();
        } else {
            return false;
        }
    }

    /**
     *
     * @param et    get edittext
     * @param error string error msg
     */
    public static void setErrorMsg(EditText et, String error) {
        String html_body = "<font color='red'>%s</font>";
        et.setError(Html.fromHtml(String.format(html_body, error)));
    }

    /**
     *
     * @param level_start_date date string
     * @return                 return true if request date is greater then current date
     */
    public static boolean isGreaterThanCurrentDate(String level_start_date) {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss");
        try {
            Date current_date = df.parse(df.format(c.getTime()));
            Date start_date = df.parse(level_start_date);

            if (current_date.compareTo(start_date) < 0) {
                // here startdate is greater than current date
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isLesserThanCurrentDate(String level_start_date) {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss");
        try {
            Date current_date = df.parse(df.format(c.getTime()));
            Date start_date = df.parse(level_start_date);

            if (current_date.compareTo(start_date) > 0) {
                // here startdate is greater than current date
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }
}