package com.differenzsystem.goyog.utility;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by mac on 11/21/17.
 */

public class WrapContentViewPager extends ViewPager {

    private int mCurrentPagePosition = 0;

    public WrapContentViewPager(Context context) {
        super(context);
    }

    public WrapContentViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        try {
            View child = getChildAt(mCurrentPagePosition);
            if (child != null) {
                child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
                int h = child.getMeasuredHeight();
                heightMeasureSpec = MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void reMeasureCurrentPage(int position) {
        mCurrentPagePosition = position;
        requestLayout();
    }
}

/**
 * public class MagicViewPager extends ViewPager {
 * <p>
 * public MagicViewPager(Context context, AttributeSet attrs) {
 * super(context, attrs);
 * }
 *
 * @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
 * // super has to be called in the beginning so the child views can be
 * // initialized.
 * super.onMeasure(widthMeasureSpec, heightMeasureSpec);
 * <p>
 * if (getChildCount() <= 0)
 * return;
 * <p>
 * // Check if the selected layout_height mode is set to wrap_content
 * // (represented by the AT_MOST constraint).
 * boolean wrapHeight = MeasureSpec.getMode(heightMeasureSpec)
 * == MeasureSpec.AT_MOST;
 * <p>
 * int width = getMeasuredWidth();
 * <p>
 * View firstChild = getChildAt(0);
 * <p>
 * // Initially set the height to that of the first child - the
 * // PagerTitleStrip (since we always know that it won't be 0).
 * int height = firstChild.getMeasuredHeight();
 * <p>
 * if (wrapHeight) {
 * <p>
 * // Keep the current measured width.
 * widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
 * <p>
 * }
 * <p>
 * int fragmentHeight = 0;
 * fragmentHeight = measureFragment(((Fragment) getAdapter().instantiateItem(this, getCurrentItem())).getView());
 * <p>
 * // Just add the height of the fragment:
 * heightMeasureSpec = MeasureSpec.makeMeasureSpec(height + fragmentHeight,
 * MeasureSpec.EXACTLY);
 * <p>
 * // super has to be called again so the new specs are treated as
 * // exact measurements.
 * super.onMeasure(widthMeasureSpec, heightMeasureSpec);
 * }
 * <p>
 * public int measureFragment(View view) {
 * if (view == null)
 * return 0;
 * <p>
 * view.measure(0, 0);
 * return view.getMeasuredHeight();
 * }}
 * <p>
 * Allows to redraw the view size to wrap the content of the bigger child.
 * @param widthMeasureSpec  with measured
 * @param heightMeasureSpec height measured
 * <p>
 * Allows to redraw the view size to wrap the content of the bigger child.
 * @param widthMeasureSpec  with measured
 * @param heightMeasureSpec height measured
 * <p>
 * Allows to redraw the view size to wrap the content of the bigger child.
 * @param widthMeasureSpec  with measured
 * @param heightMeasureSpec height measured
 * <p>
 * Allows to redraw the view size to wrap the content of the bigger child.
 * @param widthMeasureSpec  with measured
 * @param heightMeasureSpec height measured
 * <p>
 * Allows to redraw the view size to wrap the content of the bigger child.
 * @param widthMeasureSpec  with measured
 * @param heightMeasureSpec height measured
 * <p>
 * Allows to redraw the view size to wrap the content of the bigger child.
 * @param widthMeasureSpec  with measured
 * @param heightMeasureSpec height measured
 * <p>
 * Allows to redraw the view size to wrap the content of the bigger child.
 * @param widthMeasureSpec  with measured
 * @param heightMeasureSpec height measured
 */

/*public class WrapContentViewPager extends ViewPager {

    private static final String TAG = WrapContentViewPager.class.getSimpleName();
    private int height = 0;
    private int decorHeight = 0;
    private int widthMeasuredSpec;

    private boolean animateHeight;
    private int rightHeight;
    private int leftHeight;
    private int scrollingPosition = -1;

    public WrapContentViewPager(Context context) {
        super(context);
        init();
    }

    public WrapContentViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        addOnPageChangeListener(new OnPageChangeListener() {


            public int state;

            @Override
            public void onPageScrolled(int position, float offset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                if (state == SCROLL_STATE_IDLE) {
                    height = 0; // measure the selected page in-case it's a change without scrolling
                    Log.d(TAG, "onPageSelected:" + position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                this.state = state;
            }
        });
    }

    @Override
    public void setAdapter(PagerAdapter adapter) {
        if(!(adapter instanceof ObjectAtPositionInterface)) {
            throw new IllegalArgumentException("WrapContentViewPage requires that PagerAdapter will implement ObjectAtPositionInterface");
        }
        height = 0; // so we measure the new content in onMeasure
        super.setAdapter(adapter);
    }

    *//**
 * Allows to redraw the view size to wrap the content of the bigger child.
 *
 * @param widthMeasureSpec  with measured
 * @param heightMeasureSpec height measured
 *//*
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        widthMeasuredSpec = widthMeasureSpec;
        int mode = MeasureSpec.getMode(heightMeasureSpec);

        if (mode == MeasureSpec.UNSPECIFIED || mode == MeasureSpec.AT_MOST) {
            if(height == 0) {
                // measure vertical decor (i.e. PagerTitleStrip) based on ViewPager implementation
                decorHeight = 0;
                for (int i = 0; i < getChildCount(); i++) {
                    View child = getChildAt(i);
                    LayoutParams lp = (LayoutParams) child.getLayoutParams();
                    if(lp != null && lp.isDecor) {
                        int vgrav = lp.gravity & Gravity.VERTICAL_GRAVITY_MASK;
                        boolean consumeVertical = vgrav == Gravity.TOP || vgrav == Gravity.BOTTOM;
                        if(consumeVertical) {
                            decorHeight += child.getMeasuredHeight() ;
                        }
                    }
                }

                // make sure that we have an height (not sure if this is necessary because it seems that onPageScrolled is called right after
                int position = getCurrentItem();
                View child = getViewAtPosition(position);
                if (child != null) {
                    height = measureViewHeight(child);
                }
                Log.d(TAG, "onMeasure height:" + height + " decor:" + decorHeight);

            }
            int totalHeight = height + decorHeight + getPaddingBottom() + getPaddingTop();
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(totalHeight, MeasureSpec.EXACTLY);
            Log.d(TAG, "onMeasure total height:" + totalHeight);
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onPageScrolled(int position, float offset, int positionOffsetPixels) {
        super.onPageScrolled(position, offset, positionOffsetPixels);
        // cache scrolled view heights
        if (scrollingPosition != position) {
            scrollingPosition = position;
            // scrolled position is always the left scrolled page
            View leftView = getViewAtPosition(position);
            View rightView = getViewAtPosition(position + 1);
            if (leftView != null && rightView != null) {
                leftHeight = measureViewHeight(leftView);
                rightHeight = measureViewHeight(rightView);
                animateHeight = true;
                Log.d(TAG, "onPageScrolled heights left:" + leftHeight + " right:" + rightHeight);
            } else {
                animateHeight = false;
            }
        }
        if (animateHeight) {
            int newHeight = (int) (leftHeight * (1 - offset) + rightHeight * (offset));
            if (height != newHeight) {
                Log.d(TAG, "onPageScrolled height change:" + newHeight);
                height = newHeight;
                requestLayout();
                invalidate();
            }
        }
    }

    private int measureViewHeight(View view) {
        view.measure(getChildMeasureSpec(widthMeasuredSpec, getPaddingLeft() + getPaddingRight(), view.getLayoutParams().width), MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
        return view.getMeasuredHeight();
    }

    protected View getViewAtPosition(int position) {
        if(getAdapter() != null) {
            Object objectAtPosition = ((ObjectAtPositionInterface) getAdapter()).getObjectAtPosition(position);
            if (objectAtPosition != null) {
                for (int i = 0; i < getChildCount(); i++) {
                    View child = getChildAt(i);
                    if (child != null && getAdapter().isViewFromObject(child, objectAtPosition)) {
                        return child;
                    }
                }
            }
        }
        return null;
    }


}*/
