package com.differenzsystem.goyog.utility;

import android.Manifest;
import android.Manifest.permission;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by Union Assurance PLC on 21/6/16.
 */
public class UtilsPermission {

    public static final int FINELOCATION_REQUEST = 100;
    public static final int CAMERA_REQUEST = 200;
    public static final int WRITE_EXTERNAL_STORAGE_REQUEST = 300;
    public static final int BODY_SENSOR_REQUEST = 400;
    public static final int CONTACT_REQUEST = 500;

    /**
     *
     * @param context get context of particular screen
     * @return        return true if ACCESS_FINE_LOCATION permission is granted
     */
    public static boolean checkFineLocation(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINELOCATION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINELOCATION_REQUEST);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     *
     * @param context get the context of particular screen
     * @return        return true if BODY_SENSORS permission is granted
     */
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    public static boolean checkBodySensor(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, permission.BODY_SENSORS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission.BODY_SENSORS)) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{permission.BODY_SENSORS}, BODY_SENSOR_REQUEST);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{permission.BODY_SENSORS}, BODY_SENSOR_REQUEST);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     *
     * @param context get the context of particular screen
     * @return        return true if CAMERA permission is granted
     */
    public static boolean checkCamera(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     *
     * @param context get the context of particular screen
     * @return        return true if WRITE_EXTERNAL_STORAGE permission is granted
     */
    public static boolean checkWriteExternalStorage(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_REQUEST);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_REQUEST);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     *
     * @param context get the context of particular screen
     * @return        return true if READ_CONTACTS permission is granted
     */
    public static boolean checkContact(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission.READ_CONTACTS)) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{permission.READ_CONTACTS}, CONTACT_REQUEST);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{permission.READ_CONTACTS}, CONTACT_REQUEST);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
