package com.differenzsystem.goyog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.model.TimeLine;

import java.util.List;

/**
 * Created by Union Assurance PLC on 8/19/16.
 */

public class TimelineAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    List<TimeLine> timeLines;

    public TimelineAdapter(Context context, List<TimeLine> timeLines) {
        this.context = context;
        this.timeLines = timeLines;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return timeLines.size();
    }

    @Override
    public Object getItem(int position) {
        return timeLines.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.profile_timeline_list_item, null);
            holder = new ViewHolder();

            holder.iv_right_rect_image = (ImageView) view.findViewById(R.id.iv_right_rect_image);
            //holder.iv_left_circle_image = (CircleImageView) view.findViewById(R.id.iv_left_circle_image);
            holder.iv_left_circle_image = (ImageView) view.findViewById(R.id.iv_left_circle_image);
            holder.tv_left_month = (TextView) view.findViewById(R.id.tv_left_month);
            holder.tv_right_month = (TextView) view.findViewById(R.id.tv_right_month);
            holder.tv_right_level = (TextView) view.findViewById(R.id.tv_right_level);
            holder.tv_left_level = (TextView) view.findViewById(R.id.tv_left_level);
            holder.tv_right_skill = (TextView) view.findViewById(R.id.tv_right_skill);
            holder.tv_left_skill = (TextView) view.findViewById(R.id.tv_left_skill);
            holder.tv_month_title = (TextView) view.findViewById(R.id.tv_month_title);
            holder.main_left_layout = (RelativeLayout) view.findViewById(R.id.main_left_layout);
            holder.main_right_layout = (RelativeLayout) view.findViewById(R.id.main_right_layout);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        TimeLine temp = timeLines.get(position);

        holder.tv_month_title.setVisibility(View.VISIBLE);
        holder.tv_month_title.setText(temp.getMonth());
        if (position > 0) {
            if (temp.getMonth().equalsIgnoreCase(timeLines.get(position - 1).getMonth())) {
                holder.tv_month_title.setVisibility(View.GONE);
            } else {
                holder.tv_month_title.setVisibility(View.VISIBLE);
            }
        }

        if (position % 2 == 0) {
            if (temp.getType().equalsIgnoreCase("challange")) {
                holder.main_left_layout.setVisibility(View.GONE);
                holder.main_right_layout.setVisibility(View.VISIBLE);
                holder.tv_left_month.setText(temp.getMonthdate());
                holder.tv_left_skill.setText(temp.getOffer_title());
                holder.tv_left_level.setText(temp.getSubtitle());
                String url = context.getResources().getString(R.string.server_url)
                        + context.getResources().getString(R.string.challenge_images_url)
                        + temp.getThumb_image();
                Glide.with(context)
                        .load(url)
                        .placeholder(R.drawable.app_placeholder)
                        .dontAnimate()
                        .into(holder.iv_right_rect_image);
            } else {
                holder.main_left_layout.setVisibility(View.GONE);
                holder.main_right_layout.setVisibility(View.VISIBLE);
                holder.tv_left_month.setText(temp.getMonthdate());
                holder.tv_left_skill.setText(temp.getSubtitle());
                holder.tv_left_level.setText(temp.getTitle());
                String url = context.getResources().getString(R.string.server_url)
                        + context.getResources().getString(R.string.badges_image)
                        + temp.getThumb_image();
                Glide.with(context)
                        .load(url)
                        .placeholder(R.drawable.app_placeholder)
                        .dontAnimate()
                        .into(holder.iv_right_rect_image);
            }
        } else {
            if (temp.getType().equalsIgnoreCase("challange")) {
                holder.main_left_layout.setVisibility(View.VISIBLE);
                holder.main_right_layout.setVisibility(View.GONE);
                holder.tv_right_month.setText(temp.getMonthdate());
                holder.tv_right_skill.setText(temp.getSubtitle());
                holder.tv_right_level.setText(temp.getOffer_title());
                String url = context.getResources().getString(R.string.server_url)
                        + context.getResources().getString(R.string.challenge_images_url)
                        + temp.getThumb_image();
                Glide.with(context)
                        .load(url)
                        .placeholder(R.drawable.app_placeholder)
                        .dontAnimate()
                        .into(holder.iv_left_circle_image);
            } else {
                holder.main_left_layout.setVisibility(View.VISIBLE);
                holder.main_right_layout.setVisibility(View.GONE);
                holder.tv_right_month.setText(temp.getMonthdate());
                holder.tv_right_skill.setText(temp.getTitle());
                holder.tv_right_level.setText(temp.getSubtitle());
                String url = context.getResources().getString(R.string.server_url)
                        + context.getResources().getString(R.string.badges_image)
                        + temp.getThumb_image();
                Glide.with(context)
                        .load(url)
                        .placeholder(R.drawable.app_placeholder)
                        .dontAnimate()
                        .into(holder.iv_left_circle_image);
            }
        }

        /*if (temp.getType().equalsIgnoreCase("challange")) {
            holder.main_left_layout.setVisibility(View.GONE);
            holder.main_right_layout.setVisibility(View.VISIBLE);
            holder.tv_left_month.setText(temp.getMonthdate());
            holder.tv_left_skill.setText(temp.getOffer_title());
            holder.tv_left_level.setText(temp.getSubtitle());
            String url = context.getResources().getString(R.string.server_url)
                    + context.getResources().getString(R.string.challenge_images_url)
                    + temp.getThumb_image();
            Glide.with(context).load(url).placeholder(R.drawable.app_placeholder).into(holder.iv_right_rect_image);
        } else {
            holder.main_right_layout.setVisibility(View.GONE);
            holder.main_left_layout.setVisibility(View.VISIBLE);
            holder.tv_right_month.setText(temp.getMonthdate());
            holder.tv_right_skill.setText(temp.getTitle());
            holder.tv_right_level.setText(temp.getSubtitle());
            String url = Constant.badge_image_url + temp.getThumb_image();
            Glide.with(context).load(url).placeholder(R.drawable.app_placeholder).into(holder.iv_left_circle_image);
        }*/

        return view;
    }

    private static class ViewHolder {
        ImageView iv_right_rect_image, iv_left_circle_image;
        //CircleImageView iv_left_circle_image;
        TextView tv_left_month, tv_right_month, tv_right_skill, tv_left_skill, tv_month_title, tv_right_level, tv_left_level;
        RelativeLayout main_left_layout, main_right_layout;
    }
}
