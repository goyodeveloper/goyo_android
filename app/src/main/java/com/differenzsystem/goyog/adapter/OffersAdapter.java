package com.differenzsystem.goyog.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.OffersModel;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public class OffersAdapter extends BaseAdapter {
    Activity activity;
    private LayoutInflater inflator;
    ArrayList<OffersModel> dataList;

    public OffersAdapter(Context context, ArrayList<OffersModel> dataList) {
        activity = (Activity) context;
        this.dataList = dataList;
        inflator = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        OffersModel object = dataList.get(position);

        View view = convertView;
        ViewHolder holder = null;

        if (view == null) {
            view = inflator.inflate(R.layout.row_list_offeres, null);

            holder = new ViewHolder();

            holder.ll_section = (LinearLayout) view.findViewById(R.id.ll_section);
            holder.ll_main = (LinearLayout) view.findViewById(R.id.ll_main);

            holder.tv_section = (TextView) view.findViewById(R.id.tv_section);

            holder.iv_item = (ImageView) view.findViewById(R.id.iv_item);
            holder.tv_title = (TextView) view.findViewById(R.id.tv_title);
            holder.tv_status = (TextView) view.findViewById(R.id.tv_status);
            holder.tv_expireDate = (TextView) view.findViewById(R.id.tv_expireDate);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.ll_section.setVisibility(View.GONE);
        holder.ll_main.setVisibility(View.VISIBLE);

        String url = activity.getResources().getString(R.string.server_url)
                + activity.getResources().getString(R.string.challenge_images_url)
                + object.getThumb_image();

//        Glide.with(activity).load(url).placeholder(R.drawable.app_placeholder).into(holder.iv_item);

        Glide.with(activity)
                .load(url)
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                        return false;
                    }
                })
                .error(R.drawable.banner_placeholder)
                .into(holder.iv_item);

        holder.tv_title.setText(object.getOffer_title());
        holder.tv_status.setText(activity.getString(R.string.completed_lbl) + ": " + ConvertToDate(object.getCompleted_date()));

        if (object.getReward_expiry_date_flag() != null && !object.getReward_expiry_date_flag().isEmpty() &&
                object.getReward_expiry_date_flag().equalsIgnoreCase("1")) {
            //if (object.getInitial_reward().equalsIgnoreCase("1")) {
            String d1 = ConvertToDate(object.getOfferexpire_date());
            String d2 = UtilsPreferences.getString(activity, Constant.user_exp_date);
            if (d2 != null && d2.length() > 0) {
                d2 = ConvertToDate(d2);
                holder.tv_expireDate.setText(activity.getString(R.string.expires_on_lbl) + ": " + CompareDates(d1, d2));
            } else {
                holder.tv_expireDate.setText(activity.getString(R.string.expires_on_lbl) + ": " + ConvertToDate(object.getOfferexpire_date()));
            }
            /*} else {
                holder.tv_expireDate.setText(activity.getString(R.string.expires_on_lbl) + ": " + ConvertToDate(object.getOfferexpire_date()));
            }*/
        } else {
            holder.tv_expireDate.setText(activity.getString(R.string.expires_on_lbl) + ": " + ConvertToDate(object.getOfferexpire_date()));
        }

        return view;
    }

    private class ViewHolder {
        LinearLayout ll_section, ll_main;
        ImageView iv_item;
        TextView tv_section;
        TextView tv_title, tv_status, tv_expireDate;
    }

    private String ConvertToDate(String dateString) {
        String formattedDate = null;
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss");
        SimpleDateFormat reqformater = new SimpleDateFormat("dd-MM-yyyy");
        Date convertedDate = new Date();
        try {
            convertedDate = formater.parse(dateString);
            formattedDate = reqformater.format(convertedDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formattedDate;
    }

    private String CompareDates(String date1, String date2) {
        String date = null;
        SimpleDateFormat reqformater = new SimpleDateFormat("dd-MM-yyyy");
        Date d1 = new Date();
        Date d2 = new Date();

        try {
            d1 = reqformater.parse(date1);
            d2 = reqformater.parse(date2);
            if (d1.after(d2)) {
                date = reqformater.format(d2);
            } else if (d2.after(d1)) {
                date = reqformater.format(d1);
            } else {
                date = reqformater.format(d1);
            }

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return date;
    }

}
