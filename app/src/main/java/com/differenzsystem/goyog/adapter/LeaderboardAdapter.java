package com.differenzsystem.goyog.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.LeaderboardDataModel;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Union Assurance PLC on 8/13/16.
 */

public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<LeaderboardDataModel.Data> dataArrayList;

    String str_filter_id = "";
    String str_option_id = "";
    String str_tab_id = "";

    public LeaderboardAdapter(Context mContext, ArrayList<LeaderboardDataModel.Data> dataArrayList, String str_filter_id, String str_option_id, String str_tab_id) {
        this.mContext = mContext;
        this.dataArrayList = dataArrayList;
        this.str_filter_id = str_filter_id;
        this.str_option_id = str_option_id;
        this.str_tab_id = str_tab_id;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.leaderboard_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        setDataRow(holder, position);
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_no)
        TextView tv_no;

        @BindView(R.id.img_profile)
        CircleImageView img_profile;

        @BindView(R.id.tv_name)
        TextView tv_name;

        @BindView(R.id.tv_desc)
        TextView tv_desc;

        @BindView(R.id.tv_perc)
        TextView tv_perc;

        @BindView(R.id.lin_other_user)
        LinearLayout lin_other_user;

        @BindView(R.id.lin_own_user)
        LinearLayout lin_own_user;

        @BindView(R.id.tv_own_no)
        TextView tv_own_no;

        @BindView(R.id.img_own_profile)
        CircleImageView img_own_profile;

        @BindView(R.id.tv_own_name)
        TextView tv_own_name;

        @BindView(R.id.tv_own_desc)
        TextView tv_own_desc;

        @BindView(R.id.tv_own_perc)
        TextView tv_own_perc;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private void setDataRow(ViewHolder holder, int pos) {

        if (dataArrayList.get(pos).user_id.equalsIgnoreCase(UtilsPreferences.getString(mContext, Constant.user_id))) {
            holder.lin_other_user.setVisibility(View.GONE);
            holder.lin_own_user.setVisibility(View.VISIBLE);

            holder.tv_own_no.setText("" + dataArrayList.get(pos).rank);
            holder.tv_own_name.setText(mContext.getResources().getString(R.string.you_lbl));
            if (dataArrayList.get(pos).fitness_data != null && dataArrayList.get(pos).total_minutes != null) {
                holder.tv_own_desc.setText(dataArrayList.get(pos).fitness_data + " " + formatHoursAndMinutes(dataArrayList.get(pos).total_minutes));
            } else {
                holder.tv_own_desc.setText("");
            }

            if (dataArrayList.get(pos).total_percentage != null) {
                if (str_option_id.equalsIgnoreCase(Constant.option_activechallenge) || str_option_id.equalsIgnoreCase(Constant.option_activelevel)) {
                    holder.tv_own_perc.setText(dataArrayList.get(pos).total_percentage + "%");
                } else {
                    if (str_option_id.equalsIgnoreCase(Constant.option_distance)) {
                        DecimalFormat df2 = new DecimalFormat("0.0");
                        String dist_km = (df2.format(Double.parseDouble(dataArrayList.get(pos).total_percentage))+ " km");
                        holder.tv_own_perc.setText(dist_km);
                    } else {
                        holder.tv_own_perc.setText(dataArrayList.get(pos).total_percentage);
                    }
                }
            }

            String profile_url = mContext.getResources().getString(R.string.server_url)
                    + mContext.getResources().getString(R.string.profile_image)
                    + dataArrayList.get(pos).profile_image;

            Glide.with(mContext)
                    .load(profile_url)
                    .centerCrop()
                    .placeholder(R.drawable.user)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .into(holder.img_own_profile);
        } else {

            holder.lin_own_user.setVisibility(View.GONE);
            holder.lin_other_user.setVisibility(View.VISIBLE);

            holder.tv_no.setText("" + dataArrayList.get(pos).rank);
            if (dataArrayList.get(pos).first_name != null && dataArrayList.get(pos).last_name != null) {
                holder.tv_name.setText(dataArrayList.get(pos).first_name + " " + dataArrayList.get(pos).last_name);
            } else if (dataArrayList.get(pos).first_name != null && dataArrayList.get(pos).last_name == null) {
                holder.tv_name.setText(dataArrayList.get(pos).first_name);
            } else if (dataArrayList.get(pos).first_name == null && dataArrayList.get(pos).last_name != null) {
                holder.tv_name.setText(dataArrayList.get(pos).last_name);
            } else {
                holder.tv_name.setText("");
            }

            if (dataArrayList.get(pos).fitness_data != null && dataArrayList.get(pos).total_minutes != null) {
                holder.tv_desc.setText(dataArrayList.get(pos).fitness_data + " " + formatHoursAndMinutes(dataArrayList.get(pos).total_minutes));
            } else {
                holder.tv_desc.setText("");
            }

            if (dataArrayList.get(pos).total_percentage != null) {
                if (str_option_id.equalsIgnoreCase(Constant.option_activechallenge) || str_option_id.equalsIgnoreCase(Constant.option_activelevel)) {
                    holder.tv_perc.setText(dataArrayList.get(pos).total_percentage + "%");
                    //holder.tv_perc.setText(String.format("%.3f", Double.parseDouble(dataArrayList.get(pos).total_percentage)) + "%");
                } else {
                    if (str_option_id.equalsIgnoreCase(Constant.option_distance)) {
                        DecimalFormat df2 = new DecimalFormat("0.0");
                        String dist_km = (df2.format(Double.parseDouble(dataArrayList.get(pos).total_percentage))+ " km");
                        holder.tv_perc.setText(dist_km);
                    } else {
                        holder.tv_perc.setText(dataArrayList.get(pos).total_percentage);
                    }
                    //NumberFormat.getInstance().format()
                    //holder.tv_perc.setText(dataArrayList.get(pos).total_percentage);
                }
            }

            String profile_url = mContext.getResources().getString(R.string.server_url)
                    + mContext.getResources().getString(R.string.profile_image)
                    + dataArrayList.get(pos).profile_image;
            Glide.with(mContext)
                    .load(profile_url)
                    .centerCrop()
                    .placeholder(R.drawable.user)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .into(holder.img_profile);
        }
    }

    public static String formatHoursAndMinutes(String str_min) {
        try {
            int totalMinutes = Integer.parseInt(str_min);
            // for days
            if (totalMinutes / (24 * 60) > 0) {
                return Integer.toString(totalMinutes / (24 * 60)) + "days";
            } else if (totalMinutes / 60 > 0) { // for hours
                return Integer.toString(totalMinutes / 60) + "hrs";
            } else if (totalMinutes < 60 && totalMinutes > 0) {  // for mins
                return Integer.toString(totalMinutes) + "mins";
            } else {
                return "0min";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "0min";
        }

//        try {
//            int totalMinutes = Integer.parseInt(str_min);
//            String minutes = Integer.toString(totalMinutes % 60);
//            minutes = minutes.length() == 1 ? "0" + minutes : minutes;
//            String final_hrs = ((totalMinutes / 60) > 0) ? (totalMinutes / 60) + "hrs " : "";
//            String final_minutes = (Integer.parseInt(minutes) > 0) ? (minutes + "mins") : "";
//            return final_hrs + final_minutes;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "";
//        }
    }
}
