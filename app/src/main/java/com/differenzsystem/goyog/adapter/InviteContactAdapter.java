package com.differenzsystem.goyog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.differenzsystem.goyog.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Union Assurance PLC on 30/04/16.
 */
public class InviteContactAdapter extends BaseAdapter {

    String TAG = getClass().getName();
    Context context;
    ArrayList<String> data;
    private LayoutInflater inflater = null;
    private ArrayList<String> colleagueList = null;

    public InviteContactAdapter(Context context, ArrayList<String> contact_name) {
        this.context = context;
        data = contact_name;
        colleagueList = new ArrayList<String>();
        this.colleagueList.addAll(data);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public String getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final Viewholder holder;

        if (convertView == null) {
            vi = inflater.inflate(R.layout.invite_contact_listview, null);
            holder = new Viewholder();
            holder.tv_contact_name = (TextView) vi.findViewById(R.id.tv_contact_name);
            vi.setTag(holder);
        } else
            holder = (Viewholder) vi.getTag();


        holder.tv_contact_name.setText(data.get(position));

        return vi;
    }

    public void doRefresh(ArrayList<String> data) {
        //  this.checkboxState = new ArrayList<Boolean>(Collections.nCopies(data.size(), false));
        this.data = data;
        notifyDataSetChanged();
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();

        if (charText.length() == 0) {
            data.addAll(colleagueList);
        } else {
            for (String wp : colleagueList) {

                if (wp.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    data.add(wp);
                }
            }
        }
        doRefresh(data);
    }


    public class Viewholder {
        TextView tv_contact_name;
    }
}