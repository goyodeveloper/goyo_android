package com.differenzsystem.goyog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.TimeLine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Union Assurance PLC on 8/19/16.
 */

public class ProfileTimelineAdapter extends BaseAdapter {
    ArrayList<HashMap<String, String>> data;
    Context context;
    LayoutInflater inflater;
    List<TimeLine> timeLines;

    public ProfileTimelineAdapter(Context context, ArrayList<HashMap<String, String>> data, List<TimeLine> timeLines) {
        this.context = context;
        this.data = data;
        this.timeLines = timeLines;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.profile_timeline_list_item, null);
            holder = new ViewHolder();

            holder.iv_right_rect_image = (ImageView) view.findViewById(R.id.iv_right_rect_image);
            holder.iv_left_circle_image = (ImageView) view.findViewById(R.id.iv_left_circle_image);
            holder.tv_left_month = (TextView) view.findViewById(R.id.tv_left_month);
            holder.tv_right_month = (TextView) view.findViewById(R.id.tv_right_month);
            holder.tv_right_skill = (TextView) view.findViewById(R.id.tv_right_skill);
            holder.tv_left_skill = (TextView) view.findViewById(R.id.tv_left_skill);
            holder.tv_month_title = (TextView) view.findViewById(R.id.tv_month_title);
            holder.main_left_layout = (RelativeLayout) view.findViewById(R.id.main_left_layout);
            holder.main_right_layout = (RelativeLayout) view.findViewById(R.id.main_right_layout);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        HashMap<String, String> temp = data.get(position);

        holder.tv_month_title.setVisibility(View.VISIBLE);
        holder.tv_month_title.setText(temp.get(Constant.month));
        if (position > 0) {
            if (temp.get(Constant.month).equalsIgnoreCase(data.get(position - 1).get(Constant.month))) {
                holder.tv_month_title.setVisibility(View.GONE);
            }
        }
        if (position % 2 == 0) {
            holder.main_right_layout.setVisibility(View.GONE);
            holder.main_left_layout.setVisibility(View.VISIBLE);
            holder.tv_right_month.setText(temp.get(Constant.month));
            holder.tv_right_skill.setText(temp.get(Constant.skill));

        } else {
            holder.main_right_layout.setVisibility(View.VISIBLE);
            holder.main_left_layout.setVisibility(View.GONE);
            holder.tv_left_month.setText(temp.get(Constant.month));
            holder.tv_left_skill.setText(temp.get(Constant.skill));
        }

        return view;
    }

    private static class ViewHolder {
        ImageView iv_right_rect_image,iv_left_circle_image;
       // CircleImageView iv_left_circle_image;
        TextView tv_left_month, tv_right_month, tv_right_skill, tv_left_skill, tv_month_title;
        RelativeLayout main_left_layout, main_right_layout;
    }
}
