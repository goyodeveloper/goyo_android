package com.differenzsystem.goyog.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.daimajia.swipe.interfaces.SwipeItemMangerInterface;
import com.daimajia.swipe.util.Attributes;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.NotificationModel;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Union Assurance PLC on 7/27/17.
 */

public class NotificationFragmentAdapter extends RecyclerView.Adapter<NotificationFragmentAdapter.ViewHolder> implements SwipeAdapterInterface, SwipeItemMangerInterface {
    private NotificationModel notificationModel;
    private Context mContext;
    private SwipeItemRecyclerMangerImpl mItemMange;
    TextView tv_no_notification_data;
    OnItemClickListener onItemClickListener;


    public NotificationFragmentAdapter(Context mContext, NotificationModel notificationModel, OnItemClickListener onItemClickListener, TextView tv_no_notification_data) {
        this.mContext = mContext;
        this.notificationModel = notificationModel;
        this.tv_no_notification_data = tv_no_notification_data;
        this.onItemClickListener = onItemClickListener;
        mItemMange = new SwipeItemRecyclerMangerImpl(this);
    }

    public interface OnItemClickListener {
        void onItemClick(String id, int position);

        void onItemLongClick(int position);
    }

    private OnItemClickListener mListener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_item_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {
        setDataRow(viewHolder, pos);
    }

    @Override
    public int getItemCount() {
        return notificationModel.data.size();
    }

    public void removeItem(int position) {
        mItemMange.closeItem(position);
        notificationModel.data.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, notificationModel.data.size());
        if (notificationModel != null && notificationModel.data != null && notificationModel.data.size() > 0)
            tv_no_notification_data.setVisibility(View.GONE);
        else
            tv_no_notification_data.setVisibility(View.VISIBLE);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public void openItem(int position) {

    }

    @Override
    public void closeItem(int position) {

    }

    @Override
    public void closeAllExcept(SwipeLayout layout) {

    }

    @Override
    public void closeAllItems() {

    }

    @Override
    public List<Integer> getOpenItems() {
        return null;
    }

    @Override
    public List<SwipeLayout> getOpenLayouts() {
        return null;
    }

    @Override
    public void removeShownLayouts(SwipeLayout layout) {

    }

    @Override
    public boolean isOpen(int position) {
        return false;
    }

    @Override
    public Attributes.Mode getMode() {
        return null;
    }

    @Override
    public void setMode(Attributes.Mode mode) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_notification_desc, tv_notification_date;
        CircleImageView img_notification_profile;
        SwipeLayout swipe;
        TextView tv_delete;

        public ViewHolder(View view) {
            super(view);

            tv_notification_date = (TextView) view.findViewById(R.id.tv_notification_date);
            tv_notification_desc = (TextView) view.findViewById(R.id.tv_notification_desc);
            img_notification_profile = (CircleImageView) view.findViewById(R.id.img_notification_profile);
            tv_delete = (TextView) view.findViewById(R.id.tv_delete);
            swipe = (SwipeLayout) view.findViewById(R.id.swipe);
            swipe.addDrag(SwipeLayout.DragEdge.Right, view.findViewById(R.id.lin_delete));

        }
    }

    public void setDataRow(final ViewHolder viewHolder, final int pos) {

        viewHolder.tv_notification_date.setText(UtilsCommon.timeStampClass(mContext, notificationModel.data.get(pos).notification_date));
//        viewHolder.tv_notification_date.setText(UtilsCommon.timeStampClass("2017-07-30 17:44:24"));
        if (notificationModel.data.get(pos).notification_message != null)
            viewHolder.tv_notification_desc.setText(notificationModel.data.get(pos).notification_message.trim());
        else
            viewHolder.tv_notification_desc.setText(notificationModel.data.get(pos).notification_message);

        String url = "";
        String gender = UtilsPreferences.getString(mContext, Constant.gender);

        if (gender == null || gender.isEmpty() || gender.equalsIgnoreCase(Constant.male)) {
            url = mContext.getResources().getString(R.string.server_url)
                    + mContext.getResources().getString(R.string.notification_avatar_image)
                    + notificationModel.avtar_data.male_avatar;
        } else {//if (UtilsPreferences.getString(mContext, Constant.gender).equalsIgnoreCase(Constant.female))
            url = mContext.getResources().getString(R.string.server_url)
                    + mContext.getResources().getString(R.string.notification_avatar_image)
                    + notificationModel.avtar_data.female_avatar;
        }

        Glide.with(mContext)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.user)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .into(viewHolder.img_notification_profile);

        /*String url = "http://a2.mzstatic.com/us/r30/Purple5/v4/58/2e/b9/582eb968-988b-ad42-dee6-92d396cbde5c/icon256.png";
        Glide.with(mContext)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.app_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .into(viewHolder.img_notification_profile);*/

        viewHolder.swipe.setShowMode(SwipeLayout.ShowMode.PullOut);

        viewHolder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onItemClickListener.onItemClick(notificationModel.data.get(pos).id, pos);
//                removeItem(pos);
//                Toast.makeText(mContext, "" + mContext.getString(R.string.toast_delete), Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.swipe.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {
                mItemMange.closeAllExcept(layout);
            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });
        mItemMange.bindView(viewHolder.itemView, pos);

    }
}
