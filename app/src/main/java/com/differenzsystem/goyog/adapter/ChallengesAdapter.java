package com.differenzsystem.goyog.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.model.ChallengesModel;

import java.util.ArrayList;


import static com.differenzsystem.goyog.R.id.tv_description;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public class ChallengesAdapter extends BaseAdapter {
    Activity activity;
    private LayoutInflater inflator;

    ArrayList<ChallengesModel> dataList;

    public ChallengesAdapter(Context context, ArrayList<ChallengesModel> dataList) {
        activity = (Activity) context;
        this.dataList = dataList;
        inflator = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        ChallengesModel object = dataList.get(position);
        if (object.isSection()) {
            return false;
        }
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ChallengesModel object = dataList.get(position);

        View view = convertView;
        ViewHolder holder = null;

        if (view == null) {
            view = inflator.inflate(R.layout.row_list_challenges, null);

            holder = new ViewHolder();

            // initialize all controls define in xml layout
            holder.ll_section = (LinearLayout) view.findViewById(R.id.ll_section);
            holder.ll_main = (LinearLayout) view.findViewById(R.id.ll_main);

            holder.tv_section = (TextView) view.findViewById(R.id.tv_section);

            holder.iv_item = (ImageView) view.findViewById(R.id.iv_item);
            holder.tv_name = (TextView) view.findViewById(R.id.tv_name);
            holder.tv_description = (TextView) view.findViewById(tv_description);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if (object.isSection()) {
            try {
                holder.ll_section.setVisibility(View.VISIBLE);
                holder.ll_main.setVisibility(View.GONE);

                holder.tv_section.setText(object.getSectionName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                holder.ll_section.setVisibility(View.GONE);
                holder.ll_main.setVisibility(View.VISIBLE);

                String url = activity.getResources().getString(R.string.server_url)
                        + activity.getResources().getString(R.string.challenge_images_url)
                        + object.getThumb_image();
                Glide.with(activity)
                        .load(url)
                        .centerCrop()
                        .placeholder(R.drawable.app_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .dontAnimate()
                        .into(holder.iv_item); // load images

                holder.tv_name.setText(object.getName());
                //holder.tv_description.setText(object.getDescription());
                holder.tv_description.setText(object.getSubtitle());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return view;
    }

    private class ViewHolder {
        LinearLayout ll_section, ll_main;
        ImageView iv_item;
        TextView tv_section;
        TextView tv_name, tv_description;
    }
}
