package com.differenzsystem.goyog.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.HintModel;
import com.differenzsystem.goyog.userProfile.LoginActivity;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import java.util.ArrayList;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public class TutorialPageAdapter extends PagerAdapter {
    Activity activity;
    private LayoutInflater inflator;

    ArrayList<HintModel> dataList;

    public TutorialPageAdapter(Context context, ArrayList<HintModel> dataList) {
        activity = (Activity) context;
        this.dataList = dataList;
        inflator = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        HintModel object = dataList.get(position);

        View view = inflator.inflate(R.layout.row_pager_tutorial, container, false);

        assert view != null;

        final Button btn_submit = (Button) view.findViewById(R.id.btn_submit);
        final ImageView iv_pagerImage = (ImageView) view.findViewById(R.id.iv_pagerImage);
        final TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        final TextView tv_description = (TextView) view.findViewById(R.id.tv_description);

        try {
            btn_submit.setTypeface(setFont(activity, R.string.app_semibold));

            tv_title.setText(object.title);
            tv_description.setText(object.description);
            iv_pagerImage.setImageResource(object.filePath);
            if (position == dataList.size() - 1)
                btn_submit.setVisibility(View.VISIBLE);
            else
                btn_submit.setVisibility(View.INVISIBLE);
            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, LoginActivity.class);
                    UtilsPreferences.setBoolean(activity, Constant.tutorial_flag, true);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                    activity.finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        container.addView(view, 0);

        return view;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}

