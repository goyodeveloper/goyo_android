package com.differenzsystem.goyog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.model.ChallengesLevelModel;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.model.NextLevelChallengeModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by mac on 7/26/17.
 */

public class CustomGrid extends BaseAdapter {
    private Context mContext;
    private NextLevelChallengeModel nextLevelChallengeModel;

    CustomGrid(Context c, NextLevelChallengeModel nextLevelChallengeModel) {
        mContext = c;
        this.nextLevelChallengeModel = nextLevelChallengeModel;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return nextLevelChallengeModel.data.level_data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.grid_item_row, null);
            TextView tv_level_name = (TextView) grid.findViewById(R.id.tv_level_name);
            CircleImageView img_level = (CircleImageView) grid.findViewById(R.id.img_level);
            tv_level_name.setText(nextLevelChallengeModel.data.level_data.get(position).level_subtitle);

            String url = mContext.getResources().getString(R.string.server_url)
                    + mContext.getResources().getString(R.string.badges_image)
                    + nextLevelChallengeModel.data.level_data.get(position).level_image;
            Glide.with(mContext)
                    .load(url)
                    .centerCrop()
                    .placeholder(R.drawable.app_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .into(img_level);
        } else {
            grid = (View) convertView;
        }

        return grid;
    }

}
