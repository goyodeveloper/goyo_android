package com.differenzsystem.goyog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.model.completedchallbadges;

/**
 * Created by Union Assurance PLC on 8/17/16.
 */

public class ProfileGoalImageAdapter extends BaseAdapter {
    Context context;
    completedchallbadges completedchallbadges;
    LayoutInflater inflater;

    public ProfileGoalImageAdapter(Context context, completedchallbadges completedchallbadges) {
        this.context = context;
        this.completedchallbadges = completedchallbadges;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return completedchallbadges.getCompletedChallenges().size();
    }

    @Override
    public Object getItem(int position) {
        return completedchallbadges.getCompletedChallenges().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.profile_single_image, null);
            holder = new ViewHolder();

            holder.iv_goal = (ImageView) view.findViewById(R.id.iv_goal);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        String url = context.getResources().getString(R.string.server_url)
                + context.getResources().getString(R.string.challenge_images_url)
                + completedchallbadges.getCompletedChallenges().get(position).getThumb_image();
        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.app_placeholder)
                .dontAnimate()
                .into(holder.iv_goal);

        return view;
    }

    static class ViewHolder {
        ImageView iv_goal;
    }
}
