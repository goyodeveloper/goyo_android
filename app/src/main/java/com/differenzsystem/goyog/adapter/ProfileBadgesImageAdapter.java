package com.differenzsystem.goyog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.model.completedchallbadges;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Union Assurance PLC on 8/17/16.
 */

public class ProfileBadgesImageAdapter extends BaseAdapter {
    Context context;
    completedchallbadges completedchallbadges;
    LayoutInflater inflater;

    public ProfileBadgesImageAdapter(Context context, completedchallbadges completedchallbadges) {
        this.context = context;
        this.completedchallbadges = completedchallbadges;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return completedchallbadges.getCompletedBadges().size();
    }

    @Override
    public Object getItem(int position) {
        return completedchallbadges.getCompletedBadges().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.profile_single_circle_image, null);
            holder = new ViewHolder();

            holder.iv_badges = (CircleImageView) view.findViewById(R.id.iv_badges);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Glide.with(context)
                .load(context.getResources().getString(R.string.server_url)
                        + context.getResources().getString(R.string.badges_image)
                        + completedchallbadges.getCompletedBadges().get(position).getLevel_image())
                .placeholder(R.drawable.app_placeholder)
                .dontAnimate()
                .into(holder.iv_badges);

        return view;
    }

    static class ViewHolder {
        CircleImageView iv_badges;

    }
}
