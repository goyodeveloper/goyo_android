package com.differenzsystem.goyog.adapter;

import android.app.Activity;
import android.fitchart.FitChart;
import android.fitchart.FitChartValue;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.challenges.ChallengesDetailFragment;
import com.differenzsystem.goyog.dashboard.challenges.InProcessChallangeDetailFragment;
import com.differenzsystem.goyog.dashboard.home.GraphFragment;
import com.differenzsystem.goyog.dashboard.home.NextLevelDetailFragment;
import com.differenzsystem.goyog.model.ChallengesModel;
import com.differenzsystem.goyog.model.ChartDataModel;
import com.differenzsystem.goyog.model.NextLevelChallengeModel;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Collection;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by a on 28/10/16.
 */

public class HomeSlidingChartAdapter extends PagerAdapter {
    /*TextView tv_level, tv_level_name;
    ImageView iv_home_badge;
    FitChart fitChart;*/

    private static ArrayList<ChartDataModel> chartDataModels = new ArrayList<>();
    private Activity context;
    private NextLevelChallengeModel nextLevelChallengeModel;

    static final int INTENT_ACCEPT_CHALLENGE = 23;

    public HomeSlidingChartAdapter(Activity context, ArrayList<ChartDataModel> chartDataModels) {
        this.context = context;
        HomeSlidingChartAdapter.chartDataModels = chartDataModels;
    }

    public HomeSlidingChartAdapter(Activity context, ArrayList<ChartDataModel> chartDataModels, NextLevelChallengeModel nextLevelChallengeModel) {
        this.context = context;
        HomeSlidingChartAdapter.chartDataModels = chartDataModels;
        this.nextLevelChallengeModel = nextLevelChallengeModel;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if (UtilsPreferences.getString(context, Constant.user_type).equalsIgnoreCase("1"))
            return chartDataModels.size() + 1;
        else
            return chartDataModels.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        //LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int resId = -1;
        if (UtilsPreferences.getString(context, Constant.user_type).equalsIgnoreCase("1")) {
            if (position == 0)
                resId = R.layout.layout_chart_pager_screen1;
            else
                resId = R.layout.chart_pager_item;
        } else {
            resId = R.layout.chart_pager_item;
        }
        View view = inflater.inflate(resId, container, false);
        if (UtilsPreferences.getString(context, Constant.user_type).equalsIgnoreCase("1")) {
            if (position == 0) {
                processLevelChallengePagerScreen(view);
            } else {
                processLevelChartPagerScreen(view, position - 1);
            }
        } else {
            processLevelChartPagerScreen(view, position);
        }

        container.addView(view);
        return view;
    }

    // initialize all controls define in xml layout
    private void processLevelChallengePagerScreen(View view) {
        LinearLayout lin_challenge = (LinearLayout) view.findViewById(R.id.lin_challenge);
        TextView tv_challenge_name = (TextView) view.findViewById(R.id.tv_challenge_name);
        TextView tv_challenge_description = (TextView) view.findViewById(R.id.tv_challenge_description);
        ImageView img_challenge = (ImageView) view.findViewById(R.id.img_challenge);
        TextView tv_challenge_level_not_found = (TextView) view.findViewById(R.id.tv_challenge_level_not_found);
        LinearLayout lin_challenge_next_level = (LinearLayout) view.findViewById(R.id.lin_challenge_next_level);
        if (nextLevelChallengeModel != null) {
            CustomGrid adapter = new CustomGrid(context, nextLevelChallengeModel);
            GridView gv_challenge = (GridView) view.findViewById(R.id.gv_challenge);
            gv_challenge.setVerticalScrollBarEnabled(false); // disable scrolling of gridview
            gv_challenge.setAdapter(adapter);
            if (nextLevelChallengeModel != null && !nextLevelChallengeModel.data.challenge_data.isEmpty()) {
                lin_challenge.setVisibility(View.VISIBLE);
                tv_challenge_name.setText(nextLevelChallengeModel.data.challenge_data.get(0).name);
                tv_challenge_description.setText(nextLevelChallengeModel.data.challenge_data.get(0).description.
                        replace("\\n", System.getProperty("line.separator")));

                String url = context.getResources().getString(R.string.server_url)
                        + context.getResources().getString(R.string.challenge_images_url)
                        + nextLevelChallengeModel.data.challenge_data.get(0).thumb_image;
                Glide.with(context)
                        .load(url)
                        .centerCrop()
                        .placeholder(R.drawable.app_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .dontAnimate()
                        .into(img_challenge);
            } else {
                lin_challenge.setVisibility(View.GONE);
            }

            lin_challenge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();

                    Bundle extras = new Bundle();
                    extras.putBoolean(Constant.isFromNextLevelScreen, true);
                    extras.putSerializable("Object", getChallengesModel());
                    addFragment(ChallengesDetailFragment.newInstance(extras), "ChallengesDetailFragment");

                }
            });

            gv_challenge.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

//                Toast.makeText(context, "" + pos, Toast.LENGTH_SHORT).show();
                    Bundle extras = new Bundle();
                    extras.putSerializable(Constant.key_next_level_object, nextLevelChallengeModel.data.level_data.get(pos));
                    addFragment(NextLevelDetailFragment.newInstance(extras), "NextLevelDetailFragment");
                }
            });
        } else {
            lin_challenge_next_level.setVisibility(View.GONE);
            tv_challenge_level_not_found.setVisibility(View.VISIBLE);
        }

    }

    // prepare chart based on level type and populate in screen
    private void processLevelChartPagerScreen(View view, final int position) {
        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);

        TextView tv_level = (TextView) view.findViewById(R.id.tv_level);
        TextView tv_level_name = (TextView) view.findViewById(R.id.tv_level_name);
        CircleImageView iv_home_badge = (CircleImageView) view.findViewById(R.id.iv_home_badge);
        FitChart fitChart = (FitChart) view.findViewById(R.id.fitChart);
        fitChart.setMinValue(0f);
        fitChart.setMaxValue(100f);
        ChartDataModel chartDataModel = new ChartDataModel();
        chartDataModel = chartDataModels.get(position);

        int leveltype = chartDataModel.getChallengetype() == 0 ? 1 : chartDataModel.getChallengetype();
        int leveltotalvalue = chartDataModel.getTotalval() == 0 ? 1 : chartDataModel.getTotalval();
        int secondLeveltotalvalue = chartDataModel.getSectotalval() == 0 ? 1 : chartDataModel.getSectotalval();
        int thirdLeveltotalvalue = chartDataModel.getThirdtotalval() == 0 ? 1 : chartDataModel.getThirdtotalval();
        int fourthLeveltotalvalue = chartDataModel.getFourthtotalval() == 0 ? 1 : chartDataModel.getFourthtotalval();

        float levelPer = 0, secondLevelPer = 0, thirdLevelPer = 0, fourthLevelPer = 0;

        int levelvalue = chartDataModel.getVal();
        int secondLevelValue = chartDataModel.getSecval();
        int thirdLevelValue = chartDataModel.getThirdval();
        int fourthLevelValue = chartDataModel.getFourthval();

        Collection<FitChartValue> values = new ArrayList<>();

        /*if (leveltype == 3) {
            leveltotalvalue = (int) UtilsCommon.getMiles((float) leveltotalvalue);
        } else if (leveltype == 10)
            leveltotalvalue = (int) UtilsCommon.getMiles((float) leveltotalvalue);*/

        if (levelvalue <= leveltotalvalue) {
            levelPer = (float) (levelvalue * 100) / leveltotalvalue;
        } else if (levelvalue > leveltotalvalue) {
            levelPer = 100;
        }

        if (leveltype == 5 || leveltype == 6 || leveltype == 7 || leveltype == 8 || leveltype == 9 || leveltype == 10) {

            /*if (leveltype == 6) {
                secondLeveltotalvalue = (int) UtilsCommon.getMiles((float) secondLeveltotalvalue);
            }*/
            if (secondLevelValue <= secondLeveltotalvalue) {
                secondLevelPer = (float) (secondLevelValue * 100) / secondLeveltotalvalue;
                levelPer /= 2;
                secondLevelPer /= 2;
            } else if (secondLevelValue > secondLeveltotalvalue) {
                levelPer /= 2;
                secondLevelPer = 50;
            }
        }

        if (leveltype == 11 || leveltype == 12 || leveltype == 13 || leveltype == 14) {

            if (secondLevelValue <= secondLeveltotalvalue) {
                secondLevelPer = (float) (secondLevelValue * 100) / secondLeveltotalvalue;
                secondLevelPer = secondLevelPer / 3;
            } else if (secondLevelValue > secondLeveltotalvalue) {
                secondLevelPer = 33;
            }

            if (thirdLevelValue <= thirdLeveltotalvalue) {
                thirdLevelPer = (float) (thirdLevelValue * 100) / thirdLeveltotalvalue;
                thirdLevelPer = thirdLevelPer / 3;
            } else if (thirdLevelValue > thirdLeveltotalvalue) {
                thirdLevelPer = 33;
            }

            if (levelPer >= 100)
                levelPer = 34;
            else
                levelPer /= 3;

        }

        if (leveltype == 15) {

            if (secondLevelValue <= secondLeveltotalvalue) {
                secondLevelPer = (float) (secondLevelValue * 100) / secondLeveltotalvalue;
                secondLevelPer = secondLevelPer / 4;
            } else if (secondLevelValue > secondLeveltotalvalue) {
                secondLevelPer = 25;
            }

            if (thirdLevelValue <= thirdLeveltotalvalue) {
                thirdLevelPer = (float) (thirdLevelValue * 100) / thirdLeveltotalvalue;
                thirdLevelPer = thirdLevelPer / 4;
            } else if (thirdLevelValue > thirdLeveltotalvalue) {
                thirdLevelPer = 25;
            }

            if (fourthLevelValue <= fourthLeveltotalvalue) {
                fourthLevelPer = (float) (fourthLevelValue * 100) / fourthLeveltotalvalue;
                fourthLevelPer /= 4;
            } else if (fourthLevelValue > fourthLeveltotalvalue) {
                fourthLevelPer = 25;
            }

            levelPer /= 4;

        }
//TODO

        switch (leveltype) {
            case 1://steps
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress2)));
                break;
            case 2://calories
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress3)));
                break;
            case 3://distance
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress4)));
                break;
            case 4://sleep
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress1)));
                break;
            case 5://distance and sleep
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress4)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress1)));
                break;
            case 6: //steps and distance
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress2)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress4)));
                break;
            case 7://steps and calories
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress2)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress3)));
                break;
            case 8://steps and sleep
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress2)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress1)));
                break;
            case 9://sleep and calories
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress1)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress3)));
                break;
            case 10://distance and calories
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress4)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress3)));
                break;
            case 11: //steps and calories and distance
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress2)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress3)));
                values.add(new FitChartValue(thirdLevelPer, context.getResources().getColor(R.color.progress4)));
                break;
            case 12://steps and calories nad sleep
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress2)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress3)));
                values.add(new FitChartValue(thirdLevelPer, context.getResources().getColor(R.color.progress1)));
                break;
            case 13://steps and distance and sleep
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress2)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress4)));
                values.add(new FitChartValue(thirdLevelPer, context.getResources().getColor(R.color.progress1)));
                break;
            case 14://calories and distance and sleep
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress3)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress4)));
                values.add(new FitChartValue(thirdLevelPer, context.getResources().getColor(R.color.progress1)));
                break;
            case 15://steps and calories and distance and sleep
                values.add(new FitChartValue(levelPer, context.getResources().getColor(R.color.progress2)));
                values.add(new FitChartValue(secondLevelPer, context.getResources().getColor(R.color.progress3)));
                values.add(new FitChartValue(thirdLevelPer, context.getResources().getColor(R.color.progress4)));
                values.add(new FitChartValue(fourthLevelPer, context.getResources().getColor(R.color.progress1)));
                break;

        }

        if (chartDataModel.getChallengesModels() != null) {
            Log.e("challange_id", chartDataModel.getChallengesModels().getChallenge_id());
            tv_level.setText((chartDataModel.getChallengesModels().getSubtitle()));
            tv_level_name.setText(chartDataModel.getChallengesModels().getName());
            String url = context.getResources().getString(R.string.server_url)
                    + context.getResources().getString(R.string.challenge_images_url)
                    + chartDataModel.getChallengesModels().getThumb_image();
            Glide.with(context).load(url).placeholder(R.drawable.app_placeholder).diskCacheStrategy(DiskCacheStrategy.SOURCE).dontAnimate().into(iv_home_badge);
//            Glide.with(context)
//                    .load(url)
//                    .centerCrop()
//                    .placeholder(R.drawable.app_placeholder)
//                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                    .dontAnimate()
//                    .into(iv_home_badge);
        }

        if (chartDataModel.getLevelInformationModel() != null) {

            tv_level.setText(chartDataModel.getLevelInformationModel().getLevel_subtitle());
            tv_level_name.setText(chartDataModel.getLevelInformationModel().getLevel_name());

            String url = context.getResources().getString(R.string.server_url)
                    + context.getResources().getString(R.string.badges_image)
                    + chartDataModel.getLevelInformationModel().getLevel_image();

            Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.app_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .into(iv_home_badge);
        }
        //int levelPer = (float)((chartDataModel.getVal() * 100) / chartDataModel.getTotalval());
        //values.add(new FitChartValue((float) levelPer, context.getResources().getColor(R.color.progress2)));
        fitChart.setValues(values);

        final ChartDataModel finalChartDataModel = chartDataModel;
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (position == 0) {
                    addFragment(GraphFragment.newInstance(null), "GraphFragment");
                } else {
                    ChallengesModel object = finalChartDataModel.getChallengesModels();
                    Bundle extras = new Bundle();
                    extras.putSerializable("Object", object);
                    addFragment(InProcessChallangeDetailFragment.newInstance(extras), "InProcessChallengesDetailFragment");
                }
                // addFragment(new InProcessChallangeDetailFragment(),"");
            }
        });
    }

    // open required screen

    /**
     * @param fragment add fragment
     */
    private void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
        fragmentTransaction.add(R.id.frame_main, fragment, null);//change add to replace
//        if (tag.equalsIgnoreCase("InProcessChallengesDetailFragment")) {
//            fragmentTransaction.addToBackStack("challanges");
//        } else {
//            fragmentTransaction.addToBackStack(null);
//        }
        //change tag to null
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    private ChallengesModel getChallengesModel() {
        NextLevelChallengeModel.Challenge_data data = nextLevelChallengeModel.data.challenge_data.get(0);

        return new ChallengesModel(data.challenge_id,
                data.name,
                data.description,
                data.image,
                data.type,
                data.qrcode,
                data.point,
                data.date,
                data.duration,
                data.challenge_value,
                data.second_challenge_value,
                data.third_challenge_value,
                data.fourth_challenge_value,
                data.challenge_type,
                data.challenges_exp_date,
                data.offerexpire_date,
                data.subtitle,
                data.challenge_start_date,
                data.thumb_image,
                data.reward_hyperlink_text,
                data.reward_hyperlink,
                data.accept_flag);
    }

}
