package com.differenzsystem.goyog.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.InviteModel;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Union Assurance PLC on 30/04/16.
 */
public class InviteAdapter extends BaseAdapter {

    String TAG = getClass().getName();
    Context context;
    ArrayList<InviteModel> contactList;
    private LayoutInflater inflater = null;
    private ArrayList<InviteModel> colleagueList = null;

    public InviteAdapter(Context context, ArrayList<InviteModel> contactList) {
        this.context = context;
        this.contactList = contactList;

        colleagueList = new ArrayList<InviteModel>();
        this.colleagueList.addAll(contactList);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public Object getItem(int position) {
        return contactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final Viewholder holder;

        if (convertView == null) {
            vi = inflater.inflate(R.layout.invite_contact_listview, null);
            holder = new Viewholder();
            holder.tv_contact_name = (TextView) vi.findViewById(R.id.tv_contact_name);
            holder.tv_invite = (TextView) vi.findViewById(R.id.tv_invite);

            vi.setTag(holder);
        } else
            holder = (Viewholder) vi.getTag();


        holder.tv_contact_name.setText(contactList.get(position).getContactName());

        holder.tv_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent smsIntent = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:5551212;5551212"));
//                smsIntent.putExtra("sms_body", "sms message goes here");
//                context.startActivity(smsIntent);

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.invite_member_text) + " " + Constant.AppLink);
                sendIntent.setType("text/plain");
                Intent openInChooser = Intent.createChooser(sendIntent, context.getString(R.string.invite_via_lbl));
                context.startActivity(openInChooser);

            }
        });

        return vi;
    }

    public void doRefresh(ArrayList<InviteModel> data) {
        //  this.checkboxState = new ArrayList<Boolean>(Collections.nCopies(data.size(), false));
        this.contactList = data;
        notifyDataSetChanged();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        contactList.clear();

        if (charText.length() == 0) {
            contactList.addAll(colleagueList);
        } else {
            for (InviteModel wp : colleagueList) {

                if (wp.getContactName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    contactList.add(wp);
                }
            }
        }
        doRefresh(contactList);
    }


    public class Viewholder {
        TextView tv_contact_name, tv_invite;
    }

}