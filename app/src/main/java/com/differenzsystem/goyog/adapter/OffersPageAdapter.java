package com.differenzsystem.goyog.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.dashboard.challenges.OfferDetailQR;
import com.differenzsystem.goyog.model.OffersModel;

import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 13/8/16.
 */

public class OffersPageAdapter extends PagerAdapter {
    Activity activity;
    private LayoutInflater inflator;

    ArrayList<OffersModel> dataList;
    Fragment parentFragment;

    public OffersPageAdapter(Fragment parentFragment, Context context, ArrayList<OffersModel> dataList) {
        activity = (Activity) context;
        this.dataList = dataList;
        this.parentFragment = parentFragment;
        inflator = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if (dataList.size() > 4)
            return 4;
        else
            return dataList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        OffersModel object = dataList.get(position);

        View imageLayout = inflator.inflate(R.layout.row_pager_challenge_offer, container, false);

        assert imageLayout != null;

        final ImageView iv_gallery = (ImageView) imageLayout.findViewById(R.id.iv_gallery);
        final TextView tv_description = (TextView) imageLayout.findViewById(R.id.tv_description);

        if (object.getImage() != null) {
            try {
                tv_description.setText(object.getName());

                String url = activity.getResources().getString(R.string.server_url)
                        + activity.getResources().getString(R.string.challenge_images_url)
                        + object.getImage();

                Glide.with(activity)
                        .load(url)
                        .placeholder(R.drawable.banner_placeholder)
                        .into(iv_gallery);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        iv_gallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras = new Bundle();
                extras.putSerializable("Object", dataList.get(position));
                addFragment(OfferDetailQR.newInstance(extras), "ChallengesDetailFragment");
            }
        });

        container.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    private void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = parentFragment.getParentFragment().getFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_main, fragment, null);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }
}

