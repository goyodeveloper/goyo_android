package com.differenzsystem.goyog.userProfile;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.differenzsystem.goyog.utility.UtilsValidation;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

public class RegistrationActivity_3 extends AppCompatActivity implements View.OnClickListener {
    TextView tv_nic_number, tv_title;
    ImageView iv_editnic;
    LinearLayout ll_back;
    Button btn_confirm;
    String nicnum = null;
    EditText edt_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity_3);

        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegistrationActivity_3.this, Constant.Name_Registration_Step3);
    }

    // initialize all controls define in xml layout
    void initializeControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(R.string.reg_title3);

            iv_editnic = (ImageView) findViewById(R.id.iv_editnic);
            tv_nic_number = (TextView) findViewById(R.id.tv_nic_number);
            btn_confirm = (Button) findViewById(R.id.btn_confirm);

            nicnum = tv_nic_number.getText().toString().trim();

            tv_nic_number.setText(UtilsPreferences.getString(getContext(), Constant.nic));

            tv_nic_number.setTypeface(setFont(getContext(), R.string.app_regular));
            btn_confirm.setTypeface(setFont(getContext(), R.string.app_bold));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // set click event listener
    public void initializeControlsAction() {
        try {
            btn_confirm.setOnClickListener(this);
            ll_back.setOnClickListener(this);
            iv_editnic.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event listener
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm:
                if (tv_nic_number.getText().toString().trim() != null && tv_nic_number.getText().toString().trim().length() > 0) {
                    storeData();
                } else {
                    Toast.makeText(RegistrationActivity_3.this, getResources().getString(R.string.err_msg_please_enter_nic), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.iv_editnic:
                openEditDialog();
                break;
        }
    }

    // save nic in shared preferences
    public void storeData() {
        UtilsPreferences.setString(getContext(), Constant.nic, tv_nic_number.getText().toString().trim());
        //RegistrationActivity.RegDetail.put("NIC", tv_nic_number.getText().toString().trim());
        startActivity(new Intent(getContext(), RegistrationActivity_Height_Weight.class));
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

    }

    // back click event listener
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    // open dialog for edit nic
    public void openEditDialog() {
        final Dialog alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.setting_edit_name_dialog);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        edt_name = (EditText) alertDialog.findViewById(R.id.et_name);
        TextView tv_cancel = (TextView) alertDialog.findViewById(R.id.tv_cancel);
        TextView tv_change = (TextView) alertDialog.findViewById(R.id.tv_change);
        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        View vi_devider = (View) alertDialog.findViewById(R.id.vi_devider);
        tv_popup_title.setText("NIC or Passport");
        tv_cancel.setVisibility(View.GONE);
        vi_devider.setVisibility(View.GONE);

        edt_name.setTypeface(setFont(this, R.string.app_regular));
        edt_name.setText(tv_nic_number.getText().toString().trim());
        edt_name.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(12)});

        alertDialog.show();

        // cancel click event
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                alertDialog.dismiss();
            }
        });

        // change buuton click event
        tv_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidated(true)) {
                    nicnum = edt_name.getText().toString().trim();
                    tv_nic_number.setText(nicnum);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    alertDialog.dismiss();
                }

            }
        });
    }

    private Context getContext() {
        // return the context of RegistrationActivity_3 screen
        return RegistrationActivity_3.this;
    }

    // check validation for nic

    /**
     * @param isShowError boolean for showing error
     * @return true or false
     */
    public boolean isValidated(boolean isShowError) {
        boolean isValid = true;

        if (!UtilsValidation.isValidNICMain(getContext(), edt_name, isShowError)) {
            isValid = false;
        }

        return isValid;
    }

}
