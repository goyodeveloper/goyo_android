package com.differenzsystem.goyog.userProfile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;


public class RegisterHeight extends AppCompatActivity {

    EditText et_ft, et_in, et_cm;
    TextView tv_popup_title, tv_done, tv_title;
    RadioGroup rgb_selection;
    RadioButton rb_ft_in, rb_ct;
    LinearLayout lin_ft_in, lin_cm;

    boolean isCm = false;
    boolean isPnd = false;

    boolean isUpdateCall = false;

    String str_ft_in = "", str_cm = "", str_kg = "", str_lb = "";
    String result = "";
    String result_value = "";

    int PICK_HEIGHT_REQUEST = 21;
    Bundle extra;

    LinearLayout ll_back, lin_main;
    RelativeLayout rel_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_height);
        getSupportActionBar().hide();

        // initialize all controls define in xml layout
        rel_main = (RelativeLayout) findViewById(R.id.rel_main);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        lin_main = (LinearLayout) findViewById(R.id.lin_main);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_popup_title = (TextView) findViewById(R.id.tv_popup_title);
        et_ft = (EditText) findViewById(R.id.et_ft);
        et_in = (EditText) findViewById(R.id.et_in);
        et_cm = (EditText) findViewById(R.id.et_cm);
        tv_done = (TextView) findViewById(R.id.tv_done);
        rgb_selection = (RadioGroup) findViewById(R.id.rgb_selection);
        rb_ft_in = (RadioButton) findViewById(R.id.rb_ft_in);
        rb_ct = (RadioButton) findViewById(R.id.rb_ct);

        lin_ft_in = (LinearLayout) findViewById(R.id.lin_ft_in);
        lin_cm = (LinearLayout) findViewById(R.id.lin_cm);

        et_in.setFilters(new InputFilter[]{new InputFilterMinMax("0", "11")}); // here 0 is min and 11 is max value for inch

        tv_popup_title.setTypeface(setFont(getContext(), R.string.app_bold));//

        tv_done.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_ft.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_in.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_cm.setTypeface(setFont(getContext(), R.string.app_regular));//

        tv_title.setText("");
        extra = getIntent().getExtras();
        if (extra == null)
            return;
        if (extra.containsKey("iscm") && extra.containsKey("result_value")) {
            isCm = extra.getBoolean("iscm");
            if (isCm) {
                str_cm = extra.getString("result_value");
            } else {
                str_ft_in = extra.getString("result_value");
            }
        }

        tv_popup_title.setText(getString(R.string.title_enter_height));

        if (isCm) {
            rb_ct.setChecked(true);
            lin_ft_in.setVisibility(View.GONE);
            lin_cm.setVisibility(View.VISIBLE);
            if (!str_cm.isEmpty()) {
                et_cm.setText(str_cm);
            }
        } else {
            rb_ft_in.setChecked(true);
            lin_cm.setVisibility(View.GONE);
            lin_ft_in.setVisibility(View.VISIBLE);
            if (!str_ft_in.isEmpty()) {
                if (str_ft_in.contains("-")) {
                    String[] str_arr = str_ft_in.split("-");
                    et_ft.setText(str_arr[0]);
                    et_in.setText(str_arr[1]);
                }
            }
        }

        rgb_selection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                if (checkedId == R.id.rb_ft_in) {
                    lin_cm.setVisibility(View.GONE);
                    lin_ft_in.setVisibility(View.VISIBLE);
                    if (!et_cm.getText().toString().isEmpty()) {
                        String[] str_arr = centimeterToFeet(et_cm.getText().toString()).split("-");
                        et_ft.setText(String.valueOf(str_arr[0]));
                        et_in.setText(String.valueOf(str_arr[1]));
                    } else {
                        et_ft.setText("");
                        et_in.setText("");
                    }
                } else {
                    String val = "";
                    if (!et_ft.getText().toString().isEmpty()) {

                        if (et_in.getText().toString().isEmpty()) {
                            et_in.setText("0");
                        }
                        val = et_ft.getText().toString() + "-" + et_in.getText().toString();
                    } else {
                        val = "";
                    }

                    lin_ft_in.setVisibility(View.GONE);
                    lin_cm.setVisibility(View.VISIBLE);
                    if (!val.isEmpty()) {
                        if (val.contains("-")) {
                            et_cm.setText(feetToCentimeter(val));
                        }
                    } else {
                        et_cm.setText("");
                    }
                }
            }
        });

        // done or next button click event
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (rb_ft_in.isChecked()) {
                    String ft = et_ft.getText().toString();
                    String in = et_in.getText().toString();
                    if (!ft.isEmpty()) {
                        isCm = false;
                        if (in.isEmpty())
                            in = "0";
                        result = (ft + " ft" + " - " + in + " in");
                        str_ft_in = ft + "-" + in;
                        result_value = ft + "-" + in;
                        intentHeightWeight(isCm, result, result_value);

                    }
                } else {
                    if (!et_cm.getText().toString().isEmpty()) {
                        isCm = true;
                        str_cm = et_cm.getText().toString();
                        result = (str_cm + " cm");
                        result_value = et_cm.getText().toString();
                        intentHeightWeight(isCm, result, result_value);
                    }
                }


            }
        });

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        rel_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        });

        lin_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegisterHeight.this, Constant.Name_Register_Height);
    }

    // open register height weight screen
    private void intentHeightWeight(boolean flag_cm, String res, String res_val) {
        Intent intent = new Intent();
        intent.putExtra("iscm", flag_cm);
        intent.putExtra("result", res);
        intent.putExtra("result_value", res_val);
        setResult(RESULT_OK, intent);
        finish();//finishing activity
    }

    private Context getContext() {
        // return the RegisterHeight context
        return RegisterHeight.this;
    }

    // convert cm to feet
    public static String centimeterToFeet(String centemeter) {
        int feetPart = 0;
        int inchesPart = 0;
        if (!TextUtils.isEmpty(centemeter)) {
            double dCentimeter = Double.valueOf(centemeter);
            feetPart = (int) Math.floor((dCentimeter / 2.54) / 12);
            System.out.println((dCentimeter / 2.54) - (feetPart * 12));
            inchesPart = (int) Math.ceil((dCentimeter / 2.54) - (feetPart * 12));

            // added for converting 5-12 to 6-0
            if (inchesPart == 12) {
                inchesPart = 0;
                feetPart = feetPart + 1;
            }
        }
        return String.valueOf(feetPart) + "-" + String.valueOf(inchesPart);
    }

    // convert feet to cm
    public static String feetToCentimeter(String feet) {
        double dCentimeter = 0d;
        if (!TextUtils.isEmpty(feet)) {
            if (feet.contains("-")) {
                String[] str_arr = feet.split("-");
                String tempfeet = str_arr[0];
                String tempinch = str_arr[1];
                if (!TextUtils.isEmpty(tempfeet)) {
                    dCentimeter += ((Double.valueOf(tempfeet)) * 30.48);
                }
                if (!TextUtils.isEmpty(tempinch)) {
                    dCentimeter += ((Double.valueOf(tempinch)) * 2.54);
                }
            }
        }
        return String.valueOf((int) dCentimeter);
        //Format to decimal digit as per your requirement
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}
