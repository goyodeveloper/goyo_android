package com.differenzsystem.goyog.userProfile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.UtilsCommon;

public class TermConditionWebViewActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout ll_back;
    TextView tv_title;

    private WebView webView;
    String From;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.term_condition_web_view_activity);
        getSupportActionBar().hide();
        initControls();
        initControlAction();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(TermConditionWebViewActivity.this, Constant.Name_Terms_Condition);
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            // Let the system handle the back button
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }
    }

    // initialize all controls define in xml layout
    public void initControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            //tv_title.setText("Termes and Contition");

            webView = (WebView) findViewById(R.id.webView);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // set click event listener
    public void initControlAction() {
        try {
            ll_back.setOnClickListener(this);
            //startWebView("http://www.google.com");
            if (getIntent() != null && getIntent().getExtras() != null) {
                From = getIntent().getStringExtra("From");
            }

            if (From != null && From.length() > 0) {
                if (From.equalsIgnoreCase("TermsofUse")) {
                    startWebView("http://www.goyo.lk/terms");
                    tv_title.setText(R.string.tnc);
                } else if (From.equalsIgnoreCase("Support")) {
                    startWebView("http://www.goyo.lk/support.php");
                    tv_title.setText(R.string.support);
                } else {
                    startWebView("http://www.goyo.lk/privacy.php");
                    tv_title.setText(R.string.privacy_policy);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click listener
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;

            default:
                break;
        }
    }

    private void startWebView(String url) {
        webView.setWebViewClient(new WebViewClient() {
            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {

            }

            public void onPageFinished(WebView view, String url) {
                try {
                    UtilsCommon.destroyProgressBar();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

        });
        UtilsCommon.showProgressDialog(TermConditionWebViewActivity.this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
}
