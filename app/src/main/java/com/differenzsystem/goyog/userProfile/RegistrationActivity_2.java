package com.differenzsystem.goyog.userProfile;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.differenzsystem.goyog.utility.UtilsValidation;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

public class RegistrationActivity_2 extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    String[] SpArrGender;
    ArrayAdapter<String> GenderAdapter;

    String[] SpArrMeritalStatus;
    ArrayAdapter<String> MeritalStatusAdapter;

    String[] SpArrOccupation;
    ArrayAdapter<String> OccupationAdapter;


    Spinner sp_marital_status, sp_gender, sp_occupation;
    EditText et_nic;
    Button btn_next;
    LinearLayout ll_back;
    TextView tv_title;
    RelativeLayout rl_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity_2);

        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegistrationActivity_2.this, Constant.Name_Registration_Step2);
    }

//sp_age.getSelectedItemPosition() == 0)

    // initialize all controls define in xml layout
    void initializeControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(R.string.reg_title2);

            sp_marital_status = (Spinner) findViewById(R.id.sp_marital_status);
            sp_occupation = (Spinner) findViewById(R.id.sp_occupation);
            sp_gender = (Spinner) findViewById(R.id.sp_gender);
            et_nic = (EditText) findViewById(R.id.et_nic);
            btn_next = (Button) findViewById(R.id.btn_next);
            rl_main = (RelativeLayout) findViewById(R.id.rl_main);

            btn_next.setTypeface(setFont(getContext(), R.string.app_bold));
            et_nic.setTypeface(setFont(getContext(), R.string.app_regular));

            setGenderSpinner();
            setMeritalStatusSpinner();
            setOccupationSpinner();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // set click listener
    public void initializeControlsAction() {
        try {
            btn_next.setOnClickListener(this);
            ll_back.setOnClickListener(this);
            sp_marital_status.setOnTouchListener(this);
            sp_gender.setOnTouchListener(this);
            sp_occupation.setOnTouchListener(this);
            rl_main.setOnClickListener(this);
            et_nic.setFilters(new InputFilter[]{new InputFilterMinMax.AllCaps(),new InputFilter.LengthFilter(12)});

            if (LoginActivity.SocialDetail != null && LoginActivity.SocialDetail.size() > 0) {
                String gender = LoginActivity.SocialDetail.get(Constant.gender);
                if (gender != null && gender.length() > 0)
                    sp_gender.setSelection(Integer.parseInt(gender));
            }else {
                sp_gender.setSelection(0);
            }

            sp_marital_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 1) {
                        UtilsCommon.openEditDialog(getContext());
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {}
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                if (isValidated(true)) {
                    storeData();
                }
                break;
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.rl_main:
                hidError();
                break;

        }
    }

    // hide error
    public void hidError() {
//        et_other_name.setError(null);
//        et_address.setError(null);
//        et_contact_number.setError(null);
        UtilsCommon.hideSoftKeyboard(this);

        et_nic.setFocusable(false);
        et_nic.setFocusableInTouchMode(false);
        et_nic.setFocusable(true);
        et_nic.setFocusableInTouchMode(true);
    }

    // save occupation, marital status, gender and nic in shared prefrences
    public void storeData() {
//        RegistrationActivity.RegDetail.put("Occupation", sp_occupation.getSelectedItem().toString().trim());
//        RegistrationActivity.RegDetail.put("Gender", sp_gender.getSelectedItem().toString().trim());
//        RegistrationActivity.RegDetail.put("MaritalStatus", sp_marital_status.getSelectedItem().toString().trim());


//        RegistrationActivity.RegDetail.put("NIC", et_nic.getText().toString().trim());
//        RegistrationActivity.RegDetail.put("Occupation", String.valueOf(sp_occupation.getSelectedItemId()));
//        RegistrationActivity.RegDetail.put("Gender", String.valueOf(sp_gender.getSelectedItemId()));
//        RegistrationActivity.RegDetail.put("MaritalStatus", String.valueOf(sp_marital_status.getSelectedItemId()));
        UtilsPreferences.setString(getContext(), Constant.occupation, String.valueOf(sp_occupation.getSelectedItemId()));
        UtilsPreferences.setString(getContext(), Constant.marital_status, String.valueOf(sp_marital_status.getSelectedItemId()));
        UtilsPreferences.setString(getContext(), Constant.gender, String.valueOf(sp_gender.getSelectedItemId()));
        UtilsPreferences.setString(getContext(), Constant.nic, et_nic.getText().toString().trim());

        // open next registration screen
        startActivity(new Intent(this, RegistrationActivity_3.class));
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // handle dropdown click event
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (v.getId()) {
            case R.id.sp_marital_status:
                UtilsCommon.hideSoftKeyboard(this);
                break;
            case R.id.sp_gender:
                UtilsCommon.hideSoftKeyboard(this);
                break;
            case R.id.sp_occupation:
                UtilsCommon.hideSoftKeyboard(this);
                break;
        }
        return false;
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    // set data in dropdown for gender
    public void setGenderSpinner() {
        SpArrGender = new String[]{"Gender", "Male", "Female"};
        GenderAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, SpArrGender) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTypeface(setFont(getContext(), R.string.app_regular));
                if (position == 0) {
                    // tv.setTextColor(getResources().getColor(R.color.gray));
                } else {
                    //  tv.setTextColor(getResources().getColor(R.color.darker_gray_text));
                }
                return view;
            }

            public TextView getView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(setFont(getContext(), R.string.app_regular));
                return v;
            }
        };
        sp_gender.setAdapter(GenderAdapter);
    }

    // set data in dropdown for marital status
    public void setMeritalStatusSpinner() {
        SpArrMeritalStatus = new String[]{"Marital status", "Married", "Unmarried"};
        MeritalStatusAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, SpArrMeritalStatus) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTypeface(setFont(getContext(), R.string.app_regular));
                if (position == 0) {
                    // tv.setTextColor(getResources().getColor(R.color.gray));
                } else {
                    //  tv.setTextColor(getResources().getColor(R.color.darker_gray_text));
                }
                return view;
            }

            public TextView getView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(setFont(getContext(), R.string.app_regular));
                return v;
            }
        };
        sp_marital_status.setAdapter(MeritalStatusAdapter);
    }

    // set data in dropdown for occupation
    public void setOccupationSpinner() {
        SpArrOccupation = new String[]
                {"Occupation", "Business person", "Executive",
                        "Forces", "Government", "Legal", "Manager",
                        "Officer", "Schooling", "Self Employed",
                        "Snr. Management", "University", "Other"};
        OccupationAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, SpArrOccupation) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {

                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTypeface(setFont(getContext(), R.string.app_regular));
                if (position == 0) {
                    // tv.setTextColor(getResources().getColor(R.color.gray));
                } else {
                    //  tv.setTextColor(getResources().getColor(R.color.darker_gray_text));
                }
                return view;
            }

            public TextView getView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(setFont(getContext(), R.string.app_regular));
                return v;
            }
        };
        sp_occupation.setAdapter(OccupationAdapter);
    }

    private Context getContext() {
        // return the context of RegistrationActivity_2 screen
        return RegistrationActivity_2.this;
    }

    // check validation for marital status, gender, occupation, nic

    /**
     *
     * @param isShowError boolean for showing error
     * @return true or false
     */
    public boolean isValidated(boolean isShowError) {
        boolean isValid = true;
        if (sp_marital_status.getSelectedItemPosition() == 0) {
            Toast.makeText(RegistrationActivity_2.this, getResources().getString(R.string.err_msg_please_select_merital_status), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (sp_gender.getSelectedItemPosition() == 0) {
            Toast.makeText(RegistrationActivity_2.this, getResources().getString(R.string.err_msg_please_select_gender), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (sp_occupation.getSelectedItemPosition() == 0) {
            Toast.makeText(RegistrationActivity_2.this, getResources().getString(R.string.err_msg_please_select_occupation), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (!UtilsValidation.isValidNICMain(getContext(), et_nic, isShowError)) {
            isValid = false;
        }

        return isValid;
    }

}

