package com.differenzsystem.goyog.userProfile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.adapter.TutorialPageAdapter;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.HintModel;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class TutorialActivity extends AppCompatActivity implements View.OnClickListener {

    ViewPager viewPager;
    CirclePageIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_activity);
        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(TutorialActivity.this, Constant.Name_Tutorial);
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private Context getContext() {
        // return context of tutorial screen
        return TutorialActivity.this;
    }

    // initialize all controls define in xml layout
    public void initializeControls() {
        try {
            viewPager = (ViewPager) findViewById(R.id.viewPager);
            indicator = (CirclePageIndicator) findViewById(R.id.indicator);
            indicator.setStrokeWidth(2);
            indicator.setStrokeColor(getResources().getColor(R.color.pager_selected));

            ArrayList<HintModel> imageList = new ArrayList<>();
            //OLD message
            /*imageList.add(new HintModel("Tracker", "Connect any existing fitness band any you're set", R.drawable.tutorial1));
            imageList.add(new HintModel("Fun", "Get healthy any enjoy yourself at the same time", R.drawable.tutorial2));
            imageList.add(new HintModel("Win", "Great prizes and discount from GOYOG partners", R.drawable.tutorial3));*/

            imageList.add(new HintModel("Tracker", "Connect a GOYO fitness band, and you’re set", R.drawable.tutorial1));
            imageList.add(new HintModel("Fun", "Get healthy and enjoy yourself at the same time", R.drawable.tutorial2));
            imageList.add(new HintModel("Win", "Get prizes and discounts from GOYO partners", R.drawable.tutorial3));

            viewPager.setAdapter(new TutorialPageAdapter(getContext(), imageList));
            indicator.setViewPager(viewPager);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initializeControlsAction() {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {

            default:
                break;
        }
    }
}
