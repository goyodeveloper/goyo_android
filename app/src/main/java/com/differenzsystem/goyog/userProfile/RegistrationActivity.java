package com.differenzsystem.goyog.userProfile;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.CheckEmail;
import com.differenzsystem.goyog.api.RegisterCall;
import com.differenzsystem.goyog.api.RegisterCall.OnRegisterUserListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.model.RegisterModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.differenzsystem.goyog.utility.UtilsValidation;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;


public class RegistrationActivity extends AppCompatActivity implements OnClickListener, OnRegisterUserListener, OnCheckedChangeListener, TextWatcher, CheckEmail.OnCheckEmailListener {
    EditText et_fname, et_lname, et_email, et_password;
    TextView tv_title, tv_term_condition;
    CheckBox chk_term_condition;
    LinearLayout ll_back;
    RelativeLayout rl_main;
    Button btn_register;
    OnRegisterUserListener onRegisterUserListener;
    View v_linepwd;
    final static HashMap<String, String> RegDetail = new HashMap<String, String>();
    String isFrom;
    CheckEmail.OnCheckEmailListener onCheckEmailListener;

//    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity);
        getSupportActionBar().hide();

//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        if (getIntent() != null && getIntent().hasExtra("isFrom")) {
            Intent i = getIntent();
            isFrom = i.getStringExtra("isFrom");
        }

        initializeControls();
        initializeControlsAction();

        if (isFrom != null && isFrom.length() > 0) {
            if (isFrom.equalsIgnoreCase("FB")) {
                et_password.setVisibility(View.INVISIBLE);
                v_linepwd.setVisibility(View.INVISIBLE);
            }
        } else {
            UtilsPreferences.setString(getContext(), Constant.facebook_id, "");
        }
    }

    private Context getContext() {
        // return the Registration context
        return RegistrationActivity.this;
    }

    // initialize all controls define in xml layout
    void initializeControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(R.string.reg_title0);

            btn_register = (Button) findViewById(R.id.btn_register);
            et_email = (EditText) findViewById(R.id.et_email);
            et_password = (EditText) findViewById(R.id.et_password);
            et_fname = (EditText) findViewById(R.id.et_fname);
            et_lname = (EditText) findViewById(R.id.et_lname);
            tv_term_condition = (TextView) findViewById(R.id.tv_term_condition);
            chk_term_condition = (CheckBox) findViewById(R.id.chk_term_condition);
            v_linepwd = (View) findViewById(R.id.v_linepwd);

            rl_main = (RelativeLayout) findViewById(R.id.rl_main);

            btn_register.setTypeface(setFont(getContext(), R.string.app_bold));

            et_email.setTypeface(setFont(getContext(), R.string.app_regular));
            et_password.setTypeface(setFont(getContext(), R.string.app_regular));
            et_lname.setTypeface(setFont(getContext(), R.string.app_regular));
            et_fname.setTypeface(setFont(getContext(), R.string.app_regular));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            onRegisterUserListener = this;
            onCheckEmailListener = this;

            btn_register.setOnClickListener(this);
            ll_back.setOnClickListener(this);
            et_lname.addTextChangedListener(this);
            et_fname.addTextChangedListener(this);
            et_password.addTextChangedListener(this);
            et_email.addTextChangedListener(this);
            tv_term_condition.setOnClickListener(this);
            rl_main.setOnClickListener(this);
            setTermsnPrivacyClick();
            //chk_term_condition.setOnCheckedChangeListener(this);
            if (!isFrom.isEmpty())
                setDatafromFB();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set term privacy click
    private void setTermsnPrivacyClick() {
        SpannableString ss = new SpannableString("I agree to the GOYO Terms & Conditions and Privacy Policy");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.invalidate();
                Intent i = new Intent(getApplicationContext(), TermConditionWebViewActivity.class);
                i.putExtra("From", "TermsofUse");
                startActivity(i);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);

                ds.setFakeBoldText(true);
                ds.setUnderlineText(true);
                ds.setColor(getResources().getColor(R.color.privacy));
            }
        };

        // open TermConditionWebView screen
        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.invalidate();
                Intent i = new Intent(getApplicationContext(), TermConditionWebViewActivity.class);
                i.putExtra("From", "PrivacyPolicy");
                startActivity(i);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setFakeBoldText(true);
                ds.setUnderlineText(true);
                ds.setColor(getResources().getColor(R.color.privacy));
            }
        };
        ss.setSpan(clickableSpan, 20, 39, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span2, 43, 57, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_term_condition.setText(ss);
        tv_term_condition.setMovementMethod(LinkMovementMethod.getInstance());
    }

    // set the data which get from facebook
    private void setDatafromFB() {

        if (LoginActivity.SocialDetail != null && LoginActivity.SocialDetail.size() > 0) {
            String fn = LoginActivity.SocialDetail.get(Constant.first_name);
            String ln = LoginActivity.SocialDetail.get(Constant.last_name);
            String email = LoginActivity.SocialDetail.get(Constant.email);

            if (email != null && email.length() > 0) {
                et_email.setText(email);
            }
            if (fn != null && fn.length() > 0) {
                et_fname.setText(fn);
            }
            if (ln != null && ln.length() > 0) {
                et_lname.setText(ln);
            }
        }
    }

    // handle click event
    @RequiresApi(api = VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                /*if (isValidated(true)) {
                    if (chk_term_condition.isChecked())
                        doRegistration();
                    else
                        Toast.makeText(RegistrationActivity.this, getResources().getString(R.string.err_msg_not_accept_term_condition), Toast.LENGTH_SHORT).show();
                }*/

                if (isValidated(true)) {
                    CheckEmailAddress();
                    /*if (chk_term_condition.isChecked()) {
                        storeData();
                    } else {
                        Toast.makeText(RegistrationActivity.this, getResources().getString(R.string.err_msg_not_accept_term_condition), Toast.LENGTH_SHORT).show();
                    }*/
                }

                break;
            case R.id.ll_back:
                openLoginScreen();
                break;
            case R.id.rl_main:
                hidError();
                break;
//            case R.id.tv_term_condition:
//                startActivity(new Intent(getContext(), TermConditionWebViewActivity.class));
//                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
//                break;
        }

    }

    // hide the error text
    public void hidError() {
        UtilsCommon.hideSoftKeyboard(this);

        et_fname.setFocusable(false);
        et_fname.setFocusableInTouchMode(false);
        et_lname.setFocusable(false);
        et_lname.setFocusableInTouchMode(false);
        et_email.setFocusable(false);
        et_email.setFocusableInTouchMode(false);
        et_password.setFocusable(false);
        et_password.setFocusableInTouchMode(false);

        et_fname.setFocusable(true);
        et_fname.setFocusableInTouchMode(true);
        et_lname.setFocusable(true);
        et_lname.setFocusableInTouchMode(true);
        et_email.setFocusable(true);
        et_email.setFocusableInTouchMode(true);
        et_password.setFocusable(true);
        et_password.setFocusableInTouchMode(true);
    }

    // save the name, email details in shared prefrences
    public void storeData() {
        UtilsPreferences.setString(getContext(), Constant.first_name, et_fname.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.last_name, et_lname.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.emailreg, et_email.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.password, et_password.getText().toString().trim());

//        RegDetail.put("FN", et_fname.getText().toString().trim());
//        RegDetail.put("LN", et_lname.getText().toString().trim());
//        RegDetail.put("email", et_email.getText().toString().trim());
//        RegDetail.put("pwd", et_password.getText().toString().trim());
        startActivity(new Intent(getContext(), RegistrationSelectionActivity.class));
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    // check validation of first name, last name, email

    /**
     * @param isShowError boolean for showing error
     * @return
     */
    public boolean isValidated(boolean isShowError) {
        boolean isValid = true;
        if (!UtilsValidation.isValidFname(getContext(), et_fname, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidLname(getContext(), et_lname, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidEmail(getContext(), et_email, isShowError)) {
            isValid = false;
        }

        if (isFrom != null && isFrom.length() > 0) {

        } else {
            if (!UtilsValidation.isValidPassword(getContext(), et_password, isShowError, true)) {
                isValid = false;
            }
        }

        return isValid;
    }

    public void doRegistration() {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.email, et_email.getText().toString().trim());
            data.put(Constant.password, et_password.getText().toString().trim());
            data.put(Constant.full_name, et_lname.getText().toString().trim());
            data.put(Constant.device_token, UtilsPreferences.getString(RegistrationActivity.this, Constant.devicegcmid));
            data.put(Constant.device_type, Constant.device_type_value);

            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            data.put(Constant.timezone, tz.getID());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateandTime = sdf.format(new Date());
            data.put(Constant.regdate, currentDateandTime);

            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                RegisterCall reg = new RegisterCall(RegistrationActivity.this, onRegisterUserListener, data, true);
                reg.execute();
            } else {
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // open login screen
    public void openLoginScreen() {
        UtilsCommon.hideSoftKeyboard(RegistrationActivity.this);
        Intent i_login = new Intent(getContext(), LoginActivity.class);
        startActivity(i_login);
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        finish();
    }

    // success listener of RegisterCall and save the response data in shared prefrences
    @RequiresApi(api = VERSION_CODES.JELLY_BEAN)
    @Override
    public void onSucceedToRegister(RegisterModel obj) {
        if (obj != null) {
            UtilsPreferences.setString(RegistrationActivity.this, Constant.email, obj.getEmail());
            UtilsPreferences.setString(RegistrationActivity.this, Constant.full_name, obj.getFull_name());
            UtilsPreferences.setString(RegistrationActivity.this, Constant.user_id, obj.getUser_id());
            UtilsPreferences.setString(RegistrationActivity.this, Constant.accesstoken, obj.getAccesstoken());
            UtilsPreferences.setString(RegistrationActivity.this, Constant.registration_date, obj.getRegistration_date());
            UtilsPreferences.setString(RegistrationActivity.this, Constant.last_update_date, obj.getRegistration_date());

            if (!obj.getDevice_token().equalsIgnoreCase("")) {
                UtilsPreferences.setBoolean(RegistrationActivity.this, Constant.isdevicegcmidstore, true);
            }

            startActivity(new Intent(getContext(), GetStartedActivity.class));
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            finishAffinity();
        }
    }

    // failed listener of RegisterCall
    @Override
    public void onFaildToRegister(String error_msg) {
        Toast.makeText(RegistrationActivity.this, error_msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    // after text changed event listener
    @RequiresApi(api = VERSION_CODES.JELLY_BEAN)
    @Override
    public void afterTextChanged(Editable editable) {
        ChangeButtonColor();
    }

    // on check changed event listener
    @RequiresApi(api = VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        ChangeButtonColor();
    }

    @RequiresApi(api = VERSION_CODES.JELLY_BEAN)
    public void ChangeButtonColor() {
        /*if (isValidated(false) && chk_term_condition.isChecked()) {
            btn_register.setBackground(getResources().getDrawable(R.drawable.btn_filled_solid_rounded));
        } else {
            btn_register.setBackground(getResources().getDrawable(R.drawable.btn_not_filled_solid_rounded));
        }*/
    }

    // api call of checkemail
    public void CheckEmailAddress() {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.email, et_email.getText().toString().trim());

            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                CheckEmail reg = new CheckEmail(RegistrationActivity.this, onCheckEmailListener, data, true);
                reg.execute();
            } else {
                // Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of CheckEmail
    @Override
    public void onSucceedCheckEmail(String message) {
        storeData();
    }

    // failed listener of CheckEmail
    @Override
    public void onFailedCheckEmaild(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegistrationActivity.this, Constant.Name_Register);
    }

    private void recordScreenViews() {
        // [START set_current_screen]
//        mFirebaseAnalytics.setCurrentScreen(this, "Register Screen", null /* class override */);
        // [END set_current_screen]
    }
}
