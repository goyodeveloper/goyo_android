package com.differenzsystem.goyog.userProfile;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.differenzsystem.goyog.utility.UtilsValidation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

public class RegistrationActivity_1 extends AppCompatActivity implements View.OnClickListener {
    LinearLayout ll_back;
    EditText et_other_name, et_address, et_contact_number;
    TextView tv_title;
    TextView et_birth_date;
    Button btn_next;
    Calendar myCalendar;
    String DOB;
    RelativeLayout rl_main;
    DatePickerDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity_1);
        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegistrationActivity_1.this, Constant.Name_Registration_Step1);
    }

    // initialize all controls define in xml layout
    void initializeControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(R.string.reg_title1);

            btn_next = (Button) findViewById(R.id.btn_next);
            et_other_name = (EditText) findViewById(R.id.et_other_name);
            et_address = (EditText) findViewById(R.id.et_address);
            et_contact_number = (EditText) findViewById(R.id.et_contact_number);
            et_birth_date = (TextView) findViewById(R.id.et_birth_date);
            rl_main = (RelativeLayout) findViewById(R.id.rl_main);


            btn_next.setTypeface(setFont(getContext(), R.string.app_bold));
            et_other_name.setTypeface(setFont(getContext(), R.string.app_regular));
            et_address.setTypeface(setFont(getContext(), R.string.app_regular));
            et_contact_number.setTypeface(setFont(getContext(), R.string.app_regular));
            et_birth_date.setTypeface(setFont(getContext(), R.string.app_regular));

            myCalendar = Calendar.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // set click event listener
    public void initializeControlsAction() {
        try {
            btn_next.setOnClickListener(this);
            ll_back.setOnClickListener(this);
            et_birth_date.setOnClickListener(this);
            rl_main.setOnClickListener(this);
            initCalander();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // date picker dialog
    public void initCalander() {

        dialog = new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, date,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        myCalendar.set(myCalendar.get(Calendar.YEAR) - 18,
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());
    }

    // handle click event
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                if (isValidated(true)) {
                    if (et_birth_date.getText().toString().length() > 0) {
                        storeData();
                    } else {
                        Toast.makeText(RegistrationActivity_1.this, getResources().getString(R.string.err_msg_please_selsct_dob), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.et_birth_date:
                UtilsCommon.hideSoftKeyboard(this);
                dialog.show();
                /*DialogFragment dialogfragment = new DatePickerDialogTheme2();

                dialogfragment.show(getFragmentManager(), "Theme 2");*/
                break;
            case R.id.rl_main:
                hidError();
                break;
        }
    }

    // hide error
    public void hidError() {
//        et_other_name.setError(null);
//        et_address.setError(null);
//        et_contact_number.setError(null);
        UtilsCommon.hideSoftKeyboard(this);

        et_address.setFocusable(false);
        et_address.setFocusableInTouchMode(false);
        et_contact_number.setFocusable(false);
        et_contact_number.setFocusableInTouchMode(false);

        et_address.setFocusable(true);
        et_address.setFocusableInTouchMode(true);
        et_contact_number.setFocusable(true);
        et_contact_number.setFocusableInTouchMode(true);

    }

    // save other name, address, contact no, dob in shared prefrences
    public void storeData() {

//        RegistrationActivity.RegDetail.put("OthName", et_other_name.getText().toString().trim());
//        RegistrationActivity.RegDetail.put("Address", et_address.getText().toString().trim());
//        RegistrationActivity.RegDetail.put("ContactNo", et_contact_number.getText().toString().trim());
//        RegistrationActivity.RegDetail.put("DOB", et_birth_date.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.other_names, et_other_name.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.address, et_address.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.contact_number, et_contact_number.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.dob, et_birth_date.getText().toString().trim());

        // open next registration screen
        startActivity(new Intent(this, RegistrationActivity_2.class));
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);

    }

    // date picker dailog click event
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            enddateLabel();
        }
    };

    // set date in yyyy-MM-dd format which get from date picker
    private void enddateLabel() {
        String myFormatDisplay = "yyyy-MM-dd";// "MMM dd, yyyy";//In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormatDisplay, Locale.US);
        DOB = sdf.format(myCalendar.getTime());
        et_birth_date.setText(DOB);
    }

    private Context getContext() {
        // return context of RegistrationActivity_1 screen
        return RegistrationActivity_1.this;
    }

    // check validation of address, contact no

    /**
     *
     * @param isShowError boolean for showing error
     * @return
     */
    public boolean isValidated(boolean isShowError) {
        boolean isValid = true;

        if (!UtilsValidation.isValidAddress(getContext(), et_address, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidContactNo(getContext(), et_contact_number, isShowError)) {
            isValid = false;
        }

        return isValid;
    }


}
