package com.differenzsystem.goyog.userProfile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.ScanDeviceActivity;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.RegisterCall;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.model.RegisterModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

public class RegistrationSelectionActivity extends AppCompatActivity implements View.OnClickListener, RegisterCall.OnRegisterUserListener {

    Button btn_demoapp, btn_getwatch, btn_conntgoyo;
    TextView tv_title;
    LinearLayout ll_back;
    RegisterCall.OnRegisterUserListener onRegisterUserListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_selection_activity);
        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegistrationSelectionActivity.this, Constant.Name_Registration_Selection);
    }

    // initialize all controls define in xml layout
    public void initializeControls() {
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(R.string.reg_selection);

        btn_demoapp = (Button) findViewById(R.id.btn_demoapp);
        btn_getwatch = (Button) findViewById(R.id.btn_getwatch);
        btn_conntgoyo = (Button) findViewById(R.id.btn_conntgoyo);

        btn_demoapp.setTypeface(setFont(getContext(), R.string.app_bold));
        btn_getwatch.setTypeface(setFont(getContext(), R.string.app_bold));
        btn_conntgoyo.setTypeface(setFont(getContext(), R.string.app_bold));
    }

    // set click listener
    public void initializeControlsAction() {
        onRegisterUserListener = this;

        ll_back.setOnClickListener(this);
        btn_demoapp.setOnClickListener(this);
        btn_getwatch.setOnClickListener(this);
        btn_conntgoyo.setOnClickListener(this);
    }

    private Context getContext() {
        // return context of RegistrationSelectionActivity screen
        return RegistrationSelectionActivity.this;
    }

    // handle click event
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_conntgoyo:
                Intent i = new Intent(getContext(), ScanDeviceActivity.class);//RegistrationActivity_1
                i.putExtra("isFrom", "RegistrationSelectionActivity");//ScanDeviceActivity
                startActivity(i);
                //startActivity(new Intent(getContext(), RegistrationActivity_1.class));//ScanDeviceActivity
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                break;
            case R.id.btn_getwatch:
                buyWatch();
                break;
            case R.id.btn_demoapp:
                doRegistration();
                break;
            case R.id.ll_back:
                onBackPressed();
                break;
        }
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    // open goyo url screen
    public void buyWatch() {
        String url = "http://www.goyo.lk/";

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    // api call for register
    public void doRegistration() {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.email, UtilsPreferences.getString(getContext(), Constant.emailreg));

            String pwd = UtilsPreferences.getString(getContext(), Constant.password);
            if (pwd != null && pwd.length() > 0) {
                data.put(Constant.password, UtilsPreferences.getString(getContext(), Constant.password));
            } else {
                data.put(Constant.password, "0");
            }
            //data.put(Constant.password, UtilsPreferences.getString(getContext(), Constant.password));
            data.put(Constant.first_name, UtilsPreferences.getString(getContext(), Constant.first_name));
            data.put(Constant.last_name, UtilsPreferences.getString(getContext(), Constant.last_name));

            data.put(Constant.user_type, "2");
            data.put(Constant.connect_flag, Constant.connected_with_tracker);//for demo user pass this as static 3 for connection flag

            data.put(Constant.device_token, UtilsPreferences.getString(RegistrationSelectionActivity.this, Constant.devicegcmid));
            data.put(Constant.device_type, Constant.device_type_value);
            data.put(Constant.facebook_id, UtilsPreferences.getString(getContext(), Constant.facebook_id));

            /*data.put(Constant.fb_education, UtilsPreferences.getString(getContext(), Constant.fb_education));
            data.put(Constant.fb_groups, UtilsPreferences.getString(getContext(), Constant.fb_groups));
            data.put(Constant.fb_events, UtilsPreferences.getString(getContext(), Constant.fb_events));
            data.put(Constant.fb_work_exp, UtilsPreferences.getString(getContext(), Constant.fb_work_exp));*/

            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            data.put(Constant.timezone, tz.getID());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateandTime = sdf.format(new Date());
            data.put(Constant.regdate, currentDateandTime);

            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                RegisterCall reg = new RegisterCall(RegistrationSelectionActivity.this, onRegisterUserListener, data, true);
                reg.execute();
            } else {
                //  Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of RegisterCall and save the response data in shared prefrences
    @Override
    public void onSucceedToRegister(RegisterModel obj) {
        if (obj != null) {
            UtilsPreferences.setString(RegistrationSelectionActivity.this, Constant.email, obj.getEmail());
            UtilsPreferences.setString(RegistrationSelectionActivity.this, Constant.full_name, obj.getFirst_name() + " " + obj.getLast_name());
            UtilsPreferences.setString(RegistrationSelectionActivity.this, Constant.user_id, obj.getUser_id());
            UtilsPreferences.setString(RegistrationSelectionActivity.this, Constant.accesstoken, obj.getAccesstoken());
            UtilsPreferences.setString(RegistrationSelectionActivity.this, Constant.registration_date, obj.getRegistration_date());
            UtilsPreferences.setString(RegistrationSelectionActivity.this, Constant.last_update_date, obj.getRegistration_date());
            UtilsPreferences.setString(RegistrationSelectionActivity.this, Constant.facebook_id, obj.getFacebook_id());

            UtilsPreferences.setString(RegistrationSelectionActivity.this, Constant.user_type, obj.getUser_type());
            UtilsPreferences.setString(RegistrationSelectionActivity.this, Constant.connect_flag, Constant.connected_with_tracker);

            if (!obj.getDevice_token().equalsIgnoreCase("")) {
                UtilsPreferences.setBoolean(RegistrationSelectionActivity.this, Constant.isdevicegcmidstore, true);
            }
            UtilsPreferences.setBoolean(getContext(), Constant.first_launch, true);

            startActivity(new Intent(getContext(), DashboardActivity.class));
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            finishAffinity();
        }
    }

    // failed listener of RegisterCall
    @Override
    public void onFaildToRegister(String error_msg) {
        Toast.makeText(RegistrationSelectionActivity.this, error_msg, Toast.LENGTH_SHORT).show();
    }
}
