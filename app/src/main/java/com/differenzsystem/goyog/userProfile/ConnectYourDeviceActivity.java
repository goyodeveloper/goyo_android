package com.differenzsystem.goyog.userProfile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;

public class ConnectYourDeviceActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout ll_back;
    TextView tv_title;

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connect_your_device_activity);
        getSupportActionBar().hide();

        initControls();
        initControlAction();
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ConnectYourDeviceActivity.this, DashboardActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        finish();
    }

    // initialize all controls define in xml layout
    public void initControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(getString(R.string.connect_your_device));

            webView = (WebView) findViewById(R.id.webView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initControlAction() {
        try {
            ll_back.setOnClickListener(this);
            Log.e("webview url", "https://app.validic.com/57addd93ff9d930009000025/" + UtilsPreferences.getString(ConnectYourDeviceActivity.this, Constant.validic_access_token));
            startWebView("https://app.validic.com/57addd93ff9d930009000025/" + UtilsPreferences.getString(ConnectYourDeviceActivity.this, Constant.validic_access_token));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;

            default:
                break;
        }
    }

    // intent to url screen
    private void startWebView(String url) {
        webView.setWebViewClient(new WebViewClient() {
            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {

            }

            public void onPageFinished(WebView view, String url) {
                try {
                    UtilsCommon.destroyProgressBar();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        UtilsCommon.showProgressDialog(ConnectYourDeviceActivity.this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
}
