package com.differenzsystem.goyog.userProfile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.DecimalDigitsInputFilter;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;


public class RegisterWeight extends AppCompatActivity {

    EditText et_kl, et_pnd;
    TextView tv_popup_title, tv_done, tv_title;
    RadioGroup rgb_selection;
    RadioButton rb_kl, rb_pnd;

    RelativeLayout rel_main;
    LinearLayout ll_back, lin_main;

    boolean isPnd = false;

    boolean isUpdateCall = false;

    String str_kg = "", str_lb = "";
    String result = "";
    String result_value = "";

    int PICK_WEIGHT_REQUEST = 22;
    Bundle extra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_weight);
        getSupportActionBar().hide();

        // initialize all controls define in xml layout
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        lin_main = (LinearLayout) findViewById(R.id.lin_main);
        rel_main = (RelativeLayout) findViewById(R.id.rel_main);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_popup_title = (TextView) findViewById(R.id.tv_popup_title);
        et_kl = (EditText) findViewById(R.id.et_kl);
        et_pnd = (EditText) findViewById(R.id.et_pnd);
        tv_done = (TextView) findViewById(R.id.tv_done);

        rgb_selection = (RadioGroup) findViewById(R.id.rgb_selection);
        rb_kl = (RadioButton) findViewById(R.id.rb_kl);
        rb_pnd = (RadioButton) findViewById(R.id.rb_pnd);

        et_kl.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(100, 3)});
        et_pnd.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(100, 3)});

//        et_pnd.setFilters(new InputFilter[]{new InputFilterDoubleMinMax("0", "2203")}); // here 0 is min and 2203 is max value for inch

        tv_title.setText("");
        extra = getIntent().getExtras();
        if (extra == null)
            return;
        if (extra.containsKey("ispnd") && extra.containsKey("result_value")) {
            isPnd = extra.getBoolean("ispnd");
            if (isPnd) {
                str_lb = extra.getString("result_value");
            } else {
                str_kg = extra.getString("result_value");
            }
        }


        tv_popup_title.setTypeface(setFont(getContext(), R.string.app_bold));//

        tv_done.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_kl.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_pnd.setTypeface(setFont(getContext(), R.string.app_regular));//

        tv_popup_title.setText(getString(R.string.title_enter_weight));

        if (isPnd) {
            rb_pnd.setChecked(true);
            et_kl.setVisibility(View.GONE);
            et_pnd.setVisibility(View.VISIBLE);
            if (!str_lb.isEmpty()) {
                et_pnd.setText(str_lb);
            }
        } else {
            rb_kl.setChecked(true);
            et_pnd.setVisibility(View.GONE);
            et_kl.setVisibility(View.VISIBLE);
            if (!str_kg.isEmpty()) {
                et_kl.setText(str_kg);
            }
        }

        rgb_selection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                if (checkedId == R.id.rb_kl) {
                    et_pnd.setVisibility(View.GONE);
                    et_kl.setVisibility(View.VISIBLE);
                    if (!et_pnd.getText().toString().isEmpty()) {
                        double lbs = Double.parseDouble(et_pnd.getText().toString());
                        double kg = (Math.round((lbs * .45359237) * 10) / 10.0);
                        String val = String.format("%.3f", kg);
                        et_kl.setText(val);
                    } else {
                        et_kl.setText("");
                    }
                } else {
                    et_kl.setVisibility(View.GONE);
                    et_pnd.setVisibility(View.VISIBLE);
                    if (!et_kl.getText().toString().isEmpty()) {
                        double kg = Double.parseDouble(et_kl.getText().toString());
                        double lbs = (Math.round((kg / .45359237) * 10) / 10.0);
                        String val = String.format("%.3f", lbs);
                        et_pnd.setText(val);
                    } else {
                        et_pnd.setText("");
                    }
                }
            }
        });

        // done button click event
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (rb_kl.isChecked()) {
                    if (!et_kl.getText().toString().isEmpty()) {
                        isPnd = false;
                        str_kg = et_kl.getText().toString();
                        intentWeight(isPnd, str_kg + " kg", str_kg);
                    }
                } else {
                    if (!et_pnd.getText().toString().isEmpty()) {
                        isPnd = true;
                        str_lb = et_pnd.getText().toString();
                        intentWeight(isPnd, str_lb + " lbs", str_lb);
                    }
                }

            }
        });

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        rel_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        lin_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegisterWeight.this, Constant.Name_Register_Weight);
    }

    // open register height weight screen
    private void intentWeight(boolean flag_pnd, String res, String res_val) {
        Intent intent = new Intent();
        intent.putExtra("ispnd", flag_pnd);
        intent.putExtra("result", res);
        intent.putExtra("result_value", res_val);
        setResult(RESULT_OK, intent);
        finish();//finishing activity
    }

    private Context getContext() {
        // return register weight screen context
        return RegisterWeight.this;
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

}
