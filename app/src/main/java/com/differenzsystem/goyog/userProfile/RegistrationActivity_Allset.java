package com.differenzsystem.goyog.userProfile;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.RegisterCall;
import com.differenzsystem.goyog.api.UserRegisterUpdate;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.model.RegisterModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.DecimalDigitsInputFilter;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.differenzsystem.goyog.utility.UtilsValidation;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

public class RegistrationActivity_Allset extends AppCompatActivity implements View.OnClickListener,
        RegisterCall.OnRegisterUserListener, UserRegisterUpdate.OnUserRegisterUpdate, View.OnTouchListener {
    EditText et_fname, et_lname, et_othername, et_address, et_contact_number, et_email, et_nic,
            et_nomineenic, et_nomineename, et_nomineeaddress, et_nominee_contact_number;
    Button btn_confirm;
    LinearLayout ll_back, ll_main;
    TextView tv_title, tv_dob, tv_gender, tv_occupation, tv_meritalstatus, txt_detail,
            tv_double_check, tv_support, tv_personal_detail, tv_children;

    TextView tv_height, tv_weight;

    Spinner sp_marital_status, sp_gender, sp_occupation;

    String[] SpArrGender;
    ArrayAdapter<String> GenderAdapter;

    String[] SpArrMeritalStatus;
    ArrayAdapter<String> MeritalStatusAdapter;

    String[] SpArrOccupation;
    ArrayAdapter<String> OccupationAdapter;

    RegisterCall.OnRegisterUserListener onRegisterUserListener;
    UserRegisterUpdate.OnUserRegisterUpdate onUpdateUserDetailListener;

    Calendar myCalendar;
    String DOB;
    DatePickerDialog dialog;
    Boolean isFirst = true;


    boolean isCm = false;
    boolean isPnd = false;

    String str_ft_in = "", str_cm = "", str_kg = "", str_lb = "";

    static final int PICK_HEIGHT_REQUEST = 21;
    static final int PICK_WEIGHT_REQUEST = 22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity_allset);
        Intent intent = getIntent();
        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();

        if (!intent.getBooleanExtra(Constant.isFromSetting, false)) {
            setData();

        } else {
            tv_height.setClickable(false);
            tv_height.setEnabled(false);
            tv_weight.setClickable(false);
            tv_weight.setEnabled(false);

            tv_height.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            tv_weight.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            setDatafromGlobals();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegistrationActivity_Allset.this, Constant.Name_Registration_AllSet_or_PersonalDetail);
    }

    // set all the data from shared prefrences
    private void setDatafromGlobals() {

        et_fname.setText(UtilsPreferences.getString(getContext(), Constant.first_name));
        et_lname.setText(UtilsPreferences.getString(getContext(), Constant.last_name));
        et_othername.setText(UtilsPreferences.getString(getContext(), Constant.other_names));
        et_address.setText(UtilsPreferences.getString(getContext(), Constant.address));
        et_contact_number.setText(UtilsPreferences.getString(getContext(), Constant.contact_number));
        et_email.setText(UtilsPreferences.getString(getContext(), Constant.email));
        et_nic.setText(UtilsPreferences.getString(getContext(), Constant.nic));

        et_nomineenic.setText(UtilsPreferences.getString(getContext(), Constant.nominee_nic));
        et_nomineename.setText(UtilsPreferences.getString(getContext(), Constant.nominee_name));
        et_nomineeaddress.setText(UtilsPreferences.getString(getContext(), Constant.nominee_address));
        et_nominee_contact_number.setText(UtilsPreferences.getString(getContext(), Constant.nominee_number));

        tv_gender.setText(UtilsPreferences.getString(getContext(), Constant.gender));
        tv_occupation.setText(UtilsPreferences.getString(getContext(), Constant.occupation));
        tv_meritalstatus.setText(UtilsPreferences.getString(getContext(), Constant.marital_status));


        if (UtilsPreferences.getString(getContext(), Constant.user_type).equalsIgnoreCase("1")) {
            tv_dob.setText(UtilsPreferences.getString(getContext(), Constant.dob));

            String noChild = UtilsPreferences.getString(getContext(), Constant.no_of_children);
            if (noChild != null && noChild.length() > 0)
                tv_children.setText(UtilsPreferences.getString(getContext(), Constant.no_of_children));
            else
                tv_children.setText("0");
        }

        btn_confirm.setVisibility(View.GONE);
        txt_detail.setVisibility(View.GONE);
        tv_double_check.setVisibility(View.GONE);
        tv_support.setVisibility(View.VISIBLE);

        et_fname.setFocusable(false);
        et_lname.setFocusable(false);
        et_othername.setFocusable(false);
        et_address.setFocusable(false);
        et_contact_number.setFocusable(false);
        et_email.setFocusable(false);
        et_nic.setFocusable(false);

        et_nomineenic.setFocusable(false);
        et_nomineename.setFocusable(false);
        et_nomineeaddress.setFocusable(false);
        et_nominee_contact_number.setFocusable(false);

        tv_dob.setClickable(false);

        tv_title.setText(R.string.personal_detail);

        setTermsnPrivacyClick();

        loadHeightWeightData();


       /* sp_marital_status.setSelection(Integer.parseInt(RegistrationActivity.RegDetail.get("MaritalStatus")));
        sp_gender.setSelection(Integer.parseInt(RegistrationActivity.RegDetail.get("Gender")));
        sp_occupation.setSelection(Integer.parseInt(RegistrationActivity.RegDetail.get("Occupation")));*/
    }

    // set click event for term privacy
    private void setTermsnPrivacyClick() {
        SpannableString ss = new SpannableString("Note: Contact GOYO support to make any changes to your personal details.");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.invalidate();
                Intent i = new Intent(getApplicationContext(), TermConditionWebViewActivity.class);
                i.putExtra("From", "Support");
                startActivity(i);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
                ds.setColor(getResources().getColor(R.color.green));
            }
        };

        ss.setSpan(clickableSpan, 19, 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_support.setText(ss);
        tv_support.setMovementMethod(LinkMovementMethod.getInstance());
    }

    // set all the data
    public void setData() {

        setGenderSpinner();
        setMeritalStatusSpinner();
        setOccupationSpinner();

        sp_gender.setOnTouchListener(this);
        sp_marital_status.setOnTouchListener(this);
        sp_occupation.setOnTouchListener(this);

        tv_gender.setVisibility(View.GONE);
        tv_occupation.setVisibility(View.GONE);
        tv_meritalstatus.setVisibility(View.GONE);

        sp_marital_status.setVisibility(View.VISIBLE);
        sp_gender.setVisibility(View.VISIBLE);
        sp_occupation.setVisibility(View.VISIBLE);

//        et_fname.setText(RegistrationActivity.RegDetail.get("FN"));
//        et_lname.setText(RegistrationActivity.RegDetail.get("LN"));
//        et_othername.setText(RegistrationActivity.RegDetail.get("OthName"));
//        et_address.setText(RegistrationActivity.RegDetail.get("Address"));
//        et_contact_number.setText(RegistrationActivity.RegDetail.get("ContactNo"));
//        et_email.setText(RegistrationActivity.RegDetail.get("email"));
//        et_nic.setText(RegistrationActivity.RegDetail.get("NIC"));
//
//        et_nomineenic.setText(RegistrationActivity.RegDetail.get("NomineeNIC"));
//        et_nomineename.setText(RegistrationActivity.RegDetail.get("NomineeName"));
//        et_nomineeaddress.setText(RegistrationActivity.RegDetail.get("NomineeAddress"));
//        et_nominee_contact_number.setText(RegistrationActivity.RegDetail.get("NomineeContact"));
//
//        tv_dob.setText(RegistrationActivity.RegDetail.get("DOB"));
        et_fname.setText(UtilsPreferences.getString(getContext(), Constant.first_name));
        et_lname.setText(UtilsPreferences.getString(getContext(), Constant.last_name));
        et_othername.setText(UtilsPreferences.getString(getContext(), Constant.other_names));
        et_address.setText(UtilsPreferences.getString(getContext(), Constant.address));
        et_contact_number.setText(UtilsPreferences.getString(getContext(), Constant.contact_number));

        String emailreg = UtilsPreferences.getString(getContext(), Constant.emailreg);
        if (emailreg != null && emailreg.length() > 0)
            et_email.setText(UtilsPreferences.getString(getContext(), Constant.emailreg));
        else
            et_email.setText(UtilsPreferences.getString(getContext(), Constant.email));

        et_nic.setText(UtilsPreferences.getString(getContext(), Constant.nic));

        et_nomineenic.setText(UtilsPreferences.getString(getContext(), Constant.nominee_nic));
        et_nomineename.setText(UtilsPreferences.getString(getContext(), Constant.nominee_name));
        et_nomineeaddress.setText(UtilsPreferences.getString(getContext(), Constant.nominee_address));
        et_nominee_contact_number.setText(UtilsPreferences.getString(getContext(), Constant.nominee_number));

        tv_dob.setText(UtilsPreferences.getString(getContext(), Constant.dob));

        sp_marital_status.setSelection(Integer.parseInt(UtilsPreferences.getString(getContext(), Constant.marital_status)));
        sp_gender.setSelection(Integer.parseInt(UtilsPreferences.getString(getContext(), Constant.gender)));
        sp_occupation.setSelection(Integer.parseInt(UtilsPreferences.getString(getContext(), Constant.occupation)));

        String noChild = UtilsPreferences.getString(getContext(), Constant.no_of_children);
        if (noChild != null && noChild.length() > 0) {
            if (sp_marital_status.getSelectedItem().toString().equals("Married"))
                tv_children.setText(UtilsPreferences.getString(getContext(), Constant.no_of_children));
            else
                tv_children.setText("0");
        } else
            tv_children.setText("0");

        et_nic.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(12)});
        et_nomineenic.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(12)});

        // handle marital status click event
        sp_marital_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    if (isFirst)
                        isFirst = false;
                    else
                        openEditDialog();
                } else if (position == 2)
                    tv_children.setText("0");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        loadHeightWeightData();

        /* tv_gender.setText(RegistrationActivity.RegDetail.get("Gender"));
        tv_occupation.setText(RegistrationActivity.RegDetail.get("Occupation"));
        tv_meritalstatus.setText(RegistrationActivity.RegDetail.get("MaritalStatus"));*/
    }

    // initialize all controls define in xml layout
    void initializeControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            ll_main = (LinearLayout) findViewById(R.id.ll_main);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(R.string.reg_title_allset);

            et_fname = (EditText) findViewById(R.id.et_fname);
            et_lname = (EditText) findViewById(R.id.et_lname);
            et_othername = (EditText) findViewById(R.id.et_othername);
            et_address = (EditText) findViewById(R.id.et_address);
            et_contact_number = (EditText) findViewById(R.id.et_contact_number);
            et_email = (EditText) findViewById(R.id.et_email);
            et_nic = (EditText) findViewById(R.id.et_nic);
            txt_detail = (TextView) findViewById(R.id.txt_detail);
            tv_double_check = (TextView) findViewById(R.id.tv_double_check);
            tv_support = (TextView) findViewById(R.id.tv_support);
            tv_personal_detail = (TextView) findViewById(R.id.tv_personal_detail);
            tv_children = (TextView) findViewById(R.id.tv_children);

            tv_height = (TextView) findViewById(R.id.tv_height);
            tv_weight = (TextView) findViewById(R.id.tv_weight);

            et_nomineenic = (EditText) findViewById(R.id.et_nomineenic);
            et_nomineename = (EditText) findViewById(R.id.et_nomineename);
            et_nomineeaddress = (EditText) findViewById(R.id.et_nomineeaddress);
            et_nominee_contact_number = (EditText) findViewById(R.id.et_nominee_contact_number);

            tv_dob = (TextView) findViewById(R.id.tv_dob);

            tv_meritalstatus = (TextView) findViewById(R.id.tv_meritalstatus);
            tv_occupation = (TextView) findViewById(R.id.tv_occupation);
            tv_gender = (TextView) findViewById(R.id.tv_gender);

            //sp_marital_status, sp_gender, sp_occupation
            sp_marital_status = (Spinner) findViewById(R.id.sp_marital_status);
            sp_gender = (Spinner) findViewById(R.id.sp_gender);
            sp_occupation = (Spinner) findViewById(R.id.sp_occupation);

            btn_confirm = (Button) findViewById(R.id.btn_confirm);

            et_fname.setTypeface(setFont(getContext(), R.string.app_regular));
            et_lname.setTypeface(setFont(getContext(), R.string.app_regular));
            et_othername.setTypeface(setFont(getContext(), R.string.app_regular));
            et_address.setTypeface(setFont(getContext(), R.string.app_regular));
            et_contact_number.setTypeface(setFont(getContext(), R.string.app_regular));
            et_email.setTypeface(setFont(getContext(), R.string.app_regular));
            et_nic.setTypeface(setFont(getContext(), R.string.app_regular));

            et_nomineenic.setTypeface(setFont(getContext(), R.string.app_regular));
            et_nomineename.setTypeface(setFont(getContext(), R.string.app_regular));
            et_nomineeaddress.setTypeface(setFont(getContext(), R.string.app_regular));
            et_nominee_contact_number.setTypeface(setFont(getContext(), R.string.app_regular));

            tv_dob.setTypeface(setFont(getContext(), R.string.app_regular));
            tv_meritalstatus.setTypeface(setFont(getContext(), R.string.app_regular));
            tv_occupation.setTypeface(setFont(getContext(), R.string.app_regular));
            tv_gender.setTypeface(setFont(getContext(), R.string.app_regular));

            btn_confirm.setTypeface(setFont(getContext(), R.string.app_bold));

            myCalendar = Calendar.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click event
    public void initializeControlsAction() {
        try {
            onRegisterUserListener = this;
            onUpdateUserDetailListener = this;
            initCalander();
            btn_confirm.setOnClickListener(this);
            ll_back.setOnClickListener(this);
            tv_dob.setOnClickListener(this);
            ll_main.setOnClickListener(this);

            tv_height.setOnClickListener(this);
            tv_weight.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set date picker dialog
    public void initCalander() {
        dialog = new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        myCalendar.set(myCalendar
                        .get(Calendar.YEAR) - 18, myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());
    }

    // handle click event of controls
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm:
                if (isValidated(true)) {
                    doRegistration();
                }
                /*startActivity(new Intent(this, RegistrationActivity_3.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);*/
                break;
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_dob:
                //selectDate();
                dialog.show();
                break;
            case R.id.ll_main:
                UtilsCommon.hideSoftKeyboard(this);
                break;
            case R.id.tv_height:
//                openHeightSelectionDialog(getString(R.string.title_enter_height));
                IntentHeightSelection();
                break;
            case R.id.tv_weight:
//                openWeightSelectionDialog(getString(R.string.title_enter_weight));
                IntentWeightSelection();
                break;
        }
    }

    // open Register height screen to set height
    private void IntentHeightSelection() {
        Intent i_height = new Intent(RegistrationActivity_Allset.this, RegisterHeight.class);
        i_height.putExtra("iscm", isCm);
        if (isCm) {
            i_height.putExtra("result_value", str_cm);
        } else {
            i_height.putExtra("result_value", str_ft_in);
        }

        startActivityForResult(i_height, PICK_HEIGHT_REQUEST);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

    }

    // open Register weight screen to set weight
    private void IntentWeightSelection() {
        Intent i_height = new Intent(RegistrationActivity_Allset.this, RegisterWeight.class);
        i_height.putExtra("ispnd", isPnd);
        if (isPnd) {
            i_height.putExtra("result_value", str_lb);
        } else {
            i_height.putExtra("result_value", str_kg);
        }

        startActivityForResult(i_height, PICK_WEIGHT_REQUEST);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

    }

    // api call for register
    public void doRegistration() {
        try {
            JSONObject data = new JSONObject();

            if (UtilsPreferences.getString(getApplicationContext(), Constant.user_type) != null) {
                if (!UtilsPreferences.getString(getContext(), Constant.user_type).equalsIgnoreCase("1")) {
                    data.put(Constant.user_id, UtilsPreferences.getString(getContext(), Constant.user_id));
                    data.put(Constant.device_token, UtilsPreferences.getString(RegistrationActivity_Allset.this, Constant.devicegcmid));
                    data.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));
                }
            }
            data.put(Constant.email, et_email.getText().toString().trim());
            String pwd = UtilsPreferences.getString(getContext(), Constant.password);
            if (pwd != null && pwd.length() > 0) {
                data.put(Constant.password, UtilsPreferences.getString(getContext(), Constant.password));
            } else {
                data.put(Constant.password, "0");
            }

            //data.put(Constant.full_name, et_lname.getText().toString().trim());
            data.put(Constant.device_token, UtilsPreferences.getString(RegistrationActivity_Allset.this, Constant.devicegcmid));
            data.put(Constant.device_type, Constant.device_type_value);

            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            data.put(Constant.timezone, tz.getID());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateandTime = sdf.format(new Date());
            data.put(Constant.regdate, currentDateandTime);

            data.put(Constant.first_name, et_fname.getText().toString().trim());
            data.put(Constant.last_name, et_lname.getText().toString().trim());
            data.put(Constant.other_names, et_othername.getText().toString().trim());
            data.put(Constant.address, et_address.getText().toString().trim());
            data.put(Constant.contact_number, et_contact_number.getText().toString().trim());
            data.put(Constant.dob, tv_dob.getText().toString().trim());
//            data.put(Constant.occupation, tv_occupation.getText().toString().trim());
//            data.put(Constant.marital_status, tv_meritalstatus.getText().toString().trim());
//            data.put(Constant.gender, tv_gender.getText().toString().trim());
//            data.put(Constant.nic, et_nic.getText().toString().trim());
            data.put(Constant.occupation, sp_occupation.getSelectedItem().toString().trim());
            data.put(Constant.marital_status, sp_marital_status.getSelectedItem().toString().trim());
            data.put(Constant.gender, sp_gender.getSelectedItem().toString().trim());
            data.put(Constant.nic, et_nic.getText().toString().trim());
            data.put(Constant.nominee_nic, et_nomineenic.getText().toString().trim());
            data.put(Constant.nominee_name, et_nomineename.getText().toString().trim());
            data.put(Constant.nominee_address, et_nomineeaddress.getText().toString().trim());
            data.put(Constant.nominee_number, et_nominee_contact_number.getText().toString().trim());
            data.put(Constant.user_type, "1");
            data.put(Constant.connect_flag, Constant.connected_with_tracker);//for demo user pass this as static 3 for connection flag
            data.put(Constant.mac_id, UtilsPreferences.getString(getContext(), Constant.mac_id));
            data.put(Constant.facebook_id, UtilsPreferences.getString(getContext(), Constant.facebook_id));
            data.put(Constant.no_of_children, tv_children.getText().toString().trim());

            // this is for last updation set all screen
            storeDataHeightWeight();

            data.put(Constant.height, UtilsPreferences.getString(getContext(), Constant.height));
            data.put(Constant.height_scale, UtilsPreferences.getString(getContext(), Constant.height_scale));
            data.put(Constant.weight, UtilsPreferences.getString(getContext(), Constant.weight));
            data.put(Constant.weight_scale, UtilsPreferences.getString(getContext(), Constant.weight_scale));

            /*data.put(Constant.fb_education, UtilsPreferences.getString(getContext(), Constant.fb_education));
            data.put(Constant.fb_groups, UtilsPreferences.getString(getContext(), Constant.fb_groups));
            data.put(Constant.fb_events, UtilsPreferences.getString(getContext(), Constant.fb_events));
            data.put(Constant.fb_work_exp, UtilsPreferences.getString(getContext(), Constant.fb_work_exp));*/


            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {

                if (UtilsPreferences.getString(getApplicationContext(), Constant.user_type) != null) {
                    if (!UtilsPreferences.getString(getContext(), Constant.user_type).equalsIgnoreCase("1")) {
                        UserRegisterUpdate task = new UserRegisterUpdate(RegistrationActivity_Allset.this, onUpdateUserDetailListener, data, true);
                        task.execute();
                    }
                } else {
                    RegisterCall reg = new RegisterCall(RegistrationActivity_Allset.this, onRegisterUserListener, data, true);
                    reg.execute();
                }
            } else {
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of RegisterCall and save the response data in shared prefrences
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onSucceedToRegister(RegisterModel obj) {
        storeData(obj);
    }

    // failed listener of RegisterCall
    @Override
    public void onFaildToRegister(String error_msg) {
        Toast.makeText(getContext(), error_msg, Toast.LENGTH_SHORT).show();
    }

    // success listener of UserRegisterUpdate
    @Override
    public void onSucceedToUserRegisterUpdate(RegisterModel obj) {
        storeData(obj);
    }

    // failed listener of UserRegisterUpdate
    @Override
    public void onFaildToUserRegisterUpdate(String error_msg) {
        Toast.makeText(getContext(), error_msg, Toast.LENGTH_SHORT).show();
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private Context getContext() {
        // return context of RegistrationActivity_Allset screen
        return RegistrationActivity_Allset.this;
    }

    // check validation of firts name, last name, email, address etc

    /**
     * @param isShowError boolean for showing error
     * @return true or false
     */
    public boolean isValidated(boolean isShowError) {
        boolean isValid = true;

        if (!UtilsValidation.isValidFname(getContext(), et_fname, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidLname(getContext(), et_lname, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidEmail(getContext(), et_email, isShowError)) {
            isValid = false;
        }
//        if (!UtilsValidation.isValidOthername(getContext(), et_othername, isShowError)) {
//            isValid = false;
//        }

        if (!UtilsValidation.isValidAddress(getContext(), et_address, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidContactNo(getContext(), et_contact_number, isShowError)) {
            isValid = false;
        }

        // change from isValidNICMain to isValidNICMainAllSet Added on 21-07-2017
        if (!UtilsValidation.isValidNICMainAllSet(getContext(), et_nic, tv_dob.getText().toString().trim(), isShowError)) {
            isValid = false;
        }

        //nominee

        if (!UtilsValidation.isValidNICNominee(getContext(), et_nomineenic, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidName(getContext(), et_nomineename, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidAddress(getContext(), et_nomineeaddress, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidContactNo(getContext(), et_nominee_contact_number, isShowError)) {
            isValid = false;
        }

        //dropdowe
        if (sp_gender.getSelectedItemPosition() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.err_msg_please_select_gender), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (sp_occupation.getSelectedItemPosition() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.err_msg_please_select_occupation), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (sp_marital_status.getSelectedItemPosition() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.err_msg_please_select_merital_status), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        //dob
        if (tv_dob.getText().toString().length() <= 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.err_msg_please_selsct_dob), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        return isValid;
    }

    // set dropdown data of gender
    public void setGenderSpinner() {
        SpArrGender = new String[]{"Gender", "Male", "Female"};
        GenderAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, SpArrGender) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTypeface(setFont(getContext(), R.string.app_regular));
                if (position == 0) {
                    // tv.setTextColor(getResources().getColor(R.color.gray));
                } else {
                    //  tv.setTextColor(getResources().getColor(R.color.darker_gray_text));
                }

                return view;
            }

            public TextView getView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setPadding(0, v.getPaddingTop(), v.getPaddingRight(), v.getPaddingBottom());
                v.setTypeface(setFont(getContext(), R.string.app_regular));
                return v;
            }
        };
        sp_gender.setAdapter(GenderAdapter);
    }

    // set dropdown data of marital status
    public void setMeritalStatusSpinner() {
        SpArrMeritalStatus = new String[]{"Marital Status", "Married", "Unmarried"};
        MeritalStatusAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, SpArrMeritalStatus) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTypeface(setFont(getContext(), R.string.app_regular));
                if (position == 0) {
                    // tv.setTextColor(getResources().getColor(R.color.gray));
                } else {
                    //  tv.setTextColor(getResources().getColor(R.color.darker_gray_text));
                }
                return view;
            }

            public TextView getView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setPadding(0, v.getPaddingTop(), v.getPaddingRight(), v.getPaddingBottom());
                v.setTypeface(setFont(getContext(), R.string.app_regular));
                return v;
            }
        };
        sp_marital_status.setAdapter(MeritalStatusAdapter);
    }

    // set dropdown data of occupation
    public void setOccupationSpinner() {
        SpArrOccupation = new String[]
                {"Occupation", "Business person", "Executive",
                        "Forces", "Government", "Legal", "Manager",
                        "Officer", "Schooling", "Self Employed",
                        "Snr. Management", "University", "Other"};
        OccupationAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, SpArrOccupation) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setTypeface(setFont(getContext(), R.string.app_regular));
                if (position == 0) {
                    // tv.setTextColor(getResources().getColor(R.color.gray));
                } else {
                    //  tv.setTextColor(getResources().getColor(R.color.darker_gray_text));
                }
                return view;
            }

            public TextView getView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setPadding(0, v.getPaddingTop(), v.getPaddingRight(), v.getPaddingBottom());
                v.setTypeface(setFont(getContext(), R.string.app_regular));
                return v;
            }
        };
        sp_occupation.setAdapter(OccupationAdapter);
    }

    // datepicker click event
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            enddateLabel();
        }
    };

    // set the date in format yyyy-MM-dd from date picker
    private void enddateLabel() {
        String myFormatDisplay = "yyyy-MM-dd";// "MMM dd, yyyy";//In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormatDisplay, Locale.US);
        DOB = sdf.format(myCalendar.getTime());
        tv_dob.setText(DOB);
    }

    // save all the data in shared prefrences
    public void storeData(RegisterModel obj) {
        if (obj != null) {
            UtilsPreferences.setString(getContext(), Constant.email, obj.getEmail());
            UtilsPreferences.setString(getContext(), Constant.full_name, obj.getFirst_name() + " " + obj.getLast_name());
            UtilsPreferences.setString(getContext(), Constant.user_id, obj.getUser_id());
            if (obj.getAccesstoken() != null && obj.getAccesstoken().length() > 0) {
                UtilsPreferences.setString(getContext(), Constant.accesstoken, obj.getAccesstoken());
            } else {
                UtilsPreferences.setString(getContext(), Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));
            }

            UtilsPreferences.setString(getContext(), Constant.registration_date, obj.getRegistration_date());
            UtilsPreferences.setString(getContext(), Constant.last_update_date, obj.getRegistration_date());

            //setdata in global
            UtilsPreferences.setString(getContext(), Constant.first_name, obj.getFirst_name());
            UtilsPreferences.setString(getContext(), Constant.last_name, obj.getLast_name());
            UtilsPreferences.setString(getContext(), Constant.other_names, obj.getOther_names());
            UtilsPreferences.setString(getContext(), Constant.address, obj.getAddress());
            UtilsPreferences.setString(getContext(), Constant.contact_number, obj.getContact_number());
            UtilsPreferences.setString(getContext(), Constant.dob, obj.getDob());
            UtilsPreferences.setString(getContext(), Constant.occupation, obj.getOccupation());
            UtilsPreferences.setString(getContext(), Constant.marital_status, obj.getMarital_status());
            UtilsPreferences.setString(getContext(), Constant.gender, obj.getGender());
            UtilsPreferences.setString(getContext(), Constant.nic, obj.getNic());
            UtilsPreferences.setString(getContext(), Constant.nominee_nic, obj.getNominee_nic());
            UtilsPreferences.setString(getContext(), Constant.nominee_name, obj.getNominee_name());
            UtilsPreferences.setString(getContext(), Constant.nominee_address, obj.getNominee_address());
            UtilsPreferences.setString(getContext(), Constant.nominee_number, obj.getNominee_number());

            UtilsPreferences.setString(getContext(), Constant.user_type, obj.getUser_type());
            //UtilsPreferences.setString(getContext(), Constant.connect_flag, obj.getConnect_flag());
            UtilsPreferences.setString(getContext(), Constant.connect_flag, Constant.connected_with_tracker);
            UtilsPreferences.setString(getContext(), Constant.mac_id, obj.getMac_id());
            UtilsPreferences.setString(getContext(), Constant.facebook_id, obj.getFacebook_id());
            UtilsPreferences.setString(getContext(), Constant.user_exp_date, obj.getUser_exp_date());
            UtilsPreferences.setString(getContext(), Constant.no_of_children, obj.getNo_of_children());

            UtilsPreferences.setString(getContext(), Constant.height, obj.getHeight());
            UtilsPreferences.setString(getContext(), Constant.height_scale, obj.getHeight_scale());
            UtilsPreferences.setString(getContext(), Constant.weight, obj.getWeight());
            UtilsPreferences.setString(getContext(), Constant.weight_scale, obj.getWeight_scale());
            UtilsPreferences.setString(getContext(), Constant.led_visible_status, obj.getLed_visible_status());


            if (!obj.getDevice_token().equalsIgnoreCase("")) {
                UtilsPreferences.setBoolean(getContext(), Constant.isdevicegcmidstore, true);
            }
            UtilsPreferences.setBoolean(getContext(), Constant.first_launch, true);

            /*startActivity(new Intent(getContext(), DashboardActivity.class));//GetStartedActivity
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            finishAffinity();*/

            // call addSDkFunctionOrder added on 22-07-2017
//            UtilsCommon.addSDkFunctionOrder(RegistrationActivity_Allset.this);

            Intent intent = new Intent(getContext(), DashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    // open dialog for getting no of childrens
    public void openEditDialog() {
        final Dialog alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.setting_edit_name_dialog);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        //alertDialog.setCancelable(false);

        final EditText et_childcount = (EditText) alertDialog.findViewById(R.id.et_name);
        TextView tv_cancel = (TextView) alertDialog.findViewById(R.id.tv_cancel);
        TextView tv_change = (TextView) alertDialog.findViewById(R.id.tv_change);
        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        View vi_devider = (View) alertDialog.findViewById(R.id.vi_devider);

        et_childcount.setHint("Enter Number Of Children");
        et_childcount.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        et_childcount.setFilters(new InputFilter[]{new InputFilterMinMax("0", "12")});

        tv_popup_title.setText("Got kids?");

        tv_cancel.setVisibility(View.GONE);
        vi_devider.setVisibility(View.GONE);
        tv_change.setText(R.string.okay);

        et_childcount.setTypeface(setFont(this, R.string.app_regular));
        tv_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_childcount.getText().toString().length() != 0) {
                    UtilsPreferences.setString(getContext(), Constant.no_of_children, et_childcount.getText().toString().trim());
                    tv_children.setText(UtilsPreferences.getString(getContext(), Constant.no_of_children));
                    alertDialog.dismiss();
                } else {
                    et_childcount.setError("Please enter Value");
                }
            }
        });
        alertDialog.show();
    }

    // handle touch event listener
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.sp_marital_status:
                UtilsCommon.hideSoftKeyboard(this);
                break;
            case R.id.sp_gender:
                UtilsCommon.hideSoftKeyboard(this);
                break;
            case R.id.sp_occupation:
                UtilsCommon.hideSoftKeyboard(this);
                break;
        }
        return false;
    }

    public void openHeightSelectionDialog(final String title) {
        final Dialog alertDialog = new Dialog(getContext());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setCancelable(true);
        alertDialog.setContentView(R.layout.dialog_selection_height);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        final EditText et_ft = (EditText) alertDialog.findViewById(R.id.et_ft);
        final EditText et_in = (EditText) alertDialog.findViewById(R.id.et_in);
        final EditText et_cm = (EditText) alertDialog.findViewById(R.id.et_cm);
        TextView tv_done = (TextView) alertDialog.findViewById(R.id.tv_done);
        RadioGroup rgb_selection = (RadioGroup) alertDialog.findViewById(R.id.rgb_selection);
        final RadioButton rb_ft_in = (RadioButton) alertDialog.findViewById(R.id.rb_ft_in);
        RadioButton rb_ct = (RadioButton) alertDialog.findViewById(R.id.rb_ct);

        final LinearLayout lin_ft_in = (LinearLayout) alertDialog.findViewById(R.id.lin_ft_in);
        final LinearLayout lin_cm = (LinearLayout) alertDialog.findViewById(R.id.lin_cm);

        et_in.setFilters(new InputFilter[]{new InputFilterMinMax("0", "11")}); // here 0 is min and 12 is max value for inch

        tv_popup_title.setTypeface(setFont(getContext(), R.string.app_bold));//

        tv_done.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_ft.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_in.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_cm.setTypeface(setFont(getContext(), R.string.app_regular));//

        tv_popup_title.setText(title);

        if (isCm) {
            rb_ct.setChecked(true);
            lin_ft_in.setVisibility(View.GONE);
            lin_cm.setVisibility(View.VISIBLE);
            if (!str_cm.isEmpty()) {
                et_cm.setText(str_cm);
            }
        } else {
            rb_ft_in.setChecked(true);
            lin_cm.setVisibility(View.GONE);
            lin_ft_in.setVisibility(View.VISIBLE);
            if (!str_ft_in.isEmpty()) {
                if (str_ft_in.contains("-")) {
                    String[] str_arr = str_ft_in.split("-");
                    et_ft.setText(str_arr[0]);
                    et_in.setText(str_arr[1]);
                }
            }
        }

        rgb_selection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                if (checkedId == R.id.rb_ft_in) {
                    lin_cm.setVisibility(View.GONE);
                    lin_ft_in.setVisibility(View.VISIBLE);
                    if (!et_cm.getText().toString().isEmpty()) {
                        String[] str_arr = centimeterToFeet(et_cm.getText().toString()).split("-");
                        et_ft.setText(String.valueOf(str_arr[0]));
                        et_in.setText(String.valueOf(str_arr[1]));
                    } else {
                        et_ft.setText("");
                        et_in.setText("");
                    }
                } else {
                    String val = "";
                    if (!et_ft.getText().toString().isEmpty()) {

                        if (et_in.getText().toString().isEmpty()) {
                            et_in.setText("0");
                        }
                        val = et_ft.getText().toString() + "-" + et_in.getText().toString();
                    } else {
                        val = "";
                    }

                    lin_ft_in.setVisibility(View.GONE);
                    lin_cm.setVisibility(View.VISIBLE);
                    if (!val.isEmpty()) {
                        if (val.contains("-")) {
                            et_cm.setText(feetToCentimeter(val));
                        }
                    } else {
                        et_cm.setText("");
                    }
                }
            }
        });

        alertDialog.show();

        // handle done click event
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (rb_ft_in.isChecked()) {
                    String ft = et_ft.getText().toString();
                    String in = et_in.getText().toString();
                    if (!ft.isEmpty()) {
                        isCm = false;
                        if (in.isEmpty())
                            in = "0";
                        tv_height.setText(ft + " ft" + " - " + in + " in");
                        str_ft_in = ft + "-" + in;
                        alertDialog.dismiss();
                    }
                } else {
                    if (!et_cm.getText().toString().isEmpty()) {
                        isCm = true;
                        str_cm = et_cm.getText().toString();
                        tv_height.setText(str_cm + " cm");
                        alertDialog.dismiss();
                    }
                }

            }
        });

    }

    // convert feet to cm
    public static String feetToCentimeter(String feet) {
        double dCentimeter = 0d;
        if (!TextUtils.isEmpty(feet)) {
            if (feet.contains("-")) {
                String[] str_arr = feet.split("-");
                String tempfeet = str_arr[0];
                String tempinch = str_arr[1];
                if (!TextUtils.isEmpty(tempfeet)) {
                    dCentimeter += ((Double.valueOf(tempfeet)) * 30.48);
                }
                if (!TextUtils.isEmpty(tempinch)) {
                    dCentimeter += ((Double.valueOf(tempinch)) * 2.54);
                }
            }
        }
        return String.valueOf((int) dCentimeter);
        //Format to decimal digit as per your requirement
    }

    // convert cm to feet
    public static String centimeterToFeet(String centemeter) {
        int feetPart = 0;
        int inchesPart = 0;
        if (!TextUtils.isEmpty(centemeter)) {
            double dCentimeter = Double.valueOf(centemeter);
            feetPart = (int) Math.floor((dCentimeter / 2.54) / 12);
            System.out.println((dCentimeter / 2.54) - (feetPart * 12));
            inchesPart = (int) Math.ceil((dCentimeter / 2.54) - (feetPart * 12));

            // added for converting 5-12 to 6-0
            if (inchesPart == 12) {
                inchesPart = 0;
                feetPart = feetPart + 1;
            }
        }
        return String.valueOf(feetPart) + "-" + String.valueOf(inchesPart);
    }

    public void openWeightSelectionDialog(final String title) {
        final Dialog alertDialog = new Dialog(getContext());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_selection_weight);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        final EditText et_kl = (EditText) alertDialog.findViewById(R.id.et_kl);
        final EditText et_pnd = (EditText) alertDialog.findViewById(R.id.et_pnd);
        TextView tv_done = (TextView) alertDialog.findViewById(R.id.tv_done);

        RadioGroup rgb_selection = (RadioGroup) alertDialog.findViewById(R.id.rgb_selection);
        final RadioButton rb_kl = (RadioButton) alertDialog.findViewById(R.id.rb_kl);
        RadioButton rb_pnd = (RadioButton) alertDialog.findViewById(R.id.rb_pnd);

        et_kl.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(100, 3)});
        et_pnd.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(100, 3)});

//        et_pnd.setFilters(new InputFilter[]{new InputFilterDoubleMinMax("0", "2203")}); // here 0 is min and 2203 is max value for inch

        tv_popup_title.setTypeface(setFont(getContext(), R.string.app_bold));//

        tv_done.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_kl.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_pnd.setTypeface(setFont(getContext(), R.string.app_regular));//

        tv_popup_title.setText(title);

        if (isPnd) {
            rb_pnd.setChecked(true);
            et_kl.setVisibility(View.GONE);
            et_pnd.setVisibility(View.VISIBLE);
            if (!str_lb.isEmpty()) {
                et_pnd.setText(str_lb);
            }
        } else {
            rb_kl.setChecked(true);
            et_pnd.setVisibility(View.GONE);
            et_kl.setVisibility(View.VISIBLE);
            if (!str_kg.isEmpty()) {
                et_kl.setText(str_kg);
            }
        }

        rgb_selection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                if (checkedId == R.id.rb_kl) {
                    et_pnd.setVisibility(View.GONE);
                    et_kl.setVisibility(View.VISIBLE);
                    if (!et_pnd.getText().toString().isEmpty()) {
                        double lbs = Double.parseDouble(et_pnd.getText().toString());
                        double kg = (Math.round((lbs * .45359237) * 10) / 10.0);
                        String val = String.format("%.3f", kg);
                        et_kl.setText(val);
                    } else {
                        et_kl.setText("");
                    }
                } else {
                    et_kl.setVisibility(View.GONE);
                    et_pnd.setVisibility(View.VISIBLE);
                    if (!et_kl.getText().toString().isEmpty()) {
                        double kg = Double.parseDouble(et_kl.getText().toString());
                        double lbs = (Math.round((kg / .45359237) * 10) / 10.0);
                        String val = String.format("%.3f", lbs);
                        et_pnd.setText(val);
                    } else {
                        et_pnd.setText("");
                    }
                }
            }
        });

        alertDialog.show();

        // handle done click event
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (rb_kl.isChecked()) {
                    if (!et_kl.getText().toString().isEmpty()) {
                        isPnd = false;
                        str_kg = et_kl.getText().toString();
                        tv_weight.setText(str_kg + " kg");
                        alertDialog.dismiss();
                    }
                } else {
                    if (!et_pnd.getText().toString().isEmpty()) {
                        isPnd = true;
                        str_lb = et_pnd.getText().toString();
                        tv_weight.setText(str_lb + " lbs");
                        alertDialog.dismiss();
                    }
                }

            }
        });

    }

    // save height and weight in shared prefrences
    public void storeDataHeightWeight() {

        if (isCm) {
            UtilsPreferences.setString(getContext(), Constant.height, str_cm);
            UtilsPreferences.setString(getContext(), Constant.height_scale, "2");
        } else {
            UtilsPreferences.setString(getContext(), Constant.height, str_ft_in);
            UtilsPreferences.setString(getContext(), Constant.height_scale, "1");
        }

        if (isPnd) {
            UtilsPreferences.setString(getContext(), Constant.weight, str_lb);
            UtilsPreferences.setString(getContext(), Constant.weight_scale, "2");
        } else {
            UtilsPreferences.setString(getContext(), Constant.weight, str_kg);
            UtilsPreferences.setString(getContext(), Constant.weight_scale, "1");
        }

    }

    // set the data of height and weight
    private void loadHeightWeightData() {
        if (UtilsPreferences.getString(getContext(), Constant.height_scale) != null
                && UtilsPreferences.getString(getContext(), Constant.height) != null
                && UtilsPreferences.getString(getContext(), Constant.weight_scale) != null
                && UtilsPreferences.getString(getContext(), Constant.weight) != null) {

            if (UtilsPreferences.getString(getContext(), Constant.height_scale).equalsIgnoreCase("1")) {
                String[] str_arr = UtilsPreferences.getString(getContext(), Constant.height).split("-");
                tv_height.setText(str_arr[0] + " ft" + " - " + str_arr[1] + " in");
                str_ft_in = str_arr[0] + "-" + str_arr[1];
                isCm = false;

            } else if (UtilsPreferences.getString(getContext(), Constant.height_scale).equalsIgnoreCase("2")) {
                tv_height.setText(UtilsPreferences.getString(getContext(), Constant.height) + " cm");
                str_cm = UtilsPreferences.getString(getContext(), Constant.height);
                isCm = true;
            }

            if (UtilsPreferences.getString(getContext(), Constant.weight_scale).equalsIgnoreCase("1")) {
                str_kg = UtilsPreferences.getString(getContext(), Constant.weight);
                tv_weight.setText(UtilsPreferences.getString(getContext(), Constant.weight) + " kg");
                isPnd = false;

            } else if (UtilsPreferences.getString(getContext(), Constant.weight_scale).equalsIgnoreCase("2")) {
                str_lb = UtilsPreferences.getString(getContext(), Constant.weight);
                tv_weight.setText(UtilsPreferences.getString(getContext(), Constant.weight) + " lbs");
                isPnd = true;
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_HEIGHT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                isCm = data.getBooleanExtra("iscm", false);
                tv_height.setText(data.getStringExtra("result"));
                if (isCm) {
                    str_cm = data.getStringExtra("result_value");
                } else {
                    str_ft_in = data.getStringExtra("result_value");
                }
            }
        } else if (requestCode == PICK_WEIGHT_REQUEST) {
            if (resultCode == RESULT_OK) {
                isPnd = data.getBooleanExtra("ispnd", false);
                tv_weight.setText(data.getStringExtra("result"));
                if (isPnd) {
                    str_lb = data.getStringExtra("result_value");
                } else {
                    str_kg = data.getStringExtra("result_value");
                }
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }
}
