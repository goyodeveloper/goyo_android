package com.differenzsystem.goyog.userProfile;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.Update_Height_Weight;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.DecimalDigitsInputFilter;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;


public class RegistrationActivity_Height_Weight extends AppCompatActivity implements Update_Height_Weight.OnHeightWeightUpdate {

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.ll_back)
    LinearLayout ll_back;

    @BindView(R.id.tv_height)
    TextView tv_height;

    @BindView(R.id.tv_weight)
    TextView tv_weight;

    @BindView(R.id.tv_desc)
    TextView tv_desc;

    @BindView(R.id.btn_next)
    Button btn_next;

    String isFrom;
    boolean isCm = false;
    boolean isPnd = false;

    boolean isUpdateCall = false;

    String str_ft_in = "", str_cm = "", str_kg = "", str_lb = "";

    Update_Height_Weight.OnHeightWeightUpdate onHeightWeightUpdate;

    static final int PICK_HEIGHT_REQUEST = 21;
    static final int PICK_WEIGHT_REQUEST = 22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_height_weight);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        initializeFont();
        initializeProcess();
        setData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegistrationActivity_Height_Weight.this, Constant.Name_Registration_Height_Weight);
    }

    // get value for intent and check it for updation or registration
    private void initializeProcess() {
        onHeightWeightUpdate = this;

        if (getIntent() != null) {
            Intent i = getIntent();
            isFrom = i.getStringExtra("isFrom");

            if (isFrom != null && !isFrom.isEmpty()) {
                // check the current screen is redirect from setting screen
                if (isFrom.equalsIgnoreCase("SettingsFragment")) {
                    isUpdateCall = true;
                    btn_next.setText(getString(R.string.action_done));
                }
            }
        }
    }

    // set font in textview and button
    private void initializeFont() {
        btn_next.setTypeface(setFont(getContext(), R.string.app_bold));
        tv_desc.setTypeface(setFont(getContext(), R.string.app_regular));
        tv_height.setTypeface(setFont(getContext(), R.string.app_regular));
        tv_weight.setTypeface(setFont(getContext(), R.string.app_regular));
    }

    // set data when screen initialize
    private void setData() {

        tv_title.setText(getString(R.string.reg_title_height_weight));

        if (isUpdateCall) {
            loadHeightWeightData();
        }
    }

    // handle click event

    @OnClick(R.id.ll_back)
    public void backClick() {
        onBackPressed();
    }

    @OnClick(R.id.tv_height)
    public void heightClick() {
//        openHeightSelectionDialog(getString(R.string.title_enter_height));
        IntentHeightSelection();
    }

    @OnClick(R.id.tv_weight)
    public void weightClick() {
//        openWeightSelectionDialog(getString(R.string.title_enter_weight));
        IntentWeightSelection();


    }

    // open register height screen
    private void IntentHeightSelection() {
        Intent i_height = new Intent(RegistrationActivity_Height_Weight.this, RegisterHeight.class);
        i_height.putExtra("iscm", isCm);
        if (isCm) {
            i_height.putExtra("result_value", str_cm); // send cm value through intent
        } else {
            i_height.putExtra("result_value", str_ft_in); // send feet value through intent
        }

        startActivityForResult(i_height, PICK_HEIGHT_REQUEST);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

    }

    // open register weight screen
    private void IntentWeightSelection() {
        Intent i_height = new Intent(RegistrationActivity_Height_Weight.this, RegisterWeight.class);
        i_height.putExtra("ispnd", isPnd);
        if (isPnd) {
            i_height.putExtra("result_value", str_lb); // send pound value through intent
        } else {
            i_height.putExtra("result_value", str_kg); // send kg value through intent
        }

        startActivityForResult(i_height, PICK_WEIGHT_REQUEST);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

    }

    // handle next button click event
    @OnClick(R.id.btn_next)
    public void nextClick() {
        if (isValid()) {
            saveInPrefrences();
            if (isUpdateCall) {
                JSONObject data = new JSONObject();

                try {
                    data.put(Constant.user_id, UtilsPreferences.getString(getContext(), Constant.user_id));
                    data.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));
                    data.put(Constant.device_token, UtilsPreferences.getString(RegistrationActivity_Height_Weight.this, Constant.devicegcmid));
                    data.put(Constant.device_type, Constant.device_type_value);
                    data.put(Constant.height, UtilsPreferences.getString(RegistrationActivity_Height_Weight.this, Constant.height));
                    data.put(Constant.height_scale, UtilsPreferences.getString(RegistrationActivity_Height_Weight.this, Constant.height_scale));
                    data.put(Constant.weight, UtilsPreferences.getString(RegistrationActivity_Height_Weight.this, Constant.weight));
                    data.put(Constant.weight_scale, UtilsPreferences.getString(RegistrationActivity_Height_Weight.this, Constant.weight_scale));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                    Update_Height_Weight task = new Update_Height_Weight(RegistrationActivity_Height_Weight.this, onHeightWeightUpdate, data, true);
                    task.execute();
                }
            } else {
                redirectToRegister4();
            }

        }
    }

    // back pressed click event listener
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);

    }

    private Context getContext() {
        // return context of RegistrationActivity_Height_Weight screen
        return RegistrationActivity_Height_Weight.this;
    }

    public void openHeightSelectionDialog(final String title) {
        final Dialog alertDialog = new Dialog(getContext());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_selection_height);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        final EditText et_ft = (EditText) alertDialog.findViewById(R.id.et_ft);
        final EditText et_in = (EditText) alertDialog.findViewById(R.id.et_in);
        final EditText et_cm = (EditText) alertDialog.findViewById(R.id.et_cm);
        TextView tv_done = (TextView) alertDialog.findViewById(R.id.tv_done);
        RadioGroup rgb_selection = (RadioGroup) alertDialog.findViewById(R.id.rgb_selection);
        final RadioButton rb_ft_in = (RadioButton) alertDialog.findViewById(R.id.rb_ft_in);
        final RadioButton rb_ct = (RadioButton) alertDialog.findViewById(R.id.rb_ct);

        final LinearLayout lin_ft_in = (LinearLayout) alertDialog.findViewById(R.id.lin_ft_in);
        final LinearLayout lin_cm = (LinearLayout) alertDialog.findViewById(R.id.lin_cm);

        et_in.setFilters(new InputFilter[]{new InputFilterMinMax("0", "11")}); // here 0 is min and 11 is max value for inch

        tv_popup_title.setTypeface(setFont(getContext(), R.string.app_bold));//

        tv_done.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_ft.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_in.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_cm.setTypeface(setFont(getContext(), R.string.app_regular));//

        tv_popup_title.setText(title);

        if (isCm) {
            rb_ct.setChecked(true);
            lin_ft_in.setVisibility(View.GONE);
            lin_cm.setVisibility(View.VISIBLE);
            if (!str_cm.isEmpty()) {
                et_cm.setText(str_cm);
            }
        } else {
            rb_ft_in.setChecked(true);
            lin_cm.setVisibility(View.GONE);
            lin_ft_in.setVisibility(View.VISIBLE);
            if (!str_ft_in.isEmpty()) {
                if (str_ft_in.contains("-")) {
                    String[] str_arr = str_ft_in.split("-");
                    et_ft.setText(str_arr[0]);
                    et_in.setText(str_arr[1]);
                }
            }
        }

        rgb_selection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                if (checkedId == R.id.rb_ft_in) {
                    lin_cm.setVisibility(View.GONE);
                    lin_ft_in.setVisibility(View.VISIBLE);
                    if (!et_cm.getText().toString().isEmpty()) {
                        String[] str_arr = centimeterToFeet(et_cm.getText().toString()).split("-");
                        et_ft.setText(String.valueOf(str_arr[0]));
                        et_in.setText(String.valueOf(str_arr[1]));
                    } else {
                        et_ft.setText("");
                        et_in.setText("");
                    }
                } else {
                    String val = "";
                    if (!et_ft.getText().toString().isEmpty()) {

                        if (et_in.getText().toString().isEmpty()) {
                            et_in.setText("0");
                        }
                        val = et_ft.getText().toString() + "-" + et_in.getText().toString();
                    } else {
                        val = "";
                    }

                    lin_ft_in.setVisibility(View.GONE);
                    lin_cm.setVisibility(View.VISIBLE);
                    if (!val.isEmpty()) {
                        if (val.contains("-")) {
                            et_cm.setText(feetToCentimeter(val));
                        }
                    } else {
                        et_cm.setText("");
                    }
                }
            }
        });

        alertDialog.show();
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (rb_ft_in.isChecked()) {
                    String ft = et_ft.getText().toString();
                    String in = et_in.getText().toString();
                    if (!ft.isEmpty()) {
                        isCm = false;
                        if (in.isEmpty())
                            in = "0";
                        tv_height.setText(ft + " ft" + " - " + in + " in");
                        str_ft_in = ft + "-" + in;
                        alertDialog.dismiss();
                    }
                } else {
                    if (!et_cm.getText().toString().isEmpty()) {
                        isCm = true;
                        str_cm = et_cm.getText().toString();
                        tv_height.setText(str_cm + " cm");
                        alertDialog.dismiss();
                    }
                }

            }
        });

    }

    // convert feet to cm
    public static String feetToCentimeter(String feet) {
        double dCentimeter = 0d;
        if (!TextUtils.isEmpty(feet)) {
            if (feet.contains("-")) {
                String[] str_arr = feet.split("-");
                String tempfeet = str_arr[0];
                String tempinch = str_arr[1];
                if (!TextUtils.isEmpty(tempfeet)) {
                    dCentimeter += ((Double.valueOf(tempfeet)) * 30.48);
                }
                if (!TextUtils.isEmpty(tempinch)) {
                    dCentimeter += ((Double.valueOf(tempinch)) * 2.54);
                }
            }
        }
        return String.valueOf((int) dCentimeter);
        //Format to decimal digit as per your requirement
    }

    // convert cm to feet
    public static String centimeterToFeet(String centemeter) {
        int feetPart = 0;
        int inchesPart = 0;
        if (!TextUtils.isEmpty(centemeter)) {
            double dCentimeter = Double.valueOf(centemeter);
            feetPart = (int) Math.floor((dCentimeter / 2.54) / 12);
            System.out.println((dCentimeter / 2.54) - (feetPart * 12));
            inchesPart = (int) Math.ceil((dCentimeter / 2.54) - (feetPart * 12));

            // added for converting 5-12 to 6-0
            if (inchesPart == 12) {
                inchesPart = 0;
                feetPart = feetPart + 1;
            }
        }
        return String.valueOf(feetPart) + "-" + String.valueOf(inchesPart);
    }

    public void openWeightSelectionDialog(final String title) {
        final Dialog alertDialog = new Dialog(getContext());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setCancelable(true);
        alertDialog.setContentView(R.layout.dialog_selection_weight);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        final EditText et_kl = (EditText) alertDialog.findViewById(R.id.et_kl);
        final EditText et_pnd = (EditText) alertDialog.findViewById(R.id.et_pnd);
        TextView tv_done = (TextView) alertDialog.findViewById(R.id.tv_done);

        RadioGroup rgb_selection = (RadioGroup) alertDialog.findViewById(R.id.rgb_selection);
        final RadioButton rb_kl = (RadioButton) alertDialog.findViewById(R.id.rb_kl);
        RadioButton rb_pnd = (RadioButton) alertDialog.findViewById(R.id.rb_pnd);

        et_kl.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(100, 3)});
        et_pnd.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(100, 3)});

//        et_pnd.setFilters(new InputFilter[]{new InputFilterDoubleMinMax("0", "2203")}); // here 0 is min and 2203 is max value for inch


        tv_popup_title.setTypeface(setFont(getContext(), R.string.app_bold));//

        tv_done.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_kl.setTypeface(setFont(getContext(), R.string.app_regular));//
        et_pnd.setTypeface(setFont(getContext(), R.string.app_regular));//

        tv_popup_title.setText(title);

        if (isPnd) {
            rb_pnd.setChecked(true);
            et_kl.setVisibility(View.GONE);
            et_pnd.setVisibility(View.VISIBLE);
            if (!str_lb.isEmpty()) {
                et_pnd.setText(str_lb);
            }
        } else {
            rb_kl.setChecked(true);
            et_pnd.setVisibility(View.GONE);
            et_kl.setVisibility(View.VISIBLE);
            if (!str_kg.isEmpty()) {
                et_kl.setText(str_kg);
            }
        }

        rgb_selection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                if (checkedId == R.id.rb_kl) {
                    et_pnd.setVisibility(View.GONE);
                    et_kl.setVisibility(View.VISIBLE);
                    if (!et_pnd.getText().toString().isEmpty()) {
                        double lbs = Double.parseDouble(et_pnd.getText().toString());
                        double kg = (Math.round((lbs * .45359237) * 10) / 10.0);
                        String val = String.format("%.3f", kg);
                        et_kl.setText(val);
                    } else {
                        et_kl.setText("");
                    }
                } else {
                    et_kl.setVisibility(View.GONE);
                    et_pnd.setVisibility(View.VISIBLE);
                    if (!et_kl.getText().toString().isEmpty()) {
                        double kg = Double.parseDouble(et_kl.getText().toString());
                        double lbs = (Math.round((kg / .45359237) * 10) / 10.0);
                        String val = String.format("%.3f", lbs);
                        et_pnd.setText(val);
                    } else {
                        et_pnd.setText("");
                    }
                }
            }
        });

        alertDialog.show();
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (rb_kl.isChecked()) {
                    if (!et_kl.getText().toString().isEmpty()) {
                        isPnd = false;
                        str_kg = et_kl.getText().toString();
                        tv_weight.setText(str_kg + " kg");
                        alertDialog.dismiss();
                    }
                } else {
                    if (!et_pnd.getText().toString().isEmpty()) {
                        isPnd = true;
                        str_lb = et_pnd.getText().toString();
                        tv_weight.setText(str_lb + " lbs");
                        alertDialog.dismiss();
                    }
                }

            }
        });

    }

    // check validation height and weight
    private boolean isValid() {
//        tv_height.setError(null);
//        tv_weight.setError(null);

        if ((tv_height.getText().toString().isEmpty() || tv_height.getText().toString().equalsIgnoreCase(getString(R.string.height)))
                && (tv_weight.getText().toString().isEmpty() || tv_weight.getText().toString().equalsIgnoreCase(getString(R.string.weight)))) {
            Toast.makeText(getContext(), getString(R.string.height_weight_validation_err), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tv_height.getText().toString().isEmpty() || tv_height.getText().toString().equalsIgnoreCase(getString(R.string.height))) {
//            setErrorMsg(tv_height, getString(R.string.height_validation_err));
            Toast.makeText(getContext(), getString(R.string.height_validation_err), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (tv_weight.getText().toString().isEmpty() || tv_weight.getText().toString().equalsIgnoreCase(getString(R.string.weight))) {
//            setErrorMsg(tv_weight, getString(R.string.weight_validation_err));
            Toast.makeText(getContext(), getString(R.string.weight_validation_err), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public static void setErrorMsg(TextView et, String error) {
        String html_body = "<font color='red'>%s</font>";
        et.setError(Html.fromHtml(String.format(html_body, error)));
    }


    public void redirectToRegister4() {
        openPopupDialog();

    }

    // show popup
    public void openPopupDialog() {
        final Dialog alertDialog = new Dialog(RegistrationActivity_Height_Weight.this, android.R.style.Theme_Light);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.chek_mac_popup);
        final LinearLayout ll_back;
        final Button btn_okay;
        final TextView tv_desc;

        ll_back = (LinearLayout) alertDialog.findViewById(R.id.ll_back);
        tv_title = (TextView) alertDialog.findViewById(R.id.tv_title);
        tv_desc = (TextView) alertDialog.findViewById(R.id.tv_desc);
        tv_desc.setMovementMethod(new ScrollingMovementMethod());

        tv_desc.setText(R.string.reg_nic_pop_msg);
        tv_title.setText(R.string.reg_selection);
        ll_back.setVisibility(View.INVISIBLE);

        btn_okay = (Button) alertDialog.findViewById(R.id.btn_okay);
        btn_okay.setText(R.string.got_it);
        btn_okay.setTypeface(setFont(getApplicationContext(), R.string.app_bold));//.setTypeface(setFont(getContext(), R.string.app_bold));


        btn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(RegistrationActivity_Height_Weight.this, RegistrationActivity_4.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

            }
        });

        alertDialog.show();
    }

    // set data for height and weight
    private void loadHeightWeightData() {
        if (UtilsPreferences.getString(getContext(), Constant.height_scale) != null
                && UtilsPreferences.getString(getContext(), Constant.height) != null
                && UtilsPreferences.getString(getContext(), Constant.weight_scale) != null
                && UtilsPreferences.getString(getContext(), Constant.weight) != null) {

            if (UtilsPreferences.getString(getContext(), Constant.height_scale).equalsIgnoreCase("1")) {
                String[] str_arr = UtilsPreferences.getString(getContext(), Constant.height).split("-");
                tv_height.setText(str_arr[0] + " ft" + " - " + str_arr[1] + " in");
                str_ft_in = str_arr[0] + "-" + str_arr[1];
                isCm = false;

            } else if (UtilsPreferences.getString(getContext(), Constant.height_scale).equalsIgnoreCase("2")) {
                tv_height.setText(UtilsPreferences.getString(getContext(), Constant.height) + " cm");
                str_cm = UtilsPreferences.getString(getContext(), Constant.height);
                isCm = true;
            }

            if (UtilsPreferences.getString(getContext(), Constant.weight_scale).equalsIgnoreCase("1")) {
                str_kg = UtilsPreferences.getString(getContext(), Constant.weight);
                tv_weight.setText(UtilsPreferences.getString(getContext(), Constant.weight) + " kg");
                isPnd = false;

            } else if (UtilsPreferences.getString(getContext(), Constant.weight_scale).equalsIgnoreCase("2")) {
                str_lb = UtilsPreferences.getString(getContext(), Constant.weight);
                tv_weight.setText(UtilsPreferences.getString(getContext(), Constant.weight) + " lbs");
                isPnd = true;
            }
        }

    }

    // save height and weight data in shared prefrences
    private void saveInPrefrences() {
        if (isCm) {
            UtilsPreferences.setString(getContext(), Constant.height, str_cm);
            UtilsPreferences.setString(getContext(), Constant.height_scale, "2");
        } else {
            UtilsPreferences.setString(getContext(), Constant.height, str_ft_in);
            UtilsPreferences.setString(getContext(), Constant.height_scale, "1");
        }

        if (isPnd) {
            UtilsPreferences.setString(getContext(), Constant.weight, str_lb);
            UtilsPreferences.setString(getContext(), Constant.weight_scale, "2");
        } else {
            UtilsPreferences.setString(getContext(), Constant.weight, str_kg);
            UtilsPreferences.setString(getContext(), Constant.weight_scale, "1");
        }
    }

    // success listener of Update_Height_Weight
    @Override
    public void onSucceedToHeightWeightUpdate(String msg) {

        // call addSDkFunctionOrder added on 22-07-2017
//        UtilsCommon.addSDkFunctionOrder(RegistrationActivity_Height_Weight.this);

        Toast.makeText(RegistrationActivity_Height_Weight.this, "" + msg, Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    // failed listener of Update_Height_Weight
    @Override
    public void onFaildToHeightWeightUpdate(String error_msg) {
        Toast.makeText(RegistrationActivity_Height_Weight.this, "" + error_msg, Toast.LENGTH_SHORT).show();
    }

    private void setFilter(final EditText et) {
        et.setFilters(new InputFilter[]{
                new DigitsKeyListener(Boolean.FALSE, Boolean.TRUE) {
                    int beforeDecimal = 3, afterDecimal = 2;

                    @Override
                    public CharSequence filter(CharSequence source, int start, int end,
                                               Spanned dest, int dstart, int dend) {
                        String temp = et.getText() + source.toString();

                        if (temp.equals(".")) {
                            return "0.";
                        } else if (temp.toString().indexOf(".") == -1) {
                            // no decimal point placed yet
                            if (temp.length() > beforeDecimal) {
                                return "";
                            }
                        } else {
                            temp = temp.substring(temp.indexOf(".") + 1);
                            if (temp.length() > afterDecimal) {
                                return "";
                            }
                        }

                        return super.filter(source, start, end, dest, dstart, dend);
                    }
                }
        });
    }

    InputFilter filter = new InputFilter() {
        final int maxDigitsBeforeDecimalPoint = 3;
        final int maxDigitsAfterDecimalPoint = 2;

        @Override
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            StringBuilder builder = new StringBuilder(dest);
            builder.replace(dstart, dend, source
                    .subSequence(start, end).toString());
            if (!builder.toString().matches(
                    "(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?"

            )) {
                if (source.length() == 0)
                    return dest.subSequence(dstart, dend);
                return "";
            }

            return null;

        }
    };

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_HEIGHT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                isCm = data.getBooleanExtra("iscm", false);
                tv_height.setText(data.getStringExtra("result"));
                if (isCm) {
                    str_cm = data.getStringExtra("result_value");
                } else {
                    str_ft_in = data.getStringExtra("result_value");
                }

            }
        } else if (requestCode == PICK_WEIGHT_REQUEST) {
            if (resultCode == RESULT_OK) {
                isPnd = data.getBooleanExtra("ispnd", false);
                tv_weight.setText(data.getStringExtra("result"));
                if (isPnd) {
                    str_lb = data.getStringExtra("result_value");
                } else {
                    str_kg = data.getStringExtra("result_value");
                }

            }
        }

    }
}
