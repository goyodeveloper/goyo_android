package com.differenzsystem.goyog.userProfile;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.gcmNotification.GCMClientManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.FitnessBandTracker.syncdevice;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.ChallengeAcceptCall;
import com.differenzsystem.goyog.api.ChallengeAcceptCall.OnChallengeAcceptListener;
import com.differenzsystem.goyog.api.FitnessTrackerOperations;
import com.differenzsystem.goyog.api.ForceUpdate;
import com.differenzsystem.goyog.api.UpdateDeviceToken;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.dashboard.challenges.WonActivity;
import com.differenzsystem.goyog.utility.Comparator;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.veryfit.multi.nativeprotocol.ProtocolUtils;
import com.veryfit.multi.share.BleSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;

import static java.text.DateFormat.getTimeInstance;

public class SplashActivity extends AppCompatActivity implements ConnectionCallbacks, OnChallengeAcceptListener,
        syncdevice.Onsyncdevice, FitnessTrackerOperations.FitnessTrackerOperationslistener, ForceUpdate.OnForceUpdateListener, UpdateDeviceToken.OnUpdateDeviceTokenListener {
    private GCMClientManager pushClientManager;
    public static GoogleApiClient mClient = null;
    Bundle extra = null;
    String caloriesData = "0", stepsData = "0", distanceData = "0", heartRateData = "0";
    int cal = 0, step = 0, heart_rate = 0, sleep = 0;
    float distance = 0;
    Intent notificationIntent;
    OnChallengeAcceptListener onChallengeAcceptListener;
    syncdevice.Onsyncdevice onsyncdevice;
    FitnessTrackerOperations.FitnessTrackerOperationslistener fitnessTrackerOperationslistener;
    ForceUpdate.OnForceUpdateListener onForceUpdateListener;
    UpdateDeviceToken.OnUpdateDeviceTokenListener onUpdateDeviceTokenListener;

    JSONObject notification_data;
    String validic_result = "";
    String TAG = getClass().getName();
    int type;
    Boolean onPage = true;

    public static SplashActivity mContext;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @RequiresApi(api = VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //change
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        getSupportActionBar().hide();
        onChallengeAcceptListener = this;
        onsyncdevice = this;
        fitnessTrackerOperationslistener = this;
        onForceUpdateListener = this;
        onUpdateDeviceTokenListener = this;

        if (UtilsPreferences.getString(SplashActivity.this, Constant.connect_flag) != null) {
            if (UtilsPreferences.getString(SplashActivity.this, Constant.user_type).equalsIgnoreCase("1")) {
                if (UtilsPreferences.getString(this, Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
//                    bluetooth.enable(); // commented on 24-07-2017
                    ProtocolUtils.getInstance().setCanConnect(true);
                }
            }
        }
        //upgradeVersion();
    }

    public void goForward() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 200ms
                init();
            }
        }, 200);
    }

    @Override
    protected void onResume() {
        super.onResume();
        upgradeVersion();
//        updateDeviceToken();
        Application.recordScreenViews(SplashActivity.this, Constant.Name_Splash);
    }

    // api call for force update
    public void upgradeVersion() {
        try {
            if (ConnectionDetector.internetCheck(SplashActivity.this))
                new ForceUpdate(getContext(), onForceUpdateListener, false).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // api call for device token update
    public void updateDeviceToken() {

        if (UtilsPreferences.getString(getContext(), Constant.user_id) != null && !(UtilsPreferences.getString(getContext(), Constant.user_id).isEmpty())) {
            try {
                JSONObject data = new JSONObject();
                data.put(Constant.user_id, UtilsPreferences.getString(getContext(), Constant.user_id));
                data.put(Constant.device_token, UtilsPreferences.getString(getContext(), Constant.devicegcmid));
                data.put(Constant.device_type, Constant.device_type_value);
                if (ConnectionDetector.internetCheck(SplashActivity.this))
                    new UpdateDeviceToken(getContext(), onUpdateDeviceTokenListener, data, false).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    // force update popup
    public void ForceUpdatePopup() {
        final Dialog alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.setting_edit_name_dialog);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        alertDialog.setCancelable(false);

        final EditText edt_name = (EditText) alertDialog.findViewById(R.id.et_name);
        TextView tv_cancel = (TextView) alertDialog.findViewById(R.id.tv_cancel);
        TextView tv_change = (TextView) alertDialog.findViewById(R.id.tv_change);
        TextView tv_popup_title = (TextView) alertDialog.findViewById(R.id.tv_popup_title);
        TextView tv_wrn_text = (TextView) alertDialog.findViewById(R.id.tv_wrn_text);
        View vi_devider = (View) alertDialog.findViewById(R.id.vi_devider);

        edt_name.setVisibility(View.GONE);
        tv_cancel.setVisibility(View.GONE);
        vi_devider.setVisibility(View.GONE);
        tv_wrn_text.setVisibility(View.VISIBLE);

        tv_change.setText(R.string.update);
        tv_popup_title.setText(R.string.new_version);
        tv_wrn_text.setText(R.string.update_text);//R.string.sync_warning

        alertDialog.show();
        tv_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
    }

    // success listener of ForceUpdate and show update app popup if update is available
    @Override
    public void onSucceedForceUpdate(JSONObject jsonResponse) {
        try {
            String ServerVersion = jsonResponse.getString("android_version");
            int flag = Integer.parseInt(jsonResponse.getString("flag"));

            if (flag != 0) {
                if (Comparator.isVersionDownloadableNewer(this, ServerVersion)) {
                    Log.e("update avilable", "update avilable");
                    //open popup for update version
                    ForceUpdatePopup();
                } else {
                    Log.e("up to date", "up to date");
                    goForward();
                }
            } else {
                goForward();
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    // failed listener of ForceUpdate
    @Override
    public void onFailedForceUpdate(String message) {
        goForward();
    }

    // initialize when screen onCreate
    public void init() {
        mContext = this;
        //TODO  Enable fabric when go live
        Fabric.with(this, new Crashlytics());

        Debugger.debugE("DeviceGcmId", UtilsPreferences.getString(this, Constant.devicegcmid, ""));
        extra = getIntent().getExtras();

        if (extra == null) {
//            if (UtilsPreferences.getString(SplashActivity.this, Constant.devicegcmid, "").equals(""))
//                getDeviceId(true);
//            else {
            performOpration(Constant.MM_SplashTime);
            //  }
        } else {
            if (UtilsPreferences.getString(SplashActivity.this, Constant.devicegcmid, "").equals(""))
                getDeviceId(false);

            if (UtilsPreferences.getString(SplashActivity.this, Constant.email) != null) {
                if (extra.containsKey(Constant.fromNotificationFlag)) {
                    JSONObject notification = null;

                    //New challange added for all users
                    String isForAllUser = "";
                    try {
                        notification = new JSONObject(extra.getString(Constant.notification));
                        if (notification.has(Constant.NotificationType))
                            isForAllUser = notification.getString(Constant.NotificationType);

                        if (notification.has(Constant.notificationdata)) {
                            notification_data = new JSONObject(notification.getString(Constant.notificationdata));

                            if (notification_data.has(Constant.NotificationType))
                                isForAllUser = notification_data.getString(Constant.NotificationType);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (!isForAllUser.isEmpty()) {
                        if (isForAllUser.equalsIgnoreCase(Constant.NewChallange)) {
                            commanIntent(notification);
                        } else if (isForAllUser.equalsIgnoreCase(Constant.periodical_notification)) {
                            commanIntent(notification);
                        } else if (isForAllUser.equalsIgnoreCase(Constant.Upcomming_Challanges)) {
                            commanIntent(notification);
                        } else if (isForAllUser.equalsIgnoreCase(Constant.Maximum_user)) {
                            commanIntent(notification);
                        } else if (isForAllUser.equalsIgnoreCase(Constant.win_offer_expier)) {
                            commanIntent(notification);
                        } else if (isForAllUser.equalsIgnoreCase(Constant.demo_user_nitification)) {
                            buyWatch("http://www.goyo.lk/");
                        } else if (isForAllUser.equalsIgnoreCase(Constant.manually_win_nitification)) {
                            commanIntent(notification);
                        } else if (isForAllUser.equalsIgnoreCase(Constant.manually_lose_nitification)) {
                            commanIntent(notification);
                        } else if (isForAllUser.equalsIgnoreCase(Constant.sync_band_request_notification)) {
                            commanIntent(notification);
                        }
                    } else if (notification != null && notification.has(Constant.title) && notification.has(Constant.body)) {
                        performOpration(Constant.MM_SplashTime);
                    } else if (UtilsPreferences.getString(SplashActivity.this, Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                        // Check if type device, tracker or validic
                        // Type device
                        try {
                            notification_data = new JSONObject(extra.getString(Constant.notificationdata));
                            performNotificationOpration(notification_data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (UtilsPreferences.getString(SplashActivity.this, Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                        // Type tracker
                        try {
                            //notification_data = new JSONObject(extra.getString(Constant.notificationdata));
                            notification_data = new JSONObject(notification.getString(Constant.notificationdata));
                            performNotificationOpration(notification_data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        // Type validic
                        try {

                            notification_data = new JSONObject(extra.getString(Constant.notificationdata));
                            validic_result = extra.getString(Constant.validic_result);
                            if (validic_result.equalsIgnoreCase("1")) {
                                notificationIntent = new Intent(SplashActivity.this, WonActivity.class);
                                notificationIntent.putExtra(Constant.fromNotificationFlag, true);
                                notificationIntent.putExtra(Constant.notificationdata, String.valueOf(notification_data));
                                startActivity(notificationIntent);
                                finish();
                            } else {
                                notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                        .putExtra(Constant.fromNotificationFlag, true)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(notificationIntent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    performOpration(Constant.MM_SplashTime);
                }
            } else {
                performOpration(Constant.MM_SplashTime);
            }
        }
    }

    // open dashborad screen
    public void commanIntent(JSONObject notification) {
        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class);
        notificationIntent.putExtra(Constant.fromNotificationFlag, true); // send notification flag through intent
        notificationIntent.putExtra(Constant.notification, String.valueOf(notification)); // send notification type through intent
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(notificationIntent);
        finish();
    }

    // open goyo url screen
    public void buyWatch(String address) {
        String url = address;//"http://www.goyo.lk/"

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    public void forceCrash() {
        throw new RuntimeException("This crash is done forcefully for test.");
    }

    private Context getContext() {
        // return context of splash screen
        return SplashActivity.this;
    }

    /**
     * @param mmSplashtime splash time
     */
    public void performOpration(int mmSplashtime) {
        //if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (UtilsPreferences.getString(SplashActivity.this, Constant.email) != null && !UtilsPreferences.getString(SplashActivity.this, Constant.email).isEmpty()) {
                    if (UtilsPreferences.getString(SplashActivity.this, Constant.connect_flag).equalsIgnoreCase(Constant.not_connected)) {
                        Intent i_login = new Intent(getContext(), GetStartedActivity.class);
                        startActivity(i_login);
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        finish();
                    } else {
                        Intent i_login = new Intent(getContext(), DashboardActivity.class);
                        startActivity(i_login);
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        finish();
                    }
                } else {
                    if (!UtilsPreferences.getBoolean(SplashActivity.this, Constant.tutorial_flag, false)) {
                        Intent i_login = new Intent(getContext(), TutorialActivity.class);
                        startActivity(i_login);
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        finish();
                    } else {
                        Intent i_login = new Intent(getContext(), LoginActivity.class);
                        startActivity(i_login);
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        finish();
                    }
                }
            }
        }, mmSplashtime);
        /*} else
            showTryAgain();*/
    }

    public void getDeviceId(final boolean performOp) {
        try {

            /*pushClientManager = new GCMClientManager(this, Constant.SENDER_ID);
            pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                @Override
                public void onsuccess(String registrationId, boolean isNewRegistration) {
                    // TODO Auto-generated method stub
                    String regId = registrationId;
                    UtilsPreferences.setString(SplashActivity.this, Constant.devicegcmid, regId);
                    Debugger.debugE("REgId", registrationId);
                    if (performOp)
                        performOpration(0);
                }

                @Override
                public void onFailure(String e) {
                    super.onFailure(e);
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showTryAgain() {
        // Internet connection is not present
        //Debugger.debugE(TAG, "show Try again overlay & show internet dialog");
        ConnectionDetector.internetCheck(getContext());
    }

    public String getDeviceUUID() {
        String deivceUUID = "";
        try {
            deivceUUID = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            Debugger.debugE("Splash_deviceUUID", deivceUUID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deivceUUID;
    }

    public void performNotificationOpration(JSONObject data) {
        try {
            Debugger.debugE("Type..", data.getInt(Constant.challenge_type) + " ");
            //int type = data.getInt(Constant.challenge_type);
            type = data.getInt(Constant.challenge_type);
            if (ConnectionDetector.internetCheck(SplashActivity.this)) {

                if (UtilsPreferences.getString(this, Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    getHealthData();
                    //buildGOYOHR(type);
                } else {
                    buildFitnessClient(type);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void buildGOYOHR(int type) {
        //Debugger.debugE("Build..", type + "  ");
        try {
            // JSONObject obj = new JSONObject(extra.getString(Constant.notificationdata));
            //JSONObject obj = notification_data;

            String startTimeString = "";
            int duration = notification_data.getInt(Constant.duration);
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

/*
            if (!notification_data.getString(Constant.challenge_start_date).equalsIgnoreCase("null")) {
                if (notification_data.getString(Constant.challenge_start_date).equalsIgnoreCase("0000-00-00 00:00:00") ||
                        notification_data.getString(Constant.challenge_start_date) == null) {
                    startTimeString = notification_data.getString(Constant.accept_date);
                } else {
                    startTimeString = notification_data.getString(Constant.challenge_start_date);
                }
            } else {
                startTimeString = notification_data.getString(Constant.accept_date);
            }
*/
            startTimeString = notification_data.getString(Constant.accept_date);

            Date startDate = null;
            try {
                startDate = formater.parse(startTimeString);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar endCalendar = Calendar.getInstance();
            Date endDate = startDate;
            endCalendar.setTime(endDate);
            //int duration = Integer.parseInt(obj_Challenges.getDuration());
            endCalendar.add(Calendar.MINUTE, duration);
            //Date end = endCalendar.getTime();

            String chlngStartDate = formater.format(startDate);
            String chlngEndDate = formater.format(endCalendar.getTime());//
            Date endDiff = endCalendar.getTime();

            FitnessTrackerOperations fitnessTrackerOperations = new FitnessTrackerOperations(getContext(), fitnessTrackerOperationslistener);
            fitnessTrackerOperations.readDatafromTracker(chlngStartDate, chlngEndDate, type);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // sucess listener of FitnessTrackerOperations
    @Override
    public void onSucceedtoFitnessTrackerOperation(int steps, int cals, int dis, int hearts, int sleep) {
        this.cal = cals;
        this.step = steps;
        this.distance = dis;
        this.sleep = sleep;

        NotificationIntent(type);

    }

    // create GoogleApiClient
    private void buildFitnessClient(final int type) {
        Debugger.debugE("Build..", type + "  ");
        mClient = new GoogleApiClient.Builder(SplashActivity.this)
                .addApi(Fitness.HISTORY_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
                .addScope(new Scope(Scopes.FITNESS_BODY_READ))
                .addConnectionCallbacks(this).build();

        mClient.connect();
        switch (type) {
            case 1://steps
                new StepsCall().execute();
                break;
            case 2://calories
                new CaloriesCall().execute();
                break;
            case 3://distance
                new DistanceCall().execute();
                break;
            case 4://heart_date
                new HeartRateCall().execute();
                break;
            case 6://steps and Distance
                new StepsCall().execute();
                break;
            case 7://steps and calories
                new StepsCall().execute();
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mClient.connect();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mClient.disconnect();
    }

    @Override
    public void onSucceedUpdateDeviceToken(String jsonResponse) {
        Log.d("updatedevicetoken : ", "success");
    }

    @Override
    public void onFailedUpdateDeviceToken(String message) {
        Log.e("updatedevicetoken : ", "failed");
    }

    // api caloriecall
    private class CaloriesCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                caloriesData = readData(dataReadResult);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                cal = Math.round(Float.parseFloat(caloriesData.equalsIgnoreCase("") ? "0" : caloriesData));
                if (notification_data.getInt(Constant.challenge_type) == 7) {
                    NotificationIntent(7);
                } else {
                    NotificationIntent(2);
                }
                //tv_caloriesData.setText(String.valueOf(cal));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // api stepscall
    private class StepsCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                Debugger.debugE("Steps..", "inside");
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                stepsData = readData(dataReadResult);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                Debugger.debugE("ONPost...", stepsData);
                step = Integer.parseInt(stepsData.equalsIgnoreCase("") ? "0" : stepsData);
                if (notification_data.getInt(Constant.challenge_type) == 7) {
                    new CaloriesCall().execute();
                } else if (notification_data.getInt(Constant.challenge_type) == 6) {
                    new DistanceCall().execute();
                } else {
                    NotificationIntent(1);
                }
                //tv_stepsData.setText(stepsData.equalsIgnoreCase("") ? "0" : stepsData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // api distance call
    private class DistanceCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                distanceData = readData(dataReadResult);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                distance = UtilsCommon.getMiles(Float.parseFloat(distanceData.equalsIgnoreCase("") ? "0" : distanceData));
                Debugger.debugE("Distance..", distanceData);
                if (notification_data.getInt(Constant.challenge_type) == 6)
                    NotificationIntent(6);
                else
                    NotificationIntent(3);
                //tv_distanceData.setText(distanceData.equalsIgnoreCase("") ? "0.0 mile" : String.valueOf(UtilsCommon.getMiles(Float.parseFloat(distanceData))));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // api heartrate call
    private class HeartRateCall extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            try {
                DataReadRequest readRequest = requestFitnessData(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY);
                DataReadResult dataReadResult = Fitness.HistoryApi.readData(mClient, readRequest).await(1, TimeUnit.MINUTES);
                heartRateData = readData(dataReadResult);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                heart_rate = Integer.parseInt(heartRateData.equalsIgnoreCase("") ? "0" : heartRateData);
                NotificationIntent(4);
                //tv_heartRateData.setText(heartRateData.equalsIgnoreCase("") ? "0 bpm" : heartRateData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private DataReadRequest requestFitnessData(DataType datatype1, DataType datatype2) {
        DataReadRequest readRequest = null;
        try {
            JSONObject obj = new JSONObject(extra.getString(Constant.notificationdata));
            Calendar startCalendar = Calendar.getInstance();
            long startTime = startCalendar.getTimeInMillis();

            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss");
            String startTimeString = null;
            if (obj.getString(Constant.challenge_start_date).equalsIgnoreCase("0000-00-00 00:00:00") ||
                    obj.getString(Constant.challenge_start_date) == null) {
                startTimeString = obj.getString(Constant.accept_date);
            } else {
                startTimeString = obj.getString(Constant.challenge_start_date);
            }
            Date startDate = null;
            try {
                startDate = formater.parse(startTimeString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long startMillisecond = startDate.getTime();

            Calendar endCalendar = Calendar.getInstance();
            Date endDate = startDate;
            endCalendar.setTime(endDate);
            int duration = obj.getInt(Constant.duration);
            endCalendar.add(Calendar.MINUTE, duration);
            Date end = endCalendar.getTime();
            long endMillisecond = end.getTime();

            Debugger.debugE("splash-Start time", startDate + "=>");
            Debugger.debugE("splash-End time", end + "=>");

            readRequest = new DataReadRequest.Builder()
                    .aggregate(datatype1, datatype2)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(startMillisecond, endMillisecond, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return readRequest;
    }

    public String readData(DataReadResult dataReadResult) {
        String value = "";
        Debugger.debugE("REadDAte..", dataReadResult.toString());
        try {
            if (dataReadResult.getBuckets().size() > 0) {
                for (Bucket bucket : dataReadResult.getBuckets()) {
                    List<DataSet> dataSets = bucket.getDataSets();
                    for (DataSet dataSet : dataSets) {
                        value = getDataSet(dataSet);
                    }
                }
            } else if (dataReadResult.getDataSets().size() > 0) {
                for (DataSet dataSet : dataReadResult.getDataSets()) {
                    value = getDataSet(dataSet);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private String getDataSet(DataSet dataSet) {
        String value = "";
        try {
            DateFormat dateFormat = getTimeInstance();
            for (DataPoint dp : dataSet.getDataPoints()) {
                Debugger.debugE(TAG, "Data point:");
                Debugger.debugE(TAG, "\tType: " + dp.getDataType().getName());
                Debugger.debugE(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Debugger.debugE(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                for (Field field : dp.getDataType().getFields()) {
                    Debugger.debugE(TAG, "\tField: " + field.getName() +
                            " Value: " + dp.getValue(field));
                    value = String.valueOf(dp.getValue(field));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    // set notification intent
    public void NotificationIntent(int type) {
        try {
            notificationIntent = null;
            int challenge_value = Integer.parseInt(notification_data.getString(Constant.challenge_value));
            String status;
            switch (type) {
                case 1://step
                    Debugger.debugE("Splash_Data", step + "  " + cal + "  " + distance + "  " + heart_rate);
                    if (step < challenge_value) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 2: //calories
                    if (cal < challenge_value) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 3://distance
                    if (distance < challenge_value) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 4://sleep
                    if (sleep < challenge_value) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
//                    if (heart_rate < challenge_value) {
//                        status = "4";
//                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
//                                .putExtra(Constant.fromNotificationFlag, true)
//                                .putExtra("ChallangeComplete", "ChallangeComplete")
//                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
//                    } else {
//                        status = "2";
//                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
//                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
//                    }
                    break;
                case 5://distance and sleep
                    if ((distance < challenge_value
                            || sleep < Integer.parseInt(notification_data.getString(Constant.second_challenge_value)))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 6://step and distance
                    if (step < challenge_value
                            || distance < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 7://step and calories
                    if (step < challenge_value
                            || cal < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 8://step and sleep
                    if (step < challenge_value
                            || sleep < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 9://sleep and calories
                    if (sleep < challenge_value
                            || cal < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 10://distance and calories
                    if (distance < challenge_value
                            || cal < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 11://steps and calories and distance
                    if (step < challenge_value
                            || cal < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))
                            || distance < Integer.parseInt(notification_data.getString(Constant.third_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 12://steps and calories nad sleep
                    if (step < challenge_value
                            || cal < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))
                            || sleep < Integer.parseInt(notification_data.getString(Constant.third_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 13://steps and distance and sleep
                    if (step < challenge_value
                            || distance < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))
                            || sleep < Integer.parseInt(notification_data.getString(Constant.third_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 14://calories and distance and sleep
                    if (cal < challenge_value
                            || distance < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))
                            || sleep < Integer.parseInt(notification_data.getString(Constant.third_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;
                case 15://steps and calories and distance and sleep
                    if (step < challenge_value
                            || cal < Integer.parseInt(notification_data.getString(Constant.second_challenge_value))
                            || distance < Integer.parseInt(notification_data.getString(Constant.third_challenge_value))
                            || sleep < Integer.parseInt(notification_data.getString(Constant.fourth_challenge_value))) {
                        status = "4";
                        notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                                .putExtra(Constant.fromNotificationFlag, true)
                                .putExtra("ChallangeComplete", "ChallangeComplete")
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        status = "2";
                        notificationIntent = new Intent(SplashActivity.this, WonActivity.class)
                                .putExtra(Constant.notificationdata, String.valueOf(notification_data));
                    }
                    break;

                default:
                    status = "4";
                    notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class)
                            .putExtra(Constant.fromNotificationFlag, true)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    break;
            }
            UpdateChallageStatus(status);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // api call for ChallengeAccept
    public void UpdateChallageStatus(String status) {
        try {
            JSONObject object = new JSONObject();
            object.put(Constant.user_id, UtilsPreferences.getString(getContext(), Constant.user_id));
            object.put(Constant.accesstoken, UtilsPreferences.getString(getContext(), Constant.accesstoken));
            object.put(Constant.challenge_id, notification_data.getString(Constant.challenge_id));
            object.put(Constant.challenge_status, status);

            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            object.put(Constant.timezone, tz.getID());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateandTime = sdf.format(new Date());
            object.put(Constant.date, currentDateandTime);

            new ChallengeAcceptCall(getContext(), onChallengeAcceptListener, object, false).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // sucess listener of ChallengeAcceptCall
    @Override
    public void onSucceedChallengeAccept(String message, JSONObject jsonResponse) {

        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(SplashActivity.this, Constant.Key_inProgressList);

        Toast.makeText(SplashActivity.this, message, Toast.LENGTH_SHORT).show();
        startActivity(notificationIntent);
        this.finish();
    }

    // failed listener of ChallengeAcceptCall
    @Override
    public void onFailedChallengeAccept(String message) {
        // clear challenge prefrences added on n21-07-2017
        UtilsPreferences.clearKeyPreferences(SplashActivity.this, Constant.Key_inProgressList);
        Toast.makeText(SplashActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    // api call of syncDevice
    public void getHealthData() {
        if (BleSharedPreferences.getInstance().getIsBind()) {
            if (ProtocolUtils.getInstance().isAvailable() == ProtocolUtils.SUCCESS) {
                syncdevice s = new syncdevice(getContext(), onsyncdevice);
                s.syncdata();
            } else {
                notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class);
                startActivity(notificationIntent);
                finish();
            }
        } else {
            notificationIntent = new Intent(SplashActivity.this, DashboardActivity.class);
            startActivity(notificationIntent);
            finish();
        }

    }

    public static SplashActivity getInstance() {
        // return the context of current screen
        return mContext;
    }

    // success listener of syncdevice
    @Override
    public void onSucceedsyncdevice() {
        if (onPage) {
            buildGOYOHR(type);
        }
    }

    // screen ondestroy call event
    @Override
    protected void onDestroy() {
        super.onDestroy();
        onPage = false;
    }
}