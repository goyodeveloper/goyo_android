package com.differenzsystem.goyog.userProfile;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.differenzsystem.goyog.utility.UtilsValidation;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

public class RegistrationActivity_4 extends AppCompatActivity implements View.OnClickListener {
    LinearLayout ll_back;
    TextView tv_title;
    EditText et_nic, et_name, et_address, et_contact_number;
    Button btn_all_set;
    CheckBox chk_term_condition;
    RelativeLayout rl_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity_4);

        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(RegistrationActivity_4.this, Constant.Name_Registration_Step4);
    }

    // initialize all controls define in xml layout
    void initializeControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(R.string.reg_title4);

            et_nic = (EditText) findViewById(R.id.et_nic);
            et_address = (EditText) findViewById(R.id.et_address);
            et_contact_number = (EditText) findViewById(R.id.et_contact_number);
            et_name = (EditText) findViewById(R.id.et_name);

            btn_all_set = (Button) findViewById(R.id.btn_all_set);
            chk_term_condition = (CheckBox) findViewById(R.id.chk_term_condition);

            rl_main = (RelativeLayout) findViewById(R.id.rl_main);

            et_nic.setTypeface(setFont(getContext(), R.string.app_regular));
            et_address.setTypeface(setFont(getContext(), R.string.app_regular));
            et_contact_number.setTypeface(setFont(getContext(), R.string.app_regular));
            et_name.setTypeface(setFont(getContext(), R.string.app_regular));

            btn_all_set.setTypeface(setFont(getContext(), R.string.app_bold));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // set click event listener
    public void initializeControlsAction() {
        try {
            btn_all_set.setOnClickListener(this);
            ll_back.setOnClickListener(this);
            chk_term_condition.setOnClickListener(this);
            rl_main.setOnClickListener(this);
            et_nic.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(12)});

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_all_set:
                if (isValidated(true)) {
                    storeData();

                }
                break;
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.chk_term_condition:

                break;

            case R.id.rl_main:
                hidError();
                break;
        }
    }

    // hide error
    public void hidError() {
        UtilsCommon.hideSoftKeyboard(this);

        et_nic.setFocusable(false);
        et_nic.setFocusableInTouchMode(false);
        et_name.setFocusable(false);
        et_name.setFocusableInTouchMode(false);
        et_address.setFocusable(false);
        et_address.setFocusableInTouchMode(false);
        et_contact_number.setFocusable(false);
        et_contact_number.setFocusableInTouchMode(false);

        et_nic.setFocusable(true);
        et_nic.setFocusableInTouchMode(true);
        et_name.setFocusable(true);
        et_name.setFocusableInTouchMode(true);
        et_address.setFocusable(true);
        et_address.setFocusableInTouchMode(true);
        et_contact_number.setFocusable(true);
        et_contact_number.setFocusableInTouchMode(true);
    }

    // save nic, name, address and contact no in shared prefrences
    public void storeData() {
//        RegistrationActivity.RegDetail.put("NomineeNIC", et_nic.getText().toString().trim());
//        RegistrationActivity.RegDetail.put("NomineeName", et_name.getText().toString().trim());
//        RegistrationActivity.RegDetail.put("NomineeAddress", et_address.getText().toString().trim());
//        RegistrationActivity.RegDetail.put("NomineeContact", et_contact_number.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.nominee_nic, et_nic.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.nominee_name, et_name.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.nominee_address, et_address.getText().toString().trim());
        UtilsPreferences.setString(getContext(), Constant.nominee_number, et_contact_number.getText().toString().trim());

        startActivity(new Intent(this, RegistrationActivity_Allset.class));
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // handle back click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private Context getContext() {
        // return the context of RegistrationActivity_4 screen
        return RegistrationActivity_4.this;
    }

    // check validation of nic, name, address and contact no

    /**
     * @param isShowError boolean for showing error
     * @return true or false
     */
    public boolean isValidated(boolean isShowError) {
        boolean isValid = true;
        if (!UtilsValidation.isValidNICNominee(getContext(), et_nic, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidName(getContext(), et_name, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidAddress(getContext(), et_address, isShowError)) {
            isValid = false;
        }

        if (!UtilsValidation.isValidContactNo(getContext(), et_contact_number, isShowError)) {
            isValid = false;
        }

        return isValid;
    }
}
