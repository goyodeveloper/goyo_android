package com.differenzsystem.goyog.userProfile;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.FitnessBandTracker.ScanDeviceActivity;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.ValidicCreateUserCall;
import com.differenzsystem.goyog.api.ValidicCreateUserCall.OnCreateValidicUserListener;
import com.differenzsystem.goyog.api.ValidicUpdateDataCall;
import com.differenzsystem.goyog.api.ValidicUpdateDataCall.OnUpdateValidicDataListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.model.ValidicCreateUserModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Globals;
import com.differenzsystem.goyog.utility.UtilsPreferences;

import org.json.JSONObject;

/**
 * Created by Union Assurance PLC on 12/8/16.
 */

public class GetStartedActivity extends AppCompatActivity implements View.OnClickListener, OnCreateValidicUserListener, OnUpdateValidicDataListener {

    TextView tv_connect_validic, tv_connect_phone, tv_connect_tracker;
    OnCreateValidicUserListener onCreateValidicUserListener;
    OnUpdateValidicDataListener onUpdateValidicDataListener;
    boolean newValidicUserFlag = true;
    LinearLayout ll_back, ll_header;
    TextView tv_title;


    /**
     * Note
     * usertype 1 = live user
     * usertype 2 = demo user
     * connect_flag = connected_with_validic means get data from validic
     * connect_flag = connected_with_device means get data from google fit
     * connect_flag = connected_with_tracker means get data from band
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_started_activity);
        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private Context getContext() {
        // return getstarted screen context
        return GetStartedActivity.this;
    }

    // initialize all controls define in xml layout
    public void initializeControls() {
        try {
            Intent intent = getIntent();
            tv_connect_phone = (TextView) findViewById(R.id.tv_connect_phone);
            tv_connect_validic = (TextView) findViewById(R.id.tv_connect_validic);
            tv_connect_tracker = (TextView) findViewById(R.id.tv_connect_tracker);
            if (!intent.getBooleanExtra(Constant.isFromSetting, false)) {
                UtilsPreferences.setString(GetStartedActivity.this, Constant.connect_flag, Constant.not_connected);
            } else {
                ll_back = (LinearLayout) findViewById(R.id.ll_back);
                ll_header = (LinearLayout) findViewById(R.id.ll_header);
                tv_title = (TextView) findViewById(R.id.tv_title);
                ll_header.setVisibility(View.VISIBLE);
                tv_title.setText(getString(R.string.sync));
                ll_back.setOnClickListener(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            tv_connect_phone.setOnClickListener(this);
            tv_connect_validic.setOnClickListener(this);
            tv_connect_tracker.setOnClickListener(this);
            onCreateValidicUserListener = this;
            onUpdateValidicDataListener = this;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_connect_phone:
                if (UtilsPreferences.getString(GetStartedActivity.this, Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_device)) {
                    finish();
                    overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                } else {
                    updateUserData(Constant.connected_with_device);
                }
                break;
            case R.id.tv_connect_validic:
                if (UtilsPreferences.getString(GetStartedActivity.this, Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_validic)) {
                    finish();
                    overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                } else {
                    if (UtilsPreferences.getString(GetStartedActivity.this, Constant.validic_id) == null) {
                        doRegisterationOnValidic();
                    } else {
                        updateUserData(Constant.connected_with_validic);
                    }
                }
                break;
            case R.id.tv_connect_tracker:
                if (UtilsPreferences.getString(GetStartedActivity.this, Constant.connect_flag).equalsIgnoreCase(Constant.connected_with_tracker)) {
                    finish();
                    overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                } else {
                    updateUserData(Constant.connected_with_tracker);
                }

                /*UtilsPreferences.setString(GetStartedActivity.this, Constant.connect_flag, Constant.connected_with_tracker);
                startActivity(new Intent(getContext(), ScanDeviceActivity.class));
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                finish();*/
                break;

            case R.id.ll_back:
                onBackPressed();
            default:
                break;
        }
    }

    // api call of ValidicCreateUser
    public void doRegisterationOnValidic() {
        try {
            JSONObject data = new JSONObject();
            JSONObject user = new JSONObject();
            user.put(Constant.uid, UtilsPreferences.getString(GetStartedActivity.this, Constant.user_id));
            data.put(Constant.user, user);
            data.put(Constant.access_token, Constant.access_token_value);

            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                ValidicCreateUserCall task = new ValidicCreateUserCall(GetStartedActivity.this, onCreateValidicUserListener, data, true);
                task.execute();
            } else {
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // success listener of ValidicCreateUserCall
    @Override
    public void onSucceedToCreateValidicUser(ValidicCreateUserModel obj) {
        try {
            UtilsPreferences.setString(GetStartedActivity.this, Constant.validic_access_token, obj.getAccess_token());
            UtilsPreferences.setString(GetStartedActivity.this, Constant.validic_id, obj.get_id());

            updateUserData(Constant.connected_with_validic);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // failed listener of ValidicCreateUserCall
    @Override
    public void onFaildToCreateValidicUser(String error_msg) {
        Toast.makeText(GetStartedActivity.this, error_msg, Toast.LENGTH_SHORT).show();
    }

    // success listener of ValidicUpdateDataCall
    @TargetApi(VERSION_CODES.JELLY_BEAN)
    @Override
    public void onSucceedToUpdateValidicData(String connection_flag) {
        if (connection_flag.equalsIgnoreCase(Constant.connected_with_validic)) {
            UtilsPreferences.setString(GetStartedActivity.this, Constant.connect_flag, connection_flag);
            startActivity(new Intent(getContext(), ConnectYourDeviceActivity.class));
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            finishAffinity();
        } else if (connection_flag.equalsIgnoreCase(Constant.connected_with_tracker)) {
            UtilsPreferences.setString(GetStartedActivity.this, Constant.connect_flag, Constant.connected_with_tracker);
            startActivity(new Intent(getContext(), ScanDeviceActivity.class));
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            if(Globals.homeDeviceMap.size() > 0)
                Globals.homeDeviceMap.clear();
            finish();
        } else {
            UtilsPreferences.setString(GetStartedActivity.this, Constant.connect_flag, Constant.connected_with_device);
            startActivity(new Intent(getContext(), DashboardActivity.class));
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            if(Globals.homeDeviceMap.size() > 0)
                Globals.homeDeviceMap.clear();
            finish();
        }
    }

    // failed listener of ValidicUpdateDataCall
    @Override
    public void onFaildToUpdateValidicData(String error_msg) {
        Toast.makeText(GetStartedActivity.this, error_msg, Toast.LENGTH_SHORT).show();
    }

    // api call of ValidicUpdateData

    /**
     *
     * @param connection_flag get connection value
     */
    public void updateUserData(String connection_flag) {
        try {
            JSONObject data = new JSONObject();
            if (UtilsPreferences.getString(GetStartedActivity.this, Constant.validic_id) != null) {
                data.put(Constant.validic_id, UtilsPreferences.getString(GetStartedActivity.this, Constant.validic_id));
                data.put(Constant.validic_access_token, UtilsPreferences.getString(GetStartedActivity.this, Constant.validic_access_token));
            } else {
                data.put(Constant.validic_id, "");
                data.put(Constant.validic_access_token, "");
            }
            data.put(Constant.connect_flag, connection_flag);
            data.put(Constant.user_id, UtilsPreferences.getString(GetStartedActivity.this, Constant.user_id));
            data.put(Constant.accesstoken, UtilsPreferences.getString(GetStartedActivity.this, Constant.accesstoken));
            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                ValidicUpdateDataCall task = new ValidicUpdateDataCall(GetStartedActivity.this, onUpdateValidicDataListener, data, true);
                task.execute();
            } else {
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
