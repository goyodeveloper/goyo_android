package com.differenzsystem.goyog.userProfile;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.FBLogin;
import com.differenzsystem.goyog.api.LoginCall;
import com.differenzsystem.goyog.api.LoginCall.OnLoginUserListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.dashboard.DashboardActivity;
import com.differenzsystem.goyog.model.LoginModel;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.Debugger;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsPermission;
import com.differenzsystem.goyog.utility.UtilsPreferences;
import com.differenzsystem.goyog.utility.UtilsValidation;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TimeZone;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;

public class LoginActivity extends AppCompatActivity implements OnClickListener, OnLoginUserListener, TextWatcher, FBLogin.OnFBLoginListener {
    EditText et_email, et_password;
    TextView tv_reg_now;
    ImageView iv_forgot_password;
    Button btn_login;
    OnLoginUserListener onLoginUserListener;
    FBLogin.OnFBLoginListener onFBLoginListener;
    //change
    LinearLayout ll_fblogin, ll_main;
    //FB login
    String TAG = getClass().getName();
    CallbackManager callbackManager;
    LoginManager loginManager;
    final static HashMap<String, String> SocialDetail = new HashMap<String, String>();

    // [START declare_analytics]
//    private FirebaseAnalytics mFirebaseAnalytics;
    // [END declare_analytics]

    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        getSupportActionBar().hide();
        initializeControls();
        initializeControlsAction();
    }

    private Context getContext() {
        // return login screen context
        return LoginActivity.this;
    }

    // initialize all controls define in xml layout
    @RequiresApi(api = VERSION_CODES.KITKAT_WATCH)
    public void initializeControls() {
        try {

            // Obtain the FirebaseAnalytics instance.
//            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
            // [END shared_app_measurement]

            btn_login = (Button) findViewById(R.id.btn_login);
            et_email = (EditText) findViewById(R.id.et_email);
            et_password = (EditText) findViewById(R.id.et_password);
            iv_forgot_password = (ImageView) findViewById(R.id.iv_forgot_password);
            tv_reg_now = (TextView) findViewById(R.id.tv_reg_now);
            //change
            ll_fblogin = (LinearLayout) findViewById(R.id.ll_fblogin);
            ll_main = (LinearLayout) findViewById(R.id.ll_main);

            btn_login.setTypeface(setFont(getContext(), R.string.app_bold));
            et_email.setTypeface(setFont(getContext(), R.string.app_regular));
            et_password.setTypeface(setFont(getContext(), R.string.app_regular));

            //UtilsPermission.checkFineLocation(LoginActivity.this);
            //UtilsPermission.checkBodySensor(LoginActivity.this);
            UtilsPermission.checkContact(LoginActivity.this);

            //FB login
            initializeFacebookCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set click listener
    public void initializeControlsAction() {
        try {
            onLoginUserListener = this;
            onFBLoginListener = this;
            btn_login.setOnClickListener(this);
            iv_forgot_password.setOnClickListener(this);
            tv_reg_now.setOnClickListener(this);
            et_email.addTextChangedListener(this);
            et_password.addTextChangedListener(this);
            ll_main.setOnClickListener(this);

            //change
            ll_fblogin.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (isValidated(true)) {
                    doLogin();
                }
                /*UtilsPreferences.setString(LoginActivity.this, Constant.connect_flag, Constant.connected_with_tracker);
                startActivity(new Intent(getContext(), DashboardActivity.class));
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                finish();*/
                break;
            case R.id.iv_forgot_password:
                openForgotPasswordScreen();
                break;
            case R.id.tv_reg_now:
                openRegisterScreen();
                break;
            case R.id.ll_fblogin:
                if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
                    /*LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_birthday",
                            "user_hometown",
                            "user_location",
                            "user_education_history",
                            "user_work_history",
                            "user_website",
                            "user_managed_groups",
                            "user_events",
                            "user_photos",
                            "user_about_me",
                            "user_status",
                            "email",
                            "public_profile"));*/

                } else {
                    // Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.ll_main:
                hidError();
                break;
        }
    }

    // hide the error text
    public void hidError() {
        UtilsCommon.hideSoftKeyboard(this);

        et_email.setFocusable(false);
        et_email.setFocusableInTouchMode(false);
        et_password.setFocusable(false);
        et_password.setFocusableInTouchMode(false);

        et_email.setFocusable(true);
        et_email.setFocusableInTouchMode(true);
        et_password.setFocusable(true);
        et_password.setFocusableInTouchMode(true);

       /* et_email.setError(null);
        et_password.setError(null);*/
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    // check validation of email and password

    /**
     * @param isShowError boolean for showing error
     * @return
     */
    public boolean isValidated(boolean isShowError) {
        boolean isValid = true;
        if (!UtilsValidation.isValidEmail(LoginActivity.this, et_email, isShowError)) {
            isValid = false;
        }
        if (!UtilsValidation.isValidPassword(LoginActivity.this, et_password, isShowError, false)) {
            isValid = false;
        }
        return isValid;
    }

    // api call of login
    public void doLogin() {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.email, et_email.getText().toString().trim());
            data.put(Constant.password, et_password.getText().toString().trim());
            data.put(Constant.device_token, UtilsPreferences.getString(LoginActivity.this, Constant.devicegcmid));
            data.put(Constant.device_type, Constant.device_type_value);
            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            data.put(Constant.timezone, tz.getID());

            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                LoginCall login = new LoginCall(LoginActivity.this, onLoginUserListener, data, true);
                login.execute();
            } else {
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // open forget password screen
    public void openForgotPasswordScreen() {
        Intent i_forgot_password = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(i_forgot_password);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // open register screen
    public void openRegisterScreen() {
        Intent i_forgot_password = new Intent(LoginActivity.this, RegistrationActivity.class);
//        Intent i_forgot_password = new Intent(LoginActivity.this, RegistrationActivity_Allset.class);
        startActivity(i_forgot_password);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    // success listener of LoginCall and save the response data in shared preferences
    @TargetApi(VERSION_CODES.JELLY_BEAN)
    @Override
    public void onSucceedToLogin(LoginModel obj) {
        if (obj != null) {

            UtilsPreferences.setString(LoginActivity.this, Constant.email, obj.getEmail());
            UtilsPreferences.setString(LoginActivity.this, Constant.full_name, obj.getFirst_name() + " " + obj.getLast_name());
            UtilsPreferences.setString(LoginActivity.this, Constant.validic_access_token, obj.getValidic_access_token());
            UtilsPreferences.setString(LoginActivity.this, Constant.validic_id, obj.getValidic_id());
            UtilsPreferences.setString(LoginActivity.this, Constant.accesstoken, obj.getAccesstoken());
            UtilsPreferences.setString(LoginActivity.this, Constant.user_id, obj.getUser_id());
            UtilsPreferences.setString(LoginActivity.this, Constant.connect_flag, obj.getConnect_flag());
            UtilsPreferences.setString(LoginActivity.this, Constant.profile_image, obj.getProfile_image());
            UtilsPreferences.setString(LoginActivity.this, Constant.registration_date, obj.getRegistration_date());
            if (obj.getLast_update_date().equalsIgnoreCase("0")) {
                UtilsPreferences.setString(LoginActivity.this, Constant.last_update_date, obj.getRegistration_date());
            } else {
                UtilsPreferences.setString(LoginActivity.this, Constant.last_update_date, obj.getLast_update_date());
            }

            if (!obj.getDevice_token().equalsIgnoreCase("")) {
                UtilsPreferences.setBoolean(LoginActivity.this, Constant.isdevicegcmidstore, true);
            }

            //setdata in global
            UtilsPreferences.setString(LoginActivity.this, Constant.first_name, obj.getFirst_name());
            UtilsPreferences.setString(LoginActivity.this, Constant.last_name, obj.getLast_name());
            UtilsPreferences.setString(LoginActivity.this, Constant.other_names, obj.getOther_names());
            UtilsPreferences.setString(LoginActivity.this, Constant.address, obj.getAddress());
            UtilsPreferences.setString(LoginActivity.this, Constant.contact_number, obj.getContact_number());
            UtilsPreferences.setString(LoginActivity.this, Constant.dob, obj.getDob());
            UtilsPreferences.setString(LoginActivity.this, Constant.occupation, obj.getOccupation());
            UtilsPreferences.setString(LoginActivity.this, Constant.marital_status, obj.getMarital_status());
            UtilsPreferences.setString(LoginActivity.this, Constant.gender, obj.getGender());
            UtilsPreferences.setString(LoginActivity.this, Constant.nic, obj.getNic());
            UtilsPreferences.setString(LoginActivity.this, Constant.nominee_nic, obj.getNominee_nic());
            UtilsPreferences.setString(LoginActivity.this, Constant.nominee_name, obj.getNominee_name());
            UtilsPreferences.setString(LoginActivity.this, Constant.nominee_address, obj.getNominee_address());
            UtilsPreferences.setString(LoginActivity.this, Constant.nominee_number, obj.getNominee_number());

            UtilsPreferences.setString(LoginActivity.this, Constant.user_type, obj.getUser_type());
            UtilsPreferences.setString(getContext(), Constant.mac_id, obj.getMac_id());
            UtilsPreferences.setString(LoginActivity.this, Constant.facebook_id, obj.getFacebook_id());
            UtilsPreferences.setString(getContext(), Constant.user_exp_date, obj.getUser_exp_date());
            UtilsPreferences.setString(getContext(), Constant.no_of_children, obj.getNo_of_children());

            UtilsPreferences.setString(getContext(), Constant.height, obj.getHeight());
            UtilsPreferences.setString(getContext(), Constant.height_scale, obj.getHeight_scale());
            UtilsPreferences.setString(getContext(), Constant.weight, obj.getWeight());
            UtilsPreferences.setString(getContext(), Constant.weight_scale, obj.getWeight_scale());
            UtilsPreferences.setString(getContext(), Constant.led_visible_status, obj.getLed_visible_status());


            // call addSDkFunctionOrder added on 22-07-2017
//            UtilsCommon.addSDkFunctionOrder(LoginActivity.this);

            if (obj.getConnect_flag().equalsIgnoreCase(Constant.not_connected)) {
                startActivity(new Intent(getContext(), GetStartedActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                finishAffinity();
            } else {
                startActivity(new Intent(getContext(), DashboardActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                finishAffinity();
            }
        }
    }

    // failed listener of LoginCall
    @Override
    public void onFaildToLogin(String error_msg) {
        Toast.makeText(LoginActivity.this, error_msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @TargetApi(VERSION_CODES.JELLY_BEAN)
    @Override
    public void afterTextChanged(Editable editable) {
       /* if (isValidated(false)) {
            btn_login.setBackground(getResources().getDrawable(R.drawable.btn_filled_solid_rounded));
        } else {
            btn_login.setBackground(getResources().getDrawable(R.drawable.btn_not_filled_solid_rounded));
        }*/
    }

    // process when screen restart
    @Override
    protected void onRestart() {
        et_email.getText().clear();
        et_password.getText().clear();
        et_email.requestFocus();
        getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(LoginActivity.this, Constant.Name_Login);
    }

    private void recordScreenViews() {
        // [START set_current_screen]
//        mFirebaseAnalytics.setCurrentScreen(this, "Login Screen", null /* class override */);
        // [END set_current_screen]
    }

    // fb login initialize process
    public void initializeFacebookCall() {
        try {
            @SuppressLint("PackageManagerGetSignatures")
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Debugger.debugE(TAG, "KeyHash : " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        //check facebook Session state
        try {
            FacebookSdk.sdkInitialize(getApplicationContext());
            callbackManager = CallbackManager.Factory.create();

            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult accesstoken) {
                            Log.e("fb", "LoginManager FacebookCallback onSuccess");
                            if (accesstoken.getAccessToken() != null) {
                                Log.e("fb", "Access Token:: "
                                        + accesstoken.getAccessToken()
                                        + accesstoken.toString());

                                GraphRequest request = GraphRequest.newMeRequest(
                                        accesstoken.getAccessToken(),
                                        new GraphRequest.GraphJSONObjectCallback() {
                                            @Override
                                            public void onCompleted(JSONObject object, GraphResponse response) {
                                                //Log.e("response", response.toString());
                                                //Log.e("Object", object.toString());
                                                try {
                                                    String fb_email = "";

                                                    if (object.has("email") && object.getString("email") != null) {
                                                        fb_email = object.getString("email");
                                                    }

                                                    SocialDetail.put(Constant.first_name, object.getString("first_name"));
                                                    SocialDetail.put(Constant.last_name, object.getString("last_name"));
                                                    SocialDetail.put(Constant.email, fb_email);
                                                    SocialDetail.put("FBID", object.getString("id"));
                                                    UtilsPreferences.setString(LoginActivity.this, Constant.facebook_id, object.getString("id"));
                                                    UtilsPreferences.setString(getContext(), Constant.emailreg, fb_email);

                                                    /*UtilsPreferences.setString(LoginActivity.this, Constant.fb_education, getFbEducationDetail(object));
                                                    UtilsPreferences.setString(LoginActivity.this, Constant.fb_groups, getFbGroupsAndEvent(object, Constant.groups));
                                                    UtilsPreferences.setString(LoginActivity.this, Constant.fb_events, getFbGroupsAndEvent(object, Constant.events));
                                                    UtilsPreferences.setString(LoginActivity.this, Constant.fb_work_exp, getFbWorkDetail(object));*/

                                                    String gender = object.getString("gender");
                                                    if (gender.equalsIgnoreCase("male")) {
                                                        SocialDetail.put(Constant.gender, "1");
                                                    } else {
                                                        SocialDetail.put(Constant.gender, "2");
                                                    }
                                                    doFBLogin();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                Bundle parameters = new Bundle();
//                                parameters.putString("fields", "email,name,link,age_range,picture,birthday,education,first_name,last_name,gender");
                                parameters.putString("fields", "id,name,education,work,picture,email,first_name,last_name,gender,link,groups,events,birthday,location");
                                request.setParameters(parameters);
                                request.executeAsync();
                            }
                        }

                        public void onCancel() {
                            Log.e("fb", "LoginManager FacebookCallback onCancel");
                        }

                        public void onError(FacebookException e) {
                            Log.e("fb", "LoginManager FacebookCallback onError" + e.getMessage());
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFbEducationDetail(JSONObject object) {
        ArrayList<String> edList = new ArrayList<>();
        try {
            JSONArray array = object.getJSONArray(Constant.education);
            for (int i = 0; i < array.length(); i++) {
                JSONObject temp = array.getJSONObject(i).getJSONObject(Constant.school);
                edList.add(temp.getString(Constant.name));
            }
            if (edList.size() > 1) {
                Log.e("Education...", TextUtils.join(",", edList));
                return TextUtils.join(",", edList);
            } else {
                return edList.get(0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getFbWorkDetail(JSONObject object) {
        ArrayList<String> edList = new ArrayList<>();
        try {
            JSONArray array = object.getJSONArray(Constant.work);
            for (int i = 0; i < array.length(); i++) {
                JSONObject temp = array.getJSONObject(i).getJSONObject(Constant.employer);
                edList.add(temp.getString(Constant.name));
            }
            if (edList.size() > 1) {
                Log.e("Work...", TextUtils.join(",", edList));
                return TextUtils.join(",", edList);
            } else {
                return edList.get(0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getFbGroupsAndEvent(JSONObject object, String key) {
        ArrayList<String> groupList = new ArrayList<>();
        try {
            JSONArray array = object.getJSONObject(key).getJSONArray(Constant.data);
            for (int i = 0; i < array.length(); i++) {
                JSONObject temp = array.getJSONObject(i);
                groupList.add(temp.getString(Constant.name));
            }
            if (groupList.size() > 1) {
                Log.e(key + "...", TextUtils.join(",", groupList));
                return TextUtils.join(",", groupList);
            } else {
                return groupList.get(0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    // handle callback from fb screen
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // api call of facebook login
    public void doFBLogin() {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.email, UtilsPreferences.getString(getContext(), Constant.emailreg));
            data.put(Constant.facebook_id, UtilsPreferences.getString(getContext(), Constant.facebook_id));
            data.put(Constant.device_token, UtilsPreferences.getString(LoginActivity.this, Constant.devicegcmid));
            data.put(Constant.device_type, Constant.device_type_value);
            TimeZone tz = TimeZone.getDefault();
            System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
            data.put(Constant.timezone, tz.getID());

            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                FBLogin login = new FBLogin(LoginActivity.this, onFBLoginListener, data, true);
                login.execute();
            } else {
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // sucess listener of FBLogin and then save the response data in shared prefrences
    @Override
    public void onSucceedToFBLogin(LoginModel obj) {

        if (obj != null) {

            UtilsPreferences.setString(LoginActivity.this, Constant.email, obj.getEmail());
            UtilsPreferences.setString(LoginActivity.this, Constant.full_name, obj.getFirst_name() + " " + obj.getLast_name());
            UtilsPreferences.setString(LoginActivity.this, Constant.validic_access_token, obj.getValidic_access_token());
            UtilsPreferences.setString(LoginActivity.this, Constant.validic_id, obj.getValidic_id());
            UtilsPreferences.setString(LoginActivity.this, Constant.accesstoken, obj.getAccesstoken());
            UtilsPreferences.setString(LoginActivity.this, Constant.user_id, obj.getUser_id());
            UtilsPreferences.setString(LoginActivity.this, Constant.connect_flag, obj.getConnect_flag());
            UtilsPreferences.setString(LoginActivity.this, Constant.profile_image, obj.getProfile_image());
            UtilsPreferences.setString(LoginActivity.this, Constant.registration_date, obj.getRegistration_date());
            if (obj.getLast_update_date().equalsIgnoreCase("0")) {
                UtilsPreferences.setString(LoginActivity.this, Constant.last_update_date, obj.getRegistration_date());
            } else {
                UtilsPreferences.setString(LoginActivity.this, Constant.last_update_date, obj.getLast_update_date());
            }

            if (!obj.getDevice_token().equalsIgnoreCase("")) {
                UtilsPreferences.setBoolean(LoginActivity.this, Constant.isdevicegcmidstore, true);
            }

            //setdata in global
            UtilsPreferences.setString(LoginActivity.this, Constant.first_name, obj.getFirst_name());
            UtilsPreferences.setString(LoginActivity.this, Constant.last_name, obj.getLast_name());
            UtilsPreferences.setString(LoginActivity.this, Constant.other_names, obj.getOther_names());
            UtilsPreferences.setString(LoginActivity.this, Constant.address, obj.getAddress());
            UtilsPreferences.setString(LoginActivity.this, Constant.contact_number, obj.getContact_number());
            UtilsPreferences.setString(LoginActivity.this, Constant.dob, obj.getDob());
            UtilsPreferences.setString(LoginActivity.this, Constant.occupation, obj.getOccupation());
            UtilsPreferences.setString(LoginActivity.this, Constant.marital_status, obj.getMarital_status());
            UtilsPreferences.setString(LoginActivity.this, Constant.gender, obj.getGender());
            UtilsPreferences.setString(LoginActivity.this, Constant.nic, obj.getNic());
            UtilsPreferences.setString(LoginActivity.this, Constant.nominee_nic, obj.getNominee_nic());
            UtilsPreferences.setString(LoginActivity.this, Constant.nominee_name, obj.getNominee_name());
            UtilsPreferences.setString(LoginActivity.this, Constant.nominee_address, obj.getNominee_address());
            UtilsPreferences.setString(LoginActivity.this, Constant.nominee_number, obj.getNominee_number());

            UtilsPreferences.setString(LoginActivity.this, Constant.user_type, obj.getUser_type());
            UtilsPreferences.setString(getContext(), Constant.mac_id, obj.getMac_id());
            UtilsPreferences.setString(LoginActivity.this, Constant.facebook_id, obj.getFacebook_id());
            UtilsPreferences.setString(getContext(), Constant.user_exp_date, obj.getUser_exp_date());
            UtilsPreferences.setString(getContext(), Constant.no_of_children, obj.getNo_of_children());


            if (obj.getConnect_flag().equalsIgnoreCase(Constant.not_connected)) {
                startActivity(new Intent(getContext(), GetStartedActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                finishAffinity();
            } else {
                startActivity(new Intent(getContext(), DashboardActivity.class));
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                finishAffinity();
            }
        }
    }

    // failed listener of FBLogin
    @Override
    public void onFaildToFBLogin(String error_msg) {
        Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
        i.putExtra("isFrom", "FB");
        startActivity(i);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }
}
