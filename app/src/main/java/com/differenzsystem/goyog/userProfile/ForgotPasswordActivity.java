package com.differenzsystem.goyog.userProfile;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.differenzsystem.goyog.Application;
import com.differenzsystem.goyog.R;
import com.differenzsystem.goyog.api.ForgotPasswordCall;
import com.differenzsystem.goyog.api.ForgotPasswordCall.OnForgotpasswordListener;
import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.utility.ConnectionDetector;
import com.differenzsystem.goyog.utility.UtilsCommon;
import com.differenzsystem.goyog.utility.UtilsValidation;

import org.json.JSONException;
import org.json.JSONObject;

import static com.differenzsystem.goyog.utility.UtilsCommon.setFont;


public class ForgotPasswordActivity extends AppCompatActivity implements OnClickListener, OnForgotpasswordListener, TextWatcher {
    EditText et_email;
    LinearLayout ll_back;
    Button btn_send_verification_link;
    TextView tv_title;
    OnForgotpasswordListener onForgotpasswordListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);
        getSupportActionBar().hide();

        initializeControls();
        initializeControlsAction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.recordScreenViews(ForgotPasswordActivity.this, Constant.Name_ForgotPassword);
    }

    private Context getContext() {
        // return the context of ForgotPasswordActivity
        return ForgotPasswordActivity.this;
    }

    // initialize all controls define in xml layout
    void initializeControls() {
        try {
            ll_back = (LinearLayout) findViewById(R.id.ll_back);
            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_title.setText(R.string.forgot_password);

            btn_send_verification_link = (Button) findViewById(R.id.btn_send_verification_link);
            et_email = (EditText) findViewById(R.id.et_email);

            btn_send_verification_link.setTypeface(setFont(getContext(), R.string.app_semibold));
            et_email.setTypeface(setFont(getContext(), R.string.app_bold));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // set click listener
    public void initializeControlsAction() {
        try {
            onForgotpasswordListener = this;
            btn_send_verification_link.setOnClickListener(this);
            ll_back.setOnClickListener(this);
            et_email.addTextChangedListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // handle click event
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send_verification_link:
                if (isValidated(true)) {
                    doForgotPasswordAction();
                }
                break;
            case R.id.ll_back:
                openLoginScreen();
                break;
        }
    }

    // back pressed click event
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    // open login screen
    public void openLoginScreen() {
        UtilsCommon.hideSoftKeyboard(ForgotPasswordActivity.this);
        Intent i_login = new Intent(getContext(), LoginActivity.class);
        startActivity(i_login);
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        finish();
    }

    // api ForgotPasswordCall
    public void doForgotPasswordAction() {
        try {
            JSONObject data = new JSONObject();
            data.put(Constant.email, et_email.getText().toString().trim());

            if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                ForgotPasswordCall task = new ForgotPasswordCall(ForgotPasswordActivity.this, onForgotpasswordListener, data, true);
                task.execute();
            } else {
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_NO_INTERNET_MSG), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // check validation of email

    /**
     * @param isshowWError boolean to show error message
     * @return
     */
    public boolean isValidated(boolean isshowWError) {
        if (UtilsValidation.isValidEmail(getContext(), et_email, isshowWError)) {
            return true;
        }
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    // edit text after text changed event listener
    @RequiresApi(api = VERSION_CODES.JELLY_BEAN)
    @Override
    public void afterTextChanged(Editable editable) {
        if (isValidated(false)) {
            btn_send_verification_link.setBackground(getResources().getDrawable(R.drawable.btn_filled_solid_rounded));
        } else {
            btn_send_verification_link.setBackground(getResources().getDrawable(R.drawable.btn_not_filled_solid_rounded));
        }
    }

    // success listener of ForgotPasswordCall
    @Override
    public void onSucceedToForgotPassword(String msg) {
        Toast.makeText(ForgotPasswordActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    // failed listener of ForgotPasswordCall
    @Override
    public void onFaildToForgotPassword(String error_msg) {
        Toast.makeText(ForgotPasswordActivity.this, error_msg, Toast.LENGTH_SHORT).show();
    }
}
