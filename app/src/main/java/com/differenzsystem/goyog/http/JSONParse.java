package com.differenzsystem.goyog.http;

import android.util.Log;

import com.differenzsystem.goyog.constant.Constant;
import com.differenzsystem.goyog.http.AndroidMultiPartEntity.ProgressListener;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Union Assurance PLC on 7/5/16.
 */
public class JSONParse {

    public static String postJSON(String serviceURL, JSONArray jsonArray) throws Exception {
        Log.e("Url", serviceURL.toString());
        Log.e("Request", jsonArray.toString());

        String result = "";
        InputStream inputStream = null;
        OutputStream outputStream = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(serviceURL);
            //make some HTTP header2
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(20000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "application/json");
            connection.connect();
            // Send POST output
            outputStream = new BufferedOutputStream(connection.getOutputStream());
            String json = jsonArray.toString();
            outputStream.write(json.getBytes());
            //clean up
            outputStream.flush();
            int code = connection.getResponseCode();
            if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                JSONResponse errorMessage = new JSONResponse();
                errorMessage.setFlag(false);
                errorMessage.setMessage("File not found on server. Please contact administrator.");
                Gson gson = new Gson();
                result = gson.toJson(errorMessage);
            } else {
                // receive response as inputStream
                inputStream = connection.getInputStream();
                if (inputStream != null) {
                    result = convertStreamToString(inputStream);
                } else {
                    result = "";
                }
            }
        } catch (Exception e) {
            JSONResponse errorMessage = new JSONResponse();
            errorMessage.setFlag(false);
            errorMessage.setMessage("Network error please try again.");
            Gson gson = new Gson();
            result = gson.toJson(errorMessage);
        } finally {
            //clean up
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            connection.disconnect();
        }

        Log.e("Response", result.toString());
        return result;
    }

    public static String postJSON(String serviceURL, JSONObject jsonObj) throws Exception {
        Log.e("Url", serviceURL.toString());
        Log.e("Request", jsonObj.toString());

        String result = "";
        InputStream inputStream = null;
        OutputStream outputStream = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(serviceURL);
            //make some HTTP header2
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(20000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "application/json");
            connection.connect();
            // Send POST output
            outputStream = new BufferedOutputStream(connection.getOutputStream());
            String json = jsonObj.toString();
            outputStream.write(json.getBytes());
            //clean up
            outputStream.flush();
            int code = connection.getResponseCode();
            if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                JSONResponse errorMessage = new JSONResponse();
                errorMessage.setFlag(false);
                errorMessage.setMessage("File not found on server. Please contact administrator.");
                Gson gson = new Gson();
                result = gson.toJson(errorMessage);
            } else {
                // receive response as inputStream
                inputStream = connection.getInputStream();
                if (inputStream != null) {
                    result = convertStreamToString(inputStream);
                } else {
                    result = "";
                }
            }
        } catch (Exception e) {
            JSONResponse errorMessage = new JSONResponse();
            errorMessage.setFlag(false);
            errorMessage.setMessage("Network error please try again.");
            Gson gson = new Gson();
            result = gson.toJson(errorMessage);
            Log.d("InputStream", e.getLocalizedMessage());
        } finally {
            //clean up
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            connection.disconnect();
        }

        Log.e("Response", result.toString());
        return result;
    }

    public static String callPostWithFile(String url, File file) {

        String response_string = null;

        // Creating HTTP client
        HttpClient httpClient = new DefaultHttpClient();
        // Creating HTTP Post
        HttpPost httpPost = new HttpPost(url);
        try {
            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(new ProgressListener() {
                @Override
                public void transferred(long num) {

                }
            });
            entity.addPart(Constant.profile_image, new FileBody(file));
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity r_entity = response.getEntity();
            int status_code = response.getStatusLine().getStatusCode();
            if (status_code == 200) {
                response_string = EntityUtils.toString(r_entity);
            } else {
                response_string = "Error occurred! Http Status Code: " + status_code;
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response_string;
    }


    public static String GetJSON(String serviceURL) throws IOException {
        Log.e("Url", serviceURL.toString());

        String result = "";
        InputStream inputStream = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(serviceURL);
            // make GET request to the given URL
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.connect();
            int code = connection.getResponseCode();
            if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                JSONResponse errorMessage = new JSONResponse();
                errorMessage.setFlag(false);
                errorMessage.setMessage("File not found on server. Please contact administrator.");
                Gson gson = new Gson();
                result = gson.toJson(errorMessage);
            } else {
                // receive response as inputStream
                inputStream = connection.getInputStream();
                if (inputStream != null)
                    result = convertStreamToString(inputStream);
                else {
                    result = "";
                }
            }
        } catch (Exception e) {
            JSONResponse errorMessage = new JSONResponse();
            errorMessage.setFlag(false);
            errorMessage.setMessage("Network error please try again.");
            Gson gson = new Gson();
            result = gson.toJson(errorMessage);
        } finally {
            //clean up
            if (inputStream != null) {
                inputStream.close();
            }
            connection.disconnect();
        }

        Log.e("Response", result.toString());
        return result;
    }

    // convert InputStream to String
    private static String convertStreamToString(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    //
    public static JSONObject getLocationFromUrl(String serviceURL, JSONObject jsonObj) throws Exception {
        Log.e("Url", serviceURL.toString());
        Log.e("Request", jsonObj.toString());

        String result = "";
        JSONObject jObj = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(serviceURL);
            //make some HTTP header2
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "application/json");
            connection.connect();
            // Send POST output
            outputStream = new BufferedOutputStream(connection.getOutputStream());
            String json = jsonObj.toString();
            outputStream.write(json.getBytes());
            //clean up
            outputStream.flush();
            int code = connection.getResponseCode();
            if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                JSONResponse errorMessage = new JSONResponse();
                errorMessage.setFlag(false);
                errorMessage.setMessage("File not found on server. Please contact administrator.");
                Gson gson = new Gson();
                result = gson.toJson(errorMessage);
            } else {
                // receive response as inputStream
                inputStream = connection.getInputStream();
                if (inputStream != null) {
                    result = convertStreamToString(inputStream);
                    Log.e("Response", result.toString());
                } else {
                    result = "";
                }
            }
        } catch (Exception e) {
            JSONResponse errorMessage = new JSONResponse();
            errorMessage.setFlag(false);
            errorMessage.setMessage("Network error please try again.");
            Gson gson = new Gson();
            result = gson.toJson(errorMessage);
            Log.d("InputStream", e.getLocalizedMessage());
        } finally {
            //clean up
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            connection.disconnect();
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(result);
        } catch (JSONException e) {
            JSONResponse errorMessage = new JSONResponse();
            errorMessage.setFlag(false);
            errorMessage.setMessage("Network error please try again.");
            Gson gson = new Gson();
            result = gson.toJson(errorMessage);
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return jObj;
    }
}
