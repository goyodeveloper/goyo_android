package com.differenzsystem.goyog.http;

/**
 * Created by Union Assurance PLC on 7/5/16.
 */
public class JSONResponse {
    private boolean flag;
    private String message;
    private boolean is_accesstoken_valid = true;

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getIs_accesstoken_valid() {
        return is_accesstoken_valid;
    }

    public void setIs_accesstoken_valid(boolean is_accesstoken_valid) {
        this.is_accesstoken_valid = is_accesstoken_valid;
    }
}
